<?php

	require_once("../../config.inc.php");
			
	$main_module = new Spedizioni();

	$cfg_mod = $main_module->get_cfg_mod();
	
	$users = new Users;	
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
	}
	
	$ar_email_json = acs_je($ar_email_to);	

	//parametri del modulo (non legati al profilo)
	$mod_js_parameters = $main_module->get_mod_parameters();
	
	//parametri del modulo (legati al profilo)
	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());	
	
	
	//forzatura per operatore Order Entry
	if ($js_parameters->only_OE == 1) $_REQUEST['menu_type'] = 'ORD_VEN';
	
	if(isset($_REQUEST['menu_type']))
	    $menu_type = $_REQUEST['menu_type'];
    else
        $menu_type = 'PRO_MON';
        
        
    if ($menu_type == 'PRO_MON')
        $_module_descr = " Desktop Programmazione/Monitor";
    else
        $_module_descr = " Desktop Apertura/Gestione Ordini cliente";
	
?>



<html>
<head>
<title>ACS Portal_DoVe</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />

<!-- <link rel="stylesheet" type="text/css" href="resources/css/calendar.css" /> -->
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/extensible-all.css" />
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/calendar.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />

<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">

/* ELENCO DESTINAZIONI */
#pan-html-dest .x-panel-body {
    background:#ffe;
    color:#15428B;
}
#pan-html-dest .x-panel-body table td {
    border: 1px solid gray;
}


p {
    margin:5px;
}
.settings {
    background-image:url(../shared/icons/fam/folder_wrench.png);
}
.nav {
    background-image:url(../shared/icons/fam/folder_go.png);
}
.info {
    background-image:url(../shared/icons/fam/information.png);
}

/* div#page-header{background-color: white;} SPOSTATO IN STYLE.CSS */

/* PRIMA CELLA tree (per non andare a capo nel caso di immagini) */
/* (adesso invece dichiaro nella colonna tdCls: auto-height o no-auto-height
 .x-grid-row.liv_3 .x-grid-cell-first div.x-grid-cell-inner{white-space: normal;}
 .x-grid-row.liv_5 .x-grid-cell-first div.x-grid-cell-inner{white-space: normal;}
*/

.x-grid-cell.no-auto-height .x-grid-cell-inner {white-space: nowrap;}
.x-grid-cell.auto-height .x-grid-cell-inner {white-space: normal;}


/*tr.liv_totale td.x-grid-cell{background-color: #F4ED7C; border: 1px solid white;}*/
tr.liv_totale td.x-grid-cell{background-color: #F4EDAF; border: 1px solid white;}
tr.liv_totale td.x-grid-cell.festivo{opacity: 0.6;}
tr.liv_0 td.x-grid-cell{border: 1px solid white; font-weight: bold;}
tr.liv_1 td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: bold;}
tr.liv_1_no_b td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: normal;}
tr.liv_1.no_carico td.x-grid-cell{background-color: #C4D8E2}
.x-grid-row.liv_2 .x-grid-cell{background-color: #D1CEDD; border: 1px solid white;}
.x-grid-row.liv_3 .x-grid-cell{background-color: #DDDDDD; border: 1px solid white;}

/*disponibiltą colli*/
.x-grid-row.liv_1 .x-grid-cell.disponibile{background-color: #60C659;}
.x-grid-row.liv_2 .x-grid-cell.disponibile{background-color: #A0DB8E;}
.x-grid-row.liv_3 .x-grid-cell.disponibile{background-color: #a9de99;}   
.x-grid-row.liv_4 .x-grid-cell.disponibile{background-color: #bce5af;} 
 
/*NONdisponibiltą colli*/
.x-grid-row.liv_1 .x-grid-cell.non_disponibile{background-color: #FC9BB2;}
.x-grid-row.liv_2 .x-grid-cell.non_disponibile{background-color: #F9BFC1;}
.x-grid-row.liv_3 .x-grid-cell.non_disponibile{background-color: #facbcd;}   
.x-grid-row.liv_4 .x-grid-cell.non_disponibile{background-color: #fbd8d9;}     

/*VERNICIATURA*/
.x-grid-row.liv_1 .x-grid-cell.vern{background-color: #EAAF0F;}  
.x-grid-row.liv_2 .x-grid-cell.vern{background-color: #F2D13D;} 
.x-grid-row.liv_3 .x-grid-cell.vern{background-color: #F4DB60;} 
.x-grid-row.liv_4 .x-grid-cell.vern{background-color: #F4E287;}        


div#grid-arrivi .x-grid-row.liv_0 .x-grid-cell{background-color: #F4EDAF; border: 1px solid white;}

tr.liv_4 td.x-grid-cell{background-color: #EEEEEE; border: 1px solid white;}

tr.liv_totale.dett_selezionato td.x-grid-cell{background-color: #F4ED7C; font-weight: bold;}
.x-grid-row.liv_totale.flight.dett_selezionato .x-grid-cell{background-color: #FFB777; border: 1px solid white;}


.x-grid-row.liv_totale.flight .x-grid-cell{background-color: #FFD69B; border: 1px solid white;}

.x-grid-cell.tipoOrd.A {background-color: white;}   /* assistenze */
.x-grid-cell.tipoOrd.A.toDoListTp {background-color: dddddd;}   /* assistenze in ToDoList  */
.x-grid-cell.tipoOrd.M {background-color: #93C6E0;} /* mostre */
.x-grid-cell.tipoOrd.O {background-color: #A0DB8E;} /* ordini */
.x-grid-cell.tipoOrd.P {background-color: #E2D67C;} /* composizioni promozionali */
.x-grid-cell.tipoOrd.C {background-color: #D3BFB7;} /* cccc */
.x-grid-cell.tipoOrd.V {background-color: #D3BFB7;} /* materiale promozionale */
.x-grid-cell.tipoOrd.T {background-color: #ffbe7d;} /* ordini di test */

.x-grid-row.liv_0 .x-grid-cell-selected {opacity: 0.6;}
.x-grid-row.liv_1 .x-grid-cell-selected {opacity: 0.6;}
.x-grid-row.liv_2 .x-grid-cell-selected {opacity: 0.6;}
.x-grid-row.liv_3 .x-grid-cell-selected {opacity: 0.6;}
.x-grid-row.liv_4 .x-grid-cell-selected {opacity: 0.6;}

.x-grid-row-selected.liv_0 .x-grid-cell{opacity: 0.6;}
.x-grid-row-selected.liv_1 .x-grid-cell{opacity: 0.6;}
.x-grid-row-selected.liv_2 .x-grid-cell{opacity: 0.6;}
.x-grid-row-selected.liv_3 .x-grid-cell{opacity: 0.6;}
.x-grid-row-selected.liv_4 .x-grid-cell{opacity: 0.6;}

tr.liv_0 td.x-grid-cell.tp_pri_4
 , tr.liv_1 td.x-grid-cell.tp_pri_4
 , tr.liv_2 td.x-grid-cell.tp_pri_4
 , tr.liv_3 td.x-grid-cell.tp_pri_4 
 {background-color: #F9BFC1;} /* priorita' rosa */

tr.liv_0 td.x-grid-cell.tpSfondoRosa
 , tr.liv_1 td.x-grid-cell.tpSfondoRosa
 , tr.liv_2 td.x-grid-cell.tpSfondoRosa
 , tr.liv_3 td.x-grid-cell.tpSfondoRosa 
 , tr.liv_4 td.x-grid-cell.tpSfondoRosa
 , tr.liv_5 td.x-grid-cell.tpSfondoRosa
 {background-color: #F9BFC1;}

 tr.liv_0 td.x-grid-cell.tpSfondoGiallo
 , tr.liv_1 td.x-grid-cell.tpSfondoGiallo
 , tr.liv_2 td.x-grid-cell.tpSfondoGiallo
 , tr.liv_3 td.x-grid-cell.tpSfondoGiallo 
 , tr.liv_4 td.x-grid-cell.tpSfondoGiallo
 , tr.liv_5 td.x-grid-cell.tpSfondoGiallo
 {background-color: #F4E287;}
 
 tr.liv_0 td.x-grid-cell.tpSfondoVerde
 , tr.liv_1 td.x-grid-cell.tpSfondoVerde
 , tr.liv_2 td.x-grid-cell.tpSfondoVerde
 , tr.liv_3 td.x-grid-cell.tpSfondoVerde 
 , tr.liv_4 td.x-grid-cell.tpSfondoVerde
 , tr.liv_5 td.x-grid-cell.tpSfondoVerde
 {background-color: #A0DB8E;} 
 
 tr.liv_0 td.x-grid-cell.tpSfondoBianco
 , tr.liv_1 td.x-grid-cell.tpSfondoBianco
 , tr.liv_2 td.x-grid-cell.tpSfondoBianco
 , tr.liv_3 td.x-grid-cell.tpSfondoBianco 
 , tr.liv_4 td.x-grid-cell.tpSfondoBianco
 , tr.liv_5 td.x-grid-cell.tpSfondoBianco
 {background-color: #FFFFFF;} 
 
 
 .x-grid-cell.tpSfondoGrigio{background-color: #cecece;} 
 .x-grid-cell.tpSfondoRosa{background-color: #F9BFC1;}
 
 .x-grid-row .x-grid-cell.liv_dettaglio_disponibilita {opacity: 0.5;}
 
tr.liv_0 td.x-grid-cell.tpSfondoCelesteEl
 , tr.liv_1 td.x-grid-cell.tpSfondoCelesteEl
 , tr.liv_2 td.x-grid-cell.tpSfondoCelesteEl
 , tr.liv_3 td.x-grid-cell.tpSfondoCelesteEl 
 , tr.liv_4 td.x-grid-cell.tpSfondoCelesteEl
 , tr.liv_5 td.x-grid-cell.tpSfondoCelesteEl 
 {background-color: #99D6DD;} 


 /*RIGHE ORDINE*/
.x-grid-row.rigaRevocata .x-grid-cell{background-color: #F9BFC1}
.x-grid-row.rigaChiusa .x-grid-cell{background-color: #CEEA82}
.x-grid-row.rigaParziale .x-grid-cell{background-color: #F4EDAF}
.x-grid-row.rigaNonSelezionata .x-grid-cell{background-color: #cccccc}
 


.x-panel td.x-grid-cell.festivo{opacity: 0.6;}
.x-panel td.x-grid-cell.con_carico_1{background-color: #909090; color: white; font-weight: bold;} /* SOLO ALCUNI HANNO IL CARICO */
.x-panel td.x-grid-cell.con_carico_2{background-color: #3c3c3c; color: white; font-weight: bold;}  /* HANNO TUTTI IL CARICO */
.x-panel td.x-grid-cell.con_carico_3{background-color: #F9827F; font-weight: bold;}  /* NON CARICO. CON DATE CONF. */
.x-panel td.x-grid-cell.con_carico_4{background-color: #F9BFC1; font-weight: bold;}  /* NON CARICO. CON DATE TASS. */
.x-panel td.x-grid-cell.con_carico_5{background-color: #CEEA82; font-weight: bold;}  /* ALCUNI EVASI */
.x-panel td.x-grid-cell.con_carico_6{background-color: #8CD600; font-weight: bold;}  /* TUTTI EVASI */


.x-panel.arrivi .x-grid-row.liv_4 .x-grid-cell{font-weight: bold;}
.x-panel.arrivi .x-grid-row.liv_4 .rif.x-grid-cell{font-weight: normal;} /* itinerario cliente non bold */

.x-panel.tree-calendario table{border-spacing: 0px;}
.x-panel .x-column-header-text{font-weight: bold;}

/* ICE.... */
/*.x-panel .x-column-header.plan-day .x-column-header-inner{background-color: red;}*/
.x-panel#calendario .x-column-header.plan-day .x-column-header-inner{line-height: 1em; border-bottom: 6px solid tranparent;}
.x-panel#calendario .x-column-header.plan-day.stadioDay0 .x-column-header-inner{border-bottom: 6px solid white;}
.x-panel#calendario .x-column-header.plan-day.stadioDay1 .x-column-header-inner{border-bottom: 6px solid black;}
.x-panel#calendario .x-column-header.plan-day.stadioDay2 .x-column-header-inner{border-bottom: 6px solid #f9827f;}
.x-panel#calendario .x-column-header.plan-day.stadioDay3 .x-column-header-inner{border-bottom: 6px solid #A0DB8E;}

.x-grid-cell.colorStadio0{background-color: white; border: 1px solid gray;}
.x-grid-cell.colorStadio1{background-color: black; border: 1px solid gray;}
.x-grid-cell.colorStadio2{background-color: #f9827f; border: 1px solid gray;}
.x-grid-cell.colorStadio3{background-color: #A0DB8E; border: 1px solid gray;}


/*sfondo bianco*/
.x-border-layout-ct{background-color: white;}
 
.iconSpedizione {background-image: url(<?php echo img_path("icone/16x16/spedizione.png") ?>) !important;}
.iconFabbrica {background-image: url(<?php echo img_path("icone/16x16/fabbrica.png") ?>) !important;}
.iconPartenze {background-image: url(<?php echo img_path("icone/16x16/partenze.png") ?>) !important;}
.iconPrint {background-image: url(<?php echo img_path("icone/16x16/print.png") ?>) !important;}
.iconGlobe {background-image: url(<?php echo img_path("icone/16x16/globe.png") ?>) !important;}
.iconCarico {background-image: url(<?php echo img_path("icone/16x16/carico.png") ?>) !important;}
.iconScaricoIntermedio {background-image: url(<?php echo img_path("icone/16x16/exchange.png") ?>) !important;}
.iconDestinazioneFinale {background-image: url(<?php echo img_path("icone/16x16/home.png") ?>) !important;}
.iconPuzzle {background-image: url(<?php echo img_path("icone/16x16/puzzle.png") ?>) !important;}
.iconRefresh {background-image: url(<?php echo img_path("icone/16x16/rss_blue.png") ?>) !important;}
.iconArtCritici {background-image: url(<?php echo img_path("icone/16x16/arrivi.png") ?>) !important;}
.iconAccept {background-image: url(<?php echo img_path("icone/16x16/sub_blue_accept.png") ?>) !important;}
.iconRIPRO {background-image: url(<?php echo img_path("icone/16x16/button_black_repeat_dx.png") ?>) !important;}
.iconScarichiRidondanti {background-image: url(<?php echo img_path("icone/16x16/warning_red.png") ?>) !important;}
.iconDelivery {background-image: url(<?php echo img_path("icone/16x16/delivery.png") ?>) !important;}
.iconBox {background-image: url(<?php echo img_path("icone/16x16/box_open.png") ?>) !important;}
.iconClienti {background-image: url(<?php echo img_path("icone/16x16/clienti.png") ?>) !important;}
.iconInfoGray {background-image: url(<?php echo img_path("icone/16x16/info_gray.png") ?>) !important;}

.iconNoIcon {
	display : none;
	background-image:url('ext/resources/images/default/s.gif') !important;
 }

.iconBlocco {background-image: url(<?php echo img_path("icone/16x16/divieto.png") ?>) !important;}
.iconConf {background-repeat: no-repeat;
    background-image: url(<?php echo img_path("icone/16x16/puntina_rossa.png") ?>) !important;
}
.iconEvaso {
	background-repeat: no-repeat;
    background-image: url(<?php echo img_path("icone/16x16/lock.png") ?>) !important;
}

.iconNonProgrammabile {
	background-repeat: no-repeat;
    background-image: url(<?php echo img_path("icone/16x16/button_blue_pause.png") ?>) !important;
}

.carico-folder-SP {
	background-repeat: no-repeat;
    background-image: url(<?php echo img_path("icone/16x16/carico_spedito.png") ?>) !important;
}
.carico-folder-CO {
	background-repeat: no-repeat;
    background-image: url(<?php echo img_path("icone/16x16/carico_controllato.png") ?>) !important;
}
.carico-folder-LT {
	background-repeat: no-repeat;
    background-image: url(<?php echo img_path("icone/16x16/button_black_play.png") ?>) !important;
}


.x-column-header-text img{margin-left: -5px; margin-top: -7px;}

div.acs_button{display: inline; margin-top: -30px; padding: 0px;}
div.acs_button img{margin-top: 1px;}
div.acs_button span{margin-top: -5px;}

/* img#cal_fl_solo_con_carico_assegnato_img{float: right;} */
/* img.cell-img{float: right;} */

table.tab-legend tr td, table.tab-legend tr th{border: 1px solid gray; font-size: 0.8em;}
table.tab-legend tr th{background-color: #c0c0c0;}
table.tab-legend tr td img{width: 24px; padding: 2px;}

.tab-legend th{font-weight: bold; text-align: center;}
.tab-legend th.A{background-color: black; color: white;}
.tab-legend td.A{background-color: #D0D0D0; color: black;}
.tab-legend th.B{background-color: #D0D0D0; color: black;}
.tab-legend td.B{background-color: white; color: black;}

span.c_disp{font-size: 0.7em;}

/* disponibilita' */
.x-grid-row.positivo td.dispo{color: green;}
.x-grid-row.negativo td.dispo{color: red;}


.x-panel.acs_toolbar .x-panel-body{
	background-repeat: no-repeat;
	background-size:100% 100%;
    background-image: url(<?php echo img_path("sfondo_toolbar.jpg") ?>) !important;
}

textarea.segnalazioni{color: #cccccc;}
textarea.segnalazioni.errore{color: red; font-size: 12px;}

.x-grid-cell.td_warning_txt{color: red; font-weight: bold;}
.x-grid-cell.td_bg_green_01{background-color: #AADD96;}
.x-grid-cell.td_bg_red_01{background-color: red;}
.x-grid-cell.td_bg_yellow_01{background-color: #E0EA68;}

p.nota-liv_0{color: gray; font-weight: normal; text-indent: 30px; margin: 0; padding: 0;}
p.orario-liv_0{color: gray; font-weight: normal; text-indent: 70px; margin: 0; padding: 0;}
p.sottoriga-liv_0{color: gray; font-weight: normal; margin: 0; padding: 0;}


.x-panel.acs-light .x-panel-header.x-panel-header-default { 
 background-image: -moz-linear-gradient(center top , #F4EDAF, #F4EDAF 45%, #F4EDAF 46%, #F4EDAF 50%, #F4EDAF 51%, #F4EDAF); 
 background-image: -webkit-linear-gradient(top,#F4EDAF,#F4EDAF 45%,#F4EDAF 46%,#F4EDAF 50%,#F4EDAF 51%,#F4EDAF);
 }
 
.x-panel.acs-light_rx .x-panel-header.x-panel-header-default { 
 background-image: -moz-linear-gradient(center top , #AADD96, #AADD96 45%, #AADD96 46%, #AADD96 50%, #AADD96 51%, #AADD96);
 background-image: -webkit-linear-gradient(top,#AADD96,#AADD96 45%,#AADD96 46%,#AADD96 50%,#AADD96 51%,#AADD96); 
 } 
 
.acs_warning{color: red; font-weight: bold;}

.x-grid-row .x-grid-cell.grassetto{font-weight: bold;}
.x-grid-row .x-grid-cell.font-normal{font-weight: normal;}

.x-grid-row .x-grid-cell.selected_lx {background: #F4EDAF; }
.x-grid-row .x-grid-cell.selected_rx {background: #AADD96; }

.x-form-fieldcontainer.row_d .x-form-display-field {background: #D1CEDD; }

.grassetto{font-weight: bold;}




.x-grid-cell.dirtyBianco { 
  background-image: url(<?php echo img_path("dirtyBianco.gif") ?>);
  background-position: 0 0;
  background-repeat: no-repeat; 
}



/* Martina: resi */
.x-btn-group.top .x-btn {
	background : #99BADD;
	padding: 9px 15px; 
	display: inline-block;
    width: 180px;
 }
.x-btn-group.top .x-btn:hover {background: black;}
.x-btn-group.top .x-btn .x-btn-center .x-btn-inner {font-size: 18px; color: white;}
.x-btn-group.top .x-btn-group-header-text-container {background : #99BADD;}
.x-btn-group.top .x-btn-group-header-text-container .x-btn-group-header-text{
  	font-size: 22px;
 	color: white;
 	font-weight: bold;
}
.x-btn-group.top  .x-box-item .x-form-item-body .x-form-field{
  	color: black;
  	font-size: 20px;
  	text-align: center;
  	width: 50px;
  	height: 50;
}
.x-btn-group.top  .x-btn  {
  	border: 10px solid white;
  	border-radius: 4px;
  	width: 120px;
  	height: 40;
}

.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .m-1{background : #A0DB8E;}
.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .m-2{background : #a9de99;}
.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .m-3{background : #bce5af;}
.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .m-4{background : #f7eaab;}
.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .m-5{background : #f5e493;}
.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .m-6{background : #f4e287;}
.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .a_anno{background : #fbd8d9;}
.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .a_due_anni{background :#facbcd;}
.x-grid.grid-anzianita  .x-grid-header-ct-default  .x-box-inner .oltre{background : #F9BFC1;}

/* /Martina: resi */



/* dataview */
.thumb-wrap {
    background-color: #F7F7F9;
    text-align: center;
    vertical-align: bottom;
    border: 1px outset #D0D0D0;
    padding: 4px;
    margin: 2px;
    float: left;
    display: inherit;
    overflow: hidden;
    font: normal .8em Arial;
    height: 70px;
    width: 78px;
}
.thumb-wrap .displayLabel {
    white-space: nowrap;
    overflow: hidden;
    color: #000000;
}
.thumb-wrap:hover{
   border:1px solid red;
}



.div-notifica{
	position:relative;
	display:inline-block;
}

.bt-notifica{
   position:absolute;
   top:-10px;
   right:1px;
   font-size:.7em;
   background:red;
   color:white;
   /*width:30px;*/
   padding: 2px;
   height:20px;
   text-align:center;
   line-height:12px;
   border-radius:40%;
   border: 1px solid red;
}


</style>


<!-- GMaps API Key that works for dev.sencha.com -->
<!-- GMaps API Key that works for localhost -->
<!-- script type="text/javascript" src="http://www.google.com/jsapi?key=AIzaSyB5PXZq9jDyVcXlAvMGIrWFHnsBWY0HTDE&sensor=false"></script> -->
<!-- script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAA2CKu_qQN-JHtlfQ5L7BLlRT2yXp_ZAY8_ufC3CFXhHIE1NvwkxQl3I3p2yrGARYK4f4bkjp9NHpm5w" type="text/javascript"></script> -->

<script type="text/javascript" src="../../../extjs/ext-all.js"></script>

<script type="text/javascript" src="../../js/extensible-1.5.2/lib/extensible-all-debug.js"></script>
<script type="text/javascript" src="../../js/extensible-1.5.2/src/locale/extensible-lang-it.js"></script>

<!--  script type="text/javascript" src="../../../extjs/ext-theme-neptune.js"></script> -->

<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>


<!--  BUONOOOO script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> -->



    <!-- script src="ext-3.0.0/examples/ux/GMapPanel.js"></script> -->


<?php require '../../templates/import_gmap_api.php'; ?>


<script src=<?php echo acs_url("js/acs_js.js") ?>></script>





<script type="text/javascript">

<?php
  //memorizzo menu_type (PRO_MON o ORD_VEN)
  echo "menu_type = " . acs_je($menu_type) . ";";
?>  


	var refresh_stato_aggiornamento = {
        run: doRefreshStatoAggiornamento,
        interval: 30 * 1000 //millisecondi
      }
	
	function doRefreshStatoAggiornamento() {
        Ext.Ajax.request({
            url : 'get_stato_aggiornamento.php' ,
            method: 'GET',
            success: function ( result, request) {
                var jsonData = Ext.decode(result.responseText);
                var btStatoAggiornamento = document.getElementById('bt-stato-aggiornamento-esito');
                btStatoAggiornamento.innerHTML = jsonData.html;
            },
            failure: function ( result, request) {
            }
        });
	}

	

	var refresh_notifiche = {
	        run: doRefreshNotifiche,
	        interval: 60 * 1000 //millisecondi
	      }

	function doRefreshNotifiche() {
        Ext.Ajax.request({
            url : 'get_stato_notifiche.php' ,
            method: 'GET',
            success: function ( result, request) {
                var jsonData = Ext.decode(result.responseText);
                var btBookingNotifica = document.getElementById('bt-booking-notifica');

                if (Ext.get("bt-booking") != null){
                    if (jsonData.num_booking > 0){                    
                    	btBookingNotifica.innerHTML = jsonData.num_booking;
                    	Ext.get("bt-booking-notifica").show();
                    } else {
                    	btBookingNotifica.innerHTML = '0';
                    	Ext.get("bt-booking-notifica").hide();
                    }                    
                }                

                	                
            },
            failure: function ( result, request) {
            }
        });
	}        



	function test_ref(grid_id){
		Ext.getCmp(grid_id).getStore().load();		
	}

	function call_acs_refresh_all(ext_id){
		//console.log(Ext.getCmp(ext_id));
		Ext.getCmp(ext_id).acs_refresh_all();		
	}	
		 
	function gmapPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=400, height=400, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}

	function allegatiPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=600, height=600, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}	


	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {
            //'Ext.calendar': 'src',
	        "Extensible": "../../js/extensible-1.5.2/src", 
			"Extensible.example": "../../js/extensible-1.5.2/examples"	                   
        }	
	});Ext.Loader.setPath('Ext.ux', 'ux');

    Ext.require(['*', 'Ext.ux.GMapPanel', 'Ext.ux.grid.FiltersFeature', 'Ext.selection.CheckboxModel', 'Ext.ux.grid.column.ProgressBar'
                 //, 'Ext.calendar.util.Date'
                 //, 'Ext.calendar.CalendarPanel'
                 //, 'Ext.calendar.data.MemoryCalendarStore'
                 //, 'Ext.calendar.data.MemoryEventStore'
                 //, 'Ext.calendar.data.Events'
                 //, 'Ext.calendar.data.Calendars'
                 //, 'Extensible.Date',
                 // , 'Extensible.Extensible'
                 , 'Extensible.calendar.data.MemoryEventStore'
                 , 'Extensible.calendar.CalendarPanel'  
                 , 'Extensible.example.calendar.data.Events'
				 , 'Extensible.calendar.data.CalendarMappings',
    			 , 'Extensible.calendar.data.EventMappings'
    			// , 'Extensible.calendar.dd.DayDragZone'                                
    			// , 'Extensible.calendar.dd.DayDropZone'
     			, 'Ext.ux.grid.Printer'
     			, 'Ext.ux.RowExpander'    			 
                 ]);
    


    var mapwin;








    /* ELENCO ORDINI */        
    	var colElencoOrdini = [	
    		{text: 'Carico<br>&nbsp;', 		xtype: 'treecolumn', width: 250, dataIndex: 'task',
    		  menuDisabled: true, sortable: false,        		        	  
          	  renderer: function (value, metaData, record, row, col, store, gridView){	

				if (record.get('liv') == 'liv_1'){
			    	
			    	//if (record.get('tooltip_acc_paga').length > 0){		
			        if (Ext.isEmpty(record.get('tooltip_acc_paga'))==false && record.get('tooltip_acc_paga').length > 0){	    	
		    			metaData.tdAttr = 'data-qtip= "' + Ext.String.htmlEncode(record.get('tooltip_acc_paga')) + '"';;
			    	}

		    	}

              	  					
  				if (record.get('liv') == 'liv_3' || record.get('liv') == 'liv_1'){
  	  				 metaData.tdCls += ' auto-height';
  	  			
  	  				 //if (record.get('tooltip_ripro').length > 0)
  	  				 if (Ext.isEmpty(record.get('tooltip_ripro'))==false && record.get('tooltip_ripro').length > 0)
	    			 	metaData.tdAttr = ' data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_ripro')) + '"';
  				}											
  				return value;			    
				}},



				/* {
		            xtype: 'actioncolumn',
		            text: '<img src=<?php echo img_path("icone/48x48/mobile.png") ?> width=25>',
		            width: 25, menuDisabled: true, sortable: false,
		            stopSelection : false,
		            items: [{
		            	icon: <?php echo img_path("icone/24x24/mobile.png") ?>,
		                // Use a URL in the icon config
		                tooltip: 'Menu',
		               
		                handler: function (grid, rowIndex, colIndex, a, event, node) {
		        			var rec = grid.getStore().getAt(rowIndex);	
		        			
		        			
							 if (rec.get('liv') == 'liv_0')			  	
		        				showMenu_liv_0(grid, rec, node, rowIndex, event);
		        	         if (rec.get('liv') == 'liv_1')			  	
		        				showMenu_liv_1(grid, rec, node, rowIndex, event);
		        	         if (rec.get('liv') == 'liv_2')	
		        	            showMenu_liv_2(grid, rec, node, rowIndex, event);
		        		     if (rec.get('liv') == 'liv_3')			  	
		        				showMenu_liv_3(grid, rec, node, rowIndex, event);		
							
							
		                }
		            }]
		        },*/


				<?php
						//inclusione colonne flag ordini
						//$js_include_flag_ordini = array('menuDisabled' => false, 'sortable' => true);
						require("_include_js_column_flag_ordini.php"); 
					?>,	


    			    				    		
    		{text: 'Seq', 			width: 33, dataIndex: 'seq_carico',
    			    				menuDisabled: false, sortable: false,
    		  renderer: function(value, metaData, record){
        		  
		    	if (record.get('liv') == 'liv_2'){
			    	
			    	if (!Ext.isEmpty(record.get('tooltip_seq_carico')) && record.get('tooltip_seq_carico') > 0){				    	
		    			metaData.tdAttr = 'data-qtip="Consegna entro il ' + date_from_AS(Ext.String.htmlEncode(record.get('tooltip_seq_carico'))) + '"';
			    	}

			    	if (record.get('dirty_seq_carico') == 1)
		    			metaData.tdCls += ' ' + 'dirtyBianco';

			    	return value;
		    	}


		    	if (record.get('liv') == 'liv_3'){
	    	    	if(record.get('contract').trim() != ''){
		    			metaData.tdCls += ' tpSfondoGiallo';
		    			var tooltip = '<br>Contract abbinato: ' + record.get('contract');
		    		}else{
		    		    var tooltip = '';
		    		}
		    		
		    	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_seq_carico') + tooltip) + '"';			    	
		    		
	    		if (record.get('liv') == 'liv_3' && value== 'S')//contrassegno
    			 	return '<img src=<?php echo img_path("icone/48x48/bandiera_grey.png") ?> width=18>';
    		    
		    	if (record.get('liv') == 'liv_3' && value== 'I') //indice rottura
    	    		return '<img src=<?php echo img_path("icone/48x48/sticker_black.png") ?> width=18>';
		    		    	
		    	if (record.get('liv') == 'liv_3' && value== 'SI') //contrassegno e indice rottura
	    			return '<img src=<?php echo img_path("icone/48x48/sticker_blue.png") ?> width=18>';
    		    
    		}

		    	
		    	}},
    	    {text: 'Localit&agrave;/Riferimento', 	flex: 1, dataIndex: 'riferimento',  menuDisabled: false, sortable: false},
    	    {text: 'Tp', 			width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd',
    	    	 	menuDisabled: false, sortable: false, 
	    	    	renderer: function (value, metaData, record, row, col, store, gridView){						
							metaData.tdCls += ' ' + record.get('raggr');								
					return value;			    
					}},
    	    {text: 'Data', 			width: 60, dataIndex: 'data_reg', renderer: date_from_AS,  menuDisabled: false, sortable: false},
    	    {text: 'St', 			width: 30, dataIndex: 'stato',  menuDisabled: false, sortable: false},
    	    {text: 'Pr', 			width: 35, dataIndex: 'priorita', tdCls: 'tpPri',
    	    	 					menuDisabled: false, sortable: false,
    				renderer: function (value, metaData, record, row, col, store, gridView){

    					if (record.get('tp_pri') == 4)
    						metaData.tdCls += ' tpSfondoRosa';

    					if (record.get('tp_pri') == 6)
    						metaData.tdCls += ' tpSfondoCelesteEl';					
    					
    					return value;
    				    
    	    			}},
    		{text: 'Evasione<br>richiesta',	width: 60, dataIndex: 'cons_rich', menuDisabled: false, sortable: false,
				renderer: function (value, metaData, record, row, col, store, gridView){
				    if (record.get('liv') == 'liv_2' && !Ext.isEmpty(record.get('tooltip_mezzi'))){		    	
                       metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_mezzi')) + '"';
                       if (record.get('s_mezzi_red') === true)
      						metaData.tdCls += ' tpSfondoRosa';
				       return value;
				   }else
					   return date_from_AS(value);
				    
	    			}},	    	    	    
    	    {text: 'Colli',			width: 35, dataIndex: 'colli', align: 'right',  menuDisabled: false, sortable: false},	       
    	    {text: 'D',	width: 30, tdCls: 'tdAction',
    	    	 			menuDisabled: false, sortable: false, 
    			    renderer: function(value, p, record){
    			    	if (parseInt(record.get('colli_disp')) == 0 ) return '';
    			    	if (parseInt(record.get('colli_disp')) <  parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_none.png") ?> width=18>';			    	
    			    	if (parseInt(record.get('colli_disp')) >= parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_full_yellow.png") ?> width=18>';			    	
    			    	}},	    
    	    {text: 'S',	width: 30, tdCls: 'tdAction',
    			    		 menuDisabled: false, sortable: false,    			    	    
    			    renderer: function(value, p, record){
    			    	if (parseInt(record.get('colli_sped')) == 0) return '';
    			    	if (parseInt(record.get('colli_sped')) <  parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_none_black.png") ?> width=18>';			    	
    			    	if (parseInt(record.get('colli_sped')) >= parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_full_green.png") ?> width=18>';			    	
    			    	
    			    	}},
    	<?php if ($mod_js_parameters->v_pallet == 1){ ?>    			    	
    	    {text: 'Pallet',			width: 40, dataIndex: 'pallet',
    	    	 		menuDisabled: false, sortable: false,        	    
    			    renderer: function(value, p, record){
    			    	if (record.get('liv') == 'liv_0'){ //spedizione
        			    	ret = record.get('pallet');
        			    	if (parseFloat(record.get('pallet_disp')) > 0)
        			    		ret += '<br><p class="sottoriga-liv_0">[' + record.get('pallet_disp') + ']</p>';
    			    		return ret;    			    		
    			    	}	
    			    	else
    			    		return value;	
    			    }	    
    	    },
    	<? } ?> 

          	  {text: 'C.Prod.',	width: 50, dataIndex: 'c_prod', align: 'right',
        	   menuDisabled: false, sortable: false, hidden : true, tooltip : 'Colli di produzione',       	    
              },   			    		    
    	    {text: 'Volume',			width: 50, dataIndex: 'volume', align: 'right',
    	    	 		menuDisabled: false, sortable: false,        	    
    			    renderer: function(value, p, record){
    			    	if (record.get('liv') == 'liv_0'){ //spedizione
        			    	ret = record.get('volume');
        			    	if (parseFloat(record.get('volume_disp')) > 0)
        			    		ret += '<br><p class="sottoriga-liv_0">[' + record.get('volume_disp') + ']</p>';
    			    		return ret;
    			    	}	
    			    	else
    			    		return value;	
    			    }	    
    	    },	    
    		{text: 'Peso',				width: 55, dataIndex: 'peso', align: 'right',
    	    	 		menuDisabled: false, sortable: false,		
    			    renderer: function(value, p, record){
    			    	if (record.get('liv') == 'liv_0'){ //spedizione
    			    		ret = record.get('peso');
        			    	if (parseFloat(record.get('peso_disp')) > 0)
        			    		ret += '<br><p class="sottoriga-liv_0">[' + record.get('peso_disp') + ']</p>';
    			    		return ret;
    			    		
    			    	}	
    			    	else
    			    		return value;	
    			    }		
    		},

    		
    <?php if ($js_parameters->p_nascondi_importi != 'Y'){ ?>		
    		{text: 'Importo',			width: 80, dataIndex: 'importo', align: 'right',  menuDisabled: false, sortable: false,
   			 renderer: function (value, metaData, record, row, col, store, gridView){ 
					if(record.get('i_paga')!= ''){	
  				    	var icona = '<img src= "/acs_portal/images/' + record.get('i_paga') + '" width=18>';
   						var i_paga = '<span style="display: inline; float: left;">' + icona +'</span>';
    				return i_paga + value;
					}else
						return value;
  						
  			}},
    <?php } ?>
    		
    		{text: 'Evasione<br>conferm',	width: 60, dataIndex: 'cons_conf',
    			 menuDisabled: false, sortable: false,        		
        		 renderer: function (value, metaData, record, row, col, store, gridView){

					/* non va bene perche' rimane memorizzata nascosta anche in day
      				if (store.proxy.reader.jsonData.tipo_elenco == 'GATE')
     					gridView.headerCt.getHeaderAtIndex(col).hide(); */
            		  
    				if (store.proxy.reader.jsonData.tipo_elenco == 'HOLD'){
    					gridView.headerCt.getHeaderAtIndex(col).setText('Evasione<br>corrente');
    					return date_from_AS(record.get('cons_prog'), metaData, record);
    				} else {					
    					gridView.headerCt.getHeaderAtIndex(col).setText('Evasione<br>conferm');					
    					return date_from_AS(record.get('cons_conf'), metaData, record);
    				}
    							
    			}
    		},
    		{text: 'Dispon.',			width: 60, dataIndex: 'data_disp',
    			 menuDisabled: false, sortable: false,        		
        	  renderer: function (value, metaData, record, row, col, store, gridView){    					
    			if (record.get('liv') == 'liv_2' || record.get('liv') == 'liv_3'){
    				if (record.get('fl_data_disp') == '1')
    					metaData.tdCls += ' tpSfondoRosa';
    			}	 		 
    				return date_from_AS(value, metaData, record);
    			}
    		}

    	<?php if ($mod_js_parameters->v_gg_rit == 1){ ?>
    		,{text: 'GG<br>Rit.', width: 30, dataIndex: 'gg_rit',  menuDisabled: false, sortable: false},
    	<?php } ?>					
    	]
    	
    	






	function show_win_help(codice_help){
    		acs_show_win_std('Help on-line', 'acs_help_win.php?fn=open_form', {codice: codice_help}, 600, 600, {}, 'icon-info_blue-16');   			
    }

    
    
    function show_google_map() {
        // create the window on the first click and reuse on subsequent clicks
        if(!mapwin){

            mapwin = Ext.create('Ext.Window', {
                layout: 'fit',
                title: 'GMap Window',
                closeAction: 'hide',
                width:450,
                height:450,
                border: false,
                x: 40,
                y: 60,
                items: {
                    xtype: 'gmappanel',
                    zoomLevel: 14,
                    gmapType: 'map',
                    mapConfOpts: ['enableScrollWheelZoom','enableDoubleClickZoom','enableDragging'],
                    mapControls: ['GSmallMapControl','GMapTypeControl','NonExistantControl'],
                    setCenter: {
                        geoCodeAddr: '4 Yawkey Way, Boston, MA, 02215-3409, USA',
                        marker: {title: 'Fenway Park'}
                    },
                    markers: [{
                        lat: 42.339419,
                        lng: -71.09077,
                        marker: {title: 'Northeastern University'}
                    }]
                }
            });
            
        }
        
        mapwin.show();
        
    };



	//per form
	var myNumberField = Ext.extend(Ext.form.NumberField, {
        setValue : function(v){
            v = typeof v == 'number' ? v : String(v).replace(this.decimalSeparator, ".");
            v = isNaN(v) ? '' : String(v).replace(".", this.decimalSeparator);
            //  if you want to ensure that the values being set on the field is also forced to the required number of decimal places.
            // (not extensively tested)
            // v = isNaN(v) ? '' : this.fixPrecision(String(v).replace(".", this.decimalSeparator));
            return Ext.form.NumberField.superclass.setValue.call(this, v);
        },
        fixPrecision : function(value){
            var nan = isNaN(value);
            if(!this.allowDecimals || this.decimalPrecision == -1 || nan || !value){
               return nan ? '' : value;
            }
            return parseFloat(value).toFixed(this.decimalPrecision);
        }
    });





 function show_win_crt_promemoria(){

 var f = Ext.create('Ext.form.Panel', {
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        width: 600,
        url: 'acs_op_exe.php',
        fieldDefaults: {
            labelAlign: 'top',
            msgTarget: 'side'
        },

        items: [{
        	xtype: 'hidden',
        	name: 'fn',
        	value: 'crt_promemoria'
        	}, {
                xtype: 'combo',
                anchor: '100%',                
                store: Ext.create('Ext.data.ArrayStore', {
                    fields: [ 'email', 'descr' ],
                    data: <?php echo $ar_email_json ?>
                }),
                displayField: 'descr',
                valueField: 'email',
                value: '<?php global $auth; echo $auth->get_email(); ?>',
                fieldLabel: 'A',
                queryMode: 'local',
                selectOnTab: false,
                allowBlank: false,
                name: 'to'
            },
            {
            xtype: 'container',
            anchor: '100%',
            layout:'column',
            items:[{
                xtype: 'container',
                columnWidth:.5,
                layout: 'anchor',
                items: [{
                    xtype:'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Data'
					   , name: 'f_data'
					   , format: 'd/m/Y'
					   , submitFormat: 'Ymd'
					   , allowBlank: false
                       , anchor:'96%'
                       , minValue: new Date()
                }]
            },{
                xtype: 'container',
                columnWidth:.5,
                layout: 'anchor',
                items: [{
                    xtype:'timefield'
					   , fieldLabel: 'Ora'
					   , name: 'f_ora'
					   , format: 'H:i'						   
					   , submitFormat: 'Hi'
					   , allowBlank: false
                    , anchor:'96%'
                }]
            }]
        }, {
            xtype: 'textfield',
            name: 'f_oggetto',
            fieldLabel: 'Oggetto',
            anchor: '96%',
		    allowBlank: false
        }
        , {
            //xtype: 'htmleditor',
            xtype: 'textareafield',
            name: 'f_text',
            fieldLabel: 'Testo del messaggio',
            height: 200,
            anchor: '96%',
		    allowBlank: false            
        }],

        buttons: [{
            text: 'Conferma',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var m_win = this.up('window');	            	
	                form.submit(
								{
		                            //waitMsg:'Loading...',
		                            success: function(form,action) {
										m_win.close();		                            	
		                            },
		                            failure: function(form,action){
		                            }
		                        }	                	
	                );
		            //this.up('window').close();            	                	                
	             }            
        		},{
            text: 'Annulla'
        }]
    });


		// create and show window
		var win = new Ext.Window({
		  width: 800
		, height: 380
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Registra promemoria via email con data/ora invio'
		, iconCls: 'icon-clock-16'			
		, layout: 'fit'
		, border: true
		, closable: true
		, items: f
		});
		win.show();  

}





 function show_win_note_cliente(tddt, tdccon, tdcdes, tdnboc){	
 	// create and show window
		print_w = new Ext.Window({
				  width: 800
				, height: 300
				, minWidth: 300
				, minHeight: 300
				, plain: false
				, title: 'Segnalazioni di gestione spedizione'
				, layout: 'fit'
				, border: true
				, closable: true
				, maximizable: false
				, iconCls: 'icon-blog_compose-16'										
			});	
		print_w.show();

			//carico la form dal json ricevuto da php
			Ext.Ajax.request({
			        url        : 'acs_form_json_note_cliente.php?fn=view&tddt=' + tddt + '&tdccon=' + tdccon + '&tdcdes=' + tdcdes + '&tdnboc=' + tdnboc,
			        method     : 'POST',
			        waitMsg    : 'Data loading',
			        success : function(result, request){
			            var jsonData = Ext.decode(result.responseText);
			            print_w.add(jsonData.items);
			            print_w.doLayout();				            
			        },
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			    });		
} //show_win_annotazioni_ordine


 

function show_win_annotazioni_ordine(k_ordine, liv1, liv2){	
        	// create and show window
			print_w = new Ext.Window({
					  width: 800
					, height: 300
					, minWidth: 300
					, minHeight: 300
					, plain: false
					, title: 'Annotazioni ordine'
					, layout: 'fit'
					, border: true
					, closable: true
					, maximizable: false										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_form_json_annotazioni_ordine.php?k_ordine=' + k_ordine + '&liv1=' + liv1 + '&liv2=' + liv2,
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });		
} //show_win_annotazioni_ordine




function show_win_annotazioni_ordine_bl(k_ordine, bl){	
	// create and show window
	print_w = new Ext.Window({
			  width: 800
			, height: 300
			, minWidth: 300
			, minHeight: 300
			, plain: false
			, title: 'Annotazioni ordine'
			, layout: 'fit'
			, border: true
			, closable: true
			, maximizable: false										
		});	
	print_w.show();

		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : 'acs_form_json_annotazioni_ordine.php?k_ordine=' + k_ordine + '&bl=' + bl,
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            print_w.add(jsonData.items);
		            print_w.doLayout();				            
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		    });		
} //show_win_annotazioni_ordine



function show_scheda_booking(nr_sped, bstime){	
	acs_show_win_std('Scheda booking', '../desk_booking/acs_booking.php?fn=open_parameters', {nr_sped: nr_sped, bstime : bstime, visualizza: 'Y'}, null, null, {}, 'icon-calendar-16', 'Y');
} //show_scheda_booking



function show_win_entry_ordine(k_ordine, liv1, liv2){	

	var win = new Ext.Window({
		  width: 900
		, height: 500
		, minWidth: 600
		, minHeight: 400
		, plain: true
		, title: 'Riepilogo attivit&agrave; acquisizione/avanzamento ordine aperte'
		, layout: 'fit'
		, border: true
		, closable: true
		, items:  
			new Ext.grid.GridPanel({
				
					store: new Ext.data.Store({
						
						listeners: {
						            load: function () {
						                win.setTitle('Riepilogo attivit&agrave; acquisizione/avanzamento ordine aperte ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO);
						            }
						         },
						
						groupField: 'RDTPNO',			
						autoLoad:true,				        
	  					proxy: {
								url: 'acs_get_order_entry.php?nord=' + k_ordine,
								type: 'ajax',
								reader: {
						            type: 'json',
						            root: 'root'
						        }, 
						        extraParams: {}
							},
		        			fields: [
		            			'ASCAAS', 'ASUSAS', 'ASDTAS', 'ASHMAS', 'ASNOTE', 'ASUSAT', 'ASDTVI', 'ASDTSC', 'ASCAAS_OUT', 'ASDTRI', 'ASHMRI', 'ASCARI', 'ASCARI_OUT', 'ASNORI'
		        			]
		    			}),


		    	        tbar: new Ext.Toolbar({
		    	            items:[
							{
								name: 'f_anche_rilasciate',
								xtype: 'checkboxgroup',
								fieldLabel: 'Anche rilasciate',
								labelAlign: 'right',
							   	allowBlank: true,
							   	items: [{								   	
							            xtype: 'checkbox'
								      , width: 250
								      , labelWidth: 170
							          , name: 'f_anche_rilasciate' 
							          , boxLabel: ''
							          , inputValue: 'Y'		                          
							        }],
							    listeners: {
							    	change: function(checkbox, checked){
										   m_grid = this.up('window').down('grid');
										   m_grid.store.proxy.extraParams.anche_rilasciate = checked
										   m_grid.store.load();			            						            			 	            					            					                    		
							    	}
							    }    														
							}				    	            
		    	         ]            
		    	        }),		    			
  						
			        columns: [
			             {
			                header   : 'Causale',
			                dataIndex: 'ASCAAS_OUT', 
			                flex: 1, 
			                renderer: function(value, p, record){
				                if(record.get('ASCARI').trim() != '' && record.get('ASCARI_OUT') != null){
					                //info rilascio
					                value += '<BR> -> ' + record.get('ASCARI_OUT');
				                }
				                return value;
			                }
			             }, {
				            header   : 'Assegnata da',
				            dataIndex: 'ASUSAS', 
				            flex: 1
				         }, {
					            header   : 'Assegnata il',
					            dataIndex: 'ASDTAS', 
					            flex: 1,
					            renderer: function(value, p, record){
						            value = datetime_from_AS(record.get('ASDTAS'), record.get('ASHMAS'));

						            if (record.get('ASDTRI') > 0){
						                //info rilascio
						                value += '<BR>' + datetime_from_AS(record.get('ASDTRI'), record.get('ASHMRI'));
					                }
						            
					            	return value;
					            }					            
					     }, {
					            header   : 'Note',
					            dataIndex: 'ASNOTE', 
					            flex: 1,
					            renderer: function(value, p, record){
						            if (record.get('ASDTRI') > 0){
						                //info rilascio
						                value += '<BR>' + record.get('ASNORI');
					                }
						            
					            	return value;
					            }				
					            
					     }, {
					            header   : 'Assegnata a',
					            dataIndex: 'ASUSAT', 
					            flex: 1
					     }, {
					            header   : 'Notificata dal',
					            dataIndex: 'ASDTVI', 
					            flex: 1,
					            renderer: date_from_AS
					     }, {
					            header   : 'Scadenza',
					            dataIndex: 'ASDTSC', 
					            flex: 1,
					            renderer: date_from_AS
					            
					     }
			            
			         ]})   

		});
	win.show();
	
} //show_win_entry_ordine






function show_win_assegna_carico(grid, rec, node, index, event){	
        	// create and show window
			print_w = new Ext.Window({
					  width: 600
					, height: 450
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Assegna nuovo numerco carico'
					, layout: 'fit'
					, border: true
					, closable: true
					, iconCls: 'iconCarico'
				});	
			print_w.show();

			  	id_selected = grid.getSelectionModel().getSelection();
			  	list_selected_id = [];
			  	for (var i=0; i<id_selected.length; i++) 
				   list_selected_id.push(id_selected[i].data.id);


			  	for (var i=0; i<id_selected.length; i++){
				  k_carico_exp = id_selected[i].data.k_carico.split('_');				  					  	
				  if (parseInt(k_carico_exp[2]) != 0) {
					  acs_show_msg_error('Operazione ammesso solo su ordini senza carico associato');					  
					  print_w.close();
					  return false;
				  } 
				  
				  if (id_selected[i].data.liv != 'liv_2') {
					  acs_show_msg_error('Selezionare solo righe a livello cliente');					  
					  print_w.close();
					  return false;					  
				  }


<?php if (!isset($mod_js_parameters->ASS_CAR_bl) || $mod_js_parameters->ASS_CAR_bl != 'Y'){ ?>				  
				  //verifico che non ci siano ordini/clienti bloccati
				  if (parseInt(id_selected[i].get('fl_bloc')) > 0 || id_selected[i].get('iconCls') == 'iconBlocco'){
					  acs_show_msg_error('Operazione non ammessa con clienti o ordini bloccati');					  
					  print_w.close();
					  return false;					  
				  }	  
<?php } ?>					  	  
				  
			  	}  
			  	
			  	
				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_form_json_assegna_carico.php',
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        jsonData: {selected_id: list_selected_id, tree_id: grid.id}, 
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();				            
				        },
				        failure    : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            Ext.Msg.alert('Errore', jsonData.error_log);
				        }
				    });		
} //show_win_assegna_carico





function show_win_upd_spedizione(grid, rec, node, index, event){	
        	// create and show window
			print_w = new Ext.Window({
					  width: 600
					, height: 550
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Modifica spedizione'
			    	, iconCls : 'icon-delivery-16'						
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_form_json_spedizione.php',
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        jsonData: {rec_id: rec.get('id'), 
				        		   rec_liv: rec.get('liv'),
				        		   tree_id: grid.id,
				        		   itin_id: grid.initialConfig.grid.cod_iti}, 
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });		
} //show_win_upd_spedizione


function show_win_progetta_spedizioni(grid, rec, node, index, event){	
	// create and show window
	print_w = new Ext.Window({
			  width: 600
			, height: 450
			, minWidth: 300
			, minHeight: 300
			, plain: true
			, title: 'Composizione spedizioni'
	    	, iconCls : 'iconPuzzle'						
			, layout: 'fit'
			, border: true
			, closable: true
			
			, 	    listeners:{
        				beforeClose: function(){

	    						//aggiorno il day
	    						grid.getStore().treeStore.load();
    						        					
        						return this.down('panel').verifica_on_close();
				        }
		    }									

		});	
	print_w.show().maximize();

		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : 'acs_form_json_progetta_spedizioni.php',
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        jsonData: {tree_id: grid.id,
		        		   itin_id: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti,
		        		   data: grid.getStore().treeStore.proxy.reader.jsonData.data,
		        		   sped_ar: grid.getStore().treeStore.proxy.extraParams.sped_ar}, 
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            print_w.add(jsonData.items);
		            print_w.doLayout();				            
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		    });		
} //show_win_progetta_spedizioni




function show_win_upd_carico(grid, rec, node, index, event){	
        	// create and show window
			print_w = new Ext.Window({
					  width: 600
					, height: 470
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Modifica carico'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_form_json_carico.php?fn=open_carico',
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        jsonData: {rec_id: rec.get('id'), 
				        		   rec_liv: rec.get('liv'),
				        		   tree_id: grid.id,
				        		   itin_id: grid.initialConfig.grid.cod_iti}, 
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });		
} //show_win_upd_carico




function show_win_elenco_stampa(grid, rec, node, index, event, tipo_elenco){	
        	// create and show window
			print_w = new Ext.Window({
					  width: 600
					, height: 400
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Parametri report scarichi'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_get_elenco_stampa_form.php',
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        jsonData: {
					        	rec_id: rec.get('id'), 
					        	rec_liv: rec.get('liv'),
					        	tipo_elenco: tipo_elenco
					    }, 
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });		
} //show_win_elenco_stampa

function show_win_elenco_stampa_hold(grid, rec, node, index, event){	
	// create and show window
	print_w = new Ext.Window({
			  width: 600
			, height: 400
			, minWidth: 300
			, minHeight: 300
			, plain: true
			, title: 'Parametri report scarichi - hold'
			, layout: 'fit'
			, border: true
			, closable: true										
		});	
	print_w.show();

		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : 'acs_get_elenco_stampa_form_hold.php',
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        jsonData: { cod_iti: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti, 
			        		k_cli_des: rec.get('k_cli_des')}, 
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            print_w.add(jsonData.items);
		            print_w.doLayout();				            
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		    });		
} //show_win_elenco_stampa_hold





function show_win_assegna_data(grid, rec, node, index, event, confermaData){

	if(typeof(confermaData)==='undefined') confermaData = '';
	
  	id_selected = grid.getSelectionModel().getSelection();

  	list_selected_id = [];
  	for (var i=0; i<id_selected.length; i++){
		  //devo prendere solo livelli ordine o cliente
		  if (id_selected[i].data.liv != 'liv_2' && id_selected[i].data.liv != 'liv_3' && 
				  ( id_selected[i].data.liv == 'liv_1' && id_selected.length > 1 )
			  ) {
			  acs_show_msg_error('Selezionare solo righe a livello ordine o cliente o una singola riga carico');
			  return false;
		  }

		record_data = id_selected[i].data;
		record_data['riferimento'] = ''; //lo pulisco perche' se c'e' una \ va non e' piu' un json valido  
	 	//list_selected_id.push(id_selected[i].data);
		list_selected_id.push(record_data);		  
  	}


<?php if ($cfg_mod_Spedizioni['ASS_SPED_disabilita_verifica_colli_spuntati'] != 'Y'){ ?>  	
  	//impedisco se ha colli spuntati
  	for (var i=0; i<id_selected.length; i++){
		  //devo prendere solo livelli ordine o cliente
		  if (parseInt(id_selected[i].data.colli_sped) > 0) {
			  acs_show_msg_error('Operazione non ammessa con colli spuntati');
			  return false;
		  } 	  	 
	}
<? } ?>	
  	


	  //in INFO e HOLD non si puo' assegnare spedizione a clienti/ordini bloccati o evasi
	  if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO' ||
			  grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD'){	    	  
	    	id_selected = grid.getSelectionModel().getSelection();

	    	
	    	
	    	for (var i=0; i<id_selected.length; i++)
	  		if (
	  		  		//se non sono un amministratore di modulo segnalo anomalia su ordine bloccato
	  		  		<?php if ($js_parameters->p_mod_param != 'Y'){ ?>
	  		  			parseFloat(id_selected[i].get('fl_bloc')) > 1 ||
	  		  		<?php } ?>
	  		  		 
	  		  		parseFloat(id_selected[i].get('fl_evaso')) > 0  ||
	  				parseFloat(id_selected[i].get('fl_cli_bloc')) > 0
	  				//id_selected[i].parentNode.get('iconCls') == 'iconBlocco'
		  				){
	  					acs_show_msg_error('Operazione non ammessa su ordini/clienti bloccati o evasi');
	  			  		return false;			
	  		}
	  }
	
		// create and show window
		print_w = new Ext.Window({
		  width: 1050
		, height: 400
		, minWidth: 500
		, minHeight: 300
		, plain: true
		, title: 'Assegna produzione/disponibilit&agrave; alla spedizione'
    	, iconCls: 'iconFabbrica'		
		, layout: 'fit'
		, border: true
		, closable: true  
		});
		print_w.show();

		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : 'acs_form_json_assegna_spedizione.php',
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        jsonData: {list_selected_id: list_selected_id, cod_iti: grid.initialConfig.grid.cod_iti, grid_id: grid.id, confermaData: confermaData}, 
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            print_w.add(jsonData.items);
		            print_w.doLayout();				            
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		});		
}



function show_win_assegna_scarico_intermedio(grid, rec, node, index, event, tipo){

	if(typeof(tipo)==='undefined') tipo = '';
	
  	id_selected = grid.getSelectionModel().getSelection();

  	list_selected_id = [];
  	for (var i=0; i<id_selected.length; i++){

		  //devo prendere solo livelli cliente
		  if (id_selected[i].data.liv != 'liv_2') {
			  acs_show_msg_error('Selezionare solo righe a livello cliente');
			  return false;
		  }  		
  	  	 
		   list_selected_id.push(id_selected[i].data);
  	}
	
		// create and show window
		print_w = new Ext.Window({
		  width: 800
		, height: 500
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Assegna scarico/deposito'
    	, iconCls: 'iconScaricoIntermedio'		
		, layout: 'fit'
		, border: true
		, closable: true  
		});
		print_w.show();

		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : 'acs_form_json_assegna_scarico_intermedio.php',
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        jsonData: {list_selected_id: list_selected_id, cod_iti: grid.initialConfig.grid.cod_iti, grid_id: grid.id, tipo: tipo}, 
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            print_w.add(jsonData.items);
		            print_w.doLayout();				            
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		});		
}








function show_win_assegna_destinazione_finale(grid, rec, node, index, event, tipo){

	if(typeof(tipo)==='undefined') tipo = '';
	
  	id_selected = grid.getSelectionModel().getSelection();

  	list_selected_id = [];
  	var m_parent_id = id_selected[0].parentNode.id
  	var abilita_sel_multipla = true;
  	
  	for (var i=0; i<id_selected.length; i++){

		  //devo prendere solo livelli cliente
		  if (id_selected[i].data.liv != 'liv_2' && id_selected[i].data.liv != 'liv_3') {
			  acs_show_msg_error('Selezionare solo righe a livello cliente o ordine');
			  return false;
		  }

		  k_carico_exp = id_selected[i].data.k_carico.split('_');	
		  if (id_selected[i].data.liv == 'liv_3' && k_carico_exp[2] > 0) {
			  acs_show_msg_error('Operazione non ammessa su singolo ordine con carico abbinato');
			  return false;
		  }
  	  	 
		  list_selected_id.push(id_selected[i].data);

		  if (id_selected[i].data.liv != 'liv_3' || id_selected[i].parentNode.id != m_parent_id)
			  abilita_sel_multipla = false;
  	}

		if (list_selected_id.length > 1 && abilita_sel_multipla != true){
			acs_show_msg_error('Selezione multipla valida solo per ordini appartenenti allo stesso livello');
			return false;			
		}
	
		// create and show window
		print_w = new Ext.Window({
		  width: 800
		, height: 300
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Assegna destinazione cliente'
    	, iconCls: 'iconDestinazioneFinale'		
		, layout: 'fit'
		, border: true
		, closable: true  
		});
		print_w.show();

		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : 'acs_form_json_assegna_destinazione_finale.php',
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        jsonData: {list_selected_id: list_selected_id, cod_iti: grid.initialConfig.grid.cod_iti, grid_id: grid.id, tipo: tipo}, 
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            print_w.add(jsonData.items);
		            print_w.doLayout();				            
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		});		
}






function show_win_assegna_destinazione_sosta_tecnica(grid, rec, node, index, event, tipo){

	if(typeof(tipo)==='undefined') tipo = '';
	
  	id_selected = grid.getSelectionModel().getSelection();

  	list_selected_id = [];
  	for (var i=0; i<id_selected.length; i++){

		  //devo prendere solo livelli cliente
		  if (id_selected[i].data.liv != 'liv_2' && id_selected[i].data.liv != 'liv_3') {
			  acs_show_msg_error('Selezionare solo righe a livello cliente o ordine');
			  return false;
		  }

		  k_carico_exp = id_selected[i].data.k_carico.split('_');	
		  if (id_selected[i].data.liv == 'liv_3' && k_carico_exp[2] > 0) {
			  acs_show_msg_error('Operazione non ammessa su singolo ordine con carico abbinato');
			  return false;
		  }
  	  	 
		   list_selected_id.push(id_selected[i].data);
  	}

		if (list_selected_id.length > 1){
			acs_show_msg_error('Selezionare un singolo valore');
			return false;			
		}


		acs_show_win_std('Destinazine sosta tecnica', 'acs_form_json_assegna_destinazione_sosta_tecnica.php?fn=open_form', 
						{list_selected_id: list_selected_id, cod_iti: grid.initialConfig.grid.cod_iti, grid_id: grid.id, tipo: tipo}, 
						800, 450, {}, 'icon-outbox-16');		

	/*	
		// create and show window
		print_w = new Ext.Window({
		  width: 800
		, height: 300
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Assegna destinazione cliente'
    	, iconCls: 'iconDestinazioneFinale'		
		, layout: 'fit'
		, border: true
		, closable: true  
		});
		print_w.show();

		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : 'acs_form_json_assegna_destinazione_finale.php',
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        jsonData: {list_selected_id: list_selected_id, cod_iti: grid.initialConfig.grid.cod_iti, grid_id: grid.id, tipo: tipo}, 
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            print_w.add(jsonData.items);
		            print_w.doLayout();				            
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		});
	*/				
}


















function show_win_art_reso(k_cli_des){
	// create and show window
	var win = new Ext.Window({
	  width: 900
	, height: 500
	, minWidth: 600
	, minHeight: 400
	, plain: true
	, title: 'Elenco resi autorizzati'
	, iconCls: 'icon-recycle_bin-16'
	, layout: 'fit'
	, border: true
	, closable: true
	, items:  
		new Ext.grid.GridPanel({
			
				store: new Ext.data.Store({
			
					autoLoad:true,				        
  					proxy: {
							url: 'acs_get_reso_rows.php?k_cli_des=' + k_cli_des,
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'root'
					        }
						},
	        			fields: [
	            			'TDDTEP', 'RDFMAN', 'RDDTDS', 'RDDFOR', 'RDDTEP', 'RDTPNO', 'RDOADO', 'RDONDO', 'RDRIFE', 'RDART', 'RDDES1', 'RDDES2', 'RDUM', 'RDDT', 'RDODER', {name: 'RDQTA', type: 'float'}
	        			]
	    			}),
						
						
		        columns: [
					{
					    header   : 'Data',
					    dataIndex: 'RDODER', 
					    width    : 80,
					    renderer: date_from_AS
					 }, {
		                header   : 'Articolo',
		                dataIndex: 'RDDES1', 
		                width    : 300,
		                renderer: function(value, p, record){
		                	return record.get('RDDES1') + " [" + record.get('RDART') + "]"
		    			}			                			                
		             }, {
		                header   : 'Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : 'Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right', renderer: floatRenderer2
		             }, {
		                header   : 'Note',
		                dataIndex: 'RDDES2', 
		                flex    : 1
		             }
		            
		         ]})   

	});

	win.show();
}







function show_el_ordini(view, rec, item, index, eventObj, proxy_url, panel_id, sped_ar){

	if (Ext.isEmpty(sped_ar)){
		sped_ar = [];
	}
		

	el = Ext.create('Ext.tree.Panel', {		
        title: 'Loading...',
        
        stateful: true,
        stateId: 'day-std',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],

        //per rimuovere il salvataggio dell'ultimo ordinamento e ripristinare quello standar (senza ordinamento)
        //non funziona!
        applyState2222: function(state) {
            console.log('applyState');
            if (state) {
                console.log(state);
                var newState = Ext.apply({}, state);
                delete newState['sort'];
                console.log(newState);
                Ext.apply(this, newState );
            }
        },
        
        id: panel_id,
        cod_iti: "",

        defaultSortable: false,
        enableSort: false, // disable sorting
        
        //selType: 'cellmodel',
        <?php echo make_tab_closable(); ?>,
                
        tbar: new Ext.Toolbar({
            items:['<b>Loading...</b>', '->'
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
            <?php echo make_tbar_closable() ?>
         ]            
        }),        
        
        cls: 'elenco_ordini',
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        loadMask: true,
        store: Ext.create('Ext.data.TreeStore', {
                autoLoad: true,                    
			    model: 'ModelOrdini',
			    
				listeners: {
				        	beforeload: function(store, options) {
				        		Ext.getBody().mask('Loading... ', 'loading').show();
				        		},		
					
				            load: function () {
				              Ext.getCmp(panel_id).setTitle(this.proxy.reader.jsonData.titolo_panel);

				              //Aggiorno il testo della toolbar
				              Ext.getCmp(panel_id).dockedItems.items[1].items.items[0].setText('<b>' + this.proxy.reader.jsonData.tbar_title + '</b>');

				              if (this.proxy.reader.jsonData.tipo_elenco == 'HOLD'){

				            	//abilito l'ordinamento su task
				            	el.headerCt.getHeaderAtIndex(0).sortable = true;  			
					              
					            //TODO: nascondore colonna cons_conf in base a title data_index
				              	//el.headerCt.getHeaderAtIndex(19).setText('Data<br>disponib.');
				              	//el.headerCt.getHeaderAtIndex(20).hide(); //nascondo gg rit.
					            //console.log(el);  	
				              }
				              	
				              el.headerCt.getHeaderAtIndex(0).setText(this.proxy.reader.jsonData.itinerario);
				              el.cod_iti = this.proxy.reader.jsonData.cod_iti;

				              Ext.getBody().unmask();
				            }
				         },													    
			                                          
                proxy: {
                    type: 'ajax',                    
                    url: proxy_url,
                    timeout: 240000,
                    //timeout: 120,
                    reader: {
                        root: 'children'
                    },
                	extraParams: {
                    	sped_ar: sped_ar.join(),
    		    		ordina_per_tipo_ord: ''
    				}
                }
            }),
        multiSelect: true,
        singleExpand: false,

		columns: colElencoOrdini,

        listeners: {
        	
		  itemcontextmenu : function(grid, rec, node, index, event) {
	          if (rec.get('liv') == 'liv_0')			  	
				showMenu_liv_0(grid, rec, node, index, event);
	          if (rec.get('liv') == 'liv_1')			  	
				showMenu_liv_1(grid, rec, node, index, event);
	          if (rec.get('liv') == 'liv_2')			  	
				showMenu_liv_2(grid, rec, node, index, event);										
	          if (rec.get('liv') == 'liv_3')			  	
				showMenu_liv_3(grid, rec, node, index, event);					
				
				},

		  celldblclick: {				  							
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				  
			  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
			  	rec = iView.getRecord(iRowEl);
                var grid = this;
			  	if(rec.get('liv') == 'liv_3' && col_name == 'importo'){

				  	 my_listeners = {
			  			afterOkSave: function(from_win){
			 		  		grid.getStore().load();
							from_win.close();  
			        		}
						};	

				  	 acs_show_win_std('Avanzamento pagamento ordine', 'acs_gestione_avanz_pagamento.php?fn=open_tab', {k_ordine : rec.get('k_ordine')}, 400, 150,  my_listeners, 'icon-button_black_repeat_dx-16');	

		        }


			  	if (rec.get('liv') == 'liv_3_SUCC'){
				  	//paginazione

				  	rec.store.treeStore.clearOnLoad = false;
				  	rec.store.treeStore.load({
					  		node: rec.parentNode,					  		
					  		params: {p_page: rec.get('prog')},
					  		callback: function(records, operation, success) {
					  	    }    					  		
					  	});
				  	rec.store.treeStore.clearOnLoad = true;
				  	rec.destroy();				  	

				  	return false;
			  	}

				if (rec.get('liv')=='liv_2' && col_name=='tipo' && rec.get('tipo') != ''){
					iEvent.preventDefault();
					show_win_art_reso(rec.get('k_cli_des'));
					return false;
				}
											
				if (col_name=='art_mancanti' && rec.get('liv')=='liv_3'){
					iEvent.preventDefault();						
					show_win_art_critici(rec.get('k_ordine'), 'MTO');
					return false;							
				}	
				if (col_name=='art_da_prog' && rec.get('liv')=='liv_3'){
					iEvent.preventDefault();												
					show_win_art_critici(rec.get('k_ordine'), 'MTS');
					return false;							
				}														
				if (col_name=='colli' && rec.get('liv')=='liv_3'){
					iEvent.preventDefault();						
					show_win_colli_ord(rec, rec.get('k_ordine'));
					return false;
				}
				if (col_name=='data_disp' && rec.get('liv')=='liv_3'){
					iEvent.preventDefault();		
				  	acs_show_win_std('Elenco fabbisogno materiali', 'acs_background_mrp_art.php?fn=fabbisogno_ordine', {k_ordine: rec.get('k_ordine')}, 1000, 550, null, 'icon-shopping_cart_gray-16');
					return false;
				}
				if (col_name=='riferimento' && rec.get('liv')=='liv_2'){
					iEvent.preventDefault();						
					gmapPopup("gmap.php?ind=" + rec.get('gmap_ind'));
					return false;						
				}
				if (rec.get('liv')=='liv_0' && iView.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'DAY'){			
					if (iView.getStore().treeStore.proxy.extraParams.ordina_per_tipo_ord != 'Y')						
						iView.getStore().treeStore.proxy.extraParams.ordina_per_tipo_ord = 'Y';
					else
						iView.getStore().treeStore.proxy.extraParams.ordina_per_tipo_ord = '';						
						
					iView.getStore().treeStore.load();	
				}

			   

				
			 }
			},										


            
            
        	
	        itemclick: function(view,rec,item,index,eventObj) {
		        
	          if (rec.get('liv') == 'liv_3')
	          {	   
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');	        	        	
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				   }
				});

				//cronologia
				if (wdc.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
	        		wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
					wdc.store.load();
				}				

				//allegati
				if (wdi.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
		        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
		        	if (wdi.isVisible())		        	
						wdi.store.load();
				}

				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}				
				
			   }
	         }
        },
		    
		viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {
		        	v_o = record.get('liv');			        	
		        	v = record.get('liv');
		        	
		        	if (v_o=="liv_1" && record.get('n_carico')==0)
		        		v = v + ' no_carico'; 
		        	
		        	v = v + ' ' + record.get('tp_pri');												        	
		        	
		        	if (v_o=="liv_3"){
		        		v = v + ' ' + record.get('raggr');												        		
		        	} //ordini

		        	
		           if (record.get('rec_stato') == 'Y') //barrato/rilasciato
			           	v = v + ' barrato';		        	
		        	
		           return v;																
		         }   
		    }												    
		    
		    
		    																		                
      })

return el;	
} //show el ordini



function show_win_disponibilita(dt, art, rec, tpno){
		acs_show_win_std('Loading.', 'acs_background_mrp_art.php', {dt: rec.get('ditta_origine'), rdart: rec.get('RDART')}, 1100, 600, null, 'icon-shopping_cart_gray-16');
}






function show_win_art_critici(ord, type){
		// create and show window
		var win = new Ext.Window({
		  width: 950
		, height: 500
		, minWidth: 600
		, minHeight: 400
		, plain: true
		, title: 'Segnalazione articoli critici'
		, layout: 'fit'
		, border: true
		, closable: true
		, items:  
			new Ext.grid.GridPanel({
				
			        listeners: {
			        	
							  celldblclick: {								
								  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){									  
								  	rec = iView.getRecord(iRowEl);								  	
								  	show_win_disponibilita(rec.get('ditta_origine'), rec.get('RDART'), rec, rec.get('RDTPNO'));
								  }
							  }	  
					},

				
					store: new Ext.data.Store({
						
						listeners: {
						            load: function () {
						                win.setTitle('Segnalazione articoli critici ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO + ' - Disponibilit&agrave; prevista: ' + this.proxy.reader.jsonData.ordine.TDDTDS);
						            }
						         },
						
						groupField: 'RDTPNO',			
						autoLoad:true,				        
	  					proxy: {
								url: 'acs_get_order_rows.php?type=' + type + '&nord=' + ord,
								type: 'ajax',
								reader: {
						            type: 'json',
						            root: 'root'
						        }
							},
		        			fields: [
		            			'REVOCHE', 'ditta_origine', 'TDDTEP', 'RDFMAN', 'RDDTDS', 'RDDFOR', 'RDDTEP', 'RDTPNO', 'RDOADO', 'RDONDO', 'RDRIFE', 'RDART', 'RDDART', 'RDDES1', 'RDDES2', 'RDUM', 'RDDT', {name: 'RDQTA', type: 'float'}
		        			]
		    			}),
		    		features: new Ext.create('Ext.grid.feature.Grouping',{
        							groupHeaderTpl: 'Tipo: {name}',
        							hideGroupedHeader: true
    						}),
    						
					viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           if (record.get('RDFMAN')=='M')
					           		return ' segnala_riga_rosso';
					           if (record.get('RDFMAN')=='C')
					           		return ' segnala_riga_giallo';				           		
					           if (record.get('RDDTDS')==record.get('TDDTEP'))
					           		return ' segnala_riga_viola';					           		
					           return '';																
					         }   
					    },    						
    						
			        columns: [
			             {
			                header   : 'Disp. Sped.',
			                dataIndex: 'RDDTDS', 
			                width     : 70,
			                renderer: date_from_AS
			             },			        
			             {
			                header   : 'Stato',
			                dataIndex: 'RDRIFE', 
			                width     : 95
			             }, {
			                header   : 'Fornitore',
			                dataIndex: 'RDDFOR', 
			                flex    : 50
			             }, {
			                header   : 'Descrizione',
			                dataIndex: 'RDDES1', 
			                flex    : 150,
			                renderer: function(value, p, record){
			                	return record.get('RDDES1') + " [" + record.get('RDART') + "]"
			    			}			                			                
			             }, {
			                header   : 'Um',
			                dataIndex: 'RDUM', 
			                width    : 30
			             }, {
			                header   : 'Q.t&agrave;',
			                dataIndex: 'RDQTA', 
			                width    : 50,
			                align: 'right', renderer: floatRenderer2
			             }, {
			                header   : 'Consegna',
			                dataIndex: 'RDDTEP', 
			                flex    : 30,
			                renderer: date_from_AS
			             }, {
			                header   : 'Documento',
			                dataIndex: 'RDDES2', 
			                flex    : 60
			             }
			         ],
			         listeners: {
			              afterrender: function(comp){
			                 //autocheck degli ordini che erano stati selezionati
		            	  		comp.store.on('load', function(store, records, options) {
			            	  		b_rev = comp.up('window').down('#b_revoche');
									if(records[0].data.REVOCHE > 0){
										revoche = records[0].data.REVOCHE;
										b_rev.setText( '<b>' + b_rev.text + ' ('+ revoche + ') </b>');
										 
									}
			            	  							
		    					}, comp);
		    			 }
			          }
 					 ,dockedItems: [{
		                    dock: 'bottom',
		                    xtype: 'toolbar',
		                    scale: 'large',
		                    items: [
		                     {
		                        xtype: 'button',
		                        text: 'Revoche MTO',
		                        itemId : 'b_revoche',
		                        style:'border: 1px solid gray;', 
		    		            scale: 'large',	               
		    		            width : 150,     
		    		            margin : '0 0 0 400',       
		    		          	handler: function() {
		    		          	    acs_show_win_std(null,
		      						  		'../base/acs_revoche_mto.php?fn=open_win', {
		      						  		k_ordine: ord
		  						  	}, null, null, null, null);
		    	       			}
					        }
		                    ]
		                    
		                    }]

			         })   

		});

		win.show();
}



function show_win_colli_ord(ord, k_ord){
		// create and show window
		var win = new Ext.Window({
		  width: 1024
		, height: 500
		, minWidth: 300
		, minHeight: 400
		, plain: true
		, title: 'Loading...'
		, layout: 'fit'
		, border: true
		, closable: true
		, items:  
			new Ext.grid.GridPanel({
					store: new Ext.data.Store({

						listeners: {
				            load: function () {
				                win.setTitle('Elenco corrente colli ordine ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO);
				                if(this.proxy.reader.jsonData.trad == 'Y')
										 win.setTitle( win.title + ' - Traduzione ' +this.proxy.reader.jsonData.lng);
						    }
				         },
					
												
						autoLoad:true,				        
	  					proxy: {
								url: 'acs_get_order_cols.php?k_ord=' + k_ord + '&trad=Y',
								type: 'ajax',
								reader: {
						            type: 'json',
						            root: 'root'
						        }
							},
		        			fields: [
		            			'PLAADO', 'PLNRDO', {name: 'PLCOAB', type: 'int'}, 'PLDART', 'PLUM', 'PLQTA', 'PLDTEN', 'PLTMEN', 'PLDTUS', 'PLTMUS', 'PLDTRM', 'PLTMRM'
		            			, 'PSDTSC', 'PSTMSC', 'PSRESU', 'PSCAUS', 'tooltip_scarico', 'PSPALM', 'PSLATI', 'PSLONG', 'PLART', 'PLRCOL','ALDAR1', 'TADESC', 'TDRFLO'
		        			]
		    			}),

		    			<?php $trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>"; ?>
			        columns: [			        			        
			            {
			                header   : 'Nr collo', align: 'right',
			                dataIndex: 'PLCOAB', 
			                flex     : 20
			             }, {
			                header   : 'Codice',
			                dataIndex: 'PLART', 
			                flex    : 20
				         },{
			                header   : 'Descrizione',
			                dataIndex: 'PLDART', 
			                flex    : 100
			             },{
			            	text: '&nbsp;<br><?php echo $trad; ?>',
			                dataIndex: 'ALDAR1',
			                tooltip: 'Traduzione',
			                menuDisabled: true,
			                width : 30,
			                renderer: function(value, metaData, record){
				    			  if(!Ext.isEmpty(record.get('ALDAR1')) || !Ext.isEmpty(record.get('TADESC'))){
										if (record.get('PLART').trim() == record.get('PLRCOL').trim())	    			    	
											metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('TADESC')) + '"';
					    			  	else
					    			  		metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('ALDAR1')) + '"';  			    
					    			  
					    			 return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=15>';
				    			  }else{
					    			 return '';
				    			  }
				    			  
				    		  }
			             }, {
			                header   : 'UM',
			                dataIndex: 'PLUM', 
			                flex    : 10
			             }, {
			                header   : 'Quantit&agrave;',
			                dataIndex: 'PLQTA', 
			                flex    : 30,
			                align: 'right'
			             }, {
			                header   : 'Disponibilit&agrave;',
			                dataIndex: 'PLDTEN', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTEN'), record.get('PLTMEN'))
			    			}
			             }, {
			                header   : 'Spedizione',
			                dataIndex: 'PLDTUS', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTUS'), record.get('PLTMUS'))
			    			}			                
			             }, {
			                header   : 'Aggiunte',
			                dataIndex: 'PLDTRM', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTRM'), record.get('PLTMRM'))
			    			}			                			                
			             }, {
				                header   : 'Scarico',
				                dataIndex: 'PSDTSC', 
				                flex    : 30,
				                renderer: function(value, metaData, record){
				                	metaData.tdAttr = 'data-qtip="Scanner: ' + Ext.String.htmlEncode(record.get('PSPALM')) + ' - Coordinate: ' + Ext.String.htmlEncode(record.get('PSLATI')) + ' - ' + Ext.String.htmlEncode(record.get('PSLONG')) + '"';
				                	return datetime_from_AS(record.get('PSDTSC'), record.get('PSTMSC'))
				    			}			                			                
				          }, {
				                header   : 'Info',
				                dataIndex: 'PSDTSC', 
				                width: 50,
				                renderer: function(value, metaData, record){

				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)
				                		metaData.tdAttr = 'data-qtip="Segnalazione allo scarico: ' + Ext.String.htmlEncode(record.get('PSCAUS')) + '"';
			                		
				                	if (Ext.isEmpty(record.get('PSRESU'))==false && record.get('PSRESU') == 'E')			    	
							    		return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)			    	
							    		return '<img src=<?php echo img_path("icone/48x48/comments.png") ?> width=18>';
				    			}			                			                
				          }

			        
			         ],
			         listeners: {
			        	 celldblclick: {
			        		  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				   		            
					           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
								  	rec = iView.getRecord(iRowEl);

								  	if(Ext.isEmpty(rec.get('TDRFLO'))){
										var lingua = 'lingua non definita';
										var cod_lingua = null;
									 }else{
										var lingua = rec.get('TDRFLO').trim();
										var cod_lingua = rec.get('TDRFLO').trim();
									}

									if(rec.get('PLART').trim() == rec.get('PLRCOL').trim())
										var gest_type = 'C';
									else
										var gest_type = 'A';

			    				    if(col_name == 'ALDAR1' && !Ext.isEmpty(cod_lingua))	
			    					   acs_show_win_std('Traduzione in ' + lingua, 
					    					'acs_gest_trad_colli_articoli.php?fn=open_tab', 
					    					{codice: rec.get('PLART'), desc : rec.get('PLDART'), lng: lingua, type : gest_type}, 400, 270,  {
				        					'afterSave': function(from_win){
				        						iView.getStore().load();
	 											from_win.close();
				        					}
				        				}, 'icon-globe-16');
			    				
				            	}

				        	 }

				         }




				         })   

		});

		win.show();
}






var dateMenu = Ext.create('Ext.menu.DatePicker', {
        handler: function (dp, date) {
            Ext.Msg.alert('Date Selected', 'You selected ' + Ext.Date.format(date, 'M j, Y'));
        }
    });





function showMenu_liv_0(grid, rec, node, index, event) {
   		event.stopEvent();
		var record = grid.getStore().getAt(index);	      
	    var voci_menu = [];
	      

	      <?php 
	    	//in base al profilo
	    	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	    	if ($js_parameters->gest_SPED != 0){
	       ?>
	      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	          grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HANGAR' ||
	          grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'GATE')  
		      voci_menu.push({
	          		text: 'Modifica spedizione',
	        		iconCls : 'iconSpedizione',	          		
	        		handler: function() {
	        			show_win_upd_spedizione(grid, rec, node, index, event);
	        		}
	    		});

	  	<?php } ?>	


	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' && 
	    	  grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){ 		
  		    voci_menu.push({
          		text: 'Report scarichi',
        		iconCls : 'iconPrint',          		
        		handler: function() {
        			show_win_elenco_stampa(grid, rec, node, index, event, grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco);
        		}
    		});
	      }

	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD'){	 
	 	      voci_menu.push({
	       		text: 'Report scarichi (hold)',
	     		iconCls : 'iconPrint',      		
	     		handler: function() {
	     			show_win_elenco_stampa_hold(grid, rec, node, index, event);
	     		}
	 		  });
	      }	  
	      
	  		

	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'){ 		
	  		    voci_menu.push({
	          		text: 'Report per itinerario',
	        		iconCls : 'iconPrint',          		
	        		handler: function() {
				
	        			acs_show_win_std('Scelta parametri', 'acs_get_elenco_stampa_form.php', {
		        					from: 'INFO',
		        					menu_type: menu_type,
		        					record: rec.data,
		        					filter: grid.initialConfig.store.proxy.extraParams
		        				}, 600, 400, null, 'icon-print-16'); 	        			
	        		}
	    		},{
	          		text: 'Rapporto ricezione',
	        		iconCls : 'iconPrint',          		
	        		handler: function() {

	        			acs_show_win_std('Conferma report', 'acs_report_info.php?fn=open_form', {
        					from: 'INFO',
        					menu_type: menu_type,
        					record: rec.data,
        					filter: grid.initialConfig.store.proxy.extraParams
        				},330, 100, null, 'icon-print-16'); 	   
				
     			
	        		}
	    		});
		      }			      

	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' &&
		     		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD' ){     
		   		  voci_menu.push({
		          		text: 'Report carico',
		         		iconCls : 'iconPrint',          		
		         		handler: function() {
		         			acs_show_win_std('Report carico', 'acs_report_carico.php?fn=get_json_form', {
			         				rec_id: rec.get('id'), 
			         				rec_liv: rec.get('liv'),
			         				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco
			         			}, null, null, null, 'iconPrint');          		
		         		}
		     		});
		      }
  		  

		 <?php if ($auth->is_operatore_aziendale() == 1){ ?>		 
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY')  
		      voci_menu.push({
	          		text: 'Composizione spedizioni',
	        		iconCls : 'iconPuzzle',	          		
	        		handler: function() {
	        			show_win_progetta_spedizioni(grid, rec, node, index, event);
	        		}
	    		});
  		  <?php } ?>


	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY')  
		      voci_menu.push({
	          		text: 'Sincronizza',
	        		iconCls : 'iconRefresh',	          		
	        		handler: function() {

	        	    	id_selected = grid.getSelectionModel().getSelection();	        			
				    	list_selected_id = [];
				    	for (var i=0; i<id_selected.length; i++)
				  		   list_selected_id.push(id_selected[i].data);
			
				  		if (list_selected_id.length > 1){
				  			acs_show_msg_error('Selezionare una singola spedizione');
				  			return false;			
				  		}

	        			
	       			 Ext.Ajax.request({
	      			   url: 'acs_op_exe.php?fn=sincronizza_spedizione',
	      			   method: 'POST',
	      			   jsonData: {sped_id: list_selected_id[0]['sped_id']}, 

	      			   success: function(response, opts) {
	      			   	grid.store.treeStore.load();
	      			   }
	      			});

	        			
	        		}
	    		});



	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' && 
	     		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){
	  		  voci_menu.push({
	        		text: 'Google maps',
	       		iconCls : 'icon-gmaps_logo-16',          		
	       		handler: function() {
	       			sped_id	= rec.get('sped_id');
	       			carico	= rec.get('k_carico');

	       			data = grid.getStore().treeStore.proxy.reader.jsonData.data;
	       			gmapPopup('gmap_sped.php?only_view=Y&data=' + data + '&sped_car=' + sped_id + '|' + carico + '&itin_id=' + grid.getStore().treeStore.proxy.reader.jsonData.cod_iti);
	           		
	       		} //handler google maps
	   		});         
	      }
		      
	      
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		}).showAt(event.xy);
}


function showMenu_liv_1(grid, rec, node, index, event) {
	     event.stopEvent();
		  
	      var record = grid.getStore().getAt(index);

	      var voci_menu = [];	

	  <?php 
	  	   	//in base al profilo
	     	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	  	   	if ($js_parameters->gest_CAR != 0){
	   ?>
	      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && record.data.n_carico > 0)	      
	      voci_menu.push({	      	
      		text: 'Modifica carico',
    		iconCls : 'iconCarico',      		
    		handler: function() {
    			show_win_upd_carico(grid, rec, node, index, event);
    		}
		  });

	  <?php if (strlen($js_parameters->ass_SPED) == 0 || $js_parameters->ass_SPED != 0){ ?>	      
		//2014-11-17: (per Ciesse): assegna spedizione anche su carico
		// (per spostare un intero carico in un'altra spedizione
		// - devo aver selezionato un'unica riga
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && record.data.n_carico > 0)		
			  voci_menu.push({
	      		text: 'Assegna prod./dispon. alla spedizione', //DA CARICO
	    		iconCls: 'iconSpedizione',
	    		handler: function() {

	    			id_selected = grid.getSelectionModel().getSelection();		
	    	    	if (id_selected.length > 1){
	    	    		acs_show_msg_error('Selezionare una singola riga carico.');
	    	    		return false;
	    	    	}
	    			
		    		
	    			show_win_assegna_data(grid, rec, node, index, event);
	    		}
			  });
		<?php } ?>


		  

	 <?php } ?>	  

     if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' &&
    		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){	 
	      voci_menu.push({
      		text: 'Report scarichi',
    		iconCls : 'iconPrint',      		
    		handler: function() {
    			show_win_elenco_stampa(grid, rec, node, index, event, grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco);
    		}
		  });
     }	  
     

     if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' &&
    		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD' ){     
  		  voci_menu.push({
         		text: 'Report carico',
        		iconCls : 'iconPrint',          		
        		handler: function() {
        			acs_show_win_std('Report carico', 'acs_report_carico.php?fn=get_json_form', {
            			rec_id: rec.get('id'), 
            			rec_liv: rec.get('liv'),
            			tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco
            		}, null, null, null, 'iconPrint');          		
        		}
    		});
     }


     //selezione ordini per gestione veloce data evasione programmata
     if ( (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
    		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD') &&
        	parseInt(rec.get('n_carico')) == 0){
	      voci_menu.push({
      		text: 'Seleziona ordini',
    		iconCls : 'icon-leaf-16',      		
    		handler: function() {
    			acs_show_win_std('Selezione ordini', 'acs_seleziona_ordini.php?fn=open_form', {
        				solo_senza_carico: 'Y',
        				grid_id: grid.id,
        				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco,
        				sped_id: rec.get('sped_id'), n_carico: 0, cod_iti: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti}, 
        				1024, 600, {}, 'icon-leaf-16');
    		}
		  });
     }	  
     
     <?php if ($auth->is_operatore_aziendale() == 1){ ?>
     //Report personalizzati el_ordini
     if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
    		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'LIS' ){     
 		  voci_menu.push({
        		text: 'Report Personalizzati',
       		iconCls : 'iconPrint',          		
       		handler: function() {
       			acs_show_win_std('Report personalizzati', '<?php echo ROOT_PATH; ?>personal/dove_report_personalizzati_el_ordini.php?fn=get_json_form', {
           				rec_id: rec.get('id'), 
           				rec_liv: rec.get('liv'), 
           				rec: rec.data,
           				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco}, null, null, null, 'iconPrint');          		
       		}
   		});
    }
    <?php } ?>
     
    

     if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' && 
    		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){
 		  voci_menu.push({
       		text: 'Google maps',
      		iconCls : 'icon-gmaps_logo-16',          		
      		handler: function() {

      			sped_id	= rec.get('sped_id');
      			carico	= rec.get('k_carico');
      			//recuper la data da id
      			data = rec.get('id').split('|')[2].split('_')[0];
      			gmapPopup('gmap_sped.php?only_view=Y&data=' + data + '&sped_car=' + sped_id + '|' + carico + '&itin_id=' + grid.getStore().treeStore.proxy.reader.jsonData.cod_iti);
          		
      		} //handler google maps
  		});         
     }

     
//20190725 - Stosa - Il verbale di carico (per spedizione) lo lancio solo dal grid
if (1 == 2){
     if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' && 
    		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD' &&
    		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'DAY'){
 		  voci_menu.push({
       		text: 'Verbale di carico',
      		iconCls : 'icon-pencil-16',          		
      		handler: function() {

      			carico	= rec.get('k_carico');
				carico_exp = carico.split("_")
      			
			    Ext.Msg.confirm('Richiesta conferma', 'Confermi generazione verbale di carico?', function(btn, text){
				    
				      if (btn == 'yes'){

				    	  wait_win = new Ext.Window({
							  width: 400
							, height: 500
							, plain: true
							, title: 'Elaborazione verbale di carico'
							, layout: 'vbox'
							, border: true
							, closable: false
							, modal: true
							, items: [
							   {				
								xtype:'image', height: 400,
								src: <?php echo img_path("elaborazione.gif") ?>
							   }, {
								   flex: 1, width: '100%', align: 'center',
								   html: '<center>Avvio procedura<br/><br/><b>Attendere prego</b></center>',												   
							   }
							]
						});	
						wait_win.show();					      
	
					        Ext.Ajax.request({
					        	url        : '../desk_firma/index.php?fn=open_doc',
					        	jsonData   : {form_values: {
						        					f_chiave: pad(carico_exp[0], 4) + pad(carico_exp[2], 6),
							        				f_user: <?php echo j($auth->get_user()); ?>,
									        		check_disabled: 'Y'								        					
					        					} 
						        },
					            method: 'POST',
					            success: function ( result, request) {
					                jsonData = Ext.decode(result.responseText);
					                if (jsonData.success == true) {
					                    //acs_show_msg_info('Attendere elaborazione Verbale di carico');
					                    wait_win.down('panel').update('<center>Richiesta inviata a gestionale<br/><br/><b>Attendere prego</b></center>');					                    

					                    //chiudo dopo 60 secondi
					                    setTimeout(function(){
						                    wait_win.close();
						                }, 60*1000);
					                    
					                } else {
						                wait_win.close();
										acs_show_msg_error('Errore: ' + jsonData.message);
					                }	
					                    
					            },
					            failure: function ( result, request) {
						            wait_win.close();
					            	acs_show_msg_error('Errore: ' + jsonData.message);
					            }
					        });

					       	
				      } else {
				        //nothing
				      }
				    });
      			
          		
      		} //handler google maps
  		});         
     }
}





    <?php if ($auth->is_operatore_aziendale() == 1){ ?>

    /*
     if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'GATE' && rec.get('n_carico') > 0)
	      voci_menu.push({	      	
     		text: 'Unisci carico su spedizione',
   			iconCls : 'iconCarico',      		
   			handler: function() {
   				acs_show_win_std('Unisci carico su spedizione', 'acs_unisci_carico_su_spedizione.php?fn=open_form', {
    				grid_parameters: grid.getStore().treeStore.proxy.reader.jsonData,
    				grid_id: grid.id,
    				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco,
    				data: grid.getStore().treeStore.proxy.reader.jsonData.data,
    				sped_id: rec.get('sped_id'), 
    				n_carico: rec.get('n_carico'),
    				k_carico: rec.get('k_carico'),
    				cod_iti: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti}, 
    				1024, 600, {
        				onSelected: function(from_win){
            				grid.getStore().treeStore.load();		
			        		from_win.close();						
	        			}
	        	}, 'icon-leaf-16');
   			} //handler
		 });
    */		 


		 



     
     //NUOVA VERSIONE CON GESTIONE TDNBOF -------------------------------------------------
     

     if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'GATE'){
     
             voci_menu.push({	      	
          		text: 'Unisci carico su Programma di Spedizione',
        			iconCls : 'iconPartenze',      		
        			handler: function() {
        
        				var ar_carichi_selected = [];
        			    var id_selected = grid.getSelectionModel().getSelection();		
        			 	if (id_selected.length <= 1){
        			 		acs_show_msg_error('Selezionare almeno due carichi');
        			 		return false;
        			 	}     
        
        				for (var i=0; i<id_selected.length; i++){
        					if (id_selected[i].get('liv') != 'liv_1') { //no livello carico
        						acs_show_msg_error('Selezionare due carichi');
        			 			return false;
        					}
        
        					if (id_selected[i].get('n_carico') == 0) { //no livello carico
        						acs_show_msg_error('Selezionare solo carichi assegnati');
        			 			return false;
        					}
        					ar_carichi_selected.push({
        						sped_id: id_selected[i].get('sped_id'),
        						k_carico: id_selected[i].get('k_carico')
        					});
        					
        				}
        
        				
        				acs_show_win_std('Unisci carico su programma di spedizione', 'acs_unisci_carico_su_prog_sped.php?fn=open_form_unisci', {
             				cod_iti: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti,
             				data: grid.getStore().treeStore.proxy.reader.jsonData.data,
             				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco, 
             				ar_carichi_selected: ar_carichi_selected,
             				grid_id: grid.id
             				}, 
             				900, 500, {
                 				onSelected: function(from_win){
                     				grid.getStore().treeStore.load();		
            			        		from_win.close();						
            	        			}
            	        	}, 'iconSpedizione');
        			} //handler
        		 });
        
        
             voci_menu.push({	      	
           		text: 'Ripristina carico su Programma di Produzione',
         			iconCls : 'icon-partenze_grey-16',      		
         			handler: function() {
        
         				var ar_carichi_selected = [];
         			    var id_selected = grid.getSelectionModel().getSelection();		
        
         				for (var i=0; i<id_selected.length; i++){
         					if (id_selected[i].get('liv') != 'liv_1') { //no livello carico
         						acs_show_msg_error('Selezionare solo carichi');
         			 			return false;
         					}
        
         					if (id_selected[i].get('n_carico') == 0) { //no livello carico
         						acs_show_msg_error('Selezionare solo carichi assegnati');
         			 			return false;
         					}
         					ar_carichi_selected.push({
         						sped_id: id_selected[i].get('sped_id'),
         						k_carico: id_selected[i].get('k_carico')
         					});
         					
         				}
        
         				
         				acs_show_win_std('Ripristina carico su programma di Produzione', 'acs_unisci_carico_su_prog_sped.php?fn=open_form_ripristina', {
         	 				ripristina: 'Y',
              				cod_iti: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti,
              				data: grid.getStore().treeStore.proxy.reader.jsonData.data,
              				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco, 
              				ar_carichi_selected: ar_carichi_selected,
              				grid_id: grid.id
              				}, 
              				600, 300, {
                  				onSelected: function(from_win){
                      				grid.getStore().treeStore.load();		
             			        		from_win.close();						
             	        			}
             	        	}, 'iconSpedizione');
         			} //handler
         		 });	 
     }
    


	<? } ?>

     
	      
	      
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		  }).showAt(event.xy);
}


function showMenu_liv_2(grid, rec, node, index, event) {
		  event.stopEvent();
	      var record = grid.getStore().getAt(index);		  
	      var voci_menu = [];


     		  <?php 
				  //in base al profilo
				  echo "is_operatore_aziendale = {$auth->is_operatore_aziendale()};";				  
			  ?>
	      
	      

	      //se ha selezionato un ordine evaso non permetto nessuna operazione
	    	id_selected = grid.getSelectionModel().getSelection();		
	    	num_evasi = 0;
	    	for (var i=0; i<id_selected.length; i++)
	  		   num_evasi += parseFloat(id_selected[i].data.fl_evaso);

	  		if (num_evasi > 0){

		  	      <?php if ($auth->is_operatore_aziendale() == 1){ ?>
			      //Report personalizzati el_ordini
			      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
			     		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'LIS' ){
			  		  voci_menu.push({
			         		text: 'Report Personalizzati',
			        		iconCls : 'iconPrint',          		
			        		handler: function() {
			        			acs_show_win_std('Report personalizzati', '<?php echo ROOT_PATH; ?>personal/dove_report_personalizzati_el_ordini.php?fn=get_json_form', {
				        				rec_id: rec.get('id'), 
				        				rec_liv: rec.get('liv'), 
				        				rec: rec.data,
				        				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco}, null, null, null, 'iconPrint');          		
			        		}
			    		});
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);			    		
			     }
			     <?php }  else { ?>
			    
		  			acs_show_msg_error('La selezione comprende ordini evasi. Nessuna funzione valida.');
			     <?php }?>
			  
	  			
	  			return false;			
	  		}	  		

	  	 <?php if (strlen($js_parameters->ass_SPED) == 0 || $js_parameters->ass_SPED != 0){ ?>		      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
		    	  grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD'){	      
			  voci_menu.push({
	      		text: 'Assegna prod./dispon. alla spedizione', //DA CLIENTE
	    		iconCls: 'iconSpedizione',
	    		handler: function() {
	    			show_win_assegna_data(grid, rec, node, index, event);
	    		}
			  });
		  voci_menu.push({
	      		text: 'Assegna prod./dispon. alla spedizione (con conferma data)', //DA CLIENTE
	    		iconCls: 'iconConf',
	    		handler: function() {
	    			show_win_assegna_data(grid, rec, node, index, event, 'Y');
	    		}
			  });
	      }		  
		<?php } ?>  


	  <?php 
		  	   	//in base al profilo
		     	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
		  	   	if ($js_parameters->gest_CAR != 0){
	  ?>	      
	      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY')	      
	      voci_menu.push({
      		text: 'Assegna nuovo numero carico',
    		iconCls : 'iconCarico',      		
    		handler: function() {
    			show_win_assegna_carico(grid, rec, node, index, event);
    		}
		  });

	  <?php } ?>	  


	  	 <?php if (strlen($js_parameters->ass_SPED) == 0 || $js_parameters->ass_SPED != 0){ ?>	  
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY')	      
		      voci_menu.push({
	      		text: 'Assegna scarico / deposito',
	    		iconCls : 'iconScaricoIntermedio',      		
	    		handler: function() {
	    			show_win_assegna_scarico_intermedio(grid, rec, node, index, event);
	    		}
			  });
		  <?php } ?>
	      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && is_operatore_aziendale==1)	      
		      voci_menu.push({
	      		text: 'Assegna destinazione cliente',
	    		iconCls : 'iconDestinazioneFinale',      		
	    		handler: function() {
	    			show_win_assegna_destinazione_finale(grid, rec, node, index, event);
	    		}
			  });


	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && is_operatore_aziendale==1)	      
		      voci_menu.push({
	      		text: 'Assegna destinazione sosta tecnica',
	    		iconCls : 'icon-outbox-16',      		
	    		handler: function() {
	    			show_win_assegna_destinazione_sosta_tecnica(grid, rec, node, index, event);
	    		}
			  });		  
		      


	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && is_operatore_aziendale==1)	      
		      voci_menu.push({
	      		text: 'Segnalazioni per cliente/destinazione',
	    		iconCls : 'icon-blog_compose-16',      		
	    		handler: function() {
	    			acs_show_win_std('Gestione segnalazioni per cliente/destinazione', 
	    	    						'acs_form_json_note_cliente.php?fn=gest', 
	    	    						{k_cli_des: rec.get('k_cli_des'),
	    								 sped_id: rec.get('sped_id')}, 900, 450, null, 'icon-blog_compose-16');
	    		}
			  });		  


	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD'){	 
	 	      voci_menu.push({
	       		text: 'Report scarichi (hold)',
	     		iconCls : 'iconPrint',      		
	     		handler: function() {
	     			show_win_elenco_stampa_hold(grid, rec, node, index, event);
	     		}
	 		  });
	      }	  
	      


	      //selezione ordini per gestione veloce data evasione programmata
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD'){
	 	      voci_menu.push({
	       		text: 'Seleziona ordini',
	     		iconCls : 'icon-leaf-16',      		
	     		handler: function() {
	     			acs_show_win_std('Selezione ordini', 'acs_seleziona_ordini.php?fn=open_form', {
	     				    solo_senza_carico: 'Y',
	         				grid_id: grid.id,
	         				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco,
	         				sped_id: rec.get('sped_id'), n_carico: 0, cod_iti: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti,
	         				k_cli_des: rec.get('k_cli_des')
	         				}, 
	         				1024, 600, {}, 'icon-leaf-16');
	     		}
	 		  });
	      }	  
	      


	      


	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD')  
		      voci_menu.push({
	          		text: 'Sincronizza',
	        		iconCls : 'iconRefresh',	          		
	        		handler: function() {

	        	    	id_selected = grid.getSelectionModel().getSelection();	        			
				    	list_selected_id = [];
				    	for (var i=0; i<id_selected.length; i++)
				  		   list_selected_id.push(id_selected[i].data);
			
				  		if (list_selected_id.length > 1){
				  			acs_show_msg_error('Selezionare un singolo cliente');
				  			return false;			
				  		}
	        			
		       			 Ext.Ajax.request({
		      			   url: 'acs_op_exe.php?fn=sincronizza_hold',
		      			   method: 'POST',
		      			   jsonData: {
			      			   			cod_iti: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti, 
			      			   		  	k_cli_des: list_selected_id[0]['k_cli_des']
			      			}, 
	
		      			   success: function(response, opts) {
		      			   	grid.store.treeStore.load();
		      			   }
		      			});

	        			
	        		}
	    		});



	      <?php if ($auth->is_operatore_aziendale() == 1){ ?>
	      //Report personalizzati el_ordini
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	     		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'LIS' ){     
	  		  voci_menu.push({
	         		text: 'Report Personalizzati',
	        		iconCls : 'iconPrint',          		
	        		handler: function() {
	        			acs_show_win_std('Report personalizzati', '<?php echo ROOT_PATH; ?>personal/dove_report_personalizzati_el_ordini.php?fn=get_json_form', {
		        				rec_id: rec.get('id'), 
		        				rec_liv: rec.get('liv'), 
		        				rec: rec.data,
		        				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco}, null, null, null, 'iconPrint');          		
	        		}
	    		});
	     }
	     <?php } ?>

	     if ((grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' 
		     || grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO')
		     && is_operatore_aziendale==1){	      
            if(rec.get('scarico_intermedio') != '1'){
	         voci_menu.push({
	      		text: 'Mezzi assegnati',
	    		iconCls : 'iconSpedizione',      		
	    		handler: function() {
	    			acs_show_win_std('Mezzi assegnati cliente '+rec.get('cliente'), 
						'acs_mezzi_assegnati.php?fn=open_grid', 
						{k_cli_des: rec.get('k_cli_des')}, 600, 400, null, 'iconSpedizione');
	    		}
			  });
			}
	     }
	      
	      
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		}).showAt(event.xy);
}


function showMenu_liv_3(grid, rec, node, index, event) {
		  event.stopEvent();
	      var record = grid.getStore().getAt(index);
	      var voci_menu = [];



	      //info: eventuale totale
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'){
		    	var id_selected = grid.getSelectionModel().getSelection();
				var c_ord = 0, s_vol = 0, s_colli = 0;
				
		    	for (var i=0; i<id_selected.length; i++){

			       if (id_selected[i].get('liv') == 'liv_3'){
				       //il volume e' scritto con la virgola, lo converto in formato decimale (con .)
				       var m_vol = id_selected[i].get('volume').replace('.', '');
				       var m_vol = m_vol.replace(',', '.');
			    	   c_ord   	+= 1;
			    	   s_vol 	+= parseFloat(m_vol);
			    	   s_colli 	+= parseFloat(id_selected[i].get('colli'));
			       }
		    	}
		     voci_menu.push({ 
		    	iconCls : 'icon-leaf-16', 
		       	text: 'Selezionati: <b>' + c_ord + '</b> ord, vol: <b>' + addSeparatorsNF(s_vol, '.', ',', '.') + '</b>, colli: <b>' + s_colli + '</b>'
		     });
	      }
			      
	      

	      //se ha selezionato un ordine evaso non permetto nessuna operazione
	    	id_selected = grid.getSelectionModel().getSelection();		
	    	var num_evasi = 0, 
	    		num_not_trasportation = 0;
    		
	    	for (var i=0; i<id_selected.length; i++){
	  		   num_evasi += parseFloat(id_selected[i].data.fl_evaso);

	  		   //ToDo: assicurarsi che venga sempre passato TDSWPP
	  		   // e quindi togliere il controllo !Ext.isEmpty....
	  		   if (!Ext.isEmpty(id_selected[i].get('TDSWPP')) && id_selected[i].get('TDSWPP') != 'Y')
	  			 num_not_trasportation += 1;
	    	}


	    	//se ho selezionato ordini evasi o non del trasportation (TDSWPP != Y)
	  		if (num_evasi > 0 || num_not_trasportation > 0){

	  		var voci_menu = [];
		  		        
	  		  voci_menu.push({
	         		text: 'Righe ordine',
	        		iconCls : 'icon-folder_search-16',          		
	        		handler: function() {
		    		  	list_selected_id = grid.getSelectionModel().getSelection();
		    		  	rec = list_selected_id[0];	        			
	        			if (list_selected_id.length > 1){
	        				acs_show_msg_error('Selezionare un singolo valore');
	        				return false;			
	        			}
   
	        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {k_ordine: rec.get('k_ordine'), trad : 'Y'}, 900, 450, null, 'icon-folder_search-16');          		
	        		}
	    		});

             <? echo "is_operatore_aziendale = {$auth->is_operatore_aziendale()};";	?>
             
		  		if (is_operatore_aziendale==1 ){	  		  
    	    		voci_menu.push({
    		  			text: 'Inserimento nuovo stato/attivit&agrave;',
    		  		iconCls : 'icon-arrivi-16',          		
    		  		handler: function() {
    
    		  		id_selected = grid.getSelectionModel().getSelection();
    
    		  			list_selected_id = [];
    		  			for (var i=0; i<id_selected.length; i++) 
    			  			list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});	   
    
    		  		my_listeners_inserimento = {
    		  				afterInsertRecord: function(from_win){	
    		  					//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente
    		  					//rec.store.treeStore.load({node: rec.parentNode});		        						
    		  					from_win.close();
    		  		    		}
    		  				};	
    		  			
    		  		acs_show_win_std('Nuovo stato/attivit&agrave;', 
    		  			<?php echo j('acs_form_json_create_entry.php'); ?>, 
    		  			{	
    		  				list_selected_id: list_selected_id
    		  				}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
    		  								        		          		
    		  		}
    		  		});
		  		 }	

		  		 voci_menu.push({
		         		text: 'Visualizza attivit&agrave;',
		        		iconCls : 'icon-arrivi_gray-16',          		
		        		handler: function() {

			    		  	list_selected_id = grid.getSelectionModel().getSelection();
			    		  	rec = list_selected_id[0];	        			
		        			if (list_selected_id.length > 1){
		        				acs_show_msg_error('Selezionare un singolo valore');
		        				return false;			
		        			}

		        			show_win_entry_ordine(record.get('k_ordine'))
		        		}
		    		});


			
			  				    		

	  		 voci_menu.push({
	         		text: 'Altre voci menu <br> bloccate perch&egrave; <br> l\'ordine risulta evaso',
	        		iconCls : 'icon-warning_red-16',          		
	        		/*handler: function() {

		    	          		
	        		}*/
	    		});
				
			      
	          var menu = new Ext.menu.Menu({
	            items: voci_menu
		     }).showAt(event.xy);	


			     return false;		
	  		}	  //sara ordini evasi      
	      

		  <?php 
				  //in base al profilo				  
				  if ($js_parameters->sped_INFO == 1)
				  	echo "ut_sped_INFO = 1;";
				  else
				  	echo "ut_sped_INFO = 0;";
				  
				  	
				  if ($js_parameters->only_OE == 1 || $menu_type == 'ORD_VEN')
			  	    echo "ut_only_OE = 1;";
			  	  else
			  	    echo "ut_only_OE = 0;";

				  echo "is_operatore_aziendale = {$auth->is_operatore_aziendale()};";				  
			  ?>
	      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	    	  (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD' && rec.get('proposte') != 'P') ||
	    	  (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO' && ut_sped_INFO == 1 && parseFloat(rec.get('n_carico')) == 0 && rec.get('proposte') != 'P')){

	 	  	 <?php if (strlen($js_parameters->ass_SPED) == 0 || $js_parameters->ass_SPED != 0){ ?>	  
	 	  	 	  if (ut_only_OE != 1)	      
				  voci_menu.push({
		      		text: 'Assegna prod./dispon. alla spedizione', //DA ORDINE
		    		iconCls: 'iconSpedizione',
		    		handler: function() {

				    	  //in INFO faccio gestire un record alla volta,
				    	  // a meno che siano tutti hold e stesso itinerario
				    	  if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'){	    	  
						    	id_selected = grid.getSelectionModel().getSelection();

								num_hold = 0;
								num_con_carico = 0;
								list_itinerari = {};
						    	list_selected_id = [];
						    	for (var i=0; i<id_selected.length; i++){

							       if (id_selected[i].get('liv') != 'liv_3'){
							    	   acs_show_msg_error('Selezionare solo righe a livello ordine');
							  			return false;								       
							       }

							       //verifico gli hold
							       num_hold += parseInt(id_selected[i].get('fl_da_prog'))
							       list_itinerari[id_selected[i].get('cod_iti')] = 1;
						  		   list_selected_id.push(id_selected[i].data);

						  		   //verifico quelli con carico
						 		   k_carico_exp = id_selected[i].data.k_carico.split('_');	
								   if (k_carico_exp[2] > 0)
									   num_con_carico += 1;		  		   
						  		   
						    	} //per ogni ordine

								//2014-10-24: non verifico piu' che siano tutti hold, ma che non abbiano il carico
						  		//if (list_selected_id.length > 1 && (num_hold != list_selected_id.length || Object.keys(list_itinerari).length > 1)){
						  		if (list_selected_id.length > 1 && (num_con_carico > 0 || Object.keys(list_itinerari).length > 1)){
						  			acs_show_msg_error('Selezionare un singolo ordine<br>oppure solo ordini dello stesso itinerario senza carico assegnato');
						  			return false;			
						  		}
				    	  } //se INFO		

			    		
		    			show_win_assegna_data(grid, rec, node, index, event);
		    		}
				  });

	 	  	 	  if (ut_only_OE != 1)
				  voci_menu.push({
			      		text: 'Assegna prod./dispon. alla spedizione (con conferma data)', //DA ORDINE
			    		iconCls: 'iconConf',
			    		handler: function() {

					    	  //in INFO faccio gestire un record alla volta,
					    	  // a meno che siano tutti hold e stesso itinerario
					    	  if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'){	    	  
							    	id_selected = grid.getSelectionModel().getSelection();

									num_hold = 0;
									num_con_carico = 0;
									list_itinerari = {};
							    	list_selected_id = [];
							    	for (var i=0; i<id_selected.length; i++){

								       if (id_selected[i].get('liv') != 'liv_3'){
								    	   acs_show_msg_error('Selezionare solo righe a livello ordine');
								  			return false;								       
								       }

								       //verifico gli hold
								       num_hold += parseInt(id_selected[i].get('fl_da_prog'))
								       list_itinerari[id_selected[i].get('cod_iti')] = 1;
							  		   list_selected_id.push(id_selected[i].data);

							  		   //verifico quelli con carico
							 		   k_carico_exp = id_selected[i].data.k_carico.split('_');	
									   if (k_carico_exp[2] > 0)
										   num_con_carico += 1;		  		   
							  		   
							    	} //per ogni ordine

									//2014-10-24: non verifico piu' che siano tutti hold, ma che non abbiano il carico
							  		//if (list_selected_id.length > 1 && (num_hold != list_selected_id.length || Object.keys(list_itinerari).length > 1)){
							  		if (list_selected_id.length > 1 && (num_con_carico > 0 || Object.keys(list_itinerari).length > 1)){
							  			acs_show_msg_error('Selezionare un singolo ordine<br>oppure solo ordini dello stesso itinerario senza carico assegnato');
							  			return false;			
							  		}
					    	  } //se INFO		

				    		
			    			show_win_assegna_data(grid, rec, node, index, event, 'Y');
			    		}
					  });
				  
			<?php } ?>
		  }


	      if (
	    	      (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && is_operatore_aziendale==1) ||
		    	  (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO' && ut_sped_INFO == 1 && parseFloat(rec.get('n_carico')) == 0) && rec.get('proposte') != 'P'){
	    	  if (ut_only_OE != 1)	      
    		  voci_menu.push({
          		text: 'Attiva/Disattiva conferma data programmata',
        		iconCls : 'iconConf',            		
        		handler: function() {
    
    		  	id_selected = grid.getSelectionModel().getSelection();
    
    		  	list_selected_id = [];
    		  	for (var i=0; i<id_selected.length; i++) 
    			   list_selected_id.push(id_selected[i].data.k_ordine);
    
    
    	    	  //in INFO faccio gestire un record alla volta
    	    	  if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'){	    	  		
    			  		if (list_selected_id.length > 1){
    			  			acs_show_msg_error('Selezionare un singolo valore');
    			  			return false;			
    			  		}
    	    	  }		
    
    
    	    	  //non si puo' se ha scelto ordini non omogenei
    		    	id_selected = grid.getSelectionModel().getSelection();		
    		    	num_con_data = 0;
    		    	num_senza_data = 0;
    		    	for (var i=0; i<id_selected.length; i++){
    		  		   if (parseFloat(id_selected[i].data.cons_conf) > 0)
    			  		   num_con_data +=1;
    		  		   else
    		  			 	num_senza_data += 1;
    		    	}
    
    		  		if (num_con_data > 0 && num_senza_data >0){
    		  			acs_show_msg_error('Operazione non ammessa. La selezione comprende alcuni ordini con data confermata e alcuni ordini senza data confermata');
    		  			return false;			
    		  		}	      
    
    
    		  	//non si puo' se ho un ordine bloccato senza data
    		    	id_selected = grid.getSelectionModel().getSelection();		
    		    	for (var i=0; i<id_selected.length; i++){
    		  		   if (parseFloat(id_selected[i].data.cons_conf) == 0 && parseFloat(id_selected[i].data.fl_bloc) > 1){
    		  			 acs_show_msg_error('Operazione non ammessa. Non e\' possibile confermare la data su un ordine bloccato');
    		  			 return false;
    		  		   }
    		    	}
    
    
    
    		    //non si puo' se ho un ordine senza spedizione
    	    	id_selected = grid.getSelectionModel().getSelection();		
    	    	for (var i=0; i<id_selected.length; i++){
    	  		   if (id_selected[i].data.sped_id == "" || parseFloat(id_selected[i].data.sped_id) == 0){
    	  			  acs_show_msg_error('Operazione non ammessa. Non e\' possibile confermare la data su un ordine senza numero di spedizione');
    	  			  return false;
    	  		   }
    	    	}		    	
    
    	    	//devo aver selezionato solo righe a livello ordine	    	
    	    	id_selected = grid.getSelectionModel().getSelection();		
    	    	for (var i=0; i<id_selected.length; i++){
    			  if (id_selected[i].data.liv != 'liv_3') {
    				  acs_show_msg_error('Selezionare solo righe a livello ordine');
    				  return false;
    			  }
    	    	}			
    	    	
    		  	
    			 Ext.Ajax.request({
    			   url: 'acs_op_exe.php?fn=toggle_data_confermata',
    			   method: 'POST',
    			   jsonData: {list_selected_id: list_selected_id}, 
    
    			   success: function(response, opts) {
    				   				node_expanded = [];				   
    		  						for (var i=0; i<id_selected.length; i++){
    			  						node_liv1 = id_selected[i].parentNode; //.parentNode;
    
    			  						if (node_liv1 != null)
    			  						if (Ext.Array.contains(node_expanded, node_liv1.id) == false){
    		                                node_liv1.collapse();
    		                                node_liv1.data.loaded = false; //forzo aggiornamento
    		                                node_liv1.expand();
    		                                node_expanded = node_liv1.id; 			  						
    			  						}    
    	                                
    		  						} 
    			   	
    			   }
    			});
    
        		}
    		});
	      }
		  


		//Riallinea data programmazione confermata
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && ut_only_OE != 1){
	    	  //se tutte le date sono confermate e non corrispondono alla programmata
  		  		id_selected = grid.getSelectionModel().getSelection();
  		  		mostra_voce = true;
		    	for (var i=0; i<id_selected.length; i++){
			  		   if (parseInt(id_selected[i].data.cons_conf) == 0 || parseInt(id_selected[i].data.cons_conf) == parseInt(id_selected[i].data.cons_prog)){
			  			 mostra_voce = false;
			  		   }
			    	}

		    	if (mostra_voce){

		  		  voci_menu.push({
		        		text: 'Riallinea data programmazione confermata',
		      		iconCls : 'iconConf',            		
		      		handler: function() {
		    		  	id_selected = grid.getSelectionModel().getSelection();

		    		  	list_selected_id = [];
		    		  	for (var i=0; i<id_selected.length; i++) 
		    			   list_selected_id.push(id_selected[i].data.k_ordine);


			   			 Ext.Ajax.request({
			  			   url: 'acs_op_exe.php?fn=riallinea_data_confermata',
			  			   method: 'POST',
			  			   jsonData: {list_selected_id: list_selected_id}, 
	
			  			   success: function(response, opts) {
			  				   				node_expanded = [];				   
			  		  						for (var i=0; i<id_selected.length; i++){
			  			  						node_liv1 = id_selected[i].parentNode; //.parentNode;
	
			  			  						if (node_liv1 != null)
			  			  						if (Ext.Array.contains(node_expanded, node_liv1.id) == false){
			  		                                node_liv1.collapse();
			  		                                node_liv1.data.loaded = false; //forzo aggiornamento
			  		                                node_liv1.expand();
			  		                                node_expanded = node_liv1.id; 			  						
			  			  						}    
			  	                                
			  		  						} 
			  			   	
			  			   }
			  			});		    		  	
		    		  	
			      	}
		  		  });
		    	
			    	
			    	
		    	} //if mostra_voce
  		  		
	      }	      
		

	<?php if (strlen($js_parameters->ass_SPED) == 0 || $js_parameters->ass_SPED != 0){ ?>
	      if ((parseFloat(rec.get('data_conf_ord')) > 0 ||
	    	      rec.get('fl_art_manc') == 2 ||
	    	      rec.get('fl_art_manc') == 3 ||
	    	      rec.get('fl_art_manc') == 4 ||
	    	      rec.get('fl_art_manc') == 20 ||
	    	      (parseInt(rec.get('sped_id')) > 0 && parseInt(rec.get('cons_prog_stadio')) == 1) //se in data ICE, non HOLD 
	    	      ) && ut_only_OE != 1 && rec.get('proposte') != 'P')  //grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY'
	    	      	      
		      voci_menu.push({
	      		text: 'Causale riprogrammazione ordine',
	    		iconCls : 'iconRIPRO',      		
	    		handler: function() {

	    		  	id_selected = grid.getSelectionModel().getSelection();

	    		  	list_selected_id = [];
	    		  	for (var i=0; i<id_selected.length; i++) 
	    			   list_selected_id.push(id_selected[i].data.k_ordine);	    		
	    			
	    			//apro form per richiesta parametri
					var mw = new Ext.Window({
					  width: 600
					, height: 380
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Causali riprogrammazione ordine confermato'
					, iconCls: 'iconRIPRO'			
					, layout: 'fit'
					, border: true
					, closable: true								
					});				    			
	    			mw.show();			    			

					//carico la form dal json ricevuto da php
					Ext.Ajax.request({
					        url        : 'acs_form_json_RIPRO.php',
					        jsonData: {list_selected_id: list_selected_id},
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){
					            var jsonData = Ext.decode(result.responseText);
					            mw.add(jsonData.items);
					            mw.doLayout();				            
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	    		

	    			
	    		}
			  });	  
		    <?php } ?>  
	      


	      if (parseFloat(rec.get('data_conf_ord')) > 0 && is_operatore_aziendale==1 && ut_only_OE != 1 && rec.get('proposte') != 'P')  //grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY'
			  voci_menu.push({
		      		text: 'Sollecito riprogrammazione',
		    		iconCls: 'iconArtCritici',
		    		handler: function() {

		    		  	id_selected = grid.getSelectionModel().getSelection();

		    		  	list_selected_id = [];
		    		  	for (var i=0; i<id_selected.length; i++) 
		    			   list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});	    		
	
		    			//apro form per richiesta parametri
						var mw = new Ext.Window({
						  width: 800
						, height: 380
						, minWidth: 300
						, minHeight: 300
						, plain: true
						, title: 'Sollecito riprogrammazione'
						, iconCls: 'iconArtCritici'			
						, layout: 'fit'
						, border: true
						, closable: true								
						});				    			
		    			mw.show();
		    			
						//carico la form dal json ricevuto da php
						Ext.Ajax.request({
						        url        : 'acs_form_json_create_entry.php',
						        jsonData: {tipo_op: 'RIPRO', list_selected_id: list_selected_id},
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
						            mw.add(jsonData.items);
						            mw.doLayout();				            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });				    			
						    	    							    			
		    		}
				  });
	      	

	      if (
	    	     (
	    	    	     grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	    	    	     grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD' ||
	    	    	     grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'
		    	  )
		    	 && is_operatore_aziendale==1 && ut_only_OE != 1 && rec.get('proposte') != 'P')	      
		      voci_menu.push({
	      		text: 'Assegna destinazione cliente',
	    		iconCls : 'iconDestinazioneFinale',      		
	    		handler: function() {
	    			show_win_assegna_destinazione_finale(grid, rec, node, index, event);
	    		}
			  });		  
			  


  		  id_selected = grid.getSelectionModel().getSelection();	      
  		  if (id_selected.length == 1)  		  
	      if (parseFloat(rec.get('data_conf_ord')) > 0  && parseFloat(rec.get('fl_evaso')) == 0 && is_operatore_aziendale==1 && rec.get('proposte') != 'P')
			  voci_menu.push({
		      		text: 'Forza invio conferma d\'ordine',
		    		iconCls: 'icon-print-16',
		    		handler: function() {

			   			 Ext.Ajax.request({
				  			   url: 'acs_op_exe.php?fn=forza_invio_conferma_ordine',
				  			   method: 'POST',
				  			   jsonData: {list_selected_id: id_selected[0].data.k_ordine}, 
		
				  			   success: function(response, opts) {
				 	  			  acs_show_msg_info('Richiesta forzatura invio');				  			   	
				  			   }
				  			});	

			    		
		    		} //hanler function
			  });
	      



	      //Righe ordine
	  		  voci_menu.push({
	         		text: 'Righe ordine',
	        		iconCls : 'icon-folder_search-16',          		
	        		handler: function() {
 						
		    		  	list_selected_id = grid.getSelectionModel().getSelection();
		    		
		    		  	rec = list_selected_id[0];	        			
	        			if (list_selected_id.length > 1){
	        				acs_show_msg_error('Selezionare un singolo valore');
	        				return false;			
	        			}

	        			if(grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO')
		        			var info = 'Y';
	        			else
	        				var info = 'N';

	        			//900, 450, null, 'icon-folder_search-16'
	        			acs_show_win_std(null, 'acs_get_order_rows_gest.php', {k_ordine: rec.get('k_ordine'), trad: 'Y', from_info : info});          		
	        		}
	    		});


  		  
	      //Report personalizzati el_ordini
	      if (is_operatore_aziendale==1 && ut_only_OE != 1 && (
	    	      grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	    		  grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'LIST')
		      ){     
	  		  voci_menu.push({
	         		text: 'Report Personalizzati',
	        		iconCls : 'iconPrint',          		
	        		handler: function() {
	        			acs_show_win_std('Report personalizzati', '<?php echo ROOT_PATH; ?>personal/dove_report_personalizzati_el_ordini.php?fn=get_json_form', {
		        				rec_id: rec.get('id'), 
		        				rec_liv: rec.get('liv'), 
		        				rec: rec.data,
		        				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco}, null, null, null, 'iconPrint');          		
	        		}
	    		});
	      }     
	      


			//Gestione indice rottura			
          	my_listeners = {
					onClose: function(from_win){	
						//dopo che ha chiuso la maschera di assegnazione indice rottura
						from_win.close();						
						grid.getStore().treeStore.load();			            
		        		}
    				};	
			
			
	      if (
	    	      (
	    	    	   grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	    	    	   grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'
		    	      ) && is_operatore_aziendale==1 && ut_only_OE != 1 && rec.get('proposte') != 'P'){

	    	  	id_selected = grid.getSelectionModel().getSelection();

	    	  	list_selected_id = [];
			      
			      voci_menu.push({
		      		text: 'Assegna indice di produzione',
		    		iconCls : 'icon-sticker_black-16',      		
		    		handler: function() {
	
	
			    	  	for (var i=0; i<id_selected.length; i++){
			    			  //devo prendere solo livelli ordine o cliente
			    			  if (id_selected[i].data.liv != 'liv_3'){
			    				  acs_show_msg_error('Selezionare solo righe a livello ordine');
			    				  return false;
			    			  }   	  	 
			    		 	list_selected_id.push(id_selected[i].data);
			    	  	}
			    		
		    			acs_show_win_std('Assegna indice di produzione', 
		    	    						'acs_form_json_assegna_indice_rottura.php?fn=gest', 
		    	    						{list_selected_id: list_selected_id}, 600, 500, my_listeners, 'icon-sticker_black-16');
		    		}
				  });
	      }	  
			  


<?php if ($cfg_mod_Spedizioni['INFO_blocca_ordine'] != 'N'){ ?>  		  

			//blocca ordine
	        if (
	    	      (	    	 
	    	    	   grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'
		    	      ) && is_operatore_aziendale==1 && ut_only_OE != 1 && rec.get('proposte') != 'P'){

	    	  	id_selected = grid.getSelectionModel().getSelection();
	    	  	list_selected_id = [];

			      voci_menu.push({
			      		text: 'Blocca ordine',
			    		iconCls : 'icon-power_black-16',      		
			    		handler: function() {		
			    			
				    	  	for (var i=0; i<id_selected.length; i++){
				    			  //devo prendere solo livelli ordine o cliente
				    			  if (id_selected[i].data.liv != 'liv_3'){
				    				  acs_show_msg_error('Selezionare solo righe a livello ordine');
				    				  return false;
				    			  }   	  	 
				    		 	list_selected_id.push(id_selected[i].get('k_ordine'));
				    	  	}

			    			acs_show_win_std('Blocca ordini', 
    	    						'acs_blocca_ordini.php?fn=open_form', 
    	    						{list_selected_id: list_selected_id}, 600, 200, {
    	    							onClose: function(from_win){	
    	    								//dopo che ha chiuso la maschera
    	    								from_win.close();						
    	    								grid.getStore().treeStore.load();			            
    	    				        		}
    	    		    				}, 'icon-power_black-16');
				    		
			    		}
					  });	    	  	
	    	  	
	        }  	
			
<?php } ?>

	      





<?php if ($cfg_mod_Spedizioni['INFO_sblocca_ordine'] == 'Y'){ ?>  		  

//sblocca ordine
if (
      (	    	 
    	   grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'
	      ) && is_operatore_aziendale==1 && ut_only_OE != 1 && rec.get('proposte') != 'P'){

  	id_selected = grid.getSelectionModel().getSelection();
  	list_selected_id = [];

      voci_menu.push({
      		text: 'Sblocca ordine',
    		iconCls : 'icon-power_black-16',      		
    		handler: function() {		
    			
	    	  	for (var i=0; i<id_selected.length; i++){
	    			  //devo prendere solo livelli ordine o cliente
	    			  if (id_selected[i].data.liv != 'liv_3'){
	    				  acs_show_msg_error('Selezionare solo righe a livello ordine');
	    				  return false;
	    			  }   	  	 
	    		 	list_selected_id.push(id_selected[i].get('k_ordine'));
	    	  	}

	    	  	if (list_selected_id.length > 1){
	    			  acs_show_msg_error('Operazione valida su singolo ordine');
    				  return false;		    	  	
	    	  	}

    			acs_show_win_std('Sblocca ordino', 
						'acs_form_json_avanzamento_entry.php', 
						{
							from_k_ordine: 'Y',
							causale: 'SBLOC',
							k_ordine: list_selected_id[0]
						}, 600, 200, {
							'close': function(from_win){
								grid.getStore().treeStore.load();
							},	
		    			}, 'icon-power_black-16');
	    		
    		}
		  });	    	  	
  	
}  	

<?php } ?>

if (   (
	    	   grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	    	   grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'
	      ) && is_operatore_aziendale==1 && ut_only_OE != 1 && rec.get('proposte') != 'P'){

	  	id_selected = grid.getSelectionModel().getSelection();

	  	list_selected_id = [];
	      
	      voci_menu.push({
  	      text: 'Abbina spedizione ordini',
	 	  iconCls : 'icon-link-16',      		
		  handler: function() {
               for (var i=0; i<id_selected.length; i++){
	    			  //devo prendere solo livelli ordine o cliente
	    			  if (id_selected[i].data.liv != 'liv_3'){
	    				  acs_show_msg_error('Selezionare solo righe a livello ordine');
	    				  return false;
	    			  }   	  	 
	    		 	list_selected_id.push(id_selected[i].data);
	    	  	}

               my_listeners = {
            		   afterAssegna: function(from_win){	
                  			//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente
                  			rec.store.treeStore.load({node: rec.parentNode});		        						
                  			from_win.close();
                      		}
                  		};	
	    		
			acs_show_win_std('Abbina spedizione ordini', 
	    						'acs_form_json_assegna_doc_abbi.php?fn=open_form', 
	    						{rec: rec.data}, 600, 300, my_listeners, 'icon-link-16');
		}
		  });
}	  
	
if (rec.get('proposte') != 'P' 
	&& is_operatore_aziendale==1) {

  //if (1==2)
    voci_menu.push({
    	text: 'Modifica ordine confermato',
    	iconCls : 'icon-design-16',          		
    	handler: function() {
    
            var id_selected = grid.getSelectionModel().getSelection(),            
            	list_selected_id = [];
        	
           	for (var i=0; i<id_selected.length; i++) 
            	list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});	   
            
            var my_listeners_inserimento = {
            		afterInsertRecord: function(from_win){	
            			//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente
            			//rec.store.treeStore.load({node: rec.parentNode});		        						
            			from_win.close();
                		}
            		};	
            	
            acs_show_win_std('Modifica ordine confermato', 
            	<?php echo j('acs_form_json_create_entry.php'); ?>, 
            	{	
            		tipo_op: 'VAROC',
            		list_selected_id: list_selected_id
            		}, 500, 500, my_listeners_inserimento, 'icon-design-16');	
    						        		          		
    	}
    });


    voci_menu.push({
    	text: 'Genera fattura proforma',
    	iconCls : 'icon-receipt-16',          		
    	handler: function() {
    
            var id_selected = grid.getSelectionModel().getSelection(),            
            	list_selected_id = [];
        	
           	for (var i=0; i<id_selected.length; i++) 
            	list_selected_id.push(id_selected[i].data);	 

           	acs_show_win_std('Parametri genera fattura proforma', 'acs_genera_proforma.php?fn=open_form', 
                        	 {list_selected_id: list_selected_id}, 400, 200,  {}, 'icon-receipt-16');	
        					        		          		
    	}
    });


    if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'
  	    && ut_only_OE != 1){

    	  	id_selected = grid.getSelectionModel().getSelection();
            list_selected_id = [];
  		      
  		      voci_menu.push({
  	      		text: 'Riferimenti booking',
  	    		iconCls : 'icon-calendar-16',      		
  	    		handler: function() {


  		    	  	for (var i=0; i<id_selected.length; i++){
  		    			  //devo prendere solo livelli ordine o cliente
  		    			  if (id_selected[i].data.liv != 'liv_3'){
  		    				  acs_show_msg_error('Selezionare solo righe a livello ordine');
  		    				  return false;
  		    			  }   	  	 
  		    	 	}


  		    	  	if(id_selected.length > 1 ){
                       acs_show_msg_error('Selezionare un singolo ordine');
      				   return false;
  	  		    	}
  		    		
    		    	  acs_show_win_std('Parametri elenco prenotazioni ritiro merce', 'acs_booking.php?fn=open_grid', {k_ordine : rec.get('k_ordine')}, 1300, 500, {}, 'icon-calendar-16');
  	    		}
  			  });
      }	  


    if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'
    	&& ut_only_OE == 1){

            var id_selected = grid.getSelectionModel().getSelection(),            
        		list_selected_id = [];
    	
       		for (var i=0; i<id_selected.length; i++) 
	        	list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});
  		      
  		      voci_menu.push({
  	      		text: 'Abbina contract',
  	    		iconCls : 'icon-blog-16',      		
  	    		handler: function() {

  	    			var cod_cli = '#######';
  	    			
  		    	  	for (var i=0; i<id_selected.length; i++){

						  var k_cli_des_exp = id_selected[i].get('k_cli_des').split('_');
						  if (cod_cli == '#######')
							  cod_cli = k_cli_des_exp[1];
						  else {
							  if (cod_cli != k_cli_des_exp[1]){
								  acs_show_msg_error('Selezionare solo ordini dello stesso cliente');
	  		    				  return false;
							  }
						  }
	  	  		    	  	
  		    			  //devo prendere solo livelli ordine o cliente
  		    			  if (id_selected[i].data.liv != 'liv_3'){
  		    				  acs_show_msg_error('Selezionare solo righe a livello ordine');
  		    				  return false;
  		    			  }   	  	 
  		    	 	}
  		    	  	
  		    	  	/*if(id_selected.length > 1 ){
                       acs_show_msg_error('Selezionare un singolo ordine');
      				   return false;
  	  		    	}*/

  	  		    	var my_listeners = {
  	    	  		   'onCloseSuccess': function(from_win){
  	  	    	  		   	from_win.destroy();
  	  	    	  			acs_show_msg_info('Ordine contract abbinato correttamente');
  	    	  		   }
  	  		    	};
  		    		
    		    	acs_show_win_std('Abbina contract', 'acs_abbina_contract.php?fn=open_form', {
    		    		list_selected_id : list_selected_id
        		    }, 700, 500, my_listeners, 'icon-blog-16');
  	    		}
  			  });


    		    voci_menu.push({
             		text: 'Abbina allestimento showroom',
            		iconCls : 'icon-button_grey_play-16',          		
            		handler: function() {
 
            			id_selected = grid.getSelectionModel().getSelection();
		        		list_selected_id = [];
    		  			for (var i=0; i<id_selected.length; i++){ 
    		  			 if (id_selected[i].data.liv != 'liv_3') {
							  acs_show_msg_error('Selezionare solo righe a livello ordine');
							  return false;
						  } 
    		  			
    			   			list_selected_id.push(id_selected[i].data.k_ordine);
						}

    	        		 var my_listeners = {
        		  			afterOkSave: function(from_win){
        		  			    grid.getTreeStore().load();
        						from_win.close();  
    			        		}
    	    				};
            		
            		
            			acs_show_win_std('Abbina allestimento showroom', 'acs_panel_expo.php?fn=open_assegna', {list_selected_id : list_selected_id}, 400, 200, my_listeners, 'icon-button_grey_play-16');          		
            		}
        		});
      }	




    
    voci_menu.push({
    	text: 'Inserimento nuovo stato/attivit&agrave;',
    	iconCls : 'icon-arrivi-16',          		
    	handler: function() {
    
            var id_selected = grid.getSelectionModel().getSelection(),            
            	list_selected_id = [];
        	
           	for (var i=0; i<id_selected.length; i++) 
            	list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});	   
            
            var my_listeners_inserimento = {
            		afterInsertRecord: function(from_win){	
            			//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente
            			//rec.store.treeStore.load({node: rec.parentNode});		        						
            			from_win.close();
                		}
            		};	
            	
            acs_show_win_std('Nuovo stato/attivit&agrave;', 
            	<?php echo j('acs_form_json_create_entry.php'); ?>, 
            	{	                	
            		list_selected_id: list_selected_id
            	}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
    						        		          		
    	}
    });
    
} //is operatore_aziendale

if(rec.get('proposte') != 'P'){
    voci_menu.push({
    		text: 'Visualizza attivit&agrave;',
    		iconCls : 'icon-arrivi_gray-16',          		
    		handler: function() {
    
    		  	list_selected_id = grid.getSelectionModel().getSelection();
    		  	rec = list_selected_id[0];	        			
    			if (list_selected_id.length > 1){
    				acs_show_msg_error('Selezionare un singolo valore');
    				return false;			
    			}
    
    			show_win_entry_ordine(record.get('k_ordine'))
    		}
    	});
}

if ((grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO' ||
    grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD')
    && rec.get('proposte') == 'P'){

	 voci_menu.push({
	 		text: 'Conferma proposta',
	 		iconCls : 'icon-leaf-16',          		
	 		handler: function() {
	 
	 		  	list_selected_id = grid.getSelectionModel().getSelection();
	 		  	rec = list_selected_id[0];	        			
	 			if (list_selected_id.length > 1){
	 				acs_show_msg_error('Selezionare un singolo valore');
	 				return false;			
	 			}

	 			Ext.Msg.confirm('Richiesta conferma', 'Confermi generazione ordine cliente?' , function(btn, text){
    	   		if (btn == 'yes'){
    	 			Ext.Ajax.request({
    			        url: 'acs_proposte_ordine.php?fn=exe_proposta',
    			        jsonData: {
    			        	k_ordine : rec.get('k_ordine')						        	
    			        },
    			        method     : 'POST',
    			        waitMsg    : 'Data loading',
    			        success : function(result, request){	
    			            var jsonData = Ext.decode(result.responseText);
    			            acs_show_msg_info('Operazione completata');
    			           		
    			        },
    			        failure    : function(result, request){
    			            Ext.getBody().unmask();
    			            Ext.Msg.alert('Message', 'No data to be loaded');
    			        }
    			    });
        	   		}
	  	   		  });
	 
	 		}
	 	});
}

<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>

	/* voci_menu.push({
	 		text: 'Programma fabbisogno materiali',
	 		iconCls : 'icon-shopping_cart_gray-16',          		
	 		handler: function() {
	 			acs_show_win_std('Loading.', 'acs_background_mrp_art.php', {dt : <?php echo j($id_ditta_default); ?>, rdart : rec.get('articolo')},1000, 550, null, 'icon-shopping_cart_gray-16');
	 		}
	 	});*/
	
<?php } ?>

		/*
				voci_menu.push({
						text: 'Call Metron EXE',
					iconCls : 'icon-power_black-16',      		
					handler: function() {		
						allegatiPopup('../base/call_metron.php?k_ordine=' + rec.get('k_ordine'));		
					}
				  });
	    */				  







	      
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		}).showAt(event.xy);
}




function showMenu_albero_area_itin(grid, rec, node, index, event) {
		  event.stopEvent();
	      var record = grid.getStore().getAt(index);

	      var voci_menu = [];


   <?php 
	//in base al profilo
	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	if ($js_parameters->gest_SPED != 0){
   ?>



		//su liv_1: nuova spedizione con scelta itinerario
	      if (record.get('liv') == 'liv_1')	      
	      voci_menu.push(
	    		  {
		                text: 'Nuova spedizione',
		        		iconCls : 'iconSpedizione',
 		                
		                handler: function() {
				
						// create and show window
						print_w = new Ext.Window({
						  width: 300
						, height: 250
						, minWidth: 300
						, minHeight: 50
						, plain: true
						, title: 'Creazione nuova spedizione'
						, layout: 'fit'
						, border: true
						, closable: true
						//, items: formPanel					
						});
						print_w.show();
		                	
		                
		                	
						//carico la form dal json ricevuto da php
						Ext.Ajax.request({
						        url        : 'acs_form_json_spedizione.php?fn=select_itin',
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        jsonData: {create_new: 'Y',
						        		   from_area_itin: 'Y',
						        		   rec_id: rec.get('id'), 
						        		   rec_liv: rec.get('liv'),
						        		   tree_id: grid.id}, 
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
						            print_w.add(jsonData.items);
						            print_w.doLayout();				            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	                	
		                	
		                }
		            }	    	      
	    	      
	    	);		


   

	      if (record.get('liv') == 'liv_2')	      
	      voci_menu.push(
	    		  {
		                text: 'Nuova spedizione',
		        		iconCls : 'iconSpedizione',		                
		                handler: function() {
				
						// create and show window
						print_w = new Ext.Window({
						  width: 600
						, height: 550
						, minWidth: 300
						, minHeight: 300
						, plain: true
						, title: 'Creazione nuova spedizione'
						, layout: 'fit'
						, border: true
						, closable: true
						//, items: formPanel					
						});
						print_w.show();
		                	
		                
		                	
						//carico la form dal json ricevuto da php
						Ext.Ajax.request({
						        url        : 'acs_form_json_spedizione.php',
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        jsonData: {create_new: 'Y',
						        		   from_area_itin: 'Y',
						        		   rec_id: rec.get('id'), 
						        		   rec_liv: rec.get('liv'),
						        		   tree_id: grid.id,
						        		   itin_id: grid.initialConfig.grid.cod_iti}, 
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
						            print_w.add(jsonData.items);
						            print_w.doLayout();				            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	                	
		                	
		                }
		            }	    	      
	    	      
	    	);
	      

<?php } ?>



	<?php 
		//in base al profilo
		$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
		if ($js_parameters->gest_SPED != 0){
	?>	      

	      if (record.get('liv') == 'liv_3')	      
		      voci_menu.push(
		    		  {
			                text: 'Modifica spedizione',
			        		iconCls : 'iconSpedizione',			                
			                handler: function() {
					
							// create and show window
							print_w = new Ext.Window({
							  width: 600
							, height: 550
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Modifica spedizione'
							, layout: 'fit'
							, border: true
							, closable: true
							//, items: formPanel					
							});
							print_w.show();
			                	
	
							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_spedizione.php',
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        jsonData: {create_new: 'N',
							        		   from_area_itin: 'Y',
							        		   rec_id: rec.get('id'), 
							        		   rec_liv: rec.get('liv'),
							        		   tree_id: grid.id,
							        		   itin_id: grid.initialConfig.grid.cod_iti}, 
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            print_w.add(jsonData.items);
							            print_w.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	                	
			                	
			                }
			            }	    	      
		    	      
		    	);

	    <?php } ?>	
	      
	      
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		}).showAt(event.xy);
}

 
	function getCellClass (val, meta, rec, rowIndex, colIndex, store) {
//		idx = colIndex - 4; //colonna5 corrisponde a d_1

		//trovo la prima colonna per i giorni
			Ext.each(this.columns, function(col, index) {
                        if (col.dataIndex == 'd_1'){
                            idx = colIndex - index +1;
                        }                     
                    });		

		
		 
		if (rec.get('d_' + idx + '_f') == 'F' || rec.get('d_' + idx + '_f') == 'S' || rec.get('d_' + idx + '_f') == 'D'){
			meta.tdCls += ' festivo';
		}
		if (rec.get('d_' + idx + '_t') == '1'){
			meta.tdCls += ' con_carico_1';
		}		
		if (rec.get('d_' + idx + '_t') == '2'){
			meta.tdCls += ' con_carico_2';
		}
		if (rec.get('d_' + idx + '_t') == '3'){
			meta.tdCls += ' con_carico_3';
		}		
		if (rec.get('d_' + idx + '_t') == '4'){
			meta.tdCls += ' con_carico_4';
		}		
		if (rec.get('d_' + idx + '_t') == '5'){
			meta.tdCls += ' con_carico_5';
		}
		if (rec.get('d_' + idx + '_t') == '6'){
			meta.tdCls += ' con_carico_6';
		}				


		if (rec.get('liv') == 'liv_totale' && rec.get('id') == 'tot_PR'){
			meta.tdAttr += ' data-qtip="' + Ext.String.htmlEncode(rec.get('d_' + idx + '_qtip')) + '"';
		}

		if ((rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3') && rec.get('d_' + idx).length > 0 && rec.get('d_' + idx + '_d').length > 0)		
			return val += "<span class=c_disp>/" + rec.get('d_' + idx + '_d') + "</span>";
		else if (rec.get('liv') == 'liv_totale' && rec.get('d_' + idx).length > 0 && rec.get('d_' + idx + '_t').length > 0)
			return "<span style='" + rec.get('d_' + idx + '_t') + "'>" + val + "</span>";
		else
			return val;	
	}



	



    

    Ext.onReady(function() {

        Ext.QuickTips.init();


        Ext.Ajax.timeout = 240000;

		//per gestione stateful
        var memoryDays = new Date(new Date().getTime()+(1000*60*60*24*300)); //300 giorni
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider({expires: memoryDays}));
        


    Extensible.calendar.data.EventMappings = {
        // These are the same fields as defined in the standard EventRecord object but the
        // names and mappings have all been customized. Note that the name of each field
        // definition object (e.g., 'EventId') should NOT be changed for the default fields
        // as it is the key used to access the field data programmatically.
        EventId:     {name: 'EventId', mapping:'id', type:'int'}, // int by default
        CalendarId:  {name: 'CalendarId', mapping: 'cid', type: 'string'}, // int by default
        Title:       {name: 'Title', mapping: 'title'},
        StartDate:   {name: 'StartDate', mapping: 'start', type: 'date', dateFormat: 'c'},
        EndDate:     {name: 'EndDate', mapping: 'end', type: 'date', dateFormat: 'c'},
        RRule:       {name: 'RecurRule', mapping: 'rrule'},
        Location:    {name: 'Location', mapping: 'loc'},
        Notes:       {name: 'Notes', mapping: 'notes'},
        Url:         {name: 'Url', mapping: 'url'},
        IsAllDay:    {name: 'IsAllDay', mapping: 'ad', type: 'boolean'},
        Reminder:    {name: 'Reminder', mapping: 'rem'},        


        //Acs
        Risorsa: 	  {name:'Risorsa', mapping: 'risorsa', type: 'string'},
        Stabilimento: {name:'Stabilimento', mapping: 'stabilimento', type: 'string'},
        Trasp:		  {name:'Trasp', mapping: 'trasp', type: 'string'},
        Porta:    	  {name:'Porta', mapping: 'porta'},        
        
        
        // We can also add some new fields that do not exist in the standard EventRecord:
        CreatedBy:   {name: 'CreatedBy', mapping: 'created_by'},
        IsPrivate:   {name: 'Private', mapping:'private', type:'boolean'}
        
    };
    // Don't forget to reconfigure!
    Extensible.calendar.data.EventModel.reconfigure();






 /* PER DRAG & DROP **********/
    /*
     * Currently the calendar drop zone expects certain drag data to be present, so if dragging
     * from the grid, default the data as required for it to work
     */
    var nodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag1';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
        }
    };
    
    /*
     * Need to hook into the drop to provide a custom mapping between the existing grid
     * record being dropped and the new calendar record being added
     */
    var nodeDropInterceptor = function(n, dd, e, data){
        if(n && data){
            if(typeof(data.type) === 'undefined'){
                var rec = Ext.create('Extensible.calendar.data.EventModel'),
                    M = Extensible.calendar.data.EventMappings;
                
                // These are the fields passed from the grid's record
                //var gridData = data.selections[0].data;
                var gridData = data.records[0].data
                rec.data[M.Title.name] = 'salvataggio...';
                rec.data[M.CalendarId.name] = gridData.cid;
                
                // You need to provide whatever default date range logic might apply here:
                rec.data[M.StartDate.name] = n.date;
                rec.data[M.EndDate.name] = Extensible.Date.add(n.date, { hours: 1 });;
                rec.data[M.IsAllDay.name] = false;

                rec.data[M.CreatedBy.name] = gridData.TDNBOC;  //lo appoggio qui altrimenti non mi esegui il create               
                rec.data[M.Stabilimento.name] = gridData.TDSTAB;
                rec.data[M.Porta.name] = this.view.store.proxy.extraParams.f_porta;
                
                // save the evrnt and clean up the view
                ///////this.view.onEventDrop(rec, n.date);
                this.view.store.add(rec.data);                                                
                this.onCalendarDragComplete();
				this.view.fireEvent('eventadd');
                return false; //per non eseguire onDrop Normale
            }
        }
    };
    
    /*
     * Day/Week view require special logic when dragging over to create a 
     * drag shim sized to the event being dragged
     */
    var dayViewNodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag2';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
    
            var dayCol = e.getTarget('.ext-cal-day-col', 5, true);
            if (dayCol) {
                var box = {
                    height: Extensible.calendar.view.Day.prototype.hourHeight,
                    width: dayCol.getWidth(),
                    y: n.timeBox.y,
                    x: n.el.getLeft()
                }
                this.shim(n.date, box);
            }
        }
    };
    
    // Apply the interceptor functions to each class:
    var dropZoneProto = Extensible.calendar.dd.DropZone.prototype,
        dayDropZoneProto = Extensible.calendar.dd.DayDropZone.prototype 
    
    dropZoneProto.onNodeOver = Ext.Function.createInterceptor(dropZoneProto.onNodeOver, nodeOverInterceptor);
    dropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dropZoneProto.onNodeDrop, nodeDropInterceptor);
    
    dayDropZoneProto.onNodeOver = Ext.Function.createInterceptor(dayDropZoneProto.onNodeOver, dayViewNodeOverInterceptor);
    dayDropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dayDropZoneProto.onNodeDrop, nodeDropInterceptor);



    
    
    /*
     * This is a simple override required for dropping from outside the calendar. By default it
     * assumes that any drag originated within itelf, so a drag would be a move of an existing event.
     * This is no longer the case and it must check to see what the record state is.
     */
    Extensible.calendar.view.AbstractCalendar.override({
        onEventDrop : function(rec, dt){
        	//console.log('index.php - onEventDrop override');
            if (rec.phantom) {
                this.onEventAdd(null, rec);
            }
            else {
                this.moveEvent(rec, dt);
            }
        }
    });    	


	//disabilito il menu sugli eventi
	Extensible.calendar.menu.Event.override({
		showForEvent: function(rec, el, xy){
				return false;
		}
	});
    

	//formato data negli header
	Extensible.calendar.template.BoxLayout.prototype.singleDayDateFormat = 'l, j/m';	

    
    //PER RIMUOVERE L'ORARIO NEGLI EVENTI
    Extensible.calendar.view.DayBody.override({
    getTemplateEventData : function(evt){
        var M = Extensible.calendar.data.EventMappings,
            extraClasses = [this.getEventSelectorCls(evt[M.EventId.name])],
            data = {},
            colorCls = 'x-cal-default',
            title = evt[M.Title.name],
            fmt = Extensible.Date.use24HourTime ? 'G:i ' : 'g:ia ',
            recurring = evt[M.RRule.name] != '';
        
        this.getTemplateEventBox(evt);
        
        if(this.calendarStore && evt[M.CalendarId.name]){
            var rec = this.calendarStore.findRecord(Extensible.calendar.data.CalendarMappings.CalendarId.name, 
                    evt[M.CalendarId.name]);
                
            if(rec){
                colorCls = 'x-cal-' + rec.data[Extensible.calendar.data.CalendarMappings.ColorId.name];
            }
        }
        colorCls += (evt._renderAsAllDay ? '-ad' : '') + (Ext.isIE || Ext.isOpera ? '-x' : '');
        extraClasses.push(colorCls);
        
        if(this.getEventClass){
            var rec = this.getEventRecord(evt[M.EventId.name]),
                cls = this.getEventClass(rec, !!evt._renderAsAllDay, data, this.store);
            extraClasses.push(cls);
        }
        
        data._extraCls = extraClasses.join(' ');
        data._isRecurring = evt.Recurrence && evt.Recurrence != '';
        data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
        data.Title =  (!title || title.length == 0 ? this.defaultEventTitleText : title);
        return Ext.applyIf(data, evt);
    }    
    });
    
    


   //COMBO: possibilita di rimuovere valore selezionato (con forceSelection lasciava sempre l'ultimo)
   Ext.form.field.ComboBox.override({
    beforeBlur: function(){
        var value = this.getRawValue();
        if(value == ''){
            this.lastSelection = [];
        }
        this.doQueryTask.cancel();
        this.assertValue();
    }
   });
   
    




  //***********************************************************
  //PER RISOLVERE BUG CHROME VERSIONE 43
  //***********************************************************  
  // fix hide submenu (in chrome 43)
	Ext.override(Ext.menu.Menu, {
	    onMouseLeave: function(e) {
	    var me = this;
	
	    // BEGIN FIX
	    var visibleSubmenu = false;
	    me.items.each(function(item) { 
	        if(item.menu && item.menu.isVisible()) { 
	            visibleSubmenu = true;
	        }
	    })
	    if(visibleSubmenu) {
	        //console.log('apply fix hide submenu');
	        return;
	    }
	    // END FIX
	
	    me.deactivateActiveItem();
	
	    if (me.disabled) {
	        return;
	    }
	
	    me.fireEvent('mouseleave', me, e);
	    }
	});
    
    
    
    


  /* TEST PER PRINT */
  /*
    Ext.define("plugin.Printer", {
	    statics: {
		    print: function(htmlElement,printAutomatically) {
		    var win = window.open('', 'Print Panel');
		     
		    win.document.open();
		    win.document.write(htmlElement.outerHTML);
		    win.document.close();
		     
		    if (printAutomatically){
		    win.print();
		    }
		     
		    if (this.closeAutomaticallyAfterPrint){
			    if(Ext.isIE){
		    		window.close();
		    	} else {
		    		win.close();
		    	}
		    }
		    }
	     
	    }
    });

 */




    Ext.define('ModelDataView', {
        extend: 'Ext.data.Model',
        fields: ['id', 'name', 'shortName', 'url']
    });
        			
    
	
    
    Ext.define('ModelOrdini', {
        extend: 'Ext.data.Model',
        fields: [
        			{name: 'task'}, {name: 'n_carico'}, {name: 'k_carico'}, {name: 'sped_id'}, {name: 'cod_iti'}, {name: 'dt_orig'},
        			{name: 'k_ordine'}, {name: 'TDSWPP'}, {name: 'prog'}, {name: 'n_O'}, {name: 'n_M'}, {name: 'n_P'}, {name: 'n_CAV'}, {name: 'n_A'}, {name: 'n_CV'},
        			{name: 'liv'}, {name: 'liv_cod'}, {name: 'n_stadio'}, {name: 'row_cls'},
					{name: 'cliente'}, {name: 'k_cli_des'}, {name: 'gmap_ind'}, {name: 'scarico_intermedio'}, {name: 'tooltip_mezzi'}, {name: 's_mezzi_red'},
					{name: 'nr'}, {name: 'rec_stato'}, {name: 'rec_stato_updated'},
					{name: 'volume'},{name: 'pallet'},{name: 'peso'},
					{name: 'volume_disp'},{name: 'pallet_disp'},{name: 'peso_disp'},
					{name: 'colli'}, {name : 'c_prod'}, {name: 'colli_disp'},{name: 'colli_sped'},{name: 'data_disp'},{name: 'data_sped'},{name: 'stato_sped'}, {name: 'data_cons_cli'}, {name: 'data_conf_ord'}, {name: 'fl_data_disp'},
					{name: 'riferimento'}, {name: 'articolo'},
					{name: 'tipo'},{name: 'raggr'},
					{name: 'cons_rich'},					
					{name: 'priorita'},{name: 'tp_pri'},
					{name: 'seq_carico'}, {name: 'contract'},{name: 'tooltip_seq_carico'}, {name: 'dirty_seq_carico'}, {name: 'tooltip_ripro'},
					{name: 'stato'}, {name: 'cod_pagamento'}, {name: 'des_pagamento'}, {name: 'preferenza'}, {name: 'preferenza_anag'}, {name: 'proposte'},
					{name: 'cons_conf'},{name: 'cons_prog'}, {name: 'cons_prog_stadio'},{name: 'ind_modif'},
					{name: 'data_reg'}, {name: 'data_scadenza'}, {name: 'data_emiss_OF'},
					{name: 'importo'}, {name: 'gg_rit'}, {name : 'i_paga'},
					{name: 'fl_evaso'}, {name: 'fl_bloc'}, {name: 'fl_cli_bloc'}, {name: 'fl_art_manc'}, {name: 'fl_da_prog'}, {name: 'art_da_prog'}, {name: 'fl_new'},
					{name: 'qtip_tipo'}, {name: 'qtip_stato'}, {name: 'qtip_priorita'}, {name: 'tooltip_acc_paga'}, {name : 'note'}
        ]
    });    

    //Model per albero itinerari
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task',     type: 'string'},
            {name: 'user',     type: 'string'},
            {name: 'duration', type: 'string'},
            {name: 'done',     type: 'boolean'},
            'liv', 'sped_id'
        ]
    });


    //Model per ricerca clienti
    Ext.define('CliSearch', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'cod',      type: 'string'},
            {name: 'descr',    type: 'string'}
        ]
    });
    
    
    
    Ext.define('RowCalendar', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task',     type: 'string'},
            {name: 'liv',      type: 'string'},            
            {name: 'flag1',    type: 'string'},            
            {name: 'user',     type: 'string'},
            {name: 'duration', type: 'string'},
            {name: 'done',     type: 'boolean'},
            {name: 'da_data',  type: 'float'}, {name: 'DP_val'},           
            {name: 'd_1'}, 	{name: 'd_1_f'}, 	{name: 'd_1_t'}, 	{name: 'd_1_d'},	{name: 'd_1_qtip'},
            {name: 'd_2'}, 	{name: 'd_2_f'}, 	{name: 'd_2_t'}, 	{name: 'd_2_d'},	{name: 'd_2_qtip'},
            {name: 'd_3'}, 	{name: 'd_3_f'}, 	{name: 'd_3_t'}, 	{name: 'd_3_d'},	{name: 'd_3_qtip'},
            {name: 'd_4'}, 	{name: 'd_4_f'}, 	{name: 'd_4_t'}, 	{name: 'd_4_d'},	{name: 'd_4_qtip'},
            {name: 'd_5'}, 	{name: 'd_5_f'}, 	{name: 'd_5_t'}, 	{name: 'd_5_d'},	{name: 'd_5_qtip'},
            {name: 'd_6'}, 	{name: 'd_6_f'}, 	{name: 'd_6_t'}, 	{name: 'd_6_d'},	{name: 'd_6_qtip'},
            {name: 'd_7'}, 	{name: 'd_7_f'}, 	{name: 'd_7_t'}, 	{name: 'd_7_d'},	{name: 'd_7_qtip'},
            {name: 'd_8'}, 	{name: 'd_8_f'}, 	{name: 'd_8_t'}, 	{name: 'd_8_d'},	{name: 'd_8_qtip'},
            {name: 'd_9'}, 	{name: 'd_9_f'}, 	{name: 'd_9_t'}, 	{name: 'd_9_d'},	{name: 'd_9_qtip'},
            {name: 'd_10'}, {name: 'd_10_f'}, 	{name: 'd_10_t'}, 	{name: 'd_10_d'},	{name: 'd_10_qtip'},
            {name: 'd_11'}, {name: 'd_11_f'}, 	{name: 'd_11_t'}, 	{name: 'd_11_d'},	{name: 'd_11_qtip'},
            {name: 'd_12'}, {name: 'd_12_f'}, 	{name: 'd_12_t'}, 	{name: 'd_12_d'},	{name: 'd_12_qtip'},
            {name: 'd_13'}, {name: 'd_13_f'}, 	{name: 'd_13_t'}, 	{name: 'd_13_d'},	{name: 'd_13_qtip'},
            {name: 'd_14'}, {name: 'd_14_f'}, 	{name: 'd_14_t'}, 	{name: 'd_14_d'},	{name: 'd_14_qtip'},
			{name: 'fl_bloc'}, {name: 'fl_cli_bloc'}, {name: 'fl_ord_bloc'}, {name: 'fl_art_mancanti'}, {name: 'fl_da_prog'}                                  
        ]
    });    

/*    
    var store_DELETEEEEEEEEE = Ext.create('Ext.data.TreeStore', {
        model: 'Task',
        proxy: {
            type: 'ajax',
            url: 'acs_get_itinerari_tree_json.php'
        },
        folderSort: false
    });
*/    
    


    var storecal = Ext.create('Ext.data.TreeStore', {
        model: 'RowCalendar',
        proxy: {
            type: 'ajax',
            url: 'acs_get_calendario_tree_json.php'
        },
        
        reader: new Ext.data.JsonReader(),
        
        folderSort: false,
        
		listeners: {
        	beforeload: function(store, options) {
        		Ext.getBody().mask('Loading... ', 'loading').show();
        	}
        }		        
        
    });
            
        
        
        
        	/* per tabella ordini */
		    Ext.define('StoreOrdini', {
		        extend: 'Ext.data.JsonStore',        	
                    autoLoad:true,                    
				    fields: [
				       {name: 'cliente'},				       
				       {name: 'nr'},
				       {name: 'volume'},
				       {name: 'colli'},
				       {name: 'riferimento'},
				       {name: 'tipo'},
				       {name: 'cons_rich'},
				       {name: 'priorita'},
				       {name: 'stato'},
				       {name: 'cons_conf'},
				       {name: 'flag1'},				       
				       {name: 'importo',  type: 'float'}				       
				    ],                                      
                    proxy: {
                        type: 'ajax',
                        reader: {
                            root: 'results'
                        }
                    }
                });                
        

        	/* per tabella ordini */
			var store1 = Ext.create('Ext.data.TreeStore', {
                    autoLoad: false,                    
				    model: 'ModelOrdini',                                      
                    proxy: {
                        type: 'ajax',
                        reader: {
                            root: 'children'
                        }
                    }
                });



		Ext.define("selectModel", {
		extend: "Ext.data.Model",
		fields: [
		{ name: "id", type: "string" },
		{ name: "text", type: "string" }
		]
		});
        




function show_plan(){
	mp = Ext.getCmp('m-panel');
	plan = Ext.getCmp('calendario');

	if (plan){
    	plan.show();
    	plan.getStore().load();		    	
	} else {
		mp.add(show_plan_panel()).show();
		
		// attacco gli eventi e tooltip
		if (Ext.get("cal_fl_solo_con_carico_assegnato") != null){		
	        Ext.get("cal_fl_solo_con_carico_assegnato").on('click', function(){
	        	acs_show_win_std('Filtri su ordini programmati', 'acs_form_json_plan_filtri.php', null, 500, 220, null, 'icon-filter-16');
	        });
	        Ext.QuickTips.register({target: "cal_fl_solo_con_carico_assegnato",	text: 'Attiva/disattiva filtri su ordini programmati'});	        
		}


		        
		if (Ext.get("cal_report_agenda_trasportatori_autisti") != null){		
	        Ext.get("cal_report_agenda_trasportatori_autisti").on('click', function(){

	        	//TEST PRINT
	        	/*
	        	var html = Ext.dom.Query.selectNode('#m-panel');
	        	plugin.Printer.print(html,true);
	        	
	        	myGrid = Ext.getCmp('calendario');
	        	Ext.ux.grid.Printer.print(myGrid);
	        	*/	        		        	
		        
	        	acs_show_win_std('Agenda impegno trasportatori/autisti', 'acs_report_agenda_trasportatori_autisti.php?fn=get_parametri_form', null, 400, 500, null, 'iconPrint')            
	        });
	        Ext.QuickTips.register({target: "cal_report_agenda_trasportatori_autisti",	text: 'Agenda trasportatori'});	        
		}



		if (Ext.get("cal_report_calendario_settimanale_spedizioni") != null){		
	        Ext.get("cal_report_calendario_settimanale_spedizioni").on('click', function(){
	        	acs_show_win_std('Agenda programmazione settimanale', 'acs_calendario_settimanale_spedizioni.php?fn=get_parametri_form', null, 400,510, null, 'iconPrint')            
	        });
	        Ext.QuickTips.register({target: "cal_report_calendario_settimanale_spedizioni",	text: 'Agenda programmazione settimanale'});	        
		}
		
		if (Ext.get("cal_report_riepilogo_spedizioni") != null){		
	        Ext.get("cal_report_riepilogo_spedizioni").on('click', function(){
	        	acs_show_win_std('Riepilogo piani di spedizione', 'acs_riepilogo_spedizioni.php?fn=get_parametri_form', null, 480,580, null, 'iconDelivery')            
	        });
	        Ext.QuickTips.register({target: "cal_report_riepilogo_spedizioni",	text: 'Riepilogo piani di spedizione'});	        
		}


		if (Ext.get("disp_colli") != null){		
	        Ext.get("disp_colli").on('click', function(){
	        	acs_show_win_std('Analisi disponibilit&agrave; colli', 'acs_disp_colli.php?fn=get_parametri_form', null, 600, 400, null, 'iconBox')            
	        });
	        Ext.QuickTips.register({target: "disp_colli",	text: 'Analisi disponibilit&agrave; colli'});	        
		}

		if (Ext.get("colli_cliente") != null){		
	        Ext.get("colli_cliente").on('click', function(){
	        	acs_show_win_std('Disponibilit&agrave; colli per cliente', 'acs_colli_cliente.php?fn=get_parametri_form', null, 600, 440, null, 'iconClienti')            
	        });
	        Ext.QuickTips.register({target: "colli_cliente",	text: 'Disponibilit&agrave; colli per cliente'});	        
		}

		if (Ext.get("sollecito_forniture") != null){		
	        Ext.get("sollecito_forniture").on('click', function(){
	        	acs_show_win_std('Sollecito forniture', 'acs_sollecito_forniture.php?fn=get_parametri_form', null, 610, 440, null, 'iconInfoGray')            
	        });
	        Ext.QuickTips.register({target: "sollecito_forniture",	text: 'Sollecito forniture'});	        
		}
		
		
		if (Ext.get("cal_avanzamento_produzione") != null){
	        Ext.get("cal_avanzamento_produzione").on('click', function(){
	        	mp = Ext.getCmp('m-panel');
	        	av_prod = Ext.getCmp('panel-avanzamento_produzione');
	
	        	if (av_prod){
	        		av_prod.store.reload();
	        		av_prod.show();		    	
	        	} else {
	
	        		//carico la form dal json ricevuto da php
	        		Ext.Ajax.request({
	        		        url        : 'acs_panel_avanzamento_produzione.php',
	        		        method     : 'GET',
	        		        waitMsg    : 'Data loading',
	        		        success : function(result, request){
	        		            var jsonData = Ext.decode(result.responseText);
	        		            mp.add(jsonData.items);
	        		            mp.doLayout();
	        		            mp.show();
	        		            av_prod = Ext.getCmp('panel-avanzamento_produzione').show();        		            
	        		        },
	        		        failure    : function(result, request){
	        		            Ext.Msg.alert('Message', 'No data to be loaded');
	        		        }
	        		    });
	
	        	}            
	        });
	        Ext.QuickTips.register({target: "cal_avanzamento_produzione",	text: 'Avanzamento carichi'});
		}
                                
        Ext.get("cal_next_1d").on('click', function(){
        	storecal.proxy.url= 'acs_get_calendario_tree_json.php?move=next_1d';
			storecal.load();
        	storecal.proxy.url= 'acs_get_calendario_tree_json.php';			
        });	        
        Ext.get("cal_next_1w").on('click', function(){
        	storecal.proxy.url= 'acs_get_calendario_tree_json.php?move=next_1w';
			storecal.load();
        	storecal.proxy.url= 'acs_get_calendario_tree_json.php';			
        });
        Ext.get("cal_prev_1d").on('click', function(){
        	storecal.proxy.url= 'acs_get_calendario_tree_json.php?move=prev_1d';
			storecal.load();
        	storecal.proxy.url= 'acs_get_calendario_tree_json.php';			
        });
        Ext.get("cal_prev_1w").on('click', function(){
        	storecal.proxy.url= 'acs_get_calendario_tree_json.php?move=prev_1w';
			storecal.load();
        	storecal.proxy.url= 'acs_get_calendario_tree_json.php';			
        });
        Ext.get("cal_reload").on('click', function(){
			storecal.load();			
        });        


    	//tooltip
        if (Ext.get("cal_prev_1w") != null)	Ext.QuickTips.register({target: "cal_prev_1w",	text: 'Settimana indietro'});
        if (Ext.get("cal_prev_1d") != null)	Ext.QuickTips.register({target: "cal_prev_1d",	text: 'Giorno indietro'});        
        if (Ext.get("cal_reload") != null)	Ext.QuickTips.register({target: "cal_reload",	text: 'Rigenerazione contenuti finestra corrente'});        
        if (Ext.get("cal_next_1d") != null)	Ext.QuickTips.register({target: "cal_next_1d",	text: 'Giorno avanti'});        
        if (Ext.get("cal_next_1w") != null)	Ext.QuickTips.register({target: "cal_next_1w",	text: 'Settimana avanti'});        
        
        
        
                                
		                	
	}	
}
		

function show_plan_panel(){
	
 	return Ext.create('Ext.tree.Panel', {
		id: 'calendario',
		selType: 'cellmodel',
		cls: 'supply_desk',
        title: 'Plan',
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        store: storecal,
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        stateful: true,
	    stateId: 'plan_panel',
	    stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ], 	

        <?php echo make_tab_closable(); ?>,

        tbar: new Ext.Toolbar({
            items:['<b>Programmazione evasione ordini per data di produzione/disponibilit&agrave; alla spedizione</b>', '->'
            	, {
            	    iconCls: 'icon-button_black_repeat-16', 
					tooltip: 'Ripristina larghezza standard colonne',
            		handler: function(event, toolEl, panel){
                    	var grid = this.up('panel');
                    	Ext.state.Manager.clear(grid.stateId);
                    	grid.getView().refresh();
                    	//var store = grid.getStore();
                    	//var columnsNew = grid.initialConfig.columns;
                    	//grid.reconfigure(store,columnsNew);                    	
		           }}
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ 
		           	this.up('panel').getStore().load();

		           	//chiudo il west-panel (indici) e azzero la variabile. Alla prossima apertura riaggiornera' gli indici
		           	wp = Ext.getCmp('west-panel');
		           	wp.collapse();
		           	wp.is_tree_loaded = false;

		           	//chiudo il dettaglio ordine
					dett_ord = Ext.getCmp('OrdPropertiesTab');		           	
		           	dett_ord.collapse();
		           	
		           }}

	           	<?php echo make_tbar_closable() ?>

				
			]
		
        }),        
        
		listeners: {
		  itemcontextmenu: function(view,record,item,index,e,eOpts){

                e.stopEvent();
                var voci_menu = [];  
                
				//recupero la cella
				var xPos = e.getXY()[0];
			    var cols = view.getGridColumns();
			    var colSelected = null;
			
			    for(var c in cols) {
			
			        var leftEdge = cols[c].getPosition()[0];
			        var rightEdge = cols[c].getSize().width + leftEdge;
			
			        if(xPos>=leftEdge && xPos<=rightEdge) {
			            colSelected = cols[c];
			        }
			    }	

			    <?php if(isset($abilita_mobile) && $abilita_mobile == 'Y'){ ?>

			    if(record.get('liv')=='liv_2' || record.get('liv')=='liv_3' || record.get('liv')=='liv_4'){
		
			     voci_menu.push({
		         		text: '<b>Apri</b>',
		        		iconCls : 'icon-folder_search-16',          		
		        		handler: function() {
		        			
		        			mp = Ext.getCmp('m-panel');
							mp.add(
								show_el_ordini(view, record, item, index, eOpts, 'acs_get_elenco_ordini_from_calendar_json.php?m_id=' + record.get('id') + '&da_data=' +record.get('da_data') + '&col_name=' + colSelected.dataIndex, Ext.id())
			            	).show();
		        			
		        		}
		    		});			
			 	}

			   <?php } ?>


				if (record.get('liv') == 'liv_totale' && record.get('id') == 'tot_CP' &&
						colSelected.dataIndex.substring(0,2) == 'd_'){  //tot_CP
					voci_menu.push(
							{
					      		text: 'Colli per linea produttiva',
					    		iconCls : 'icon-print-16',      		
					    		handler: function() {
					    			acs_show_win_std('Riepilogo colli per linea di produzione', 'acs_report_riepilogo_colli_linea_prod_params.php?fn=open_form', {da_data: record.get('da_data'), col_name: colSelected.dataIndex}, 600, 380, {}, 'icon-info_blue-16'); 
					    		}
						  }							
					);
				} 
			   

			    if (record.get('liv') != 'liv_4' && record.get('liv') != 'liv_totale'){
				               
			      voci_menu.push({
		      		text: 'Seleziona ordini',
		    		iconCls : 'icon-leaf-16',      		
		    		handler: function() {
		    			acs_show_win_std('Selezione ordini', 'acs_seleziona_ordini.php?fn=open_form', {
		        				solo_senza_carico: 'N',
		        				grid_id: view.id,
		        				tipo_elenco: 'DA_PLAN',
			        			da_data: record.get('da_data'),
			        			col_name: colSelected.dataIndex,
			        			record_liv: record.get('liv'),
			        			record_id:  record.get('id'),
		    					},		        				 
		        				1024, 600, {}, 'icon-leaf-16');
		    		}
			      }); 

			  
			     if(colSelected.dataIndex != 'task'){
			     voci_menu.push({
			      		text: 'Proposta ordini anticipabili',
			    		iconCls : 'icon-button_black_repeat-16',      		
			    		handler: function() {

			    			var rec = record;
				    		var col_name = colSelected.dataIndex;
				    		var col_name_exp = col_name.split('_');
				        	var t = js_date_from_AS(rec.get('da_data'));
				        	t.setDate(t.getDate() + parseInt(col_name_exp[1]) -1);
				        	var liv_exp = rec.get('id').split("|");;
				        	var m_data = Ext.Date.format(t, 'Ymd')		
				        	var m_itin = liv_exp[4];
				        	var m_area = liv_exp[2];

				        	acs_show_win_std(null, 'acs_ordini_anticipabili.php?fn=open_form', 
									{data : m_data, area : m_area, itin: m_itin});		

				        	/*acs_show_panel_std('acs_ordini_anticipabili.php?fn=open_tab', 'panel-ordini-anticipabili',{
        		            	data : m_data, 
        		            	area : m_area,
        		            	itin: m_itin
    		            	}, null, null);*/
				    		
			    		}
				      });
			     }

				  if(colSelected.dataIndex == 'fl_da_prog'){
				      voci_menu.push({
			      		text: 'Riepilogo hold',
			    		iconCls : 'icon-button_blue_pause-16',      		
			    		handler: function() {
			    			 window.open('acs_riepilogo_hold.php?liv_id=' + record.get('id')); 
			    		}
				       }); 
				  }
                  if(record.get('liv') == 'liv_2'){
		           voci_menu.push({
					    text: 'Selezione spedizioni',
			    		iconCls : 'icon-delivery-16',      		
			    		handler: function() {
							var rec = record;
				    		var col_name = colSelected.dataIndex;
				    		var col_name_exp = col_name.split('_');
				        	var t = js_date_from_AS(rec.get('da_data'));
				        	t.setDate(t.getDate() + parseInt(col_name_exp[1]) -1);
				        	var liv_exp = rec.get('id').split("|");;
				        	var m_data = Ext.Date.format(t, 'Ymd')		
				        	var m_itin = liv_exp[4];
				        	var m_area = liv_exp[2];
				        	//apro win per selezione spedizioni
	
			    			var aree_abilitate_a_filtro_sped = <? echo acs_je($cfg_mod_Spedizioni['aree_abilitate_a_filtro_sped']); ?>;
	
								var my_listeners = {
			        					onSelected: function(from_win, sped_ar){	
	
				    							mp = Ext.getCmp('m-panel');
				    							mp.add(
				    								show_el_ordini(this, rec, 0, 0, 0, 
						    							'acs_get_elenco_ordini_from_calendar_json.php?m_id=' + rec.get('id') + '&da_data=' +rec.get('da_data') + '&col_name=' + col_name, 
						    							Ext.id(),
						    							sped_ar
						    							)
				    			            	).show();			        						
	
									        	from_win.close();
				        						
							        		}
					    				};					        	
	
						        	acs_show_win_std('Selezione spedizioni', 'acs_form_json_filtra_spedizioni_itin_data.php?fn=open_form', 
													{itin: m_itin, data: m_data}, 
													1050, 450, my_listeners, 'icon-delivery-16');		
						        	
						    		return false;
			    		}


					  
				  }); 
                  }
			    }
			    //}


			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				}).showAt(e.xy);
                             

            },


			
				load: function(sender, node, records) {									
			        	var c = Ext.getCmp('calendario');
			        	
			        	Ext.getBody().unmask();
			        	
						if (c.store.proxy.reader.jsonData.toggle_fl_solo_con_carico_assegnato == 1)
							Ext.get('cal_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_attivo.png"); ?>;
						else
							Ext.get('cal_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_disattivo.png"); ?>;																		


						
							
						c.headerCt.getHeaderAtIndex(5).setText(c.store.proxy.reader.jsonData.columns.c1);						        	
						c.headerCt.getHeaderAtIndex(6).setText(c.store.proxy.reader.jsonData.columns.c2);									
						c.headerCt.getHeaderAtIndex(7).setText(c.store.proxy.reader.jsonData.columns.c3);						        	
						c.headerCt.getHeaderAtIndex(8).setText(c.store.proxy.reader.jsonData.columns.c4);									
						c.headerCt.getHeaderAtIndex(9).setText(c.store.proxy.reader.jsonData.columns.c5);									
						c.headerCt.getHeaderAtIndex(10).setText(c.store.proxy.reader.jsonData.columns.c6);									
						c.headerCt.getHeaderAtIndex(11).setText(c.store.proxy.reader.jsonData.columns.c7);									
						c.headerCt.getHeaderAtIndex(12).setText(c.store.proxy.reader.jsonData.columns.c8);						        	
						c.headerCt.getHeaderAtIndex(13).setText(c.store.proxy.reader.jsonData.columns.c9);									
						c.headerCt.getHeaderAtIndex(14).setText(c.store.proxy.reader.jsonData.columns.c10);						        	
						c.headerCt.getHeaderAtIndex(15).setText(c.store.proxy.reader.jsonData.columns.c11);									
						c.headerCt.getHeaderAtIndex(16).setText(c.store.proxy.reader.jsonData.columns.c12);									
						c.headerCt.getHeaderAtIndex(17).setText(c.store.proxy.reader.jsonData.columns.c13);									
						c.headerCt.getHeaderAtIndex(18).setText(c.store.proxy.reader.jsonData.columns.c14);
						
						//coloro il bordo in base allo stadio
						for (var i=5; i<=18; i++) {
							c.headerCt.getHeaderAtIndex(i).removeCls('stadioDay0');							
							c.headerCt.getHeaderAtIndex(i).removeCls('stadioDay1');
							c.headerCt.getHeaderAtIndex(i).removeCls('stadioDay2');
							c.headerCt.getHeaderAtIndex(i).removeCls('stadioDay3');							
						}

						c.headerCt.getHeaderAtIndex(5).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c1);						
						c.headerCt.getHeaderAtIndex(6).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c2);
						c.headerCt.getHeaderAtIndex(7).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c3);
						c.headerCt.getHeaderAtIndex(8).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c4);
						c.headerCt.getHeaderAtIndex(9).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c5);
						c.headerCt.getHeaderAtIndex(10).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c6);
						c.headerCt.getHeaderAtIndex(11).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c7);
						c.headerCt.getHeaderAtIndex(12).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c8);
						c.headerCt.getHeaderAtIndex(13).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c9);
						c.headerCt.getHeaderAtIndex(14).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c10);
						c.headerCt.getHeaderAtIndex(15).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c11);
						c.headerCt.getHeaderAtIndex(16).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c12);
						c.headerCt.getHeaderAtIndex(17).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c13);
						c.headerCt.getHeaderAtIndex(18).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c14);



						//tooltip con settimana e stadio
						
						for (var i=5; i<=18; i++) {						
							Ext.QuickTips.unregister(c.headerCt.getHeaderAtIndex(i).el);
							
						}								
						
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(5).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c1,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c1,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(6).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c2,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c2,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(7).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c3,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c3,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(8).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c4,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c4,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(9).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c5,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c5,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(10).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c6,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c6,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(11).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c7,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c7,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(12).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c8,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c8,
						});																														
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(13).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c9,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c9,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(14).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c10,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c10,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(15).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c11,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c11,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(16).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c12,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c12,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(17).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c13,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c13,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(18).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c14,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c14,
						});
						
						
				},

					
				celldblclick: {								

					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						
					  //fn: function(view,rec,item,index,eventObj) {
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
				    	
						
						if (col_name=='task' && rec.get('liv')=='liv_totale' && (
								rec.get('id') == 'tot_VOL' || 
								rec.get('id') == 'tot_COL' ||
								rec.get('id') == 'tot_PAL' ||
								rec.get('id') == 'tot_ORD' ||
								rec.get('id') == 'tot_CP'
								) ){										
							storecal.proxy.url= 'acs_get_calendario_tree_json.php?set_field=' + rec.get('id');
							storecal.load();
				        	storecal.proxy.url= 'acs_get_calendario_tree_json.php';										
						}
						if (rec.get('liv')=='liv_totale') return false;
						

						if (col_name!='task' && rec.get('liv')=='liv_totale'){										
							window.open ('acs_print_riepilogo_programmate.php?da_data=' +rec.get('da_data') + '&col_name=' + col_name,"mywindow"); 										
						}
												    	
				    	
if (col_name!='task' && (rec.get('liv')=='liv_2' || rec.get('liv')=='liv_3' || rec.get('liv')=='liv_4')){

				    		
				    		//prima verifico in base al numero di spedizioni se aprire la form per filtrarle (es: per l'estero)

				    		//al momento lo attivo solo su tasto dx
				   		if (1==2 && col_name.startsWith('d_')){
					    		
					    		var col_name_exp = col_name.split('_');
					        	var t = js_date_from_AS(rec.get('da_data'));
					        	t.setDate(t.getDate() + parseInt(col_name_exp[1]) -1);
					        	var liv_exp = rec.get('id').split("|");;
					        	var m_data = Ext.Date.format(t, 'Ymd')		
					        	var m_itin = liv_exp[4];
					        	var m_area = liv_exp[2];
					        	//apro win per selezione spedizioni

					        	console.log(m_area);
				    			var aree_abilitate_a_filtro_sped = <? echo acs_je($cfg_mod_Spedizioni['aree_abilitate_a_filtro_sped']); ?>;
				    			if (Ext.isEmpty(aree_abilitate_a_filtro_sped)== false && Ext.Array.contains(aree_abilitate_a_filtro_sped, m_area) == true){
									var my_listeners = {
				        					onSelected: function(from_win, sped_ar){	

					    							mp = Ext.getCmp('m-panel');
					    							mp.add(
					    								show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 
							    							'acs_get_elenco_ordini_from_calendar_json.php?m_id=' + rec.get('id') + '&da_data=' +rec.get('da_data') + '&col_name=' + col_name, 
							    							Ext.id(),
							    							sped_ar
							    							)
					    			            	).show();			        						

										        	from_win.close();
					        						
								        		}
						    				};					        	

							        	acs_show_win_std('Filtro spedizioni per itinerario/data', 'acs_form_json_filtra_spedizioni_itin_data.php?fn=open_form', 
														{itin: m_itin, data: m_data}, 
														800, 450, my_listeners, 'icon-outbox-16');		
							        	
							    		return false;
							    							    			
			  					} //se area_abilitata_a_filtro_sped    				    			

				    		} //if startsWith d_

			    		
					    								    							    									    	
							mp = Ext.getCmp('m-panel');
							mp.add(
								show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_from_calendar_json.php?m_id=' + rec.get('id') + '&da_data=' +rec.get('da_data') + '&col_name=' + col_name, Ext.id())
			            	).show();
						  return false; //previene expand/collapse di default
				    	}
				    	
				    }
				}						
		   },

		viewConfig: {
		        //row class CALENDARIO
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
			        	if (v=="liv_totale"){
			        		
				        	c = Ext.getCmp('calendario');						        						        	
							if ('tot_' + c.store.proxy.reader.jsonData.field_dett == record.get('id'))
				        		v = v + ' dett_selezionato';												        		
			        	} //la riga di totale selezionata					        	
		            return v;					            
		         	}   
		    },			


		<?php
				$ss = "";
				// $ss .= "<img src=" . img_path("icone/48x48/button_grey_repeat.png") . " height=30>";		
				$ss .= "<img id=\"cal_prev_1w\"src=" .  img_path("icone/48x48/button_grey_rew.png") . " height=30>";
				$ss .= "<img id=\"cal_prev_1d\"src=" .  img_path("icone/48x48/button_grey_first.png") . " height=30>";						
				$ss .= "<img id=\"cal_reload\" src=" .  img_path("icone/48x48/button_grey_repeat.png") . " height=30>";				
				$ss .= "<img id=\"cal_next_1d\" src=" . img_path("icone/48x48/button_grey_last.png") . " height=30>";		
				$ss .= "<img id=\"cal_next_1w\" src=" . img_path("icone/48x48/button_grey_ffw.png") . " height=30>";
				$ss .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				
				//FILTRO PER SOLO ORDINI CON CARICO ASSEGNATO
				$img_name = ($main_module->get_fl_solo_con_carico_assegnato() == 1) ? "filtro_attivo" : "filtro_disattivo";
				$ss .= "<span id=\"cal_fl_solo_con_carico_assegnato\" class=acs_button style=\"\">";							
				 $ss .= "<img id=\"cal_fl_solo_con_carico_assegnato_img\" src=" . img_path("icone/48x48/{$img_name}.png") . " height=30>";														
				 //$ss .="<span id=\"cal_fl_solo_con_carico_assegnato_lbl\">Solo con carico assegnato</span>";
				$ss .= "</span>";

				//APERTURA GRAFICO AGENDA TRASPORTATORI
				if ($auth->is_operatore_aziendale() == 1){
					$ss .= "&nbsp;&nbsp;&nbsp;";				
					$ss .= "<span id=\"cal_report_agenda_trasportatori_autisti\" class=acs_button style=\"\">";				
						$ss .= "<img id=\"cal_report_agenda_trasportatori_autisti_img\" src=" . img_path("icone/48x48/tras_plan.png") . " height=30>";
					$ss .= "</span>";
				}					

				
				//APERTURA CALENDARIO SETTIMANALE SPEDIZIONI
				if ($auth->is_operatore_aziendale() == 1){
					$ss .= "&nbsp;&nbsp;&nbsp;";
					$ss .= "<span id=\"cal_report_calendario_settimanale_spedizioni\" class=acs_button style=\"\">";
					$ss .= "<img id=\"cal_report_calendario_settimanale_spedizioni_img\" src=" . img_path("icone/48x48/print.png") . " height=30>";
					$ss .= "</span>";
				}
				
				//APERTURA  RIEPILOGO SPEDIZIONI
				if ($auth->is_operatore_aziendale() == 1){
					$ss .= "&nbsp;&nbsp;&nbsp;";
					$ss .= "<span id=\"cal_report_riepilogo_spedizioni\" class=acs_button style=\"\">";
					$ss .= "<img id=\"cal_report_riepilogo_spedizioni_img\" src=" . img_path("icone/48x48/delivery.png") . " height=30>";
					$ss .= "</span>";
				}				
				
				//APERTURA  RIEPILOGO SPEDIZIONI
				if ($auth->is_operatore_aziendale() == 1){
					$ss .= "&nbsp;&nbsp;&nbsp;";
					$ss .= "<span id=\"disp_colli\" class=acs_button style=\"\">";
					$ss .= "<img id=\"disp_colli_img\" src=" . img_path("icone/48x48/box_open.png") . " height=30>";
					$ss .= "</span>";
				}
				
				//APERTURA  DISPONIBILTą COLLI PER CLIENTE
				if ($auth->is_operatore_aziendale() == 1){
					$ss .= "&nbsp;&nbsp;&nbsp;";
					$ss .= "<span id=\"colli_cliente\" class=acs_button style=\"\">";
					$ss .= "<img id=\"colli_cliente_img\" src=" . img_path("icone/48x48/clienti.png") . " height=30>";
					$ss .= "</span>";
				}
				
				if ($auth->is_operatore_aziendale() == 1){
					$ss .= "&nbsp;&nbsp;&nbsp;";
					$ss .= "<span id=\"sollecito_forniture\" class=acs_button style=\"\">";
					$ss .= "<img id=\"sollecito_forniture_img\" src=" . img_path("icone/48x48/info_gray.png") . " height=30>";
					$ss .= "</span>";
				}
				
/*
 * ADESSO E' TRA I BOTTONI DELLA FASCETTA				
				//APERTURA Riepilogo avanzamento produzione
				$ss .= "&nbsp;&nbsp;&nbsp;";
				$ss .= "<span id=\"cal_avanzamento_produzione\" class=acs_button style=\"\">";
				$ss .= "<img id=\"cal_avanzamento_produzione_img\" src=" . img_path("icone/48x48/bandiera_scacchi.png") . " height=30>";
				$ss .= "</span>";
*/				
				
				
				//img per intestazioni colonne flag
				$cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=25>";
				$cf2 = "<img src=" . img_path("icone/48x48/power_black.png") . " height=25>";
				$cf3 = "<img src=" . img_path("icone/48x48/warning_black.png") . " height=25>";
				$cf4 = "<img src=" . img_path("icone/48x48/button_blue_pause.png") . " height=25>";																					
		?>		


        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 25,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: '<?php echo $ss; ?>'
        },{
			    text: '<?php echo $cf1; ?>',
			    width: 30, tooltip: 'Clienti bloccati',
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
		  },{
			    text: '<?php echo $cf2; ?>',
			    width: 30, tooltip: 'Ordini bloccati',
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_ord_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';}         			    
		  },{
			    text: '<?php echo $cf3; ?>',
			    width: 30, tooltip: 'Articoli mancanti',
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_art_mancanti')==1) return '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=18>';}         			    
		  },{
			    text: '<?php echo $cf4; ?>',
            dataIndex: 'fl_da_prog', tooltip: 'Ordini da programmare',         			    
			    width: 30,
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';}         			    
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_1',
		   align: 'right',
		   id: "col_cal_d_1",
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass,
//		   tooltip: 'abcde',
//		   tooltipType: 'title'
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_2',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_3',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_4',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_5',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_6',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_7',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		   
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_8',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_9',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_10',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_11',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_12',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_13',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		               		   
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_14',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		   
		  }        			          			  
        ]        
})

} //function show plan();



		








function show_el_arrivi(){
	mp = Ext.getCmp('m-panel');
	arrivi = Ext.getCmp('grid-arrivi');

	if (arrivi){
    	arrivi.store.reload();
    	arrivi.show();		    	
	} else {
/////		mp.add(show_el_arrivi_panel()).show();

    		Ext.Ajax.request({
    		        url        : 'acs_panel_todolist.php',
    		        method     : 'GET',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            mp.add(jsonData.items);
    		            mp.doLayout();
    		            mp.show();
    		            arrivi = Ext.getCmp('grid-arrivi');    		            
    		            arrivi.show();
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });



		if (Ext.get("todo_fl_solo_con_carico_assegnato") != null){		
	        Ext.get("todo_fl_solo_con_carico_assegnato").on('click', function(){
	        	acs_show_win_std('Imposta filtri', 'acs_form_json_todo_filtri.php', null, 500, 220, null, 'icon-filter-16');
	        });
	        Ext.QuickTips.register({target: "todo_fl_solo_con_carico_assegnato",	text: 'Attiva/Disattiva filtri su attivit&agrave; elencate'});	        
		}		
		
	}
}

















        // NOTE: This is an example showing simple state management. During development,
        // it is generally best to disable state management as dynamically-generated ids
        // can change across page loads, leading to unpredictable results.  The developer
        // should ensure that stable state ids are set for stateful components in real apps.
        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            // create instance immediately
            Ext.create('Ext.Component', {
                region: 'north',
                id: 'page-header',
                height: 110, // give north and south regions a height
                contentEl: 'header'
            }), {
                // lazily created panel (xtype:'panel' is default)
                region: 'south',
                contentEl: 'south',
                split: true,
                height: 300,
                minSize: 100,
                maxSize: 800,
                autoScroll:true,                
                collapsible: true,
                collapsed: true,
                title: 'Legenda',
                margins: '0 0 0 0'
            }, {
                xtype: 'tabpanel',
                region: 'east',
                title: 'Dettagli ordine',
                id: 'OrdPropertiesTab',
                animCollapse: true,
                collapsible: true,
                collapsed: true,
                split: true,
                width: 295, // give east and west regions a width
                minSize: 175,
                maxSize: 400,
                margins: '0 5 0 0',
                activeTab: 0,
                tabPosition: 'bottom',

        		imposta_title: function(tddocu){
        				this.setTitle(tddocu);
        			},
                
                items: [Ext.create('Ext.grid.PropertyGrid', {
                        title: 'Riferimenti',
                        id: 'OrdPropertyGrid',
                        closable: false,
                        source: {},
                        disableSelection: true,
                        listeners: {
    					    'beforeedit': {
            					fn: function () {
                					return false;
            						}
        						}
    					}
                    }),

                    new Ext.grid.GridPanel({
                        title: 'Cronologia',
                        id: 'OrdPropertyCronologia',
                  		 store: new Ext.data.Store({
                  									
                  				autoLoad: false,				        
                  	  			proxy: {
                  							url: 'acs_get_order_cronologia.php',
                  							type: 'ajax',
                  							reader: {
                  						      type: 'json',
                  						      root: 'root'
                  						     },
                  						     extraParams: {
                  		    		    		k_ordine: ''
                  		    				}               						        
   				        
                  						},
                  		        fields: ['data', 'utente', 'attivita', 'stato_data'],
                  		     	groupField: 'data',               		     	
                  			}),
       		    	features: new Ext.create('Ext.grid.feature.Grouping',{
   						groupHeaderTpl: 'Data: {[date_from_AS(values.name)]}',
   						hideGroupedHeader: false
   					}),               			
                     						    						
                        columns: [{
   			                header   : 'Utente',
   			                dataIndex: 'utente', 
   			                flex     : 3
   			             }, {
   				                header   : 'Attivit&agrave;',
   				                dataIndex: 'attivita', 
   				                width    : 70
   				         }, {
   			                header   : 'Stato / Evas. Prog.',
   			                dataIndex: 'stato_data', 
   			                flex     : 5
   			             }],

                        }),                      
                    



                    
                    new Ext.grid.GridPanel({
                     title: 'Allegati',
                     id: 'OrdPropertyGridImg',
					 <?php if ($cfg_mod_Spedizioni["visualizza_order_abilita_upload"] == 'Y') { ?>
                     tbar: new Ext.Toolbar({
        		            items:[
        		            	'->', '<b>Upload</b>', 
        		            {iconCls: 'icon-search-16', 
            		            handler: function(event, toolEl, panel){
                		            var k_ordine=this.up('grid').store.proxy.extraParams.k_ordine;
                		            var m_grid = this.up('grid');
            		            	acs_show_win_std('Upload', 'acs_upload.php?fn=open_form', {
            		            			from_visualizza_order_img: 'Y',
                		            		k_ordine: k_ordine
                		            	} , 600, 200, {
            		            		afterUpload: function(from_win){
            		            			m_grid.store.load();
            		            			from_win.close()
                		            	}
            		            	}, 'icon-search-16');

                		    }}
        		       		
        		         ]            
        		        }),
        		     <?php } ?>

                     
               		 store: new Ext.data.Store({
               									
               				autoLoad: false,				        
               	  			proxy: {
               							url: 'acs_get_order_images.php',
               							type: 'ajax',
               							reader: {
               						      type: 'json',
               						      root: 'root'
               						     },
               						     extraParams: {
               		    		    		k_ordine: ''
               		    				}               						        
				        
               						},
               		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
               		     	groupField: 'tipo_scheda',               		     	
               			}),
    		    	features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),               			
                  						    						
                     columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }],
		             //per la vitemper, se funziona metterlo parametrico
		             buttons :[

							{							
			                     xtype: 'button',		                
			                     text: 'Apri',
						         handler: function() {
						        	 var rec_selected = this.up('grid').getSelectionModel().getSelection()[0];
									 allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + rec_selected.get('IDOggetto'));				         							         	
						         }
							},

			             ],
			         listeners: {
				      activate: {
					      fn: function(e){
						   	if (this.store.proxy.extraParams.k_ordine != '')
							   	this.store.load();
					      }
				      },
					       
			   		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
						  }
			   		  }
			   		 
				      <?php if ($cfg_mod_Spedizioni["visualizza_order_abilita_delete"] == 'Y') { ?>
				       , itemcontextmenu : function(grid, rec, node, index, event) {							  
							event.stopEvent();

							var voci_menu = [];
							 voci_menu.push({
					         	   text: 'Elimina file',
					        	   iconCls : 'icon-sub_red_delete-16',          		
						           handler: function() {
            							Ext.Msg.confirm('Richiesta conferma', 'Confermi la cancellazione del file?', function(btn, text){																							    
            									if (btn == 'yes'){																	         	
            										 Ext.Ajax.request({
            								         url        : 'acs_upload.php?fn=exe_delete_file',
            								         method     : 'POST',
            					        			 jsonData: {
            					        				file_path: rec.get('IDOggetto'),
            					        				from_visualizza_order_img: 'Y'
            										 },							        
            								         success : function(result, request){
            								        	 grid.store.load();
            								     	 },
            								         failure    : function(result, request){
            								            Ext.Msg.alert('Message', 'No data to be loaded');
            								         }
            								    });				         	
            											         	
            									}
            								 });
						           }
							 });

							 var menu = new Ext.menu.Menu({
						            items: voci_menu
							       }).showAt(event.xy);	
							 
						}			
				      <?php } ?>
				      
			         }

                     })                      





<?php if (isset($cfg_mod_Spedizioni["allegati_root_C"])){ ?>
               , new Ext.grid.GridPanel({
                    title: 'File',
                    id: 'OrdPropertyGridFiles',
                    tbar: new Ext.Toolbar({
       		            items:[
       		            	'->', '<b>Upload</b>', 
       		            {iconCls: 'icon-search-16', 
           		            handler: function(event, toolEl, panel){
               		            k_ordine=this.up('grid').store.proxy.extraParams.k_ordine;
               		            var m_grid = this.up('grid');
           		            	acs_show_win_std('Upload', 'acs_upload.php?fn=open_form', {k_ordine: k_ordine} , 600, 200, {
           		            		afterUpload: function(from_win){
           		            			m_grid.store.load();
           		            			from_win.close()
               		            	}
           		            	}, 'icon-search-16');

               		            }}
       		       		
       		         ]            
       		        }),   
              		 store: new Ext.data.Store({
              			autoLoad: false,				        
              	  			proxy: {
              							url: 'acs_get_allegati.php',
              							type: 'ajax',
              							reader: {
              						      type: 'json',
              						      root: 'root'
              						     },
              						     extraParams: {
              		    		    		k_ordine: ''
              		    				}               						        
				        
              						},
              		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
              		     	groupField: 'tipo_scheda',               		     	
              			}),
   		    	features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),               			
                 						    						
                    columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }],

			         listeners: {
				      activate: {
					      fn: function(e){
						   	if (this.store.proxy.extraParams.k_ordine != '')
							   	this.store.load();
					      }
				      },
					       
			   		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('acs_get_allegati.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
						  }
			   		  },

	 			   		itemcontextmenu : function(grid, rec, node, index, event) {

							  
							event.stopEvent();							
							var grid = this;		             		
		    				//rows = grid.getSelectionModel().getSelection()[0].data.num_ord;		
							
		    				id_selected = grid.getSelectionModel().getSelection();
		    			  	list_selected_id = [];
		    			  	for (var i=0; i<id_selected.length; i++) 
		    				   list_selected_id.push({attachments: [id_selected[i].get('IDOggetto')]});
		    					  				
							var voci_menu = [];

							k_ord = this.getStore().proxy.extraParams.k_ordine;
							
			
					           voci_menu.push({
				         	   text: 'Cancella',
				        	   iconCls : 'icon-sub_red_delete-16',          		
					           handler: function() {
						        		     
              		    	 Ext.Msg.confirm('Richiesta conferma', 'Confermi la cancellazione degli allegati?', function(btn, text){																							    
      								   if (btn == 'yes'){																	         	
      									  
      								    	 Ext.Ajax.request({
      										        url        : '../base/file_utility.php?fn=exe_delete_file',
      										        method     : 'POST',
      							        			jsonData: {
      							        				k_ordine: k_ord,
      							        			    list_selected_id: list_selected_id
      												},							        
      										        success : function(result, request){
      							            			grid.getStore().load();	
      							            		 
      							            		},
      										        failure    : function(result, request){
      										            Ext.Msg.alert('Message', 'No data to be loaded');
      										        }
      										    });					         	
      						         	
      									}
      								   }); 
					        		    	 
								         }
					    		});


						      var menu = new Ext.menu.Menu({
						            items: voci_menu
							       }).showAt(event.xy);						    		
							  
					  }
			   		  
				         
			         }

                    })   
<?php } ?>
                





                    ]
            }




            <?php if ($js_parameters->p_solo_inter != 'Y'){ ?>            

            , {
                region: 'west',
                stateId: 'navigation-panel',
                id: 'west-panel', // see Ext.getCmp() below
                title: 'Indice spedizioni',
                split: true,
                width: 200,
                minWidth: 175,
                maxWidth: 400,
                collapsible: true,
                collapsed: true,
                animCollapse: true,
                margins: '0 0 0 5',
                layout: 'accordion',

                is_tree_loaded: false,

				listeners:{
					expand: function (p, eOpts ){

						if (this.is_tree_loaded == false){
							t = Ext.getCmp('itinerari_tree');
							t.getStore().load();
														
							t = Ext.getCmp('itinerari_tree_data');
							t.getStore().load();

							t = Ext.getCmp('itinerari_tree_trasp');
							t.getStore().load();							
							
							this.is_tree_loaded = true;
						}
				
					}
				}, scope: this,                        
                
                
                items: [

				Ext.create('Ext.tree.Panel', {
					id: 'itinerari_tree',		
			        title: 'Itinerario',
			        iconCls: 'icon-globe-16',			        
			        collapsible: true,
			        useArrows: true,
			        rootVisible: false,
			        root: {loaded: true}, //evita l'autoLoad
			        loadMask: true,		

			        //store: store,			        
			        store: Ext.create('Ext.data.TreeStore', {
					    	model: 'Task',				        
		                    autoLoad: true, //senbra non funzionare per treeStore                    
						                                          
		                    proxy: {
		                        type: 'ajax',
		                        url: 'acs_get_itinerari_tree_json.php',
								actionMethods: {read: 'POST'},
		                        
		                        reader: {
		                            root: 'children'
		                        }
		                    , doRequest: personalizza_extraParams_to_jsonData        				
		                    },

                    		listeners: {
                    			beforeload: function() {
                        			//console.log(this);
                    				//this.up('tree').getEl().mask("Loading", 'x-mask-loading');
                    			}
                    		}
		                }),			        
			        
			        multiSelect: true,
			        singleExpand: false,
					listeners: {

							afterrender: function (comp) {
						        this.getStore().on({
						            scope: this
						            , beforeload: function() {
						                this.getEl().mask("Loading", 'x-mask-loading');
						            }
						            , load: function() {
						                this.getEl().unmask();
						            }					            
						        });								
							},
								        											        	
						    itemcontextmenu : function(grid, rec, node, index, event) {
							         showMenu_albero_area_itin(grid, rec, node, index, event);
						    },
										        							
						
					        itemclick: function(view,rec,item,index,eventObj) {            		            
				            
					            if (rec.get('leaf')){
            							mp = Ext.getCmp('m-panel');
            							mp.remove('elenco_ordini');
            							mp.add(		
											show_el_ordini(view, rec, item, index, eventObj, 'acs_get_elenco_ordini_json.php?m_id=' + rec.get('id'), 'elenco_ordini')										            
						            	).show();
									  return false; //previene expand/collapse di default					            					            					        	            		            
							  }					             
						   }					             
					                      
					   },
			
			        //the 'columns' property is now 'headers'
			        columns: [{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            text: '',
			            flex: 2,
			            sortable: true,
			            dataIndex: 'task'
			        }
			        ]        
			      }),
                
				Ext.create('Ext.tree.Panel', {
					id: 'itinerari_tree_data',		
			        title: 'Data',
			        collapsible: true,
			        useArrows: true,
			        rootVisible: false,
			        root: {loaded: true}, //evita l'autoLoad			        
			        
			        store: Ext.create('Ext.data.TreeStore', {
					    	model: 'Task',				        
		                    autoLoad: true, //senbra non funzionare per treeStore                    
						                                          
		                    proxy: {
		                        type: 'ajax',
		                        url: 'acs_get_itinerari_per_data_tree_json.php',
								actionMethods: {read: 'POST'},
		                        
		                        reader: {
		                            root: 'children'
		                        }
		                    , doRequest: personalizza_extraParams_to_jsonData        				
		                    }
		                }),			        
			        

			        multiSelect: true,
			        singleExpand: false,
			        iconCls: 'icon-calendar-16',
			        
					viewConfig: {
			            plugins: {
			                ptype: 'treeviewdragdrop',
			                dragGroup: 'firstGridDDGroup',
			                dropGroup: 'secondGridDDGroup'
			            },
			            
			            listeners: {            					
        				}
			        },
			        
					listeners: {

							afterrender: function (comp) {
						        this.getStore().on({
						            scope: this
						            , beforeload: function() {
						                this.getEl().mask("Loading", 'x-mask-loading');
						            }
						            , load: function() {
						                this.getEl().unmask();
						            }					            
						        });								
							},
					
						
					        itemclick: function(view,rec,item,index,eventObj) {            		            
				            
					            if (rec.get('leaf')){
            							mp = Ext.getCmp('m-panel');
            							mp.remove('elenco_ordini_per_data');
            							mp.add(
											show_el_ordini(view, rec, item, index, eventObj, 'acs_get_elenco_ordini_json.php?tipo_cal=per_data&m_id=' + rec.get('id'), 'elenco_ordini_per_data')
						            	).show();
									  return false; //previene expand/collapse di default					            					            					        	            		            
							  }					             
						   }					             
					                      
					   },
			
			        //the 'columns' property is now 'headers'
			        columns: [{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            text: '',
			            flex: 2,
			            sortable: true,
			            dataIndex: 'task'
			        }
			        ]        
			      })
                


				, Ext.create('Ext.tree.Panel', {
					id: 'itinerari_tree_trasp',		
			        title: 'Trasportatori',
			        collapsible: true,
			        useArrows: true,
			        rootVisible: false,
			        root: {loaded: true}, //evita l'autoLoad			        

			        store: Ext.create('Ext.data.TreeStore', {
				    	model: 'Task',				        
	                    autoLoad: true, //senbra non funzionare per treeStore                    
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: 'acs_get_itinerari_per_trasp_tree_json.php',
							actionMethods: {read: 'POST'},
	                        
	                        reader: {
	                            root: 'children'
	                        }
	                    , doRequest: personalizza_extraParams_to_jsonData        				
	                    }
	                }),

			        
			        multiSelect: true,
			        singleExpand: false,
			        iconCls: 'iconSpedizione',
			        
					viewConfig: {
			            plugins: {
			                ptype: 'treeviewdragdrop',
			                dragGroup: 'firstGridDDGroup',
			                dropGroup: 'secondGridDDGroup'
			            },
			            
			            listeners: {
            					
        				}
			        },
			        
					listeners: {

							afterrender: function (comp) {
						        this.getStore().on({
						            scope: this
						            , beforeload: function() {
						                this.getEl().mask("Loading", 'x-mask-loading');
						            }
						            , load: function() {
						                this.getEl().unmask();
						            }					            
						        });								
							},
					
						
					        itemclick: function(view,rec,item,index,eventObj) {            		            
				            
					            if (rec.get('leaf')){
            							mp = Ext.getCmp('m-panel');
            							mp.remove('elenco_ordini_per_data');
            							mp.add(
											// show_el_ordini(view, rec, item, index, eventObj, 'acs_get_elenco_ordini_json.php?tipo_cal=per_data&m_id=' + rec.get('id'), 'elenco_ordini_per_data')
											show_el_ordini(view, rec, item, index, eventObj, 'acs_get_elenco_ordini_json.php?tipo_cal=per_sped&sped_id=' + rec.get('sped_id'), Ext.id())											
						            	).show();
									  return false; //previene expand/collapse di default					            					            					        	            		            
							  }					             
						   }					             
					                      
					   },
			
			        columns: [{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            text: '',
			            flex: 2,
			            sortable: true,
			            dataIndex: 'task'
			        }
			        ]        
			      })

	                
                
                ]
            }

			<?php } ?>




            
            // in this instance the TabPanel is not wrapped by another panel
            // since no title is needed, this Panel is added directly
            // as a Container
            , Ext.create('Ext.tab.Panel', {
            	id: 'm-panel',
        		cls: 'supply_desk_main_panel',            	
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 1,     // first tab initially active
                items: [
			                                   
                             					                                                
                ]
            })]
        });
        // get a reference to the HTML element with id "hideit" and add a click listener to it
        
        
        



        
        Ext.get("hideit").on('click', function(){
        	
        	var c = Ext.getCmp('calendario');
        	c.headerCt.getHeaderAtIndex(9).setText('abc');
        	
        	c.store.load();
        	
            // get a reference to the Panel that was created with id = 'west-panel'
            var w = Ext.getCmp('west-panel');
            // expand or collapse that Panel based on its collapsed property state
            w.collapsed ? w.expand() : w.collapse();
        });

        
        //--------------------------------------------------------------------------------        
        if (Ext.get("add-promemoria") != null){
        	Ext.QuickTips.register({
    			target: "add-promemoria",
    			title: 'Memo',
    			text: 'Attiva promemoria via email'
    		});
            
	        Ext.get("add-promemoria").on('click', function(){
	        	show_win_crt_promemoria();
	        });
        }            


        //--------------------------------------------------------------------------------
        if (Ext.get("bt-grafici") != null){
        	Ext.QuickTips.register({
    			target: "bt-grafici",
    			title: 'Analysis',
    			text: 'Grafici e report direzionali'
    		});
                    
	        Ext.get("bt-grafici").on('click', function(){
				acs_show_win_std('Grafici e report direzionali', 'acs_json_menu_statistiche.php', null, 900, 250, null, 'icon-grafici-16')	        	
	        });
        }            


        //--------------------------------------------------------------------------------
        if (Ext.get("bt-protocollazione") != null){
        	Ext.QuickTips.register({
    			target: "bt-protocollazione",
    			title: 'Heading',
    			text: 'Genera intestazione nuovo ordine'
    		});
                    
	        Ext.get("bt-protocollazione").on('click', function(){

	        	mp = Ext.getCmp('m-panel');
	        	av_prot = Ext.getCmp('panel-protocollazione');

	        	if (av_prot){
	        		//av_prot.store.reload();
	        		av_prot.show();		    	
	        	} else {

	        		//carico la form dal json ricevuto da php
	        		Ext.Ajax.request({
	        		        url        : 'acs_panel_protocollazione.php',
	        		        method     : 'GET',
	        		        waitMsg    : 'Data loading',
	        		        success : function(result, request){
	        		            var jsonData = Ext.decode(result.responseText);
	        		            mp.add(jsonData.items);
	        		            mp.doLayout();
	        		            mp.show();
	        		            av_prot = Ext.getCmp('panel-protocollazione').show();        		            
	        		        },
	        		        failure    : function(result, request){
	        		            Ext.Msg.alert('Message', 'No data to be loaded');
	        		        }
	        		    });

	        	}

	        	
	        });
        }        




        

//*********************
// PARTENZE (PLAN)
//*********************
        if (Ext.get("bt-partenze") != null){
        	Ext.QuickTips.register({
    			target: "bt-partenze",
    			title: 'Plan',
    			text: 'Programma di produzione/disponibilit&agrave; alla spedizione'
    		});            
	        Ext.get("bt-partenze").on('click', function(){             
				show_plan();
		    });
        }    



//*********************
// Flights (PLAN spedizioni)
//*********************
   if (Ext.get("bt-flights") != null){
	Ext.QuickTips.register({
        			target: "bt-flights",
         			title: 'Flight',
         			text: 'Programma di carico/spedizione'
    });            
    Ext.get("bt-flights").on('click', function(){             
    	mp = Ext.getCmp('m-panel');
    	flights = Ext.getCmp('panel-flights');

    	if (flights){
    		flights.store.reload();
    		flights.show();		    	
    	} else {
    		Ext.Ajax.request({
    		        url        : 'acs_panel_flights_plan.php',
    		        method     : 'GET',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            mp.add(jsonData.items);
    		            mp.doLayout();
    		            mp.show();
    		            flights = Ext.getCmp('panel-flights');    		            
    		            flights.show();
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });

    	}    		
    });
   }    
        


//*********************
// (GRID) Avanzamento produzione
//*********************
   if (Ext.get("bt-avanzamento_produzione") != null){
	Ext.QuickTips.register({
        			target: "bt-avanzamento_produzione",
         			title: 'Grid',
         			text: 'Controllo avanzamento disponibilit&agrave;/spedizione colli'
    });            
    Ext.get("bt-avanzamento_produzione").on('click', function(){             
    	mp = Ext.getCmp('m-panel');
    	acs_show_win_std('Controllo avanzamento disponibilit&agrave;/spedizione colli per data spedizione', 'acs_panel_avanzamento_produzione.php?fn=get_form_params', null, 530, 370, {}, 'icon-bandiera_scacchi-16');    		
    	  		
    });
   }    
   

   
        
//*********************
// ARRIVI
//*********************
        if (Ext.get("bt-arrivi") != null){
        	Ext.QuickTips.register({
    			target: "bt-arrivi",
    			title: 'To Do List',
    			text: 'Attivit&agrave; utente di acquisizione/avanzamento ordini'
    		});            
	        Ext.get("bt-arrivi").on('click', function(){             
	     		show_el_arrivi();
	     	});	    
        }





        
     //*********************
     // PRINT
     //*********************
        if (Ext.get("bt-print") != null){
         	Ext.QuickTips.register({
    			target: "bt-print",
    			title: 'Report',
    			text: 'Elenco/Agenda spedizioni e avanzamento colli'
    		});         
        
                    
	        Ext.get("bt-print").on('click', function(){
	        	
				print_w = new Ext.Window({
						  width: 600
						, height: 600
						, minWidth: 300
						, minHeight: 300
						, plain: true
						, title: 'Report settimanale'
						, layout: 'fit'
						, border: true
						, closable: true										
					});	
				print_w.show();
	
					//carico la form dal json ricevuto da php
					Ext.Ajax.request({
					        url        : 'acs_get_print_form.php',
					        method     : 'GET',
					        waitMsg    : 'Data loading',
					        success : function(result, request){
					            var jsonData = Ext.decode(result.responseText);
					            print_w.add(jsonData.items);
					            print_w.doLayout();
					            
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });			
	        });        
        }


     if (Ext.get("bt-report-art-mancanti-grid") != null){

     	Ext.QuickTips.register({
			target: "bt-report-art-mancanti-grid",
			title: 'Alert',
			text: 'Segnalazione articoli critici/mancanti'
		});    	 
    	 
        Ext.get("bt-report-art-mancanti-grid").on('click', function(){
        	
			print_w = new Ext.Window({
					  width: 600
					, height: 500
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Elenco segnalazioni su articoli critici/mancanti'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_form_json_articoli_mancanti_critici.php',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();
				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
        });        
        
     }   

        



     if (Ext.get("bt-espo") != null){

     	Ext.QuickTips.register({
			target: "bt-espo",
			title: 'Overflow',
			text: 'Clienti/Ordini bloccati, esposizione'
		});    	 
    	 
        Ext.get("bt-espo").on('click', function(){
        	
			print_w = new Ext.Window({
					  width: 950
					, height: 450
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Overflow'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_panel_espo.php?fn=open_form_filtri',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();
				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
        });        
        
     }   
     


     if (Ext.get("bt-order-entry") != null){

     	Ext.QuickTips.register({
			target: "bt-order-entry",
			title: 'Order Entry',
			text: 'Attivit&agrave; acquisizione/conferma ordini cliente'
		});    	 
    	 
        Ext.get("bt-order-entry").on('click', function(){
        	
			print_w = new Ext.Window({
					  width: 700
					, height: 430
					, minWidth: 300
					, minHeight: 350
					, plain: true
					, title: 'Order Entry - Attivit&agrave; acquisizione/conferma ordini cliente'
					, iconCls: 'icon-design-16'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_panel_order_entry.php?fn=open_form_filtri',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();
				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
        });        
        
     }

     if (Ext.get("bt-expo") != null){

       	Ext.QuickTips.register({
  			target: "bt-expo",
  			title: 'Showroom',
  			text: 'Attivitą di programmazione installazione mostre'
  		});    	 
      	 
          Ext.get("bt-expo").on('click', function(){
          	
  			print_w = new Ext.Window({
  					  width: 700
  					, height: 450
  					, minWidth: 300
  					, minHeight: 300
  					, plain: true
  					, title: 'Showroom - Attivitą di programmazione installazione mostre'
  					, iconCls: 'icon-module-16'
  					, layout: 'fit'
  					, border: true
  					, closable: true										
  				});	
  			print_w.show();

  				//carico la form dal json ricevuto da php
  				Ext.Ajax.request({
  				        url        : 'acs_panel_expo.php?fn=open_form_filtri',
  				        method     : 'GET',
  				        waitMsg    : 'Data loading',
  				        success : function(result, request){
  				            var jsonData = Ext.decode(result.responseText);
  				            print_w.add(jsonData.items);
  				            print_w.doLayout();
  				            
  				        },
  				        failure    : function(result, request){
  				            Ext.Msg.alert('Message', 'No data to be loaded');
  				        }
  				    });			
          });        
          
       }
     
        
     if (Ext.get("bt-booking") != null){

      	Ext.QuickTips.register({
			target: "bt-booking",
			title: 'Booking',
			text: 'Prenotazione ritiro merce'
		});         
         
        Ext.get("bt-booking").on('click', function(){
        	
			print_w = new Ext.Window({
					  width: 450
					, height: 350
					, minWidth: 350
					, minHeight: 210
					, plain: true
					, title: 'Parametri elenco prenotazioni ritiro merce'
					, layout: 'fit'
					, border: true
					, closable: true
					, iconCls: 'icon-calendar-16'									
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_booking.php?fn=open_form',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
        });        
     }



        
     if (Ext.get("bt-call-pgm") != null) {

       	Ext.QuickTips.register({
			target: "bt-call-pgm",
			title: 'Start',
			text: 'Avvio elaborazioni non interattive'
		});    	 

        Ext.get("bt-call-pgm").on('click', function(){
        	acs_show_win_std('Avvio processi batch', '../base/acs_processi_batch.php?fn=open_tab', 
    	        	{file_tabelle : <?php echo j($cfg_mod); ?>, menu_type : <?php echo j($menu_type); ?>}, 950, 500, null, 'icon-button_blue_play-16');
        });       	

     }   

        

     if (Ext.get("bt-prenotazione_carichi") != null) {

        	Ext.QuickTips.register({
 			target: "bt-prenotazione_carichi",
 			title: 'Upload',
 			text: 'Programma di carico trasportatori'
 		});    	 

         Ext.get("bt-prenotazione_carichi").on('click', function(){
      	   acs_show_win_std('Programma di carico trasportatori', 'acs_panel_prenotazione_carichi.php', null, 530, 420);	
         });       	

      }   




     if (Ext.get("bt-agenda_settimanale_spedizioni") != null) {

     	Ext.QuickTips.register({
			target: "bt-agenda_settimanale_spedizioni",
			title: 'Week',
			text: 'Agenda settimanale spedizioni'
		});    	 

      Ext.get("bt-agenda_settimanale_spedizioni").on('click', function(){
   	   acs_show_win_std('Agenda settimanale spedizioni', 'acs_panel_agenda_settimanale.php?fn=get_form_params', null, 530, 420);	
      });       	

   }   
      






     
     

     if (Ext.get("bt-manutenzione-costi") != null) {

        	Ext.QuickTips.register({
    			target: "bt-manutenzione-costi",
    			title: 'Shipping rates',
    			text: 'Controllo/manutenzione costo spedizioni'
    		});    	 
    		         
        Ext.get("bt-manutenzione-costi").on('click', function(){
        	
			print_w = new Ext.Window({
					  width: 430
					, height: 350
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Controllo/Manutenzione costo spedizioni'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_json_manutenzione_costi.php',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();
				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
        });        


     }   


  	// ------------------- REGOLE RIORDINO FORNITORI ------------------------        
     if (Ext.get("bt-marketing") != null) {
       	Ext.QuickTips.register({
			target: "bt-marketing",
			title: 'Righe ordine marketing',
			text: 'Righe ordine marketing'
		});    	 
         
        Ext.get("bt-marketing").on('click', function(){
     	   acs_show_win_std('Righe ordine marketing', 'acs_righe_ordine_marketing.php?fn=ricerca_ordini_grid', {}, 1100, 450, null, 'icon-gift-16');	
        });        
     }  



	 // ------------------- GIFT ------------------------
	 // Riordino materiali promozionali   
     if (Ext.get("bt-gift") != null){

     	Ext.QuickTips.register({
			target: "bt-gift",
			title: 'Marketing',
			text: 'Riordino materiali promozionali'
		});         
         
        Ext.get("bt-gift").on('click', function(){
      	   acs_show_panel_std('acs_panel_riordino_materiali_promo.php?fn=get_json_grid', 'panel-riordino_materiali_promo');              	   
        });
	}
	 


     //-----------------------------------------------   
     if (Ext.get("main-site-home") != null){

     	Ext.QuickTips.register({
			target: "main-site-home",
			title: 'Home Page',
			text: 'Chiude il desktop e torna alla pagina iniziale '
		});         
     }

     //-----------------------------------------------   
     if (Ext.get("main-site-logout") != null){

     	Ext.QuickTips.register({
			target: "main-site-logout",
			title: 'Logout',
			text: 'Chiude il desktop e scollega utente corrente'
		});         
     }     
     


  <?php if ($js_parameters->p_solo_inter != 'Y'){ ?>     
     //-----------------------------------------------   
     if (Ext.get("main-site-tools") != null){

     	Ext.QuickTips.register({
			target: "main-site-tool",
			title: '(WIP)',
			text: 'Modifica parametri elaborazione utente corrente'
		});         
         
        Ext.get("main-site-tools").on('click', function(){
        	
			print_w = new Ext.Window({
					  width: 600
					, height: 330
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Strumenti di supervisione ambiente'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_history_log.php?fn=get_parametri_form',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
        });
	}         
  <?php } ?>	
     

   <?php if ($js_parameters->p_disabilita_Info != 'Y'){ ?>     
     //-----------------------------------------------   
     if (Ext.get("main-site-search") != null){

     	Ext.QuickTips.register({
			target: "main-site-search",
			title: 'Info',
			text: 'Interrogazione ordini da programmare/programmati '
		});         
         
        Ext.get("main-site-search").on('click', function(){
        	
			print_w = new Ext.Window({
					  width: 650
					, height: 550
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Parametri interrogazione ordini da evadere'
					, layout: 'fit'
					, border: true
					, closable: true
					, iconCls: 'icon-search-16'
					, tools: [
						{
						    type:'help',
						    tooltip: 'Help',
						    handler: function(event, toolEl, panel){
						        show_win_help('MAIN_SEARCH');
						    }
						}								
					]															
				});	
			print_w.show();

				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'acs_form_json_main_search.php',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
        });
	}
 <?php } ?>	         
        

     

   //------------------------------------     
    if (Ext.get("bt-page-refresh") != null){
     	Ext.QuickTips.register({
			target: "bt-page-refresh",
			title: 'Refresh',
			text: 'Esegui il refresh della pagina (F5)'
		});         
    
	    Ext.get("bt-page-refresh").on('click', function(){
	    	window.location.reload();
		});
    }


	//tooltip stato aggiornamento
    if (Ext.get("bt-stato-aggiornamento") != null){
     	Ext.QuickTips.register({
			target: "bt-stato-aggiornamento",
			title: 'Synchro',
			text: <?php echo j($main_module->get_orari_aggiornamento()); ?>
		});
    }

    if (Ext.get("bt-toolbar_ov") != null) {

     	Ext.QuickTips.register({
			target: "bt-toolbar_ov",
			title: 'Setup',
			text: 'Manutenzione voci/parametri gestionali di base'
		});    	 

      Ext.get("bt-toolbar_ov").on('click', function(){
   	   acs_show_win_std('Manutenzione voci/parametri gestionali di base', 'acs_form_json_toolbar_ov.php', null, 500, 300, null, 'icon-design-16');	
      });       	

   }   

    if (Ext.get("bt-toolbar") != null){
     	Ext.QuickTips.register({
			target: "bt-toolbar",
			title: 'Setup',
			text: 'Manutenzione voci/parametri gestionali di base'
		});
            
	    Ext.get("bt-toolbar").on('click', function(){
				print_w = new Ext.Window({
						  width: 1160
						, height: 300
						, minWidth: 300
						, minHeight: 300
						, plain: true
						, title: 'Manutenzione voci/parametri gestionali di base'
						, layout: 'fit'
						, border: true
						, closable: true
						, scrollable: 'auto'
						, tools: [
									{
									    type:'help',
									    tooltip: 'Help',
									    handler: function(event, toolEl, panel){
									        show_win_help('SETUP_MAIN');
									    }
									}								
								]										
					});	
				print_w.show(); 
				
					//carico la form dal json ricevuto da php
					Ext.Ajax.request({
					        url        : 'acs_form_json_toolbar.php',
					        method     : 'GET',
					        waitMsg    : 'Data loading',
					        success : function(result, request){
					            var jsonData = Ext.decode(result.responseText);
					            print_w.add(jsonData.items);
					            print_w.doLayout();
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });						   	
	    });
    }

<?php 
	//apro le finestre in automatico in base al profilo
	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

	if ($js_parameters->open_PLAN == 1){
	    if ($menu_type != 'ORD_VEN')
		  echo "show_plan();";
	}
	
	if ($js_parameters->open_ToDo == 1){
		echo "show_el_arrivi();";
	}	
	
	if ((int)$js_parameters->open_UPLOAD > 0){

		//verifico se esiste la memorizzazione
		$f8 = new F8();
		$f8_row = $f8->get_memorizzazione_data_by_id($js_parameters->open_UPLOAD, (object)array("mod" => $main_module->get_cod_mod(), "func" => "OPEN_UPLOAD"));
		
		if ($f8_row != null){
			echo "acs_show_panel_std('acs_panel_prenotazione_carichi.php?fn=get_json_grid', 'panel-prenotazione_carichi', " . $f8_row['DFMEMO'] . ")";
		}

	}
	
	if ($_REQUEST['fn'] == 'open_form_sped'){
		$m_params = acs_m_params_json_decode();
		?>
		
		{"success":true, "items": [
		
		        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: true,
		            title: '',
		            //url: 'acs_print_lista_consegne.php',
		            
		            items: [
		            	{
	                	xtype: 'hidden',
	                	name: 'f_filter',
	                	value: '<?php echo acs_je($m_params->filter); ?>'
	                }  ,
						{
	                    xtype: 'radiogroup',
	                    anchor: '100%',
	                    fieldLabel: 'Data',
	                    items: [
	                        {
	                            xtype: 'radio'
	                          , name: 'f_sceltadata' 
	                          , boxLabel: 'Programmata'
	                          , inputValue: 'TDDTEP'
	                          , checked: true
	                        },
	                        {
	                            xtype: 'radio'
	                          , name: 'f_sceltadata' 
	                          , boxLabel: 'Spedizione'
	                          , inputValue: 'TDDTSP'                          
	                        }
	                   ]
	                } ,
	                {
								flex: 1,						 
								xtype: 'checkboxgroup',
								fieldLabel: 'Bilingue',
								labelAlign: 'left',
							   	allowBlank: true,
							   	labelWidth: 140,
							   	items: [{
			                            xtype: 'checkbox'
			                          , name: 'f_bilingue' 
			                          , boxLabel: ''
			                          ,checked: false
			                          , inputValue: 'Y'
			                        }]														
							 },  {
								flex: 1,						 
								xtype: 'checkboxgroup',
								fieldLabel: 'Logo',
								labelAlign: 'left',
							   	allowBlank: true,
							   	labelWidth: 140,
							   	items: [{
			                            xtype: 'checkbox'
			                          , name: 'f_logo' 
			                          , boxLabel: ''
			                          ,checked: false
			                          , inputValue: 'Y'
			                        }]														
							 }         
		            ],
		            
					buttons: [					
						{
				            text: 'Visualizza',
					        iconCls: 'icon-folder_search-24',		            
					        scale: 'medium',		            
				            handler: function() {
				                this.up('form').submit({
			                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
			                        target: '_blank', 
			                        standardSubmit: true,
			                        method: 'POST',
			                        params: {
			                        	form_values: Ext.encode(this.up('form').getValues())
			                        }
			                  });
			                  
			                  this.up('window').close();
				                
				            }
				        } 
			        
			        
			        ]            
		            
		           
		}
			
		]}
		
		
		<?php 
		}
?>	
    


	//schedulo la richiesta dell'ora ultimo aggiornamento
	Ext.TaskManager.start(refresh_stato_aggiornamento);	
	Ext.TaskManager.start(refresh_notifiche);

        
        
    });
    </script>
</head>
<body>

<!-- use class="x-hide-display" to prevent a brief flicker of the content -->
    <div id="west" class="x-hide-display">
        <p>Hi. I'm the west panel.</p>
    </div>
    <div id="center2" class="x-hide-display">
        <a id="hideit" href="#">Toggle the west region</a>
    </div>
    <div id="center1" class="x-hide-display">
    </div>
    
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;">
    </div>
    <div id="south" class="x-hide-display">
		<?php include("_legenda.php"); ?>
    </div>
</body>
</html>
