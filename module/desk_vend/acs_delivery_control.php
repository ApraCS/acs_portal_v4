<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));

ini_set('max_execution_time', 6000);



//$ts = microtime(true);
$m_params = acs_m_params_json_decode();
$ts = $m_params->tms;


//********************************************************************
//********************** MBM *****************************************
//********************************************************************

if ($_REQUEST['fn'] == 'call'){
	$m_params = acs_m_params_json_decode();
	$sped = $m_params->sped_id;
	
	$seq = array();
	global $conn;
	//Eseguo la call
	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	 $cl_p .= sprintf("%-2s",  $id_ditta_default);
	 $cl_p .= sprintf("%09d", $sped);
	 $cl_p .= sprintf("%-2s", "");
	 $cl_p .= sprintf("%04d", 0);
	 $cl_p .= sprintf("%06d", 0);
	 $cl_p .= sprintf("%-1s", "");
	 $cl_p .= sprintf("%-1s", "");
	 $cl_p .= sprintf("%-2s", "");
	 $cl_p .= sprintf("%-10s", "");
	 $cl_p .= sprintf("%-30s", "");
	 $cl_p .= sprintf("%-50s", "");
	 $cl_p .= sprintf("%-9s", "");
	 $cl_p .= sprintf("%-9s", "");
	 $cl_p .= sprintf("%-30s", $ts);  //progressivo timestamp
	 $cl_p .= sprintf("%-91s", "");

	 $call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
	 $call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
	 $call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());

	 $cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
	 $call_return = $tkObj->PgmCall('UR21HE', $libreria_predefinita_EXE, $cl_in, null, null);

	 $ret = array();
	 $ret['success'] = true;
	 echo acs_je($ret);
	 exit;
	 
}


if ($_REQUEST['fn'] == 'send'){

	$now = DateTime::createFromFormat('U.u', microtime(true));
	$sf = $now->format("YmdHisu");
	
	//scrivo file zip contentente i file
	$zip = new ZipArchive();
	$filename = '/tmp/dc_zip_'.$ts.'.zip';
	
	if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
		$ret = array();
		$ret['success'] = false;
		$ret['message'] = "Impossibile creare file zip {$filename}";
		echo acs_je($ret);
		exit;
	}
	

	foreach ($cfg_delivery_control as $k1=>$v1){
		 
		$contenuto_file = contenuto_file($ts, $k1);
		
/*		
			$scrivo_file= fopen (strtoupper($k1."_ITF").".{$sf}", 'w');
			fwrite($scrivo_file,  $contenuto_file);
			fclose($scrivo_file);
*/			
			
		
		$zip->addFromString(strtoupper($k1."_ITF").".{$sf}", $contenuto_file);

		$nome_file = $ts."_".$k1.".csv";
		$local_dir= "/tmp/";
		$remote_dir="item";
 
	}
	

	$ret = array();
	$ret['success'] = true;
	$ret["numfiles"] = $zip->numFiles;
	$ret["status"]   = $zip->status;
	$zip->close();
	

	//invio file zip tramite sftp	

	 $currentIncludePath = get_include_path();
	
	 set_include_path($currentIncludePath.  PATH_SEPARATOR . ROOT_ABS_PATH . "utility/phpseclib1.0.5");
	
	 require('Crypt/Base.php');
	 require('Crypt/RC4.php');
	 require('Crypt/Hash.php');
	 require('Math/BigInteger.php');
	 require('Net/SSH2.php');
	 require('Net/SFTP.php');
	 require('Crypt/Random.php');
	 require('Crypt/Rijndael.php');

	 define('NET_SFTP_LOGGING', NET_SFTP_LOG_COMPLEX);
	
	 $sftp = new Net_SFTP($cfg_delivery_control_sftp['host']);
	 if (!$sftp->login($cfg_delivery_control_sftp['user'], $cfg_delivery_control_sftp['psw'])) {
	 exit('Login Failed');
	 }
	

	 // ftp_put($conn_ftp, $remote_dir.$nome_file, $local_dir.$nome_file, FTP_ASCII)
	
	 if ($sftp->put("xintesa/tr_{$sf}.zip", $filename, NET_SFTP_LOCAL_FILE))
	 {
	 	$ret['message'] = "Successfully uploaded $filename";
	 	$ret['success'] = true;
	 }
	 else
	 {
	 	$ret['message'] = "Error uploading $filename";
	 	$ret['success'] = false; 
	 }
	
	
	
	echo acs_je($ret);
	exit;
}










//********************************************************************
//********************** SAV *****************************************
//********************************************************************

if ($_REQUEST['fn'] == 'call_sav'){
	$m_params = acs_m_params_json_decode();
	$sped = $m_params->sped_id;

	$seq = array();
	global $conn, $auth;
	//Eseguo la call
	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s",  $id_ditta_default);
	$cl_p .= sprintf("%09d", $sped);
	$cl_p .= sprintf("%-2s", "");//tipo_ca
	$cl_p .= sprintf("%04d", 0); //anno_ca
	$cl_p .= sprintf("%06d", 0); //numero ca
	$cl_p .= sprintf("%-10s", trim($auth->get_user())); //utente ToDo
	$cl_p .= "N";
	$cl_p .= " "; //elab

	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
	$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());

	$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
	$call_return = $tkObj->PgmCall('UR21HF', $libreria_predefinita_EXE, $cl_in, null, null);
	
	exit;
}

if ($_REQUEST['fn'] == 'send_sav'){

	// al momento non faccio niente
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
	

	/*
	 define('NET_SFTP_LOGGING', NET_SFTP_LOG_COMPLEX);

	 $sftp = new Net_SFTP('192.168.2.8');
	 if (!$sftp->login('sma1sftp', 'sma1sftp')) {
		exit('Login Failed');
		}
		*/

	//$ts='1484150529.6425';
	
	/* intanto come prova invio email */
	$mail = new PHPMailer();
	$mail->From = "m.arosti@apracs.it";
	$mail->FromName = "Matteo Arosti";
	$mail->AddAddress("davidem@apracomputersystem.it");
	$mail->AddAddress("msegaria@apracomputersystem.it");
	$mail->AddAddress("ict01@alacucine.sm");
	$mail->IsHTML(true);
	$mail->Subject = 'Invio file delivery contro SAV';
	$mail->Body = 'Invio file di test per spunta vettore';

	foreach ($cfg_delivery_control as $k1=>$v1){
			
		$contenuto_file = contenuto_file_all($k1);
		$nome_file_tmp = '/tmp/'.$ts.'_'.$k1.'.csv';
		
		$scrivo_file= fopen ($nome_file_tmp, 'w');
		fwrite($scrivo_file,  $contenuto_file);
		fclose($scrivo_file);

		$nome_file = $ts."_".$k1.".csv";
		$local_dir= "/tmp/";
		$remote_dir="item";
			
		$mail->AddAttachment($nome_file_tmp);
		//Inviamo l'email
			
		/*
		 // ftp_put($conn_ftp, $remote_dir.$nome_file, $local_dir.$nome_file, FTP_ASCII)

		 if ($sftp->put($remote_dir.$nome_file, $local_dir.$nome_file, NET_SFTP_LOCAL_FILE))
		 {
			echo "Successfully uploaded $local_dir.$nome_file";
			}
			else
			{
			echo "Error uploading $local_dir.$nome_file";
			}
			*/

	}
	
	if($mail->Send())
		echo "Email inoltrata correttamente";
	
	exit;
	
}










function contenuto_file($ts, $tabella){
	global $cfg_delivery_control, $conn, $s;

	$sql="SELECT * FROM {$cfg_delivery_control[$tabella]['file']} WHERE {$cfg_delivery_control[$tabella]['id']}='{$ts}'";  

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	if (isset($cfg_delivery_control[$tabella]['campi_data']))
		$campi_data = $cfg_delivery_control[$tabella]['campi_data'];
	else
		$campi_data = array();

	while($row = db2_fetch_assoc($stmt)){
		array_shift($row); //toglie il primo elemento
		
		foreach ($row as $kr=>$v) { //se e' di tipo data ed e' uguale a 0 sostituisco il valore con blank
			if (in_array($kr, $campi_data)) {
				$my_v = trim($v);
				if (strlen($my_v) == 8)
					$row[$kr] = print_date($my_v, "%Y/%m/%d");
				else if (strlen($my_v) == 20) {					
					$row[$kr] = print_date(substr($my_v, 0, 8), "%Y/%m/%d") ."-". implode(".", array(
						substr($my_v, 8, 2),substr($my_v, 10, 2),substr($my_v, 12, 2),substr($my_v, 14, 6)
					));
				}
				else 
					$row[$kr] = "";

			} //conversione data
		}
		
		//sostituisco le coordinate
		if (isset($cfg_delivery_control[$tabella]['campi_coordinate'])){
			$gmap_ind = implode(",", array_map('trim', array($row['ADNAZI'], $row['ADCAP'], $row['ADPROV'], $row['ADCITT'] , substr($row['ADINDI'], 0, 30))));
			$row_coordinate = $s->get_coordinate_by_ind($gmap_ind);
			if (is_null($row_coordinate) == false && $row_coordinate['GLLAT'] && $row_coordinate['GLLNG']) {
				$row[$cfg_delivery_control[$tabella]['campi_coordinate']['long']] = $row_coordinate['GLLNG'];
				$row[$cfg_delivery_control[$tabella]['campi_coordinate']['lati']] = $row_coordinate['GLLAT'];
			}
		}
		
		
		
		$contenuto_r[] = implode('|', array_map('trim', $row));
	}

	return implode("\n", $contenuto_r) . "\n";

}


function contenuto_file_all($tabella){
	global $cfg_delivery_control, $conn;

	$sql="SELECT * FROM {$cfg_delivery_control[$tabella]['file']}";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	if (isset($cfg_delivery_control[$tabella]['campi_data']))
		$campi_data = $cfg_delivery_control[$tabella]['campi_data'];
	else
		$campi_data = array();

	while($row = db2_fetch_assoc($stmt)){
		///array_shift($row); //toglie il primo elemento

		foreach ($row as $kr=>$v) { //se e' di tipo data ed e' uguale a 0 sostituisco il valore con blank
			if (in_array($kr, $campi_data) && trim($v) == '0')
				$row[$kr] = "";
		}

		$contenuto_r[] = implode('|', array_map('trim', $row));
	}

	return implode("\n", $contenuto_r) . "\n";

}
