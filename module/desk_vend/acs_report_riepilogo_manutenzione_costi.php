<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

?>


<html class=acs_report>
<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table{border-collapse:collapse; width: 100%; margin-top: 15px;}
table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
h2.acs_report{background-color: black; color: white;}

html.acs_report body{padding: 10px;}
html.acs_report h2{font-size: 20px; padding: 5px;}
   

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}
 
@media print
{
	.noPrint{display:none;}
	.page-break  { display: block; page-break-before: always; }
 	.onlyPrint{display: block;}	
}

</style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
  
  </script>

</head>
<body>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 	

<div id='my_content'>

<h2>Riepilogo spedizioni programmate</h2>

<?php


$form_ep = strtr($_REQUEST['form_ep'], array('\"' => '"'));
$form_ep = json_decode($form_ep);

//data
if (strlen($form_ep->f_data_a) > 1)
	$sql_where.= " AND CSDTSP >= " . $form_ep->f_data . " AND CSDTSP <= " . $form_ep->f_data_a;
else
	$sql_where.= " AND CSDTSP = " . $form_ep->f_data;

//area spedizione
if (strlen($form_ep->f_area_spedizione) > 0)
	$sql_where.= " AND TA_ASPE.TAKEY1 = " . sql_t($form_ep->f_area_spedizione);

//tipologia trasporto (multipla)
if (count($form_ep->f_trasporto) > 0)
	$sql_where.= " AND SP.CSTITR IN (" . sql_t_IN($form_ep->f_trasporto) . ")";

//trasportatore
if (count($form_ep->f_trasportatore) > 0)
	$sql_where.= " AND TA_TRAS.TAKEY1 IN (" . sql_t_IN($form_ep->f_trasportatore) . ")";

$ar = array();

$sql = "SELECT TA_SPED.TADESC AS SPED, TA_TRASP.TADESC AS TRASPO, TA_MEZZO.TADESC AS MEZZO, TA_ASPE.TADESC AS AREASP, TA_ASPE.TAKEY1 AS CODASPE, CSDTSP, CSPROG, TA_TRAS.TADESC AS TRASP, TA_VETT.TADESC VETT, TA_ITIN.TADESC AS ITIN, CSCSTR, CSCSTD, CSKMTR, CSDESC   
FROM {$cfg_mod_Spedizioni['file_calendario']} SP
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
		  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI   
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE 
		  ON TA_ASPE.TADT = TA_ITIN.TADT AND TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE

		/* vettore -- trasportatore */  
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
   			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
   			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_SPED
   			ON SP.CSDT = TA_SPED.TADT AND TA_SPED.TATAID = 'TSPE' AND TA_SPED.TAKEY1 = SP.CSTISP
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRASP
   			ON SP.CSDT = TA_TRASP.TADT AND TA_TRASP.TATAID = 'TTRA' AND TA_SPED.TAKEY1 = SP.CSTITR
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_MEZZO 
            ON TA_MEZZO.TADT = SP.CSDT AND TA_MEZZO.TAKEY1 = SP.CSCAUT AND TA_MEZZO.TATAID = 'AUTO'
WHERE CSDT='$id_ditta_default' AND CSCALE = '*SPR' {$sql_where} 
ORDER BY TA_ASPE.TADESC, CSDTSP, TA_TRAS.TADESC";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);

while ($row = db2_fetch_assoc($stmt)){
	$row['CSDTSP'] 	                = $row['CSDTSP'];
	$row['AREASP'] 			        = $row['AREASP'];
	$row['CSPROG'] 			        = $row['CSPROG'];
	$row['TRASP'] 			        = $row['TRASP'];
	$row['CODASPE'] 			    = $row['CODASPE'];
	$row['VETT']                 	= $row['VETT'];
	$row['MEZZO'] 		            = $row['MEZZO'];
	$row['ITIN'] 	             	= $row['ITIN'];
	$row['CSCSTR'] 				    = $row['CSCSTR'];
	$row['CSCSTD'] 				    = $row['CSCSTD'];
	$row['CSKMTR'] 				    = $row['CSKMTR'];
	$row['TRASPO'] 			        = $row['TRASPO'];
	$row['SPED'] 			        = $row['SPED'];
	$row['CSDESC'] 		            = $row['CSDESC'];

 $liv0 = trim($row['CODASPE']);
 $liv1 = trim($row['CSDTSP']);
 
 $d_ar = &$ar; 
 $c_liv = $liv0;
 if (!isset($d_ar[$c_liv]))
 	$d_ar[$c_liv] = array("cod" => $c_liv, "area" => $row['CODASPE'], "descr"=>$c_liv, "children"=>array());
 
 $d_ar = &$d_ar[$c_liv]['children'];
 	
 
 $c_liv = $liv1;
 
 	if (!isset($d_ar[$c_liv]))
 		$d_ar[$c_liv] = array("cod" => $c_liv, "area" => $row['CODASPE'], "descr"=>$c_liv, "children"=>array());

 $d_ar = &$d_ar[$c_liv]['children']; 
 
 $d_ar[] = $row;
 
}



// STAMPO REPORT *******************************
foreach ($ar as $k0 => $l0){

 $l0_tot = array();

 echo "<br><h2 class=acs_report>" .$s->decod_std('ASPE', $l0['area']) . "</h2>";
 
 echo "<table class=acs_report>";
 
 ?>
		<tr class="t-l1">
		 <td>Data</td>
 		 <td>Trasportatore</td>
 		 <td>Vettore</td>		 
 		 <td>Mezzo</td>		 
 		 <td>Itinerario</td>		 		 
 		 <td>Costo trasporto</td>		 
 		 <td>Costo deposito</td> 		 
 		 <td>Km</td>		 
 		 <td>Tipo spedizione</td>		 
 		 <td>Tipologia trasporto</td>		 
 		 <td>Descrizione spedizione</td> 	 
 		</tr>

 <?php 		
 
	 foreach ($l0['children'] as $k1 => $l1){ //per ogni data
	 	
	 	$l0_gg = array();
	 
	 	foreach ($l1['children'] as $k2 => $l2){ //per ogni riga ordine
		?>
		
		<tr>
		 <td><?php echo print_date($l2['CSDTSP']); ?></td>
		 <td><?php echo $l2['TRASP']. " (#{$l2['CSPROG']})"; ?></td>
		 <td><?php echo $l2['VETT']; ?></td>		 
		 <td><?php echo $l2['MEZZO']; ?></td>		 
		 <td><?php echo $l2['ITIN']; ?></td>		 		 
		 <td class=number><?php echo n($l2['CSCSTR'],2); ?></td>
		 <td class=number><?php echo n($l2['CSCSTD'],2); ?></td>
		 <td class=number><?php echo n($l2['CSKMTR'],0); ?></td>		 
		 <td><?php echo $l2['SPED']; ?></td>		 
		 <td><?php echo $l2['TRASPO']; ?></td>		 
		 <td><?php echo $l2['CSDESC']; ?></td>		 	 
		 </tr>

		<?php

		
		//somme e conteggi
		$l0_tot = somme_conteggi($l0_tot, $l2);
		$gen_tot = somme_conteggi($gen_tot, $l2);
		$l0_gg = somme_conteggi($l0_gg, $l2);
		
	 	
	  } //per ogni riga
	  
	  
	  
	 echo " <tr>
	  <td colspan=5 style=font-weight:bold;>Totale giorno</td>
	  <td class=number style=font-weight:bold;>".n($l0_gg['CSCSTR'],2)."</td>
	  <td class=number style=font-weight:bold;>".n($l0_gg['CSCSTD'],2)."</td>
	  <td class=number style=font-weight:bold;>".n($l0_gg['CSKMTR'],0)."</td>
	  <td colspan=3>&nbsp;</td>
	  		</tr>";
	  //total per data
	  
	  
	 
	 } //per ogni data
	 
	 ?>
	 

		<tr class="f-l1">
		 <td colspan=5>Totale area spedizione <?php echo $s->decod_std('ASPE', $l0['area']); ?></td>		 
		 <td class=number><?php echo n($l0_tot['CSCSTR'],2); ?></td>		 		 
		 <td class=number><?php echo n($l0_tot['CSCSTD'],2); ?></td>
		 <td class=number><?php echo n($l0_tot['CSKMTR'],0); ?></td>		 		 
		 <td colspan=3>&nbsp;</td>	
		</tr>

	 <?php

 echo "</table>";

}


?>

<?php if (count($ar) > 1){ ?>
        <br><h2 class=acs_report>Totale generale</h2>
        <table class=acs_report>
		<tr class="t-l1">
		 <td width="50%" colspan=5></td>		 
 		 <td>Costo trasporto</td>		 		 
 		 <td>Costo deposito</td>
 		 <td>Km</td>	 	 
 		</tr>
		<tr class="f-l1">
		 <td colspan=5>Totale generale</td>		 
		 <td class=number><?php echo n($gen_tot['CSCSTR'],2); ?></td>		 		 
		 <td class=number><?php echo n($gen_tot['CSCSTD'],2); ?></td>
		 <td class=number><?php echo n($gen_tot['CSKMTR'],0); ?></td>	 
		</tr>
 		
</table>
<?php } ?>




<?php


function somme_conteggi($ar, $r){
	$ar['CSKMTR'] 				+= $r['CSKMTR'];	
	$ar['CSCSTR'] 				+= $r['CSCSTR'];
	$ar['CSCSTD'] 				+= $r['CSCSTD'];		
  return $ar;	
} 



?>


</div>
</body>
</html>


