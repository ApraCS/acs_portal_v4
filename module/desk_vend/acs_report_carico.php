<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$da_form = acs_m_params_json_decode();

// ******************************************************************************************
// FORM RICHIESTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_form'){
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
			buttons: [{
	            text: 'Visualizza',
	            handler: function() {
	                this.up('form').submit({
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST'
                  });
	                
	            }
	        }],             
            
            items: [
                {
                	xtype: 'hidden',
                	name: 'rec_id',
                	value: '<?php echo $da_form->rec_id ?>'
                }, {
                	xtype: 'hidden',
                	name: 'rec_liv',
                	value: '<?php echo $da_form->rec_liv ?>'
                }, {
                	xtype: 'hidden',
                	name: 'tipo_elenco',
                	value: '<?php echo $da_form->tipo_elenco ?>'
                }, {
                	xtype: 'hidden',
                	name: 'tipo',
                	value: 'lista_consegne_programmate'
                },{                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 10',
			        items: [{
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa importi',
			                boxLabel: 'Si',
			                name: 'stampa_importi',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_importi',
			                inputValue: 'N'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			        }, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a radio button
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa pallet',
			                boxLabel: 'Si',
			                name: 'stampa_pallet',
			                inputValue: 'Y'
			            }, {			            	
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_pallet',
			                inputValue: 'N'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			        }, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a radio button
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa GG rit',
			                boxLabel: 'Si',
			                name: 'stampa_gg_rit',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_gg_rit',
			                inputValue: 'N'
			            }]
			        }
			        
			        
			        ]			        
			        
			        
			        
			        
			        
			    }  
			    
			    
			    
			    
,{                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 10',
			        items: [{
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Data',
			                boxLabel: 'Progr.',
			                name: 'data_riferimento',
			                inputValue: 'data_programmata'
			            }, {
			                boxLabel: 'Sped.',
			                checked: true,			                
			                name: 'data_riferimento',
			                inputValue: 'data_spedizione'
			            }]
			        },
			        {
			            xtype: 'component',
			            width: 10
			        },
			        
			        {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Colli produzione',
			                boxLabel: 'Si',
			                name: 'colli_prod',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'colli_prod',
			                inputValue: 'No'
			            }]
			        },
			        
			        {xtype : 'component',
			        width: 10},
			         {xtype : 'component',
			        flex : 1}
			        
			        ]			        
			        
			    }                 
            ]
        }
	
]}
<?php
exit;
}

// ******************************************************************************************
// REPORT
// ******************************************************************************************
$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


$sottostringhe = explode("|", $_REQUEST['rec_id']);

if ($_REQUEST['tipo_elenco'] == 'GATE')
    $td_sped_field = 'TDNBOF';
else
    $td_sped_field = 'TDNBOC';
        

switch ($_REQUEST['rec_liv']){
	case "liv_0":
		$m_sped = $sottostringhe[2];
		$spedizione = $s->get_spedizione($m_sped);
		$email_subject = "Lista sequenza carico del " . print_date($spedizione['CSDTSP']);
		break;
	case "liv_1":
		$tmp_carico = $sottostringhe[2];
		$tmp = explode("___", $tmp_carico);
		$m_data = $tmp[0];
		$m_sped = $tmp[1];
		$m_tpca = $tmp[2];
		$m_aaca = $tmp[3];
		$m_nrca = $tmp[4];
		$spedizione = $s->get_spedizione($m_sped);
		$email_subject = "Lista sequenza carico " . implode("_", array((int)$m_aaca, trim($m_tpca), (int)$m_nrca));
		
		break;
	case "liv_2":
		$tmp_carico = $sottostringhe[2];
		$tmp = explode("___", $tmp_carico);
		$m_data = $tmp[0];
		$m_sped = $tmp[1];
		$m_tpca = $tmp[2];
		$m_aaca = $tmp[3];
		$m_nrca = $tmp[4];

		$tmp_cliente = $sottostringhe[4];
		$tmp = explode("___", $tmp_cliente);
		$m_ccon = $tmp[1];

		break;
	default: die("error");
}



$itinerario = new Itinerari();
$itinerario->load_rec_data_by_k(array("TAKEY1"=>$spedizione['CSCITI']));

$vettore = new Vettori();
$vettore->load_rec_data_by_k(array("TAKEY1"=>$spedizione['CSCVET']));

$ar_email_to = array();
$ar_email_to[] = array(trim($itinerario->rec_data['TAMAIL']), "ITINERARIO " . j(trim($itinerario->rec_data['TADESC'])) . " (" .  trim($itinerario->rec_data['TAMAIL']) . ")");
$ar_email_to[] = array(trim($vettore->rec_data['TAMAIL']), "VETTORE " . j(trim($vettore->rec_data['TADESC'])) . " (" .  trim($vettore->rec_data['TAMAIL']) . ")");

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
    
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {
        border-collapse: unset;
	     	
    }
        .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 
<div style='text-align: right;'> 
<?php if ($cfg_mod_Spedizioni['print_barcode_piano_di_carico'] == 'Y'){ ?> 	
	<?php $barcode = sprintf("%09s", $m_sped); ?>
	<img src = "../base/generate_img_barcode.php?barcode=<?php echo $barcode; ?>"/>
	<p style = 'margin-right: 85px; margin-bottom: 20px;' ><b>  <?php echo $barcode; ?></b></p>
<?php }?>
</div>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php
	 
	$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
				   LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				     ON TD.TDDT=SP.CSDT AND {$td_sped_field} = SP.CSPROG AND SP.CSCALE = '*SPR'
				   ";
	if ($_REQUEST['colli_prod']=='Y'){ 
	   $sql_SELECT = ", RDQTA AS COLLI_PROD ";
	   $sql_JOIN = " LEFT OUTER JOIN (
  					  SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
    				  FROM {$cfg_mod_Spedizioni['file_righe']} RDO
  					  WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
   					  GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
								     ) RD
   				  ON TD.TDDT=RD.RDDT AND TD.TDOTID=RD.RDOTID AND TD.TDOINU=RD.RDOINU AND TD.TDOADO=RD.RDOADO AND TD.TDONDO=RD.RDONDO";
	}
	$sql_WHERE = " WHERE " . $s->get_where_std() . " AND TDSWSP='Y' ";

	/* NON FILTRO MAI PER DATA, COSI' VENGONO FUORI EVENTUALI ANOMALIE */
	//if (isset($m_data)) $sql_WHERE .= " AND TDDTEP = $m_data ";
	
	if (isset($m_sped)) $sql_WHERE .= " AND {$td_sped_field} = " . $m_sped;	
	if (isset($m_tpca)) $sql_WHERE .= " AND TDTPCA = " . sql_t_trim($m_tpca);	
	if (isset($m_tpca)) $sql_WHERE .= " AND TDAACA = " . $m_aaca;	
	if (isset($m_nrca)) $sql_WHERE .= " AND TDNRCA = " . $m_nrca;	
	if (isset($m_ccon)) $sql_WHERE .= " AND TDCCON = " . sql_t_trim($m_ccon);	

	
	$sql = "SELECT TD.*, SP.* " .$sql_SELECT. $sql_FROM . $sql_JOIN. $sql_WHERE	. " ORDER BY TDDTEP, TDCITI, {$td_sped_field}, TDSECA, TDCCON";
	
	$stmt = db2_prepare($conn, $sql);		
	$result = db2_execute($stmt);
 
    $liv0_in_linea = null;
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
	    $liv0 = implode("|", array($r['TDDTEP'], $r['CSCITI'], $r['CSCVET'], $r['CSCAUT'], $r['CSCCON'], $r['TDTPCA'], $r['TDAACA'], $r['TDNRCA']));
	    $liv1 = $r['TDSECA'];
		$liv2 = implode("|", array($r['TDCCON'], $r['TDCDES']));		
		$liv3 = implode("|", array($r['TDDT'], $r['TDOTID'], $r['TDOINU'], $r['TDOADO'], $r['TDONDO']));		
		
		//data|itinerario|vettore
		$t_ar = &$ar;
		$l = $liv0;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r); 					  								  
			$t_ar = &$t_ar[$l]['children'];
		
		//carico
		$l = $liv1;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>implode("_", array($r['TDAACA'], $r['TDTPCA'], $r['TDNRCA'])), "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar[$l]['carico'] = $s->get_carico_td($r);
			$t_ar[$l]['orario_carico'] = $r['CSHMPG'];			 								  
			$t_ar = &$t_ar[$l]['children'];
		 								  								  
		//cliente|dest
		$l = $liv2;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];								  								  

		//ordine
		$l = $liv3;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];			
		
	} //while

	
//STAMPO
 $cl_liv_cont = 0;
 if ($_REQUEST['stampa_dettaglio_ordini'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
 $liv2_row_cl = ++$cl_liv_cont; 
 $liv1_row_cl = ++$cl_liv_cont;
 
echo "<div id='my_content'>"; 
foreach ($ar as $kl0 => $l0){
    echo liv0_intestazione_open($l0['record']);
    
    foreach ($l0['children'] as $kl1 => $l1){
//				echo liv1_intestazione_open($l1, $liv1_row_cl);	  			
				
					global $cambia_seca, $n_ord_in_seca;
					$cambia_seca = 'Y';

					//conto le righe che stampero'
					$n_ord_in_seca = 0;
					foreach ($l1['children'] as $kl2 => $l2)
						$n_ord_in_seca += count($l2['children']);							

			  		foreach ($l1['children'] as $kl2 => $l2){
						echo liv2_intestazione_open($l2, $liv2_row_cl);
						
//						if ($_REQUEST['stampa_dettaglio_ordini']=="Y")
				  		foreach ($l2['children'] as $kl3 => $l3)
							echo liv3_intestazione_open($l3, $liv3_row_cl);			
							
					}			
			}		
	
			
	echo liv0_intestazione_close($l0);
	
	echo "<div class=\"page-break\"></div>";
	
}

echo "</div>";

 		
?>

 </body>
</html>



<?php


function somma_valori($ar, $r){
 //il volume e' aumentato in base alla percentuale passata (recuperata prima dall'itinerario)
 $ar['COUNT'] 	+= 1; 
 $ar['VOLUME'] 	+= $r['TDVOLU'] * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
 $ar['COLLI'] 	+= $r['TDTOCO'];
 $ar['PALLET'] 	+= $r['TDBANC'];
 $ar['IMPORTO'] += $r['TDTIMP']; 
 $ar['PESO'] 	+= $r['TDPLOR']; 
 $ar['COLLI_PROD'] 	+= $r['COLLI_PROD']; 
 return $ar;  
}


function liv3_intestazione_open($l, $cl_liv){
	global $cambia_cliente, $cambia_seca, $n_ord_in_seca, $n_ord_in_cliente;
	
	if ($_REQUEST['indirizzo_di'] == 'ind_destinazione'){
		$m_loca = 'TDLOCA';
		$m_prov = 'TDPROV';
		$m_cap  = 'TDCAP';
	} else {
		$m_loca = 'TDDLOC';
		$m_prov = 'TDPROD';
		$m_cap  = 'TDDCAP';
	}	
	

	$ret = "<tr class=liv{$cl_liv}>";
		
	if ($cambia_seca == 'Y')
		$ret .= "<td rowspan = {$n_ord_in_seca} valign=top>{$l['record']['TDSECA']}</td>";
	$cambia_seca = 'N';
	
	$ret .= "<td>{$l['record']['TDNRLO']}</td>";
	
	if ($cambia_cliente == 'Y'){	
		$ret .= "<td valign=top rowspan={$n_ord_in_cliente} class=grassetto>{$l['record']['TDDCON']}</td>";	
		$ret .= "<td valign=top rowspan={$n_ord_in_cliente}>{$l['record'][$m_loca]}</td>";
		$cambia_cliente = 'N';
	} else {
		//$ret .= "<td colspan=2>&nbsp;</td>";
	}	
		
	$ret .= "<td>{$l['record']['TDVSRF']}</td>";	
	$ret .= "<td>{$l['record']['TDOTPD']}</td>";		
	$ret .= "<td>{$l['record']['TDOADO']}</td>";	
	$ret .= "<td>{$l['record']['TDONDO']}</td>";	
	$ret .= "<td>{$l['record']['TDSTAT']}</td>";
	$ret .= "<td>{$l['record']['TDSELO']}</td>";
	$ret .= "<td>{$l['record']['TDRFCA']}</td>";		
	$ret .= "<td>{$l['record']['TDDVN1']}</td>";	
	$ret .= "<td>{$l['record']['TDDVN2']}</td>";	
	if ($_REQUEST['stampa_pallet']=='Y') $ret .= "<td class=number>" . n($l['record']['TDBANC'], 0) . "</td>";	
	$ret .= "<td class=number>" . n($l['val']['VOLUME'], 3) . "</td>";	
	$ret .= "<td class=number>" . n($l['val']['COLLI'], 0) . "</td>";	
	if ($_REQUEST['colli_prod']=='Y') $ret .= "<td class=number>" . n($l['record']['COLLI_PROD'], 0) . "</td>";	
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td class=number>" . n($l['record']['TDTIMP'], 0) . "</td>";	
	if ($_REQUEST['stampa_gg_rit']=='Y') $ret .= "<td class=number>{$l['record']['TDGGRI']}</td>";	
	
	$ret .= "</tr>";
 return $ret;
}



function liv2_intestazione_open($l, $cl_liv){
	global $cambia_cliente, $n_ord_in_cliente;
	$cambia_cliente = 'Y';
	$n_ord_in_cliente = count($l['children']);
}




function liv0_intestazione_open($r){
	global $s, $spedizione, $itinerario;
	global $m_aaca, $m_tpca, $m_nrca, $m_sped;

	if ($_REQUEST['data_riferimento'] == 'data_spedizione')
		$f_data = 'TDDTSP';
	else
		$f_data = 'TDDTEP';
	
		
	$ret = "
       		
			<H2>LISTA SEQUENZE DI CARICO</H2>
			<H3>" . $itinerario->rec_data['TADESC'] . "</span></H3>
			
			
			<table class=int0 width=\"100%\">
             <tr>
			  <td width=\"50%\">Spedizione del " . print_date($r[$f_data]) . " - Carico: " . implode("_", array((int)$m_aaca, trim($m_tpca), (int)$m_nrca)) . "</td>
	 		  <td>Vettore: </td>	 		
			  <td width=\"50%\">"  . 
			  	$s->decod_std('AUTR', $spedizione['CSCVET']) . " - " . $s->decod_std('AUTO', $spedizione['CSCAUT']) . " " .	$s->decod_std('COSC', $spedizione['CSCCON']) .
			   "</td>			  
			 </tr> 
			 <TR>
			  <TD COLSPAN=2> </td>
			  <TD style='font-weight: normal; font-size: 0.9em;'>
    				" . stampa_dati_targa($spedizione['CSTARG']) . "
        		
        	  </TD>
			 </TR> 			
			</table>
			
			
			<table class=int1>

			 <tr class=liv_totale>";
	  		  $ret .= "<td class=grassetto>Seq.</td>";
	  		  $ret .= "<td class=grassetto>Lotto</td>";	  		  	  		  
	  		  $ret .= "<td class=grassetto>Cliente</td>";
	  		  $ret .= "<td class=grassetto>Localit&agrave;</td>";
	  		  $ret .= "<td class=grassetto>Riferimento</td>";
	  		  $ret .= "<td class=grassetto colspan=3>Ordine</td>";
	  		  $ret .= "<td class=grassetto>St</td>";
	  		  $ret .= "<td class=grassetto>Rf.Pr.</td>";	  		  
	  		  $ret .= "<td class=grassetto>Rf.Sc.</td>";	  		  	
	  		  $ret .= "<td class=grassetto>Modello</td>";
	  		  $ret .= "<td class=grassetto>Finitura</td>";
	  		  if ($_REQUEST['stampa_pallet']=='Y') $ret .= "<td class=grassetto>Pallet</td>";	  		  
	  		  $ret .= "<td class=grassetto>Volume</td>";	  		  	  		  
			  $ret .= "<td class=grassetto>Colli</td>";
			  if ($_REQUEST['colli_prod']=='Y') $ret .= "<td class=grassetto>Colli prod.</td>";
			  if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td class=grassetto>Imp.</td>";			  
			  if ($_REQUEST['stampa_gg_rit']=='Y') $ret .= "<td class=grassetto>GG Rit</td>";	
	     $ret .= "</tr>";
	   	     
	     	
 return $ret;	
}


function liv0_intestazione_close($l){

   	$ret  = "<tr class=liv_totale>";
	$ret .= "<td colspan=7 class=number>&nbsp;</td>";
	$ret .= "<td class=\"grassetto number\">[ " . n($l['val']['COUNT'],  0) . " ]</td>";
	$ret .= "<td colspan=5>&nbsp;</td>";
	if ($_REQUEST['stampa_pallet']=='Y') $ret .= "<td class=number>" . n($l['val']['PALLET'], 0) . "</td>";	
	$ret .= "<td class=number>" . n($l['val']['VOLUME'], 3) . "</td>";	
	$ret .= "<td class=number>" . n($l['val']['COLLI'],  0) . "</td>";
	if ($_REQUEST['colli_prod']=='Y') $ret .= "<td class=number>" . n($l['val']['COLLI_PROD'],  0) . "</td>";
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td class=number>" . n($l['val']['IMPORTO'],  0) . "</td>";
	if ($_REQUEST['stampa_gg_rit']=='Y') $ret .= "<td>&nbsp;</td>";	
	
	$ret .= "</tr>";
	$ret .= "</table>";
    

 return $ret;
}

function stampa_dati_vettore($cod_vettore){
	$vt = new Vettori();
	$vt->load_rec_data_by_k(array('TAKEY1' => $cod_vettore));
	return $vt->rec_data['TATELE'];
}

function stampa_dati_targa($targa){
  if (strlen(trim($targa)) > 0){
	return "<span style='margin-left: 90px;'>Targa: " . trim($targa) . "</span>";
  }
  else return "";	

}

 
?>


