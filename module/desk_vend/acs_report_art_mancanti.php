<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$ar_email_to = array();
$ar_email_json = acs_je($ar_email_to);

?>


<html>
 <head>

  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white;}
   
   div#my_content h1{font-size: 22px; padding: 5px;}   
   div#my_content h2{font-size: 18px; padding: 5px; margin-top: 20px;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>

 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'N';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'> 

<?php





//costruzione sql
$ar_seleted_stato = json_decode($_REQUEST['list_selected_stato']);
if (count($ar_seleted_stato) > 0 && strlen($_REQUEST['list_selected_stato']) > 0){
	$m_where = " AND RDRIFE IN (" . sql_t_IN($ar_seleted_stato) . ") ";
} else $m_where = '';


$form_ep = $_REQUEST;
if (strlen(trim($form_ep['f_data_da'])) > 0)
	$m_where .= " AND TDDTEP >= {$form_ep['f_data_da']}";
if (strlen(trim($form_ep['f_data_a'])) > 0)
	$m_where .= " AND TDDTEP <= {$form_ep['f_data_a']}";

if (strlen(trim($form_ep['f_referente'])) > 0)
	$m_where .= " AND TDDOCL = '{$form_ep['f_referente']}'";

if (strlen(trim($form_ep['f_area'])) > 0)
		$m_where .= " AND TA_ITIN.TAASPE = '{$form_ep['f_area']}'";

		
if(strlen(trim($form_ep['f_tipo']))> 0)
    $m_where .= " AND RDTPNO = '{$form_ep['f_tipo']}'";
    
if(trim($form_ep['f_blocco']) == 'A'){
    $m_where .= " AND TDBLEV = 'Y'";
    
}elseif(trim($form_ep['f_blocco']) == 'C'){
    $m_where .= " AND TDBLOC = 'Y'";
    
}elseif(trim($form_ep['f_blocco']) == 'N'){
    $m_where .= " AND TDBLEV <> 'Y' AND TDBLOC <> 'Y'";
}

global $auth, $s;
$js_parameters = $auth->get_module_parameters($s->get_cod_mod());
if ($js_parameters->alert_CF == 1)
	$m_where .= " AND TDDTCF > 0";


	
	
	if($_REQUEST['cliente'] != 'Y'){
	
		$order = "ORDER BY RDDFOR, RDTPNO, RDRIFE, TDDTEP, TDONDO";
	}else{
	
		$order = "ORDER BY TDDCON, TDDTEP, TDONDO, RDDFOR";
	
	}



	$sql = "SELECT RDFORN, RDDFOR, RDTPNO, TDDTEP, TDDTSP, TDCCON, TDDCON, TDOADO, TDONDO, TDOTPD, TDSTAT,
					TDCDES, TDDLOC, TDPROD, TDNAZD, TDDNAD, TDDOCU, TDBLEV, TDBLOC,
				   RDART, RDDES1, RDUM, RDDTDS, RDODER, RDDTEP, RDRIFE, RDDES2, SUM(RDQTA) AS RDQTA
			FROM {$cfg_mod_Spedizioni['file_righe']} 
			 INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
			  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI				 
		WHERE RDFMAN IN('M', 'C') {$m_where}
		GROUP BY RDFORN, RDDFOR, RDTPNO, TDDTEP, TDDTSP, TDCCON, TDDCON, TDOADO, TDONDO, TDOTPD, TDSTAT,
		           TDCDES, TDDLOC, TDPROD, TDNAZD, TDDNAD, TDDOCU, TDBLEV, TDBLOC,
				   RDART, RDDES1, RDUM, RDDTDS, RDODER, RDDTEP, RDRIFE, RDDES2
		{$order} ";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();		
$result = db2_execute($stmt);

$ar = array();
$ar_tot = array();



/***********************************************************************************************
 *  ELENCO 
 ***********************************************************************************************/

if (1 == 1) {

 echo "<h1>Riepilogo segnalazioni su articoli mancanti/critici</h1>";
 
	if($_REQUEST['cliente'] != 'Y'){
	
	  $f_liv1 = "RDFORN"; 			$d_liv1 = "RDDFOR";
	}else{
		
	  $f_liv1 = "TDCCON"; 			$d_liv1 = "TDDCON";
		
	}
	 
 
 $ar_tot["TOTALI"] = array();
 
 while ($r = db2_fetch_assoc($stmt)) {
			
	 	
		//liv1
		if (!isset($d_ar[$r[$f_liv1]]))	
				$d_ar[$r[$f_liv1]] = array("cod" => $r[$f_liv1], "descr"=>$r[$d_liv1], 
																  "val" => array(), 
																  "children"=>array()); 														  
																  																  
		 $tmp_ar = &$d_ar[$r[$f_liv1]];

		 //Per sapere su quante date, articoli.... ho problemi con un certo fornitore
		 $tmp_ar["val"]['TDDTEP'][$r['TDDTEP']] +=1; 
		 $tmp_ar["val"]['TDDTSP'][$r['TDDTSP']] +=1;
		 $tmp_ar["val"]['TDCCON'][$r['TDCCON']] +=1;					
		 $tmp_ar["val"]['TDONDO'][$r['TDONDO']] +=1;	
		 $tmp_ar["val"]['RDFORN'][$r['RDFORN']] +=1;
		 $tmp_ar["val"]['RDART'][$r['RDART']]   +=1;		 
		 $tmp_ar["val"]['RDDTDS'][$r['RDDTDS']] +=1;		 
		 $tmp_ar["val"]['RDODER'][$r['RDODER']] +=1;		 
		 $tmp_ar["val"]['RDDTEP'][$r['RDDTEP']] +=1;		 
		 
		 //AGGIUNGO IL RECORDI IN LINEA
		 $tmp_ar['children'][] = $r;		 
	
 } //while


 if(is_array($d_ar)){
foreach ($d_ar as $kf => $f){
	
	if($_REQUEST['cliente'] != 'Y'){
	
		echo "<h2>Fornitore " . $f['descr'] . "</h2>";
	}else{
	
		if(trim($r['TDNAZD']) == 'IT' || trim($r['TDNAZD']) =='ITA'){
			echo "<h2>Cliente (".trim($r['TDCCON'])."-".trim($r['TDCDES']).") " . $f['descr'] . " - ".trim($r['TDDLOC']) ." [".trim($r['TDPROD'])."]</h2>";
		
		}else{
			echo "<h2>Cliente (".trim($r['TDCCON'])."-".trim($r['TDCDES']).") " . $f['descr'] . " - ".trim($r['TDDLOC']) ." [".trim($r['TDDNAD'])."]</h2>";
		}
	
	}
	
	
	$liv_2_inline = '-----xxxxxx------';
	
	echo "<table>";
	
		echo "<th>Data<br>Prog.<BR> (# " . count($f['val']['TDDTEP']) . ")</th>";
		echo "<th>Data<br>Sped.<BR> (# " . count($f['val']['TDDTSP']) . ")</th>";
		if($_REQUEST['cliente'] != 'Y'){
		
			echo "<th>Cliente<BR> (# " . count($f['val']['TDCCON']) . ")</th>";
		}else{
		
			echo "<th>Fornitore<BR> (# " . count($f['val']['RDFORN']) . ")</th>";
		
		}
		
		echo "<th>Denominazione</th>";		
		echo "<th>Ordine<br>Cliente<BR> (# " . count($f['val']['TDONDO']) . ")</th>";
		echo "<th><img src=" . img_path("icone/48x48/power_black.png") . " height=20></th>";
		echo "<th>Articolo<BR> (# " . count($f['val']['RDART']) . ")</th>";
		echo "<th>Descrizione<BR>&nbsp;</th>";		
		echo "<th>Stato</th>";		
		echo "<th>UM</th>";
		echo "<th>Quantita&grave;</th>";
		echo "<th>Disponib.<BR> (# " . count($f['val']['RDDTDS']) . ")</th>";
		echo "<th>Consegna<BR> (# " . count($f['val']['RDDTEP']) . ")</th>";	
		echo "<th>Documento</th>";	
		echo "<th style='text-align:center'><img src=" . img_path("icone/48x48/unlock.png") . " height=20></th>";
	
	
	foreach ($f['children'] as $r){
	  
	    $autDeroghe = new SpedAutorizzazioniDeroghe();
	    $ha_deroghe	= $autDeroghe->ha_deroghe_by_k_ordine($r['TDDOCU']);
	    
	    //elenco deroghe per tooltip
	    if ($ha_deroghe == 1){
	        $ar_deroghe = array();
	        $array_deroghe = $autDeroghe->array_deroghe_by_k_ordine($r['TDDOCU']);
	        foreach ($array_deroghe as $r_deroga){
	            $ar_deroghe[] = "[" . trim($r_deroga['ADUSRI']) . "] " . print_date($r_deroga['ADDTDE']) . " - " . trim($s->decod_std('AUDE', $r_deroga['ADAUTOR'])) . ": " .  acs_u8e(trim($r_deroga['ADCOMM']));
	        }
	        $tooltip_deroghe = implode("<br>", $ar_deroghe);
	    } else $tooltip_deroghe = '';
	    
	
		if ($liv_2_inline != implode("_", array($r['RDTPNO'], $r['RDRIFE']))){
			echo "<tr class=\"ag_liv2\">";
				echo "<td colspan=15>" . implode(" - ", array($r['RDTPNO'], $r['RDRIFE'])) . "</td>";
			echo "</tr>";
			$liv_2_inline = implode("_", array($r['RDTPNO'], $r['RDRIFE']));
		}
	
		echo "<tr>";
			echo "<td>" . print_date($r['TDDTEP']) . "</td>";
			echo "<td>" . print_date($r['TDDTSP']) . "</td>";
			if($_REQUEST['cliente'] != 'Y'){
			
				echo "<td>" . trim($r['TDCCON']) .  "</td>";
				echo "<td>" . trim($r['TDDCON']) .  "</td>";
			}else{
			
				echo "<td>" . trim($r['RDFORN']) .  "</td>";
				echo "<td>" . trim($r['RDDFOR']) .  "</td>";
			
			}
			
						
			echo "<td nowrap>" . implode('_' , array(trim($r['TDOADO']), trim($r['TDONDO']), trim($r['TDOTPD']))) . " (" . trim($r['TDSTAT']) . ")</td>";
			
			
			$fl_bloc = $s->get_ord_fl_bloc($r);
			if($fl_bloc == 4){
			    echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
			}else if($fl_bloc == 3){
			    echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
			}else if($fl_bloc == 2){
			    echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
			}elseif($fl_bloc == 1){
			    echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
			}else{
			    echo "<td>&nbsp;</td>";
			}
			
			echo "<td>" . trim($r['RDART']) .  "</td>";
			echo "<td>" . trim($r['RDDES1']) .  "</td>";			
			echo "<td>" . trim($r['RDRIFE']) . "</td>";
			echo "<td>" . trim($r['RDUM']) . "</td>";
			echo "<td>" . trim($r['RDQTA']) . "</td>";		
			echo "<td>" . print_date($r['RDDTDS']) . "</td>";
			echo "<td>" . print_date($r['RDDTEP']) . "</td>";
			echo "<td>" . trim($r['RDDES2']) . "</td>";		
			
			if($ha_deroghe == 1){
			    echo "<td>{$tooltip_deroghe}</td>";
	       	}else{
			    echo "<td>&nbsp;</td>";
			}
			
		echo "</tr>";
	}
	echo "</table>";
   } 
}
 
//echo "<pre>"; print_r($d_ar); echo "</pre>";
 
 
} //if elenco

 
 		
?>
 </div>
 </body>
</html>		