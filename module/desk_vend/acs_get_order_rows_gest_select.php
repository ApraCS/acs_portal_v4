<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();

	$raw_post_data = acs_raw_post_data();
	if (strlen($raw_post_data) > 0)
	{	
		$m_params = acs_m_params_json_decode();
		$k_ordine = $m_params->k_ordine;
	}

	if (isset($_REQUEST['k_ordine']))
		$k_ordine = $_REQUEST['k_ordine'];

	$oe = $main_module->k_ordine_td_decode_xx($k_ordine);

	
	
// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();
	
	global $backend_ERP;
	switch ($backend_ERP){

		case 'GL':
			$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_art']} ART
					ON RD.MECAR0 = ART.MACAR0
					WHERE '1 ' = ? AND RD.MECAD0 = ? AND RD.METND0 = ? AND RD.MEAND0 = ? AND RD.MENUD0 = ?";
			
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']));
			
			while ($row = db2_fetch_assoc($stmt)) {
			$row['data_selezionata'] = sprintf("%08s", $dtep);
				$row['data_consegna'] = $row['MEDT20'];
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
					$row['RDART'] = acs_u8e($row['MECAR0']);
				$row['RDDART'] = acs_u8e($row['RDDART']);
			
				$row['RDRIGA'] = $row['MERID0'];
				$row['RDQTA'] = $row['MEQTA0'];
				$row['RDQTE'] = $row['MEQTS0'];
			
				$row['residuo'] = max((int)$row['MEQTA0'] - (int)$row['RDQTE'], 0);
				$row['RDUM'] = $row['MEUNM0'];
				$row['RDDART'] = acs_u8e($row['MADES0']);

				if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
					$row['COMMESSA'] = acs_u8e($row['MECME0']);
				}				
			
				$data[] = $row;
			}
							
		break;
		
		
		default: //SV2
			$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
					AND RD.RDTISR = '' AND RD.RDSRIG = 0";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $oe);
			
			while ($row = db2_fetch_assoc($stmt)) {
				$row['data_selezionata'] = sprintf("%08s", $dtep);
					$row['data_consegna'] = sprintf("%02s", $row['RDAAEP']) . sprintf("%02s", $row['RDMMEP']) . sprintf("%02s", $row['RDGGEP']);
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
					$row['RDART'] = acs_u8e($row['RDART']);
				$row['RDDART'] = acs_u8e($row['RDDART']);
				$row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
				$row['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
				$data[] = $row;
			}
							
		
	} //switch $backend_ERP
	

			//metto prima le righe della data selezionata
				////usort($data, "cmp_by_inDataSelezionata");
			
			$ord_exp = explode('_', $n_ord);
			$anno = $ord_exp[3];
			$ord = $ord_exp[4];			
		  	$ar_ord =  $main_module->get_ordine_by_k_docu($n_ord);

			$m_ord = array(
								'TDONDO' => $ar_ord['TDONDO'],
								'TDOADO' => $ar_ord['TDOADO'],								
								'TDDTDS' => print_date($ar_ord['TDDTDS'])
						   );
			
		echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
		
		$appLog->save_db();		
exit();				
}


?>


{"success": true, "items":
	{
		xtype: 'gridpanel',
		
		stateful: true,
        stateId: 'seleziona-righe-ordini',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
		
  	    store: Ext.create('Ext.data.Store', {
					
					
					groupField: 'RDTPNO',			
					autoLoad:true,
					loadMask: true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&k_ordine=<?php echo $k_ordine; ?>',
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'root'
					        }
						},
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART', 'RDUM', 'RDQTA', 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna', 'data_selezionata', 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO'
	            			, 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR', 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE'
	        			]
	    			}),
	    			
		        columns: [
					{
		                header   : '<br/>&nbsp;Cons.',
		                dataIndex: 'data_consegna', 
		                width: 60,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('inDataSelezionata') == 0) return ''; //non mostro la data se e' quella selezionata																	
						  					return date_from_AS(value);			    
										}		                
		                 
		             },		        
		             {
		                header   : '<br/>&nbsp;Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right'
		             },			        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width     : 110
		             }
		             
		             
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    			    	}}			    	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		        			dataIndex: 'ARESAU',		
    	     				tooltip: 'Articoli in esaurimento',		        			    	     
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARESAU') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
		    			    	}}			    			             
		             
		             
		             , {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150               			                
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             }, {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;S',
		                dataIndex: 'RDSTEV', 
		                width    : 20,
		                align: 'right'
		             }, {
		                header   : '<br/>&nbsp;T.R.',
		                dataIndex: 'RDT	PRI', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Pr.',
		                dataIndex: 'RDPRIO', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;St.',
		                dataIndex: 'RDSTAT', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Carico',
		                dataIndex: 'RDNRCA', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRCA') == 0) return ''; //non mostro																											
						  					return rec.get('RDTICA') + "_" + rec.get('RDNRCA');			    
										}		                
		             }
		             
		             
				<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             , {
		                header   : '<br/>&nbsp;Commessa',
		                dataIndex: 'COMMESSA', 
		                width    : 80
		             }				
		        <?php } else { ?>
		             , {
		                header   : '<br/>&nbsp;Lotto',
		                dataIndex: 'RDNRLO', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRLO') == 0) return ''; //non mostro																											
						  					return rec.get('RDTPLO') + "_" + rec.get('RDNRLO');			    
										}
		             }		        
		        <?php } ?> 
		        
		         ]	    					
		
		, listeners: {		
	 			afterrender: function (comp) {
	 				//comp.up('window').setWidth(950);
	 				//comp.up('window').setHeight(500);
					//window_to_center(comp.up('window'));
					comp.up('window').setTitle('<?php echo "Righe ordine {$oe['TDOADO']}_{$oe['TDONDO']} {$oe['TDPROG']}"; ?>');	 				
	 			},
	 			
				  celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	var t_win = this.up('window');
					  	var rec = iView.getRecord(iRowEl);
					  	t_win.fireEvent('onRowSelected', t_win, rec);
					  }
				  }
				  
				  
				  
				 , itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      var record = grid.getStore().getAt(index);				      		 
				      var voci_menu = [];
				      var t_win = this.up('window');
				      
				
				     voci_menu.push({
				         		text: 'Visualizza distinta componenti',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			
				        		 		my_listeners = {
						            	    onCompoSelected: function(from_win, row_selected){
					        				console.log('abc');
					        				console.log(t_win);
					        				t_win.fireEvent('onCompoSelected', t_win, row_selected);
					        				from_win.close();
				        					}
										            		
										      };
				        		
				        		
				        			acs_show_win_std('Distinta componenti', 'acs_get_riga_componenti.php', {k_ordine: '<?php echo $k_ordine; ?>', nrec: rec.get('RDNREC'), art: rec.get('RDDART')}, 900, 450, my_listeners, 'icon-folder_search-16');          		
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
				      
				 } //itemcontextmenu
	 			 			
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';		        	
		         }   
		    }												    
			
		         
	}
}
