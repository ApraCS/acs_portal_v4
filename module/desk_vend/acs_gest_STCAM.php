<?php
require_once("../../config.inc.php");

$main_module =  new Spedizioni();
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$row = find_TA_std('STCAR', $m_params->TAKEY1, 1, null, null, null, $main_module);
$descrizione = $row[0]['text'];

$m_table_config = array(
    'module'      => $main_module,
    'tab_name'    => $cfg_mod['file_tabelle'],
    'descrizione' => "Stati carico ammessi",
    't_panel' =>  "STCAM - {$m_params->TAKEY1}",
    'form_title' => "Dettagli opzioni [{$m_params->TAKEY1}] {$descrizione}",
    'maximize'    => 'Y',
    'conferma_elimina'    => 'Y',
    'fields_preset' => array(
        'TATAID' => 'STCAM',
        'TAKEY1' => $m_params->open_request->TAKEY1
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
   
    'fields_key' => array('TAKEY1', 'TADESC'),
    
    'fields_grid' => array('TAKEY2', 'TADESC', 'immissione'),
    'fields_form' => array('TAKEY2', 'TADESC'),

    
    
    'fields' => array(
        'TAKEY2' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione', 'fw'=>'flex: 1'),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
   
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione'),
        
    )
);


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
