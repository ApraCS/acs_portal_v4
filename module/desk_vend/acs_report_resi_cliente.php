<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");


	$ar_email_json = acs_je($ar_email_to);

if ($_REQUEST['fn'] == 'get_parametri_form'){
	
?>

{"success" : true, "items":
	
		{
	        xtype: 'form',
	        layout: 'form',
	        collapsible: false,
	        id: 'my_form',
	        url: '<?php echo $_SERVER['PHP_SELF']; ?>',
	        frame: false,
	        bodyPadding: '15 15 15 15',
	        width: 300,
	        fieldDefaults: {
	            msgTarget: 'side',
	            labelWidth: 75,
	    		format: 'd/m/Y',
	    		submitFormat: 'Ymd'	            
	        },
	        defaultType: 'datefield',
	        items: [ {
						flex: 1,
						name: 'area_spedizione',
						xtype: 'combo',
						fieldLabel: 'Area spedizione',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection:true,
					   	allowBlank: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
							    ] 
						}						 
				  	},{
						flex: 1,
						name: 'itinerario_spedizione',
						xtype: 'combo',
						fieldLabel: 'Itinerario spedizione',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection:true,
					   	allowBlank: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
							    ] 
						}						  
					},{
						flex: 1,
						name: 'agente',
						xtype: 'combo',
						fieldLabel: 'Agente',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection:true,
					   	allowBlank: true,													
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json($s->get_options_agenti('Y'), '') ?> 	
							    ] 
						}						  
					},{
						flex: 1,
			            xtype: 'combo',
						name: 'f_cliente_cod',
						fieldLabel: 'Cliente',
						minChars: 2,			
            
			            store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_get_select_json.php?select=search_cli_anag',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['cod', 'descr'],		             	
			            },
                        
						valueField: 'cod',                        
			            displayField: 'descr',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
				        listeners: {
				            change: function(field,newVal) {	            	
				            	var form = this.up('form').getForm(); 	            	
				            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
				            	form.findField('f_destinazione_cod').store.load();	            	
				            }
				        },
            
            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun cliente trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{descr}</span></h3>' +
			                        '{cod}' +
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000
        		},{
        			xtype: 'radiogroup',
                    layout: 'hbox',
                    fieldLabel: 'Dettaglio',
                    flex: 1,
                    allowBlank: false,
                    margin: "20 10 10 10",
                    items: [{
	                            xtype: 'radio'
	                          , name: 'rb_anche_dettaglio_articoli' 
	                          , boxLabel: 'Si'
	                          , inputValue: 'Y'
	                          , width: 40                          
	                        },
	                        {
	                            xtype: 'radio'
	                          , name: 'rb_anche_dettaglio_articoli' 
	                          , boxLabel: 'No'
	                          , inputValue: 'N'
	                          , checked: true					                          
	                          , width: 40					                                                    
	                        }
                   ]
        		}],

	        buttons: [{
		            text: 'Report <br/> Resi',
		            iconCls: 'icon-print-32',
		            scale: 'large',	                     
		            handler: function() {
		                form = this.up('form').getForm();
		                
		                if (form.isValid()){	                	                
			                form.submit({
					                standardSubmit: true,
			                        method: 'POST',
									target : '_blank'		                        
			                });
		                }	                
		            }
		        },{
		        	text: 'Report  <br/> anzianit&aacute',
		            iconCls: 'icon-print-32',
		            scale: 'large',	                     
		            handler: function() {
		                form = this.up('form').getForm();
		                if (form.isValid()){	                	                
			                form.submit({
					                url: 'acs_report_anzianita_resi.php',
		                        	params: {form_ep: JSON.stringify(form.getValues())},
		                            standardSubmit: true,
                                    submitEmptyText: false,
		                            method: 'POST',
		                            target : '_blank'	                        
			                });
		                }	                
		            }
		        
		        },{
		        	text: 'Visualizza',
		            iconCls: 'icon-folder_search-32',
		            scale: 'large',	                     
		            handler: function() {
		                form = this.up('form').getForm();
		                
		                if (form.isValid()){	                	                
			               acs_show_win_std('Grafico resi','acs_resi_grafici.php', {form_values: form.getValues()}, 600, 400, {}, 'icon-grafici-16', 'Y'); 
		                }	                
		            }
		        }
	        ]
	    }
	
		
	}



<?php 
	exit;
	}
?>


<html>
<head>
	<title>RIEPILOGO RESI CLIENTE DA RITIRARE</title>
	<!-- <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/report.css"); ?> />  -->
	
	<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

	<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
 	<script type="text/javascript" src="../../../extjs/ext-all.js"></script>
	<script src=<?php echo acs_url("js/acs_js_report.js") ?>></script>  
	
	<style type="text/css">
	
	table{border-collapse:collapse; width: 100%;}
	table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
	
	.acs_report{font-family: "Times New Roman",Georgia,Serif;}
	
	tr.liv1 td{background-color: #cccccc; font-weight: bold;}
	tr.liv3 td{font-size: 9px;}
	
	tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
	tr.ag_liv2 td{background-color: #DDDDDD;}
	tr.ag_liv1 td{background-color: #ffffff;}
	tr.ag_liv0 td{font-weight: bold;}
	
	tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
	tr.ag_liv_data th{background-color: #333333; color: white;}
	
	.grassetto{font-weight: bold;}   
	.number{text-align: right;}
	
	h2.acs_report{background-color: black; color: white; padding: 5px; font-size: 20px;}
	h2.acs_report_title{font-size: 20px; padding: 10px;}
	
	@media print
	{
		.noPrint{display:none;}
	}
	
	
	</style>
	</head>
<body>


	<div class="page-utility noPrint">
		<?php 
		
					$bt_fascetta_print = 'Y';
					$bt_fascetta_email = 'Y';
					$bt_fascetta_excel = 'Y';
					$bt_fascetta_close = 'Y';
					include  "../../templates/bottoni_fascetta.php";
		
		?>	
	</div>
	
	<div id='my_content'>
		
		<h2 class="acs_report_title">RIEPILOGO RESI CLIENTE DA RITIRARE </h2>
		<p align="right"> Data elaborazione:  <?php echo date('d/m/Y  H:i');?></p>
		
		<?php  	
		
			function add_array_valori_liv(&$ar, $r){
				$ar["IMPORTO"] 		+= $r['RDQTA2']; 
				$ar["N_RIGHE"] += 1;
			}
			
			function make_tr($n_td,  $array_value, $tr_class = " "){
				
				$ar_class = array();
				$ar_class[4] = $ar_class[5] = $ar_class[9] = $ar_class[12] = 'number';
				
				echo "<tr class=$tr_class>";
				for ($i=0; $i<$n_td; $i++){
					echo "<td class=\"{$ar_class[$i]}\">". $array_value[$i] ."</td>";
				}
				echo "</tr>";
			}
			
		

		/**************** INTERROGAZIONE DB E CHECK FILTRI   *********************************************************************************/
			
		
			$field = "TDASPE, TDCITI, TDCCON, TDDCON, TDLOCA, TDCDES, TDDOCU, 
					RDONDO, RDDES1, RDDES2, RDART, RDUM, RDQTA, RDQTA2, SUBSTRING(RDDES2,16,25) AS CAUSA_NC, 
	        		SUBSTRING(RDDES2,56,2) AS N_TENTATIVI, SUBSTRING(RDDES2,62,10) AS DATA_SOLLECITO, RDODER AS DATA_RESO,
					TA_ASPE.TADESC AS D_SPE, TA_ITIN.TADESC AS D_ITIN";      
			
			$m_where = "TDOTID = 'VO' AND TDOINU = '' AND TDOADO = 0 AND TDSWPP = 'R'";
			$m_where .= sql_where_by_combo_value("TDASPE", $_REQUEST['area_spedizione']);
			$m_where .= sql_where_by_combo_value("TDCCON", $_REQUEST['f_cliente_cod']);
			$m_where .= sql_where_by_combo_value("TDCITI", $_REQUEST['itinerario_spedizione']);
			$m_where .= sql_where_by_combo_value("TDCAG1", $_REQUEST['agente']);
			
			$sql = "SELECT $field
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD  
					ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO 
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
							ON TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TD.TDASPE
							LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
								ON TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI			
			WHERE  $m_where
			ORDER BY TD.TDASPE, TA_ITIN.TASITI, TD.TDDCON, TD.TDCDES";
				
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
			
			
			
		/**************** CREAZIONE ALBERO   *********************************************************************************/		
			$ar = array();
			while ($r = db2_fetch_assoc($stmt)){
				
				$liv0 = "TOTALE";
				$liv1 = implode("_", array(trim($r['TDASPE'])));
				$liv2 = implode("_", array(trim($r['TDCITI'])));
				$liv3 = implode("_", array(trim($r['TDCCON']), trim($r['TDCDES']))); // COD CLIENTE _ COD DESTINAZIONE
				
				// ----LIV0
				$d_ar = &$ar;
				$c_liv = $liv0;
				if (!isset($d_ar[$c_liv]))
					$d_ar[$c_liv] = array(
							"DESCRIZIONE"  => 'TOTALE GENERALE',
							"children"=>array());
				add_array_valori_liv($d_ar[$c_liv], $r);
				$d_ar = &$d_ar[$c_liv]['children'];
				
				// --- LIV1
				$c_liv = $liv1;
				if (!isset($d_ar[$c_liv]))
					$d_ar[$c_liv] = array(
							"COD_AREA_SPEDIZIONE"  => $r['TDASPE'],
							"DESCRIZOINE_AREA_SPEDIZIONE" => $r['D_SPE'],
							"children"=>array());
				add_array_valori_liv($d_ar[$c_liv], $r);
				$d_ar = &$d_ar[$c_liv]['children'];
				
				// --- LIV2
				$c_liv = $liv2;
				if (!isset($d_ar[$c_liv]))
					$d_ar[$c_liv] = array(
							"COD_ITINERARIO" => $r['TDCITI'],
							"DESCRIZIONE_ITINERARIO" => $r['D_ITIN'],
							"children"=>array());
				add_array_valori_liv($d_ar[$c_liv], $r);
				$d_ar = &$d_ar[$c_liv]['children'];
				
				// --- LIV3
				$c_liv = $liv3;
				if (!isset($d_ar[$c_liv]))
					$d_ar[$c_liv] = array(
							"COD_CLIENTE" => $r['TDCCON'],
							"RAGIONE_SOCIALE" => $r['TDDCON'],
							"LOCALITA" => $r['TDLOCA'],
							"COD_DESTINAZIONE" => $r['TDCDES'],
							"children"=>array());
				add_array_valori_liv($d_ar[$c_liv], $r);
				
				if ($_REQUEST['rb_anche_dettaglio_articoli'] == 'Y'){
					// DETTAGLIO
					array_push($d_ar[$c_liv]['children'], array(
							'IMPORTO' => $r['RDQTA2'],
							'COD_ARTICOLO' => $r['RDART'],
							'DESC_ARTICOLO' => $r['RDDES1'],
							'UNITA_MISURA' => $r['RDUM'],
							'QUANTITA' => $r['RDQTA'],
							'CAUSA_NC' => trim($r['CAUSA_NC']),
							'N_ TENTATIVI' => trim($r['N_TENTATIVI']),
							'DATA_RESO' => trim($r['DATA_RESO']),
							'DATA_SOLLECITO' => trim($r['DATA_SOLLECITO']),
					
					));
				}
			}
			
			/*
			echo "<pre>"; 
			print_r($ar); 
			exit;
			*/
			


		/**************** STAMPA REPORT   *********************************************************************************/			
			
			echo "<table class=\"acs_report\">";
			echo "<tr class=\"ag_liv_data\">";
			echo "<th>Cod</th> <th>Descrizione</th> <th>Codice destinazione</th> <th>Localit&agrave;</th> 
	         			<th>Importo</th> <th>Nr. righe</th>";
			$nr_tr = 6;
			if ($_REQUEST['rb_anche_dettaglio_articoli'] == 'Y'){
				$nr_tr = 14;
				echo "<th>Cod. articolo</th> <th>Desc articolo</th> <th>UM</th> <th>Qt&agrave;</th> 
	                      <th>Causa NC</th> <th>Data reso</th> <th>Nr. tentativi</th><th>Al</th>";
			}
			echo "</tr>";
			
			// TOTALE GENERALE
			make_tr($nr_tr,
					array("", "TOTALE GENERALE", "", "", n($ar['TOTALE']['IMPORTO']), $ar['TOTALE']['N_RIGHE']),
					'liv1');
			
			if(count($ar['TOTALE']['children']) != 0){
				//AREA SPEDIZIONE
				foreach ($ar['TOTALE']['children'] as $a_sped){
					make_tr($nr_tr,
						array($a_sped['COD_AREA_SPEDIZIONE'],  $a_sped['DESCRIZOINE_AREA_SPEDIZIONE'], "", "",
								n($a_sped['IMPORTO']),  $a_sped['N_RIGHE']),'ag_liv3');
						
					//ITINERARIO SPEDIZIONE
					foreach ($a_sped['children'] as $it_sped){	
						make_tr($nr_tr,
								array($it_sped['COD_ITINERARIO'], $it_sped['DESCRIZIONE_ITINERARIO'], "", "",
										n($it_sped['IMPORTO']), $it_sped['N_RIGHE']),
								'ag_liv2'
								);
							
						//CLIENTE - LOCALITA'
						foreach ($it_sped['children'] as $cli_loc){
							make_tr( $nr_tr,
									array($cli_loc['COD_CLIENTE'], $cli_loc['RAGIONE_SOCIALE'], $cli_loc['COD_DESTINAZIONE'],$cli_loc['LOCALITA'],
											n($cli_loc['IMPORTO']), $cli_loc['N_RIGHE']),
									$_REQUEST['rb_anche_dettaglio_articoli'] == 'Y' ? 'ag_liv0' : '');

							if ($_REQUEST['rb_anche_dettaglio_articoli'] == 'Y'){
								//ARTICOLO DEL RESO
								foreach ($cli_loc['children'] as $articolo){
									make_tr($nr_tr,
											array("", "", "", "", n($articolo['IMPORTO']), "1", $articolo['COD_ARTICOLO'],
													$articolo['DESC_ARTICOLO'], $articolo['UNITA_MISURA'], $articolo['QUANTITA'],
													$articolo['CAUSA_NC'], print_date($articolo['DATA_RESO'], "%d/%m/%Y"), $articolo['N_ TENTATIVI'], $articolo['DATA_SOLLECITO']),
											'');
								}
							}

						}
					}
				}
				
			} //fine if
					
			
			
			echo "</table>";

		?>


	</div>

</body>