<?php


require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();
$cfg_mod = $main_module->get_cfg_mod();
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// EXE - modifica o duplica riga
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_proposta'){
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'CONF_PROPOSTA',
            "k_ordine"	=> $m_params->k_ordine,
            "vals" => array(
                'RICITI' => 'Y'
            )
        )
        );
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// EXE - modifica o duplica riga
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_cancella_riga'){
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'DEL_RIGA_PRO',
            "k_ordine"	=> $m_params->k_ordine,
            "vals" => array(
                "RINRCA" 	=> (int)$m_params->rec->RDNREC,
                "RIDTVA"    => (int)$m_params->rec->RDRIGA,
                "RIART" 	=> $m_params->rec->RDART,
                "RIDART" 	=> $m_params->rec->RDDART,
            )
            )
        );
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// EXE - modifica o duplica riga
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_riga'){
    
    if($m_params->duplica == 'Y')
        $rirges = "DUP_RIGA_PRO";
    else 
        $rirges = "MOD_RIGA_PRO";
            
        
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> $rirges,
            "k_ordine"	=> $m_params->k_ordine,
            "vals" => array(
                "RINRCA"    => (int)$m_params->rec->RDNREC,
                "RIDTVA"    => (int)$m_params->rec->RDRIGA,
                "RIART" 	=> $m_params->form_values->f_articolo,
                "RIQTA"     => sql_f($m_params->form_values->f_qta),
                "RIDTEP"    => $m_params->form_values->f_data
            )
        )
        );
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'open_form'){
    
    if($m_params->duplica == 'Y')
        $title = 'Duplica riga';
    else 
        $title = 'Modifica riga';
    
    ?>
{"success":true, 

m_win: {
		width: 350,
		height: 200,
		iconCls: 'icon-pencil-16',
		title: <?php echo j($title) ?>,
		
	},
"items": [    
    {
    	xtype: 'form',
    	bodyStyle: 'padding: 10px',
    	bodyPadding: '5 5 0',
    	frame: true,
    	title: '',    
    	defaults:{ anchor: '-10'},    
    	
    	items: [
    		{
    			name: 'f_qta',						
    			xtype: 'numberfield',
    			fieldLabel: 'Quantit&agrave;',
    		    value: '<?php echo $m_params->rec->RDQTA; ?>',
				flex: 1,
				hideTrigger:true,
				decimalPrecision:4,					
    		} ,
    		
    		{
			     name: 'f_data'             		
			   , xtype: 'datefield'
			   , startDay: 1 //lun.
			   , fieldLabel: 'Data consegna'
			   , format: 'd/m/Y'
			   , submitFormat: 'Ymd'
			   , allowBlank: true
			   , flex: 1
			   , value: '<?php echo print_date($m_params->rec->data_consegna, "%d/%m/%Y") ?>' 
			   , listeners: {
			       invalid: function (field, msg) {
        			       Ext.Msg.alert('', msg);}
        		 }
        	},
        	  {
				flex: 1,
	            xtype: 'combo',
				name: 'f_articolo',
				fieldLabel: 'Articolo',
				value: '<?php echo trim($m_params->rec->RDART); ?>',
				anchor: '-15',
				minChars: 2,			
				store: {
	            	pageSize: 1000,            	
					proxy: {
			            type: 'ajax',
			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_articoli',
			            actionMethods: {
				          read: 'POST'
				        },
			            extraParams: {
		    				},				            
				        doRequest: personalizza_extraParams_to_jsonData, 
			            reader: {
			                type: 'json',
			                root: 'root',
			                method: 'POST',	
			                totalProperty: 'totalCount'
			            }
			        },       
						fields: [{name:'id'}, {name:'text'}],            	
	            },
              
				valueField: 'id',                        
	            displayField: 'text',
	            typeAhead: false,
	            hideTrigger: true,
	            listConfig: {
	                loadingText: 'Searching...',
	                emptyText: 'Nessun articolo trovato',
        
	                // Custom rendering template for each item
	                getInnerTpl: function() {
	                    return '<div class="search-item">' +
	                        '<h3><span>{text}</span></h3>' +
	                        '[{id}] {text}' + 
	                    '</div>';
	                	}                
                
            	},
    
    			pageSize: 1000,
        		}   				
    	],
    	
    	buttons: [ '->',
    			  {
    	            text: 'Conferma',
    	            iconCls: 'icon-save-32', scale: 'large',
    	            handler: function() {
    	            	var form = this.up('form').getForm();
    	            	var form_values = form.getValues();
    	            	var loc_win = this.up('window');
    	            	
    	            	<?php if($m_params->duplica == 'Y'){?>
    	            	
        	            	if(form_values.f_qta > <?php echo $m_params->rec->RDQTA ?>){
        	            	    acs_show_msg_error('Impossibile duplicare <br> La quantit&agrave; deve essere minore della quantit&agrave; di riga');
                                return;
        	            	}
    	            	<?php }?>
    
    					if(form.isValid()){
    					    Ext.getBody().mask('Loading... ', 'loading').show();
    						Ext.Ajax.request({
    						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_riga',
    						        jsonData: {
    						        	form_values: form.getValues(),
    						        	rec: <?php echo acs_je($m_params->rec); ?>,
    						        	k_ordine : <?php echo j($m_params->k_ordine)?>,
    						        	duplica : <?php echo j($m_params->duplica); ?>   						        	
    						        },
    						        method     : 'POST',
    						        waitMsg    : 'Data loading',
    						        success : function(result, request){	
    						            Ext.getBody().unmask();            	  													        
    						            var jsonData = Ext.decode(result.responseText);
    						            if (jsonData.success == true){
    						            	loc_win.fireEvent('onCloseSuccess', loc_win);
    						            } else {
    						            	Ext.Msg.alert('Message', 'Error');
    						            }	
    						        },
    						        failure    : function(result, request){
    						            Ext.getBody().unmask();
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
    						    });	    
    				    }            	                	                
    	            }
    	        }]              
    				
            } //form
    ]}
<?php exit; } 

if ($_REQUEST['fn'] == 'get_json_data_articoli'){
    
    global $backend_ERP;
    
    if ($backend_ERP == 'GL'){
        $sql = "SELECT MACAR0 AS ARART, MADES0 AS ARDART
        FROM {$cfg_mod_Admin['file_anag_art']} AR
        WHERE '1 ' = '{$id_ditta_default}'
        AND UPPER(MACAR0) LIKE '%" . strtoupper($_REQUEST['query']) . "%'
            ORDER BY MACAR0";
        
    }else{
        
        if(isset($m_params->sottogruppo) && strlen($m_params->sottogruppo) > 0)
            $sql_where = " AND ARSGME = '{$m_params->sottogruppo}'";
        
        $sql = "SELECT ARART, ARDART
        FROM {$cfg_mod_Admin['file_anag_art']} AR
        WHERE ARDT = '{$id_ditta_default}' {$sql_where}
        AND (UPPER(ARART) LIKE '%" . strtoupper($_REQUEST['query']) . "%' OR UPPER(ARDART) LIKE '%" . strtoupper($_REQUEST['query']) . "%')
            ORDER BY ARART";
        
    }
        
      
 
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $text = "[" . trim($row['ARART']) . "] " . trim($row['ARDART']);
        
        $ret[] = array( "id" 	=> trim($row['ARART']),
            "text" 	=> acs_u8e($text) );
    }


    
    echo acs_je($ret);
    exit;
}