<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();
$m_params = acs_m_params_json_decode();

function sum_columns_value(&$ar_r, $r){
    
    $ar_r['C_LOTTO1']  += $r['LOTTO1'];
    $ar_r['COLLI_T'] += $r['COLLI_T_R'];
    $ar_r['COLLI_D'] += $r['COLLI_D_R'];
    $ar_r['COLLI_S'] += $r['COLLI_S_R'];
    if($ar_r['liv'] == 'liv_1' || $ar_r['liv'] == 'liv_2'){
        $ar_r['diff_val'] = $ar_r['COLLI_T'] - $ar_r['capac'];
        if($ar_r['capac'] != 0)
            $ar_r['perc'] = $ar_r['diff_val']/$ar_r['capac'] * 100;
    }
    
    if($r['TEMPO_S'] > 0)
        $ar_r['tempo_s'] += $r['TEMPO_S']/3600;
      
}

if ($_REQUEST['fn'] == 'exe_salva'){
    
    
    foreach($m_params->form_values as $k => $v){
        
        if(substr($k, 0, 3) == 'cap'){
            
            $ar_val = explode("_", $k);
            $data = $ar_val[1];
           
        
            $sql_cp = "SELECT COUNT(*) AS C_ROW
            FROM {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP
            WHERE CPDATA = {$data} AND CP.CPTPCO = '{$m_params->gruppo}' AND CP.CPDT='{$id_ditta_default}'
            ";
            
            $stmt_cp = db2_prepare($conn, $sql_cp);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_cp);
            $row = db2_fetch_assoc($stmt_cp);
            if($row['C_ROW'] > 0){
                
                $ar_upd = array();
                $ar_upd['CPCAPA'] 	= $v;
                
                
                $sql = "UPDATE {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP
                SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE CPDT='{$id_ditta_default}' AND CPDATA = {$data} AND CPTPCO = '{$m_params->gruppo}'";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_upd);
                
                
            }else{
                
                $ar_ins = array();
                $ar_ins['CPDT']     = $id_ditta_default;
                $ar_ins['CPDATA'] 	= $data;
                $ar_ins['CPRIFE'] 	= 0;
                $ar_ins['CPAREA'] 	= '';
                $ar_ins['CPTPCO'] 	= $m_params->gruppo;
                $ar_ins['CPCAPA'] 	= $v;
                
                $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_capacita_produttiva']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                
                
            }
            
            
        }
    }
        
        
 
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


// ******************************************************************************************
// DATI PER GRID AVANZAMENTO LOTTO (COLLI)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_avanzamento_lotto'){
    
    //$m_params = $m_params->open_request;
    $open_request = $m_params->open_request;
    $form_values = $open_request->form_values;
    $da_data  = $open_request->da_data;
    $col_name = $open_request->col_name;

    if (strlen($da_data) > 0){    
        $add_day = (int)substr($col_name, 2, 2) - 1;
        $m_data = date('Ymd', strtotime($da_data . " +{$add_day} days"));
        
        $sql_data = "SELECT CSAARG, CSNRSE, CSGIOR FROM {$cfg_mod_Spedizioni['file_calendario']} CS
                     WHERE CSDT='{$id_ditta_default}' AND CSCALE ='*CS' AND CSDTRG = {$m_data}";
        
        $stmt_data = db2_prepare($conn, $sql_data);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_data);
        $row_data = db2_fetch_assoc($stmt_data);
        
        $day = $row_data['CSGIOR'];
        $week = $row_data['CSNRSE'];
        $year = $row_data['CSAARG'];
        
        $sql_WHERE_week = " AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year} ";
    }
    
    if (isset($form_values->area_spedizione) && $form_values->area_spedizione != '')
        $sql_WHERE .= " AND TA_ITIN.TAASPE = '{$form_values->area_spedizione}' ";
    if (isset($form_values->divisione) || $form_values->divisione != '')
        $sql_WHERE .= " AND TDCDIV = '{$form_values->divisione}' ";
    if (isset($form_values->num_carico) && strlen($form_values->num_carico) > 0)
        $sql_WHERE .= " AND TDNRCA ='{$form_values->num_carico}'";
    if (isset($form_values->num_lotto) && strlen($form_values->num_lotto) > 0)
        $sql_WHERE .= " AND TDNRLO ='{$form_values->num_lotto}'";
                
    if ($form_values->carico_assegnato == "Y")
        $sql_WHERE .= " AND TDNRCA > 0 ";
    if ($form_values->carico_assegnato == "N")
        $sql_WHERE .= " AND TDNRCA = 0 ";
    if ($form_values->lotto_assegnato == "Y")
        $sql_WHERE .= " AND TDNRLO > 0 ";
    if ($form_values->anomalie_evasione == "Y")
        $sql_WHERE .= " AND TDOPUN = 'N' ";
    if ($form_values->lotto_assegnato == "N")
        $sql_WHERE .= " AND TDNRLO = 0 ";
    
    if (isset($open_request->filtra_gruppo))
        $sql_WHERE .= " AND  TA_LIPC.TACINT = '{$open_request->filtra_gruppo}'";
    
    if (isset($open_request->open_request->filtro_type)){
        $filtro_type = $open_request->open_request->filtro_type;
        $filtro_c    = $open_request->open_request->filtro_c;
        switch ($filtro_type) {
            case 'LOTTO':
                //ricevo LP_2016_92601_ (in fondo opzionalmente trovo la ditta origine)
                $ar_filtro_c = explode('_' , $filtro_c);
                $sql_WHERE .= " AND TDTPLO = " . sql_t($ar_filtro_c[0]);
                $sql_WHERE .= " AND TDAALO = " . $ar_filtro_c[1];
                $sql_WHERE .= " AND TDNRLO = " . $ar_filtro_c[2];
                //if (count($ar_filtro_c) == 4)
                //    $sql_WHERE .= " AND TDDTOR = " . sql_t($ar_filtro_c[3]);                
                break;
            case 'CARICO':
                //ricevo CA_2016_92601_ (in fondo opzionalmente trovo la ditta origine)
                $ar_filtro_c = explode('_' , $filtro_c);
                $sql_WHERE .= " AND TDTPCA = " . sql_t($ar_filtro_c[0]);
                $sql_WHERE .= " AND TDAACA = " . $ar_filtro_c[1];
                $sql_WHERE .= " AND TDNRCA = " . $ar_filtro_c[2];
                if (count($ar_filtro_c) == 4)
                    $sql_WHERE .= " AND TDDTOR = " . sql_t($ar_filtro_c[3]);
                    
                break;
        }
    }
        
         
    $sql_fs = array('TDDTEP,
                    SUBSTR(TA_PUOP.TAREST, 141, 15) AS LIV_GEN,
                    TA_LIPC.TACINT AS LIV_DET, PLFAS10 AS LINEA', 'TA_LIPC.TADESC AS D_LINEA', 'SUBSTR(TA_LIP2.TAREST, 220, 10) AS TEMPO_S');
    $sql_gb = array('TDDTEP, SUBSTR(TA_PUOP.TAREST, 141, 15), TA_LIPC.TACINT, PLFAS10, TA_LIPC.TADESC', 'SUBSTR(TA_LIP2.TAREST, 220, 10)');
    
        
    $sql_ff = array(
        "SUM(CASE WHEN PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_T_R",
        "SUM(CASE WHEN PLDTEN>0 AND PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_D_R",
        "SUM(CASE WHEN PLDTUS>0 AND PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_S_R"
    );
    
    
    if ($cfg_mod_Spedizioni['disabilita_digits_on_PL0'] == 'Y')
      $sql_join_on_PL0_PLNRDO = "PLNRDO";
    else
      $sql_join_on_PL0_PLNRDO = "digits(PLNRDO)";
          
    
     
        
    if (isset($open_request->open_request) && $open_request->open_request->no_produzione == 'Y')
        $sql_where_produzione = '';
    else{
        if($cfg_mod_Spedizioni['linea_produzione'] != 'N')
            $sql_where_produzione = " AND SUBSTR(TA_PUOP.TAREST, 141, 15) = 'PRODUZIONE' ";
    }
        
    if ($form_values->lotto1 == 'Y'){
        $sql_l1 = ", SUM(LOTTO1) AS LOTTO1";
        $j_lotto1 = "  LEFT OUTER JOIN (
                            SELECT COUNT(*) AS LOTTO1, REDT, RETPCO, REAACO, RENRCO, RETPSV, REPROG, RECOLC
                            FROM {$cfg_mod_DeskArt['file_ricom']} 
                            WHERE REDT = '{$id_ditta_default}' AND RETIDE = 'L'
                            GROUP BY REDT, RETPCO, REAACO, RENRCO, RETPSV, REPROG, RECOLC
                            ) RE
                        ON PLDT=REDT AND PLTPCO=RETPCO AND PLAACO=REAACO AND PLNRCO=RENRCO AND PLTPSV=RETPSV
                        AND CASE WHEN PLRCOL <> '' THEN REPROG ELSE RECOLC END = PLNCOL";
        
    }
            
        
    $sql = "SELECT " . implode(",", array_merge($sql_fs, $sql_ff)) . " {$sql_l1}
            FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
            INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
                ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
            INNER JOIN {$cfg_mod_Spedizioni['file_colli']} PL
                    ON PLDT = TD.TDDT
				      AND PLTIDO = TD.TDOTID
			          AND PLINUM = TD.TDOINU
				      AND PLAADO = TD.TDOADO
				      AND PLNRDO = SUBSTR(TD.TDONDO, 1, 6)
            /* per soli colli PRODUZIONE */       
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_PUOP
                ON  TA_PUOP.TADT = PL.PLDT
                AND TA_PUOP.TAID = 'PUOP'
                AND TA_PUOP.TANR = PL.PLOPE2
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIPC
	  	  	    ON PL.PLDT = TA_LIPC.TADT AND TA_LIPC.TAID = 'LIPC' AND TA_LIPC.TANR = PL.PLFAS10 
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIP2
	  	  	    ON PL.PLDT = TA_LIP2.TADT AND TA_LIP2.TAID = 'LIP2' AND TA_LIP2.TANR = PL.PLFAS10                     
            INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL
				ON TDDT = CAL.CSDT AND TD.TDDTEP = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
         
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
           {$j_lotto1}
            WHERE " . $s->get_where_std() . " AND TDSWSP='Y'
			{$sql_WHERE_week}
            {$sql_where_produzione}
			{$sql_WHERE}
			GROUP BY " . implode(",", $sql_gb) . " 
			ORDER BY " . implode(",", $sql_gb) . "
			";			
	
			
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    
    //creo l'albero
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)){
      
        //controllare perch� non tornano i totali come report e gestire la capacit�
       
        //stacco dei livelli
        $cod_liv0 = trim($row['TDDTEP']);
        $cod_liv1 = trim($row['LIV_GEN']);
        $cod_liv2 = trim($row['LIV_DET']);
        $cod_liv3 = trim($row['LINEA']);
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        //SETTIMANA
        $liv =$cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = ucfirst(print_date($row['TDDTEP'], "%A %d/%m"));
            $ar_new['expanded'] = true;
            $ar_new['liv'] = 'liv_0';
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //PRODUZIONE
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = $liv;
            $cp = new SpedCapacitaProduttiva;
            $ar_new['capac'] =  $cp->get_tot_by_data($row['TDDTEP'], $liv);
            $ar_new['data'] =  trim($row['TDDTEP']);
            $ar_new['expanded'] = true;
            $ar_new['liv'] = 'liv_1';
            $ar_r["{$liv}"] = $ar_new;
        }
        
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //GRUPPO
        $liv=$cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            if(trim($liv) == '')
                $ar_new['task'] = 'NON ASSEGNATO';
            else
                $ar_new['task'] = $liv;
           
            $cp = new SpedCapacitaProduttiva;
            $ar_new['capac'] =  $cp->get_tot_by_data($row['TDDTEP'], $liv); 
            $ar_new['data'] =  trim($row['TDDTEP']);
            
         $sql_cs = "SELECT CSDTRG FROM {$cfg_mod_Spedizioni['file_calendario']} CAL
                    WHERE CSDT = '{$id_ditta_default}' AND CSDTRG < {$ar_new['data']}
                    AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
                    AND CSTPGG = 'L'
                    ORDER BY CSDTRG DESC
                    LIMIT 3";
                    
            
            $stmt_cs = db2_prepare($conn, $sql_cs);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_cs);
            echo db2_stmt_errormsg($stmt_cs);
            
            $i = 1;
            while ($r = db2_fetch_assoc($stmt_cs)){
                $ar_new["data_{$i}"] = $r['CSDTRG'];
                $i++;
            }
            
                   
            
            $ar_new['expanded'] = false;
            $ar_new['liv'] = 'liv_2';
            $ar_r["{$liv}"] = $ar_new;
        }
        
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //LINEE
        $liv=$cod_liv3;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = $liv;
            $ar_new['desc'] = trim($row['D_LINEA']);
           
            $ar_new['data'] =  trim($row['TDDTEP']);
            $ar_new['liv'] = 'liv_3';
            $ar_new['leaf'] = true;
            $ar_r["{$liv}"] = $ar_new;
        }
        
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
    }
    
  
    
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array('success' => true, 'children' => $ret));
    exit;
}


// ******************************************************************************************
// GRID PER AVANZAMENTO COLLI LOTTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_avanzamento_lotto_grid'){
    
    
    if (strlen($m_params->da_data) > 0){
    
        $add_day = (int)substr($m_params->col_name, 2, 2) - 1;
        $m_data = date('Ymd', strtotime($m_params->da_data . " +{$add_day} days"));
        
        $sql_data = "SELECT CSAARG, CSNRSE, CSGIOR FROM {$cfg_mod_Spedizioni['file_calendario']} CS
                     WHERE CSDT='{$id_ditta_default}' AND CSCALE ='*CS' AND CSDTRG = {$m_data}";
        
        $stmt_data = db2_prepare($conn, $sql_data);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_data);
        $row_data = db2_fetch_assoc($stmt_data);
        
        $day = $row_data['CSGIOR'];
        $week = $row_data['CSNRSE'];
        $year = $row_data['CSAARG'];
    }    
?>
{"success":true, "items":
	{
	    xtype: 'treepanel' ,
		flex: 1,
        useArrows: true,
        rootVisible: false,
        loadMask: true,
	    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['desc', 'tempo_s', 'data', 'liv', 'task', 'capac', 'diff_val', 'perc', 
                    'LIV_GEN', 'LIV_DET', 'RIFE', 'LINEA', 'D_LINEA', 'data_1', 'data_2', 'data_3',
                    {name: 'non_scaricati', type: 'float'}, {name: 'COLLI_T', type: 'float'}, 
                    {name: 'COLLI_D', type: 'float'}, {name: 'COLLI_S', type: 'float'}, 'C_LOTTO1'],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_avanzamento_lotto',
						actionMethods: {read: 'POST'},
						extraParams: {
        				    open_request : <?php echo acs_je($m_params); ?>
        			    },
                       doRequest: personalizza_extraParams_to_jsonData  ,     
						//Add these two properties
					      actionMethods: {
					          read: 'POST'
					      },
						
						reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'children'						            
				        }       				
                    }

                }),
	   columns: [
			{xtype: 'treecolumn', 
    		text: 'Centro/Linee', 	
    		flex: 1,
    		dataIndex: 'task',
    		renderer: function (value, metaData, record, row, col, store, gridView){
            	if (record.get('liv') == 'liv_3')
            		return "[" + value + "] " + record.get('desc');
            	else
            		return value; 
            }
    	    },
		   {header: 'Capacit�', dataIndex: 'capac', width: 90, renderer: floatRenderer0, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer0(value); 
	        }},	
	        
	          {header: 'Diff. in valore', dataIndex: 'diff_val', width: 100, 
	          summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	             return floatRenderer0(value); 
	           },
	           renderer: function (value, metaData, record, row, col, store, gridView){
	             if (record.get('liv') != 'liv_0'){
			     if(record.get('perc') >= 5)
				 	metaData.tdCls += ' tpSfondoRosa';
				 else if(record.get('perc') >= -5)
				 	metaData.tdCls += ' tpSfondoGiallo';
				 else
				 	metaData.tdCls += ' tpSfondoVerde';
				 	
  			 	 return floatRenderer0(value);
				}else{
				   return floatRenderer0(value);
				}
			  
			  }
	        
	        },
	        
	          {header: '%', dataIndex: 'perc', width: 50, 
	          summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer1(value); 
	        }, renderer: function (value, metaData, record){
	             if (record.get('liv') != 'liv_0'){
			     if(record.get('perc') >= 5)
				 	metaData.tdCls += ' tpSfondoRosa';
				 else if(record.get('perc') >= -5)
				 	metaData.tdCls += ' tpSfondoGiallo';
				 else
				 	metaData.tdCls += ' tpSfondoVerde';
				 	
				 	 return floatRenderer1(value);
				}else{
				    return floatRenderer1(value);
				}
			   
			  }},				
						
			{header: 'Colli totale', dataIndex: 'COLLI_T', width: 90, align: 'right',
				summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
		               return floatRenderer0(value); 
				},
				renderer: function(value, metaData, record){
				    if(record.get('liv') == 'liv_1' || record.get('liv') == 'liv_2'){
				    	diff = record.get('COLLI_T') - record.get('capac');
				    	perc = diff/record.get('capac') * 100;
						q_tip = diff + ' , '+ floatRenderer1(perc) + '%';
		      			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
		    	 		return floatRenderer0(value);			
		    	 	}else{	
		    	 		return floatRenderer0(value);	
		    	 	}				    	 			    	
		    	}					        
					        
			},
	        {header: 'Colli disponibili', dataIndex: 'COLLI_D', width: 90, align: 'right', renderer: floatRenderer0,
 				summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer0(value); 
	        }}, 
	        {header: 'Colli spediti', dataIndex: 'COLLI_S', width: 90, align: 'right', renderer: floatRenderer0,
 				summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer0(value); 
	        }},
	        {header: 'Tempo stimato', dataIndex: 'tempo_s', width: 100, renderer: floatRenderer3, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer3(value); 
	        }},
	       <?php if($m_params->form_values->lotto1 == 'Y'){?> 
	          {header: 'Lotto1', dataIndex: 'C_LOTTO1', width: 70, align: 'right', renderer: floatRenderer0}
	        <?php }?>
	        
			
		],
		
		
		listeners: {
		
				beforeload: function(store, options) {
    		     var t_win = this.up('window');
                    Ext.getBody().mask('Loading... ', 'loading').show();
                    },
    
                load: function () {
                 Ext.getBody().unmask(); 
                },  
		
		    afterrender: function (comp) {
		    	<?php  if (isset($week)) { ?>
 					comp.up('window').setTitle(comp.up('window').title + '<?php echo " - Settimana {$week}"; ?>');
 				<?php } ?>
 			},
		    itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     var m_grid = this;
					     if(rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3'){	
					     
					            ar_params = {};
    							ar_params['filtro'] = rec.data;
    							//ar_params['filtro']['col_name'] = col_name;
    							ar_params['filtro']['da_data'] = rec.get('data');
    							ar_params['filtro']['gruppo'] = rec.get('task');
    							ar_params['filtro']['liv'] = rec.get('liv');
    							
					     
			    		 voci_menu.push({
			         		text: 'Visualizza situazione al gg-1/mm',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		   	ar_params['filtro']['c_data'] = rec.get('data_1');
			        		   acs_show_win_std('Visualizza ordini situazione al gg-1/mm ' + date_from_AS(rec.get('data_1')), 'acs_riepilogo_colli_ordini.php?fn=open_tab', Ext.encode(ar_params), 1200, 500, null, 'icon-leaf-16');
					  	
				                }
			    		});	
			    		
			    		voci_menu.push({
			         		text: 'Visualizza situazione al gg-2/mm',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		   	ar_params['filtro']['c_data'] = rec.get('data_2');
    					     acs_show_win_std('Visualizza ordini situazione al gg-2/mm '+ date_from_AS(rec.get('data_2')), 'acs_riepilogo_colli_ordini.php?fn=open_tab', Ext.encode(ar_params), 1200, 500, null, 'icon-leaf-16');
					  	
				                }
			    		});	
			    		
			    		voci_menu.push({
			         		text: 'Visualizza situazione al gg-3/mm',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		   	ar_params['filtro']['c_data'] = rec.get('data_3');
    					     acs_show_win_std('Visualizza ordini situazione al gg-3/mm '+ date_from_AS(rec.get('data_3')), 'acs_riepilogo_colli_ordini.php?fn=open_tab', Ext.encode(ar_params), 1200, 500, null, 'icon-leaf-16');
					  	
				                }
			    		});		
			    	 }
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			   },	
				celldblclick: {								

					fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	iEvent.preventDefault();
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;	
					  		
					  	if(rec.get('liv') == 'liv_1' || rec.get('liv') == 'liv_2'){							
					  		if(col_name == 'capac'){
					  	      var my_listeners = {
    			    		  			afterOkSave: function(from_win){
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					     acs_show_win_std('Modifica capacit&agrave; - ' + rec.get('task'), 'acs_riepilogo_colli_linea_prod.php?fn=open_form', {da_data : rec.get('data'), gruppo : rec.get('task'), capa : rec.get('capac'), year : '<?php echo $year; ?>', week : '<?php echo $week; ?>'}, 350, 300, my_listeners, 'icon-pencil-16');
					  	
					  		}
					  	}
					  	
					  	if(rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3'){
					  	    if(col_name != 'capac'){
					  	        ar_params = {};
    							ar_params['filtro'] = rec.data;
    							ar_params['filtro']['col_name'] = col_name;
    							ar_params['filtro']['da_data'] = rec.get('data');
    							ar_params['filtro']['gruppo'] = rec.get('task');
    							ar_params['filtro']['liv'] = rec.get('liv');
    							ar_params['filtro']['carico_assegnato'] = <?php echo j($m_params->form_values->carico_assegnato) ?>;
    							ar_params['filtro']['lotto_assegnato'] = <?php echo j($m_params->form_values->lotto_assegnato) ?>;
    							ar_params['filtro']['lotto1'] = <?php echo j($m_params->form_values->lotto1) ?>;
    							ar_params['filtro']['open_request'] = <?php echo acs_je($m_params->open_request) ?>;
    
    							acs_show_win_std('Elenco corrente colli', '../desk_vend/acs_json_elenco_colli.php?fn=get_json_grid', Ext.encode(ar_params), 900, 500, null, 'icon-box_open-16')
					  	    }
					  	}	
					  		
					}
				}				
		}, 	 viewConfig: {
				 toggleOnDblClick: false,
		         getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           
 		           return v;																
		         }   
		    }
		
				 
	}

}	


<?php
	exit;
}


if ($_REQUEST['fn'] == 'open_form'){
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	            
	            	{
					    xtype: 'fieldcontainer',
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						<?php 
						$year = $m_params->year;
						$week = $m_params->week;
						$sql = "SELECT CSDTRG, CSAARG, CSGIOR 
                                FROM {$cfg_mod_Spedizioni['file_calendario']} CS
					            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP
                                ON CS.CSDT=CP.CPDT AND CS.CSDTRG = CP.CPDATA AND CPTPCO = '{$m_params->gruppo}'
                                AND CPAREA = '{$m_params->area}'
                                WHERE CSDT='{$id_ditta_default}' AND CSCALE ='*CS' 
						        AND CSNRSE = {$week} AND CSAARG = {$year} AND CSTPGG NOT IN ('S', 'D')";
						
						$stmt = db2_prepare($conn, $sql);
						echo db2_stmt_errormsg();
						$result = db2_execute($stmt);
						
						
						$sql_s = "SELECT CPCAPA
		                          FROM {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP2		 
		                          WHERE CPDT ='{$id_ditta_default}' AND CPTPCO = '{$m_params->gruppo}'
                                  AND CPDATA = ? AND CPRIFE = ?";
						
						
						
						$stmt_s = db2_prepare($conn, $sql_s);
						echo db2_stmt_errormsg();
						
						while($row = db2_fetch_assoc($stmt)){
						  						
						  $label = ucfirst(print_date($row['CSDTRG'], "%A %d/%m"));
						  $cp = new SpedCapacitaProduttiva;
						  $capac = $cp->get_tot_by_data($row['CSDTRG'], $m_params->gruppo);
						  $result = db2_execute($stmt_s, array($row['CSAARG'], $row['CSGIOR']));
						  $row_s = db2_fetch_assoc($stmt_s);
						  $capac_std = "<font color='red'> / Standard : ".$row_s['CPCAPA'] . "</font>";
						  
						  
						?>
						
						   {
					    xtype: 'fieldcontainer',
					    flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    
						    {
        					name: 'cap_<?php echo $row['CSDTRG']; ?>',
        					xtype: 'numberfield',
        					width : 180, 
        					hideTrigger : true,
        					fieldLabel: '<?php echo $label; ?>',
        					value : <?php echo j($capac)?>
        					},
        					
        					{
        					name: 'f_std',
        					margin : '0 0 0 5',
        					xtype: 'displayfield',
        					value : <?php echo j($capac_std)?>
        					}
						
						]},
						   
						    
        					
        				<?php }?>
						 ]}
	            ],
	            
				buttons: [	
				{
			            text: 'Conferma',
				        scale: 'large',	                     
						iconCls: 'icon-button_blue_play-32',	            
			            handler: function() {
			                 var form = this.up('form');
			                 var form_values = form.getValues();
			                 var loc_win = this.up('window');
			                 Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_salva',
								        method     : 'POST',
					        			jsonData: {
					        				data : <?php echo j($m_params->da_data);?>,
					        				gruppo : <?php echo j($m_params->gruppo)?>,
					        				form_values : form_values
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          loc_win.fireEvent('afterOkSave', loc_win);
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}
