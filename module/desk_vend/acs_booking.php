<?php

require_once "../../config.inc.php";
require_once("acs_booking_include.php");


$main_module = new Spedizioni();
$s = new Spedizioni();



if ($_REQUEST['fn'] == 'exe_test_invio_email'){
  $m_params = acs_m_params_json_decode();
  send_email_conferma($m_params->sped_id, $m_params->bstime);
  exit;
}


if ($_REQUEST['fn'] == 'exe_modifica_stato'){


	$m_params = acs_m_params_json_decode();

	$list_bstime = $m_params->list_bstime;

	$ar_ins = array();
	if(strlen($m_params->causale) > 0)
		$ar_ins['BSCAUT'] 	= $m_params->causale;
	$ar_ins['BSSTAT'] 	= $m_params->new_stato;
	$ar_ins['BSUSAL']   = trim($auth->get_user());
	$ar_ins['BSDTAL']   = oggi_AS_date();
	$ar_ins['BSORAL']   = oggi_AS_time();

	foreach($list_bstime as $v){

		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSTIME= '{$v->bstime}' ";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		
		//cambio lo stato della spedizione
		$br = bk_get_row($v->bstime);
		
		try {
		    write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Assegna stato [{$m_params->new_stato}] {$conf_stato_booking[$m_params->new_stato]}");
		} catch(Exception $e){
		    $mail = prepare_new_mail();
		    $mail->addAddress('s.cultrera@apracomputersystem.it');
		    $mail->Subject = 'Prove try and catch';
		    $body = "l'invio dell'email non � andato a buon fine";
		    $mail->Body = $body;
		    $mail->send();
		   //echo printf($e->getMessage());
		}
		
		
		
		if ($m_params->new_stato == '02'){
			cambia_stato_spedizione($br['BSNBOC'], 'PI');
			try {
			    send_email_conferma($br['BSNBOC'], $bstime);
			} catch(Exception $e){
			    //nothing - ToDo
			    //write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Esito invio email negativo con errore {$e->getMessage()}");
			}
		} else {
			cambia_stato_spedizione($br['BSNBOC'], 'DP');
			
			if ($m_params->new_stato == '04'){ //respinta
			    try {
			        send_email_respinta($br['BSNBOC'], $bstime);
			    } catch(ErrorException $e){
			        //nothing - ToDo
			    }
			}

		}

	}


	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;

}

if ($_REQUEST['fn'] == 'exe_conferma_log'){

	$ret_rows = array();
	$m_params = acs_m_params_json_decode();
	$list_bstime = $m_params->list_bstime;
	
	$ar_ins = array();
	$ar_ins['BSFLAL'] 	= 'Y';
	$ar_ins['BSUSAL']   = trim($auth->get_user());
	$ar_ins['BSDTAL']   = oggi_AS_date();
	$ar_ins['BSORAL']   = oggi_AS_time();
	
	foreach($list_bstime as $v){
		
		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSTIME= '{$v->bstime}' ";				
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		
		$br = bk_get_row($v->bstime);
		write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Confermata dalla logistica");
		
		set_conferma($v->bstime);
		$br = bk_get_row($v->bstime);
		$ret_rows[$br['BSTIME']] = bk_out_row($br);
	}
	
	$ret = array();
	$ret['success'] = true;
	$ret['rows'] = $ret_rows;
	echo acs_je($ret);
	exit;

}

if ($_REQUEST['fn'] == 'exe_resp_log'){
	$ret_rows = array();
	$m_params = acs_m_params_json_decode();
	$list_bstime = $m_params->list_bstime;

	$ar_ins = array();
	$ar_ins['BSFLAL'] 	= 'N';
	$ar_ins['BSUSAL']   = trim($auth->get_user());
	$ar_ins['BSDTAL']   = oggi_AS_date();
	$ar_ins['BSORAL']   = oggi_AS_time();

	foreach($list_bstime as $v){

		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSTIME= '{$v->bstime}' ";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);

		$br = bk_get_row($v->bstime);
		write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Respinta dalla logistica");
		
		set_respinta($v->bstime);	
		$br = bk_get_row($v->bstime);
		$ret_rows[$br['BSTIME']] = bk_out_row($br);
		
	}


	$ret = array();
	$ret['success'] = true;
	$ret['rows'] = $ret_rows;
	echo acs_je($ret);
	exit;

}


if ($_REQUEST['fn'] == 'exe_annulla_log'){
	$ret_rows = array();
	$m_params = acs_m_params_json_decode();
	$list_bstime = $m_params->list_bstime;

	$ar_ins = array();
	$ar_ins['BSFLAL'] 	= '';
	$ar_ins['BSUSAL']   = trim($auth->get_user());
	$ar_ins['BSDTAL']   = oggi_AS_date();
	$ar_ins['BSORAL']   = oggi_AS_time();

	foreach($list_bstime as $v){

		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSTIME= '{$v->bstime}' ";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		
		$br = bk_get_row($v->bstime);
		write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Annullata dalla logistica");
		
		set_annulla($v->bstime);
		$br = bk_get_row($v->bstime);
		$ret_rows[$br['BSTIME']] = bk_out_row($br);		
	}

	$ret = array();
	$ret['success'] = true;
	$ret['rows'] = $ret_rows;
	echo acs_je($ret);
	exit;

}

if ($_REQUEST['fn'] == 'exe_conferma_amm'){
	$ret_rows = array();
	$m_params = acs_m_params_json_decode();
	$list_bstime = $m_params->list_bstime;

	$ar_ins = array();
	$ar_ins['BSFLAA'] 	= 'Y';
	$ar_ins['BSUSAA']   = trim($auth->get_user());
	$ar_ins['BSDTAA']   = oggi_AS_date();
	$ar_ins['BSORAA']   = oggi_AS_time();

	foreach($list_bstime as $v){

		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSTIME= '{$v->bstime}' ";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		
		$br = bk_get_row($v->bstime);
		write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Confermata dall'amministrazione");
		
		set_conferma($v->bstime);
		$br = bk_get_row($v->bstime);
		$ret_rows[$br['BSTIME']] = bk_out_row($br);
	}

	$ret = array();
	$ret['success'] = true;
	$ret['rows'] = $ret_rows;
	echo acs_je($ret);
	exit;

}


if ($_REQUEST['fn'] == 'exe_resp_amm'){
	$ret_rows = array();
	$m_params = acs_m_params_json_decode();

	$list_bstime = $m_params->list_bstime;

	$ar_ins = array();
	$ar_ins['BSFLAA'] 	= 'N';
	$ar_ins['BSUSAA']   = trim($auth->get_user());
	$ar_ins['BSDTAA']   = oggi_AS_date();
	$ar_ins['BSORAA']   = oggi_AS_time();

	foreach($list_bstime as $v){

		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
		SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
		WHERE BSTIME= '{$v->bstime}' ";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);

		$br = bk_get_row($v->bstime);
		write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Respinta dall'amministrazione");
		
		set_respinta($v->bstime);
		$br = bk_get_row($v->bstime);
		$ret_rows[$br['BSTIME']] = bk_out_row($br);

	}

	$ret = array();
	$ret['success'] = true;
	$ret['rows'] = $ret_rows;
	echo acs_je($ret);
	exit;

}

if ($_REQUEST['fn'] == 'exe_annulla_amm'){
	$ret_rows = array();
	$m_params = acs_m_params_json_decode();
	$list_bstime = $m_params->list_bstime;

	$ar_ins = array();
	$ar_ins['BSFLAA'] 	= '';
	$ar_ins['BSUSAA']   = trim($auth->get_user());
	$ar_ins['BSDTAA']   = oggi_AS_date();
	$ar_ins['BSORAA']   = oggi_AS_time();

	foreach($list_bstime as $v){

		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSTIME= '{$v->bstime}' ";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);

		$br = bk_get_row($v->bstime);
		write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Annullata dall'amministrazione");
		
		set_annulla($v->bstime);		
		$br = bk_get_row($v->bstime);
		$ret_rows[$br['BSTIME']] = bk_out_row($br);
		
	}

	$ret = array();
	$ret['success'] = true;
	$ret['rows'] = $ret_rows;
	echo acs_je($ret);
	exit;

}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
	
	auto_accettazione_amministrativa();
	auto_evasione_confermate_con_ordini_evasi();
	auto_chiusura_scadute_e_invio_email();
	
	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;
	$k_ordine = $m_params->open_request->k_ordine;
	
	if(isset($k_ordine)){
	    $row_ord  = $s->get_ordine_by_k_ordine($k_ordine);
	    $sped = trim($row_ord['TDNBOF']);
	    $sql_where .= sql_where_by_combo_value('BSNBOC', $sped);
	}else{
	    $sql_where .= sql_where_by_combo_value('BSNBOC', $form_values->f_sped);
	}
	    
	
	
	$sql_where .= sql_where_by_combo_value('BSSTAT', $form_values->f_stato);
	$sql_where .= sql_where_by_combo_value('BSCITI', $form_values->f_itin);
	
	if($form_values->f_scaduti == 'N'){ //esclusi scaduti
		$sql_where .= " AND BSDTSR >= ".oggi_AS_date();
	}
	if($form_values->f_scaduti == 'Y'){ //solo scaduti
		$sql_where .= " AND BSDTSR < ".oggi_AS_date();
	}
	
	if(strlen($form_values->f_data_da) > 0){
	    $sql_where .= " AND BSDTGE >= ".$form_values->f_data_da . " ";
	}
	
	if(strlen($form_values->f_data_a) > 0){
	    $sql_where .= " AND BSDTGE <= ".$form_values->f_data_a . " ";
	}

	$sql = "SELECT BS.*, CFRGS1
			FROM {$cfg_mod_Booking['file_richieste']} BS
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF 
				ON BSDT = CF.CFDT AND CFTICF = 'C' AND CFFLG3 = '' AND BSCCON=CFCD
			WHERE BSDT='$id_ditta_default' $sql_where
            ORDER BY BSDTGE DESC, BSORGE DESC
            limit 100";
	



	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$data = array();
	
	
	while ($row = db2_fetch_assoc($stmt)) {
		$ar = array();
		
	    $nboc=$row['BSNBOC'];
	    $row['id'] = $row['BSTIME'];
		$row['nr_sped'] = acs_u8e(trim($row['BSNBOC']));
		$row['data'] = acs_u8e(trim($row['BSDTGE']));
		$row['ora'] = sprintf("%06s", $row['BSORGE']);
		$row['data_ora'] = $row['BSDTGE'].sprintf("%06s", $row['BSORGE']);
		$row['data_ora_out'] = print_date($row['BSDTGE'])." ".print_ora($row['BSORGE']);
		$row['utente_generazione'] = acs_u8e(trim($row['BSUSGE']));
		$row['cliente'] =acs_u8e(trim($row['CFRGS1']))." [". acs_u8e(trim($row['BSCCON']))."]";
		$row['itin'] = $s->decod_std('ITIN', $row['BSCITI']);
		$row['stato'] = acs_u8e(trim($row['BSSTAT']));
		$row['stato_desc'] = "[" .acs_u8e(trim($row['BSSTAT']))."] ".$conf_stato_booking[$row['BSSTAT']];
		$row['bstime'] = acs_u8e(trim($row['BSTIME']));
		$row['f_log'] = acs_u8e(trim($row['BSFLAL']));
		$row['f_log_data'] = acs_u8e(trim($row['BSDTAL']));
		$row['f_log_data_rit'] = acs_u8e(trim($row['BSDTSR']));
		$row['f_log_ora'] = acs_u8e(trim($row['BSORAL']));
		$row['f_log_utente'] = acs_u8e(trim($row['BSUSAL']));
		$row['f_amm'] = acs_u8e(trim($row['BSFLAA']));
		$row['f_amm_data'] = acs_u8e(trim($row['BSDTAA']));
		$row['f_amm_ora'] = acs_u8e(trim($row['BSORAA']));
		$row['f_amm_utente'] = acs_u8e(trim($row['BSUSAA']));
		$row['volume'] = acs_u8e(trim($row['BSVOLU']));
		$row['colli'] = acs_u8e(trim($row['BSTOCO']));
		
		
    	$sql_1 = "SELECT TDTPCA, TDAACA, TDNRCA, PSFG01, PSFG02, MIN(TDAUX1) AS MIN_TDAUX1
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
		WHERE TDNBOF='$nboc'
		GROUP BY TDTPCA, TDAACA, TDNRCA, PSFG01, PSFG02
		ORDER BY PSFG02, TDTPCA, TDAACA, TDNRCA";
    	
    	

		$stmt_1 = db2_prepare($conn, $sql_1);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt_1);
		while ($row1 = db2_fetch_assoc($stmt_1)) {
			$anno_numero=implode("_", array($row1['TDAACA'], $row1['TDNRCA']));
			
			//se non ho lo stato di pagamento sl carico prendo il minimo tipo documento (TDAUX1)
			if (strlen(trim($row1['PSFG01'])) > 0)
			 $ar[$anno_numero] = $row1['PSFG01'];
			else
			 $ar[$anno_numero] = "T{$row1['MIN_TDAUX1']}";
		}		
		
		//$ar = $s->get_el_carichi_by_sped($nboc, 'AN', 'N', 'N', null, 'Y', 'Y');				

		$row['carico_out'] = componi_carico_out($ar);
		$row['flag_pagamento'] = componi_flag_pagamento($ar);
	
		if (strlen(trim($row['carico_out'])) == 0)
			$row['carico_out'] = '[' . trim($row['BSELCA']) . ']';
		
		//print_r($row['flag_pagamento']);

		$data[] = $row;
	}

	echo acs_je($data);
	
	exit();
	
}

if ($_REQUEST['fn'] == 'open_form'){


	?>
{"success":true, "items": [

        {
            xtype: 'form',
            frame: true,
            title: '',
          	bodyStyle: 'padding: 10px',
            bodyPadding: '5 2 0',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			    
			defaults: {
				margin: '5 5 5 5'
			},
            
            items: [
            
						{							
							name: 'f_stato',
							xtype: 'combo',
							fieldLabel: 'Stato booking',
							value: ['01', '02'],
							anchor: '-15',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: <?php echo acs_ar_to_json($conf_stato_booking); ?>
								    
								}						 
							},
							
				{							
							name: 'f_itin',
							xtype: 'combo',
							fieldLabel: 'Itinerario',
							value: '01',
							anchor: '-15',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							 	data: [								    
								    <?php echo acs_ar_to_select_json(get_options_itin(), '') ?>
								    ] 
								    
								}						 
							},
							
							{
								name: 'f_sped',
								xtype: 'textfield',
								fieldLabel: 'Spedizione',
								value: '',
							    anchor: '-150px',
							    width: '150'
							 },
							 
					//Filtri per data		 
					, {
				        xtype: 'fieldcontainer',
				        fieldLabel: '',
						defaultType: 'textfield',
	 			        layout: 'hbox',
	 			        items: [
	 			        	<?php echo_je(extjs_datefield('f_data_da', 'Da data', null, array('allowBlank' => true, 'flex' => 1))) ?>,
	 			        	<?php echo_je(extjs_datefield('f_data_a',  'A data',  null, array('allowBlank' => true, 'flex' => 1, 'labelAlign' => 'right'))) ?>
	 			        ]	 			     
					},
							 
							
							
							
					{
							name: 'f_scaduti',
							xtype: 'radiogroup',
							fieldLabel: 'Scaduti',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "20 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_scaduti' 
		                          , boxLabel: 'Inclusi'
		                          , inputValue: 'T'
		                          , width: 50
								  , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_scaduti' 
		                          , boxLabel: 'Esclusi'
		                          , inputValue: 'N'                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_scaduti' 
		                          , boxLabel: 'Solo'	
		                          , inputValue: 'Y'
		                          
		                        }]
						}							

            ] ,
            
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "FILTRI_BOOKING"); ?> 
			{
	            text: 'History',
	            iconCls: 'icon-blog-32',
	            scale: 'large',	            
	              handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_win_std('Interroga cronologia booking', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_history', {form_values: form.getValues()}, 300, 200, null, 'icon-blog-16');
							//this.up('window').close();
			
			            }
	        },
			{
	            text: 'Visualizza',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',	            
	              handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_grid', 'panel_stato_ordini', {form_values: form.getValues()});  //recupero i valori del form per passarli nel tree
							this.up('window').close();
			
			            }
	        }]            
				
        }
]}

<?php 

}

if ($_REQUEST['fn'] == 'open_grid'){
	
	$m_params = acs_m_params_json_decode();
	
	
	?>
	
	{"success":true, "items": [

			 
				{
		     xtype: 'grid',
			 multiSelect: true,
			 title: 'Booking',
	         loadMask: true,	
	          <?php echo make_tab_closable(); ?>,
             tbar: new Ext.Toolbar({
	            items:[ 
	            
	            {iconCls: 'icon-calendar-16'}, '<b>Elenco prenotazioni ritiro merce</b> ' , '->',
	           
	           	<?php if ($auth->is_admin()) { ?> 
	           
	            {
	             iconCls: 'icon-gear-16',
	             tooltip: 'Aggiungi indice di classificazione',
	             handler: function(event, toolEl, panel){
	             
	                    	acs_show_win_std('Permessi utente per modulo (funzioni : B_ACC_AMM, B_ACC_LOG, B_CHG_STS)', 'acs_permessi_utente_booking.php?fn=open_tab', {}, 980, 500, null, 'icon-gear-16');
			          
		           		 
		           		 }
		           	 },
		         <?php }?>  	 
		           	 
	              {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),
             store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
						            root: 'root'
						        }
							},
							
		        			fields: [
		            			'nr_sped', 'cliente', 'itin', 'stato_desc', 'f_log', 'f_log_data', 'f_log_data_rit', 'f_log_ora', 'f_log_utente',
		            			'f_amm', 'f_amm_data', 'f_amm_ora', 'f_amm_utente', 'data', 'data_ora_out', 'bstime', 'carico_out',
		            			'flag_pagamento', 'stato', 'volume', 'colli', 'utente_generazione', 'data_ora', 'ora'
		            			 
		        			]						
									
			}, //store
			
		     <?php $cf1 = "<img src=" . img_path("icone/128x128/euro_green.png") . " height=25>"; ?>
		     <?php $cf2 = "<img src=" . img_path("icone/16x16/check_green.png") . " style= padding-top:5px >"; ?>
				

			      columns: [
			             {
			                header   : 'Ut. gen.',
			                tooltip: 'Utente di generazione',
			                dataIndex: 'utente_generazione', 
			                width: 70
			             }
			              ,{ 
            					dataIndex: 'data_ora',
            					width: 90,
            					align: 'right',
           						header: 'Data/Ora',
           						renderer: function(value, p, record){
 								  return datetime_from_AS(record.get('data'), record.get('ora'), 'd/m/y');
 								  }
                         },{
			                header   : 'Stato',
			                dataIndex: 'stato_desc', 
			                width: 100,
			                renderer: function(value, metaData, record){

								if (record.get('stato') == '02' )
									metaData.tdCls += ' sfondo_verde';		
									
								if (record.get('stato') == '03' || record.get('stato') == '04')
									metaData.tdCls += ' sfondo_rosso';	

								if (record.get('stato') == '05')
									metaData.tdCls += ' sfondo_grigio';	
						
						
								return value;	
							}
			             },
			             {
			                header   : 'Sped.',
			                dataIndex: 'nr_sped', 
			                width: 60, align: 'right'
			             },{
			                header   : 'Itinerario',
			                dataIndex: 'itin', 
			                width: 80
			             },			        
			             {
			                header   : 'Cliente',
			                dataIndex: 'cliente', 
			                 width: 200
			             },{
			                header   : 'Carico',
			                dataIndex: 'carico_out', 
			                 width: 100,
			                 renderer: function(value, metaData, record){
									metaData.tdCls += ' auto-height';							
														
								return value;	
							}
			             },{
			                text: '<?php echo $cf1; ?>',
			                width     : 30,
			                tooltip: 'Pagamento',
			                tdCls: 'tdAction',         			
                            menuDisabled: true, sortable: false,  
			                dataIndex: 'flag_pagamento', 
			                renderer: function(value, p, record){
			                	
			                	if(record.get('flag_pagamento')=='C') return '<img src=<?php echo img_path("icone/128x128/euro_green.png") ?> width=25>';
			                	
			                	if(record.get('flag_pagamento')=='P') return '<img src=<?php echo img_path("icone/128x128/euro_yellow.png") ?> width=25>';
								
								if(record.get('flag_pagamento')=='N') return '<img src=<?php echo img_path("icone/128x128/euro_red.png") ?> width=25>';
						
								if(record.get('flag_pagamento')=='B') return '<img src=<?php echo img_path("icone/128x128/currency_blue_euro.png") ?> width=25>';
				
								
			                	}  
			             },{
			                header   : 'Vol.',
			                dataIndex: 'volume', 
			                width: 40,
			                align: 'right',
			                renderer: floatRenderer2
			             },{
			                header   : 'Colli',
			                dataIndex: 'colli', 
			                 width: 40,
			                 align: 'right',
			                renderer: floatRenderer0
			             },{
			                header   : 'Data ritiro',
			                dataIndex: 'f_log_data_rit', 
			                width: 70,
			                renderer: function(value, metaData, record){
				                 	<!-- segnalo i confermati non ancora evasi  -->
									if (record.get('stato') == '02' && value == <?php echo oggi_AS_date() ?>)
										metaData.tdCls += ' sfondo_giallo grassetto';								
									if (record.get('stato') == '02' && value < <?php echo oggi_AS_date() ?>)
										metaData.tdCls += ' sfondo_rosso grassetto';								
									return date_from_AS(value);	
								}
			             },{
			             	header: 'Accettazione logistica',
                    			columns: [
                    			
                      		{header: '<?php echo $cf2; ?>', dataIndex: 'f_log', width: 50, align: 'center',
                      		  renderer: function(value, p, record){
			                	
			                	if(record.get('f_log')=='Y') return '<img src=<?php echo img_path("icone/16x16/check_green.png") ?> >';
			                	
			                	if(record.get('f_log')=='N') return '<img src=<?php echo img_path("icone/16x16/divieto.png") ?>>';
			                	
			                	if(record.get('f_log')=='') return '<img src=<?php echo img_path("icone/16x16/help_blue.png") ?>>';
						
			                	}
                      		
                      		},
                     		{header: 'Data', dataIndex: 'f_log_data', width: 70, renderer: date_from_AS},
                     		{header: 'Ora', dataIndex: 'f_log_ora', width: 50, renderer: time_from_AS},
                     		{header: 'Utente', dataIndex: 'f_log_utente', width: 70}
                  				]
                 		 },{
			             	header: 'Accettazione amministrativa',
                    			columns: [
                      		{header: '<?php echo $cf2; ?>', dataIndex: 'f_amm', width: 50, align: 'center',
                      		 renderer: function(value, p, record){
			                	
			                	if(record.get('f_amm')=='Y') return '<img src=<?php echo img_path("icone/16x16/check_green.png") ?> >';
			                	
			                	if(record.get('f_amm')=='N') return '<img src=<?php echo img_path("icone/16x16/divieto.png") ?>>';
						
			                	if(record.get('f_amm')=='') return '<img src=<?php echo img_path("icone/16x16/help_blue.png") ?>>';
			                	}
                      		},
                     		{header: 'Data', dataIndex: 'f_amm_data', width: 70, renderer: date_from_AS},
                     		{header: 'Ora', dataIndex: 'f_amm_ora', width: 50, renderer: time_from_AS},
                     		{header: 'Utente', dataIndex: 'f_amm_utente', width: 70}
                  				]
                 		 }
			             
			            
			         ]
			         
			         , listeners: {		
	 				
			 		itemcontextmenu : function(grid, rec, node, index, event) {
			 		<?php 
			 		
			 		$js_parameters =  $auth->get_module_parameters($main_module->get_cod_mod());
		  		    $list_user_function = $auth->list_module_function_permission($main_module->get_cod_mod(), null, 'UTMOB');
		  	
				  	 ?>
		  		
		  		event.stopEvent();
		  	
			     var xPos = event.getXY()[0];
			    var cols = grid.getGridColumns();
			    var colSelected = null;
			   
			    for(var c in cols) {
			
			        var leftEdge = cols[c].getPosition()[0];
			        var rightEdge = cols[c].getSize().width + leftEdge;
			
			        if(xPos>=leftEdge && xPos<=rightEdge) {
			            colSelected = cols[c];
			        }
			    }		  
				  var voci_menu = [];
				  
				  id_selected = grid.getSelectionModel().getSelection();
			  	list_selected_id = [];
			  	for (var i=0; i<id_selected.length; i++) 
				   list_selected_id.push({bstime : id_selected[i].get('bstime')});
				   
			 if(colSelected.dataIndex == 'nr_sped') { 
				   
				    voci_menu.push({
	          		text: 'Modifica spedizione',
	        		iconCls : 'iconSpedizione',	          		
	        		handler: function() {
	        			var my_listeners = {
	        					acsaftersave: function(){
	        							grid.getStore().load();
					        		}
			    				};												
											
	    			  	acs_show_win_std('Modifica spedizione', 'acs_form_json_spedizione.php', 
	    			  		{		
	    			  		 sped_id: rec.get('nr_sped'),						    			  		 
	    			  		 from_sped_id: 'Y',
	    			  		 bstime: rec.get('bstime')
	    			  		}, 600, 550, my_listeners, 'iconSpedizione');
	        		}
	    		});
	    		
	    		
				    voci_menu.push({
	          		text: 'Modifica carico',
	        		iconCls : 'iconCarico',	          		
	        		handler: function() {											
											
	    			  	acs_show_win_std('Modifica carico', 'acs_form_json_carico.php?fn=open_carico', 
	    			  		{		
	    			  		 sped_id: rec.get('nr_sped'),						    			  		 
	    			  		 from_sped: 'Y'
	    			  		}, 600, 450, {				        			
				        					'afterSave': function(from_win){
					        					grid.getStore().load();			        					
		 										from_win.close();	 											
								        	}
							}, 'iconCarico');
	        		}
	    		});	    		
	    		
	    		
	    		
	    		    voci_menu.push({
	          		text: 'Report booking',
	        		iconCls : 'icon-print-16',	          		
	        		handler: function() {
	        		window.open ('../desk_booking/acs_booking_report.php?nr_sped=' + rec.get('nr_sped') + '&controllo=Y' + '&bstime=' + rec.get('bstime')  ,"mywindow");	
	        		}
	    		});
	    		
	    		voci_menu.push({
	          		text: 'Visualizza prenotazione',
	        		iconCls : 'icon-calendar-16',	          		
	        		handler: function() {
	        			acs_show_win_std('Visualizzazone richiesta booking', '../desk_booking/acs_booking.php?fn=open_parameters', {nr_sped: rec.get('nr_sped'), bstime: rec.get('bstime'), visualizza: 'Y'}, null, null, {}, 'icon-calendar-16', 'Y');
	        		}
	    		});
	    		
	    		    voci_menu.push({
	          		text: 'Email',
	        		iconCls : 'icon-address_black-16',	          		
	        		handler: function() {
	        		window.open ('../desk_booking/acs_booking_report.php?r_auto_email=Y&nr_sped=' + rec.get('nr_sped') ,"mywindow");	
	        		}
	    		});
	    		
	    		
/*	    		
	    		    voci_menu.push({
	          		text: 'Test invio automatico email',
	        		iconCls : 'icon-address_black-16',	          		
	        		handler: function() {
	        		
		        		Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_test_invio_email',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				sped_id: rec.get('nr_sped'),
		        				bstime: rec.get('bstime')
							},							        
					        success : function(result, request){
		            			alert('ok');			            		   
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	        		}
	    		});	    		
*/	    		
	    		
				   
				   
				   }
				   
				   <?php 
				   
	 if ($auth->is_admin()){
	     
	     if($auth->is_config($main_module->get_cod_mod(), 'B_CHG_STS', 'UTMOB') == true){
	         if (is_null($list_user_function) == false && empty($list_user_function) == false)
	             $chg_sts = true;
             else
                 $chg_sts = false;
	     }else{
	         $chg_sts = true;
	     }
	     
	  if ($chg_sts == true){?> 
				   
				   if(colSelected.dataIndex == 'stato_desc') { 
				   
				   <?php foreach($conf_stato_booking as $k => $v){
				   
				   	if ($k == '00') continue; //non posso ripassarlo in inserimento
				   	
				   	switch($k){
				   		case 00:
				   			$icon = "icon-pencil-16";
				   			break;
				   		case 01:
				   			$icon = "icon-label_blue_new-16";
				   			break;
				   		case 02:
				   			$icon = "icon-check_green-16";
				   			break;
			   			case 03:
			   				$icon = "icon-sub_red_delete-16";
			   				break;	
		   				case 04:
		   					$icon = "icon-divieto-16";
				   			break;
			   			case 05:
			   				$icon = "iconEvaso";
			   				break;
				   	}
			
				   	?>
				    voci_menu.push({
	          		text: '<?php echo $v; ?>',  
	          		iconCls : '<?php echo $icon; ?>',       		
	        		handler: function() {
	        		
	        		<?php  if($k == '04'){ //respinto: chiedo causale ?>
	        		
	        		
	        		my_listeners = {
	        					aftercausale: function(form_win, form_values){
	        								Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_stato',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        			    list_bstime : list_selected_id,
					        				new_stato: '<?php echo $k; ?>',
					        				causale: form_values.f_causale
										},							        
								        success : function(result, request){
					            			grid.getStore().load();	
					            			form_win.close();
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
					        		}
			    				};
	        		
	        			acs_show_win_std('Stato respinto', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_causale_stato', 
	    			  		{		
	    			  		 list_bstime : list_selected_id,
		        				new_stato: '<?php echo $k; ?>'
	    			  		}, 300, 200, my_listeners, 'icon-calendar-16');
	        		
	        		<?php }else{ ?>
		        		Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_stato',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_bstime : list_selected_id,
		        				new_stato: '<?php echo $k; ?>'
							},							        
					        success : function(result, request){
		            			grid.getStore().load();	
		            		   
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
				    
				    <?php }?>
				    
	        		}
	    		
	    		
	    		});
	    		
	    		   <?php }?>
	    		
	    		 
				   
				   
				   }
			 <?php  }   

            }?>

			//verifico che tutti gli ordini siano in stato Ricevuto (01)
			var can_select = true; 	 
  			id_selected = grid.getSelectionModel().getSelection();
			for (var i=0; i<id_selected.length; i++)
				if (id_selected[i].get('stato') != '01')
					can_select = false;

		 		 
		<?php 
		
		if($auth->is_config($main_module->get_cod_mod(), 'B_ACC_LOG', 'UTMOB') == true){
		    if (is_null($list_user_function) == false && empty($list_user_function) == false)
	           $acc_log = true;
	        else
	            $acc_log = false;
		}else{
		    $acc_log = true;
		}
		
		if ($acc_log == true){ ?> 
			if(colSelected.dataIndex == 'f_log' && can_select == true) { 
				      	 voci_menu.push({
				         		text: 'Confermata',
				        		iconCls : 'icon-check_green-16',          		
				        		handler: function() {
			            
			             			Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_log',
									        timeout: 2400000,
									        method     : 'POST',
						        			jsonData: {
						        				list_bstime : list_selected_id
											},							        
									        success : function(result, request){
									        	var jsonData = Ext.decode(result.responseText);
						            			Ext.each(grid.getSelectionModel().getSelection(), function(item) {
						            				item.set(jsonData.rows[item.get('bstime')]);
						            			});
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
			            
			            		
	        				
			            }
	    				});

				
				
			  	 voci_menu.push({
	         		text: 'Respinta',
	        		iconCls : 'icon-divieto-16',          		
	        		handler: function () {
	        			   Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_resp_log',
									        timeout: 2400000,
									        method     : 'POST',
						        			jsonData: {
						        				list_bstime : list_selected_id
											},							        
									        success : function(result, request){
									        	var jsonData = Ext.decode(result.responseText);
						            			Ext.each(grid.getSelectionModel().getSelection(), function(item) {
						            				item.set(jsonData.rows[item.get('bstime')]);
						            			});						            		  
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		                }
	    		});	  
	    		
	    		voci_menu.push({
		         		text: 'Annulla spunta',
		        		iconCls : 'icon-sub_red_delete-16',          	 	
		        		handler: function () {
		        			   Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_annulla_log',
									        timeout: 2400000,
									        method     : 'POST',
						        			jsonData: {
						        				list_bstime : list_selected_id
											},							        
									        success : function(result, request){
									        	var jsonData = Ext.decode(result.responseText);
						            			Ext.each(grid.getSelectionModel().getSelection(), function(item) {
						            				item.set(jsonData.rows[item.get('bstime')]);
						            			});						            		  
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
			                }
		    		});		

            }
            
            <?php }
            
            
            if($auth->is_config($main_module->get_cod_mod(), 'B_ACC_AMM', 'UTMOB') == true){
                if (is_null($list_user_function) == false && empty($list_user_function) == false)
                    $acc_amm = true;
                else
                    $acc_amm = false;
            }else{
                $acc_amm = true;
            }
            
            if($acc_amm == true){?> 
            
            if(colSelected.dataIndex =='f_amm' && can_select == true){
		      	 voci_menu.push({
		         		text: 'Confermata',
		        		iconCls : 'icon-check_green-16',          		
		        		handler: function () {
		        			     Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_amm',
									        timeout: 2400000,
									        method     : 'POST',
						        			jsonData: {
						        				list_bstime : list_selected_id
											},							        
									        success : function(result, request){
									      		var jsonData = Ext.decode(result.responseText);
						            			Ext.each(grid.getSelectionModel().getSelection(), function(item) {
						            				item.set(jsonData.rows[item.get('bstime')]);
						            			});
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
			                }
		    		});

		      	 voci_menu.push({
		         		text: 'Respinta',
		        		iconCls : 'icon-divieto-16',          	 	
		        		handler: function () {
		        			   Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_resp_amm',
									        timeout: 2400000,
									        method     : 'POST',
						        			jsonData: {
						        				list_bstime : list_selected_id
											},							        
									        success : function(result, request){
									        	var jsonData = Ext.decode(result.responseText);
						            			Ext.each(grid.getSelectionModel().getSelection(), function(item) {
						            				item.set(jsonData.rows[item.get('bstime')]);
						            			});
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
			                }
		    		});
		    		
		    		voci_menu.push({
		         		text: 'Annulla',
		        		iconCls : 'icon-sub_red_delete-16',          	 	
		        		handler: function () {
		        			   Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_annulla_amm',
									        timeout: 2400000,
									        method     : 'POST',
						        			jsonData: {
						        				list_bstime : list_selected_id
											},							        
									        success : function(result, request){
									        	var jsonData = Ext.decode(result.responseText);
						            			Ext.each(grid.getSelectionModel().getSelection(), function(item) {
						            				item.set(jsonData.rows[item.get('bstime')]);
						            			});						            		  
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
			                }
		    		});
				
				  }
				  
				  <?php }?>
				  
				   
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	
			    			
			   }
	 					
	 			 
	 			      }
	 			      
	 			      
         
	   		
			
		}//grid	

]}
	
<?php

}


if ($_REQUEST['fn'] == 'form_causale_stato'){
	$m_params = acs_m_params_json_decode();

	?>
{"success":true, "items": [

        {
            xtype: 'form',
            frame: true,
            title: '',
          	bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			                        
            
            items: [
   

						{
							//flex: 1,							
							name: 'f_causale',
							xtype: 'combo',
							fieldLabel: 'Causale rifiuto',
							value: '01',
							anchor: '-15',
							margin: "10 10 10 10",
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
								data: [								    
							     <?php echo acs_ar_to_select_json($main_module->find_TA_std('CAUSR'), ''); ?> 	
							    ] 
								    
								}						 
							}


            ] ,
            
			buttons: [{
	            text: 'Conferma',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',	            
	              handler: function() {
	              			var form = this.up('form').getForm();
	              			var form_values = form.getValues();
			            	var loc_win = this.up('window');
			            	
							loc_win.fireEvent('aftercausale', loc_win, form_values);
			
			            }
	        }]            
				
        }
]}

<?php 
exit;

}

if ($_REQUEST['fn'] == 'get_json_data_history'){
    
    $m_params = acs_m_params_json_decode();
    $sql_where = "";
    
    $spedizione = $m_params->open_request->f_sped;
    $data = $m_params->open_request->f_data;
    
    $format_data = print_date($data, "%Y-%m-%d");
     
    if(isset($spedizione) && strlen($spedizione) > 0){
        $sql_where .= " AND NTKEY1 = '{$spedizione}'";
    }
    
    if(isset($data) && strlen($data) > 0){
        $sql_where .= " AND NTTSCR >= '{$format_data}' AND NTTSCR < '{$format_data} 23:59:59.998'";
    }
    
    $sql= "SELECT *
    FROM {$cfg_mod_Spedizioni['file_note']} NT
    WHERE NTDT = '$id_ditta_default' AND NTTPNO = 'BOOKA'
    $sql_where
    ORDER BY NTTSCR DESC LIMIT 200";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['spedizione']	= trim($row['NTKEY1']);
        $nr['data']  	= db2_ts_to_datetime($row['NTTSCR']); //date('d/m/Y H:i:s', $row['NTTSCR']);
        $nr['user']  	= trim($row['NTUSGE']);
        $nr['nota']  	= trim($row['NTMEMO']);
        $ar[] = $nr;
        
    }
    
    echo acs_je($ar);
    exit;
}


if ($_REQUEST['fn'] == 'open_history'){
    
    $m_params = acs_m_params_json_decode();
    
 ?>
 
 

{"success":true, "items": [


   {
            xtype: 'form',
            frame: true,
            title: '',
          	flex : 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
     		defaults: {
				margin: '5 5 5 5'
			},
			items: [
            			{
							     name: 'f_data'  
							  // , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							},
				
							
							{
								name: 'f_sped',
								xtype: 'textfield',
								fieldLabel: 'Spedizione',
								value: '',
							    
							 },
							
												

            ] ,
            
			buttons: [
			{
	            text: 'Conferma',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',	            
	              handler: function() {
			            	var form = this.up('form').getForm();
			            	var data = form.findField('f_data').getValue();
			            	var sped = form.findField('f_sped').getValue();
			            	if(data === null && sped.trim() == ''){
			            	   acs_show_msg_error('Inserire almeno una data o una spedizione');
							   return false;
			            	}
			            	
							
							acs_show_win_std('Interroga cronologia booking', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_list', {form_values: form.getValues()}, 700, 400, null, 'icon-blog-16');
							this.up('window').close();
			
			            }
	        }
			]            
				
        }

			
		
 				
	
     ]
        
 }
    
<?php 

exit;
}


if ($_REQUEST['fn'] == 'open_list'){
    
    $m_params = acs_m_params_json_decode();
    
    ?>
    
    
{"success":true, "items": [

		{
			xtype: 'grid',
	        loadMask: true,	
	        features: [{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 	type: 'boolean',
			dataIndex: 'visible'
		     }
		      ]
			}],
	 		store: {
			xtype: 'store',
			autoLoad:true,

				proxy: {
				   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_history', 
				   method: 'POST',								
				   type: 'ajax',

			       actionMethods: {
			          read: 'POST'
			        },
			        
			           extraParams: {
						 open_request: <?php echo acs_je($m_params->form_values) ?>
    				},
    				
    				doRequest: personalizza_extraParams_to_jsonData, 
		
				   reader: {
		            type: 'json',
					method: 'POST',						            
		            root: 'root'						            
		        }
			},
			
			fields: ['spedizione', 'data', 'user', 'nota']							
									
			}, //store
		   columns: [	
			      {
	                header   : 'Sped.',
	                dataIndex: 'spedizione',
	                width : 50,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Data/ora',
	                dataIndex: 'data',
	                width : 110,
	                filter: {type: 'string'}, filterable: true
	                },
	                  {
	                header   : 'User',
	                dataIndex: 'user',
	                width : 80,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Descrizione attivit&agrave;',
	                dataIndex: 'nota',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                }
	         ] ,

		}//grid

     ]
        
 }
    
    
    <?php 
    
    exit;   
 
 }


function componi_carico_out($ar){

	if(count($ar)==1)
		return array_keys($ar)[0];
	
	$ret = array();
	foreach($ar as $k => $v){
		$ret[] = $k . ' ' . get_image_per_carico($v);
	}

	return implode(' ', $ret);
}

function get_image_per_carico($v){
	$ret = '';
    
	if($v == 'C'){
		$img_com_name = "icone/128x128/euro_green.png";
	}elseif($v == 'P'){
		$img_com_name = "icone/128x128/euro_yellow.png";
	}elseif($v == 'N' || trim($v) == ''){
		$img_com_name = "icone/128x128/euro_red.png";
	
	}elseif(substr($v, 0, 1) == 'T' && in_array(substr($v, 1, 1), array('D', 'E', 'F'))
	    ){   //in base alla minima Tipologia pagamento sugli ordini
	       $img_com_name = "icone/128x128/currency_blue_euro.png";	    
	}else{
		$img_com_name = '';
	}
	
	if ($img_com_name != '')	    
		$ret =  '<span style="display: inline"><img class="cell-img" src=' . img_path($img_com_name) . ' height=15 ></a></span>';
	
	
	return $ret;
}

?>