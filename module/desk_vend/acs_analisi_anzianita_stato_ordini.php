<?php

require_once("../../config.inc.php");
require_once("acs_analisi_anzianita_stato_ordini_include.php");

$s = new Spedizioni();
$main_module = new Spedizioni();

if ($_REQUEST['fn'] == 'get_json_data'){

	$m_params = acs_m_params_json_decode();

	$ar=crea_ar_tree_anzianita_stato_ordini($m_params->open_request->form_values);
	
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}

	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
}


if ($_REQUEST['fn'] == 'open_parameters'){
	
	$m_params = acs_m_params_json_decode();

	?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "STATO_ORDINI"); ?> 
					{
			            text: 'Report',
			            iconCls: 'icon-print-32',
			            scale: 'large',
			            handler: function() {
		                    form = this.up('form').getForm();
	                        this.up('form').submit({
	                        url: 'acs_analisi_anzianita_stato_ordini_report.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',                        
	                        params: {
	                            form_values: Ext.encode(form.getValues())
						    }
                  			});            	                	     
            	                	                
	            }
			         },
					
					{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_analisi_anzianita_stato_ordini.php?fn=open_tab', 'panel_stato_ordini', {form_values: form.getValues(), show: '<?php echo $m_params->show?>'});  //recupero i valori del form per passarli nel tree
							this.up('window').close();
			
			            }
			         }
					 
						
						], items: [
						
					<?php if ($m_params->show !='Y'){ ?>	
		         	  	{
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?>	
								    ] 
							}						 
						}, {
							name: 'f_agente',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Agente',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json($s->get_options_agenti(), '') ?>
								    ] 
							}						 
						}, {
							name: 'f_areaspe',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 
								    ] 
							}						 
						},
						
						<?php }
						
						
						if ($m_params->show !='Y'){ ?>	
						
						
						{
							name: 'f_itinerario',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
								    ] 
							}						 
						}
						
						<?php } else { ?>
						
						{
							name: 'f_sede',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Sede',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('START'), '') ?>	 	
								    ] 
							}						 
						}, 
						
							
				        { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						  {
							name: 'f_stato_ordine',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Stati ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    width : 350,
						    margin: "5 10 5 10",		   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ]
							}						 
						}, {
							name: 'f_filtra_stati',
							xtype: 'radiogroup',
							fieldLabel: '',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "5 10 5 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_filtra_stati' 
		                          , boxLabel: 'Includi'
		                          , inputValue: 'Y'
		                          , width: 100
		                          , checked: false
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_filtra_stati' 
		                          , boxLabel: 'Escludi'
		                          , inputValue: 'N'
		                          , checked: true
		                        }]
						}
						
						   
						]}
						
						
						
						
						<?php } ?>
						
						,{
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
							}						 
						}
						
                						
		                , {
                				name: 'f_tipo_ordine',
                				xtype: 'combo',
                            	anchor: '-15',
						    	margin: "5 10 10 10",
                				fieldLabel: 'Tipo ordine',
                				displayField: 'text',
                				valueField: 'id',
                				emptyText: '- seleziona -',
                				forceSelection: true,
                			    allowBlank: true,
                					     		
                				store: {
                					autoLoad: true,
                					editable: false,
                					autoDestroy: true,
                				    fields: [{name:'id'}, {name:'text'}],
                				    data: [
                                              <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, null, null, null, 0, '', 'Y'), ''); ?>
                					    ]
                				}
                			}							
						
						
						
						<?php if ($m_params->show !='Y'){ ?>
						
						, {
							name: 'f_hold',
							xtype: 'radiogroup',
							fieldLabel: 'Hold',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "5 10 5 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Inclusi'
		                          , inputValue: 'A'
		                          , width: 50
		                         , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Esclusi'
		                          , inputValue: 'Y'
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'N'
		                          
		                        }]
						}
						
						<?php }?>
						
						, { 
						xtype: 'fieldcontainer',
						margin: "5 25 10 10",
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data anzianit&agrave; da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data anzianit&agrave; a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }, { 
						xtype: 'fieldcontainer',
						margin: "5 25 5 10",   
						layout: 'hbox',
					    items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data progr. da'
						   , flex : 1
						   , name: 'f_data_prog_da'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data progr. a'
						   , labelAlign: 'right'
						   , name: 'f_data_prog_a'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , flex: 1		
						   , anchor: '-15'	
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					 
						
						
					
										 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}

	
	<?php
	exit; 

}

if ($_REQUEST['fn'] == 'open_tab'){
	$request = acs_m_params_json_decode(); //in questa variabile recupero con la REQUEST i valori dell'array del form in questo caso
	?>

{"success":true, "items": [

        {
            xtype: 'treepanel',
	        title: 'Aging',
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        multiSelect : true,
	        loadMask: true,
	        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Analisi anzianit&agrave; stato ordini corrente</b>', '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),             
			
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
                   
				   fields: ['task', 'fl_art_manc', 'art_da_prog', 'riferimento', 'tipo', 'data_reg', 
				   'priorita', 'cons_rich', 'data_disp', 'cons_prog', 'n_O', 'n_P', 'n_M', 'n_A', 'n_CV', 
				   'fl_da_prog', 'fl_bloc', 'art_da_prog', 'fl_new', 'n_stadio', 'importo','gg_lav', 'liv', 'cod_cli',
				    'tipo_liv', 'liv_tipo_qtip', 'classe_ordine', 'k_ordine', 'stato', 'desc', 'fl_evaso', 'tdfg06'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
					
                        extraParams: {
                                    open_request: <?php echo acs_je($request) ?>
                               
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData, 
								

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						        }       				
                    }

                }),
				

		columns: [{
			            xtype: 'treecolumn', 
			            columnId: 'task', 
			            flex: 1,
			            dataIndex: 'task',
			            menuDisabled: true, 
			            sortable: false,
			            header: 'Stato ordine/<br>Data assegnazione'
			        }, 
			        
			        <?php require_once("_include_js_column_flag_ordini.php"); ?>,
			         
			         { 
			            dataIndex: 'riferimento',
			            header: 'Riferimento', width: 100
			        }, { 
			            dataIndex: 'tipo',
			            header: 'Tp', width: 50,
			            tdCls: 'tipoOrd',
			            renderer: function(value, metaData, record){
						
					    	if (record.get('liv_tipo_qtip') != ''){
				    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('liv_tipo_qtip')) + '"';			    	
					    	}						
							metaData.tdCls += ' ' + record.get('classe_ordine');
						
						 return value;
						}
			        }, { 
			            dataIndex: 'data_reg',
			            header: 'Data Ricez.', width: 80,
			            renderer: date_from_AS
			        }, { 
			            dataIndex: 'priorita',
			            header: 'Pr', width:50
			        }, { 
			            dataIndex: 'cons_rich',
			            header: 'Cons.<br>richiesta', width: 60,
			            renderer: date_from_AS
			        }, { 
			            dataIndex: 'data_disp',
			            header: 'Dispon.', width: 80,
			            renderer: date_from_AS
			        }, 
			      
			        { 
			             dataIndex: 'cons_prog',
			        	<?php if($request->show !='Y'){?>	
			            header: 'Produz./<br>Disponib.',
			            <?php }else{ ?>
			            header: 'Evas./<br>Programm.',
			            <?php }?>
			             width: 70,
			            renderer: date_from_AS
			        },
			        
			        
			        
			       <?php require_once("_include_js_column_classe_ordini.php"); ?>,
			        
			         { 
			            dataIndex: 'importo',
			            header: 'Importo', width: 100,
			            renderer: floatRenderer2,
			            align: 'right'
			        }, { 
			            dataIndex: 'gg_lav',
			            header: 'GG Lavorativi', width: 100,
			            align: 'right'
			        }
			     
	         ]	
	         , listeners: {
	         
	         	     
			     beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },

                        load: function () {
                          Ext.getBody().unmask();
                        }, 
			    
			     itemclick: function(view,rec,item,index,eventObj) {
	
			     
			          if (rec.get('tipo_liv') == 'ordine')
			          {	
			        	var w = Ext.getCmp('OrdPropertyGrid');
			        	//var wd = Ext.getCmp('OrdPropertyGridDet');
			        	var wdi = Ext.getCmp('OrdPropertyGridImg');        	
			        	var wdc = Ext.getCmp('OrdPropertyCronologia');

			        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdi.store.load()
						wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdc.store.load()			        												        	
			        	
						Ext.Ajax.request({
						   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
						   success: function(response, opts) {
						      var src = Ext.decode(response.responseText);
						      w.setSource(src.riferimenti);
					          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);						      
						      //wd.setSource(src.dettagli);
						   }
						});
						
        				//files (uploaded) - ToDo: dry 
        				var wdu = Ext.getCmp('OrdPropertyGridFiles');
        				if (!Ext.isEmpty(wdu)){
        					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
        			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
        			        	if (wdu.isVisible())		        	
        							wdu.store.load();
        					}					
        				}						
						
					   }												        	
			         }, celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.stopEvent();

						if (rec.get('tipo_liv')=='stato_ordine'){
							iEvent.preventDefault();
							acs_show_panel_std('acs_stato_ordini_grafico.php', 'panel_stato_ordini', {
								open_parameters: <?php echo acs_je($request) ?>,
								stato_selected: rec.get('stato'),
								desc_stato: rec.get('desc')
							});
					
						}
						
						<?php if($request->show =='Y'){?>	
						
						if (col_name=='art_mancanti' && rec.get('liv')=='liv_4'){
						iEvent.preventDefault();	
						acs_show_win_std('Segnalazione articoli critici', 'acs_art_critici.php?fn=open_art', {k_ordine: rec.get('k_ordine'), type: 'MTO'}, 950, 500, null, 'icon-shopping_cart_gray-16');					
					
						return false;							
					}	
					
					   <?php }?>
													
						return false;

					 }
					}
					
			<?php if($request->show =='Y'){?>		
		
			, itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
		  													  
				  var voci_menu = [];
				  
				  if (rec.get('liv')=='liv_4'){
				  
				   //se ha selezionato un ordine evaso non permetto nessuna operazione
				    	id_selected = grid.getSelectionModel().getSelection();		
				    	num_evasi = 0;
				    	for (var i=0; i<id_selected.length; i++)
				  		   num_evasi += parseFloat(id_selected[i].data.fl_evaso);
			
				  		if (num_evasi > 0){
			
				  		var voci_menu = [];
					  		        
				      	 voci_menu.push({
				         		text: 'Stampa',
				        		iconCls : 'icon-print-16',          		
				        		handler: function () {
				        			    list_selected_id = grid.getSelectionModel().getSelection();
						    		  	rec = list_selected_id[0];	 
						    		  	window.open('acs_report_pdf.php?k_ordine='+ rec.get('k_ordine'));
					                }
	    				});


						 voci_menu.push({
				         		text: 'Modifica stato', 
				        		iconCls : 'icon-clessidra-16',          	 	
				        		handler: function () {
				        		
				        		my_listeners = {
			    		  			afterOkSave: function(from_win){
			    		  			console.log(grid);
		        						grid.getStore().treeStore.load();
		        						from_win.close();  
						        		}
				    				};	
				        		
						         acs_show_win_std('Cambia stato', '../desk_vend/acs_gestione_stato_ordini.php?fn=change_state', {k_ordine: rec.get('k_ordine')}, 400, 150,  my_listeners, 'icon-clessidra-16');	
					           }
				    		});

	    		
			  		 voci_menu.push({
			         		text: 'Altre voci menu <br> bloccate perch&egrave; <br> l\'ordine risulta evaso',
			        		iconCls : 'icon-warning_red-16',          		
			        		/*handler: function() {

		    	          		
	        		}*/
	    		});
	    		
	    		
	    		
				
			      
	          var menu = new Ext.menu.Menu({
	            items: voci_menu
		     }).showAt(event.xy);	

			     return false;		
	  		}	  //sara ordini evasi
				
				
			  	 voci_menu.push({
	         		text: 'Testata ordine',
	        		iconCls : 'icon-pencil-16',          		
	        		handler: function () {
	        			    list_selected_id = grid.getSelectionModel().getSelection();
			    		  	rec = list_selected_id[0];	
			    		  	
			    		  	my_listeners = {
			    		  			afterOkSave: function(from_win, new_value){
		        						rec.set('tipo', new_value.TDOTPD);
		        						rec.set('riferimento', new_value.TDVSRF);
		        						rec.set('data_reg', new_value.TDODRE);
		        						rec.set('priorita', new_value.TDOPRI);
		        						from_win.close();  
						        		}
				    				};	
			    		  	 
			    		  	acs_show_win_std('Modifica informazioni di testata', 
		                			'acs_panel_protocollazione_mod_testata.php?fn=open_form', {
		                				tddocu: rec.get('k_ordine')
									}, 800, 600, my_listeners, 'icon-shopping_cart_gray-16');
		                }
	    		});	  		


		      	 voci_menu.push({
		         		text: 'Righe ordine',
		        		iconCls : 'icon-leaf-16',          		
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
								  acs_show_win_std('Visualizza righe', 'acs_righe_ordine.php?', {k_ordine: rec.get('k_ordine')}, 900, 550, null, 'icon-leaf-16');
			                }
		    		});
    		
    		 	   	 voci_menu.push({
		      		text: 'Documenti ordine',
		    		iconCls : 'iconScaricoIntermedio',      		
		    		handler: function() {
						acs_show_win_std('Elenco documenti ordine', '../desk_punto_vendita/acs_documenti_cliente.php?fn=open_list', {
							k_ordine: rec.get('k_ordine')
						}, 1100, 600, {}, 'icon-leaf-16');
		    		}
				  });

		      	 voci_menu.push({
		         		text: 'Stampa contratto',
		        		iconCls : 'icon-print-16',          	 	
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    		  	window.open('acs_report_pdf.php?k_ordine='+ rec.get('k_ordine'));
			                }
		    		});
		    		
						 voci_menu.push({
				         		text: 'Modifica stato', 
				        		iconCls : 'icon-clessidra-16',          	 	
				        		handler: function () {
				        		
				        		my_listeners = {
			    		  			afterOkSave: function(from_win){
			    		  			console.log(grid);
		        						grid.getStore().treeStore.load();
		        						from_win.close();  
						        		}
				    				};	
				        		
						         acs_show_win_std('Cambia stato', '../desk_vend/acs_gestione_stato_ordini.php?fn=change_state', {k_ordine: rec.get('k_ordine')}, 400, 150,  my_listeners, 'icon-clessidra-16');	
					           }
				    		});
				    		
				    		
			    		voci_menu.push({
     					text: 'Inserimento nuovo stato/attivit&agrave;',
    					iconCls : 'icon-arrivi-16',          		
    					handler: function() {

            			id_selected = grid.getSelectionModel().getSelection();
      
            		  	list_selected_id = [];
            		  	for (var i=0; i<id_selected.length; i++) 
            			   list_selected_id.push(id_selected[i].data.k_ordine);
        
            		  	my_listeners_inserimento = {
            					afterInsertRecord: function(from_win){	
            						from_win.close();
        			        		}
        	    				};	
        
                		
        				acs_show_win_std('Nuovo stato/attivit&agrave;', 
        					<?php echo j('acs_form_json_create_entry.php'); ?>, 
        					{	tipo_op: 'VERST',
        						rif : 'POS',
        		  				list_selected_id: list_selected_id
        		  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
        		  							        		          		
                		}
            			});
						 
		    			id_selected = grid.getSelectionModel().getSelection();
            			list_selected_id = [];
		  				for (var i=0; i<id_selected.length; i++) 
			   				list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});
			   
        		    voci_menu.push({
        	     		text: 'Evidenza generica',
        	    		iconCls : 'icon-sub_blue_accept-16', 
        	    		menu:{
        	        	items:[
        	                {  text: 'Rosso',
        	                   iconCls : 'icon-sub_blue_accept-16',
        	                   style: 'background-color: #F9BFC1;', 
        	                   handler : function(){
        	                     Ext.Ajax.request({
        					        url        : 'acs_op_exe.php?fn=evidenza_generica',
        					        timeout: 2400000,
        					        method     : 'POST',
        		        			jsonData: {
        		        				list_selected_id: list_selected_id,
        		        				flag : '1'
        							},							        
        					        success : function(result, request){
        					         var jsonData = Ext.decode(result.responseText);
        					         if(jsonData.success == true){
        					         var ar_value = jsonData.or_flag;
        					         	for (var i=0; i<id_selected.length; i++) 
        					        	 	for (var chiave in ar_value) {
        						        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
        											id_selected[i].set('tdfg06', ar_value[chiave].flag);  
        						        	 }
        					         }
        							 },
        					        failure    : function(result, request){
        					            Ext.Msg.alert('Message', 'No data to be loaded');
        					        }
        					    });
        	                   }
        	                },
        	            	{  text: 'Giallo',
        	            	   iconCls : 'icon-sub_blue_accept-16', 
        	            	   style: 'background-color: #F4E287;',
        	            	        handler : function(){
        	                     Ext.Ajax.request({
        	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
        					        timeout: 2400000,
        					        method     : 'POST',
        		        			jsonData: {
        		        				list_selected_id: list_selected_id,
        		        				flag : '2'
        							},							        
        					        success : function(result, request){
        					         var jsonData = Ext.decode(result.responseText);
        					         if(jsonData.success == true){
        						         var ar_value = jsonData.or_flag;
        						         	for (var i=0; i<id_selected.length; i++) 
        						        	 	for (var chiave in ar_value) {
        							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
        												id_selected[i].set('tdfg06', ar_value[chiave].flag);
        							        	 }
        						         }
        					         
        		            		},
        					        failure    : function(result, request){
        					            Ext.Msg.alert('Message', 'No data to be loaded');
        					        }
        					    });
        	                   }
        	                },{
        	                    text: 'Verde',
        	                    iconCls : 'icon-sub_blue_accept-16', 
        	                    style: 'background-color: #A0DB8E;',
        	                         handler : function(){
        	                     Ext.Ajax.request({
        	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
        					        timeout: 2400000,
        					        method     : 'POST',
        		        			jsonData: {
        		        				list_selected_id: list_selected_id,
        		        				flag : '3'
        							},							        
        					        success : function(result, request){
        					         var jsonData = Ext.decode(result.responseText);
        					         if(jsonData.success == true){
        						         var ar_value = jsonData.or_flag;
        						         	for (var i=0; i<id_selected.length; i++) 
        						        	 	for (var chiave in ar_value) {
        							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
        												id_selected[i].set('tdfg06', ar_value[chiave].flag);
        							        	 }
        						         }
        					         
        		            		},
        					        failure    : function(result, request){
        					            Ext.Msg.alert('Message', 'No data to be loaded');
        					        }
        					    });
        	                   }
        	                },{
        	                	text: 'Nessuno',
        	                	iconCls : 'icon-sub_blue_accept-16', 
        	                	     handler : function(){
        	                     Ext.Ajax.request({
        	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
        					        timeout: 2400000,
        					        method     : 'POST',
        		        			jsonData: {
        		        				list_selected_id: list_selected_id,
        		        				flag : ''
        							},							        
        					        success : function(result, request){
        					         var jsonData = Ext.decode(result.responseText);
        					         if(jsonData.success == true){
        						         var ar_value = jsonData.or_flag;
        						         	for (var i=0; i<id_selected.length; i++) 
        						        	 	for (var chiave in ar_value) {
        							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
        												id_selected[i].set('tdfg06', ar_value[chiave].flag);
        							        	 }
        						         }
        					         
        		            		},
        					        failure    : function(result, request){
        					            Ext.Msg.alert('Message', 'No data to be loaded');
        					        }
        					    });
        	                   }
        	                }
        	            ]
        	        }        		
        	    	
        			});
        		    		
				
				  
			    	}
			    	
			    	if(rec.get('liv') == 'liv_3'){
			    	voci_menu.push({
        	      		text: 'Ordini aperti cliente',
        	    		iconCls : 'icon-exchange_black-16',      		
        	    		handler: function() {
        		    		console.log(rec.data);
        					acs_show_win_std('Elenco ordini aperti', 'acs_documenti_cliente.php?fn=open_list', {
        						cliente_selected : rec.get('cod_cli'), ord_aperti : 'Y'
        					}, 1024, 600, {}, 'icon-leaf-16');
        	    		}
        			  });
			    	
			    	}
			    	
			    	
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    			
			   }	
			  
			  <?php }?>
	         }
	         
	         , viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           
			    
			           return ret;																
			         }   
			    }																			  			
	            
        }  

]
}

<?php exit; }