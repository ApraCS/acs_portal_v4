<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
		'tab_name' =>  $cfg_mod_Spedizioni['file_tabelle'],
		
		'TATAID' => 'TODRF',
		'descrizione' => 'Gestiona tabella riferimenti precodificati per singola ToDo',
		
		'fields_key' => array('TAKEY1', 'TAKEY2'),
		
		'fields' => array(				
				'TAKEY1' => array('label'	=> 'Attivit�'),
		        'TAKEY2' => array('label'	=> 'ID'),
	            'TADESC' => array('label'	=> 'Installatore'),
		        'TACOGE' => array('label'	=> 'Codice fornitore'),
		)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
