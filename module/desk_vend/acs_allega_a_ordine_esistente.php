<?php
require_once "../../config.inc.php";

$main_module = new Spedizioni();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// ALLEGA A ORDINE ESISTENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_allega'){    
    $ret = array();
    
    //get binary email
    $imap_config = (array)$m_params->email_config;
    $mbox = imap_open("{{$imap_config['server']}}" . $imap_config['folder'], $imap_config['username'], $imap_config['password']);
    
    foreach($m_params->id_messages as $message){
        
        // ****** EMAIL AS BINARY ********************************
        
        $msgid =  imap_msgno ($mbox, $message->message_uid );
        //genero .eml content
        $headers_eml = imap_fetchheader($mbox, $msgid, FT_PREFETCHTEXT);
        $body_eml = imap_body($mbox, $msgid);        
        $binary_email = $headers_eml . "\n" . $body_eml;
        
        require_once '../webmail/_process_email.php';
        $h = _email_info_header($mbox, $msgid);
        
        // ****** UPLOAD NELLA CARTELLA ORDINE ****************
        $save_with_filename = sanitize_filename($h['info']->subject . ".eml");
        if ($cfg_mod_Spedizioni["visualizza_order_img"] == "FROM_PC"){
            $data = array (
                'function' => 'exe_upload',
                'k_ordine' => $m_params->k_ordine,
                'file_name' => $save_with_filename,
                'content_file' => $binary_email);
            $data = http_build_query($data);
            
            $context_options = array (
                'http' => array (
                    'method' => 'POST',
                    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                    . "Content-Length: " . strlen($data) . "\r\n",
                    'content' => $data
                )
            );            
            
            $context = stream_context_create($context_options);
            $fp = file_get_contents($cfg_mod_Spedizioni["url_img_FROM_PC"], false, $context);
            $ret_upload = json_decode($fp);
        }
        
        // ****** ARCHIVO EMAIL ******************************
        if ($m_params->move_to_importati){
            if (strlen($imap_config['folder_to']) > 0){
                $ret_move_to_importati = imap_mail_move($mbox, $msgid, $imap_config['folder_to']);
                imap_expunge($mbox); 
                $ret_move_to_importati_log = "Messaggio spostato in {$imap_config['folder_to']}";
            }
        }
        
    } //foreach messages
    
    
    $ret['success'] = true;
    //$ret['data'] = $data;
    $ret['ret_upload'] = $ret_upload;
    $ret['move_to_importati'] = $ret_move_to_importati;
    $ret['move_to_importati_log'] = $ret_move_to_importati_log;
    echo acs_je($ret);
    exit;
}

