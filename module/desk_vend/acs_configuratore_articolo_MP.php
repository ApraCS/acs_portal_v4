<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


//dall'articolo recupero il configuratre
$c_art = $m_params->c_art;

$sql = "SELECT ARMODE
        FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
        WHERE ARDT = '{$id_ditta_default}' AND ARART = ?";

$stmt_art = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_art, array($c_art));
$row_art = db2_fetch_assoc($stmt_art);
$config_art = trim($row_art['ARMODE']);

//in base al configuratore preparo le domande
switch ($config_art) {
    case 'BAT':
        $ar_dom = array('MOD', 'CAS');
        break;
    case 'MP':
        $ar_dom = array('MOD', 'TCA');
        break;
    case 'MP1':
        $ar_dom = array('MOD', 'MOS');
        break;
    case 'MP2':
        $ar_dom = array('ACA', 'ACM');
        break;
    default:
        $ar_dom = array();
    break;
}



if ($_REQUEST['fn'] == 'open_tab'){
    
    ?>
    {"success":true, "items": [
               {
 		            xtype: 'form',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            frame: true,
 		            items: [
 		            
 		            	<?php foreach($ar_dom as $dom){ 
 		            	
 		            	    $decode = get_TA_sys('PUVR', $dom)?>
 		            
 		            
 		               {name: '<?php echo $dom ?>',
                		xtype: 'combo',
                		flex: 1,
                		fieldLabel: <?php echo j($decode['text']) ?>,
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		//allowBlank: false,
                   		value : <?php echo j($m_params->ar_config_row->$dom)?>,							
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $dom, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); ?>	
                		    ]
                		}, listeners: {
                        	 beforequery: function (record) {
                                record.query = new RegExp(record.query, 'i');
                                record.forceAll = true;
                            }
                   }
                		
                   },
                   
                   <?php } ?>
 		            
 		         ], 
 		         
 		         dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                   {
                     xtype: 'button',
                    text: 'Conferma',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	     var form = this.up('form').getForm();
		          	     var form_values = form.getValues();
		          	     var loc_win = this.up('window');
		          	     
		          	     loc_win.fireEvent('afterConferma', loc_win, form_values);
		          	
		          	}
		          	
		          	}
                 
                 
                 ]
               
                 
                 }]
 		         
 		         
 		         
 		         }
    ]}
    <?php 
    
}
