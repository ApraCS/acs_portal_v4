<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();
$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// JSON GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){ 
   
    $sql = "SELECT DISTINCT TACINT
    FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA
    WHERE TADT = '{$id_ditta_default}' AND TAID = 'LIPC'
    ORDER BY TACINT";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $ar_linea = array();
    while($row = db2_fetch_assoc($stmt)){ 
        $gg = $m_params->filtro->gg;
        $c_data = trim($m_params->filtro->c_data);
        $cp = new SpedCapacitaProduttiva;
        $capac =  $cp->get_tot_by_data_linea($c_data, trim($row['TACINT']));
        $ar_linea[trim($row['TACINT'])] = array('name' =>trim($row['TACINT']),
                                                'c_color' =>$capac['c_color'],
                                                'tooltip' => "{$capac['COLLI_T']}|{$capac['capac']}"
        );
        
    }
       
    
   
   
 ?>
{"success":true, "items":
         {
			xtype: 'panel',
			 autoScroll: true,
		    layout: {
				type: 'vbox',
				align: 'stretch',
				pack : 'start',
			},
			items: [{
              xtype: 'grid',
              flex : 0.8,
              itemId: 'k_grid',
              autoScroll: true,
              selModel: {selType: 'checkboxmodel',  mode: 'SIMPLE'},
              store: {
                	xtype: 'store',
                    autoLoad:true,				        
                	proxy: {
                			url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
                			timeout: 2400000, 
                			jsonData: <?php echo acs_je($m_params); ?>,
                			method: 'POST',								
                			type: 'ajax',
                			reader: {
                	            type: 'json',
                	            root: 'root'
                	        },
    
    			actionMethods: {
    				read: 'POST'					
    			},
    									        
    			extraParams: {
    			window_post: <?php echo acs_je(acs_raw_post_data()); ?>,
    			k_list : ''},
    			doRequest: personalizza_extraParams_to_jsonData					        
    		},
    		
    		fields: [
    			'k_ordine_out', 'k_ordine', 'c_data', 'non_assegnato', 'raggr', 'fl_bloc', 's_red',
    			'cliente', 'stato', 'tipo', 'atp', 'totale', 'liv', 'selected', 't_colli'
    			<?php foreach($ar_linea as $k => $v){?>
    			
    			, '<?php echo $k?>'
    			, '<?php echo $k . "_c"?>'
    		
    			<?php }?>
    			
    			
    		],
    	},
			        columns: [		
			        	   			        
			             {
			                header   : 'Ordine',
			                dataIndex: 'k_ordine_out', 
			                 width: 70,
			             },
			             
			             {
			                header   : 'Cliente',
			                dataIndex: 'cliente', 
			                flex : 1,
			             },
			             {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini bloccati',
						  renderer: function(value, metaData, record){
	    			        if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
	    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
	    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
	    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';    			    				    	
	    			    	}}, 
			             {
			                header   : 'Tp',
			                dataIndex: 'tipo', 
			                width: 30,
			                tdCls: 'tipoOrd',
			                renderer: function (value, metaData, record, row, col, store, gridView){						
						      metaData.tdCls += ' ' + record.get('raggr');										
					          return value;			    
							}
			             },{
			                header   : 'St',
			                dataIndex: 'stato', 
			                width: 30,
			                 renderer: function (value, metaData, record, row, col, store, gridView){						
						      if(record.get('s_red') == 'Y')
						      	metaData.tdCls += ' sfondo_rosso';										
					          return value;			    
							}
			             },{
			                header   : 'Dispon.',
			                dataIndex: 'atp', 
			                width: 70,
			                renderer: date_from_AS 
			             } ,{
			                header   : 'Tot. colli',
			                dataIndex: 't_colli', 
			                width: 70,
			                align: 'right',
			             }
			               
			               	<?php foreach($ar_linea as $k=>$v){?>
    			
    			         ,{
			                header   : '<?php echo $v['name'] ?>',
			                dataIndex: '<?php echo $k?>', 
			                width: 90,
			                align: 'right',
			                tooltip : '<?php echo $v['tooltip']?>',
			                renderer: function (value, metaData, record, row, col, store, gridView){
	                             var cols = gridView.getGridColumns();
			                     var col_name = cols[col].dataIndex;
			                     metaData.tdCls += ' '+ record.get(col_name + '_c');
			                   
			                     return value;
			  
			  				}
			             }
    			
    			<?php }?>
    			         ,{
			                header   : 'NON ASSEGNATO',
			                dataIndex: 'non_assegnato', 
			                width: 100,
			                align: 'right',
			             }
			      
			         ],
			         
        listeners: {
        
        	  afterrender: function(comp){
        	  
        	  		//autocheck degli ordini che erano stati selezionati
        	  		comp.store.on('load', function(store, records, options) {
						var recs_selected = [];
    					store.each(function(rec){
	    					if (rec.get('selected') == true)
	    						comp.getSelectionModel().select(rec, true);	    						    				
    					});						
					}, comp);
        	  },
        
        
	           itemcontextmenu : function(grid, rec, node, index, event) {
		  			event.stopEvent();
				  													  
					 var voci_menu = [];
				     var m_grid = this;
				     
				     id_selected = grid.getSelectionModel().getSelection();
					 list_selected_id = [];
  	                 for (var i=0; i<id_selected.length; i++){
		 				record_data = id_selected[i].data;
		  				list_selected_id.push(record_data);		  
  					 }
					 
				     voci_menu.push({
		      			text: 'Assegna prod./dispon. alla spedizione', //DA ORDINE
		    			iconCls: 'iconSpedizione',
		    			handler: function() {
          				
          				 my_listeners = {
        					onSpedConfirmed: function(){
        						grid.getStore().load();
        					}
        					}
          				
			    			 acs_show_win_std('Assegna produzione/disponibilit&agrave; alla spedizione', 
			    			 				  'acs_form_json_assegna_spedizione.php',
			    			 				   {list_selected_id: list_selected_id, solo_data:'<?php echo $m_params->filtro->c_data; ?>'}, 1050, 400, my_listeners, 'icon-delivery-16');
		    		}
				  });
				 
				 
				     var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			   }, 	
               }			         
	         },{
              xtype: 'grid',
              itemId : 't_grid',
              flex : 0.2,
              store: {
                	xtype: 'store',
                    autoLoad:true,				        
                	proxy: {
                			url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tot_gg',
                			timeout: 2400000, 
                			jsonData: <?php echo acs_je($m_params); ?>,
                			method: 'POST',								
                			type: 'ajax',
                			reader: {
                	            type: 'json',
                	            root: 'root'
                	        },
    
    			actionMethods: {
    				read: 'POST'					
    			},
    									        
    			extraParams: {
    			window_post: <?php echo acs_je(acs_raw_post_data()); ?>,
    			k_list : ''},	
    			doRequest: personalizza_extraParams_to_jsonData						        
    		},
    		
    		fields: [
    			'k_ordine_out', 't_colli', 't_colli_t', 'non_assegnato'
    			<?php foreach($ar_linea as $k => $v){?>
    			
    			, '<?php echo $k?>'
    			, '<?php echo $k . "_c"?>'
    		    , '<?php echo $k . "_t"?>'
    			<?php }?>
    			
    			
    		], 
    		
    	},
			        columns: [		
			        	   			        
			             {
			                header   : 'Totali per giorno',
			                dataIndex: 'k_ordine_out', 
			                flex : 1,
			             }, {
			                header   : 'Tot. colli',
			                dataIndex: 't_colli', 
			                width: 70,
			                align: 'right',
			                renderer: function (value, metaData, record, row, col, store, gridView){
	                             var cols = gridView.getGridColumns();
			                     var col_name = cols[col].dataIndex;
			                     metaData.tdCls += ' '+ record.get('t_colli_c');
			                     metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_colli_t')) + '"';    			    
			                     return value;
			  
			  				}
			             }
			               
			               	<?php foreach($ar_linea as $k=>$v){?>
    			
    			         ,{
			                header   : '<?php echo $v['name'] ?>',
			                dataIndex: '<?php echo $k?>', 
			                width: 90,
			                align: 'right',
			                renderer: function (value, metaData, record, row, col, store, gridView){
	                             var cols = gridView.getGridColumns();
			                     var col_name = cols[col].dataIndex;
			                     metaData.tdCls += ' '+ record.get(col_name + '_c');
			                     metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get(col_name + '_t')) + '"';    			    
			                     return value;
			  
			  				}
			             }
    			
    			<?php }?>
    			         ,{
			                header   : 'NON ASSEGNATO',
			                dataIndex: 'non_assegnato', 
			                width: 115,
			                align: 'right',
			             }
			      
			         ],
					         
	 }
	 ],    dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                    	'->', {
                     xtype: 'button',
	            	 scale: 'medium',
					 text: 'Ricalcola',
	            	 iconCls: 'icon-rss_black-24',                     
			            handler: function(a, b, c) {
			            	var k_grid = this.up('panel').down('#k_grid');
			            	var k_selected = k_grid.getSelectionModel().getSelection();
		    			  	k_list = [];
		    			  	k_list_r = [];
		    			  	for (var i=0; i<k_selected.length; i++) 
		    				   	k_list.push(k_selected[i].get('k_ordine'));	
		    				   		
			            	
			            	k_grid.getStore().proxy.extraParams.k_list = k_list;
			            	k_grid.getStore().load();
			            	       	
			            	var t_grid = this.up('panel').down('#t_grid');
			            	t_grid.getStore().proxy.extraParams.k_list = k_list;
			            	t_grid.getStore().load();
			            		
			            }
			     },
                
			     ]
		   }]
	 
	 }
	
 
 
 
}  
  	
	
	
<?php	
	exit;
}

// ******************************************************************************************
// JSON GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_tot_gg'){
    
    global $is_linux;
    if ($is_linux == 'Y')
        $_REQUEST['window_post'] = strtr($_REQUEST['window_post'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
        
    $m_params = (array)json_decode($_REQUEST['window_post']);
    $m_params['filtro'] = (array)$m_params['filtro'];
    
    $filtro = array();
    $filtro['da_data'] = trim($m_params['filtro']['da_data']);
    $filtro['c_data'] = trim($m_params['filtro']['c_data']);
   
    
    $params = acs_m_params_json_decode();
                
    $sql = "SELECT TA_LIPC.TACINT AS LINEA
            FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIPC
            WHERE TA_LIPC.TADT = '{$id_ditta_default}' AND TA_LIPC.TAID = 'LIPC'
            GROUP BY TA_LIPC.TACINT ";
    
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    $ar = array();
    $nr = array();
while($row = db2_fetch_assoc($stmt)){

    $cp = new SpedCapacitaProduttiva;
      
    $capac =  $cp->get_tot_by_data_linea($filtro['da_data'], trim($row['LINEA']),  array('IE' => 'E', 'ar_tddocu' =>$params->k_list));
    $capac_c =  $cp->get_tot_by_data_linea($filtro['c_data'], trim($row['LINEA']),  array('IE' => 'I', 'ar_tddocu' =>$params->k_list));
 
       
    $ar['k_ordine_out'] = "<b>Totale ".print_date($filtro['c_data'])."</b>";
    $ar['t_colli'] += $capac_c['COLLI_T'];
    
    $tot_cap += $capac_c['capac'];
    $diff_val = $ar['t_colli'] - $tot_cap;
    if($tot_cap != 0)
        $p_cap = $diff_val/$tot_cap * 100;
    
    $diff_val = n($diff_val, 0, 'Y');
    $p_cap = n($p_cap, 2, 'Y');
    $ar['t_colli_t'] = "Capacit� {$tot_cap}, {$diff_val}, {$p_cap}%";
    
    if(trim($row['LINEA']) == '')
        $ar['non_assegnato'] = "<b>{$capac_c['COLLI_T']}</b>";
    else
        $ar[trim($row['LINEA'])] = "<b>{$capac_c['COLLI_T']}</b>";
        $ar[trim($row['LINEA'])."_c"] = $capac_c['c_color'];
        $ar[trim($row['LINEA'])."_t"] = $capac_c['tooltip'];
        
    $nr['k_ordine_out'] = "<b>Totale ".print_date($filtro['da_data'])."</b>";
    $nr['t_colli'] += $capac['COLLI_T'];
    
    $tot_cap1 += $capac['capac'];
    $diff_val1 = $nr['t_colli'] - $tot_cap1;
    if($tot_cap1 != 0)
        $p_cap1 = $diff_val1/$tot_cap1 * 100;
        
        $diff_val1 = n($diff_val1, 0, 'Y');
    $p_cap1 = n($p_cap1, 2, 'Y');
    $nr['t_colli_t'] = "Capacit� {$tot_cap1}, {$diff_val1}, {$p_cap1}%";
    
    if(trim($row['LINEA']) == '')
        $nr['non_assegnato'] = "<b>{$capac['COLLI_T']}</b>";
    else
        $nr[trim($row['LINEA'])] = "<b>{$capac['COLLI_T']}</b>";
        
        $nr[trim($row['LINEA'])."_c"] = $capac['c_color'];
        $nr[trim($row['LINEA'])."_t"] = $capac['tooltip'];
    
}


$ret[] = $ar;
$ret[] = $nr;
echo acs_je($ret);
exit;
    
}
// ******************************************************************************************
// JSON GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
    ini_set('memory_limit', '2048M');
	//bug parametri linux
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['window_post'] = strtr($_REQUEST['window_post'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));	
	
	$m_params = (array)json_decode($_REQUEST['window_post']);
	$m_params['filtro'] = (array)$m_params['filtro'];
    $filtro = array();	

	if(isset($m_params['filtro']['gruppo']))
	    $filtro['gruppo'] = trim($m_params['filtro']['gruppo']);
    if(isset($m_params['filtro']['liv']))
        $filtro['liv'] = trim($m_params['filtro']['liv']);
    
    $filtro['da_data'] = trim($m_params['filtro']['da_data']);
   
    if($filtro['liv'] == 'liv_2'){
        $sql_join_ta =  "  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIPC
        ON PL.PLDT = TA_LIPC.TADT AND TA_LIPC.TAID = 'LIPC' AND TA_LIPC.TANR = PL.PLFAS10  ";
        if(trim($filtro['gruppo']) != 'NON ASSEGNATO')
            $sql_where .= " AND TA_LIPC.TACINT = " .sql_t(trim($filtro['gruppo']));
        else
            $sql_where .= " AND TA_LIPC.TACINT IS NULL";
    }
    if($filtro['liv'] == 'liv_3'){
        $sql_where .= " AND PLFAS10 = " .sql_t(trim($filtro['gruppo']));
    }

    if (strlen(trim($filtro['da_data'])) > 0)
        $sql_where .= " AND TDDTEP = " .sql_t(trim($m_params['filtro']['da_data']));
            
    $sql_join_on_PL0 = "ON PLDT=TDDT AND PLTIDO=TDOTID AND PLINUM=TDOINU AND PLAADO=TDOADO AND digits(PLNRDO)=TDONDO";
    if ($cfg_mod_Spedizioni['disabilita_digits_on_PL0'] == 'Y')
        $sql_join_on_PL0 = "ON PLDT=TDDT AND PLTIDO=TDOTID AND PLINUM=TDOINU AND PLAADO=TDOADO AND PLNRDO=TDONDO";
     
    $params = acs_m_params_json_decode();
        
    $sql = "SELECT TDDOCU, TA_LIPC.TACINT AS LINEA, TDDCON, TDSTAT, TDOTPD, TDDTDS, TDCLOR,
            TDBLEV, TDBLOC,
            SUM(CASE WHEN PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_T
            FROM {$cfg_mod_Spedizioni['file_colli']} PL
            INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
            {$sql_join_on_PL0}
            {$sql_join_ta}
            WHERE TDDOCU IN (
              SELECT DISTINCT TDDOCU
              FROM {$cfg_mod_Spedizioni['file_colli']} PL
              INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
              {$sql_join_on_PL0}
              {$sql_join_ta}
              WHERE TDDT = '{$id_ditta_default}' AND TDDTDS <= {$m_params['filtro']['c_data']}
              {$sql_where}
            )
            GROUP BY TDDOCU, TA_LIPC.TACINT, TDDCON, TDSTAT, TDOTPD, TDDTDS, TDCLOR, TDBLEV, TDBLOC 
            ORDER BY TDDOCU";
               
                     
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
	  
	$ar = array();	
	while ($row = db2_fetch_assoc($stmt)) {
	    
	    //stacco dei livelli
	    $cod_liv0 = trim($row['TDDOCU']);
	     
	    $tmp_ar_id = array();
	    $ar_r= &$ar;
	    
        $cp = new SpedCapacitaProduttiva;
        $capac =  $cp->get_tot_by_data_linea($m_params['filtro']['c_data'], trim($row['LINEA']), array('IE' => 'I', 'ar_tddocu' =>$params->k_list));
        $array_tot['k_ordine_out'] = "<b>TOTALE</b>";
        $array_tot['k_ordine'] = "N";
        $array_tot[trim($row['LINEA'])] = "<b>{$capac['COLLI_T']}</b>";
        $array_tot[trim($row['LINEA'])."_c"] = $capac['c_color'];
	       
	    $liv =$cod_liv0;
	    $tmp_ar_id[] = $liv;
	    if (!isset($ar_r["{$liv}"])){
	        $ar_new = $row;
	        $ar_new['children'] = array();
	        $ar_new['id'] = implode("|", $tmp_ar_id);
	        $r_ord = $main_module->k_ordine_td_decode(trim($row['TDDOCU']));
	        $ar_new['k_ordine_out'] = $r_ord['TDOADO']."_".$r_ord['TDONDO'];
	        $ar_new['k_ordine'] = trim($row['TDDOCU']);
	        $ar_new['cliente'] = trim($row['TDDCON']);
	        $ar_new['stato'] = trim($row['TDSTAT']);
	        if(trim($row['TDSTAT']) == 'CC')
	            $ar_new['s_red'] = 'Y';
	        $ar_new['tipo'] = trim($row['TDOTPD']);
	        $ar_new['atp'] = trim($row['TDDTDS']);
	        $ar_new['raggr'] =  trim($row['TDCLOR']);
	        $ar_new['fl_bloc']  = $main_module->get_ord_fl_bloc($row);
	        $ar_new['expanded'] = true;
	        $ar_new['liv'] = 'liv_3';
	        if (is_array($params->k_list))
	           $ar_new['selected'] = in_array(trim($row['TDDOCU']), $params->k_list);
	        $ar_r["{$liv}"] = $ar_new;	        	        
	    }
	    $ar_r = &$ar_r["{$liv}"];
	    $ar_r['t_colli']  += $row['COLLI_T'];
	    if(trim($row['LINEA']) == '')
	        $ar_r['non_assegnato'] = $row['COLLI_T'];
	    else{
	        $ar_r[trim($row['LINEA'])] = $row['COLLI_T'];
	        $ar_r[trim($row['LINEA'])."_c"] = $array_tot[trim($row['LINEA'])."_c"];
	        
	    }
	   
	}
	
	//array_unshift($ar, $array_tot);

    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je($ret);
    exit;
}
?>



