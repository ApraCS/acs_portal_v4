<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// exe ASSEGNA INDICE ROTTURA 
// ******************************************************************************************
if ($_REQUEST['fn'] == 'assegna_indice_rottura'){
	$sir = new SpedIndiciRottura();	
	$ret = array();
	$ret['success'] = true;

	if ($m_params->f_indice_rottura == 'COD') //indice di rottura personalizzato
		$indice_rottura = $m_params->f_codice;
	else
		$indice_rottura = $m_params->f_indice_rottura;

	if ($_REQUEST['imposta_seq'] == 'Y') {
		//azzero prima tutti gli indici di rottura dell'indice di rottura per ripartire con la numerazione
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDSELO='' WHERE TDIRLO = ? AND " . $s->get_where_std();
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($indice_rottura));		
	}
	
	//assegno indice di rottura
	foreach((array)$m_params->list_selected_id as $tddocu){
		$sir->assegna_indice_rottura($tddocu, $indice_rottura, $_REQUEST['imposta_seq']);	
	}
	
	//scrivo eventuale messaggo END_INRO (fine scrittura indice rottura, per rigenerazione documenti su gest)
	global $cfg_mod_Spedizioni;
	if ($cfg_mod_Spedizioni['scrivi_END_indice_rottura'] == 'Y')
	 $sir->scrivi_END_indice_rottura($indice_rottura);
	
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// exe ASSEGNA INDICE ROTTURA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'remove_indice_rottura'){
	$sir = new SpedIndiciRottura();
	$ret = array();
	$ret['success'] = true;

	foreach((array)$m_params->list_selected_id as $tddocu){			
		$sir->remove_indice_rottura($tddocu);
	}

	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// DATI PER GRID ultimi indici rottura
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_last_indici_rottura'){
	$sir = new SpedIndiciRottura();
	$ultimi = $sir->get_last_indici_rottura();
	echo acs_je($ultimi);
	exit;
}


// ******************************************************************************************
// CREO NUOVO PROGRESSIVO INDICE ROTTURA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'create_new_indice_rottura'){
	$sir = new SpedIndiciRottura();
	$nuovo_indice = $sir->create_new_indice_rottura($m_params->f_anno_create_new_indice_rottura);
	$ret = array();
	$ret['success'] = true;
	$ret['indice'] = $nuovo_indice;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// CREO NUOVO PROGRESSIVO INDICE ROTTURA SECONDARIO (PER MINILOTTO)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'create_new_indice_rottura_secondario'){
	$sir = new SpedIndiciRottura();
	$nuovo_indice = $sir->create_new_indice_rottura($m_params->f_anno_create_new_indice_rottura, 'SECONDARIO', 500);
	$ret = array();
	$ret['success'] = true;
	$ret['indice'] = $nuovo_indice;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// SALVO DESCRIZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'salva_descrizione'){
	$sir = new SpedIndiciRottura();
	$data = $sir->salva_descrizione($m_params);
	$ret = array();
	$ret['success'] = true;
	$ret['data'] = $data;
	$ret['indice'] = $sir->out_codice($data);
	echo acs_je($ret);
	exit;
}




$list_selected_id = $m_params->list_selected_id;

//ricavo elenco ordini selezionati (TDDOCU in base a selezione su Day)
$ordini_selezionati = $s->get_el_ordini_by_day_list_select($list_selected_id);




function out_array_hidden_selected_id($list){
	$ret = array();
	foreach ($list as $l) {
		$ret[] = array('xtype' => "hidden", 'name'=>"list_selected_id", 'value'=>$l);
	}
	return implode(',' , array_map('json_encode', $ret));
}



?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
					layout: {
					    type: 'vbox',
					    align: 'stretch',
					    pack : 'start',
					},            
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'assegna_indice_rottura'
                	}
                	
					 , <?php echo out_array_hidden_selected_id($ordini_selezionati) ?>                	
			  	 
					 
					 , {
								name: 'f_indice_rottura',
								xtype: 'combo',
								fieldLabel: 'Indice produzione',
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
							    anchor: '-15',
							    forceSelection:true,
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
							    	data: [								    
								      <?php echo acs_ar_to_select_json($s->find_TA_std('INRO'), ""); ?>	
								    ]
								}, 
							listeners: {
        						select: function(ele, rec, idx) {
        							value = ele.value;
        							if (value == 'COD')
        								visible_personalizzato = true;
        							else
        							  	visible_personalizzato = false;
        							
        							
        								//VALORE PERSONALIZZATO
        								form = this.up('form').getForm();
										form.findField("f_codice").setVisible(visible_personalizzato);
										form.findField("f_descrizione").setVisible(visible_personalizzato);
        								this.up('form').down('#panelSelectIndiceRottura').setVisible(visible_personalizzato);
        								this.up('form').down('#b_salva_descrizione').setVisible(visible_personalizzato);
        								
        								if (visible_personalizzato == true)
        									this.up('form').down('grid').store.load();
        						}
    						}
						 },  

					 , {
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
						items: [
								 {
									name: 'f_codice',
									hidden: true,
									xtype: 'textfield',
									fieldLabel: 'Codice',
									value: '', maxLength: 10,
								    anchor: '-15',
								    readOnly: true
								 }
						]
					}	
					 
					 , {
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
						items: [
							 {
								name: 'f_descrizione',
								hidden: true,
								xtype: 'textfield',
								fieldLabel: 'Descrizione',
								value: '', maxLength: 100,
							    flex: 1,
							    readOnly: false,
							    allowBlank: false
							 }, 
								{
			                     xtype: 'button',
				            	 scale: 'medium',
				            	 hidden: true,
				            	 itemId: 'b_salva_descrizione',
				            	 iconCls: 'icon-save-24',
			                     text: 'Salva',
						            handler: function() {
								            	var form = this.up('form').getForm();
								            	indici_grid = this.up('form').down('grid');
								            	if(form.isValid()){
													Ext.Ajax.request({
													        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=salva_descrizione',
													        jsonData: form.getValues(),
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){	            	  													        
													            var jsonData = Ext.decode(result.responseText);
													            form.findField("f_codice").setValue(jsonData.indice);
													            form.findField("f_descrizione").setValue(jsonData.data.PSDESC);
													         	indici_grid.store.load();	
													        },
													        failure    : function(result, request){
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });
													}													    
						            }
						         }							 
							 
						]
					 }
					
					
					
					//grid e bottone per selezione codice
	
					, {				
				    xtype: 'panel',
				    hidden: true,
				    itemId: 'panelSelectIndiceRottura',
				    flex: 1,
					layout: {
					    type: 'vbox',
					    align: 'stretch',
					    pack : 'start',
					},
				   items: [
						{
							xtype: 'gridpanel',
							title: 'Elenco indici di produzione',
							flex: 1,
							
					  	    store: Ext.create('Ext.data.Store', {
										autoLoad:false,				        
					  					proxy: {
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_last_indici_rottura',
												type: 'ajax',
												reader: {
										            type: 'json',
										            root: 'root'
										        }
											},
						        			fields: ['INDICE', 'DESCRIZIONE', 'DATA']
						    			}),
						    			
							        columns: [
							             {
							                header   : 'Data',
							                dataIndex: 'DATA', 
							                widht: 80,
							                renderer: date_from_AS
							             },{
							                header   : 'Indice',
							                dataIndex: 'INDICE', 
							                widht: 100
							             },
										 {
							                header   : 'Descrizione',
							                dataIndex: 'DESCRIZIONE', 
							                flex: 1
							             }										             
							         ]	    					
							
							, listeners: {		
									  celldblclick: {								
										  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
										  	indice = iView.getSelectionModel().getSelection()[0].get('INDICE');
										  	descrizione = iView.getSelectionModel().getSelection()[0].get('DESCRIZIONE');
										  	form = this.up('form').getForm();
										  	form.findField("f_codice").setValue(indice);
										  	form.findField("f_descrizione").setValue(descrizione);
										  }
									  }	 			
						 			 			
								}
							  
							  
							  
								, dockedItems: [{
					                dock: 'right',
					                xtype: 'toolbar',
					                scale: 'large',
					                align: 'right',
					                border: true,
					                items: [{
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-button_black_repeat-32',
					                     text: 'Aggiorna elenco',
								            handler: function() {
								            	this.up('grid').store.load();
								            }
								         }, {
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-sub_black_add-32',
					                     text: 'Nuovo indice primario',
								            handler: function() {
								            	var form = this.up('form').getForm();
								            	indici_grid = this.up('grid');
												Ext.Ajax.request({
												        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=create_new_indice_rottura',
												        jsonData: form.getValues(),
												        method     : 'POST',
												        waitMsg    : 'Data loading',
												        success : function(result, request){	            	  													        
												            var jsonData = Ext.decode(result.responseText);
												            form.findField("f_codice").setValue(jsonData.indice);
												            form.findField("f_descrizione").setValue('');
												            
												            form.findField("f_descrizione").emptyText = 'Inserisci e salva descrizione indice rottura';
															form.findField("f_descrizione").applyEmptyText();
															
															form.isValid();
												            
												         	indici_grid.store.load();	
												        },
												        failure    : function(result, request){
												            Ext.Msg.alert('Message', 'No data to be loaded');
												        }
												    });								            	
								            	
								            }
								         }, {
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-sub_grey_add-32',
					                     text: 'Nuovo indice secondario',
								            handler: function() {
								            	var form = this.up('form').getForm();
								            	indici_grid = this.up('grid');
												Ext.Ajax.request({
												        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=create_new_indice_rottura_secondario',
												        jsonData: form.getValues(),
												        method     : 'POST',
												        waitMsg    : 'Data loading',
												        success : function(result, request){	            	  													        
												            var jsonData = Ext.decode(result.responseText);
												            form.findField("f_codice").setValue(jsonData.indice);
												            form.findField("f_descrizione").setValue('');
												            
												            form.findField("f_descrizione").emptyText = 'Inserisci e salva descrizione indice rottura';
															form.findField("f_descrizione").applyEmptyText();
															
															form.isValid();
												            
												         	indici_grid.store.load();	
												        },
												        failure    : function(result, request){
												            Ext.Msg.alert('Message', 'No data to be loaded');
												        }
												    });								            	
								            	
								            }
								         }, {
												name: 'f_anno_create_new_indice_rottura',
												xtype: 'textfield', labelAlign: 'top',
												fieldLabel: 'Anno',
												maxLength: 4,
											    allowBlank: true,
											    padding: '10 10 10 10'
												<?php											    
												 	$day 	= oggi_AS_date();
												  	$year 	= substr($day, 0, 4);
												 ?>
												 , value: <?php echo $year; ?>
											    
											 }
								    ]
								 }
								]    
							  
							         
						}				   
				   
				   ]
				   }					
					
					
					 
					 
					 
				],
			buttons: [
			
			{
	            text: 'Cancella indice',
				scale: 'large',
				iconCls: 'icon-sub_red_delete-32',	            
	            handler: function() {
	            	var form = this.up('form').getForm();
		            loc_win = this.up('window');	            	
		              
		              

	            	
	            	  //recupero il numeratore	            	 
								Ext.Ajax.request({
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=remove_indice_rottura',
								        jsonData: form.getValues(),
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){	            	  													        
								            var jsonData = Ext.decode(result.responseText);
								            loc_win.fireEvent('onClose', loc_win);	
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								            console.log('errorrrrr');
								        }
								    });	            	  
	            	  return;

            	                	                
	            }
	        }
	        
	        , {xtype: 'component', flex: 1}
			
			, {
	            text: 'Conferma',
				scale: 'large',
				iconCls: 'icon-button_blue_play-32',	            
	            handler: function() {
	            	var form = this.up('form').getForm();
		            loc_win = this.up('window');	            	

		              
		              if (Ext.isEmpty(form.findField("f_indice_rottura").getValue())){
		              	acs_show_msg_error('Selezionare una tipologia di indice di produzione');
		              	return false;
		              }
		              
		              if (form.findField("f_indice_rottura").getValue() == 'COD'){ //codice personalizza
			              if (Ext.isEmpty(form.findField("f_codice").getValue())){
			              	acs_show_msg_error('Selezionare un codice rottura personalizzato');
			              	return false;
			              }		              
		              }
		              
		              <?php foreach($m_params->list_selected_id as $k=>$v){
		                  $k_docu = $v->k_ordine;
		                  $ord = $s->get_ordine_by_k_docu($k_docu);
		                  if(trim($ord['TDNRLO']) != 0){?>
		                  		acs_show_msg_error('Impossibile effettuare l\'operazione. <br> Almeno un ordine ha gi� un lotto assegnato');
			              		return false;
		                  <?php     
		                  }
		              }?>
		              
               
	            	  //recupero il numeratore	            	 
								Ext.Ajax.request({
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=assegna_indice_rottura',
								        jsonData: form.getValues(),
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){	            	  													        
								            var jsonData = Ext.decode(result.responseText);
								            loc_win.fireEvent('onClose', loc_win);	
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								            console.log('errorrrrr');
								        }
								    });	            	  
	            	  return;

            	                	                
	            }
	        }, {
	            text: 'Conferma/Assegna ID Ordine',
				scale: 'large',
				iconCls: 'icon-button_black_play-32',	            
	            handler: function() {
	            	var form = this.up('form').getForm();
		            loc_win = this.up('window');	            	

		              
		              if (Ext.isEmpty(form.findField("f_indice_rottura").getValue())){
		              	acs_show_msg_error('Selezionare una tipologia di indice di produzione');
		              	return false;
		              }
		              
		              if (form.findField("f_indice_rottura").getValue() == 'COD'){ //codice personalizza
			              if (Ext.isEmpty(form.findField("f_codice").getValue())){
			              	acs_show_msg_error('Selezionare un codice rottura personalizzato');
			              	return false;
			              }		              
		              }
		              
		                        
		              <?php foreach($m_params->list_selected_id as $k=>$v){
		                  $k_docu = $v->k_ordine;
		                  $ord = $s->get_ordine_by_k_docu($k_docu);
		                  if(trim($ord['TDNRLO']) != 0){ ?>
		                  		acs_show_msg_error('Impossibile effettuare l\'operazione. <br> Almeno un ordine ha gi� un lotto assegnato');
			              		return false;
		                  <?php     
		                  }
		              }?>
		              

	            	
	            	  //recupero il numeratore	            	 
								Ext.Ajax.request({
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=assegna_indice_rottura&imposta_seq=Y',
								        jsonData: form.getValues(),
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){	            	  													        
								            var jsonData = Ext.decode(result.responseText);
								            loc_win.fireEvent('onClose', loc_win);	
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								            console.log('errorrrrr');
								        }
								    });	            	  
	            	  return;

            	                	                
	            }
	        }],             
				
        }
]}