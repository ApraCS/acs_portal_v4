<?php

require_once "../../config.inc.php";
require_once("../../utility/function_chart.php");

$s = new Spedizioni();

function get_ordini_oltre_max_attesa($tdstat, $gg_max_attesa){
	
	global $cfg_mod_Spedizioni, $conn;
	
	$sql = "SELECT COUNT(*) AS CONTEGGIO
			FROM {$cfg_mod_Spedizioni['file_testate']}
			WHERE TDSTAT= '{$tdstat}' AND TDGGAN> {$gg_max_attesa}";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$r = db2_fetch_assoc($stmt);

	return $r['CONTEGGIO'];
}



if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();
	$m_params = acs_m_params_json_decode();

	$sql = "SELECT TDSTAT, TDDSST, TA_STGAT.TAVOLU AS GG_MAX_ATTESA, COUNT (TDSTAT) AS CONTEGGIO,  MAX(TDGGAN) AS VECCHIO
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_STGAT  ON TD.TDDT=TA_STGAT.TADT AND TD.TDSTAT=TA_STGAT.TAKEY1 AND TA_STGAT.TATAID='STGAT'
	WHERE ". $s->get_where_std()."
    GROUP BY TDSTAT, TDDSST, TA_STGAT.TAVOLU";
	
	$ar_sql = array();

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_sql);
	

	while ($row = db2_fetch_assoc($stmt)) {
		$nr = array();
		$nr['TDSTAT'] 		= acs_u8e(trim($row['TDSTAT']));
		$nr['TDDSST']		= acs_u8e(trim($row['TDDSST']));
		$nr['CONTEGGIO']	= trim($row['CONTEGGIO']);
		$nr['VECCHIO']		= trim($row['VECCHIO']);
		$nr['GG_MAX_ATTESA']		= trim($row['GG_MAX_ATTESA']);
		$nr['ordini_supero_gg'] = get_ordini_oltre_max_attesa($row['TDSTAT'] , (int)$row['GG_MAX_ATTESA']);
		
		if ($nr['ordini_supero_gg'] > 0){
		
		$nr['liv'] = 'liv_3';
		}
		
		$data[] = $nr;
	}

	echo acs_je($data);
	exit;
}

if ($_REQUEST['fn'] == 'get_json_data_grid2'){
	$data = array();
	$m_params = acs_m_params_json_decode();

	$sql = "SELECT TDSTAT, TDOADO, TDONDO, TDDCON, TDGGAN, TA_STGAT.TAVOLU AS GG_MAX_ATTESA
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_STGAT  ON TD.TDDT=TA_STGAT.TADT AND TD.TDSTAT=TA_STGAT.TAKEY1 AND TA_STGAT.TATAID='STGAT'
	WHERE TDSTAT= '{$m_params->stato}' ORDER BY TDGGAN DESC FETCH FIRST 20 ROWS ONLY"; 

	$ar_sql = array();

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_sql);

	while ($row = db2_fetch_assoc($stmt)) {
		$nr = array();
		$nr['TDSTAT']	= acs_u8e(trim($row['TDSTAT']));
		$nr['TDOADO']	= trim($row['TDOADO']);
		$nr['TDGGAN']	= trim($row['TDGGAN']);
		$nr['TDONDO']	= trim($row['TDONDO']);
		$nr['TDDCON']	= acs_u8e(trim($row['TDDCON']));
		$nr['GG_MAX_ATTESA']		= trim($row['GG_MAX_ATTESA']);
	
		if ($nr['TDGGAN'] > (int)$nr['GG_MAX_ATTESA']){
			$nr['liv'] = 'liv_3';
		}
	
		$data[] = $nr;
	}

	echo acs_je($data);
	exit;
}

// ******************************************************************************************
// DATI PER CHART2 (area o stab)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_chart1'){

	$data = array();
	$m_params = acs_m_params_json_decode();

	
	$sql = "SELECT {$m_params->campo_cod} AS COD, {$m_params->campo_desc} AS DESC, COUNT (*) AS ORD_RIT
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_STGAT  ON TD.TDDT=TA_STGAT.TADT AND TD.TDSTAT=TA_STGAT.TAKEY1 AND TA_STGAT.TATAID='STGAT'
	WHERE TDGGAN > TA_STGAT.TAVOLU
	GROUP BY {$m_params->campo_cod}, {$m_params->campo_desc}";
	
	$ar_sql = array();
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_sql);
	
	while ($row = db2_fetch_assoc($stmt)) {
		$nr = array();
	
		if (trim($row['COD']) == ''){
			$nr['COD']= "ND";
		}else{
			$nr['COD']	= acs_u8e(trim($row['COD']));
		}
	
		if (trim($row['DESC']) == ''){
			$nr['DESC']= "Non Definito";
		}else{
			$nr['DESC']	= acs_u8e(trim($row['DESC']));
		}
	
		$nr['NR']	= trim($row['ORD_RIT']);
	
	
		$data[] = $nr;
	}
	
	echo acs_je($data);
	exit;
}

?>
{"success":true, "items": [

 {
         xtype: 'panel',
         title: '[RI] Stato ordine'
         width:70,
          layout : {
               type :'vbox',
               pack: 'start', 
               align: 'stretch'
            },
          items : [  {
				         xtype: 'panel',  //panel1 sopra
				         autoScroll: true,
				         flex:3, 
				         layout : {
				               type :'hbox' 
				            },
				          items : [{
				                
				               xtype: 'grid',				               
								
								store: {
										xtype: 'store',
										autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['TDSTAT', 'TDDSST', 'CONTEGGIO', 'VECCHIO', 'GG_MAX_ATTESA', 'ordini_supero_gg', 'liv']							
									
			}, //store
				
	        columns: [	
				{
	                header   : 'Stato',
	                dataIndex: 'TDSTAT',
	                width: 50 
	            },{
	                header   : 'Descrizione',
	                dataIndex: 'TDDSST',
	                flex: 10
	            },{
	                header   : 'Num',
	                dataIndex: 'CONTEGGIO',
	                flex: 4
	            },{
	                header   : 'Old',
	                dataIndex: 'VECCHIO',
	                renderer: floatRenderer0N,
	                flex: 5
	            },{
	                header   : 'Max GG',
	                dataIndex: 'GG_MAX_ATTESA',
	                renderer: floatRenderer0N,
	                flex: 5
	            }, {
	                header   : 'Ordini >',
	                dataIndex: 'ordini_supero_gg',
	                renderer: floatRenderer0N,
	                flex: 5,
	             
	            }
	         ], listeners: {

                  itemclick: {								
						  fn: function(iView,rec,item,index,eventObj){
						  	grid_dettagli_riga = iView.up('panel').up('panel').getComponent('dettaglio_per_riga');
						  	grid_dettagli_riga.store.proxy.extraParams.stato = rec.get('TDSTAT');
						  	grid_dettagli_riga.store.load();
						  }
					  }	 
                    } ,  viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           
			    
			           return ret;																
			         }   
			    },
	
				               flex : 1
				            }
				            
				            ,{
				              
				               xtype: 'grid', //panel 2
				               autoScroll: true,
				               flex : 1, 
							 itemId: 'dettaglio_per_riga',
								store: {
										xtype: 'store',
										autoLoad:false,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid2',  
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							           extraParams: {
										stato: ''
			        				},
			        				
			        				 doRequest: personalizza_extraParams_to_jsonData,	
								
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['TDSTAT', 'TDOADO', 'TDGGAN', 'TDONDO', 'TDDCON', 'liv']							
									
			}, //store
				
	        columns: [	
				{
	                header   : 'Stato',
	                dataIndex: 'TDSTAT',
	                width: 50
	            },{
	                header   : 'Anno',
	                dataIndex: 'TDOADO',
	                flex: 3
	            },{
	                header   : 'Giorni',
	                dataIndex: 'TDGGAN',
	                flex: 3,
	                renderer: floatRenderer0N,
	           
	            },{
	                header   : 'Numero',
	                dataIndex: 'TDONDO',
	                flex: 3
	            },{
	                header   : 'Cliente',
	                dataIndex: 'TDDCON',
	                flex: 10
	            }

	         ],  viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           
			    
			           return ret;																
			         }   
			    }	,
	
				               
				            }]
				            
				            
				        },  {
				        
				         xtype: 'panel', // parte sotto
				         flex : 2,
				         layout : {
				               type :'hbox' 
				            },
				            
				          items : [ {
				          
				         xtype: 'panel',
				         flex : 1,
				         layout : {
				               type :'hbox' 
				            },
				          items : [{	
				          
				          		
				xtype: 'panel', // primo grafico a torta
				rowspan: 1,
				layout: 'fit', 
				flex: 1,
				title: 'Stato Ordini in ritardo per stato',
				items: [				
				 <?php echo crea_json_chart("TDSTAT", "TDDSST")?>
				 					
				 //chart1
				 
				
				]
				}, //panel torta area di spedizione
		            
				            {	
				          
				          		
				xtype: 'panel', // primo grafico a torta
				rowspan: 1,
				layout: 'fit', 
				flex: 1,
				title: 'Stato Ordini in ritardo per referente',
				items: [				
				 <?php echo crea_json_chart("TDCORE", "TDDORE")?>
				 					
				 //chart2
				 
				
				]
				}]
				            },
				            {
				          
				         xtype: 'panel',
				         flex : 1,
				         layout : {
				               type :'hbox' 
				            },
				          items : [{
				               title: 'Panel 3',
				               html : 'Panel with flex 1',
				               flex : 1
				            },{
				               title: 'Panel 3.2',
				               html : 'Panel with flex 1',
				               flex : 1
				            }]
				            }]
				        
				        } 

				  ]
		
		
		 }		        

     ]
        
 }