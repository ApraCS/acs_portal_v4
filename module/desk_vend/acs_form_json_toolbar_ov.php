<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			cls: 'acs_toolbar',
			tbar: [{
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Order Entry',
			            items: [
			      {
			                text: 'Tipologie<br>ordine',
			                scale: 'large',			                 
			                iconCls: 'icon-blog_compose-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new TipologieOrdine(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "tipologieordine") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "tipologieordine") ?>
								<?php echo $cl->out_Writer_Model("tipologieordine") ?>
								<?php echo $cl->out_Writer_Store("tipologieordine") ?>
								<?php echo $cl->out_Writer_sotto_main("tipologieordine") ?>
								<?php echo $cl->out_Writer_main("Lista tipologie ordine", "tipologieordine") ?>								
								<?php echo $cl->out_Writer_window("Tabella tipologie ordine", "SETUP_TIP_ORD") ?>							

								} //handler function()
			            },

			            {
			                text: 'Parametri<br/>operatore OE',
			                scale: 'large',			                 
			                iconCls: 'icon-design-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new SpedParametriOperatoriOE(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "parametrioperatorioe") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "parametrioperatorioe") ?>
								<?php echo $cl->out_Writer_Model("parametrioperatorioe") ?>
								<?php echo $cl->out_Writer_Store("parametrioperatorioe") ?>
								<?php echo $cl->out_Writer_sotto_main("parametrioperatorioe", 0.4) ?>								
								<?php echo $cl->out_Writer_main("Lista parametri operatori Order Entry", "parametrioperatorioe") ?>								
								<?php echo $cl->out_Writer_window("Tabella parametri operatori Order Entry") ?>							

								} //handler function()
			            },	
			             {
			                text: 'Ticket fuori<br> standard',
			                scale: 'large',			                 
			                iconCls: 'icon-receipt-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								 acs_show_panel_std('acs_ticket_fuori_std.php?fn=open_tab', 'panel_gestione_ticket');
    		    					panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			           
			            			            
			    	]
			   },
			    {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Email',
			            items: [{
			                xtype:'splitbutton',
			                iconCls: 'icon-email_send-32',
			                text: 'Ricezione OE',
			                scale: 'large',
			                iconAlign: 'top',
			                arrowAlign:'bottom',
			                menu: {
			                	xtype: 'menu',
		        				items: [	
		        				  	
            						{
            							xtype: 'button',
            				            text: 'Profili di ricezione',
            				            iconCls: 'icon-email_send-16', 
            				            scale: 'small',
            				            handler: function() {
            				            	  acs_show_panel_std('acs_gest_EMPRF.php?fn=open_tab', 'panel_gestione_EMPRF', "");	          	                	                
            				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
            				           			panel.close();
            				            }
            				        },{
            							xtype: 'button',
            				            text: 'Abilitazione a profilo di accesso',
            				            iconCls: 'icon-user-16', 
            				            scale: 'small',
            				            handler: function() {
            				            	  acs_show_panel_std('acs_gest_OEPRU.php?fn=open_tab', 'panel_gestione_OEPRU', "");	          	                	                
            				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
            				           			panel.close();
            				            }
            				        }
		        				 ]
			                
			                }
			            }]
			        }
			]
		}
	]		
}            