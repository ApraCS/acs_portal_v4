<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

	$sottostringhe = explode("|", $_REQUEST["m_id"]);

	$liv1 = $sottostringhe[2];
	$liv2 = $sottostringhe[4];
	$liv3 = $sottostringhe[6];
	$liv3_explode = explode("-", $liv3);
	$filtro = array();
	
	if (strlen($_REQUEST['sped_ar']) > 0)
		$filtro['ar_spedizioni'] = explode(",", $_REQUEST['sped_ar']);
	

	switch ($_REQUEST["tipo_cal"])
	{
		case "per_data":
			$filtro["data_spedizione"] = substr($liv2, 0, 8);		
			$filtro["itinerario"] 	= trim($liv3_explode[0]);
			break;
		case "per_sped":
			$sped = $s->get_spedizione($_REQUEST["sped_id"]);
			$filtro["data_spedizione"]  = $sped['CSDTSP'];
			$filtro["itinerario"] 		= $sped['CSCITI'];
			break;			
		case "iti_data":
			$filtro["data_spedizione"] 	= $_REQUEST['data'];			
			$filtro["itinerario"] 		= $_REQUEST['cod_iti'];					
			break;			
		default:	
			$filtro["itinerario"] = $liv2;
			$filtro["data_spedizione"] = trim($liv3_explode[0]);						
	}
	

		if (strlen($_REQUEST["da_data"])==8 && substr($_REQUEST["col_name"], 0, 2)=="d_"){
			$add_day = (int)substr($_REQUEST["col_name"], 2, 2) - 1;
			$m_data = date('Ymd', strtotime($_REQUEST["da_data"] . " +{$add_day} days"));			
			$filtro["data_spedizione"] = $m_data;			
		}
		
		
		//se ho aperto il livello degli ordini
		//if (($row['TDSECA'] . "___" . $row['TDCCON'] . "___" . $row['TDCDES'])  != $liv2_v){
		if (strlen($_REQUEST["node"]) > 0 && $_REQUEST["node"] != "root" && $_REQUEST["node"] != ''){
			$sottostringhe_root = explode("|", $_REQUEST["node"]);
			$liv2_root_explode = explode("___", $sottostringhe_root[4]);
			$filtro['seca'] = $liv2_root_explode[0];			
			$filtro['cliente'] = $liv2_root_explode[1];						
			$filtro['destinazione'] = $liv2_root_explode[2];			
		}
				
		
		
		//solo ordini programmati
		$filtro["TDSWSP"] = "Y";

		$tipo_elenco = 'DAY';
		
		$titolo_panel = $s->get_titolo_panel_by_tipo($tipo_elenco);		
		$titolo_panel .= " " . implode(' ', array(date('d/m', strtotime($filtro["data_spedizione"])), $filtro["itinerario"], $filtro["vettore"]));

		$tbar_title = 'Elenco ordini per data di produzione/disponibilit&agrave;';
		$m_data_per_tbar = date('d/m/y', strtotime($filtro["data_spedizione"] . " +0 days"));		
		switch($tipo_elenco){
			case 'DAY': $tbar_title = "Elenco ordini programmati per itinerario e singola data produzione/disponibilit&agrave; alla spedizione ({$m_data_per_tbar})"; break;
		}
		
		
		$items 	= $s->get_elenco_ordini($filtro); 
		$ars_ar = $s->crea_array_valori_el_ordini($items);
		$ars 	= $s->crea_alberatura_el_ordini_json($ars_ar, $_REQUEST["node"], $filtro["itinerario"], $titolo_panel, $tbar_title, $tipo_elenco, null, $filtro["data_spedizione"]);
		echo $ars;

		$appLog->save_db();		
		


function date_from_as($num)
{
 return substr($num, 6, 2) . "/" . substr($num, 4,2) . "/" . substr($num, 2, 2);	
}

?>
