<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************

function parametri_sql_where($form_values){

	global $s;

	$sql_where.= " WHERE " . $s->get_where_std();
	
	$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);

	$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);

	$sql_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);

	$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);
	
	$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values ->f_cliente);

	//controllo HOLD
	$filtro_hold=$form_values->f_hold;

	if($filtro_hold== "Y")
		$sql_where.=" AND TDSWSP = 'Y'";
		if($filtro_hold== "N")
			$sql_where.=" AND TDSWSP = 'N'";

			$filtro_evasi=$form_values->f_evasi;

	  //solo ordini evasi
			if ($filtro_evasi == "Y"){
				$sql_where.= " AND TDSWPP = 'Y' AND TDFN11 = 1 ";
			}
			//solo ordini da evadere
			if ($filtro_evasi == "N"){
				$sql_where.= " AND TDSWPP = 'Y' AND TDFN11 = 0 ";
			}

			return $sql_where;
}

if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();
     $m_params = acs_m_params_json_decode();
     $form_values=$m_params->form_values;

    $sql_where=parametri_sql_where($form_values);
    
    $stato_selected=$m_params->stato_selected;
    $data_selected=$m_params->data_selected;
    $id_scaduti_rit=$m_params->id_scaduti_rit;
    $cliente_selected=$m_params->cliente_selected;
    
    $scaduti_rit= explode("|", $id_scaduti_rit);
    $intervallo_rit= $scaduti_rit[1];
    $periodo= explode("_", $intervallo_rit);
    $periodo_da= $periodo[1];
    $periodo_a= $periodo[3];
    
 
    if ($stato_selected != ""){
    	$sql_where.= " AND TDSTAT ='{$stato_selected}' ";
    	
    }
    
    if ($cliente_selected != ""){
    	$sql_where.= " AND TDCCON ='{$cliente_selected}' ";
    }
    
    
    
    if (strpos($id_scaduti_rit, 'SCADUTI') !== false) {
    	
    	$sql_where.= " AND TDDTEP < {$form_values->f_data}";    	
    	
    	if($periodo_da > 0)
    		$sql_where.= " AND TDGGRI >{$periodo_da}";
    	
    	 $sql_where.= " AND TDGGRI <= {$periodo_a} ";
    	
    }else{
    	$sql_where.= " AND TDDTEP >={$form_values->f_data} ";
    	
    	if ($data_selected != ""){
    		$sql_where.= " AND TDDTEP ={$data_selected} ";
    	}
    }
    

    
	

	$sql = "SELECT TD.*, RD.RDQTA AS PROD, RD_M.RDQTA AS MAGAZ
	FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
       LEFT OUTER JOIN (
	      SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
	      FROM {$cfg_mod_Spedizioni['file_righe']} RDO
	      WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
	      GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO) RD
	    ON TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO
	      LEFT OUTER JOIN (
	      SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
	      FROM {$cfg_mod_Spedizioni['file_righe']} RDO
	      WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE <>'PRODUZIONE'
	      GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO) RD_M
	    ON TDDT=RD_M.RDDT AND TDOTID=RD_M.RDOTID AND TDOINU=RD_M.RDOINU AND TDOADO=RD_M.RDOADO AND TDONDO=RD_M.RDONDO
    $sql_where ORDER BY TDDTEP DESC";



			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
	

		while ($row = db2_fetch_assoc($stmt)) {
			$nr = array();
			$nr['TDDCON'] 		= acs_u8e(trim($row['TDDCON']));
			$nr['localita'] 	= acs_u8e(trim($s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD'])));
			$nr['TDVSRF']		= acs_u8e(trim($row['TDVSRF']));
			$nr['k_ordine']		= trim($row['TDDOCU']);
			$nr['ordine_out']	= $s->k_ordine_out($row['TDDOCU']);
			$nr['tipoOrd']		= trim($row['TDOTPD']);
			$nr['fl_cli_bloc']	= trim($row['TDFN02']);
			$nr['fl_evaso']		= trim($row['TDFN11']);
			$nr['fl_bloc']		= $s->get_ord_fl_bloc($row);
			$nr['cons_prog']	= $row['TDDTEP'];
			$nr['colli_sped'] 	= $row['TDCOSP'];
			$nr['colli'] 	    = $row['TDTOCO'];
			$nr['colli_prod']   = $row['PROD'];
			$nr['magazzino']    = $row['MAGAZ'];
			$nr['gg_ritardo'] 	= $row['TDGGRI'];
			$nr['proforma'] 	= trim($row['TDPROF']);
			$nr['stato'] 		= trim($row['TDSTAT']);
			$nr['raggr'] 		= trim($row['TDCLOR']);
			$nr['var1']			= acs_u8e(trim($row['TDDVN1']));
			$nr['k_carico']		= $s->k_carico_td($row);
			$nr['liv']			= 'liv_2'; //serve per assegna carico
			$nr['fl_cons_conf'] = $row['TDFN06'];
			
			
			$sr = new SpedAutorizzazioniDeroghe();
			if ($sr->ha_RIPRO_by_k_ordine($row['TDDOCU'])){
				$nr['ripro']	= 'Y';
				$ar_tooltip_ripro = $sr->tooltip_RIPRO_by_k_ordine($row['TDDOCU']);
				$nr['ripro_tip'] = implode("<br>", $ar_tooltip_ripro);
			}
				
			
			if ((int)$row['TDNRCA'] > 0)
				$nr['carico'] 		= implode("_", array($row['TDAACA'], trim($row['TDNRCA'])));

				if ((int)$row['TDNRLO'] > 0)
					$nr['lotto'] 		= implode("_", array($row['TDAALO'], trim($row['TDNRLO'])));

					$nr['rif_scarico']		= trim(acs_u8e($row['TDRFCA']));
					$nr['indice_rottura'] = trim($row['TDIRLO']);
					$nr['fl_art_manc'] = $s->get_fl_art_manc($row);
					$nr['art_da_prog'] = $s->get_art_da_prog($row);
					$nr['TDVOLU'] 	   = $row['TDVOLU'];
					$nr['TDSELO'] 	   = trim($row['TDSELO']);

					if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
						$nr['cod_articolo'] = trim(acs_u8e($row['RDART']));
						$nr['des_articolo'] = trim(acs_u8e($row['RDDES1']));
						$nr['um'] 			= trim(acs_u8e($row['RDUM']));
						$nr['qta'] 			= trim(acs_u8e($row['RDQTA']));
					}

					$data[] = $nr;
		}

		echo acs_je($data);

		exit;
}


$m_params = acs_m_params_json_decode();
?>
{"success": true, "items":
	{
		xtype: 'gridpanel',
		multiSelect: true,
		
        stateful: true,
        stateId: 'seleziona-ordini-colli',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
        
        features: [
	
		{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 	type: 'boolean',
			dataIndex: 'visible'
		     }
		      ]
		}],
		
  	    store: Ext.create('Ext.data.Store', {
													
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							type: 'ajax',
							actionMethods: {
								read: 'POST'
							},							
							reader: {
					            type: 'json',
					            root: 'root'
					        }
					        
					        , extraParams: <?php echo acs_raw_post_data(); ?>
					        
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
							, doRequest: personalizza_extraParams_to_jsonData					        
					        
						},
	        			fields: [
	            			'TDCCON', 'TDDCON', 'TDVOLU', 'TDSELO', 'fl_bloc', 'fl_cli_bloc', 'localita', 'TDVSRF', 'k_ordine', 'ordine_out', 
	            			'tipoOrd', 'cons_prog', 'fl_evaso', 'colli_sped', 'proforma', 'stato', 'raggr', 'var1', 'cod_articolo','des_articolo', 
	            			'um', 'qta', 'k_carico', 'colli', 'fl_bloc', 'iconCls', 'liv', 'fl_cons_conf', 'carico', 'lotto', 'rif_scarico', 
	            			'indice_rottura', 'fl_art_manc', 'art_da_prog', 'gg_ritardo', 'ripro', 'ripro_tip', 'colli_prod', 'magazzino'
	        			]
	    			}),
	    			
		        columns: [{
						    text: '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> height=25>',
						    width: 30, tooltip: 'Clienti bloccati', dataIndex: 'fl_cli_bloc', 
					    	tdCls: 'tdAction',         			
			            	menuDisabled: true, sortable: false,            		        
							renderer: function(value, p, record){if (record.get('fl_cli_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
							
					  }, 
		             {
		                header   : 'Cliente',
		                dataIndex: 'TDDCON', 
		                flex: 1
		                , filter: {type: 'string'}, filterable: true
		             },			        
		             {
		                header   : 'Localit&agrave;',
		                dataIndex: 'localita', 
		                flex: 1
		                , filter: {type: 'string'}, filterable: true
		             },			        
		             {
		                header   : 'Riferimento',
		                dataIndex: 'TDVSRF', 
		                width     : 110
		                , filter: {type: 'string'}, filterable: true
		             }, {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Ordini bloccati', dataIndex: 'fl_bloc', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';			    				    	
    			    	}},	
    			    	
    			    	
			    	    {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', 
			    	    		dataIndex: 'art_mancanti', menuDisabled: true, sortable: false,
			    	    	    tooltip: 'Ordini MTO',    	    		    
			    			    renderer: function(value, p, record){
			    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
			    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
			    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=18>';
			    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
			    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';			    	
			    			    	}},			    	
			    	    {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
			        			dataIndex: 'art_da_prog',
			    	    	    tooltip: 'Articoli mancanti MTS', menuDisabled: true, sortable: false,        					    	     
			    			    renderer: function(value, metaData, record ){
			    			    	if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
			    			    	if (record.get('art_da_prog')==2) return '<img src=<?php echo img_path("icone/48x48/warning_red.png") ?> width=18>';    			    	
			    			    	if (record.get('art_da_prog')==3) return '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=18>';
			    			    	
			    			    	if (record.get('ripro')== 'Y') {
			    			    	  
			    			    	    metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('ripro_tip')) + '"';
			    			    		return '<img src=<?php echo img_path("icone/48x48/button_black_repeat_dx.png") ?> width=18>';
			    			    	
			    			    	}
			    			    	
			    			    	}
			    			    	
			    			    	
			    			    	},    			    	
    			    	
    			    	
					 {text: '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Consegna confermata', dataIndex: 'fl_cons_conf', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    	if (record.get('fl_cons_conf')==1) return '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=18>';
    			    	}},    			    	
    			    	
    			    			        
		             {
		                header   : 'Ordine',
		                dataIndex: 'ordine_out', 
		                width     : 110
		                , filter: {type: 'string'}, filterable: true
		             },			        
		             {
		                header   : 'Tp', tooltip: 'Tipo',
		                dataIndex: 'tipoOrd',
		                tdCls: 'tipoOrd', 
		                width     : 30,
		                renderer: function (value, metaData, record, row, col, store, gridView){						
							metaData.tdCls += ' ' + record.get('raggr');										
							return value;			    
						}
						, filter: {type: 'string'}, filterable: true
		             }, 
		             {text: 'Vol.',	width: 60, dataIndex: 'TDVOLU', renderer: floatRenderer1, align: 'right'},
		             {text: 'Colli',	width: 50, dataIndex: 'colli', align: 'right'},
		             {text: 'Colli <br> Prod.',	width: 50, dataIndex: 'colli_prod', align: 'right', renderer: floatRenderer0},
		              {text: 'Colli <br> Rivend.',	width: 50, dataIndex: 'magazzino', align: 'right', renderer: floatRenderer0},
		             {text: 'Prod./<br>Disp.sped.',	width: 65, dataIndex: 'cons_prog', renderer: date_from_AS},
		             {text: 'GG <br>Rit.',	width: 50, dataIndex: 'gg_ritardo'},
		             {text: 'St.',	width: 30, dataIndex: 'stato', tooltip: 'Stato ordine'},
		             		             		             
		             {text: 'Variante',	width: 100, dataIndex: 'var1' 		<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo ", hidden: true" ?>, filter: {type: 'string'}, filterable: true},
		             {text: 'Proforma',	width: 100, dataIndex: 'proforma'	<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo ", hidden: true" ?>, filter: {type: 'string'}, filterable: true},
		             		        
<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             {text: 'Articolo',		width: 80, dataIndex: 'cod_articolo'},
		             {text: 'Descrizione',	width: 120, dataIndex: 'des_articolo'},
		             {text: 'UM',	width: 40, dataIndex: 'um'},
		             {text: 'Q.t&agrave;',	width: 80, dataIndex: 'qta', renderer: floatRenderer0, align: 'right'},
<?php }?>		             		        
		             		            
		             {text: 'Carico',		width: 90, dataIndex: 'carico', filter: {type: 'string'}, filterable: true},
		             {text: 'Rifer.<br/>Scar.',		width: 45, dataIndex: 'rif_scarico', filter: {type: 'string'}, filterable: true},
		             {text: 'Ind.<br/>Prod.',	width: 50, dataIndex: 'indice_rottura', filter: {type: 'string'}, filterable: true},
		             {text: 'Rif.<br/>Prod.',	width: 50, dataIndex: 'TDSELO', filter: {type: 'string'}, filterable: true},
		             {text: 'Lotto',		width: 90, dataIndex: 'lotto', filter: {type: 'string'}, filterable: true}		             
		         ]	    					
		
		, listeners: {		
		
		
		
				celldblclick: {								

					fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	
						if (col_name == 'colli'){
							iEvent.preventDefault();							
							ar_params = {k_ordine: rec.get('k_ordine')};							
							acs_show_win_std('Riepilogo avanzamento spunta colli', 'acs_panel_avanzamento_produzione.php?fn=get_avanzamento_lotto_grid', ar_params, 500, 500, null, 'icon-box_open-16')
							return false;
						}
						
					  	
				    }
				},		
		
		
		
	 			afterrender: function (comp) {
	 				//comp.up('window').setWidth(950);
	 				//comp.up('window').setHeight(500);
					//window_to_center(comp.up('window'));
					comp.up('window').setTitle('<?php echo "Elenco ordini"; ?>');	 				
	 			}
	 			
	 			
	 			
		  , itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
		  		
			  	id_selected = grid.getSelectionModel().getSelection();
			  	list_selected_id = [];
			  	for (var i=0; i<id_selected.length; i++) 
				   list_selected_id.push({k_ordine: id_selected[i].get('k_ordine'), liv: 'liv_3'});
		  														  
				  var voci_menu = [];
				
				
			  voci_menu.push({
				         		text: 'Visualizza righe',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {k_ordine: rec.get('k_ordine')}, 900, 450, null, 'icon-folder_search-16');          		
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    			
			  }	
		 			
	 			
	 			
	        , itemclick: function(view,rec,item,index,eventObj) {
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');  
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');      	

	        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdi.store.load();	        	
	  			wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdc.store.load();
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				      //wd.setSource(src.dettagli);
				   }
				});
				
				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}				
				
	         }	 			
	 			
	 			 			
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {		        	
		         }   
		    }
		    
		
		, dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-print-32',
                     text: 'Stampa',
			            handler: function() {
	        				Ext.ux.grid.Printer.print(this.up('grid'));
			            }
			     }, {
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-delivery-32',
                     //text: 'Riepilogo spedizioni',
			            handler: function() {
			            
			            var grid = this.up('grid');
			            all_rows =   grid.getStore().getRange();
                        list_rows = [];
                        
                        Ext.each(all_rows, function(item) {
    							// add the fields that you want to include
    						var Obj = {
       							 k_carico: item.get('k_carico')
    								};
    					
   						list_rows.push(Obj); // push this to the array
						}, this);
	
	
						var list_unique=[];
							
							function get_unique_from_array_object(array,property){
    							var unique = {};
    							var distinct = [];
    							for( var i in array ){
       								if( typeof(unique[array[i][property]]) == "undefined"){
          								distinct.push(array[i]);
      								 }
       							unique[array[i][property]] = 0;
    									}
    							return distinct;
										}


                         list_unique=get_unique_from_array_object(list_rows,'k_carico');
	   		          
	        			    acs_show_win_std('Carichi per data','acs_riepilogo_spedizioni_carichi.php?', {list_rows:list_unique, tipo_elenco: 'DA_DISP_COLLI'}, 700, 500, {}, 'icon-delivery-16');
			            }
			     }]
		   }]



	}
}