
<?php   

require_once "../../config.inc.php";


$s = new Spedizioni();
$main_module = new Spedizioni();

function somma_valori(&$ar, $r){
	
	global $s;
	
	/*if($ar['pref']=='N' || $r['TDFU03']=='N'){    //se metto P=N o gi� lo � riporto nei livelli sopra la N
		$ar['pref']='N';
		$ar['expanded']	= false;
		
	}*/
	
	$ar['imp'] += $r['TDTIMP'];
	
	$ar['volume'] 	+= $r['TDVOLU'];
	$ar['colli_tot'] 	+= $r['TDTOCO'];
	$ar['colli_disp'] 	+= $r['TDCOPR'];
	$ar['colli_sped'] 	+= $r['TDCOSP'];
	$ar['fl_bloc'] = $s->get_fl_bloc($ar['fl_bloc'], $s->calc_fl_bloc($r));
	$ar['fl_art_manc'] =max($ar['fl_art_manc'], $s->get_fl_art_manc($r));
	

	if($r['TDFU03']=='C'){											//'C' -> tutto in produzione
		$ar['imp_mob'] += $r['MOBILI'];
		$ar['imp_mob'] += $r['ELETR'];
	} else if($r['TDFU03']!='N' && trim($r['TDFU03'])!=''){			//'N' -> non considero gli importi
		$ar['imp_mob'] += $r['MOBILI'];
		$ar['imp_el'] += $r['ELETR'];
	}
	
	return $ar;
}



if ($_REQUEST['fn'] == 'exe_aggiorna_preferenza'){

	$m_params = acs_m_params_json_decode();
	

	$all_rows=$m_params->all_rows;
	
	foreach ($all_rows as $v){


		$ex_pref=$s->get_ordine_by_k_docu($v->k_ordine);

		$sh = new SpedHistory();
		$sh->crea(
				'aggiorna_preferenza',
				array(
			
						"pref" 			=> $v->new_pref,  //la mia nuova preferenza
						"k_ordine"		=> $v->k_ordine, //il tddocu 
						"ex"		    => "Ex: " .$ex_pref['TDFU03'], //la preferenza precedente
				)
				);
		
	}
  exit;
}



if ($_REQUEST['fn'] == 'get_data_tree'){
	
	$m_params = acs_m_params_json_decode();
	
	
	$form_values=$m_params->open_request->form_values;
	//$pagamento=array();
	$pagamento=$form_values->f_pagamento;
	
	if(is_null($pagamento)){
		$pagamento=array();
	}
	
	
	//controllo opzione programmato o spedizione
	if($form_values->data_riferimento == "TDDTEP"){
		$tipo_data="TDDTEP";}
	if($form_values->data_riferimento == "TDDTSP"){
		$tipo_data="TDDTSP";}
	if($tipo_data==0){
				$tipo_data="TDDTEP";
			}
	
			$sql_where.= " WHERE " . $s->get_where_std();
	
			$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);
	
			$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);
	
			$sql_where.= sql_where_by_combo_value('TD.TDCITI', $form_values ->f_itinerario);
			
			$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values ->f_cliente);
			
			$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values ->f_cliente);
	
			//$sql_where .=sql_where_by_combo_value('CS.CSDTRG', $form_values->f_data);
	
			
	
			//controllo opzione ordini con carico
			if ($form_values->f_solo_ordini_con_carico == 'Y')
				$sql_where .= " AND TD.TDFN01 = 1 ";
	
	
				if(count($m_params->open_request->list_selected_carico)>0){
					
				
					$sql_where.= " AND ( 1=2 ";
					foreach($m_params->open_request->list_selected_carico as $c){
	
						$car = explode("_", $c->num_carico);
	
						$anno_carico= $car[0];
						$num_carico= $car[2];
						
						$num_spe = $c->nr_spe;
						$area_spe = $c->area_spe;
						
	
						$sql_where.= " OR (TD.TDAACA = {$anno_carico} AND TD.TDNRCA= {$num_carico} AND TD.TDNBOC={$num_spe}
						 AND TD.TDASPE='{$area_spe}')";
	
					}
						
					$sql_where.= ")";
						
				}
				
			
			$sql= "SELECT TD.*, RD2.RDQTA2 AS MOBILI, RD2.RDQTA3 AS ELETR
			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
		    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.{$tipo_data}
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				     ON TD.TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
				      LEFT OUTER JOIN (
                               SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
                               FROM {$cfg_mod_Spedizioni['file_righe']} RDO
                               WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
                               GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO) RD
                      ON TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO
                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']}
                ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe']} RD2
					ON TDDT=RD2.RDDT AND TDOTID=RD2.RDOTID AND TDOINU=RD2.RDOINU AND TDOADO=RD2.RDOADO AND TDONDO=RD2.RDONDO AND RD2.RDTPNO='RGMOB'
                $sql_where ORDER BY CS.CSDTRG DESC, TDSECA, TDDCON, TDDLOC";
			
  /* print_r($sql);
    exit;*/
			
                
    $stmt = db2_prepare($conn, $sql);	
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
 
   
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		
	    $liv1 = implode("_", array($r['TDAACA'], $r['TDTPCA'], $r['TDNRCA']));		
		$liv2 = $r['TDCCON'];		
		$liv3 = implode("_", array($r['TDOADO'], $r['TDONDO']));		
		
		$t_ar = &$ar;

		
		$ha_commenti = $s->has_commento_ordine_by_k_ordine($s->k_ordine_td($r), 'NDPRE');
	
		//carico
		$l = $liv1;
		if (!isset($t_ar[$l]))
			$t_ar[$l] = array("cod" => $l, "descr"=>$liv1, "children"=>array());
			somma_valori($t_ar[$l], $r);
			$t_ar[$l]['liv'] = 'liv_1';
			$t_ar[$l]['expanded']	= true;
			$t_ar = &$t_ar[$l]['children'];
				
			
		 								  								  
		//cliente
		$l = $liv2;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$r['TDDCON'], "children"=>array());
 			$t_ar[$l]['riferimento']=$s->scrivi_rif_destinazione($r['TDLOCA'], $r['TDNAZI'], $r['TDPROV'], $r['TDDNAZ']);
 			$t_ar[$l]['pref_anag']=$r['TDFU04'];
 			$t_ar[$l]['seq_scar']=$r['TDSECA'];
 			$t_ar[$l]['codice_pagam']=trim($r['TDCPAG']);
 			
 			if (in_array($t_ar[$l]['codice_pagam'], $pagamento)) {
 				$t_ar[$l]['evidenzia_giallo']='Y';
 			}
 			
 			if(trim($r['TDFU03'])!='') {
 				$t_ar[$l]['pref']= '<img class="cell-img" src=' . img_path("icone/48x48/currency_black_euro.png") . ' width= 18> ';
 				$t_ar[$l]['expanded']	= true;
 			
 			}
 			
 	
 			if ($ha_commenti == TRUE)
 				$t_ar[$l]['descr'] = trim($r['TDDCON']) . '<span style="display: inline; float: right;"><img class="cell-img" src=' . img_path("icone/16x16/comment_edit_yellow.png") . '></span>';

 			somma_valori($t_ar[$l], $r);
 			$t_ar[$l]['liv'] = 'liv_2';
 			$t_ar = &$t_ar[$l]['children'];	
			

		//ordine
		$l = $liv3;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			//$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar[$l]['k_ordine']=$r['TDDOCU'];
		    $t_ar[$l]['riferimento']=trim($r['TDVSRF']);
			$t_ar[$l]['tipo']=$r['TDOTPD'];
			$t_ar[$l]['data_reg']=$r['TDODRE'];
			$t_ar[$l]['stato']=$r['TDSTAT'];
			
			$t_ar[$l]['pagam']=$r['TDDPAG'];
			$t_ar[$l]['codice_pagam']=trim($r['TDCPAG']);
			
			if (in_array($t_ar[$l]['codice_pagam'], $pagamento)) {
				$t_ar[$l]['evidenzia_giallo']='Y';
			}
			
			/*print_r(in_array($t_ar[$l]['codice_pagam'], $pagamento));
			exit;*/
			
			$t_ar[$l]['pref']=trim($r['TDFU03']);
			$t_ar[$l]['pref_anag']=$r['TDFU04'];
			$t_ar[$l]['volume']=$r['TDVOLU'];
			$t_ar[$l]['colli_tot']=trim($r['TDTOCO']);
			$t_ar[$l]['colli_disp']=trim($r['TDCOPR']);
			$t_ar[$l]['colli_sped']=trim($r['TDCOSP']);
			$t_ar[$l]['imp']=$r['TDTIMP'];
			$t_ar[$l]['imp_mob']=$r['MOBILI'];
			$t_ar[$l]['imp_el']=$r['ELETR'];
			$t_ar[$l]['val_mob']=$r['MOBILI'];
			$t_ar[$l]['val_el']=$r['ELETR'];
			$t_ar[$l]["raggr"] =  $r['TDCLOR'];
			$t_ar[$l]['fl_art_manc'] = $s->get_fl_art_manc($r);
			$t_ar[$l]['fl_bloc']		= $s->get_ord_fl_bloc($r);
			$t_ar[$l]['qtip_tipo'] = acs_u8e($r['TDDOTD']);
			$t_ar[$l]['qtip_tipo'] = acs_u8e($r['TDDSST']);
			$t_ar[$l]['liv'] = 'liv_3';
			//$t_ar = &$t_ar[$l]['children'];		
			$t_ar[$l]['leaf']=true;

			if ($ha_commenti == FALSE)
				$img_com_name = "icone/16x16/comment_light.png";
			else
				$img_com_name = "icone/16x16/comment_edit_yellow.png";
				//icona commento ordine
			$t_ar[$l]['descr'] .= '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine_bl(\'' . $r['TDDOCU'] . '\', \'' . 'NDPRE' . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
				
						
			
			
			if($t_ar[$l]['pref']=='N' || $t_ar[$l]['pref']==''){
				$t_ar[$l]['imp_mob']=0;
				$t_ar[$l]['imp_el']=0;
			}
			
			if($t_ar[$l]['pref']=='C'){											//'C' -> tutto in produzione
				$t_ar[$l]['imp_el'] = 0;
				$t_ar[$l]['imp_mob']  =  $r['MOBILI'] + $r['ELETR'];
			}

				
			
		
	} //while
	

	$ret = array();
	foreach($ar as $kar => $v){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
}




$m_params = acs_m_params_json_decode();
$primo_carico=$m_params->list_selected_carico[0]->num_carico;
$dettaglio_area=$m_params->list_selected_carico[0]->area_spe;
$dettaglio_itin=$m_params->list_selected_carico[0]->itin;
$dettaglio_data=$m_params->list_selected_carico[0]->data;
$titolo_panel = "Delivery ".$primo_carico;
$dettagli="[".$dettaglio_area." - ".$dettaglio_itin."] - [".print_date($dettaglio_data)."]";
if(count($m_params->list_selected_carico)> 1){
	$titolo_panel .="...";
	$dettagli="[".$dettaglio_area." - ".$dettaglio_itin."...] - [".print_date($dettaglio_data)."...]";
}


//print_r($pagamento);

?>


{"success":true, "items": [

        {
            xtype: 'treepanel' ,
	        title: '<?php echo  $titolo_panel;  ?>' ,
	        tbar: new Ext.Toolbar({
	            items:['<b>Elenco spedizioni programmate per sequenza di scarico <?php echo  $dettagli;  ?> </b>', '->'
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	    
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,            
			//multiSelect:true,
			singleExpand: false,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                                
				    fields: ['descr', 'riferimento', 'tipo', 'data_reg', 'stato', 'imp', 'raggr', 
				    'qtip_tipo', 'pagam', 'pref', 'pref_anag', 'volume', 'k_ordine', 'fl_bloc', 
				    'fl_art_manc', 'seq_scar', 'colli_tot', 'colli_disp', 'colli_sped', 'liv', 
				    'imp_mob', 'imp_el', 'val_mob', 'val_el', 'evidenzia_giallo'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tree',
						actionMethods: {read: 'POST'},
						
						extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>
			        				},
                        
                       	reader: {
                            root: 'children'
                        },
                       	                      
                    	doRequest: personalizza_extraParams_to_jsonData        				
                    }

                }),
				

		columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'descr', 
			            flex: 2,
			            dataIndex: 'descr',
			            menuDisabled: true, sortable: false,
			            header: 'Carico/Cliente/Ordine',
			                renderer: function (value, metaData, record, row, col, store, gridView){
			                
								if (record.get('liv') == 'liv_2' || record.get('liv') == 'liv_3')
				  	  				 metaData.tdCls += ' auto-height';			                
				                						
							    if (record.get('pref') != "N" && record.get('pref').trim() != "" && record.get('liv') == 'liv_3')	    			    	
							    	metaData.tdCls += ' grassetto';
							  	
							    return value;
	    					}
			        },   
			        {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini bloccati',
	    			    renderer: function(value, metaData, record){
	    			    
	    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
	    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
	    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
	    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';    			    				    	
	    			    	}},		
	    	    {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', 
	    	    		dataIndex: 'art_mancanti',	  tooltip: 'Ordini MTO',       
	    			    renderer: function(value, p, record){
	    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
	    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
	    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=18>';
	    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
	    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';			    	
	    			    	}},
	    			     { 
			            dataIndex: 'seq_scar',
			            header: 'Seq.', 
			            align: 'right',
			             width: 33
			        },
			        {text: 'Localit&agrave; /Riferimento', 	
			        flex: 1,
			        dataIndex: 'riferimento'},
			        
			          {text: 'Tp',
			          width: 30, 
			          dataIndex: 'tipo', 
			          tdCls: 'tipoOrd', 
	        	     renderer: function (value, metaData, record, row, col, store, gridView){						
								metaData.tdCls += ' ' + record.get('raggr');
								
								if (record.get('qtip_tipo') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '"';								
																			
							return value;			    
	    			}},
	    	    {text: 'Data', 			width: 60, dataIndex: 'data_reg', renderer: date_from_AS},
	    	    {text: 'St', 			width: 30, dataIndex: 'stato',
	    			    renderer: function(value, metaData, record){
	    			    
							if (record.get('qtip_stato') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_stato')) + '"';	    			    
	    			    
							if (value == 'V') return '';
							if (value == 'R') return '';
							if (value == 'G') return '';
							if (value == 'B') return '';
							return value;
	    			    }	    	     
	    	    },{ 
			            dataIndex: 'colli_tot',
			            header: 'Colli', 
			            renderer: floatRenderer0,
			            align: 'right',
			             width: 50
			        }, {text: 'D',	width: 30, tdCls: 'tdAction',
    	    	 			menuDisabled: false, sortable: false, 
    			    renderer: function(value, p, record){
    			    	if (parseInt(record.get('colli_disp')) == 0 ) return '';
    			    	if (parseInt(record.get('colli_disp')) <  parseInt(record.get('colli_tot'))) return '<img src=<?php echo img_path("icone/48x48/star_none.png") ?> width=18>';			    	
    			    	if (parseInt(record.get('colli_disp')) >= parseInt(record.get('colli_tot'))) return '<img src=<?php echo img_path("icone/48x48/star_full_yellow.png") ?> width=18>';			    	
    			    	}},	    
    	    {text: 'S',	width: 30, tdCls: 'tdAction',
    			    		 menuDisabled: false, sortable: false,    			    	    
    			    renderer: function(value, p, record){
    			    	if (parseInt(record.get('colli_sped')) == 0) return '';
    			    	if (parseInt(record.get('colli_sped')) <  parseInt(record.get('colli_tot'))) return '<img src=<?php echo img_path("icone/48x48/star_none_black.png") ?> width=18>';			    	
    			    	if (parseInt(record.get('colli_sped')) >= parseInt(record.get('colli_tot'))) return '<img src=<?php echo img_path("icone/48x48/star_full_green.png") ?> width=18>';			    	
    			    	
    			    	}},
			        { 
			            dataIndex: 'imp',
			            header: 'Importo', 
			            renderer: floatRenderer2,
			            align: 'right',
			            width: 70,
			            renderer: function (value, metaData, record, row, col, store, gridView){						
						    if (record.get('pref') != "N" && record.get('pref').trim() != "" && record.get('liv') == 'liv_3')	    			    	
						    	metaData.tdCls = ' grassetto';
						  	
						    return floatRenderer2(value);
							
	    			}
			        },
			        { 
			            dataIndex: 'imp_mob',
			            header: 'Produzione', 
			            renderer: floatRenderer2,
			            align: 'right',
			            width: 80,
			            renderer: function (value, metaData, record, row, col, store, gridView){	
			            if (record.get('pref') != "N" && record.get('pref').trim() != "" && record.get('liv') == 'liv_3')	    			    	
						    	metaData.tdCls = ' grassetto';
			            					
						if ((record.get('pref') == "N" || record.get('pref').trim() == "") && record.get('liv') == 'liv_3' )	    			    	
								return '';
								else return floatRenderer2(value);
			    
	    			}
			        },{ 
			            dataIndex: 'imp_el',
			            header: 'Rivendita', 
			            renderer: floatRenderer2,
			            align: 'right',
			            width: 80,
			            renderer: function (value, metaData, record, row, col, store, gridView){
			            	
								if (record.get('pref') != "N" && record.get('pref').trim() != "" && record.get('liv') == 'liv_3')	    			    	
						    	metaData.tdCls = ' grassetto';
						    							
								if ((record.get('pref') == "N" || record.get('pref').trim() == "") && record.get('liv') == 'liv_3')	    			    	
								return '';
								else return floatRenderer2(value);
							
	    
	    			}
			        },{ 
			            dataIndex: 'pagam',
			            header: 'Pagamento', 
			            flex: 1,
			                renderer: function (value, metaData, record, row, col, store, gridView){						
						   if (record.get('pref') != "N" && record.get('pref').trim() != "" && record.get('liv') == 'liv_3')	    			    	
						    	metaData.tdCls += ' grassetto';

						 if (record.get('evidenzia_giallo')=='Y') 
						    metaData.tdCls += ' tpSfondoGiallo';			
							
						  	return value;
	    			}
			        },{ 
			            dataIndex: 'pref',
			            header: 'P', 
			            width: 30,	
			            renderer: function (value, metaData, record, row, col, store, gridView){						
								
								if (record.get('pref') == "N")	    			    	
									metaData.tdCls += ' tpSfondoRosa';	
								
								return value;						
								    
	    			}
			        },{ 
			            dataIndex: 'pref_anag',
			            header: 'PA', 
			            width: 30
			        },{ 
			            dataIndex: 'volume',
			            header: 'Vol.', 
			            align: 'right',
			            renderer: floatRenderer2,
			             width: 70
			        }
	         ],		//columns	
	         
	          dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [{
                     xtype: 'button',
					 text: 'Ultima preferenza confermata',
		            iconCls: 'icon-print-32',
		            scale: 'large',	                     
		            handler: function() {
		              
		              	var panel = this.up('treepanel');
			            	
						t_form = new Ext.FormPanel();
                   
                                t_form.submit({
                                        url: 'acs_riepilogo_spedizioni_report_pref.php',
                                        params: {                                          
                                        	open_request: Ext.encode(<?php echo acs_raw_post_data(); ?>)
                                        },	
                                        target: '_blank', 
	                        			standardSubmit: true,
	                        			method: 'POST'
                                });                   
                   
			
			            }

			     }, '->',{
                     xtype: 'button',
					 text: 'Conferma preferenza',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		              
		              	var panel = this.up('treepanel');
			            	
			            	  getDeepAllChildNodes = function(node){ 
       							 var allNodes = new Array(); 
        						 if(!Ext.value(node,false)){ 
                				return []; 
        									} 
        
        					if(!node.hasChildNodes()){ 
               					 return node; 
       						 }else{ 
               					 allNodes.push(node); 
                			node.eachChild(function(Mynode){
                			allNodes = allNodes.concat(getDeepAllChildNodes(Mynode));
                			});         
       							 } 
        							return allNodes; 
                              }

			            	
			            	
			            	getDeepAllLeafNodes = function(node,onlyLeaf){
       							 var allNodes = new Array();
        									if(!Ext.value(node,false)){
               									 return [];
        									}
       										if(node.isLeaf()){
   												return node;
       										}else{
   												node.eachChild(function(Mynode){
     											allNodes = allNodes.concat(getDeepAllChildNodes(Mynode));
   															 }
                                                  	);      
                                             }
                                          return allNodes;
                                             }	
                       
                                      
                                             				
                       var allChildNodes = getDeepAllLeafNodes(panel.getRootNode());
                      
                       
                      
  				      list_rows = [];
  				     for (var i=0; i<allChildNodes.length; i++) 
  				            if (allChildNodes[i].get('liv') == 'liv_3' &&  allChildNodes[i].get('pref') != ' ')
	   				        list_rows.push({k_ordine: allChildNodes[i].get('k_ordine'), new_pref: allChildNodes[i].get('pref')});
                   
                   
                    Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna_preferenza',
									        method     : 'POST',
						        			jsonData: {
						        				all_rows: list_rows
											},							        
									        success : function(result, request){
						            			panel.getStore().load();									        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
                   
			
			            }

			     }]
		   }]  ,
	         
	          listeners: {
	          
	          
	       		 celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.stopEvent();

						if (col_name=='colli_tot' && rec.get('liv')=='liv_3'){
					     iEvent.preventDefault();						
					     show_win_colli_ord(rec, rec.get('k_ordine'));
					     return false;
				        }
													
						return false;

					 }
					},
					
				itemcontextmenu : function(grid, rec, node, index, event) {	
				
				
				  event.stopEvent();			      
			      var voci_menu = [];
							      
	      		
								  if (rec.get('k_ordine').length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  }
							  
						 
						  if (rec.get('pref_anag').trim() == '')
							  pref_anag = 'vuoto';
						  else
							  pref_anag = rec.get('pref_anag');
							  
						<?php 
						if (isset($cambia_preferenza)){
						?>  

						  config_cambia_preferenza = <?php echo $cambia_preferenza ?>;
						  array_pref_ammesse = config_cambia_preferenza[pref_anag];
						  
						  for (var chiave in array_pref_ammesse) {
  
 
						  voci_menu.push({
				      		text: 'Preferenza ' + array_pref_ammesse[chiave],
				    		iconCls: 'iconArtCritici',
							val_to_set: array_pref_ammesse[chiave],
				    		handler: function() {
				    		
					    			rec.set('pref', this.val_to_set);
					    				
					    			if(rec.get('pref')=='N'|| rec.get('pref').trim() == ''){					    		
						    			rec.set('imp_el', 0);
						    			rec.set('imp_mob', 0);					    	
					    			} else if (rec.get('pref')=='C'){	//tutto in produzione
						    			rec.set('imp_el', 0);
						    			rec.set('imp_mob', rec.get('val_el') + rec.get('val_mob'));					    			
					    			} else {
						    			rec.set('imp_el', rec.get('val_el'));
						    			rec.set('imp_mob', rec.get('val_mob'));					    			
					    			}
					    		
					    		
					    		parentNode = rec.parentNode;   //cliente
					    	
					    		var somma_imp_el=0;
					    		var somma_imp_mob=0;
					    		
					    
					    
                           parentNode.eachChild(function(mynode){   
                         
						          
						        if(Ext.isEmpty(mynode.get('imp_el'))==false && Ext.isEmpty(mynode.get('imp_mob'))==false) {
						         
						      
								somma_imp_el += parseFloat(mynode.get('imp_el'));
								somma_imp_mob += parseFloat(mynode.get('imp_mob'));
								 }
								 
                                  }); 
                                  
                                  parentNode.set('imp_el', somma_imp_el);
								  parentNode.set('imp_mob', somma_imp_mob);
					
								  first_parent= parentNode.parentNode; //carico
								  
								  var somma_imp_el=0;
					    		var somma_imp_mob=0;
								  
								  
                              first_parent.eachChild(function(mynode){   
                         
						          
						        if(Ext.isEmpty(mynode.get('imp_el'))==false && Ext.isEmpty(mynode.get('imp_mob'))==false ) {
						         
						      
								somma_imp_el += parseFloat(mynode.get('imp_el'));
								somma_imp_mob += parseFloat(mynode.get('imp_mob'));
								 }
								 
                                  }); 
                                  
                                  first_parent.set('imp_el', somma_imp_el);
								  first_parent.set('imp_mob', somma_imp_mob);
								  
							
                                 
					    		
				    		}
						  });
						  
						  }
						  
						  <?php }
						  
						  else{ ?>
						  	
						  	acs_show_msg_error('Manca la configurazione, contattare l\'amministratore di sistema');
						  	
						  <?php 	
						  }
						  
						  ?>

				      
				      
				  		  voci_menu.push({
				         		text: 'Visualizza righe',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {k_ordine: rec.get('k_ordine')}, 900, 450, null, 'icon-folder_search-16');          		
				        		}
				    		});
				    		
				   voci_menu.push({
	          		text: 'Sincronizza',
	        		iconCls : 'iconRefresh',	          		
	        		handler: function() {

	        			
	       			 Ext.Ajax.request({
	      			   url: 'acs_op_exe.php?fn=sincronizza_ordine',
	      			   method: 'POST',
	      			   jsonData: {k_ordine: rec.get('k_ordine')}, 

	      			   success: function(response, opts) {
	      			   			grid.store.treeStore.load();
	      			   }
	      			});

	        			
	        		}
	    		});
				    		
				    						      
				      					      
				      
				      
				      
				      
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);		  
				
				
				}, //itemcontextmenu
				
				
			        itemclick: function(view,rec,item,index,eventObj) {
			          if (rec.get('liv') == 'liv_3')
			          {	
			        	var w = Ext.getCmp('OrdPropertyGrid');
			        	//var wd = Ext.getCmp('OrdPropertyGridDet');
			        	var wdi = Ext.getCmp('OrdPropertyGridImg');        	
			        	var wdc = Ext.getCmp('OrdPropertyCronologia');

			        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdi.store.load()
						wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdc.store.load()			        												        	
			        	
						Ext.Ajax.request({
						   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
						   success: function(response, opts) {
						      var src = Ext.decode(response.responseText);
						      w.setSource(src.riferimenti);
					          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);						      
						      //wd.setSource(src.dettagli);
						   }
						});
						
        				//files (uploaded) - ToDo: dry 
        				var wdu = Ext.getCmp('OrdPropertyGridFiles');
        				if (!Ext.isEmpty(wdu)){
        					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
        			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
        			        	if (wdu.isVisible())		        	
        							wdu.store.load();
        					}					
        				}							
						
					   }												        	
			         }		
				
 }	,	
 viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }
	        																			  			
	            
        }  

]
}