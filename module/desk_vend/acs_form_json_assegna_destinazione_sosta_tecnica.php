<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));



// ******************************************************************************************
// SAVE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save'){
	global $cfg_mod_Spedizioni;

	$destSostaTecnica = new SpedDestinazioneSostaTecnica();
	$destSostaTecnica->save($_REQUEST);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// CREATE NEW
// ******************************************************************************************
if ($_REQUEST['fn'] == 'create_new'){
	global $cfg_mod_Spedizioni;
	
	$destSostaTecnica = new SpedDestinazioneSostaTecnica();
	$destSostaTecnica->create_new($_REQUEST);
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
 exit;
}


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	global $cfg_mod_Spedizioni;
	$r = $_REQUEST;	
	//bug parametri linux
	global $is_linux;
	if ($is_linux == 'Y')
		$r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	
	$list_selected_id = json_decode($r['list_selected_id']);
	
	//recupero il codice cliente
	$k_cli_des = $list_selected_id[0]->k_cli_des;
	$k_cod_cli_exp = explode("_", $k_cli_des);
	$cod_cli = $k_cod_cli_exp[1];
	
	$destSostaTecnica = new SpedDestinazioneSostaTecnica();
	
	$ar_dest = $destSostaTecnica->get_per_cliente(
			$k_cod_cli_exp[0], $k_cod_cli_exp[1]
	);
	
	echo acs_je(array('root' => $ar_dest, 'properties' => $properties));
	
	exit;
}


//recupero il codice cliente
$k_cli_des = $m_params->list_selected_id[0]->k_cli_des;
$k_cod_cli_exp = explode("_", $k_cli_des);

?>



{"success":true, "items": [

        {
            xtype: 'grid',
			
			store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
			        		    	list_selected_id: '<?php echo $list_selected_id_encoded; ?>'
			        				},
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['cod', 'cod_cli', 'dt', 'descr', 'denom', 'IND_D', 'LOC_D', 'CAP_D', 'PRO_D', 'TEL_D', 'NAZIONE']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Codice',
	                dataIndex: 'cod',
	                width: 50 
	            }, {
	                header   : 'Denominazione',
	                dataIndex: 'denom',
	                flex: 10
	            }, {
	                header   : 'Indirizzo',
	                dataIndex: 'IND_D',
	                flex: 10
	            }, {
	                header   : 'CAP',
	                dataIndex: 'CAP_D',
	                width: 80
	            }, {
	                header   : 'Localit&agrave;',
	                dataIndex: 'LOC_D',
	                flex: 5
	            }, {
	                header   : 'Prov',
	                dataIndex: 'PRO_D',
	                width: 50
	            }, {
	                header   : 'Naz.',
	                dataIndex: 'NAZIONE',
	                width: 50
	            }, {
		            xtype:'actioncolumn', 
		            width:50,
		            items: [{
		                icon: <?php echo img_path("icone/16x16/keyboard.png") ?>,  // Use a URL in the icon config
		                tooltip: 'Modifica dati destinazione',
		                handler: function(grid, rowIndex, colIndex) {
		                		var rec = grid.getStore().getAt(rowIndex);
		                		var mgrid = grid;
                    			

								var win = new Ext.Window({
								  width: 800
								, height: 280
								, minWidth: 300
								, minHeight: 300
								, plain: true
								, title: 'Modifica definitiva dati destinazione sosta tecnica'
								, iconCls: 'icon-outbox-16'			
								, layout: 'fit'
								, border: true
								, closable: true
								, items: [
									{
										xtype: 'form',
									            bodyStyle: 'padding: 10px',
									            bodyPadding: '5 5 0',
									            frame: false,
									            title: '',
									            url: 'acs_print_wrapper.php',
												buttons: [{
										            text: 'Conferma',
										            iconCls: 'icon-sub_blue_accept-32',
										            scale: 'large',
										            handler: function() {
										                if (this.up('form').getForm().isValid()){										                
											                this.up('form').submit({
										                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save',
										                        method: 'POST',
										                        
														success: function (formPanel, action) {
														  win.close(),
										                  mgrid.store.load();
											                },										                        
										                        
										                  });

										                }
										            }
										        }],             								
										items: [
										 	{
							                    xtype: 'hiddenfield',
							                    name: 'dt',
							                    value: rec.get('dt')    
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'taid',
							                    value: 'VUDS'    
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'cod_cli',
							                    value: rec.get('cod_cli').trim()
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'cod_des',
							                    value: rec.get('cod')    
							                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '-5',
								                    name: 'denom1',
								                    value: Ext.util.Format.substr(rec.get('denom'), 0, 30).trim(),    
								                    fieldLabel: 'Denominazione',
								                    allowBlank: false,
								                    flex: 1,
								                    maxLength: 60
							                	}]
											}, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '0',							                    
								                    name: 'ind1',    
								                    value: Ext.util.Format.substr(rec.get('IND_D'), 0, 30).trim(),
								                    fieldLabel: 'Indirizzo',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 60
								                }]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'cap',    
							                    value: rec.get('CAP_D').trim(),
							                    fieldLabel: 'Cap',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 10
							                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',							                    
								                    name: 'loc1',    
								                    value: Ext.util.Format.substr(rec.get('LOC_D'), 0, 30).trim(),
								                    fieldLabel: 'Loc',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 60
							                	}]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'pro',    
							                    value: rec.get('PRO_D').trim(),
							                    fieldLabel: 'Provincia',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 2
							                }, {
							                    xtype: 'textfield',							                    
							                    name: 'tel',    
							                    value: rec.get('TEL_D').trim(),
							                    fieldLabel: 'Telefono',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 20
							                }, {
												name: 'nazione',
												xtype: 'combo',
												fieldLabel: 'Nazione',
												displayField: 'text',
												valueField: 'id',
												emptyText: '- seleziona -',
												forceSelection: true,
											   	allowBlank: false,
											    anchor: '-15',
											    value: rec.get('NAZIONE').trim(),							
												store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}],
												    data: [								    
													     <?php echo acs_ar_to_select_json($s->find_TA_std('NAZIO'), ""); ?>	
													    ] 
												}						 
											  }
										]
									}	
								
								] 
								});
								win.show();  
		                		
                    			
                    			
		                }
		            }]
		        }
	            
	         ],																					

	         
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
									Ext.Ajax.request({
									   url: 'acs_op_exe.php?fn=exe_assegna_destinazione_sosta_tecnica',
									   method: 'POST',
									   jsonData: {list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
									   			  tipo: '<?php echo $m_params->tipo; ?>',
									   			  to_destinazione: iView.getSelectionModel().getSelection()[0].data.cod
									   			  }, 
									   
									   
									   
									   success: function(response, opts) {					   				
					                                this.print_w.close();
								  				  	m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								  				  	if (m_grid.getStore().$className == 'Ext.data.NodeStore')
								  				  		m_grid.getStore().treeStore.load();
								  				  	else	
									   					m_grid.getStore().load();
									   }
									});						  	
						  	
						  	
						  }
					  }	  
			},
			
			
 			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-button_black_play-32',
                     text: 'Crea nuovo',
			            handler: function() {

			            	/* CREA NUOVO */
			            
		                		var mgrid = this.up('grid');
                    			
								// create and show window
								var win = new Ext.Window({
								  width: 800
								, height: 280
								, minWidth: 300
								, minHeight: 300
								, plain: true
								, title: 'Crea nuova destinazione sosta tecnica'
								, iconCls: 'icon-outbox-16'			
								, layout: 'fit'
								, border: true
								, closable: true
								, items: [
									{
										xtype: 'form',
									            bodyStyle: 'padding: 10px',
									            bodyPadding: '5 5 0',
									            frame: false,
									            title: '',
									            url: 'acs_print_wrapper.php',
												buttons: [{
										            text: 'Conferma',
										            iconCls: 'icon-sub_blue_accept-32',
										            scale: 'large',
										            handler: function() {
										                if (this.up('form').getForm().isValid()){										                
											                this.up('form').submit({
										                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=create_new',
										                        method: 'POST',
										                        
														success: function (formPanel, action) {
														  win.close(),
										                  mgrid.store.load();
											                },										                        
										                        
										                  });

										                }
										            }
										        }],             								
										items: [
										 	{
							                    xtype: 'hiddenfield',
							                    name: 'dt',
							                    value: <?php echo acs_je($k_cod_cli_exp[0]); ?>   
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'taid',
							                    value: 'VUDS'    
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'cod_cli',
							                    value: <?php echo acs_je(trim($k_cod_cli_exp[1])); ?>
							                }, {
								                    xtype: 'textfield',
								                    anchor: '-15',							                    
								                    name: 'cod_des',    
								                    fieldLabel: 'Codice',
								                    allowBlank: false,
								             		flex: 1,
								                    maxLength: 3
								                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '-5',
								                    name: 'denom1',    
								                    fieldLabel: 'Denominazione',
								                    allowBlank: false,
								                    flex: 1,
								                    maxLength: 100
							                	}]
											}, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '0',							                    
								                    name: 'ind1',    
								                    fieldLabel: 'Indirizzo',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 60
								                }]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'cap',    
							                    fieldLabel: 'Cap',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 10
							                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',							                    
								                    name: 'loc1',
								                    fieldLabel: 'Loc',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 60
							                	}]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'pro',
							                    fieldLabel: 'Provincia',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 2
							                }, {
							                    xtype: 'textfield',							                    
							                    name: 'tel',
							                    fieldLabel: 'Telefono',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 20
							                }, {
												name: 'nazione',
												xtype: 'combo',
												fieldLabel: 'Nazione',
												displayField: 'text',
												valueField: 'id',
												emptyText: '- seleziona -',
												forceSelection: true,
											   	allowBlank: false,
											    anchor: '-15',							
												store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}],
												    data: [								    
													     <?php echo acs_ar_to_select_json($s->find_TA_std('NAZIO'), ""); ?>	
													    ] 
												}						 
											  }                
										]
									}	
								
								] 
								});
								win.show(); 			            
			            
			            
			            
			             }                       
                     }, {
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-sub_red_delete-32',
                     text: 'Annulla destinazione sosta tecnica selezionata',
			            handler: function() {
							//RIPRISTINO DESTINAZIONE INIZIALE (CLIENTE)	            
			            
											Ext.Ajax.request({
											   url: 'acs_op_exe.php?fn=exe_assegna_destinazione_sosta_tecnica',
											   method: 'POST',
											   jsonData: {list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
											   			  to_destinazione: 'RIPRISTINO'
											   			  }, 
											   
											   success: function(response, opts) {					   				
					                                this.print_w.close();
								  				  	m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								  				  	if (m_grid.getStore().$className == 'Ext.data.NodeStore')
								  				  		m_grid.getStore().treeStore.load();
								  				  	else	
									   					m_grid.getStore().load();
											   	
											   }
											});	            
			            
			             }                       
                     }]
        		}],				
			
				  
	            
        }    

]}