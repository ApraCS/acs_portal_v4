<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();
$sped = $s->get_spedizione($m_params->sped_id);
if($sped['CSDTIC'] == 0)
  $data_carico = $sped['CSDTSP'];
else
  $data_carico = $sped['CSDTIC'];

  $area = trim($m_params->area);
  $carico = $s->get_el_carichi_by_TDNBOF($sped['CSPROG']);
  $itinerario = trim($s->decod_std('ITIN', $sped['CSCITI']));
  $title = "Modifica data/ora carico #{$sped['CSPROG']} [{$area}] {$carico} {$itinerario}" ;
  

if ($_REQUEST['fn'] == 'exe_modifica_sped'){
     
      $ar_upd = array();
      $ar_upd['CSDTIC'] = $m_params->form_values->f_data;
      if($m_params->form_values->f_ora > 0)
        $ar_upd['CSHMIC'] = $m_params->form_values->f_ora;
      $ar_upd['CSDESC'] = $m_params->form_values->f_descrizione;
      
      $sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} 
              SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
              WHERE CSDT = '{$id_ditta_default}' AND CSCALE = '*SPR' AND CSPROG = {$m_params->sped_id}";
     
      $stmt = db2_prepare($conn, $sql);
      echo db2_stmt_errormsg();
      $result = db2_execute($stmt, $ar_upd);
      echo db2_stmt_errormsg($stmt);

      
      $ret = array();
      $ret['success'] = true;
      echo acs_je($ret);
      exit;
  }

if ($_REQUEST['fn'] == 'open_form'){
    
    ?>
	    {"success":true, 
    	    m_win: {
    		title: <?php echo j($title)?>,
    		iconCls: 'icon-leaf-16'
    	    },
	    items: 
	    {
        xtype: 'form',
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        frame: true,
        buttons: [{
	            text: 'Conferma',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',
	            handler: function() {
	                var loc_win = this.up('window');
	            	var form = this.up('form').getForm();
	            	var form_values = form.getValues();
					    Ext.Ajax.request({
				            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_sped',
				            jsonData: {
				            	sped_id: <?php echo j($m_params->sped_id)?>,
				            	form_values : form_values
				             },
				            method: 'POST',
				            success: function ( result, request) {
				               var jsonData = Ext.decode(result.responseText);
				               loc_win.fireEvent('afterSave', loc_win);											                
				                
				            },
				            failure: function ( result, request) {
				            }
				        });	
					 
			      }
			     }],   		
			  items: [
			    	   {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data carico'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , value: '<?php echo print_date($data_carico, "%d/%m/%Y"); ?>'	
						},{
                           xtype:'timefield'
                    	   , fieldLabel: 'Ora carico'
                    	   , value: ''		
                    	   , name: 'f_ora'
                    	   , format: 'H:i'						   
                    	   , submitFormat: 'His'
                    	   , increment: 30
                    	   , value: '<?php echo print_ora($sped['CSHMIC']); ?>'		
                       },
                       {
                           xtype:'textfield'
                    	   , fieldLabel: 'Descrizione'
                    	   , value: ''		
                    	   , name: 'f_descrizione'
                    	   , value : <?php echo j(trim($sped['CSDESC'])); ?>
                    	   , anchor : '-15'
                    	   , maxLenght : 100 
                       },
	             	  ]
					
			    	
        
        }
	    
	    }
<?php 
exit;
}
