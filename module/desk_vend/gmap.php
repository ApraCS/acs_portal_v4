<?php
require_once "../../config.inc.php";
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="core.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

 <?php require '../../templates/import_gmap_api.php'; ?>

</head>
<body>

<script type="text/javascript">

 var map;
 var address, latitude, longitude = '';

$(document).ready(function(){

<?php
 if (isset($_REQUEST['ind'])) echo "address = '{$_REQUEST['ind']}' ;";
 if (isset($_REQUEST['lat'])) echo "latitude = '{$_REQUEST['lat']}' ;";
 if (isset($_REQUEST['lng'])) echo "longitude = '{$_REQUEST['lng']}' ;";

?>
	
var geocoder = new google.maps.Geocoder();

if (latitude != '' && longitude != ''){

	map = new GMaps({
	    el: '#basic_map',
	    lat: latitude,
	    lng: longitude,
	    zoom: 10,
	    zoomControl : true,
	    zoomControlOpt: {
	        style : 'SMALL',
	        position: 'TOP_LEFT'
	    },
	    panControl : false
	  });

	map.addMarker({
	  lat: latitude,
	  lng: longitude,
	  title: 'Cliente',
	  click: function(e) {
	    //alert('You clicked in this marker');
	  }
	});	
  return;	
}

//non ho passato lat e lng.... geolocalizzo
geocoder.geocode( { 'address': address}, function(results, status) {

if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
    var longitude = results[0].geometry.location.lng();

 map = new GMaps({
    el: '#basic_map',
    lat: latitude,
    lng: longitude,
    zoom: 10,
    zoomControl : true,
    zoomControlOpt: {
        style : 'SMALL',
        position: 'TOP_LEFT'
    },
    panControl : false
  });

map.addMarker({
  lat: latitude,
  lng: longitude,
  title: 'Cliente',
  click: function(e) {
    //alert('You clicked in this marker');
  }
});

    } 
}); 

});
</script>

<div id="basic_map" class="map"></div>

</body>
</html>
