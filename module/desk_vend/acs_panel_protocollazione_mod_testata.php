<?php 
require_once "../../config.inc.php";


$main_module = new Spedizioni();
$s = new Spedizioni();
$deskGest = new DeskGest(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// EXE modifica info testata
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_info_testata'){
    
	ini_set('max_execution_time', 3000);
    $form_values = $m_params;
	
	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= $id_ditta_default; //ditta
	
	///$cl_p .= str_repeat("0", 97); //non servono tutti gli altri parametri
	$cl_p .= sprintf("%-2s", $form_values->tido);
	$cl_p .= sprintf("%-2s", $form_values->tpdo);
	$cl_p .= sprintf("%-2s", $form_values->stdo);
	$cl_p .= sprintf("%0-4s", substr($form_values->dtrg, 0, 4));
	$cl_p .= sprintf("%09s", $form_values->f_cliente_cod);
	$cl_p .= sprintf("%0-8s", $form_values->dtrg);
	$cl_p .= sprintf("%0-8s", $form_values->dtep);
	$cl_p .= sprintf("%-2s", $form_values->prio);
	$cl_p .= sprintf("%-11s", $form_values->mode); //passato da 3 a 11 per GL
	
	$cl_p .= sprintf("%-1s", $form_values->endp);
	$cl_p .= sprintf("%-1s", $form_values->elab);
	$cl_p .= sprintf("%-2s", $form_values->dexp);
	
	$cl_p .= sprintf("%-30s", $form_values->vsrf);
	$cl_p .= sprintf("%-3s", trim($form_values->f_destinazione_cod));
	$cl_p .= sprintf("%0-6s", $form_values->nrdo);
	$cl_p .= sprintf("%-3s", $form_values->tip_prod);
	$cl_p .= sprintf("%-3s", $form_values->divisione);

	
	
	
	$cl_p .= sprintf("%-50s", trim($m_params->TDDOCU));
	$cl_p .= sprintf("%-1s", 'E'); //modifica completa testata (M -> modifica parziale, seconda videata dopo protocollazione)
	$cl_p .= sprintf("%-3s", trim($m_params->f_pagamento));
	$cl_p .= sprintf("%-30s", ''); //progetto grafico
	$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_invio_conferma));
	$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_attesa_conferma));
	$cl_p .= sprintf("%-1s", trim($m_params->f_preferenza));
	$cl_p .= sprintf("%-10s", trim($auth->get_user()));
	
	$cl_p .= sprintf("%012d", $m_params->f_acconto) . '000'; //acconto 12+3
	$cl_p .= sprintf("%012d", $m_params->f_caparra) . '000'; //caparra 12+3
	$cl_p .= sprintf("%012d", $m_params->f_imp_trasp) . '000'; //caparra 12+3
	$cl_p .= sprintf("%-3s", trim($m_params->f_referente));
	
	$cl_p .= sprintf("%-1s", ''); //O=ordine, P=preventivo
	$cl_p .= sprintf("%0-8s", $form_values->f_data_validita);
	
	$cl_p .= sprintf("%-3s", '');		   //stabilimento (qui non gestito)
	
	$cl_p .= sprintf("%0-8s", $form_values->dtep2);			//data evasione programmata
	$cl_p .= sprintf("%-3s",  $form_values->f_tipo_pag_cap);	//tipo pagamento caparra
	$cl_p .= sprintf("%-9s",  $form_values->f_trasportatore);	//Trasportatore (AVET)
	$cl_p .= sprintf("%-3s",  $form_values->f_architetto);		//Architetto (ARCHI)
	
	
	//salvo in TA0 i parametri personalizzati

	$sql = "DELETE FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT='$id_ditta_default' AND TATAID = 'PVNOR' AND TAINDI=?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->TDDOCU));
	
	foreach ($m_params as $p => $pv) {
		if (substr($p, 0, 7) == "f_pers_") {
				
			$id_dom = substr($p, 7);
			$f_nota = "f_nota_$id_dom";
			$ar_ins = array();
			$ar_ins['TADT'] 	= $id_ditta_default;
			$ar_ins['TATAID'] 	= 'PVNOR';
			$ar_ins['TAKEY1'] 	= $s->next_num('PVNOR');
			$ar_ins['TAINDI'] 	= $m_params->TDDOCU;
			$ar_ins['TAMAIL'] 	= $m_params->$f_nota;
				
			$ar_ins['TAKEY2'] 	= substr($p, 7); //DOMANDA
			$ar_ins['TAKEY3'] 	= $pv; //RISPOSTA

			$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
			echo db2_stmt_errormsg($stmt);
		}
	}

	$cl_in 	= array();
	$cl_out = array();

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.UR21H5C('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('UR21H5C', $libreria_predefinita_EXE, $cl_in, null, null);
		$tkObj->disconnect();
	}

	
	$ord = $s->get_ordine_by_k_docu($m_params->TDDOCU);
	
	if(trim($ord['TDVETT']) != trim($form_values->f_vettore)){
	    
	    $sql = "UPDATE {$cfg_mod_DeskPVen['file_testate']} TD
	    SET TDVETT= ?
	    WHERE TDDOCU = '{$m_params->TDDOCU}'";
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, array(trim($form_values->f_vettore)));
	    
	    $sh = new SpedHistory();
	    $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> 'ASS_VETTORE',
	            "k_ordine"	=> $m_params->TDDOCU,
	            "vals" => array("RIVETT" => trim($form_values->f_vettore))
	        )
	        );
	    
	}
	
	
	//update campi su testata ordine gest
	$ar_upd = array();
	$oe = $s->k_ordine_td_decode_xx($m_params->TDDOCU);
	
	
	set_ar_value_if_is_set($ar_upd, 'TDBANC', acs_toDb($m_params->form_values->TDBANC));
	set_ar_value_if_is_set($ar_upd, 'TDABI', $m_params->form_values->TDABI);
	set_ar_value_if_is_set($ar_upd, 'TDCAB', $m_params->form_values->TDCAB);
	
	set_ar_value_if_is_set($ar_upd, 'TDPAGA', $m_params->form_values->TDPAGA);
	set_ar_value_if_is_set($ar_upd, 'TDTSIV', $m_params->form_values->TDTSIV);
	set_ar_value_if_is_set($ar_upd, 'TDGGIV', $m_params->form_values->TDGGIV);
	set_ar_value_if_is_set($ar_upd, 'TD1RIV', $m_params->form_values->TD1RIV);
	set_ar_value_if_is_set($ar_upd, 'TDABI', $m_params->form_values->TDABI);
	set_ar_value_if_is_set($ar_upd, 'TDCAB', $m_params->form_values->TDCAB);
	set_ar_value_if_is_set($ar_upd, 'TDAG1', $m_params->form_values->TDAG1);
	set_ar_value_if_is_set($ar_upd, 'TDVSRF', $m_params->form_values->TDVSRF);
	
	
	if (isset($m_params->form_values->TDPA1))
	    set_ar_value_if_is_set($ar_upd, 'TDPA1', sql_f($m_params->form_values->TDPA1));
	    set_ar_value_if_is_set($ar_upd, 'TDAG2', $m_params->form_values->TDAG2);
    if (isset($m_params->form_values->TDPA2))
        set_ar_value_if_is_set($ar_upd, 'TDPA2', sql_f($m_params->form_values->TDPA2));
        /* set_ar_value_if_is_set($ar_upd, 'TDAG3', $m_params->form_values->TDAG3);
         if (isset($m_params->form_values->TDPA3))
         set_ar_value_if_is_set($ar_upd, 'TDPA3', sql_f($m_params->form_values->TDPA3));
         */
         set_ar_value_if_is_set($ar_upd, 'TDREFE', $m_params->form_values->TDREFE);
         set_ar_value_if_is_set($ar_upd, 'TDLIST', $m_params->form_values->TDLIST);
         set_ar_value_if_is_set($ar_upd, 'TDIVDO', $m_params->form_values->TDIVDO);
         set_ar_value_if_is_set($ar_upd, 'TDLING', $m_params->form_values->TDLING);
         set_ar_value_if_is_set($ar_upd, 'TDSC1', $m_params->form_values->TDSC1);
         set_ar_value_if_is_set($ar_upd, 'TDSC2', $m_params->form_values->TDSC2);
         set_ar_value_if_is_set($ar_upd, 'TDSC3', $m_params->form_values->TDSC3);
         set_ar_value_if_is_set($ar_upd, 'TDSC4', $m_params->form_values->TDSC4);
         set_ar_value_if_is_set($ar_upd, 'TDSC5', $m_params->form_values->TDSC5);
         set_ar_value_if_is_set($ar_upd, 'TDSC6', $m_params->form_values->TDSC6);
         set_ar_value_if_is_set($ar_upd, 'TDSC7', $m_params->form_values->TDSC7);
         set_ar_value_if_is_set($ar_upd, 'TDSC8', $m_params->form_values->TDSC8);
         set_ar_value_if_is_set($ar_upd, 'TDVALU', $m_params->form_values->TDVALU);
         set_ar_value_if_is_set($ar_upd, 'TDCINT', $m_params->form_values->TDCINT);
         set_ar_value_if_is_set($ar_upd, 'TDCAUT', $m_params->form_values->TDCAUT);
	         
     //eseguo update
     $sql = "UPDATE {$cfg_mod_Spedizioni['file_testate_doc_gest']}
             SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
             WHERE TDDT = ? AND TDTIDO = ? AND TDINUM = ? AND TDAADO = ? AND TDNRDO = ?";
     
     $stmt = db2_prepare($conn, $sql);
     echo db2_stmt_errormsg();
     $result = db2_execute($stmt, array_merge($ar_upd, $oe));
     echo db2_stmt_errormsg($stmt);
	         
     //messaggio su RI
     $sh = new SpedHistory();
     $ret_RI = $sh->crea(
         'pers',
         array(
             "messaggio"	=> 'UPD_GEST_TD',
             "k_ordine"	=> $m_params->TDDOCU,
             "vals" => array(
             )));
	

	$ret = array();
	if ($call_return){
		$ret['success'] = true;
		$ret['call_return'] 	= $call_return['io_param']['LK-AREA'];
		$ret['valori'] 	= $ord;
	} else {
		$ret['success'] = false;
	}

	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// open form
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
	$m_params = acs_m_params_json_decode();
	$ord = $s->get_ordine_by_k_docu($m_params->tddocu);
	$from_protocollazione = 'Y';
	
	?>
	
{"success":true, "items": [
        
       {
            xtype: 'form',
            autoScroll : true,
            //bodyStyle: 'padding: 10px',
           // bodyPadding: '5 0 0',
            frame: true,
            items: [
       
       
        {
    	xtype: 'tabpanel',
    	//bodyStyle: 'padding: 10px',
    	//bodyPadding: '5 5 0',
    	frame: true,
    	title: '',
    	defaults:{ anchor: '-10' , labelWidth: 130 },    
    	
    	items: [
        
        {
            xtype: 'form',
            autoScroll: true,
           // bodyStyle: 'padding: 10px',
           // bodyPadding: '5 5 0',
            frame: true,
            title: 'Intestazione',
            layout: {
			    type: 'vbox',
			    align: 'stretch',
			    pack : 'start',
			},
			
			items: [
            		{
                		xtype: 'hidden',
                		name: 'TDDOCU',
                		value: '<?php echo $ord['TDDOCU'] ?>'
                	},
                	
                	 	{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
							 {  xtype: 'combo',
								name: 'f_cliente_cod',
								flex: 1,
								margin: "0 10 0 0",
								fieldLabel: 'Cliente',
								minChars: 2,
								allowBlank: false,
					            store: {
					            	pageSize: 1000,
									proxy: {
							            type: 'ajax',
							            url : 'acs_get_select_json.php?select=search_cli_anag',
							            reader: {
							                type: 'json',
							                root: 'root',
							                totalProperty: 'totalCount'
							            }
							        },
									fields: ['cod', 'descr', 'out_ind', 'out_loc'],
					            },
					
								valueField: 'cod',
					            displayField: 'descr',
					            typeAhead: false,
					            hideTrigger: true,
					            anchor: '100%',
					
					
						        listeners: {
						        	afterrender: function(comp){
						        		data = [
						        			{cod: <?php echo j($ord['TDCCON'])?>, descr: <?php echo j(trim($ord['TDDCON']). " [".trim($ord['TDCCON']). "]") ?>}
						        		];
										comp.store.loadData(data);
										comp.setValue(<?php echo j($ord['TDCCON'])?>);
						        	},						        
						        
						            change: function(field,newVal) {
										//console.log('cliente change');
						            	var form = this.up('form').getForm();
						            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal						            	
						            	form.findField('f_destinazione_cod').store.load();
						            	form.findField('f_destinazione_cod').setValue('');
						            },
									select: function(combo, row, index) {
										//console.log('cliente select');
						            	var form = this.up('form').getForm();
						     			form.findField('out_ind').setValue(row[0].data.out_ind);
						     			form.findField('out_loc').setValue(row[0].data.out_loc);

						     			form.findField('f_destinazione_cod').setValue('');
					
					 					//imposto quella a standard
					 					/*
					 					//console.log('select');
										form.findField('f_destinazione_cod').store.load({
						 					callback: function(records, operation, success) {
					 							//console.log(this);
					 							if (records.length == 1){
					 								this.setValue(records[0].get('cod'));
					 							}
					    					}, scope: form.findField('f_destinazione_cod')
					 					});
					 					//console.log('ok_load');
					 					*/
					 					//console.log(form.findField('f_destinazione_cod').getStore().data);
					
					//					all_dest = Ext.pluck(form.findField('f_destinazione_cod').getStore().data.items, 'data')
					// 					//console.log(all_dest);
					 	
									}
					 	
					 	
						        },
					
					
					            listConfig: {
					                loadingText: 'Searching...',
					                emptyText: 'Nessun cliente trovato',
					
					                // Custom rendering template for each item
					                getInnerTpl: function() {
					                    return '<div class=\"search-item\">' +
					                        '<h3><span>{descr}</span>[{cod}]</h3>' +
					 						' {out_ind}<br/>' +
					                        ' {out_loc}' +
					                    '</div>';
					                }
					
					            },
					
					            pageSize: 1000
					        }, 
	                 {
            xtype: 'combo',
            flex: 1,
			name: 'f_destinazione_cod',
			fieldLabel: 'Destinazione',
			minChars: 2,
			allowBlank: true,
            store: {
            	pageSize: 1000,
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_des_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            },
                 extraParams: {
        		  	cliente_cod: '',
        		  	trim_cod: 'Y'
        		 }
		        },
				fields: ['cod', 'descr', 'denom', 'IND_D', 'LOC_D'],
            },

			valueField: 'cod',
            displayField: 'denom',
            typeAhead: false,
            hideTrigger: false, //mostro la freccetta di selezione
            anchor: '100%',

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessuna destinazione trovata',

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class=\"search-item\">' +
                        '<h3><span>{denom}</span></h3>' +
                        '{IND_D}<BR/>' +
                        '{LOC_D}' +
                    '</div>';
                }

            },
 	
	        listeners: {
	        
	        	afterrender: function(comp){
	        		comp.store.proxy.extraParams.cliente_cod = <?php echo j(trim($ord['TDCCON']))?>;
	        		comp.store.load();  		
					comp.setValue(<?php echo j(trim($ord['TDCDES'])) ?>);
	        	},						        
	        
 	
				change: function(combo, row, index) {
					//console.log('dest change');
	            	var form = this.up('form').getForm();
					if (Ext.isEmpty(this.valueModels)){
						form.findField('out_ind_dest').setValue('');
	     				form.findField('out_loc_dest').setValue('');
					} else {
	     				form.findField('out_ind_dest').setValue(this.valueModels[0].get('IND_D'));
	     				form.findField('out_loc_dest').setValue(this.valueModels[0].get('LOC_D'));
					}
				},
				select: function(combo, row, index) {
					//console.log('dest select');					
	            	var form = this.up('form').getForm();
					if (Ext.isEmpty(this.valueModels)){
	     				form.findField('out_ind_dest').setValue('');
	     				form.findField('out_loc_dest').setValue('');
					} else {
	     				form.findField('out_ind_dest').setValue(row[0].data.IND_D);
	     				form.findField('out_loc_dest').setValue(row[0].data.LOC_D);					
					}
				}
 	
 	
	        },

            pageSize: 1000
        }
						
						]
					 },	{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
											{
					 			xtype: 'textfield', disabled: true,
								fieldLabel: 'Indirizzo',
					 			name: 'out_ind',
					 			margin: "0 10 0 0",
					 			flex: 1,
				 			
								value: <?php echo j(trim($ord['TDINDI'])) ?>
					 		},	{
					 			xtype: 'textfield', disabled: true,
								fieldLabel: 'Indirizzo',
					 			name: 'out_ind_dest',
					 			flex: 1,
				 				value: <?php echo j(trim($ord['TDIDES'])) ?>
					 		}
						
						]
					 },		
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
										 {
					 			xtype: 'textfield', disabled: true,
								fieldLabel: 'Localit&agrave;',
					 			name: 'out_loc',
					 			flex: 1,
					 			margin: "0 10 0 0",
								value: <?php echo j(trim($ord['TDDLOC'])) ?>
					 		},{
					 			xtype: 'textfield', disabled: true,
								fieldLabel: 'Localit&agrave;',
					 			name: 'out_loc_dest',
					 			flex: 1,
								value: <?php echo j(trim($ord['TDLOCA'])) ?>
					 		}
						
						]
					 }
					 
					 
					 ,
									{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
						{
				name: 'tpdo',
				xtype: 'combo',
				 flex: 1,
				 margin: "0 10 0 0",
            	anchor: '100%',
				fieldLabel: 'Tipo ordine',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,

				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					    <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, 'VO'), '') ?>
					    ]
				},
				value: <?php echo j(trim($ord['TDOTPD'])) ?>

				
			}, 
	                {
				     name: 'vsrf'
				    ,  flex: 1
				   , xtype: 'textfield'
				   , fieldLabel: 'Riferimento'
				   , maxLength: 30
				   , value: <?php echo j(trim($ord['TDVSRF'])) ?>
				}
						
						]
					 },
					 
					 
					 				{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
				{
				name: 'mode',
				xtype: 'combo',
            	anchor: '100%',
            	 flex: 1,
				 margin: "0 10 0 0",
				fieldLabel: 'Modello',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					    <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, 'MOD'), '') ?>
					    ]
				},
				 value: <?php echo j(trim($ord['TDCVN1'])) ?>
			}, 
	                {
						name: 'stdo',
						xtype: 'combo',
		            	anchor: '-15',
		            	flex: 1,
			            fieldLabel: 'Stato ordine',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						value: <?php echo j(trim($ord['TDSTAT'])) ?>,
						forceSelection: true,
					   	allowBlank: false,
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [
							     <?php 
							     $ar_stati = find_TA_sys('BSTA', null, null, 'VO', null, explode_if_isset("|", $cfg_mod_DeskPVen["protocollazione"][$m_params->doc_type]["stdo"]["LIST"]));
							     
							     //lascio solo quello attuale e quelli che possono essere impostati in base allo stato attuale
							     $ar_stati_to_select = $s->find_TA_std('CHSTT', $ord['TDSTAT'], 'N', 'Y');
							     
							     $ar_stato_to = array($ord['TDSTAT']); //aggiungo sempre lo stato attuale
							     foreach($ar_stati_to_select as $ks => $sv)
							     	$ar_stato_to[] = trim($sv['TAKEY2']);
							     	
							     //elimino quelli non ammessi
							     foreach($ar_stati as $ids => $ss){
							     	if (!in_array(trim($ss['id']), $ar_stato_to)){
							     		unset($ar_stati[$ids]);	
							     	}
							     }

							     echo acs_ar_to_select_json($ar_stati, '') ?>
							     //echo acs_ar_to_select_json(find_TA_sys('BSTA', null, null, 'VO', null, explode_if_isset("|", $cfg_mod_DeskPVen["protocollazione"][$m_params->doc_type]["stdo"]["LIST"])), '') ?>
							    ]
						}
					}
					
					
					
						]
						
					 },
					 				{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
			
							{
							name: 'mode',
							xtype: 'combo',
			            	anchor: '100%',
			            	 flex: 1,
							 margin: "0 10 0 0",
							fieldLabel: 'Tipologia produzione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: false,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,
							    fields: [{name:'id'}, {name:'text'}],
							    data: [
								    <?php echo acs_ar_to_select_json(find_TA_sys('BLTO', null, null, null, null, explode_if_isset("|", $cfg_mod_Spedizioni["protocollazione"]["tip_prod"]["LIST"])), '') ?>
								    ]
							},
							 value: <?php echo j(trim($ord['TDCVN1'])) ?>
						}, {
								name: 'prio',
								xtype: 'combo',
				            	anchor: '100%',
				            	flex: 1,
								fieldLabel: 'Priorit&agrave',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,
								value: <?php echo j(trim($ord['TDOPRI'])) ?>,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}, {name:'tarest'}],
								    data: [
									     <?php echo acs_ar_to_select_json(find_TA_sys('BPRI', null, null, 'VO', null, null, 1), '') ?>
									    ]
								}
							}
						]
						
					 },
					 
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
										
								{
								     name: 'dtep'
								   , flex: 1
								   , anchor: '100%'
								   , margin: "0 10 0 0"
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'Consegna richiesta'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: false
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
									 }
								   , value: '<?php echo print_date($ord['TDODER'], "%d/%m/%Y"); ?>'
									},  {
						     name: 'dtrg'
						   , flex: 1
						   , anchor: '100%'
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Ricezione'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , value: '" . print_date(oggi_AS_date(), "%d/%m/%Y") . "'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
							, value: '<?php echo print_date($ord['TDODRE'], "%d/%m/%Y"); ?>'					 
						}
					             
				 
							
						]
						
					 },
					 
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
			         {
					     name: 'dtep2'
					   , flex: 1
					   , xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Consegna prog.'
					   <?php if($ord['TDNRCA'] > 0 || (!$auth->is_admin() && count($stato_from_ar) > 0 && $stato_from_ar[0]['TAFG04'] == 'Y')){?>
					   , readOnly : true
					   <?php }?>
					   , margin: "0 10 0 0"
					   , format: 'd/m/Y'
					   , submitFormat: 'Ymd'
					   , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						 }
					   , value: '<?php echo print_date($ord['TDDTEP'], "%d/%m/%Y"); ?>'
				}, {
						name: 'f_referente',
						xtype: 'combo',
						fieldLabel: 'Referente', 
						labelAlign: 'left',
						displayField: 'text',
						valueField: 'id',
						flex: 1,
						forceSelection:true,
					   	allowBlank: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [<?php echo acs_ar_to_select_json(find_TA_sys('BREF'), '') ?>] 
						},
						value: <?php echo j(trim($ord['TDCORE'])) ?>
				}
			]
						
					 },
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
							{
								name: 'f_trasportatore',
								xtype: 'combo',
				            	anchor: '-15',
				            	flex: 1,
				            	margin: "0 10 0 0",
					            fieldLabel: 'Trasportatore',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								value: <?php echo j(trim($ord['TDVET1'])) ?>,
								forceSelection: true,
							   	allowBlank: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [
									     <?php echo acs_ar_to_select_json($s->find_TA_std('AVET'), '') ?>
									    ]
								}
							}, {
								name: 'f_vettore',
								xtype: 'combo',
				            	anchor: '-15',
				            	flex: 1,
					            fieldLabel: 'Vettore',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								value: <?php echo j(trim($ord['TDVETT'])) ?>,
								forceSelection: true,
							   	allowBlank: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [
									     <?php echo acs_ar_to_select_json($s->find_TA_std('AUTR'), '') ?>
									    ]
								}
							}
						
						]
					 },	
					
					
			]
		},  //form intestazione
		
		//form "Dati contabili"
			<?php include '../base/acs_panel_testata_ordine_gest_SV2_comm.php'; ?>
		
    	]
    } //form
    ],
     dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                '->',
                {
		            text: 'Salva',
		            iconCls: 'icon-windows-32',
		            scale: 'large',
		            handler: function() {
		            
		            console.log('salva');
		            	var form = this.up('form').getForm();
		            	var loc_win = this.up('window');
		            	
 						if(form.isValid()){ 	
 						
 					Ext.getBody().mask('Salvataggio... ', 'salvataggio in corso').show();
 					//loc_win.close();
	 					//Call protocollazione
						Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_info_testata',
							        timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										 Ext.getBody().unmask();
										jsonData = Ext.decode(result.responseText);
										
										  if (jsonData.success == false){
        						            	acs_show_msg_error(jsonData.error_msg);
        						            	return;										  
										  }
										
										
										   if (!Ext.isEmpty(loc_win.events.afteroksave)) {
											  	loc_win.fireEvent('afterOkSave', loc_win, jsonData.valori); 
											  }	
											  else {
				            			loc_win.close();
				            				   }
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							            loc_win.fireEvent('afterUpdateRecord', loc_win);
							        }
							  });	
							  

							     /*    if (!Ext.isEmpty(loc_win.events.afteroksave)) {
											  	loc_win.fireEvent('afterOkSave', loc_win);
											  }	
											  else {
				            			loc_win.close();
				            				   }*/

							  
							 /* 	loc_m_win.fireEvent('acsaftersave');
											loc_m_win.close();
									   		return false;*/

							  
							  
							  		
 						}

		            }
		        }
                
                ]
                
                
                
                }]
    
    }
	]
}
<?php
 exit;
}