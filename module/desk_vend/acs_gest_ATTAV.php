<?php
require_once("../../config.inc.php");

$main_module =  new Spedizioni();
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    'module'      => $main_module,
    'tab_name'    => $cfg_mod['file_tabelle'],
    'descrizione' => "To Do Entry",
    't_panel' =>  "ATTAV - ToDo Config",
    'form_title' => "Dettagli tabella attivit�",
    'maximize'    => 'Y',
    'conferma_elimina'    => 'Y',
    'fields_preset' => array(
        'TATAID' => 'ATTAV',
        //'TARIF1' => $m_params->open_request->sezione
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
   
    'fields_key' => array('TAKEY1', 'TADESC'),
    
    'fields_grid' => array('TAKEY1', 'TADESC', 'TARIF1', 'TAB_ABB', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TARIF1', 'TAFG01', 'TAFG03', 'TAFG04', 'TAFG05', 'TAPESO', 'TANAZI', 'TACOGE', 'TAASPE'),

    
    
    'fields' => array(
        
        'TAPESO' => array('label'	=> 'Codice', 'type' => 'numeric'),
        'TAKEY1' => array('label'	=> 'Seq.', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione'),
        'TARIF1' => array('label'	=> 'Riferimento', 'fw'=>'width: 180'),
        'TAFG01' => array('label'	=> 'TAFG01', 'fw'=>'width: 170', 'tooltip' => 'TAFG01'),
        'TAFG03' => array('label'	=> 'TAFG03', 'fw'=>'width: 170', 'tooltip' => 'Riferimento precodificato'),
        'TAFG04' => array('label'	=> 'TAFG04', 'fw'=>'width: 170', 'tooltip' => 'Assegna utente', 'type' => 'from_TA', 'TAID' => 'ARY'),
        'TAFG05' => array('label'	=> 'TAFG05', 'fw'=>'width: 170', 'tooltip' => 'Chiusura todo', 'type' => 'from_TA', 'TAID' => 'ARY'),
        'TANAZI' => array('label'	=> 'Codice lingua', 'fw'=>'width: 170', 'tooltip' => 'TANAZI'),
        'TACOGE' => array('label'	=> 'Codice predefinito', 'fw'=>'width: 170', 'tooltip' => 'Codice predefinito'),
        'TAASPE' => array('label'	=> 'Stato', 'fw'=>'width: 170', 'type' => 'from_TA_sys', 'TAID' => 'BSTA'),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        //TABELLA ABBINATA
        'TAB_ABB' =>  array('label' => 'rilav', 'type' => 'TA',
            'iconCls' => 'search',
            'tooltip' => 'Causali rilascio',
            'select' => "TA_RL.C_ROW AS TAB_ABB",
            'join' => "LEFT OUTER JOIN (
            SELECT COUNT(*) AS C_ROW, TA2.TAKEY1
            FROM {$cfg_mod['file_tabelle']} TA2
            WHERE TA2.TADT = '{$id_ditta_default}' AND TA2.TATAID = 'RILAV'
            GROUP BY TA2.TAKEY1) TA_RL
            ON TA.TAKEY1 = TA_RL.TAKEY1",
            'ta_config' => array(
                'tab' => 'RILAV',
                'file_acs'     => '../desk_vend/acs_gest_RILAV.php?fn=open_tab',
                
            )
            ),
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione'),
        
    )
);


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
