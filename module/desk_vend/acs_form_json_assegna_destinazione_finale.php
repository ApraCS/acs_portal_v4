<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();


if(isset($m_params->cliente) && $m_params->cliente != ''){
    $ar_cli[] = array('k_cli_des' =>"_{$m_params->cliente}");
    $list_selected_id_encoded = strtr(acs_je($ar_cli), array("'" => "\'", '"' => '\"'));
    $ditta_cli= $id_ditta_default;
    $cod_cli = $m_params->cliente;
}else{
    
    $list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));
    //recupero codice cliente
    $k_cli_des = $m_params->list_selected_id[0]->k_cli_des;
    $k_cod_cli_exp = explode("_", $k_cli_des);
    $ditta_cli= $k_cod_cli_exp[0];
    $cod_cli = $k_cod_cli_exp[1];
}


 


if ($_REQUEST['fn'] == 'exe_genera_dest_agente'){
	
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();


	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $m_params->ditta_cli);
	$cl_p .= sprintf("%09s", $m_params->cod_cli);
	$cl_p .= sprintf("%10s", $auth->get_user());
	
	/*print_r($cl_p);
	exit;*/

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.UR21JS('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('UR21JS', $libreria_predefinita_EXE, $cl_in, null, null);
		$tkObj->disconnect();
	}
	
	
	$ret = array();
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



?>



{"success":true, "items": [

        {
            xtype: 'grid',
			
			store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '../desk_vend/acs_get_select_json.php?select=get_el_destinazioni_finali',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
			        		    	list_selected_id: '<?php echo $list_selected_id_encoded; ?>'
			        				},
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['d_ass', 'des_s', 't_indi', 'cod', 'cod_cli', 'dt', 'descr', 'denom', 'IND_D', 'LOC_D', 'CAP_D', 'PRO_D', 'TEL_D']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'TI',
	                tooltip : 'Tipo indirizzo',
	                dataIndex: 't_indi',
	                width: 40,
	                renderer: function(value, p, record){
	                      if(record.get('des_s') == 'S') p.tdCls += ' sfondo_verde';
		    			  if(record.get('t_indi') == 'P') return '<img src=<?php echo img_path("icone/48x48/clienti.png") ?> width=15>';
		    			  if(record.get('t_indi') == 'D' || record.get('t_indi').trim() == '') return '<img src=<?php echo img_path("icone/48x48/delivery.png") ?> width=15>';
		    			  if(record.get('t_indi') == 'A') return '<img src=<?php echo img_path("icone/48x48/briefcase.png") ?> width=15>';
						  			    		 
		    		  } 
	            }, {
	                header   : 'Codice',
	                dataIndex: 'cod',
	                width: 50 
	            }, {
	                header   : 'Denominazione',
	                dataIndex: 'denom',
	                flex: 10
	            }, {
	                header   : 'Indirizzo',
	                dataIndex: 'IND_D',
	                flex: 10
	            }, {
	                header   : 'CAP',
	                dataIndex: 'CAP_D',
	                width: 80
	            }, {
	                header   : 'Localit&agrave;',
	                dataIndex: 'LOC_D',
	                flex: 5
	            }, {
	                header   : 'Prov',
	                dataIndex: 'PRO_D',
	                width: 50
	            }, {
		            xtype:'actioncolumn', 
		            width:50,
		           renderer: function (value, metaData, record, row, col, store, gridView) {
                        if(record.get('t_indi') == 'P' || record.get('t_indi') == 'A') {
                           this.up('grid').columns[col].items[0].icon = '';
                           if(record.get('t_indi') == 'P' && record.get('d_ass') != '')
                              return '<span style = "font-size: 11px; line-height: 1em;">[' +
                                    record.get('d_ass') + ']</span>';
                           else return '';         
                        }else{
                         this.up('grid').columns[col].items[0].icon = <?php echo img_path("icone/16x16/keyboard.png") ?>;
                        }
                        
                    },
		            items: [{
		                icon: <?php echo img_path("icone/16x16/keyboard.png") ?>,  // Use a URL in the icon config
		                tooltip: 'Modifica dati destinazione',
		                handler: function(grid, rowIndex, colIndex) {
		                		var rec = grid.getStore().getAt(rowIndex);
		                		var mgrid = grid;
                    			
								// create and show window
								var win = new Ext.Window({
								  width: 800
								, height: 280
								, minWidth: 300
								, minHeight: 300
								, plain: true
								, title: 'Modifica definitiva dati destinazione di anagrafica cliente'
								, iconCls: 'iconDestinazioneFinale'			
								, layout: 'fit'
								, border: true
								, closable: true
								, items: [
									{
										xtype: 'form',
									            bodyStyle: 'padding: 10px',
									            bodyPadding: '5 5 0',
									            frame: false,
									            title: '',
									            url: 'acs_print_wrapper.php',
												buttons: [{
										            text: 'Conferma',
										            iconCls: 'icon-sub_blue_accept-32',
										            scale: 'large',
										            handler: function() {
										                if (this.up('form').getForm().isValid()){										                
											                this.up('form').submit({
										                        url: 'acs_op_exe.php',
										                        method: 'POST',
										                        
														success: function (formPanel, action) {
														  win.close(),
										                  mgrid.store.load();
											                },										                        
										                        
										                  });

										                }
										            }
										        }],             								
										items: [
										 	{
							                    xtype: 'hiddenfield',
							                    name: 'fn',
							                    value: 'exe_modifica_dati_destinazione'    
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'dt',
							                    value: rec.get('dt')    
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'cod_cli',
							                    value: rec.get('cod_cli').trim()
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'cod_des',
							                    value: rec.get('cod')    
							                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '-5',
								                    name: 'denom1',
								                    value: Ext.util.Format.substr(rec.get('denom'), 0, 30).trim(),    
								                    fieldLabel: 'Denominazione',
								                    allowBlank: false,
								                    flex: 1,
								                    maxLength: 30
							                	}, {
								                    xtype: 'textfield',
								                    anchor: '-15',
								                    name: 'denom2',
								                    value: Ext.util.Format.substr(rec.get('denom'), 30, 30).trim(),    
								                    fieldLabel: '',
								                    allowBlank: true,
								                    flex: 1,
								                    maxLength: 30
							                	}]
											}, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '0',							                    
								                    name: 'ind1',    
								                    value: Ext.util.Format.substr(rec.get('IND_D'), 0, 30).trim(),
								                    fieldLabel: 'Indirizzo',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 30
								                }, {
								                    xtype: 'textfield',
								                    anchor: '-15',							                    
								                    name: 'ind2',    
								                    value: Ext.util.Format.substr(rec.get('IND_D'), 30, 30).trim(),
								                    fieldLabel: '',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 30
								                }]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'cap',    
							                    value: rec.get('CAP_D').trim(),
							                    fieldLabel: 'Cap',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 10
							                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',							                    
								                    name: 'loc1',    
								                    value: Ext.util.Format.substr(rec.get('LOC_D'), 0, 30).trim(),
								                    fieldLabel: 'Loc',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 30
							                	}, {
								                    xtype: 'textfield',							                    
								                    name: 'loc2',    
								                    value: Ext.util.Format.substr(rec.get('LOC_D'), 30, 30).trim(),
								                    fieldLabel: '',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 30
							                	}]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'pro',    
							                    value: rec.get('PRO_D').trim(),
							                    fieldLabel: 'Provincia',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 2
							                }, {
							                    xtype: 'textfield',							                    
							                    name: 'tel',    
							                    value: rec.get('TEL_D').trim(),
							                    fieldLabel: 'Telefono',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 20
							                }                
										]
									}	
								
								] 
								});
								win.show();  
		                		
                    			
                    			
		                }
		            }]
		        }
	            
	         ],dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [{
                    xtype: 'button',
					text: 'Genera/aggiorna destinazione agente',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		            grid = this.up('grid');
		            var rec =  grid.getSelectionModel().getSelection()[0];
		            if(rec.get('t_indi') == 'P'){
		            	acs_show_msg_error('Indirizzo punto vendita: non selezionabile come destinazione merce!');
        				return false;
		            }
		          
                    Ext.Ajax.request({
                    
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_dest_agente',
						        method     : 'POST',
			        			jsonData: {
			        				cod_cli: <?php echo $cod_cli; ?>,
			        				ditta_cli: <?php echo $ditta_cli; ?>
			        				
								},							        
						        success : function(result, request){
			            			grid.getStore().load();									        
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
       
			
			            }

			     }]
		   }],
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	var loc_win = this.up('window');
						  	       
						  	         Ext.Ajax.request({
									   url: '../desk_vend/acs_op_exe.php?fn=exe_assegna_destinazione_finale',
									   method: 'POST',
									   jsonData: {list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
									   			  tipo: '<?php echo $m_params->tipo; ?>',
									   			  to_destinazione: iView.getSelectionModel().getSelection()[0].data.cod
									   			  }, 
									   
									   
									   
									   success: function(response, opts) {					   				
					                                this.print_w.close();
								  				  	m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								  				  	if (m_grid.getStore().$className == 'Ext.data.NodeStore')
								  				  		m_grid.getStore().treeStore.load();
								  				  	else	
									   					m_grid.getStore().load();
									   }
									});		
						  	       
						
						  	
						  }
					  }	,
					  
					   itemcontextmenu : function(grid, rec, node, index, event) {
			          		event.stopEvent();
				  													  
					 		var voci_menu = [];
				     		var row = rec.data;
				     		var m_grid = this;
				     		var loc_win = this.up('window');
				     		
				     		if(rec.get('t_indi') != 'P'){
							  voci_menu.push({
			         		   text: 'Assegna come destinazione',
			        		   iconCls : 'icon-leaf-16',          		
			        		   handler: function () {
			        		        loc_win.fireEvent('afterAssegna', loc_win, rec.get('cod'));
				                }
			    				});
				     		}
				     		
    				         var menu = new Ext.menu.Menu({
            				    items: voci_menu
            					}).showAt(event.xy);	
				        
				        }  
				        
						, beforeselect: function(grid, record, index, eOpts) {
            			if (record.get('t_indi').trim() == 'P') {//replace this with your logic.
               				 return false;
            			}
       				 },
			}, viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			           console.log(record);
			           if (record.get('t_indi').trim() == 'P')
			           		return ' segnala_riga_disabled';			           		
			           return '';																
			         }   
					}
			
				  
	            
        }    

]}