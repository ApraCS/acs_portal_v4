<?php

require_once "../../config.inc.php";
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

$tp_tab = $m_params->open_parameters->tp_tab;


if($tp_tab == 'ven')
 $ta0 = $cfg_mod_Spedizioni['file_tabelle'];
if($tp_tab == 'acq')
 $ta0 = $cfg_mod_DeskAcq['file_tabelle'];
if($tp_tab == 'art')
 $ta0 = $cfg_mod_DeskArt['file_tabelle'];


if ($_REQUEST['fn'] == 'exe_canc'){

	$tp_tab = $m_params->open_parameters->tp_tab;

	$sql = "DELETE FROM {$ta0} TA
	WHERE RRN(TA) = '{$m_params->rrn}'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);

			//exit;

	exit;
}


if ($_REQUEST['fn'] == 'exe_inserisci_stato_from'){

	$tab = $m_params->form_values->f_tab;
	$key = $m_params->form_values->f_key;
	$des = $m_params->form_values->f_des;

	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= $tab;
	$ar_ins['TAKEY1'] 	= $key;
	$ar_ins['TADESC'] 	= $des;

	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();


	$sql = "INSERT INTO {$ta0}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}


if ($_REQUEST['fn'] == 'exe_modifica_master'){

	$mod_rows= $m_params->mod_rows;
	

	foreach ($mod_rows as $v){

		if(isset($v)){

			$ar_ins = array();

			$ar_ins['TADT'] 	= $v->ditta;
			$ar_ins['TATAID'] 	= utf8_decode($v->tabella);
			$ar_ins['TAKEY1'] 	= utf8_decode($v->chiave);
			$ar_ins['TADESC'] 	= utf8_decode($v->descrizione);

			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();

			$sql = "UPDATE {$ta0} TA
			SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
			WHERE RRN(TA) = '$v->rrn' ";


			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);

			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);

			//exit;

		}

	}

	exit;
}



if ($_REQUEST['fn'] == 'get_data_grid'){

	$sql = "SELECT RRN (TA) AS RRN, TADT, TATAID, TADESC, TAKEY1
	FROM {$ta0} TA
	WHERE TADT='$id_ditta_default' and tataid = ?";


	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->show_tab));

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$row['ditta'] = acs_u8e(trim($row['TADT']));
		$row['tabella'] = acs_u8e(trim($row['TATAID']));
		$row['chiave'] = acs_u8e(trim($row['TAKEY1']));
		$row['descrizione'] = acs_u8e(trim($row['TADESC']));
		$row['rrn'] = acs_u8e(trim($row['RRN']));

		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}


if ($_REQUEST['fn'] == 'get_data_tab'){

	$sql = "SELECT TATAID, COUNT(*) AS C_ROW
	FROM {$ta0} TA
	WHERE TADT='$id_ditta_default' GROUP BY TATAID";

	/*print_r($sql);
	 exit;*/

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {
		$row['tataid'] = acs_u8e(trim($row['TATAID']));			
		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}


if ($_REQUEST['fn'] == 'open_grid'){

	?>

				
{"success":true, "items": [

        {
        
        xtype: 'grid',
        title: 'Gestione tabelle',
        <?php echo make_tab_closable(); ?>,
		multiSelect: true,
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],

					store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },							        
							        
							           extraParams: {
										 show_tab : '',
										 open_parameters : <?php echo acs_je($m_params->open_parameters); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'ditta', 'tabella', 'chiave', 'descrizione', 'rrn'
		        			]
		    			},
		    		
			        columns: [
			       				        
			       		 {
			                header   : 'Ditta',
			                dataIndex: 'ditta', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },
			             { 
			                header   : 'Tabella',
			                dataIndex: 'tabella', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },
			             { 
			                header   : 'Chiave',
			                dataIndex: 'chiave', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },{ 
			                header   : 'Descrizione',
			                dataIndex: 'descrizione', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             }
			            
			         ],  
					
						listeners: {
						
						
						itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
											      
			      			  var voci_menu = [];
				
		
				    		
				    		voci_menu.push({
				         		text: 'Cancella',
				        		iconCls : 'icon-sub_red_delete-16',          		
				        		handler: function() {
				        		
				        		console.log(grid);
				        			Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
											        method     : 'POST',
								        			jsonData: {
														rrn: rec.get('rrn'),
														open_parameters : <?php echo acs_je($m_params->open_parameters); ?>
													},							        
											        success : function(response, opts){
											       
												     grid.getStore().load();
												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
				        		}
				    		});
				
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							
					
					}
					
					}, 
					
					 viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
		            frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', itemId: 'name_tab', name: 'f_tab', fieldLabel: 'Tabella', width: '150', labelWidth: 40, maxLength: 5, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_key', fieldLabel: 'Chiave', width: '150', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_des', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
					           
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_stato_from',
											        method     : 'POST',
								        			jsonData: {
														form_values: search_form.getValues(),
														open_parameters : <?php echo acs_je($m_params->open_parameters); ?>
													},							        
											        success : function(response, opts){
											       
												      m_grid.getStore().load();
												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
					   ]
					},{
                     xtype: 'button',
               		 text: 'Tab. esistenti',
		            iconCls: 'icon-database_active-32',
		            scale: 'large',	                     
		            handler: function() {
		             var name_tab = this.up('panel').down('#name_tab');
		           		my_listeners = {
			        			    afterClick: function(record, from_win){
			        				var rec = record.data;
			        				
			        				name_tab.setValue(record.data.tataid);
			        				
 									from_win.close();
 											
							        	}
									}
		            
		            		acs_show_win_std('Tabelle esistenti', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', {open_parameters : <?php echo acs_je($m_params->open_parameters); ?>}, 150, 400, my_listeners, 'icon-folder_search-16');          		
			     
			            }

			     }, {
                    xtype: 'button',
               		text: 'Apri tab.',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            	var grid = this.up('grid');
		            	var m_form = grid.down('form').getForm();
		            	grid.store.proxy.extraParams.show_tab = m_form.findField('f_tab').getValue();
		            	grid.store.load();
		            }
		         }, 
		         
		         {
                     xtype: 'button',
               		 text: 'Verifica',
		            iconCls: 'icon-folder_search-32',
		            scale: 'large',	                     
		            handler: function() {
		             var name_tab = this.up('panel').down('#name_tab');
		             var grid = this.up('grid');
		            
		             
		             		my_listeners = {
			        			    DoppioClick: function(record, from_win){
			        				grid.store.proxy.extraParams.show_tab = record.data.tataid;
		            	            grid.store.load();
 									//from_win.close();
 									}
						    }
		           	
		            		acs_show_win_std('Configurazioni dati tabelle', '<?php echo ROOT_PATH?>/module/admin/sincro_tabelle_di_sistema.php?fn=open_tab', {open_parameters : <?php echo acs_je($m_params->open_parameters); ?>}, 700, 400, my_listeners, 'icon-folder_search-16');          		
			     
			            }

			     },'->' ,
			     
			     
			     {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			                console.log(list_rows_modified);
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_master',
									        method     : 'POST',
						        			jsonData: {
						        				mod_rows : list_rows_modified,
						        				open_parameters : <?php echo acs_je($m_params->open_parameters); ?>
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php 

exit; 

}


if ($_REQUEST['fn'] == 'open_tab'){

	?>

{"success":true, "items": [

        {
        
        xtype: 'grid',
        multiSelect: true,
        store: {
						
					xtype: 'store',
					autoLoad:true,
				   					        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
						type: 'ajax',
						 actionMethods: {
					          read: 'POST'
					        },
				           extraParams: {
				           open_parameters : <?php echo acs_je($m_params->open_parameters); ?>
	        				},
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 
				
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
        			fields: [ 'tataid', 'C_ROW']
    			},
    		
	        columns: [
							        
	       		 {header   : 'TATAID', dataIndex: 'tataid', flex: 1},
	       		 {header   : 'Count', dataIndex: 'C_ROW', width: 50, aligh: 'right'},	           
	            
	         ],  
			listeners: {
	         
	          celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var loc_win = this.up('window');
					  	
					  	loc_win.fireEvent('afterClick', rec, loc_win);
	           
	            	}
	          
	           }
	         

	         }

			 ,viewConfig: {
	         getRowClass: function(record, index) {
	         //return ret;																
	         }   
	    },
			    
				
         
	        																			  			
	            
        }  

]
}

<?php

exit;

}



