<?php   

require_once "../../config.inc.php";


$s = new Spedizioni();
$main_module = new Spedizioni();



if ($_REQUEST['fn'] == 'get_parametri_form'){

	//Elenco opzioni "Area spedizione"
	$ar_area_spedizione = $s->get_options_area_spedizione();
	$ar_area_spedizione_txt = '';

	$ar_ar2 = array();
	foreach ($ar_area_spedizione as $a)
		$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
		$ar_area_spedizione_txt = implode(',', $ar_ar2);
		
   
?>

{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: false,
	title: '',
	//url: 'acs_riepilogo_spedizioni_carichi.php?fn=get_json_data_carichi',
	buttonAlign:'center',
	buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "REP_RIEP_SPE");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	            handler: function() {
	                form = this.up('form').getForm();
	            
		               if (form.isValid()){	   
		               
		                 if (Ext.isEmpty(form.findField("f_cliente").getValue()) && Ext.isEmpty(form.findField("f_data").getValue()) 
		                && Ext.isEmpty(form.findField("f_data_a").getValue())){
		              	acs_show_msg_error('Inserire almeno la data o il cliente per procedere');
		              	return false;
		              } 
		               
		                        	                
			               acs_show_panel_std('acs_riepilogo_spedizioni_carichi.php?', 'delivery', {form_values: form.getValues()});
			               this.up('window').close(); 	  
		                }	
	                
	                               
	            }
	        }],             
            
            items: [{
					xtype: 'fieldset',
	                title: 'Seleziona data',
	                defaultType: 'textfield',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	             	items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data da'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin: "5 10 5 10"
			
						},{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data a'
						   , name: 'f_data_a'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin: "5 10 5 10"
						},
						
						
						 {
                    xtype: 'radiogroup',
                    anchor: '100%',
                    fieldLabel: '',
                    items: [
                        {
                            xtype: 'radio'
                          , name: 'data_riferimento' 
                          , boxLabel: 'Programmata'
                          , inputValue: 'TDDTEP'
                          , checked: true
                          
                        },
                        {
                            xtype: 'radio'
                          , name: 'data_riferimento' 
                          , boxLabel: 'Spedizione'
                          , inputValue: 'TDDTSP'  
                         
						                          
                        }
                   ]
                }
					]
	            } ,
	            
	             {
					xtype: 'fieldset',
	                title: 'Seleziona area/divisione/itinerario',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
	             {
							name: 'f_areaspe',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 
								    ] 
							}						 
						},   	{
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?>	
								    ] 
							}						 
						},  {
							name: 'f_itinerario',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
								    ] 
							}						 
						},{
						flex: 1,
			            xtype: 'combo',
						name: 'f_cliente',
						fieldLabel: 'Cliente',
						 anchor: '-15',
						 margin: "20 20 10 10",	
						minChars: 2,			
            
			            store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_get_select_json.php?select=search_cli',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['cod', 'descr'],		             	
			            },
                        
						valueField: 'cod',                        
			            displayField: 'descr',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun cliente trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{descr}</span></h3>' +
			                        '{cod}' +
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000
        		},{
							flex: 1,							
							name: 'f_pagamento', 
							anchor: '-15',
						    margin: "20 10 10 10",
							xtype: 'combo',
							fieldLabel: 'Pagamento',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_pagamento(), '') ?> 	
								    ] 
								}						 
							}
	             
	             ]
	             }, {
					xtype: 'fieldset',
					border: true,
	                title: '',
	                defaultType: 'textfield',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	                items: [{
							flex: 1,						 
							name: 'f_solo_bloccati',
							xtype: 'checkboxgroup',
							fieldLabel: 'Solo ordini con carico',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_solo_ordini_con_carico' 
		                          , boxLabel: ''
		                          ,checked: true
		                          , inputValue: 'Y'
		                        }]														
						 }, {
							flex: 1,						 
							name: 'f_solo_bloccati',
							xtype: 'checkboxgroup',
							fieldLabel: 'Stampa importi',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_stampa_importi' 
		                          , boxLabel: ''
		                          , inputValue: 'Y'
		                        }]														
						 }
					]
	            }                                       
            ]
        }

	
]}

<?php	
 exit;	
}