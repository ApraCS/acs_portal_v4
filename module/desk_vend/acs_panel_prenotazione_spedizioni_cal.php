<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$all_params = array();
$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());


if ((int)$all_params['data_carico'] == 0)
	$data = $all_params['data_spedizione'];
else $data = $all_params['data_carico'];



function calc_minuti($ora_iniziale, $ora_finale) {
	$from_time 	= strtotime(print_ora($ora_iniziale) . ':00');
	$to_time 	= strtotime(print_ora($ora_finale) . ':00');		
	return round(abs($to_time - $from_time) / 60,2);
}


function calc_ora_end($ora_start, $minuti) {
	 $start = print_ora($ora_start) . ':00';
	 
	 //bug (se ora 23:00 + 1h non viene gestito correttamente)
	 if ($start == '23:00:00' && ($minuti == 0 || $minuti == 60))
	     $minuti = 59;	     
	 
	 if ($minuti == 0) return date('Hi00',strtotime('+1 hour +0 minutes', strtotime($start))); //default: 1h
	 $num_ore 		= intval($minuti / 60);
	 $num_minuti 	= $minuti % 60;
	 
	 // in base a minuti calcolo ore/minuti
	 return date('Hi00',strtotime("+{$num_ore} hour +{$num_minuti} minutes", strtotime($start))); //default: 1h
}

function out_trasp_vett($r) {
	$ret = '';
	$ret .= trim($r['D_VETT']);
	if (strlen(trim($r['CSDESC'])) > 0) {
		$ret .= ' [' . trim($r['CSDESC']) . ']';
	}
	return $ret;
}


function to_AS_date_from_format($format, $data){
	switch ($format){
		case "m-d-Y":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[0], $d_ar[1], $d_ar[2]);
			return to_AS_date($d);
		case "Y-m-d":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[1], $d_ar[2], $d_ar[0]);
			return to_AS_date($d);
			
	}
}


function decodifica_data($calendar_datatime){
	$data_ora = explode("T", $calendar_datatime);
	$data_t = $data_ora[0] . '';	
	$data_ext = explode("-", $data_t);
	
	return implode("", $data_ext);
}

function decodifica_ora($calendar_datatime){
	$data_ora = explode("T", $calendar_datatime);
	$ora_t = $data_ora[1] . '';
	$ora = substr($ora_t, 0, 2) . substr($ora_t, 3, 2) . substr($ora_t, 6, 2);	
	return $ora;
}



// ******************************************************************************************
// EDIT EVENT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'edit_event'){
	?>
	
{"success":true, "items": [

	{
		xtype: 'form',
		layout: {
                type: 'vbox',
                align: 'stretch'
            },
		flex: 1, frame: true,
		padding:    '10 10 10 10',
		margins:    '10 10 10 10',
		items: [{
		            xtype: 'textfield',
		            disabled: true,
		            itemId: this.id + '-title',
		            name: Extensible.calendar.data.EventMappings.Title.name,
		            fieldLabel:'Prenotazione',
		            anchor: '100%',
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        },{
		            xtype: 'extensible.daterangefield',
		            itemId: this.id + '-dates',
		            name: 'dates',
		            anchor: '95%',
		            singleLine: true,
		            fieldLabel: 'Orario',
		            showAllDay: false,
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        },{
		            xtype: 'combo',	                     
					itemId: 'risorsa',
	                name: Extensible.calendar.data.EventMappings.Risorsa.name,				
					fieldLabel: 'Risorsa',
					forceSelection: true,
	                anchor: '100%',	
	                delimiter: ' , ', 
	                editable: false, 
	                multiSelect: true,                 
	                
					store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [<?php echo acs_ar_to_select_json($main_module->find_TA_std('RISEN'), ""); ?>]
					},
	            
					valueField: 'id',                       
		            displayField: 'text',
		            
		            padding:    '10 0 10 0',
					margins:    '0 0 0 0'				
		        }],
		        
		buttons: [
			{
                itemId: 'delete-btn',
                iconCls: 'icon-delete',                
                text: 'Elimina prenotazione',
                disabled: false,
                handler: function() {
                	rec = this.up('window').open_vars.record;                	                	
					rec.store.remove(rec);

                            		//aggiorno calendario attuale
                            		this.up('window').open_vars.from_comp.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = this.up('window').open_vars.main_tab_panel.down('grid');												
									grid_sped.getStore().reload();				                    	
						
									this.up('window').close();
                	
                },
                //scope: this,
                minWidth: 150,
                hideMode: 'offsets'
            },
            {
                text: 'Salva',
                iconCls: 'icon-save',
                disabled: false,
                handler: function(){
                	rec = this.up('window').open_vars.record; 
			        var fields = rec.fields,
			            values = this.up('form').getForm().getValues(),
			            name,
			            M = Extensible.calendar.data.EventMappings,			            
			            obj = {};
			
			        fields.each(function(f) {
			            name = f.name;
			            if (name in values) {
			                obj[name] = values[name];
			            }
			        });
			        
			        rangefields = this.up('form').query('[xtype="extensible.daterangefield"]');
			        Ext.each(rangefields, function(rf) {
				        var dates = rf.getValue();
				        obj[M.StartDate.name] = dates[0];
				        obj[M.EndDate.name] = dates[1];
				        obj[M.IsAllDay.name] = dates[2];
				    });    
			        
			            rec.beginEdit();
        				rec.set(obj);
        				rec.endEdit();
        					
                            		//aggiorno calendario attuale
                            		this.up('window').open_vars.from_comp.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = this.up('window').open_vars.main_tab_panel.down('grid');												
									grid_sped.getStore().reload();				                    	
						
									this.up('window').close();
        					
                
                
                }
                //scope: this
            }
		],        
		        
		        
		listeners: {
				afterrender: function(comp){					
					console.log(comp.up('window'));
					rec = comp.up('window').open_vars.record;
					comp.loadRecord(rec);
					
					rangefields = comp.query('[xtype="extensible.daterangefield"]');
					Ext.each(rangefields, function(rf) {
						console.log(rf);
						
							//carico i valori per il datarange
							rf.setValue(rec.data);
							
							//disabilito i campi data (puo' modificare solo l'ora)
							Ext.each(rf.query('datefield'), function(dtrf) {
								dtrf.disable();
							});
							
					});
				 
				}
				
                , 'eventdelete': {
                        fn: function(win, rec){
                        	console.log('----- eventdelete ----');
                        	console.log(rec);
                            this.eventStore.remove(rec);
                            this.eventStore.sync();
                            win.hide();
                            this.showMsg('Event '+ rec.data.Title +' was deleted');
                        },
                        scope: this
                    }				
				
							 					 
		}		        
		        
	}

]}	
	
	
<?php	
	exit;
}



// ******************************************************************************************
// AGGIORNA PORTA/ORA SU SPEDIZIONE/STABILIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_sped_set_ora'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	$m_params = acs_m_params_json_decode();

	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDBCAR = ?, TDDTIC = ?, TDHMIC = ?, TDDTFC = ?, TDHMFC = ? 
			WHERE " . $main_module->get_where_std() . " AND TDNBOF = ? AND TDSTAB = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($m_params->porta,
							 $m_params->data, $m_params->ora,
							 $m_params->data, (int)$m_params->ora + 10000,
							 $m_params->sped_id, $m_params->c_stabilimento));
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// AGGIORNA DATA DI CARICO SU SPEDIZIONE/STABILIMENTO ??????? SERVE QUI??????
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_cambia_data_carico'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	$m_params = acs_m_params_json_decode();

	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDDTIC	 = ?, TDDTFC = ?, TDBCAR = '', TDHMIC = 0, TDHMFC = 0
			WHERE " . $main_module->get_where_std() . " AND TDNBOF = ? AND TDSTAB = ?";
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt, array( $m_params->to_date, $m_params->to_date,
												$m_params->record->TDNBOF, $m_params->record->TDSTAB));

			$ret['success'] = true;
	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// AGGIORNA EVENTO (resize, ....)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_update_event'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	$m_params = acs_m_params_json_decode();

//	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} SET CSHMPG = ?, CSHMFG = ? WHERE CSPROG = ? AND CSCALE = '*SPR'";
//	$stmt = db2_prepare($conn, $sql);
//	$result = db2_execute($stmt, array($m_params->start, $m_params->end, $m_params->rec->id));

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// CREA EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_crt_events'){
	global $cfg_mod_Spedizioni;
	$ret = array();

	//$all_params = ...;

	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}
			SET  CSPORT = ?, CSDTPG = ?, CSHMPG = ?
			WHERE CSPROG = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array(
			$all_params['Porta'], decodifica_data($all_params['StartDate']),decodifica_ora($all_params['StartDate']),
			$all_params['CreatedBy']));

	//registro attivita utente
		$au = new AttivitaUtente();
		$au->crea($main_module, 'PREN_SPED', $id_ditta_default,
				array($all_params['CreatedBy'])				
		);	
	
			$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// AGGIORNA EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_upd_events'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	
	//$all_params = ...;	
	
	$minuti = calc_minuti(decodifica_ora($all_params['StartDate']), decodifica_ora($all_params['EndDate']));
		
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} 
				SET CSDTPG = ?, CSHMPG = ?, CSMDPG = ?
				WHERE CSPROG = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array( 
										decodifica_data($all_params['StartDate']),decodifica_ora($all_params['StartDate']), $minuti,
										$all_params['EventId']));
	
	echo db2_stmt_errormsg($stmt);


	//todo: COSA E'???? SERVE??????
	//registro attivita utente
	$au = new AttivitaUtente();
	$au->crea($main_module, 'PREN_SPED', $id_ditta_default,
			array($all_params['EventId'])
	);
	
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// DELETE EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_del_events'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	$m_params = acs_m_params_json_decode();
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} SET CSPORT = ?, CSHMPG=? 
				 WHERE CSPROG = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array('', 0, $all_params['EventId']));

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// EVENTI CALENDARIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_get_events'){
	global $conn;
	global $cfg_mod_Spedizioni;	
	
	
 $ret = array();
 $ret['events'] = array(); 
 
 //devo per forza aver specificato unaporta
 if (strlen($_REQUEST['f_porta']) == 0){
 	echo acs_je($ret); exit;
 } 

 	if (strlen($_REQUEST['f_porta']) == 0) $_REQUEST['f_porta'] = 'NNNNNNNNNNNNNNNN';
 
 	
 		//data iniziale e di fine
 			$from_calendar_format = 'Y-m-d'; 		
 			$d_start 	= to_AS_date_from_format($from_calendar_format, $_REQUEST["startDate"]);
 			$d_end 		= to_AS_date_from_format($from_calendar_format, $_REQUEST["endDate"]); 			

 	
 			$sql = "SELECT TA_VETT.TAKEY1 AS C_VETT, TA_VETT.TADESC AS D_VETT,
 							TA_TRAS.TAKEY1 AS C_TRASP, TA_TRAS.TADESC AS D_TRASP, TA_ASPE.TARIF2 AS C_STAB,
 							SUM(TDVOLU) AS S_TDVOLU, SUM(TDTOCO) AS S_TDTOCO, 							
 							CSPROG, CSCITI, CSDTPG, CSHMPG, CSDESC, CSMDPG, CSCAUT, TA_AUTO.TADESC AS D_AUTO
 					FROM {$cfg_mod_Spedizioni['file_calendario']} SP
 						
 					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
 						ON SP.CSDT = TD.TDDT AND SP.CSPROG = TD.TDNBOF	 

					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  		ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
					INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
					  ON TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE
				  		
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
					   ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
					   ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'		   
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_AUTO
					   ON TA_AUTO.TADT = SP.CSDT AND TA_AUTO.TAKEY1 = SP.CSCAUT AND TA_AUTO.TATAID = 'AUTO'		   
					   
				   
					WHERE " . $main_module->get_where_std() . "
	 				AND CSDTPG >= {$d_start} AND CSDTPG <= {$d_end}
	 				AND TA_ASPE.TARIF2 = " . sql_t($_REQUEST['c_stabilimento']) . "
					AND CSPORT = '{$_REQUEST['f_porta']}' AND CSHMPG > 0
					GROUP BY TA_TRAS.TAKEY1, TA_TRAS.TADESC, TA_VETT.TAKEY1, TA_VETT.TADESC, TA_ASPE.TARIF2, CSPROG, CSCITI, CSDTPG, CSHMPG, CSDESC, CSMDPG, CSCAUT, TA_AUTO.TADESC

				";

 			$stmt = db2_prepare($conn, $sql);
 			echo db2_stmt_errormsg();
 			$result = db2_execute($stmt);
 			
 			$somma_volume = 0;
 			$somma_colli  = 0; 			
 
 			while ($r = db2_fetch_assoc($stmt)) {

 				$e = array();
 					$e['id'] = (int)$r['CSPROG']; //id calendario 					
 					$e['title'] = $main_module->decod_std('ITIN', $r['CSCITI']) . 
 	 							" [" . $main_module->get_el_carichi_by_TDNBOF($r['CSPROG'], 'TN', 'N', 'N') . "] " . 
 								out_trasp_vett($r) . " " . trim($r['D_AUTO']);
 					$e['start'] = print_date($r['CSDTPG'], '%Y-%m-%d') . " " . print_ora($r['CSHMPG']) 			. ":00";
 					
 					//all'ora di start aggiunto i minuti della durata della programmazione
 					$ora_end = calc_ora_end($r['CSHMPG'], $r['CSMDPG']); 					
 					$e['end'] 	= print_date($r['CSDTPG'], '%Y-%m-%d') . " " . print_ora($ora_end) 	. ":00";

// 					$ora_inizio_t = date('His', strtotime(print_ora($ora_inizio_t)) + 30*60);
// 					$ora_finale_t = date('His', strtotime(print_ora($ora_inizio_t)) + $durata_in_minuti*60); 					
 					
 					$e['notes'] = "Have fun";
 					$e['risorsa'] = '';
 					$e['stabilimento'] = trim($r['C_STAB']);
 					$c['trasp'] = trim($r['C_TRASP']); 					

 					if (strlen(trim($all_params['f_trasportatore'])) > 0){
 						//se sono entrato filtrando un trasportatore, se non e' mio
 						// l'evento non deve essere visibile (descrizione) e modificabile
 						if (trim($r['C_TRASP']) == $all_params['f_trasportatore'])
	 						$e['cid'] = 1; //id calendario
	 					else {
	 						$e['cid'] = 3;
	 						$e['title'] = "#{$r['CSPROG']}";
	 					}	 						
 						
 					} else {
	 					if (trim($r['C_TRASP']) == $all_params['c_trasp'])
	 						$e['cid'] = 1; //id calendario
	 					else 
	 						$e['cid'] = 1; //2 (al momento non differenzio il mio trasp dagli altri, se posso gestirli)
 					}
 					
 					$ret['events'][] = $e; 		

 					$somma_volume += $r['S_TDVOLU'];
 					$somma_colli  += $r['S_TDTOCO'];
 					
 			}
 
 			$ret['totals']['TDVOLU'] = $somma_volume;
 			$ret['totals']['TDTOCO'] = $somma_colli;
 
 echo acs_je($ret);	
 exit;	
} //fn=cal_get_events





// ******************************************************************************************
// ELENCO SPEDIZIONI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_spedizioni'){
	global $conn;
	global $cfg_mod_Spedizioni;
	
	$ret = array();
	
	
	//TODO: DRY******************************************
		//parametri dalla form di ingresso
		$form_ep = (object)$all_params['form_ep'];
		
		//data (NON DEVO FILTRARE PER DATA) PERCHE' LO PRENDO GIA' DAL RECORD DELLO STABILIMENTO
		
		//area spedizione
		$sql_form_where .= sql_where_by_combo_value('TA_ASPE.TAKEY1', $form_ep->f_area_spedizione);
		
		//tipologia trasporto (multipla)
		$sql_form_where .= sql_where_by_combo_value('SP.CSTITR', $form_ep->f_trasporto);
			
		//divisione
		$sql_form_where .= sql_where_by_combo_value('TDCDIV', $form_ep->f_divisione);
			
		//trasportatore
		$sql_form_where .= sql_where_by_combo_value('TA_TRAS.TAKEY1', $form_ep->f_trasportatore);

	//***********************************************************	
	
	
	
	
	$s_field = explode(" ", "CSPROG CSCITI CSHMPG CSPORT CSDESC");
	$sql = "SELECT " . implode(", ", $s_field) . ", TA_TRAS.TADESC AS D_TRASP,
						TA_VETT.TAKEY1 AS C_VETT, TA_VETT.TADESC AS D_VETT, TA_AUTO.TADESC AS D_AUTO
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				   ON TD.TDDT=SP.CSDT AND SP.CSPROG = TD.TDNBOF AND CSCALE = '*SPR'
				   
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
				ON TA_ASPE.TADT = TA_ITIN.TADT AND TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE
				   
				   
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			   ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			   ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_AUTO
					   ON TA_AUTO.TADT = SP.CSDT AND TA_AUTO.TAKEY1 = SP.CSCAUT AND TA_AUTO.TATAID = 'AUTO'				   		   
				   
			WHERE " . $main_module->get_where_std() . "
			  AND (TDDTSP = {$data})
			  {$sql_form_where} 

			/* escludo le DP non ancora evase */
			AND (SP.CSSTSP <> 'DP' OR TD.TDFN11 <> 0)
			  
			GROUP BY " . implode(", ", $s_field) . ", TA_TRAS.TADESC, TA_VETT.TAKEY1, TA_VETT.TADESC, TA_AUTO.TADESC"; 

	
    ///&if (trim($row['CSSTSP']) == 'DP' && $row['TDFN11'] == 0){ //da programmare e non ancora evaso			  
			  
	$stmt = db2_prepare($conn, $sql);	
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while ($row = db2_fetch_assoc($stmt)) {
		$row['id']     = $row['CSPROG'];
		$row['TDNBOC'] = $row['CSPROG'];  //SI DEVE CONTINUARE A CHIAMARE TDNBOF ALTRIMENTI GLI EVENTI DEL CALENDARIO NON FUNZIONANO
		$row['CSPORT'] = $row['CSPORT'];
		$row['CSHMPG'] = $row['CSHMPG'];
		$row['D_CSCITI'] = $main_module->decod_std('ITIN', $row['CSCITI']);
		$row['EL_CARICHI_D'] = $main_module->get_el_carichi_by_TDNBOF($row['CSPROG'], 'TN', 'N', 'N');

		if ((int)$row['id'] == (int)$all_params['sped_id_selected'])
			$row['rowCls'] = 'selected';
		
		if (strlen(trim($row['CSDESC'])) > 0) {
			$row['CSDESC'] = " [" . acs_u8e(trim($row['CSDESC'])) . "]";
		} else {
			$row['CSDESC'] = implode(" - ", array(
						acs_u8e(trim($row['D_VETT'])),
						acs_u8e(trim($row['D_AUTO']))
			));
		}
		
		
					
		$ret[] = $row;
	}
	

	echo acs_je($ret);
	exit;
} //fn=cal_get_events



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
	
$panel_id = rand();

//dalla spedizione selezionata recupero l'itinerario e l'area di spedizione -> stabilimento
$sped_selected = $main_module->get_spedizione((int)$all_params['sped_id_selected']);
$aspe = new AreeSpedizione();
$aspe->load_rec_data_by_itin($sped_selected['CSCITI']);
$all_params['c_stabilimento'] = $aspe->rec_data['TARIF2'];

?>
{"success":true, "items":
 {
  xtype: 'panel',
  id: 'panel-prenotazione_spedizioni_cal_<?php echo $panel_id; ?>',
  title: 'Exit <?php echo $all_params['c_stabilimento']; ?>',
  closable: false,
  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
  
  listeners: {                    
    afterrender: function (comp) {
                    		comp.show(); 
                 	},
                 	
   beforeClose: function(a,b,c,d){
    console.log('_beforeClose');
    console.log(a);
    console.log(b);
    console.log(c);
    console.log(d);    
   }              	
  },
  
  
  items: [
  
  // ------------------------ menu laterale -----------------------------
  		, {
			xtype: 'panel',
			layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
			flex: 0.4,			
			items: [			
					
					//form filtri
					, {
						xtype: 'form',
						layout: {type: 'hbox', border: false},
						items: [

			
							 , {
								     name: 'f_data'
								   , disabled: false                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: '<b>Data di spedizione</b>'
								   , margins:    '10 10 10 10'
								   , padding:    '15 10 10 10'
								   , labelAlign: 'right'
								   , format: 'd/m/Y'						   						   
								   , submitFormat: 'Ymd'
								   , allowBlank: false
								   , width: 230
								   , value: '<?php echo print_date($data, "%d/%m/%Y"); ?>'
								   , listeners: {
								       		invalid: function (field, msg) {
								       			Ext.Msg.alert('', msg);
								       		},
								       		
								            change: function(){      
								                  		
								            	var form = this.up('form').getForm();
										
												if(form.isValid()){
												
													f_d = form.getValues()['f_data'];
													f_d_Y = f_d.substr(0, 4);
													f_d_M = f_d.substr(4, 2) - 1;
													f_d_D = f_d.substr(6, 2);																		
													var d1 = new Date(f_d_Y, f_d_M, f_d_D);
													var to_date = Ext.Date.format(d1, 'Ymd'),
												
													//sul giorno selezionato eseguo la procedura per associare
													// le porte e orari di default
													
													panel_main_fo_presca = this.up('#panel-prenotazione_spedizioni_cal_<?php echo $panel_id; ?>');
													
											        Ext.Ajax.request({
											            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_porte_orari_default',
											            method: 'POST',
									        			jsonData: form.getValues(),						            
											            success: function ( result, request) {
											                var jsonData = Ext.decode(result.responseText);
										                    	
							                            	//aggiorno tutte
													        cal_porte = panel_main_fo_presca.query('[xtype="extensible.calendarpanel"]');	                    	
											                Ext.each(cal_porte, function(cal) {cal.setStartDate(d1);});
																
																//aggiorno elenco spedizioni del giorno selezionato
																grid_sped = panel_main_fo_presca.down('grid');
																grid_sped.getStore().proxy.extraParams.data_carico = to_date;
																grid_sped.getStore().reload();
											                
											                											                															
											            },
											            failure: function ( result, request) {
											            }
											        });									
													
				
											    }    								                  		
												                  		
								                  		            	
								            } //change															       		
								       		
									 },
									 	
								}, {
									xtype: 'button'
								    , iconCls: 'icon-print-32'
	            					, scale: 'large'
	            					, margins:    '10 10 10 10'
								    , padding:    '15 10 10 10'
								    , handler: function() {
								    	form = this.up('form').getForm();
						          	
					        	acs_show_win_std('Conferma report', 'acs_panel_prenotazione_spedizioni_cal_report.php?fn=open_form', {
        						c_stabilimento: <?php echo j($all_params['c_stabilimento']); ?>,
        						form_values: form.getValues()
        						},330, 130, null, 'icon-print-16');  								    
								    }
								}			
			
							
						
						]
						
 	
					}
					
  
  					//----- elenco ricezioni ------------
  					, {					
						xtype: 'grid',
						flex: 1,
						//id: 'fo_presca_grid_spedizioni',
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},

	        			viewConfig: {
	        
				            plugins: {
				                ptype: 'gridviewdragdrop',
				                ddGroup: 'DayViewDD',
				                enableDrop: false
				            }
				            
				            
					       , getRowClass: function(record, index) {
					        	if (record.get('rowCls') == 'selected')
					        		return ' segnala_riga_giallo';															
					         }   				            
				            
				            
				        },
     					
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_spedizioni',
								method: 'POST',
								type: 'ajax',
								
								extraParams: <?php echo acs_raw_post_data() ?>,
								
									/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */					
						            doRequest: function(operation, callback, scope) {
						                var writer  = this.getWriter(),
						                    request = this.buildRequest(operation, callback, scope);
						
						                if (operation.allowWrite()) {
						                    request = writer.write(request);
						                }
						
						                Ext.apply(request, {
						                    headers       : this.headers,
						                    timeout       : this.timeout,
						                    scope         : this,
						                    callback      : this.createRequestCallback(request, operation, callback, scope),
						                    method        : this.getMethod(request),
						                    disableCaching: false // explicitly set it to false, ServerProxy handles caching
						                });
						
										/* extraParams lo passo come jsonData */
						                request.jsonData = this.extraParams;
						
						                Ext.Ajax.request(request);
						                return request;
						            },	   								
										
														
								
								
								
								

								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
					            						
							},
								
							fields: ['id', 'CSCITI', 'D_CSCITI', 'TDNBOC', 'TDSTAB', 'TDCCON', 'TDDCON', 'CSPORT', {name: 'CSHMPG', type: 'int'}, 'D_TRASP', 'TDDTSP', 'rowCls', 'EL_CARICHI_D', 'C_VETT', 'D_VETT', 'CSDESC']
										
										
						}, //store
						multiSelect: true,
						
					
						columns: [{header: 'Sped.', dataIndex: 'TDNBOC', width: 50, hidden: true},
								  {header: 'Itinerario', dataIndex: 'D_CSCITI', flex: 8,
								      renderer: function(value, metaData, record){				    			    	
											metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + ' (#' + record.get('TDNBOC') + ')' + '"';    			    	
				    			    	 	return value + ' (#' + record.get('TDNBOC') + ')';				    			    	 			    	
				    			    	}								  
								  },
								  {header: 'Carichi', dataIndex: 'EL_CARICHI_D', width: 60,
								      renderer: function(value, metaData, record){				    			    	
											metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value)  + '"';    			    	
				    			    	 	return value;				    			    	 			    	
				    			    	}
								  },
								  {header: 'Vett.', dataIndex: 'CSDESC', width: 65,
								      renderer: function(value, metaData, record){
								      		q_tip = '';
								      		q_tip = q_tip + '<br/>Vettore: ' + record.get('D_VETT');				    			    	
								      		q_tip = q_tip + '<br/>Trasportatore: ' + record.get('D_TRASP');
								      		q_tip = q_tip + '<br/>' + record.get('CSDESC');
											metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
				    			    	 	return value;				    			    	 			    	
				    			    	}								  
								  },
								  {header: 'Porta', dataIndex: 'CSPORT', width: 65},
								  {header: 'Ora', dataIndex: 'CSHMPG', width: 50, renderer: time_from_AS, align: 'right'}]
								  
					 , listeners: {
					 
					 
					    //per dd
					    render: function(grid){ 
							
										    grid.getDragData = function(e) {
										        console.log("dragging");
										        var target = Ext.get(e.getTarget());
										        thisProxy = Ext.ensible.cal._statusProxyInstance;
										        var record = this.getSelectionModel().getSelectedNode();
										        eventName = "plop"; /*we don't care*/
										        eventData = new Ext.ensible.cal.EventRecord();
										        eventStore = myCal.calendarStore; /*myCal is a CalendarPanel*/
										        eventRecord = new Ext.ensible.cal.EventRecord({
										            'EventId': 1500,
										            'cid': '',
										            'Title': eventName,
										            'StartDate': new Date(),
										            'EndDate': new Date(),
										            'notes': '',
										        });
										        eventStore.insert(0, eventRecord);
										        eventRecord = eventStore.getAt(0);
										        newDDel = document.createElement("div");
										        this.ddel.className = 'jibber-calendar-week-bd-evt-' + eventRecord.data['EventId'];
										        return {
										            ddel: this.ddel,
										            item: target,
										            proxy: thisProxy,
										            data: eventData,
										            type: 'eventdrag',
										        };
										    }								
							
					    	
					    }, //renderer
					    
					 
					 
					 
					 
					 

						itemcontextmenu : function(grid, rec, node, index, event) {
						  				  event.stopEvent();
									      var record = grid.getStore().getAt(index);		  
									      var voci_menu = [];
									      

				
									      var menu = new Ext.menu.Menu({
									            items: voci_menu
										}).showAt(event.xy);
								   }					 
					 	
					 	
					 }			  
						 
					} //menu laterale
  			]
  		}	
  
  
  
  // ------------------------ panel e calendario --------------------------------
  
	, {
	  xtype: 'panel', flex: 1,
	  title: <?php echo j("Programmazione porta/orario di uscita per stabilimento/data spedizione [Stabilimento " . $main_module->decod_std('START', $all_params['c_stabilimento']) . "]"); ?>,	  
	  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
	  cls: 'acs-light',	  	  	
	  
		tools: [{ 
		        qtip: 'Chiudi',
		        handler: function(e, toolEl, panel, tc){
		            m_tab = Ext.getCmp('panel-prenotazione_spedizioni_cal_<?php echo $panel_id; ?>');
		            m_tab.close();
		        }
		    }],	  
	    
	  items: [  
  
  
	  	<?php
	  	//visualizzo il calendario per ogni porta 
	  	$el_porte = $main_module->get_elenco_porte_stabilimento($all_params['c_stabilimento']);
	  	foreach ($el_porte as $kp => $porta){  	
	  	?>
  
  
		 , {
		        xtype: 'extensible.calendarpanel',		        
		        flex: 1,
		        border: false,
		        frame: true,
		        title_initial: <?php echo j("Porta: " . $porta['text']); ?>,
				title: <?php echo j("Porta: " . $porta['text']); ?>,
				
                collapsible: false,
                collapsed: false,	
                
				enableEditDetails: false,
				readOnly: false,                 
                
					viewConfig: {
				            // enable dd sharing with the grid
				            ddGroup: 'DayViewDD'
				        },                
                


        			//recupero eventi
					eventStore: new Extensible.calendar.data.EventStore({

					       autoLoad: true,
        				   autoSync: true,

		                    proxy: {
		                    	url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
		                        type: 'ajax',
								method: 'POST',		                        
		                        extraParams: {
		                        	f_porta: <?php echo j($porta['id']); ?>
		                          , c_stabilimento: <?php echo j($all_params['c_stabilimento']); ?>	 
		                          , c_trasp: <?php echo j($all_params['c_trasp']); ?>
		                          , f_trasportatore: <?php echo j($all_params['form_ep']->f_trasportatore); ?>		                          		                          
		                        },
		                        reader: {
            						type: 'json',		                        
		                            root: 'events'
		                        },
		                        
		                        
					            writer: {
					                type: 'json',
					                writeAllFields: true
					            },		
					            
					            api: {
					                read: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
					                create: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_crt_events',
					                update: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_upd_events',
					                destroy: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_del_events',
					            },
					            				                        
		                        
		                    }					          
					    }),


                    activeItem: 0, // day view 
                    
                    showNavBar: false,                      
                    
                    dayViewCfg: {
                        showHeader: false,
                        showWeekLinks: true,
                        showWeekNumbers: true,
						startDate: js_date_from_AS(<?php echo $data; ?>),
						viewStartHour  : 0,
						scrollStartHour: 6,
						ddGroup: 'DayViewDD',
						showTime: false,
						showTodayText: false,						
						showHourSeparator: true,
						hourHeight: 30							
                    },                      
			    
			    
			    
					//definizione calendari        
        			calendarStore: new Extensible.calendar.data.MemoryCalendarStore({
            			data: {
						    "calendars" : [{
						        "id"    : 1,
						        "title" : "Std",
						        "color" : 26
						    },{
						        "id"    : 2,
						        "title" : "non usato",
						        "color" : 2
						    },{
						        "id"    : 3,
						        "title" : "Non modificabili",
						        "color" : 22
						    },{
						        "id"    : 4,
						        "title" : "Altro",
						        "color" : 26
						    }]
						}
        			}),			
        			    
			    
                  listeners: {
                  
						'afterrender': function(comp){
							comp.store.on('load', function(store, records, options) {
							  comp.setTitle(comp.title_initial);
							  comp.setTitle(comp.title + '<div align=right>Mc: ' + floatRenderer2(store.proxy.reader.jsonData.totals.TDVOLU) + ', Colli: ' + floatRenderer0(store.proxy.reader.jsonData.totals.TDTOCO) + '</div>');
							}, comp);
						},                  
                  
                  
                  
						'eventresize': {
                            fn: function(dd, rec, dt){
                            	console.log('eventresize');                            	
                            		//aggiorno calendario attuale
                            		this.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = dd.up('#panel-prenotazione_spedizioni_cal_<?php echo $panel_id; ?>').down('grid');												
									grid_sped.getStore().reload();				                    	
                            	
                            }
                         }
                         
						, 'eventmove': {
                            fn: function(dd, rec, dt){
                            	console.log('eventmove');                            	
                            		//aggiorno calendario attuale
                            		this.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = dd.up('#panel-prenotazione_spedizioni_cal_<?php echo $panel_id; ?>').down('grid');												
									grid_sped.getStore().reload();				                    	
                            	
                            }
                         }
                         
                         
					   , 'eventrdrag': {
                            fn: function(dd, rec, dt, a, b){
                            	console.log('------ eventrdrag ------');
                            }
                         }
                         
					   , 'eventadd': {
                            fn: function(dd, rec, dt, a, b){
                            	console.log('------ eventadd ------');
                            	
                           		//Aggiorno tutti i calendari
                            	//aggiorno tutte
						        cal_porte = this.up('#panel-prenotazione_spedizioni_cal_<?php echo $panel_id; ?>').query('[xtype="extensible.calendarpanel"]');	                    	
				                Ext.each(cal_porte, function(cal) {cal.store.reload();});
                           		
												
								//aggiorno elenco spedizioni del giorno selezionato
								grid_sped = this.up('#panel-prenotazione_spedizioni_cal_<?php echo $panel_id; ?>').down('grid');												
								grid_sped.getStore().reload();				                    	
                            	
                            }
                         }                         

                         
                        , 'eventclick': {
                			fn: function(panel, rec, el){                			
                			
		                		if (parseInt(rec.get(Extensible.calendar.data.EventMappings.CalendarId.name)) == 3)
                                	return false; //non posso spostare gli eventi non miei                			
                			
		                		acs_show_win_std('Modifica prenotazione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=edit_event', null, 680, 220, null, 'icon-inbox-16', null, null, {
		                			record: rec, 
		                			from_comp: panel,
		                			main_tab_panel: panel.up('#panel-prenotazione_spedizioni_cal_<?php echo $panel_id; ?>')
		                			});			                			                            	                           
                                return false; //per non attivare evento std
                            },
                            scope: this
                        }	
                         
                       //per prevenire la creazione di nuovi eventi  
					   , 'dayclick': {fn: function(dd, rec, dt, a, b){return false;}}
					   , 'rangeselect': {fn: function(dd, rec, dt, a, b){return false;}}
					   
					   //per calendario readonly
                        , 'beforeeventmove': {
                			fn: function(panel, rec, el){                			
		                		console.log('beforeeventmove');			                			                            	                           
		                		if (parseInt(rec.get(Extensible.calendar.data.EventMappings.CalendarId.name)) == 3)
                                	return false; //non posso spostare gli eventi non miei
                            },
                            scope: this
                        }
                        , 'beforeeventresize': {
                			fn: function(panel, rec, el){                			
		                		console.log('beforeeventresize');			                			                            	                           
		                		if (parseInt(rec.get(Extensible.calendar.data.EventMappings.CalendarId.name)) == 3)
                                	return false; //non posso spostare gli eventi non miei
                            },
                            scope: this
                        }                        					                                                    
                        
                                           	
                  },			    
			    
			    
			    
			    
		        
		        // this is a good idea since we are in a TabPanel and we don't want
		        // the user switching tabs on us while we are editing an event:
		        editModal: true
		    }
  

  		
  	<?php } //per ogni porta?>	
  		
  	  ]
    } //panel con calendari porte
  
  ]
  
  
  
 }
}