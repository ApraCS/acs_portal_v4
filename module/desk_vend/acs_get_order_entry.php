<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$as = new SpedAssegnazioneOrdini();

if ($_REQUEST['anche_rilasciate'] == 'Y')
	$solo_aperte = 'N';
else
	$solo_aperte = 'Y';

			$n_ord = $_REQUEST['nord'];
			$stmt = $as->get_el_entry_per_ordine($n_ord, $solo_aperte);
			$data = array();

			while ($row = db2_fetch_assoc($stmt)) {
			    $row['ASCAAS_OUT'] = acs_u8e($s->decod_std('ATTAV', $row['ASCAAS']));
				
				if (trim($row['ASCARI']) != ''){
					$ta_ar = $s->get_TA_std('RILAV', $row['ASCAAS'], $row['ASCARI']);
					$row['ASCARI_OUT'] = $ta_ar['TADESC'];
				}	
				
				$data[] = $row;								
			}
			
			$ord_exp = explode('_', $n_ord);
			$anno = $ord_exp[3];
			$ord = $ord_exp[4];			
		  	$ar_ord =  $s->get_ordine_by_k_docu($n_ord);
			$m_ord = array(
								'TDONDO' => $ar_ord['TDONDO'],
								'TDOADO' => $ar_ord['TDOADO'],								
								'TDDTDS' => print_date($ar_ord['TDDTDS'])
						   );
			
		echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
		
		$appLog->save_db();		


?>
