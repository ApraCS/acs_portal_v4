<?php 

require_once "../../config.inc.php";

$s = new Spedizioni();



if ($_REQUEST['fn'] == 'get_json_data'){	

	//bug parametri linux
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['form_values'] = strtr($_REQUEST['form_values'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	
$form_values = json_decode($_REQUEST['form_values']);

if (isset($form_values->tipo_area)){
	 	$sel = $form_values->tipo_area;
		if($form_values->tipo_area == 'TAKEY1')
				$sel_desc = " TADESC ";
		elseif ($form_values->tipo_area == 'TDCAG1')
				$sel_desc = " TDDAG1 ";
		elseif ($form_values->tipo_area == 'TDDVN1')
				$sel_desc = " TDDVN1 ";		
		else
				$sel_desc = " TDDNAZ ";
}else{
		$sel = " TAKEY1 ";
		$sel_desc = " TADESC ";
}

$sql_where = '';
$parametri = array();
		
if (isset($form_values->f_area_spedizione)){
	$sql_where .= " AND TAASPE = ?";
	$parametri[] = $form_values->f_area_spedizione;
}

if (isset($form_values->f_divisione)){
	$sql_where .= " AND TDCDIV = ?";
	$parametri[] = $form_values->f_divisione;
}
		
if ($form_values->f_tipologia_ordine != null)
	$sql_where .= " AND TDCLOR IN (". sql_t_IN($form_values->f_tipologia_ordine) . ")";
		
if ($form_values->f_agente != null)
	$sql_where .= " AND TDCAG1 IN (". sql_t_IN($form_values->f_agente) . ")";

		
if ($form_values->f_tipo_ordine != null)
	$sql_where .= " AND TDOTPD IN (". sql_t_IN($form_values->f_tipo_ordine) . ")";

if (isset($form_values->f_stato_ordine) && count($form_values->f_stato_ordine) > 0)
	$sql_where .= " AND TDSTAT IN (". sql_t_IN($form_values->f_stato_ordine) . ")";


$tot = " sum(TDVOLU) as T_TDVOLU,  sum(TDTOCO) as T_TDTOCO, 
		 	sum(TDPLOR) as T_TDPLOR, sum(TDTIMP) as T_TDTIMP,
		 	sum(RDQTA) AS T_RDQTA";
$avg = " avg(TDVOLU) as M_TDVOLU,  avg(TDTOCO) as M_TDTOCO,
		 	avg(TDPLOR) as M_TDPLOR, avg(TDTIMP) as M_TDTIMP,
		 	avg(RDQTA) AS M_RDQTA";


$sql = "
		SELECT COUNT(DISTINCT TDDOCU) AS NUM_ORD, $sel, $sel_desc AS GROUP, $tot, $avg
		FROM {$cfg_mod_Spedizioni['file_testate']} TD 

		
		LEFT OUTER JOIN (
			SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
			FROM {$cfg_mod_Spedizioni['file_righe']} RDO
			WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE' 
			GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO			
			) RD
			ON TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO				
		
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
		  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG				    
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
		  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI				    
		WHERE " . $s->get_where_std(null, array('filtra_su' => 'SP,TD')) . " AND (TDDTCF > 0 OR TDNRCA > 0)  $sql_where
		GROUP BY $sel, $sel_desc
		";	



$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();		
$result = db2_execute($stmt, $parametri);

$ar_records = array();
while ($r = db2_fetch_assoc($stmt)){
	$r['GROUP'] = acs_u8e($r['GROUP']);
	$ar_records[] = $r;
}

echo acs_je($ar_records);
exit();

}

?>



{"success":true, "items": [
{
			xtype: 'panel',
			title: '',
			layout: {
          type: 'hbox',
          align: 'stretch'
      },
			items: [{
				xtype: 'grid',
				layout: 'fit',
				flex: 2.3,
				features: [{
			      ftype: 'summary'
			  }],
				title: 'Dati',
				autoScroll: true,
				store: new Ext.data.Store({												        
						autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							type: 'ajax',
							extraParams: {},
							reader: {
					            type: 'json',
					            root: 'root'
					        }
						},
	        	fields: [{name:'GROUP', type: 'string'},
										 {name:'NUM_ORD', type: 'float'},
										 {name:'T_TDVOLU', type: 'float'}, 
										 {name:'T_TDTOCO', type: 'float'}, 
										 {name:'T_TDPLOR', type: 'float'}, 
										 {name:'T_TDTIMP', type: 'float'},
										 {name:'T_RDQTA', type: 'float'},										 
										 {name:'M_TDVOLU', type: 'float'}, 
										 {name:'M_TDTOCO', type: 'float'}, 
										 {name:'M_TDPLOR', type: 'float'}, 
										 {name:'M_TDTIMP', type: 'float'},
										 {name:'M_RDQTA', type: 'float'}]
	    	}),				 		
				columns: [
        { header: 'Denominazione', flex: 2, dataIndex: 'GROUP'},
        { header: 'Nr. Ordini', dataIndex: 'NUM_ORD', flex: 1, align: 'right', summaryType: 'sum', 
                              					summaryRenderer: function(value, summaryData, dataIndex) {
                              					            return floatRenderer0(value); 
                              					       }	
																			},
        { header: 'Volume', columns: [{header: 'Totale', dataIndex: 'T_TDVOLU',  sortable: true, width: 80, align: 'right', renderer: floatRenderer2, summaryType: 'sum', 
                              					summaryRenderer: function(value, summaryData, dataIndex) {
                              					            return floatRenderer2(value); 
                              					       }	
																			},
																		 {header: 'Media', dataIndex: 'M_TDVOLU', sortable: true, width: 60, align: 'right', renderer: floatRenderer2, summaryType: 'average', 
                              					summaryRenderer: function(value) {
                              					            return floatRenderer2(value); 
                              					       }	
																			}]},
				{ header: 'Colli', columns: [{header: 'Totale', dataIndex: 'T_TDTOCO', sortable: true, width: 80, align: 'right', renderer: floatRenderer0, summaryType: 'sum', 
                              					summaryRenderer: function(value, summaryData, dataIndex) {
                              					            return floatRenderer0(value); 
                              					       }	
																			},
													 					{header: 'Media', dataIndex: 'M_TDTOCO', sortable: true, width: 60, align: 'right', renderer: floatRenderer2, summaryType: 'average', 
                              					summaryRenderer: function(value) {
                              					            return floatRenderer2(value); 
                              					       }	
																			}]},
				{ header: 'Colli produzione', columns: [{header: 'Totale', dataIndex: 'T_RDQTA', sortable: true, width: 80, align: 'right', renderer: floatRenderer0, summaryType: 'sum', 
                              					summaryRenderer: function(value, summaryData, dataIndex) {
                              					            return floatRenderer0(value); 
                              					       }	
																			},
												{header: 'Media', dataIndex: 'M_RDQTA', sortable: true, width: 60, align: 'right', renderer: floatRenderer2, summaryType: 'average', 
                              					summaryRenderer: function(value) {
                              					            return floatRenderer2(value); 
                              					       }	
																			}]},																			
				{ header: 'Peso', columns: [{header: 'Totale', dataIndex: 'T_TDPLOR', sortable: true, width: 80, align: 'right', renderer: floatRenderer2, summaryType: 'sum', 
                              					summaryRenderer: function(value, summaryData, dataIndex) {
                              					            return floatRenderer2(value); 
                              					       }	
																			},
																	 {header: 'Media', dataIndex: 'M_TDPLOR', sortable: true, width: 60, align: 'right', renderer: floatRenderer2, summaryType: 'average', 
                              					summaryRenderer: function(value) {
                              					            return floatRenderer2(value); 
                              					       }	
																			}]},
				{ header: 'Importo', columns: [{header: 'Totale', dataIndex: 'T_TDTIMP', sortable: true, width: 110, align: 'right', renderer: floatRenderer2, summaryType: 'sum', 
                              					summaryRenderer: function(value, summaryData, dataIndex) {
                              					            return floatRenderer2(value); 
                              					       }	
																			},
														 					{header: 'Media', dataIndex: 'M_TDTIMP', sortable: true, width: 60, align: 'right', renderer: floatRenderer2, summaryType: 'average', 
                              					summaryRenderer: function(value) {
                              					            return floatRenderer2(value); 
                              					       }	
																			}]},
    		],						
			},{
					
						xtype: 'form',
						layout: {
                type: 'vbox',
                align: 'stretch'
            },
						flex: 0.7,
						title: 'Filtri',
						items: [{															
								xtype: 'fieldset',
								title: 'Raggruppamento',
								layout: 'fit',
								items: [{
										xtype: 'radiogroup',
										anchor: '100%',								
										fieldLabel: '',
										items: [{
												xtype: 'radio' 
							  				, name: 'tipo_area'  
							  				, boxLabel: 'Itineraio'
							  				, inputValue: 'TAKEY1'
							  				, checked: true							  
										},{
										 	 	xtype: 'radio' 
							  	 			, name: 'tipo_area'
							  	 			, boxLabel: 'Nazione'
							  	 			, inputValue: 'TANAZI'                           												
										},{
										 	 	xtype: 'radio' 
							  	 			, name: 'tipo_area'
							  	 			, boxLabel: 'Agente'
							  	 			, inputValue: 'TDCAG1'                           												
										},{
										 	 	xtype: 'radio' 
							  	 			, name: 'tipo_area'
							  	 			, boxLabel: 'Variante'
							  	 			, inputValue: 'TDDVN1'                           												
										}]																		
								}]
						},{
								xtype: 'fieldset',
								title: '',
								layout: 'vbox',
								border: false,
								items: [{				 
										name: 'f_area_spedizione',
										xtype: 'combo',
										fieldLabel: 'Area Spedizione',
										displayField: 'text',
										labelAlign:'left',
										width: '100%',
										valueField: 'id',
										emptyText: '- seleziona -',
										allowBlank: true,														
										store: {
												autoLoad: true,
												editable: false,
												autoDestroy: true,	 
												fields: [{name:'id'}, {name:'text'}],
												data: [ 
																<?php echo acs_ar_to_select_json($s->get_options_area_spedizione('Y'), '', true, 'Y') ?>
															] 
										}			 
								},{						 
										name: 'f_divisione',
										xtype: 'combo',
										fieldLabel: 'Divisione',
										displayField: 'text',
										labelAlign:'left',
										width: '100%',
										valueField: 'id',
										emptyText: '- seleziona -',
										allowBlank: true,														
										store: {
												autoLoad: true,
												editable: false,
												autoDestroy: true,	 
												fields: [{name:'id'}, {name:'text'}],
												data: [
															 <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '', true, 'Y') ?> 
															] 
										}			 
								},{
									 name: 'f_tipologia_ordine',
									 xtype: 'combo',
									 fieldLabel: 'Tipologia ordine',
									 displayField: 'text',
									 labelAlign:'left',
									 width: '100%',
									 valueField: 'id',
									 emptyText: '- seleziona -',
									 forceSelection:true,
									 allowBlank: true,
									 multiSelect: true,														
									 store: {
												autoLoad: true,
												editable: false,
												autoDestroy: true,	 
												fields: [{name:'id'}, {name:'text'}],
												data: [ 
															 <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?>
															] 
										}					 
								},{
									 name: 'f_tipo_ordine',
									 xtype: 'combo',
									 fieldLabel: 'Tipo ordine',
									 displayField: 'text',
									 labelAlign:'left',
									 width: '100%',
									 valueField: 'id',
									 emptyText: '- seleziona -',
									 forceSelection:true,
									 allowBlank: true,
							 		 multiSelect: true,														
									 store: {
									 		autoLoad: true,
											editable: false,
											autoDestroy: true,	 
											fields: [{name:'id'}, {name:'text'}],
											data: [
														 <?php echo acs_ar_to_select_json($s->options_select_tipi_ordine(), '') ?>
														] 
									}							 
								},{
									 name: 'f_agente',
									 xtype: 'combo',
									 fieldLabel: 'Agente',
									 displayField: 'text',
									 labelAlign:'left',
									 width: '100%',
									 valueField: 'id',
									 emptyText: '- seleziona -',
									 forceSelection:true,
									 allowBlank: true,
							 		 multiSelect: true,														
									 store: {
									 		autoLoad: true,
											editable: false,
											autoDestroy: true,	 
											fields: [{name:'id'}, {name:'text'}],
											data: [
														 <?php echo acs_ar_to_select_json($s->get_options_agenti('Y'), '') ?>
														] 
									}							 
								},{
									 name: 'f_stato_ordine',
									 xtype: 'combo',
									 fieldLabel: 'Stato ordine',
									 displayField: 'text',
									 labelAlign:'left',
									 width: '100%',
									 valueField: 'id',
									 emptyText: '- seleziona -',
									 forceSelection:true,
									 allowBlank: true,
							 		 multiSelect: true,														
									 store: {
									 		autoLoad: true,
											editable: false,
											autoDestroy: true,	 
											fields: [{name:'id'}, {name:'text'}],
											data: [
														 <?php echo acs_ar_to_select_json($s->get_options_stati_ordine('Y'), '') ?>
											] 
									}							 
								}]				
				}],
				buttons: [{
						scale: 'large',
						iconCls: 'icon-button_black_repeat-32',				
						text: 'Reimposta',
						handler: function() {
								var form = this.up('form').getForm();
                form.reset();
						}
						},{
						scale: 'large',
						iconCls: 'icon-button_black_play-32',
					    text: 'Applica',
						handler: function() {
								var grid = this.up('window').down('grid');
								var form = this.up('form').getForm();
								grid.store.proxy.extraParams = {form_values: Ext.encode(form.getValues())};							
                grid.store.load();										 
						}
						}]
			}],
			listeners: {
					afterrender: function(comp){
								comp.up('window').maximize();			 
					}			 					 
			}			
}]
}