<?php

require_once "../../config.inc.php";
require_once "acs_panel_todolist_data.php";

$s = new Spedizioni();

//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($s->get_cod_mod());
$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// ASSEGNA PUNTO VENDITA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_assegna_pven'){
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'INS_PVEN',
            "k_ordine" => $m_params->k_ordine,
            "vals" => array(
                "RICITI" 	=> $m_params->data_pven->cod,
                
            ),
        )
        );
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'ASS_BCCO_BTC',
            "k_ordine" => $m_params->k_ordine,
            "vals" => array(
                "RICLIE" 	=> $m_params->cliente,
                "RICITI" 	=> $m_params->data_pven->cod,
                
            ),
        )
        );
    
    $sql = "SELECT ASFLRI FROM {$cfg_mod_Spedizioni['file_assegna_ord']} WHERE ASIDPR = ?";
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt, array($m_params->prog));
    $r = db2_fetch_assoc($stmt);
    
    
    $ret = array();
    $ret['ASFLRI'] = $r['ASFLRI'];
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();
	
	$sql_select = "";
	$sql_join   = "";
	$sql_where  = "";
	if (isset($m_params->grid_proxy_params)){
	    $params = $m_params->grid_proxy_params;
	    $where = "";
	    if(isset($params->f_cls)){
            $sql_join   .= "LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                           ON TD.TDDT = GC.GCDT AND TD.TDCCON = GC.GCCDCF";
            $sql_select .= ", GCSEL1, GCSEL2, GCSEL3, GCSEL4";
        }  
           
        
        if(isset($params->f_avanz)){
            $sql_join .= " LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_cronologia_ordine']} PQ
            ON PQ.PQDT=TD.TDDT AND PQ.PQTIDO=TD.TDOTID AND PQ.PQINUM=TD.TDOINU
            AND PQ.PQAADO=TD.TDOADO AND PQ.PQNRDO=TD.TDONDO AND PQ.PQFUNZ = '{$params->f_avanz}'";
            
            $sql_select .= ", PQDTGE";
        }
	}
	

	

	$sql = "SELECT AO.*, TD.*, TA_ITIN.TADESC AS D_ITIN {$sql_select}
			FROM {$cfg_mod_Spedizioni['file_assegna_ord']} AO
			INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
				ON TD.TDDOCU = AO.ASDOCU			
			" . $s->add_riservatezza() . "
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL
				ON TD.TDDT = CAL.CSDT AND TD.TDDTEP = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'				  
			{$sql_join}
            WHERE " . $s->get_where_std(null, array('filtra_su' => 'TD')) . " 
			  AND ASFLRI <> 'Y' ";
	
	
	$ar_sql = array();	
	
	$sql_where .= " AND ASCAAS = ? ";
	$ar_sql[] = $m_params->causale;
	
	if (strlen(trim($m_params->tree_rec_id)) > 0) {
		$filtro = imposta_filtri(explode("|", $m_params->tree_rec_id));		
		$sql_where.= sql_where_by_combo_value('ASCASS', $filtro['ASCASS']);
		$sql_where.= sql_where_by_combo_value('ASUSAT', $filtro['ASUSAT']);
		$sql_where.= sql_where_by_combo_value('TA_ITIN.TAASPE', $filtro['TAASPE']);
		$sql_where.= sql_where_by_combo_value('TD.TDCOCL', $filtro['TDCOCL']);
		$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $filtro['TDSTAT']);
		
		//TODO: finire anche le altre chiavi

	} else {

		if (strlen(trim($m_params->stato)) > 0) {
			$sql_where .= " AND TDSTAT = ? ";
			$ar_sql[] = trim($m_params->stato);
		}
		
	}
	
	
	
	
	//da filtri fatti su todo
	if (isset($m_params->grid_proxy_params)){
		$where = "";
		$copy_m_params = $m_params;
		$m_params = $m_params->grid_proxy_params;
		global $cfg_mod_Spedizioni;
		if (isset ($m_params->f_cliente_cod) && count($m_params->f_cliente_cod) > 0)
			if ($cfg_mod_Spedizioni['disabilita_sprintf_codice_cliente'] == 'Y')
				$where .= " AND TD.TDCCON = " . sql_t($m_params->f_cliente_cod);
			else
				$where .= " AND TD.TDCCON = " . sql_t(sprintf("%09s", $m_params->f_cliente_cod));
			
		$where .= sql_where_by_combo_value('TD.TDCITI', $m_params->f_itinerario);
		$where .= sql_where_by_combo_value('TD.TDCLOR', $m_params->f_tipologia_ordine);
		$where .= sql_where_by_combo_value('TD.TDCAUT', $m_params->f_causale_trasporto);
		$where .= sql_where_by_combo_value('TD.TDASPE', $m_params->f_area);
		if (strlen($m_params->f_data_prod) > 0)
			$where .= " AND TD.TDDTEP = " . $m_params->f_data_prod;
		if (strlen($m_params->f_assegnazione_da_data) > 0)
			$where .= " AND ASDTAS >= " . $m_params->f_assegnazione_da_data;
		if (strlen($m_params->f_assegnazione_da_ora) > 0)
			$where .= " AND ASHMAS >= " . $m_params->f_assegnazione_da_ora;
		if (strlen($m_params->f_assegnazione_a_data) > 0)
			$where .= " AND ASDTAS <= " . $m_params->f_assegnazione_a_data;
		
		//data/ora scadenza
		if (strlen($m_params->f_scadenza_da_data) > 0)
			$where .= " AND ASDTSC >= " . $m_params->f_scadenza_da_data;
		if (strlen($m_params->f_scadenza_a_data) > 0)
			$where .= " AND ASDTSC <= " . $m_params->f_scadenza_a_data;
						
			if (isset($m_params->f_stadio) && count($m_params->f_stadio) > 0){
			if (is_array($m_params->f_stadio))
				$where .= " AND CAL.CSFG01 IN (" . sql_t_IN($m_params->f_stadio) . ") ";
			else
				$where .= " AND CAL.CSFG01 = " . sql_t($m_params->f_stadio);
		}
				
				
		$sql_where .= $where;
		$m_params = $copy_m_params;		
	}	

	$sql = $sql . $sql_where;
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_sql);

	//per conteggio righe
	$sql_rd = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
		WHERE RD.RDDT = ?
		  AND RD.RDTIDO = ?
		  AND RD.RDINUM = ?
		  AND RD.RDAADO = ?
		  AND RD.RDNRDO = ?
		  AND RD.RDTISR = '' AND RD.RDSRIG = 0
	";
	$stmt_rd = db2_prepare($conn, $sql_rd);
	echo db2_stmt_errormsg();
	

	while ($row = db2_fetch_assoc($stmt)) {
			$nr = array();
				$nr['ASCAAS'] 		= acs_u8e(trim($row['ASCAAS']));
				$nr['prog'] 		= $row['ASIDPR'];
				$nr['D_ITIN'] 		= acs_u8e(trim($row['D_ITIN']));
								
				$oe = $s->k_ordine_td_decode_xx($row['TDDOCU']);
				$result = db2_execute($stmt_rd, $oe);
				$count_rd_row = db2_fetch_assoc($stmt_rd);
				$nr['C_ROW'] 		= $count_rd_row['C_ROW'];
				
				$nr['TDCCON'] 		= acs_u8e(trim($row['TDCCON']));
				$nr['TDDAG1'] 		= acs_u8e(trim($row['TDDAG1']));
				$nr['TDODRE']       = $row['TDODRE'];
				$nr['cons_rich']    = $row['TDODER'];
				$nr['max_atp']      = $row['TDAUX2'];
				if(isset($params->f_cls) && $params->f_cls == '1') 
				    $nr['classe']      =  $row['GCSEL1'];
			    if(isset($params->f_cls) && $params->f_cls == '2')
			        $nr['classe']      =  $row['GCSEL2'];
		        if(isset($params->f_cls) && $params->f_cls == '3')
		            $nr['classe']      =  $row['GCSEL3'];
	            if(isset($params->f_cls) && $params->f_cls == '4')
	                $nr['classe']      =  $row['GCSEL4'];
	            
                $nr['note']  = $s->has_commento_ordine_by_k_ordine($row['TDDOCU']);
          
				$nr["priorita"]		=  $row['TDOPRI'];
				$nr["qtip_priorita"]=  acs_u8e($row['TDDPRI']);
				$nr["tp_pri"]		=  $s->get_tp_pri(trim($row['TDOPRI']), $row);
				
				$nr['TDDCON'] 		= acs_u8e(trim($row['TDDCON']));
				$nr['localita'] 	= acs_u8e(trim($s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD'])));
				$nr['TDVSRF']		= acs_u8e(trim($row['TDVSRF']));
				$nr['k_ordine']		= trim($row['TDDOCU']);
				$nr['ordine_out']	= $s->k_ordine_out($row['TDDOCU'])."_".$row['TDOTPD'];
				$nr['indice_modifica']	= trim($row['TDMODI']);

				$nr['ord_orig']		= trim($row['TDASOR']);
				
				if (strlen(trim($nr['ord_orig'])) > 0) {
					$ord_exp_orig_tmp	= explode("_", $nr['ord_orig']);
					$ord_exp_orig 		= explode("_", $nr['k_ordine']);
					//$ord_exp_orig[2]	= $ord_exp_orig_tmp[0];
					$ord_exp_orig[3]	= $ord_exp_orig_tmp[0];
					$ord_exp_orig[4]	= $ord_exp_orig_tmp[1];
					$nr['k_ordine_orig']= implode("_", $ord_exp_orig);
				}
				
				$nr['data_ord_orig']= trim($row['TDDAOR']);
				$nr['fat_ord_orig']	= trim($row['TDASFR']);
				$nr['data_fat_ord_orig']	= trim($row['TDDAFR']);
				$nr['tipoOrd']		= trim($row['TDOTPD']);
				$nr['fl_cli_bloc']	= trim($row['TDFN02']);
				$nr['fl_evaso']		= trim($row['TDFN11']);
				$nr['fl_bloc']		= $s->get_ord_fl_bloc($row);
				$nr['cons_prog']	= $row['TDDTEP'];
				$nr['PQDTGE']	= $row['PQDTGE'];
				$nr['colli_sped'] 	= $row['TDCOSP'];
				$nr['proforma'] 	= trim($row['TDPROF']);
				$nr['stato'] 		= trim($row['TDSTAT']);				
				$nr['raggr'] 		= trim($row['TDCLOR']);
				$nr['f_ril'] =  trim($row['ASFLNR']);
				$nr['var1']			= acs_u8e(trim($row['TDDVN1']));
				$nr['k_carico']		= $s->k_carico_td($row);
				$nr['liv']			= 'liv_2'; //serve per assegna carico
				$nr['fl_cons_conf'] = $row['TDFN06'];
				if ((int)$row['TDNRCA'] > 0)
					$nr['carico'] 		= implode("_", array($row['TDAACA'], trim($row['TDNRCA'])));
				$nr['indice_rottura'] = trim($row['TDIRLO']);								
				$nr['fl_art_manc'] = $s->get_fl_art_manc($row);
				$nr['art_da_prog'] = $s->get_art_da_prog($row);
				$nr['causale']	   = trim($row['TDCAUT']);
				
				
				if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){				
					$nr['cod_articolo'] = trim(acs_u8e($row['RDART']));
					$nr['des_articolo'] = trim(acs_u8e($row['RDDES1']));
					$nr['um'] 			= trim(acs_u8e($row['RDUM']));
					$nr['qta'] 			= trim(acs_u8e($row['RDQTA']));
				}
				
				$data[] = $nr;
	}
	
	$ret = array();
	$ret['success'] = true;
	$ret['root'] = $data;
	$ret['totalCount'] = count($data);
	echo acs_je($ret);
exit;
}




?>
{"success": true, "items":
	{
		xtype: 'gridpanel',
		multiSelect: true,
		stateful: true,
        stateId: 'seleziona-ordini-todo<?php echo trim($m_params->causale)?>',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
        features: [
	
		{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 	type: 'boolean',
			dataIndex: 'visible'
		     }
		      ]
		}],	
		
  	    store: Ext.create('Ext.data.Store', {
													
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							type: 'ajax',
							actionMethods: {
								read: 'POST'
							},							
							reader: {
					            type: 'json',
					            root: 'root'
					        }
					        
					        , extraParams: <?php echo acs_raw_post_data(); ?>
					        
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
							, doRequest: personalizza_extraParams_to_jsonData					        
					        
						},
	        			fields: [
	        				'ASCAAS', 'rec_stato', 'prog', 'D_ITIN', 'C_ROW', 'f_ril',
	            			'TDCCON', 'TDDCON', 'fl_bloc', 'fl_cli_bloc', 'localita', 'TDVSRF', 'k_ordine', 'ordine_out', 
	            			'tipoOrd', 'cons_prog', 'fl_evaso', 'colli_sped', 'proforma', 'stato', 'raggr', 'var1', 'cod_articolo','des_articolo', 'um', 'qta',
	            			'k_carico', 'fl_bloc', 'iconCls', 'liv', 'fl_cons_conf', 'carico', 'indice_rottura', 'fl_art_manc', 
	            			'art_da_prog', 'ord_orig', 'k_ordine_orig', 'data_ord_orig', 'fat_ord_orig', 'data_fat_ord_orig', 'causale', 'indice_modifica',
	            			'TDDAG1', 'TDODRE', 'PQDTGE', 'priorita', 'qtip_priorita', 'tp_pri', 'cons_rich', 'max_atp', 'classe', 'note'
	        			]
	    			}),
	    			
		        columns: [		
		 			 <?php $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>";   ?>     
		             {
		                header   : 'Righe',
		                dataIndex: 'C_ROW', 
		                width: 30
		             }, {
						    text: '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> height=25>',
						    width: 30, tooltip: 'Clienti bloccati', dataIndex: 'fl_cli_bloc', 
					    	tdCls: 'tdAction',         			
			            	menuDisabled: true, sortable: false,            		        
							renderer: function(value, p, record){if (record.get('fl_cli_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
					  }, 
		             {
		                header   : 'Cliente',
		                dataIndex: 'TDDCON', 
		                flex: 1,
		                filter: {type: 'string'}, filterable: true
		             },			  
		             {
		                header   : 'Classe',
		                dataIndex: 'classe', 
		                width: 50,
		                filter: {type: 'string'}, filterable: true
		             },	      
		             {
		                header   : 'Localit&agrave;',
		                dataIndex: 'localita', 
		                flex: 1,
		                filter: {type: 'string'}, filterable: true
		             },
		             {
		                header   : 'Itinerario',
		                dataIndex: 'D_ITIN', 
		                flex: 1,
		                filter: {type: 'string'}, filterable: true
		             },	
		             {
		                header   : 'Agente',
		                dataIndex: 'TDDAG1', 
		                flex: 1,
		                filter: {type: 'string'}, filterable: true
		             },		        
		             {
		                header   : 'Riferimento',
		                dataIndex: 'TDVSRF', 
		                width     : 110,
		                filter: {type: 'string'}, filterable: true
		             }, {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Ordini bloccati', dataIndex: 'fl_bloc', sortable: true,    	    	     
    			    	renderer: function(value, p, record){
    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';			    				    	
    			    	}},	
    			    	
    			    	
			    	    {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', 
			    	    		dataIndex: 'art_mancanti', sortable: true,
			    	    	    tooltip: 'Ordini MTO',    	    		    
			    			    renderer: function(value, p, record){
			    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
			    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
			    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=18>';
			    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
			    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';			    	
			    			    	}},			    	
			    	    {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
			        			dataIndex: 'art_da_prog',
			    	    	    tooltip: 'Articoli mancanti MTS', sortable: true,        					    	     
			    			    renderer: function(value, p, record){
			    			    	if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
			    			    	if (record.get('art_da_prog')==2) return '<img src=<?php echo img_path("icone/48x48/warning_red.png") ?> width=18>';    			    	
			    			    	if (record.get('art_da_prog')==3) return '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=18>';
			    			    	}},    			    	
    			    	
    			    	
					 {text: '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Consegna confermata', dataIndex: 'fl_cons_conf', sortable: true,    	    	     
    			    	renderer: function(value, p, record){
    			    	if (record.get('fl_cons_conf')==1) return '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=18>';
    			    	}},    			    	
    			    	
    			    			        
		             {
		                header   : 'Ordine',
		                dataIndex: 'ordine_out', 
		                width     : 110,
		                filter: {type: 'string'}, filterable: true
		             },
	                 {
    				    text: '<?php echo $note; ?>',
    				    width: 30, 
    				    tooltip: 'Commento ordine',
    				    tdCls: 'tdAction',  
    				    dataIndex : 'note',       			
    	    	        menuisabled: true, sortable: false,        		        
    					renderer: function(value, p, record){
        						if (record.get('note') == 1) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
        			    		if (record.get('note') == 0) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
        				}
    			      },
		              {
		                header   : 'Data',
		                dataIndex: 'TDODRE', 
		                width     : 80,
		                renderer: date_from_AS
		             }, {text: 'Pr', 			width: 35, dataIndex: 'priorita', tdCls: 'tpPri',
	    				renderer: function (value, metaData, record, row, col, store, gridView){

	    					if (record.get('qtip_priorita') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_priorita')) + '"';
	    				
	    					if (record.get('tp_pri') == 4)
	    						metaData.tdCls += ' tpSfondoRosa';
	
	    					if (record.get('tp_pri') == 6)
	    						metaData.tdCls += ' tpSfondoCelesteEl';					
	    					
	    					return value;
	    				    
	    	    	 }},
	    	    	 {text: 'Cons.<br>richiesta',	width: 60, dataIndex: 'cons_rich', renderer: date_from_AS},
	    	    	<?php if(strlen($m_params->grid_proxy_params->f_avanz) > 0){?>
	    	    	 {
		                header   : 'Avanz.',
		                dataIndex: 'PQDTGE', 
		                width     : 60,
		                renderer: date_from_AS
		             },
	    	    	<?php }?>
	    	    	
	    	    	 {text: 'Max<br>ATP',	width: 40, dataIndex: 'max_atp'},
	    	    	 {
		                header   : 'IR',
		                dataIndex: 'indice_modifica', 
		                width     : 40,
		                tooltip   : 'Indice di modifica',
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'Caus.',
		                dataIndex: 'causale', 
		                width     : 50,
		                filter: {type: 'string'}, filterable: true
		             },		
		              {
		                header   : 'Ordine origine',
		                dataIndex: 'ord_orig', 
		                width     : 100,
		                filter: {type: 'string'}, filterable: true
		             },
		              {
		                header   : 'Data <br>ord. orig.',
		                dataIndex: 'data_ord_orig', 
		                width     : 80,
		                renderer: date_from_AS
		             },
		              {
		                header   : 'Fattura <br>ord. orig.',
		                dataIndex: 'fat_ord_orig', 
		                width    : 100,
		                
		             }
		             ,
		              {
		                header   : 'Data fattura <br> ord. orig.',
		                dataIndex: 'data_fat_ord_orig', 
		                width     : 80,
		                renderer: date_from_AS
		             },	        
		             {
		                header   : 'Tp', tooltip: 'Tipo',
		                dataIndex: 'tipoOrd',
		                tdCls: 'tipoOrd', 
		                width     : 30,
		                filter: {type: 'string'}, filterable: true,
		                renderer: function (value, metaData, record, row, col, store, gridView){						
							metaData.tdCls += ' ' + record.get('raggr');										
							return value;			    
						}
		             }, 
		             {text: 'Prod./<br>Disp.sped.',	width: 65, dataIndex: 'cons_prog', renderer: date_from_AS},
		             {text: 'St.',	width: 30, dataIndex: 'stato', tooltip: 'Stato ordine', filter: {type: 'string'}, filterable: true},
		             		             		             
		             {text: 'Variante',	width: 100, dataIndex: 'var1' 		<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo ", hidden: true" ?>},
		             {text: 'Proforma',	width: 100, dataIndex: 'proforma'	<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo ", hidden: true" ?>},
		             		        
<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             {text: 'Articolo',		width: 80, dataIndex: 'cod_articolo'},
		             {text: 'Descrizione',	width: 120, dataIndex: 'des_articolo'},
		             {text: 'UM',	width: 40, dataIndex: 'um'},
		             {text: 'Q.t&agrave;',	width: 80, dataIndex: 'qta', renderer: floatRenderer0, align: 'right'},
<?php }?>		             		        
		             		            
		             {text: 'Carico',	width: 90, dataIndex: 'carico'},
		             {text: 'Ind.<br/>Rottura',	width: 90, dataIndex: 'indice_rottura'},
		         ]	    					
		
		
		, listeners: {	
		
		
		
<?php
$desc_causale = desc_TA_std('ATTAV', $m_params->causale);
?>		
		
          afterrender: function (comp) {
                  	comp.store.on('load', function(store, records, options) {
    						t_rows = comp.store.totalCount;	
    						l_win = comp.up('window');
    					    l_win.setTitle(comp.up('window').title +  ' [' + t_rows + '] - ToDo List - <?php echo $desc_causale ?>');
        										
    					}, comp);
						
	 			},		 			
	 			
	 			
	 	  celldblclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
 	  			 
 	  				var col_name = iView.getGridColumns()[iColIdx].dataIndex;
					var rec = iView.getRecord(iRowEl);
					var grid = this;
					iEvent.stopEvent();
 	  			
 	  		          if(col_name == 'note'){
	        		
			        		  var my_listeners = {
			        			
			        					afterOkSave: function(from_win, jsonData){
			        					    grid.getStore().load();
 											from_win.close();
 											
							        	}
									}
									
		            		acs_show_win_std('Annotazioni ordine ' + rec.get('ordine_out'), 'acs_form_json_annotazioni_ordine.php', {k_ordine : rec.get('k_ordine')}, 800, 300, my_listeners, 'icon-comment_edit-16');	 
		            		
	            		}else{
	            			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest_assistenza.php', {
        	        		k_ordine: rec.get('k_ordine'),
        	        		k_ordine_orig: rec.get('k_ordine_orig'),
        	        		tipo: rec.get('tipoOrd'),
        	        		prog: rec.get('prog'),
        	        		from_win: iView.up('window').id,
        	        		causale: <?php echo j($m_params->causale) ?>
        	        	}, 900, 450, null, 'icon-folder_search-16');     
	            	
		            		
		            }
 	  			
	             			 	  
	 	  },	
	 			
		   itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
		  		
			  	id_selected = grid.getSelectionModel().getSelection();
			  	list_selected_id = [];
			  	for (var i=0; i<id_selected.length; i++) 
				   list_selected_id.push({
				   		k_ordine: id_selected[i].get('k_ordine'), 
				   		prog: id_selected[i].get('prog'),
				   		f_ril: id_selected[i].get('f_ril')
				   	});
		  														  
				  var voci_menu = [];
				  
				  voci_menu.push({
					text: 'Dettaglio ordine',
					iconCls : 'icon-leaf-16',	          		
					handler: function() {
	        						
											
	    			  	    acs_show_win_std('Dettaglio ordini', 'acs_win_dettaglio_ordine.php?fn=open_win', 
	    			  		{k_ordine : rec.get('k_ordine')}, 400, 550, null, 'icon-leaf-16');
	        		}
	    		});
				  
					  if (rec.get('k_ordine').length > 0 && rec.get('rec_stato')!='Y') //sono nel livello ordine
					  voci_menu.push({
			      		text: 'Avanzamento/Rilascio attivit&agrave;',
			    		iconCls: 'icon-arrivi-16',
			    		handler: function() {

				    		//verifico che abbia selezionato solo righe a livello ordine 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('k_ordine').length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  }
							  }
  	
				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('rec_stato') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: 'acs_panel_todolist.php?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							        	list_selected_id: list_selected_id, grid_id: grid.id,
							        	cliente: rec.get('TDCCON'),
							        	causale: <?php echo j($m_params->causale)?>
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
			    		}
					  });		
					  
					

				  
				  
				  
<?php
#TODO: questo puo' andare bene anche per SBLOC: unificare
$causali_rilascio = $s->find_TA_std('RILAV', null, 'N', 'Y', null,  null, null, 'P'); //recupero tutte le RILAV
foreach($causali_rilascio as $ca) {
?>	
			
				  
	if (	rec.get('k_ordine').length > 0 &&
		 	rec.get('rec_stato')!='Y' &&
			rec.get('ASCAAS') == <?php echo j(trim($ca['id'])); ?>		 	
		 ) //sono nel livello ordine e nella causale giusta					  
	voci_menu.push({
	      		text: <?php echo j(utf8_decode($ca['text'])); ?>,
	    		iconCls: 'iconAccept',
	    		handler: function() {
	    		
	    		
				    		//verifico che abbia selezionato solo righe a livello ordine 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('k_ordine').length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  }
							  }
  	
				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('rec_stato') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: 'acs_panel_todolist.php?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							           			list_selected_id: list_selected_id, grid_id: grid.id,
							           			causale: <?php echo j($m_params->causale)?>,
							           			cliente: rec.get('TDCCON'),
												auto_set_causale: <?php echo j(trim($ca['TAKEY2'])); ?>							        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
	    
						    
		
			    }
			  });
  <?php } ?>				  
				  
				  
				  
				  
<?php if ($m_params->causale == 'ASSCAU'){ ?>				  
		  		  voci_menu.push({
		         		text: 'Visualizza righe',
		        		iconCls : 'icon-folder_search-16',          		
		        		handler: function() {
		        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest_assistenza.php', {
		        				k_ordine: rec.get('k_ordine'),
		        				k_ordine_orig: rec.get('k_ordine_orig'),
		        				tipo: rec.get('tipoOrd'),
		        				prog: rec.get('prog'),
		        				from_win: grid.up('window').id,
		        				causale: <?php echo j($m_params->causale) ?>
		        			}, 900, 450, null, 'icon-folder_search-16');          		
		        		}
		    		});					  
				  
		/*
		  		  voci_menu.push({
		         		text: 'Visualizza righe origine',
		        		iconCls : 'icon-folder_search-16',          		
		        		handler: function() {
		        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest_assistenza.php', {
		        				k_ordine: rec.get('k_ordine_orig'),
		        				k_ordine_assistenza: rec.get('k_ordine'),
		        				cliente: rec.get('TDCCON'),
		        				denom: rec.get('TDDCON'),
		        				prog: rec.get('prog'),
		        				from_win_seleziona_ordini: grid.up('window').id,
		        				tipo: 'origine'
		        			}, 900, 450, null, 'icon-folder_search-16');          		
		        		}
		    		});
		*/    		
<?php } ?>

				  <?php if ($m_params->causale == 'ASSBCCO'){ ?>
					     
				  
				  voci_menu.push({
		         		text: 'Visualizza condizioni commerciali',
		        		iconCls : 'icon-folder_search-16',          		
		        		handler: function() {
            				  if(list_selected_id.length > 1){
            				  	acs_show_msg_error('Operazione non ammessa su pi� ordini');
            				  	return;
            				  }
            				 
            				    var my_listeners = {
                        	  			afterAssegna: function(from_win, flag){
                        	  			  rec.set('rec_stato', flag);  
                        	  			  from_win.close();
                        	        	   }
                        				};
		        		
		        			acs_show_win_std('Condizioni commerciali [' +rec.get('ordine_out') + '] '+rec.get('TDDCON') + ' ['+rec.get('TDCCON')+']', 
		        			   'acs_condizioni_commerciali.php?fn=open_grid', {
		        				k_ordine: rec.get('k_ordine'),
		        				cliente: rec.get('TDCCON'),
		        				denom: rec.get('TDDCON'),
		        				prog: rec.get('prog'),
		        				from_win_seleziona_ordini: grid.up('window').id,
		        			
		        			}, 900, 450, my_listeners, 'icon-folder_search-16');          		
		        		}
		    		});
				  
				  <?php }?>
				  
				    <?php if ($m_params->causale == 'ASSPVEN'){ ?>
					     
				  
				  voci_menu.push({
		         		text: 'Assegna punto vendita',
		        		iconCls : 'icon-folder_search-16',          		
		        		handler: function() {
            				  if(list_selected_id.length > 1){
            				  	acs_show_msg_error('Operazione non ammessa su pi� ordini');
            				  	return;
            				  }
            				  
            				  var my_listeners = {
                	  			afterSelected: function(from_win, data_pven){
                	  			 
                	  			  Ext.Ajax.request({
        					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_pven',
        					        jsonData: {
        					           		k_ordine: rec.get('k_ordine'),
        					           		data_pven : data_pven,
        					           		cliente: rec.get('TDCCON'),
        					           		prog: rec.get('prog'),					        
        					        },
        					        method     : 'POST',
        					        waitMsg    : 'Data loading',
        					        success : function(result, request){
        					            var jsonData = Ext.decode(result.responseText);
        					           // acs_show_msg_info('Fine operazione');
        					            rec.set('rec_stato', jsonData.ASFLRI);  
        					            from_win.destroy();				            
        					        },
        					        failure    : function(result, request){
        					            Ext.Msg.alert('Message', 'No data to be loaded');
        					        }
        					    });
                	  			  
                	        	   }
                				};
            				  
            			acs_show_win_std('Seleziona punto vendita', 
	 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
							{cliente : rec.get('TDCCON'), type : 'P'
							 }, 800, 300, my_listeners, 'icon-sub_blue_add-16');
            				  
            				  
            				           		
		        		}
		    		});
				  
				  <?php }?>
				  
				
				
			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				}).showAt(event.xy);				
			  }	
		 			
	 			
	 			
	        , itemclick: function(view,rec,item,index,eventObj) {
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');  
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');      	

	        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdi.store.load();	        	
	  			wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdc.store.load();
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				      //wd.setSource(src.dettagli);
				   }
				});
				
				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}
																        	
	         }	 			
	 			
	 			 			
			}
			
		, viewConfig: {
		        getRowClass: function(record, index) {			        			           
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }		         
		    }
		    
		
		, dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-print-32',
                     text: 'Stampa',
			            handler: function() {
	        				Ext.ux.grid.Printer.print(this.up('grid'));
			            }
			     }, {
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-button_black_repeat_dx-32',
                     text: 'Refresh',
			            handler: function() {
	        				this.up('grid').store.load();
			            }
			     },  '->'
			     
			   /*  <?php if ($m_params->causale == 'ASSCAU'){ ?>
			     
			     , {
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-button_black_repeat_dx-32',
                     text: 'Fine attivit&agrave;',
			            handler: function() {
	        				this.up('grid').store.load();
			            }
			     }
			    <?php } ?> */
			     
			     ]
		   }]


	}
}


