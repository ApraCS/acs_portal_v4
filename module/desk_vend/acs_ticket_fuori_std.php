<?php
require_once "../../config.inc.php";

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    
    $ar_ins['TKDTGE'] = oggi_AS_date();
    $ar_ins['TKORGE'] = oggi_AS_time();
    $ar_ins['TKUSGE'] = substr($auth->get_user(), 0, 8);
    $ar_ins['TKDTUM'] = oggi_AS_date();
    $ar_ins['TKORUM'] = oggi_AS_time();
    $ar_ins['TKUSUM'] = substr($auth->get_user(), 0, 8);
    $ar_ins["TKDT"]   = $id_ditta_default;
    
    $ar_ins['TKTICK']  = $form_values->TKTICK;
    $ar_ins['TKVALU']  = $form_values->valuta;
    $ar_ins['TKNOTE']  = $form_values->TKNOTE;
    $ar_ins['TKFORN']  = $form_values->TKFORN;
    
    if(strlen($form_values->TKPLOR) > 0)
        $ar_ins['TKPLOR']   = sql_f($form_values->TKPLOR);
    if(strlen($form_values->TKPNET) > 0)
        $ar_ins['TKPNET']   = sql_f($form_values->TKPNET);
    if(strlen($form_values->TKVOLU) > 0)
        $ar_ins['TKVOLU']   = sql_f($form_values->TKVOLU);
    if(strlen($form_values->TKCOLL) > 0)
        $ar_ins['TKCOLL']   = sql_f($form_values->TKCOLL);
    
    if(strlen($form_values->TKPRZL) > 0)
        $ar_ins['TKPRZL']   = sql_f($form_values->TKPRZL);
    if(strlen($form_values->TKLTFO) > 0)
        $ar_ins['TKLTFO']   = sql_f($form_values->TKLTFO);
        
     
    $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_ticket_no_std']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ret = array();
    $form_values = $m_params->form_values;
   
    $ar_ins['TKDTUM'] = oggi_AS_date();
    $ar_ins['TKORUM'] = oggi_AS_time();
    $ar_ins['TKUSUM'] = substr($auth->get_user(), 0, 8);
    
    
    $ar_ins['TKTICK']  = $form_values->TKTICK;
    $ar_ins['TKVALU']  = $form_values->valuta;
    $ar_ins['TKNOTE']  = $form_values->TKNOTE;
    $ar_ins['TKFORN']  = $form_values->TKFORN;
    
    if(strlen($form_values->TKPLOR) > 0)
        $ar_ins['TKPLOR']   = sql_f($form_values->TKPLOR);
    if(strlen($form_values->TKPNET) > 0)
        $ar_ins['TKPNET']   = sql_f($form_values->TKPNET);
    if(strlen($form_values->TKVOLU) > 0)
        $ar_ins['TKVOLU']   = sql_f($form_values->TKVOLU);
    if(strlen($form_values->TKCOLL) > 0)
        $ar_ins['TKCOLL']   = sql_f($form_values->TKCOLL);
                    
    if(strlen($form_values->TKPRZL) > 0)
        $ar_ins['TKPRZL']   = sql_f($form_values->TKPRZL);
    if(strlen($form_values->TKLTFO) > 0)
        $ar_ins['TKLTFO']   = sql_f($form_values->TKLTFO);
                        
                
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_ticket_no_std']} TK
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(TK) = '{$form_values->rrn}'";
    
   
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_delete'){
    
    $m_params = acs_m_params_json_decode();
    
    $rrn= $m_params->form_values->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_ticket_no_std']} TK WHERE RRN(TK) = '{$rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
    
    if($m_params->open_request->ticket != '')
        $sql_where = " AND TKTICK = '{$m_params->open_request->ticket}'";
   
    $sql = " SELECT RRN(TK) AS RRN, TK.*, RD.C_ROW AS T_RIGHE, CF_FORNITORE.CFRGS1 AS D_FORNITORE
             FROM {$cfg_mod_Spedizioni['file_ticket_no_std']} TK
             LEFT OUTER JOIN (
                 SELECT COUNT(*) AS C_ROW, RDDT, K00001
                 FROM {$cfg_mod_Spedizioni['file_righe_ticket_no_std']} 
                 WHERE RDDT = '{$id_ditta_default}'
                 GROUP BY RDDT, K00001) RD
             ON RD.RDDT = TK.TKDT AND TK.TKTICK = RD.K00001
             LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
              ON CF_FORNITORE.CFDT = TK.TKDT AND CF_FORNITORE.CFCD = TK.TKFORN AND CFTICF = 'F'
             WHERE TKDT = '{$id_ditta_default}' {$sql_where}";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
   
    while($row = db2_fetch_assoc($stmt)){
        
        $nr = array();
        $nr = array_map('rtrim', $row);
        $nr['rrn'] = $row['RRN'];
        $nr['t_righe'] = $row['T_RIGHE'];
        $nr['valuta']  = trim($row['TKVALU']);
        $val_valuta   = find_TA_sys('VUVL', $nr['valuta']);
        $nr['t_valuta'] = "[".$nr['valuta']."] ".trim($val_valuta[0]['text']);
        $nr['c_forn'] = "[".$row['TKFORN']."]";
        $nr['d_forn'] = "<b>".$row['D_FORNITORE']."</b>";
        $ar[] = $nr;
        
    }
    
    echo acs_je($ar);
    exit;
}
if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
			xtype: 'panel',
			<?php 
			    if($m_params->hide_grid != 'Y'){?>
				title: 'Ticket FS',
        		<?php echo make_tab_closable(); ?>,
        	<?php }?>
        	layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
					{
			xtype: 'grid',
			title: '',
			flex:0.7,
			autoScroll: true,
			<?php if($m_params->hide_grid == 'Y'){?>	
				hidden : true,
		    <?php } ?>
	        loadMask: true,
	        stateful: true,
	        stateId: 'seleziona-sconti-age',
	        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],	
	        store: {
			//xtype: 'store',
			autoLoad:true,

					proxy: {
					   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
					   method: 'POST',								
					   type: 'ajax',

				       actionMethods: {
				          read: 'POST'
				        },
				        
				        
				           extraParams: {
							 open_request: <?php echo acs_je($m_params) ?>
        				},
        				
        				doRequest: personalizza_extraParams_to_jsonData, 
			
					   reader: {
			            type: 'json',
						method: 'POST',						            
			            root: 'root'						            
			        }
				},
				
    			fields: [ 'rrn', 'listino', 'valuta', 'TKDTGE', 'TKDTUM', 'TKUSGE', 'TKUSUM', 't_righe',
    			          'TKTICK', 'TKPLOR', 'TKPNET', 'TKVOLU', 'TKCOLL', 'TKLTFO', 'TKPRZL', 'TKNOTE', 'TKFORN', 'c_forn', 'd_forn'
                        ]							
						
			}, //store
			<?php $search = "<img src=" . img_path("icone/48x48/search.png") . " height=20>"; ?>
			
			      columns: [	
			        {
	                header   : 'Ticket',
	                dataIndex: 'TKTICK',
	                width : 100,
	                },
	                {
	                header   : 'Val.',
	                dataIndex: 'valuta',
	                width : 40,
	                },
	                {
                    header   : 'Prezzo listino',
                    dataIndex: 'TKPRZL',
                    width: 150,
                    renderer: floatRenderer5
                    },
	           
                  { header: 'Note'
                  , dataIndex: 'TKNOTE'
                  , flex : 1
                  , filter: {type: 'string'}, filterable: true
                  
                  },
                  { header: 'LT'
                  , dataIndex: 'TKLTFO'
                  , tooltip : 'Lead time'
                  , width : 30
                  , filter: {type: 'string'}, filterable: true
                  
                  },
                   { header: '<?php echo $search ?>'
                  , dataIndex: 't_righe'
                  , width : 30
                  , filter: {type: 'string'}, filterable: true
                  , renderer: function(value, metaData, record){
                         if(record.get('t_righe') > 0){
		    			   return '<img src=<?php echo img_path("icone/48x48/search.png") ?> width=15>';
		    		 	}
		    			       
		    		  }
                  },
                {header: 'Immissione',
            	 columns: [
            	 {header: 'Data', dataIndex: 'TKDTGE', renderer: date_from_AS, width: 70, sortable : true,
            	 renderer: function(value, metaData, record){
            	         if (record.get('TKDTGE') != record.get('TKDTUM')) 
            	          metaData.tdCls += ' grassetto';
            	 
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TKDTUM')) + ', Utente ' +record.get('TKUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return date_from_AS(value);	
            	}
            	 
            	 }
            	 ,{header: 'Utente', dataIndex: 'TKUSGE', width: 70, sortable : true,
            	  renderer: function(value, metaData, record){
            	  
            	  		if (record.get('TKUSGE') != record.get('TKUSUM')) 
            	          metaData.tdCls += ' grassetto';
            	  
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TKDTUM')) + ', Utente ' +record.get('TKUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return value;	
            	} 
            	 }
            	 ]}
                 	 
	                     
	                
	                
	         ], listeners: {
	         
	         	 <?php if($m_params->autoload == 'firstRec'){?>
		       		  	 	 'afterrender': function(comp){
		       		  	 	    var win = comp.up('window');
								comp.store.on('load', function(store, records, options) {
								 var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().setValues(records[0].data);
								}, comp);
    						},
				 <?php }?>
	         
	         selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('panel').down('#dx_form');
		               form_dx.getForm().reset();
		               rec_index = selected[0].index;
		               form_dx.getForm().findField('rec_index').setValue(rec_index);
	                   acs_form_setValues(form_dx, selected[0].data);
	                   
	                   }
		          },
		          
		          celldblclick: {								

					fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.preventDefault();
					  	
						if (col_name == 't_righe' && rec.get('t_righe') > 0){
							
							 acs_show_win_std(null, 
						     'acs_ticket_fuori_std_righe.php?fn=open_elenco',
						     { ticket : rec.get('TKTICK')  });
							
						}
						
					  	
				    }
				},		
		
		   	 }
				     
		
		
		},
		   {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Dettaglio ticket articoli non standard',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.25,
 		            frame: true,
 		            items: [
 		            
 		            {xtype: 'textfield', name: 'rrn', hidden : true}, 
				    {xtype: 'textfield', name: 'rec_index', hidden : true},
				    {xtype: 'textfield', name: 'TKTICK', fieldLabel: 'ID Ticket', maxLength : 10, width : 200, allowBlank : false},
				    
				      {
                    xtype : 'numberfield',
            	 	name: 'TKPLOR', 
                    fieldLabel: 'Peso lordo',
                    hideTrigger:true,
                    decimalPrecision : 4,
                    keyNavEnabled : false,
            		mouseWheelEnabled : false,
            		width : 200	
                  },
                    {
                    xtype : 'numberfield',
            	 	name: 'TKPNET', 
                    fieldLabel: 'Peso netto',
                    hideTrigger:true,
                    decimalPrecision : 4,
                    keyNavEnabled : false,
            		mouseWheelEnabled : false,
            		width : 200
                  },
                    {
                    xtype : 'numberfield',
            	 	name: 'TKVOLU', 
                    fieldLabel: 'Volume',
                    hideTrigger:true,
                    decimalPrecision : 4,
                    keyNavEnabled : false,
            		mouseWheelEnabled : false,
            		width : 200
                  },
                    {
                    xtype : 'numberfield',
            	 	name: 'TKCOLL', 
                    fieldLabel: 'Colli unitari',
                    hideTrigger:true,
                    decimalPrecision : 4,
                    keyNavEnabled : false,
            		mouseWheelEnabled : false,
            		width : 150	
                  },
                    {
    					name: 'TKFORN',
    					xtype: 'textfield',
    					hidden : true,
    											
    				   },
    				    
    				    { xtype: 'fieldcontainer'
                          , layout:
                              {	type: 'hbox'
                              , pack: 'start'
                        	  , align: 'stretch'
                                }
                          , items: [ 
                             {
            				name: 'c_forn',
            				fieldLabel : 'Fornitore',
            				xtype: 'displayfield',
            				anchor: '-15',
            										
            			   }, 
            			   {xtype: 'component',
                                flex : 1,
                            },
                          { xtype: 'button'
                         , margin: '0 0 0 0'
                         , anchor: '-5'	
                         , scale: 'small'
                         , iconCls: 'icon-search-16'
                         , iconAlign: 'top'
                         , width: 25			
                         , handler : function() {
                               var m_form = this.up('form').getForm();
                               var my_listeners = 
                                 { afterSel: function(from_win, row) {
                                     m_form.findField('TKFORN').setValue(row.CFCD);
                                     m_form.findField('c_forn').setValue('['+row.CFCD+']');
                                     m_form.findField('d_forn').show();
                                     m_form.findField('d_forn').setValue('<b>'+row.CFRGS1+'</b>');
                                     from_win.close();
                                   }
                                 }
                             
                               acs_show_win_std('Anagrafica clienti/fornitori'
                                               , '../desk_utility/acs_anag_ricerca_cli_for.php?fn=open_tab'
                                               , {cf: 'F'}, 600, 500, my_listeners
                                               , 'icon-search-16');
                             }
                                    
                        }
                          
                          ]
                         },
				   
			            {
            				name: 'd_forn',
            				fieldLabel : '&nbsp;',
            				labelSeparator : '',
            				xtype: 'displayfield',
            				anchor: '-15'							
            			   },
                  
                   {
                    xtype : 'numberfield',
            	 	name: 'TKLTFO', 
                    fieldLabel: 'Lead time forn.',
                    hideTrigger:true,
                    decimalPrecision : 4,
                    keyNavEnabled : false,
            		mouseWheelEnabled : false,
            		width : 150
                  },
        		
              
                {
                    xtype : 'numberfield',
            	 	name: 'TKPRZL', 
                    fieldLabel: 'Prezzo listino',
                    hideTrigger:true,
                    decimalPrecision : 5,
                    keyNavEnabled : false,
            		mouseWheelEnabled : false,
            		width : 200
                  },
             		
            	 {
            		name: 'valuta',
            		xtype: 'combo',
            		fieldLabel: 'Valuta',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
               		allowBlank: false,	
               		queryMode: 'local',
            	    minChars: 1, 	
            	    anchor: '-5',	
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [								    
            		     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('VUVL'), ''); ?>	
            		    ]
            		}
            		,listeners: { 
            	 		beforequery: function (record) {
                     		record.query = new RegExp(record.query, 'i');
                     		record.forceAll = true;
            			 }
            		 }
            		
                  },
                  
                
				   
             	{xtype: 'textarea', name: 'TKNOTE', fieldLabel: 'Note', labelAlign : 'top', maxLength : 100 , height : 60, anchor: '-5'}
                			
                	 ],
			<?php if($m_params->only_view != 'Y'){?>	
			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		     
		           
			
			            }

			     }, '->',
                 {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
		  <?php }?>
			 	 }
			 ],
					 
					
					
	}
	
]}


<?php

exit;
}





