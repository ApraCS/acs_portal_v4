
<?php

require_once "../../config.inc.php";
$s = new Spedizioni();

$m_params = acs_m_params_json_decode();


//------------------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_tab'){
//------------------------------------------------------------------------------
$sped_id = $m_params->sped_id;
$sped  = $s->get_spedizione($sped_id);
?>
{"success":true, 
  m_win: {
  	title: 'Applica sequenza di scarico',
  	width: 900,
  	height: 550
  },

  items: [
  {
			xtype: 'grid',
			
			loadMask: true,
			
			stateful: true,
        	stateId: 'seq-scarichi',
        	stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '../desk_vend/acs_form_json_progetta_spedizioni.php', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							        extraParams: {
							        	fn: 'grid_data',
										sped_id: <?php echo j($sped_id )?>,
										data: <?php echo j($sped['CSDTSP']); ?>,
										itin_id: <?php echo j($sped['CSCITI']) ?>
							        },
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['k_sped_carico', 'k_cli_des', 'm_cls', 'TDNAZI', 'TDSECA', 'TDCCON', 'TDCDES', 'TDDCON', {name: 'TDTOCO', type: 'float'}, {name: 'TDVOLU', type: 'float'}, {name: 'S_PESO', type: 'float'}, {name: 'S_IMPORTO', type: 'float'}, 'TDDLOC', 'DEC_DEST', 'TDIDES', 'TDDCAP', 'TDIDES', 'TDPROD', 'VETTORE_ID', 'TDDT', 'LAT', 'LNG', 'ha_coordinate', 'history_coordinate', 'gmap_ind', 'GLID', 'km_n', 'km_t', 'ind_richiesta', 'ha_anomalia_sped_car', 'T_CLI_BLOC', 'T_ORD_BLOC']		
									
			}, //store
				

			    columns: [	
    				{header: 'S', dataIndex: 'TDSECA', width: 25, tooltip: 'Sequenza carico'},			
    				{header: 'Denominazione', dataIndex: 'TDDCON', flex: 1,
    						renderer: function (value, metaData, record, row, col, store, gridView){
    						
    							if (parseInt(record.get('ha_anomalia_sped_car')) == 1)
    								metaData.tdCls += ' tpSfondoGrigio';
    						
        						metaData.tdCls += ' ' + record.get('m_cls');
        						return value;	
        					}								 
    				},	
    				{header: 'Indirizzo', dataIndex: 'TDIDES', flex: 1},
    				{header: 'Localit&agrave;', dataIndex: 'DEC_DEST', flex: 1},				
    				{header: 'CAP', dataIndex: 'TDDCAP', width: 50},
    				{header: 'Pv', dataIndex: 'TDPROD', width: 30},
    				{header: 'Naz.', dataIndex: 'TDNAZI', width: 35},
    				{header: 'Colli', dataIndex: 'TDTOCO', width: 50, renderer: floatRenderer0, align: 'right', summaryType: 'sum', 
    					summaryRenderer: function(value, summaryData, dataIndex) {
    					            return floatRenderer0(value); 
    					        }				
    				},
    				{header: 'Vol.', dataIndex: 'TDVOLU', width: 60, renderer: floatRenderer2, align: 'right', summaryType: 'sum', 
    					summaryRenderer: function(value, summaryData, dataIndex) {
    					            return floatRenderer2(value); 
    					        }				
    				},
    				{header: 'Vol. Pr.', dataIndex: 'TDVOLU', width: 50, align: 'right', hidden : true, renderer: function(v, a, b, c, d, store, e, f){
    										
    					if (c==0)
    						store.tmp_value = 0;
    					store.tmp_value += parseFloat(v);
    					return floatRenderer2(store.tmp_value);
    										
    				}},										
    				{header: 'Peso', dataIndex: 'S_PESO', width: 60, renderer: floatRenderer0, align: 'right', hidden : true, summaryType: 'sum',
    					summaryRenderer: function(value, summaryData, dataIndex) {
    					            return floatRenderer0(value); 
    					        }				
    				}, 				
    				{header: 'Km', dataIndex: 'km_n', width: 60, summaryType: 'sum', align: 'right', hidden : true,
    					summaryRenderer: function(value, summaryData, dataIndex) {
    					            return floatRenderer0(value / 1000); 
    					        }, 
    						renderer: function (value, metaData, rec, row, col, store, gridView){
        						return rec.get('km_t');	
    						
        					}					        				
    				}
	         	] ,
	         
	           viewConfig: {
	        
	            plugins: {
	                ptype: 'gridviewdragdrop',
	                dragGroup: 'sped_grid',
	                dropGroup: 'sped_grid'
	            }
	            
	            
	          
	        },
	        	dockedItems: [{
	            dock: 'bottom',
	            xtype: 'toolbar',
	 
	            items: [
	                    {
	                    iconCls: 'icon-save-32', scale: 'large',
	                    itemId: 'applica_sequenza',
	                    text: 'Applica sequenza di scarico',
	                    disabled: false,
	                    scope: this,
	                    handler: function(bt){	                    	
	         
							Ext.Ajax.request({
							        url        : 'acs_op_exe.php?fn=progetta_spedizioni_applica_sequenza',
							        method     : 'POST',
							        waitMsg    : 'Data loading',
				        			jsonData: {
				        				no_stacco_carico: 'Y',
				        				sped_car_id: <?php echo j($sped_id) ?>,
				        				selected_id: Ext.pluck(bt.up('grid').getStore().data.items, 'data'),
				        				cod_iti: <?php echo j($sped['CSCITI']) ?>, data: <?php echo j($sped['CSDTSP']) ?>	
									},							        
							        success : function(result, request){
							        		bt.up('grid').getStore().load();
							       
							        		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });							
							
			                
			            }
	                }            
	            ]
			}]

			
			
		}				
	
     ]
        
 }
<?php 
 exit;
}
?>