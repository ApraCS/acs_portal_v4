<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();
$main_module =  new Spedizioni();
$cfg_mod = $main_module->get_cfg_mod();


$m_table_config = array(
    'module'      => $main_module,
    't_panel' =>  "OEPRU - Profili OE",
    'tab_name' =>  $cfg_mod['file_tabelle'],
    'descrizione' => "Gestione tabella Order Entry Profili per utente",
    'form_title' => "Dettagli tabella profili email",
    'fields_preset' => array(
        'TATAID' => 'OEPRU'
    ),
    
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TAKEY2'),
    'fields_grid' => array('TAKEY1', 'TAKEY2', 'immissione'),
    'fields_form' => array('TAKEY1', 'TAKEY2'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Utente',  'maxLength' => 20),
        'TAKEY2' => array('label'	=> 'Profilo di accesso',  'type' => 'from_TA', 'TAID' => 'EMPRF'),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
