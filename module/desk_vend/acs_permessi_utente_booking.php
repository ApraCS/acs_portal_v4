<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
		'tab_name' =>  $cfg_mod_Spedizioni['file_tabelle'],
		
		'TATAID' => 'UTMOB',
		'descrizione' => 'Gestione permessi funzioni booking per utente',
		
		'fields_key' => array('TAKEY1', 'TAKEY2', 'TAKEY3'),
		
		'fields' => array(				
				'TAKEY1' => array('label'	=> 'Utente', 'type' => 'user'),
		        'TAKEY2' => array('label'	=> 'Modulo', 'type' => 'from_TA', 'TAID' => 'MODUL'),
				'TAKEY3' => array('label'	=> 'Funzione'),
				'TADESC' => array('label'	=> 'Descrizione'),
				'TASTAL' => array('label'	=> 'Notifica in menu', 'type' => 'checkbox')				
		),
    
    
     'flt_adv' => array(
         'TAKEY3' => array('B_ACC_AMM', 'B_ACC_LOG', 'B_CHG_STS'),
                   
     ),
 
 
		
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
