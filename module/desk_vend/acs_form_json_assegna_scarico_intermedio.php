<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));



// ******************************************************************************************
// ELENCO vettori per itinerario
// ******************************************************************************************
/* con preselezionata la destinazione standard */

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $sql = "SELECT TA.*,
            TA_VUDE.TANR AS C_DES,
            TA_VUDE.TAREST AS VUDE_TAREST,
            TA_TRASP.TAINDI AS TRASP_INDI,
            TA_TRASP.TALOCA AS TRASP_LOCA,
            TA_TRASP.TAPROV AS TRASP_PROV,
            TA_TRASP.TANAZI AS TRASP_NAZI
                
		 	FROM {$cfg_mod_Spedizioni['file_tabelle']} TA

            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_VUDE
                ON TA_VUDE.TADT = TA.TADT AND TA_VUDE.TAID = 'VUDE' 
               AND TA_VUDE.TACOR1 = TA.TAKEY1 AND TA_VUDE.TATP <>'S'
               /* solo Destinazioni */ AND SUBSTRING(TA_VUDE.TAREST, 163, 1) IN ('D', '')
               /* solo la standard */ AND SUBSTR(TA_VUDE.TAREST, 173, 1) = 'S'

            /* join con trasportatore */
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRASP
                ON TA_TRASP.TADT = TA.TADT AND TA_TRASP.TATAID = 'AVET' 
               AND TA_TRASP.TAKEY1 = TA.TAKEY1


		 	WHERE TA.TADT='{$id_ditta_default}' AND TA.TATAID = 'TRIT' AND TA.TAKEY2 = ?";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->cod_iti));
    echo db2_stmt_errormsg($stmt);
    
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $n = array(
            "COD_TRASPORTATORE"	=> trim($row['TAKEY1']),
            "TRASPORTATORE"		=> acs_u8e($s->decod_std('AVET', $row['TAKEY1'])),
            "C_DES"             => trim($row['C_DES'])
        );
        
        $n['ANAG_INDIRIZZO'] = acs_u8e($row['TRASP_INDI']);
        $n['ANAG_LOCALITA']  = acs_u8e($row['TRASP_LOCA']);
        $n['ANAG_PROV']      = acs_u8e($row['TRASP_PROV']);
        $n['ANAG_NAZ']       = acs_u8e($row['TRASP_NAZI']);
        
        if (strlen($row['C_DES']) > 0){
            //indirizzo da dest. standard
            $n['INDIRIZZO'] = acs_u8e(substr($row['VUDE_TAREST'],  30,  60));
            $n['LOCALITA']  = acs_u8e(substr($row['VUDE_TAREST'],  90,  60));
            $n['PROV']      = substr($row['VUDE_TAREST'], 160, 2);
            $n['NAZ']       = substr($row['VUDE_TAREST'], 193, 2);
        } else {
            //indirizzo da tabella trasportatori (TRIT)
            $n['INDIRIZZO'] = acs_u8e($row['TRASP_INDI']);
            $n['LOCALITA']  = acs_u8e($row['TRASP_LOCA']);
            $n['PROV']      = acs_u8e($row['TRASP_PROV']);
            $n['NAZ']       = acs_u8e($row['TRASP_NAZI']);
        }
        
        $ar[] = $n;
    } //while
    
    echo acs_je($ar);    
    exit;
}

?>
{"success":true, "items": [

        {
            xtype: 'grid',            
			
 			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-button_black_repeat-32',
                     text: 'Annulla scarico intermedio/deposito',
			            handler: function() {
							//RIPRISTINO DESTINAZIONE INIZIALE (CLIENTE)	            
			            
											Ext.Ajax.request({
											   url: 'acs_op_exe.php?fn=exe_assegna_scarico_intermedio',
											   method: 'POST',
											   jsonData: {list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
											   			  to_trasportatore: 'RIPRISTINO'
											   			  }, 
											   
											   success: function(response, opts) {					   				
							                                this.print_w.close();
										  				  	m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
										  				  	m_grid.getStore().treeStore.load();
											   	
											   }
											});	            
			            
			             }                       
                     }, '->', {
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-button_blue_play-32',
                     text: 'Conferma scarico / deposito',
			            handler: function() {
								
								var m_grid = this.up('grid'),
									rec_sel = m_grid.getSelectionModel().getSelection();
									
								if (Ext.isEmpty(rec_sel)){
                                	acs_show_msg_error('Selezionare un vettore');
						        	return false;
                            	}
								
								rec_sel = rec_sel[0];
								
			        			Ext.Ajax.request({
									   url: 'acs_op_exe.php?fn=exe_assegna_scarico_intermedio',
									   method: 'POST',
									   jsonData: {list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
									   			  to_trasportatore: rec_sel.get('COD_TRASPORTATORE'),
									   			  to_trasportatore_cod_des: rec_sel.get('C_DES')
									   			  }, 
									   
									   success: function(response, opts) {					   				
					                                this.print_w.close();
								  				  	m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								  				  	m_grid.getStore().treeStore.load();
									   	
									   }
									});
			             }
                     }]
        		}],			
			
			store: {
					xtype: 'store',
					autoLoad:true,
					
					
					proxy: {
						url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						extraParams   : {cod_iti: '<?php echo $m_params->cod_iti ?>'},
						method: 'POST', type: 'ajax',
						
						actionMethods: {read: 'POST'},
			
						reader: {
							type: 'json',
							method: 'POST',
							root: 'root'					
						}
						
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
						, doRequest: personalizza_extraParams_to_jsonData						        
						
					},	
										
		        	fields: ['COD_TRASPORTATORE', 'TRASPORTATORE', 'C_DES',
		        			'INDIRIZZO', 'LOCALITA', 'PROV', 'NAZ',
		        			'ANAG_INDIRIZZO', 'ANAG_LOCALITA', 'ANAG_PROV', 'ANAG_NAZ']							
					
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Trasportatore',
	                dataIndex: 'TRASPORTATORE',
	                flex: 1 
	            }, 
	            {
	                header   : 'Dest.',
	                dataIndex: 'C_DES',
	                width: 70 
	            },
	            {
	                header   : 'Indirizzo',
	                dataIndex: 'INDIRIZZO',
	                flex: 1 
	            },
	            {
	                header   : 'Localit&agrave;',
	                dataIndex: 'LOCALITA',
	                flex: 1 
	            },
	            {
	                header   : 'Prov.',
	                dataIndex: 'PROV',
	                width: 60 
	            },
	            {
	                header   : 'Naz.',
	                dataIndex: 'NAZ',
	                width: 60 
	            },
	            
	            
	            { //GESTIONE DESTINAZIONE
                    xtype:'actioncolumn', width:60,
                    text: '<img src=<?php echo img_path("icone/48x48/home.png") ?> width=28>',
                    tdCls: 'tdAction', tooltip: 'Destinazione',
                    menuDisabled: true, sortable: false,
                    
                    items: [
                    	//ricerca destinazione
                    	{
                                icon: <?php  echo img_path("icone/24x24/search.png") ?>,
                                tooltip: 'Seleziona destinazione',
                	            handler: function(view, rowIndex, colIndex, item, e, record, row) {
                	                
                	                var my_listeners_search_loc = {
                	                	afterSelected: function(from_win, destSelected){
                	                		record.set('C_DES', destSelected.cod);
                	                		record.set('INDIRIZZO', destSelected.IND_D);
                	                		record.set('LOCALITA', destSelected.LOC_D);
                	                		record.set('PROV', destSelected.PRO_D);
                	                		//record.set('NAZ', destSelected.cod);
                	                		record.commit();
                	                		from_win.destroy();
                	                	}						    
									};
									
                	                acs_show_win_std(null, 
			 				        	'../base/acs_seleziona_destinazione.php?fn=open_panel', 
										{cliente: record.get('COD_TRASPORTATORE'), type : 'D'
									 	}, null, null, my_listeners_search_loc);
                	                }
                        },
                        //rimuovo destinazione (imposto indirizzo da anagrafica)
                        {
                        	icon: <?php  echo img_path("icone/24x24/sub_red_delete.png") ?>,
                        	tooltip: 'Rimuovi selezione destinazione',
                	        handler: function(view, rowIndex, colIndex, item, e, record, row) {
                	        	record.set('C_DES', '');
    	                		record.set('INDIRIZZO', record.get('ANAG_INDIRIZZO'));
    	                		record.set('LOCALITA', record.get('ANAG_LOCALITA'));
    	                		record.set('PROV', record.get('ANAG_PROV'));
    	                		//record.set('NAZ', record.get('ANAG_NAZ'));
    	                		record.commit();
                	        }
                        }
                    ]
                }
	         ],																					

	         
	        listeners: {
	           /* sostituito da bottone */
	           /*
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
									Ext.Ajax.request({
									   url: 'acs_op_exe.php?fn=exe_assegna_scarico_intermedio',
									   method: 'POST',
									   jsonData: {list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
									   			  to_trasportatore: iView.getSelectionModel().getSelection()[0].data.COD_TRASPORTATORE
									   			  }, 
									   
									   
									   
									   success: function(response, opts) {					   				
					                                this.print_w.close();
								  				  	m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								  				  	m_grid.getStore().treeStore.load();
									   	
									   }
									});						  	
						  	
						  	
						  }
					  }
				*/	  	  
			}			
			
				  
	            
        }    

]}