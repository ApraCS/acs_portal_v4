<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();
$main_module =  new Spedizioni();
$cfg_mod = $main_module->get_cfg_mod();

if(isset($m_params->open_request->menu_type))
    $menu_type = $m_params->open_request->menu_type;
else 
    $menu_type = $m_params->menu_type;
    
if($menu_type == 'ORD_VEN')
    $TAKEY3 = 'OV';
if($menu_type == 'PRO_MON')
    $TAKEY3 = 'PM';

$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    
    'descrizione' => "Gestione tabella funzioni",
    'form_title' => "Dettagli tabella funzioni",
    'fields_preset' => array(
        'TATAID' => 'BATCH',
        'TAKEY2' => 'DOVE',
        'TAKEY3' => $TAKEY3,
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
		
	'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TADESC', 'TARIF1', 'TATELE', 'TAMAIL', 'TAINDI', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC',  'TARIF1', 'TATELE', 'TAMAIL', 'TAINDI'),
    
	'fields' => array(				
	    'TAKEY1' => array('label'	=> 'Codice', 'maxLength' => 10, 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
	    'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 100),
	    'TAMAIL' => array('label'	=> 'Note',  'maxLength' => 100),
	    'TARIF1' => array('label'	=> 'Gruppo',  'maxLength' => 20, 'fw'=>'width: 160', 'type' => 'from_TA', 'TAID' => 'BATCG', 'k2' => 'DOVE', 'k3' => $TAKEY3),
	    'TATELE' => array('label'	=> 'Write RI', 'maxLength' => 20),
	    'TAINDI' => array('label'	=> 'Richiama funzione', 'maxLength' => 60, 'tooltip' => 'es: desk_vend/verifica.php'),
	    
	    //immissione
	    'immissione' => array(
	        'type' => 'immissione', 'fw'=>'width: 70',
	        'config' => array(
	            'data_gen'   => 'TADTGE',
	            'user_gen'   => 'TAUSGE'
	        )
	        
	    ),
	    'TADTGE' => array('label'	=> 'Data generazione'),
	    'TAUSGE' => array('label'	=> 'Utente generazione')
	)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
