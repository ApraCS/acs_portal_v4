<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$da_form = acs_m_params_json_decode();

$week = $da_form->settimana;
$year = $da_form->anno;

 
 $sottostringhe = explode("|", $da_form->rec_id);
 
 switch ($da_form->rec_liv){
 	case "liv_0":
		$m_sped = $sottostringhe[2];		
		break;
 	case "liv_1":
		$tmp_carico = $sottostringhe[2];
		$tmp = explode("___", $tmp_carico);
		$m_data = $tmp[0];
		$m_sped = $tmp[1];
		$m_tpca = $tmp[2];
		$m_aaca = $tmp[3];
		$m_nrca = $tmp[4];		
		break;
 	case "liv_2": 
		$tmp_carico = $sottostringhe[2];
		$tmp = explode("___", $tmp_carico);
		$m_data = $tmp[0];
		$m_sped = $tmp[1];
		$m_tpca = $tmp[2];
		$m_aaca = $tmp[3];
		$m_nrca = $tmp[4];		
		
		$tmp_cliente = $sottostringhe[4];
		$tmp = explode("___", $tmp_cliente);
		$m_ccon = $tmp[1];				
		
		break;		
	default: die("error");			
 }
 
 
 
	$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} ";
	$sql_WHERE = " WHERE " . $s->get_where_std() . " AND TDSWSP='Y' ";

	if (isset($m_data)) $sql_WHERE .= " AND TDDTEP = $m_data ";
	if (isset($m_sped)) $sql_WHERE .= " AND TDNBOC = " . $m_sped;	
	if (isset($m_tpca)) $sql_WHERE .= " AND TDTPCA = " . sql_t_trim($m_tpca);	
	if (isset($m_tpca)) $sql_WHERE .= " AND TDAACA = " . $m_aaca;	
	if (isset($m_nrca)) $sql_WHERE .= " AND TDNRCA = " . $m_nrca;	
	if (isset($m_ccon)) $sql_WHERE .= " AND TDCCON = " . sql_t_trim($m_ccon);	

	
	$sql = "SELECT * " . $sql_FROM . $sql_WHERE	. " ORDER BY TDDTEP, TDCITI, TDNBOC, TDSECA, TDCCON";
	
	$stmt = db2_prepare($conn, $sql);		
	$result = db2_execute($stmt);
 
    $liv0_in_linea = null;
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$liv0 = implode("|", array($r['TDDTEP'], $r['TDCITI'], $r['TDVETT']));
		$liv1 = implode("|", array($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA']));		
		$liv2 = implode("|", array($r['TDCCON'], $r['TDCDES']));		
		$liv3 = implode("|", array($r['TDDT'], $r['TDOTID'], $r['TDOINU'], $r['TDOADO'], $r['TDONDO']));		
		
		//data|itinerario|vettore
		$t_ar = &$ar;
		$l = $liv0;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r); 					  								  
			$t_ar = &$t_ar[$l]['children'];
		
		//carico
		$l = $liv1;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r); 								  
			$t_ar = &$t_ar[$l]['children'];
		 								  								  
		//cliente|dest
		$l = $liv2;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];								  								  

		//ordine
		$l = $liv3;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];			
		
	} //while

 		
?>

            



{"success":true, "items": [
		{
			xtype: 'panel',
			layout: 'fit',
			title: 'Geolocalizzazione e assegnazione sequenza scarichi',
			items: [
							{	            	
					            xtype: 'form',
					            //bodyStyle: 'padding: 10px',
					            layout: 'hbox',
					            align: 'stretch',
					            height: 500,
					            //bodyPadding: '5 5 0',
					            frame: true,
					            title: '',
					            url: 'ssssss.php',
					            items: [
										   {
											  xtype     : 'hiddenfield',											  
									          name      : 'fn',
									          value		: 'registra_sequenza_scarico'
									        },					            
										   {
						            		width: 50, height: '100%',
											  xtype     : 'textareafield',											  
									          name      : 'new_sequence',
									          fieldLabel: ''
									        }, {
						            		width: 50, height: '100%',
											  xtype     : 'textareafield',
				          					  value: <?php echo j(text_seq_area_val($ar)); ?>,									          
									          fieldLabel: ''
									        }, {
											  xtype     : 'hiddenfield',
									          name      : 'old_sequence',
				          					  value: <?php echo j(text_seq_codici($ar)); ?>,									          
									          fieldLabel: ''
									        }, {
						            		flex: 1, height: '100%',
											  xtype     : 'textareafield',
									          name      : 'elenco',
				          					  value: <?php echo j(text_area_val($ar)); ?>,									          
									          fieldLabel: ''
									        }									        
					            ],
					            buttons: [
											{
	            						text: 'Optimap',	            						
							            handler: function() {
							                this.up('form').submit({
						                        url: 'http://gebweb.net/optimap/',
						                        target: '_blank', 
						                        standardSubmit: true,
						                        method: 'POST'
						                  });            
							            }	            						
	            						
	            						
	            					}, {
	            						text: 'Applica nuova sequenza',
							            handler: function() {
							                this.up('form').submit({
						                        url: 'acs_op_exe.php',
						                        method: 'POST',
						                            success: function(form,action) {
														tree = Ext.getCmp('<?php echo $da_form->grid_id ?>');
														tree.getStore().treeStore.load();		                            	
						                            }						                        
						                  }); 
						                  this.up('window').close();           
							            }	            		            						
	            						
	            					}, {
	            						text: 'Applica nuova sequenza (inversa)',
							            handler: function() {
							                this.up('form').submit({
						                        url: 'acs_op_exe.php?inverse=Y',
						                        method: 'POST',
						                            success: function(form,action) {
														tree = Ext.getCmp('<?php echo $da_form->grid_id ?>');
														tree.getStore().treeStore.load();		                            	
						                            }						                        
						                  }); 
						                  this.up('window').close();           
							            }	            		            						
	            						
	            					}
	            				]
							}
			
			]
			    		
    	}
/*    	
    	,{
    		xtype: 'grid',
    		layout: 'fit',
    		flex: 80,
    		title: 'Riepilogo',
    		columns: [
              {
                  text: 'Seq',
                  width: 30,
                  dataIndex: 'seq'
              },{
                  text: 'Cliente',
                  flex: 30,
                  dataIndex: 'cli'
              }, {
                  text: 'Descr',
                  dataIndex: 'descr',
                  flex: 30
              }],
             store: {
             	fields: [{name: 'seq'}, {name: 'cli'}, {name: 'descr'}],
             	data: [
             	 <?php echo table_data($ar) ?>
             	]
             } 
    		
    	}  	 
*/
]}





<?php


function table_data($ar){
	$ar_out = array();
	foreach ($ar as $kl0 => $l0){
	  		foreach ($l0['children'] as $kl1 => $l1){
			  		foreach ($l1['children'] as $kl2 => $l2){	
			  			$r = $l2['record'];
						//$ret .= implode(', ', array_map('trim', array($r['TDDNAD'], $r['TDDCAP'], $r['TDPROD'], $r['TDDLOC'] , substr($r['TDIDES'], 0, 30)))) . "\n";
						
						$ar_out[] = "{" . "'seq': " . j(trim($r['TDSECA'])) . ",
										   'cli': " . j(trim($r['TDDDES'])) . ", 
										   'descr': " . j(trim($r['TDDLOC'])) . "}";
					}			
			}		
		
	}
	
 return implode(', ', $ar_out);	
}


function html_table($ar){
	$ret = "<table>";
	foreach ($ar as $kl0 => $l0){
	  		foreach ($l0['children'] as $kl1 => $l1){
			  		foreach ($l1['children'] as $kl2 => $l2){
			  		  $ret = $ret . "<TR>";	
			  			$r = $l2['record'];
						//$ret .= implode(', ', array_map('trim', array($r['TDDNAD'], $r['TDDCAP'], $r['TDPROD'], $r['TDDLOC'] , substr($r['TDIDES'], 0, 30)))) . "\n";
						$ret = $ret . "<TD>" . trim($r['TDSECA']) . "</TD>";
						$ret = $ret . "<TD>" . trim($r['TDDLOC']) . "</TD>";						
					  $ret .= "</TR>";
					}			
			}		
		
	}
  $ret .= "</table>";	
 return $ret;	
}




function text_seq_codici($ar){
	$c = 0;
	$ret = "";
	
	//start map	
	$c++;
	$ret .= "$c##START MAP\n";
	
	foreach ($ar as $kl0 => $l0){
	  		foreach ($l0['children'] as $kl1 => $l1){
			  		foreach ($l1['children'] as $kl2 => $l2){
			  			$c++;
			  			$r = $l2['record'];
						$ret .= "{$c}##" . implode('|', array_map('trim', array(
										$r['TDDTEP'], $r['TDCITI'], $r['TDVETT'],
										$r['TDTPCA'], $r['TDAACA'], $r['TDNRCA'],
										$r['TDCCON'], $r['TDCDES']
										))) . "\n";
					}			
			}		
		
	}
 return $ret;	
}

function text_seq_area_val($ar){
	$ret = "";
	
	//start map
	$ret .= "ST\n";
	
	foreach ($ar as $kl0 => $l0){
	  		foreach ($l0['children'] as $kl1 => $l1){
			  		foreach ($l1['children'] as $kl2 => $l2){
			  			$r = $l2['record'];
						$ret .= implode(', ', array_map('out_seq', array($r['TDSECA']))) . "\n";
					}			
			}		
		
	}
 return $ret;	
}


function text_area_val($ar){
	global $s;
	$ret = "";
	
	//start map
	$ret .= trim($s->get_start_map()) . "\n";
	
	foreach ($ar as $kl0 => $l0){
	  		foreach ($l0['children'] as $kl1 => $l1){
			  		foreach ($l1['children'] as $kl2 => $l2){
			  			$r = $l2['record'];
						$ret .= implode(', ', array_map('trim', array($r['TDDNAD'], $r['TDDCAP'], $r['TDPROD'], $r['TDDLOC'] , substr($r['TDIDES'], 0, 30)))) . "\n";
					}			
			}		
		
	}
 return $ret;	
}

function somma_valori($ar, $r){
 $ar['VOLUME'] 	+= $r['TDVOLU'];
 $ar['COLLI'] 	+= $r['TDTOCO'];
 $ar['PALLET'] 	+= $r['TDBANC'];
 $ar['IMPORTO'] += $r['TDTIMP']; 
 $ar['PESO'] 	+= $r['TDPLOR']; 
 return $ar;  
}

function out_seq($v){
	if (strlen(trim($v)) == 0)
	 return "-";
	else {
	 return trim($v);
	}
}


?>		