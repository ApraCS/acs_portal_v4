<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();
$smc = new SpedManutenzioneCosti();

$stmt = $smc->get_stmt_rows();

?>


<html>
<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table{border-collapse:collapse; width: 100%;}
table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
h2.acs_report{background-color: black; color: white; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}
 
@media print
{
	.noPrint{display:none;}
}

</style>



</head>
<body>

<div class="page-utility noPrint">
<A HREF="javascript:window.print()"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>
	
	<A HREF="#" onclick="window.open('data:application/vnd.ms-excel,' + '<html><head>' + escape(document.getElementsByTagName('style')[0].outerHTML) + '</head>' + '<body>' + escape(document.getElementById('my_content').outerHTML) + '</body></html>');"><img src=<?php echo img_path("icone/48x48/save.png") ?>></A>
		
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>
		
</div> 	

<div id='my_content'>

<h2>Riepilogo incidenza trasporto spedizioni programmate/spedite</h2>
<pre>
<?php

$ar = array();
while ($row = db2_fetch_assoc($stmt)){
 $row = $smc->after_get_row($row);
 
 $row['imp_riprog'] = $smc->get_importo_riprogrammato_su_sped($row['CSPROG']);
 $row['imp_prog']	= $row['S_IMPORTO'] + $row['imp_riprog'];
 $row['NR_ORDINI_COMPLETI'] = $s->get_nr_ordini_completi_by_sped($row['CSPROG']); 
 
 $liv0 = implode("_", array($row['CSDTSP'], trim($row['TAASPE'])));
 
 $d_ar = &$ar; 
 $c_liv = $liv0;
 if (!isset($ar[$c_liv]))
 	$ar[$c_liv] = array("cod" => $c_liv, "data" => $row['CSDTSP'], "area" => $row['TAASPE'], "descr"=>$c_liv, "children"=>array());

 $d_ar = &$d_ar[$c_liv]['children']; 
 
 $d_ar[] = $row;
 
}


// STAMPO REPORT *******************************
foreach ($ar as $k0 => $l0){

 $l0_tot = array();

 echo "<h2 class=acs_report>" . print_date($l0['data']) . " - " . $s->decod_std('ASPE', $l0['area']) . "</h2>";
 
 echo "<table class=acs_report>";
 
 ?>
		<tr class="t-l1">
		 <td>Trasportatore</td>
 		 <td>Vettore</td>
 		 <td>Itinerario</td>		 
 		 <td>Elenco carichi</td>		 
 		 <td>Nr ordini completi</td>		 		 
 		 <td>Nr scarichi</td>		 
 		 <td>Km</td> 		 
 		 <td>Colli</td>		 
 		 <td>Volume</td>		 
 		 <td>Costo trasporto</td>		 
 		 <td>Importo<br/>spedizione</td>		 
 		 <td>% inc.</td>		
 		 <td>Importo<br/>programmato</td>		 
 		 <td>% inc.</td>				  
 		 <td>Importo<br/>riprogramm.</td>		 
 		</tr>
 		
 <?php 		
 
 	 usort($l0['children'], "cmp_by_TRASP_D");  
	 foreach ($l0['children'] as $k1 => $l1){

		if ($l1['S_IMPORTO'] != 0)
			$perc_incidenza = ($l1['CSCSTR'] + $l1['CSCSTD']) / $l1['S_IMPORTO']  * 100;
		else $perc_incidenza = 0;

		if ($l1['imp_prog'] != 0)
			$perc_incidenza_prog = ($l1['CSCSTR'] + $l1['CSCSTD']) / $l1['imp_prog']  * 100;
		else $perc_incidenza_prog = 0;		
		
		
		?>
		
		<tr>
		 <td><?php echo $l1['TRASP_D']  . " (#{$l1['CSPROG']})"; ?></td>
		 <td><?php echo $l1['VMC_D']; ?>
		 
		 	<?php
		 	 	if (strlen(trim($l1['CSDESC'])) > 0)
		 	 		echo "<br/>" . $l1['CSDESC']; 
		 	?>
		 
		 </td>
		 <td><?php echo $l1['ITIN_D']; ?></td>		 
		 <td><?php echo $l1['EL_CARICHI_D']; ?></td>		 
		 <td class=number><?php echo $l1['NR_ORDINI_COMPLETI']; ?></td>		 		 
		 <td class=number><?php echo $l1['C_CLIENTI_DEST']; ?></td>
		 <td class=number><?php echo $l1['CSKMTR']; ?></td>
		 <td class=number><?php echo $l1['S_COLLI']; ?></td>		 
		 <td class=number><?php echo n($l1['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo $l1['CSCSTR_OUT']; ?></td>		 
		 <td class=number><?php echo n($l1['S_IMPORTO'], 2); ?></td>		 
		 <td class=number><?php echo n($perc_incidenza, 2); ?></td>		
		 <td class=number><?php if ($l1['S_IMPORTO'] <> $l1['imp_prog']) echo n($l1['imp_prog'], 2); ?></td>		 
		 <td class=number><?php if ($l1['S_IMPORTO'] <> $l1['imp_prog'])echo n($perc_incidenza_prog, 2); ?></td>				  
		 <td class=number><?php if ($l1['S_IMPORTO'] <> $l1['imp_prog'])echo n($l1['imp_riprog'], 2); ?></td>		 
		</tr>
		
		
		<?php
		
		//somme e conteggi
		$l0_tot = somme_conteggi($l0_tot, $l1);
		$gen_tot = somme_conteggi($gen_tot, $l1);		
		
	 } //per ogni riga
	 
	 //totale per area
		 if ($l0_tot['S_IMPORTO'] != 0)
		 	$perc_incidenza = ($l0_tot['CSCSTR'] + $l0_tot['CSCSTD']) / $l0_tot['S_IMPORTO']  * 100;
		 else $perc_incidenza = 0;
		 
		 if ($l0_tot['imp_prog'] != 0)
		 	$perc_incidenza_prog = ($l0_tot['CSCSTR'] + $l0_tot['CSCSTD']) / $l0_tot['imp_prog']  * 100;
		 else $perc_incidenza_prog = 0;
	 
	 ?>
	 
		<tr class="f-l1">
		 <td colspan=4>Totale area spedizione <?php echo $s->decod_std('ASPE', $l0['area']); ?></td>		 
		 <td class=number><?php echo $l0_tot['NR_ORDINI_COMPLETI']; ?></td>		 		 
		 <td class=number><?php echo $l0_tot['C_CLIENTI_DEST']; ?></td>
		 <td class=number><?php echo $l0_tot['S_KM']; ?></td>		 		 
		 <td class=number><?php echo $l0_tot['S_COLLI']; ?></td>		 
		 <td class=number><?php echo n($l0_tot['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo n($l0_tot['CSCSTR'] + $l0_tot['CSCSTD'], 2); ?></td>		 
		 <td class=number><?php echo n($l0_tot['S_IMPORTO'], 2); ?></td>		 
		 <td class=number><?php echo n($perc_incidenza, 2); ?></td>		
		 <td class=number><?php if ($l0_tot['S_IMPORTO'] <> $l0_tot['imp_prog']) echo n($l0_tot['imp_prog'], 2); ?></td>		 
		 <td class=number><?php if ($l0_tot['S_IMPORTO'] <> $l0_tot['imp_prog']) echo n($perc_incidenza_prog, 2); ?></td>				  
		 <td class=number><?php if ($l0_tot['S_IMPORTO'] <> $l0_tot['imp_prog']) echo n($l0_tot['imp_riprog'], 2); ?></td>		 
		</tr>
	 
	 
	 
	 <?php
	 
	 
	 
 echo "</table>";

}


// TOTALE GENERALE *******************************************************************
	if ($gen_tot['S_IMPORTO'] != 0)
		$perc_incidenza = ($gen_tot['CSCSTR'] + $gen_tot['CSCSTD']) / $gen_tot['S_IMPORTO']  * 100;
	else $perc_incidenza = 0;
		
	if ($gen_tot['imp_prog'] != 0)
		$perc_incidenza_prog = ($gen_tot['CSCSTR'] + $gen_tot['CSCSTD']) / $gen_tot['imp_prog']  * 100;
	else $perc_incidenza_prog = 0;

?>


<?php if (count($ar) > 1){ ?>
<h2 class=acs_report>Totale generale</h2>
<table class=acs_report>
		<tr class="t-l1">
		 <td width="50%" colspan=4></td>		 
 		 <td>Nr ordini completi</td>		 		 
 		 <td>Nr scarichi</td>
 		 <td>Km</td>		 
 		 <td>Colli</td>		 
 		 <td>Volume</td>		 
 		 <td>Costo trasporto</td>		 
 		 <td>Importo<br/>spedizione</td>		 
 		 <td>% inc.</td>		
 		 <td>Importo<br/>programmato</td>		 
 		 <td>% inc.</td>				  
 		 <td>Importo<br/>riprogramm.</td>		 
 		</tr>
		<tr class="f-l1">
		 <td colspan=4>Totale generale</td>		 
		 <td class=number><?php echo $gen_tot['NR_ORDINI_COMPLETI']; ?></td>		 		 
		 <td class=number><?php echo $gen_tot['C_CLIENTI_DEST']; ?></td>
		 <td class=number><?php echo $gen_tot['S_KM']; ?></td>		 
		 <td class=number><?php echo $gen_tot['S_COLLI']; ?></td>		 
		 <td class=number><?php echo n($gen_tot['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo n($gen_tot['CSCSTR'] + $gen_tot['CSCSTD'], 2); ?></td>		 
		 <td class=number><?php echo n($gen_tot['S_IMPORTO'], 2); ?></td>		 
		 <td class=number><?php echo n($perc_incidenza, 2); ?></td>		
		 <td class=number><?php if ($gen_tot['S_IMPORTO'] <> $gen_tot['imp_prog']) echo n($gen_tot['imp_prog'], 2); ?></td>		 
		 <td class=number><?php if ($gen_tot['S_IMPORTO'] <> $gen_tot['imp_prog']) echo n($perc_incidenza_prog, 2); ?></td>				  
		 <td class=number><?php if ($gen_tot['S_IMPORTO'] <> $gen_tot['imp_prog']) echo n($gen_tot['imp_riprog'], 2); ?></td>		 
		</tr>
 		
</table>
<?php } ?>




<?php


function cmp_by_TRASP_D($a, $b)
{
	return strcmp($a["TRASP_D"], $b["TRASP_D"]);
}


function somme_conteggi($ar, $r){
	$ar['NR_ORDINI_COMPLETI'] 	+= $r['NR_ORDINI_COMPLETI'];
	$ar['C_CLIENTI_DEST'] 		+= $r['C_CLIENTI_DEST'];
	$ar['S_KM'] 				+= $r['CSKMTR'];	
	$ar['S_COLLI'] 				+= $r['S_COLLI'];
	$ar['S_VOLUME'] 			+= $r['S_VOLUME'];
	$ar['S_IMPORTO'] 			+= $r['S_IMPORTO'];
	$ar['imp_prog'] 			+= $r['imp_prog'];
	$ar['imp_riprog'] 			+= $r['imp_riprog'];	
	$ar['CSCSTR'] 				+= $r['CSCSTR'];
	$ar['CSCSTD'] 				+= $r['CSCSTD'];		
  return $ar;	
} 



?>


</div>
</body>
</html>


