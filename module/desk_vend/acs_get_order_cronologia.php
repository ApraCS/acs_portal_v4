<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$n_ord = $_REQUEST["k_ordine"];		
$oe = $s->k_ordine_td_decode($n_ord);

$stmt = $s->get_cronologia_ordine($n_ord);	

$ret = array();
while ($row = db2_fetch_assoc($stmt)){
	
	$row['data'] 		= $row['PQDTGE'];
	$row['utente']		= print_ora($row['PQHMGE'], 4) . " - " . $row['PQUTEN'];
	
	//se diversa aggiunto l'ora di fine attivita'
	if ($row['PQHMUM'] != 0 && ($row['PQDTGE'] != $row['PQDTUM'] || $row['PQHMGE'] != $row['PQHMUM']) )
		$row['utente'] .= "<br> -> " . print_ora($row['PQHMUM'], 4);	
	
	$row['attivita']	= acs_u8e($row['TADESC']); 
	$row['stato_data']	= $row['PQSTAT'] . " - " . print_date($row['PQDTCE']);
	
	
	//global $backend_ERP;
	//if ($backend_ERP == 'GL')
	if(trim($row['PQNOQ1']) != '')
	    $row['attivita']	.= " - ".trim($row['PQNOQ1']);		
		
	//php7 $row = array_map('utf8_encode', $row);
	$ret[] = $row;
}

echo acs_je($ret);

$appLog->save_db();




 


?>
