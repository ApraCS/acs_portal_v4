<?php
	if (!isset($js_include_flag_ordini)){
		$js_include_flag_ordini = array('menuDisabled' => true, 'sortable' => false);
	}
	
	if (!isset($todo_data_config))
		$todo_data_config = array('columns_show' => array('seq_carico' => array()), 'columns_hide' => array('art_da_prog' => array()));
	
	
?>

			{text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini bloccati',
						menuDisabled: <?php echo j($js_include_flag_ordini['menuDisabled'])?>, sortable: <?php echo j($js_include_flag_ordini['sortable'])?>,	  
	    			    renderer: function(value, metaData, record){
	    			    
	    			    	if (record.get('tooltip_ripro') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_ripro')) + '"';	    			    
	    			    
							if (record.get('stato') == 'V') metaData.tdCls += ' tpSfondoVerde';
							if (record.get('stato') == 'R') metaData.tdCls += ' tpSfondoRosa';
							if (record.get('stato') == 'G') metaData.tdCls += ' tpSfondoGiallo';
							if (record.get('stato') == 'B') metaData.tdCls += ' tpSfondoBianco';
	    			    
	    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
	    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
	    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
	    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';    			    				    	
	    			    	}}
	    			    			
    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini MTO',  
    	    		dataIndex: 'art_mancanti',
					menuDisabled: <?php echo j($js_include_flag_ordini['menuDisabled'])?>, sortable: <?php echo j($js_include_flag_ordini['sortable'])?>,    	    			    
    			    renderer: function(value, p, record){
    			    	if (record.get('fl_art_manc')==20) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_red.png") ?> width=18>';
    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=18>';
    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';			    	
    			    	}}

<?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['art_da_prog'])) { ?>	    			    	
	    	  , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Articoli mancanti MTS',
	        			dataIndex: 'art_da_prog',
						menuDisabled: <?php echo j($js_include_flag_ordini['menuDisabled'])?>, sortable: <?php echo j($js_include_flag_ordini['sortable'])?>,	        					    	     
	    			    renderer: function(value, p, record){
	    			    	if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
			    			if (record.get('art_da_prog')==2) return '<img src=<?php echo img_path("icone/48x48/warning_red.png") ?> width=18>';			    	
			    			if (record.get('art_da_prog')==3) return '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=18>';	    			    	
	    			    	}}

<?php } ?>    			    				    	
	    				
<?php if ($js_include_flag_ordini['show_new'] != 'N'){ ?>    	
	    	  	, {text: '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Segnalazioni stato ordine',

						menuDisabled: <?php echo j($js_include_flag_ordini['menuDisabled'])?>, sortable: <?php echo j($js_include_flag_ordini['sortable'])?>,	    	    
	    			    renderer: function(value, metaData, record){
	    			    		if (record.get('fl_new')=='Y') return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';
	    			    		if (record.get('fl_new')=='O') return '<img src=<?php echo img_path("icone/48x48/label_new_grey.png") ?> width=18>';
						    	if (record.get('fl_new')==1)   return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';
						    	if (record.get('fl_new')==2)   return '<img src=<?php echo img_path("icone/48x48/mano_ko.png") ?> width=18>';			    				    				    		    			    		
	    			    		if (record.get('fl_new')==3)   return '<img src=<?php echo img_path("icone/48x48/timer_blue.png") ?> width=18>';
			    			    if (record.get('fl_new')==4)   return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=18>';
		    			    	if (record.get('fl_new')==10)  return '<img src=<?php echo img_path("icone/48x48/bandiera_scacchi.png") ?> width=18>';
		    			    	if (record.get('fl_new')==11)  return '<img src=<?php echo img_path("icone/48x48/bandiera_grey.png") ?> width=18>';
		    			    	if (record.get('fl_new')==12)  return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
		    			    	
		    			    	if (record.get('fl_new').trim() == 'C')   return '<img src=<?php echo img_path("icone/48x48/certificate.png") ?> width=18>';
		    			    	
		    			    	if (!Ext.isEmpty(record.get('ind_modif'))){
						    		if (record.get('ind_modif').trim() == 'A') 	return '<img src=<?php echo img_path("icone/48x48/sub_blue_add.png") ?> width=18>';			    	
						    		if (record.get('ind_modif').trim() == 'R') 	return '<img src=<?php echo img_path("icone/48x48/sub_grey_add.png") ?> width=18>';
						    	}	
						    	
						    	if (record.get('tdfg06') == '1') metaData.tdCls += ' sfondo_rosso'; 
    			    			if (record.get('tdfg06') == '2') metaData.tdCls += ' sfondo_giallo';
    			    			if (record.get('tdfg06') == '3') metaData.tdCls += ' sfondo_verde';		    			    
		    			    	
	    			    	}}
<?php } ?>	    			    	
	    			    	
<?php if (
			$cfg_mod_Spedizioni['SBLOC_use_check_list'] == 'Y' &&
			in_array($m_params->form_open->tipo_op, $todo_data_config['columns_show']['seq_carico'])
		) { ?>	    			    	
	    	   , {text: '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=25>',	width: 40, tdCls: 'tdAction',
	        			dataIndex: 'seq_carico',
						menuDisabled: <?php echo j($js_include_flag_ordini['menuDisabled'])?>, sortable: <?php echo j($js_include_flag_ordini['sortable'])?>,	        			
	    			    renderer: function(value, metaData, record){
	    			    	 if (record.get('liv') == 'liv_5'){
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_seq_carico')) + '"';	    			    
	    			    			return value;
	    			    		}
	    			    	}}
<?php } ?>
