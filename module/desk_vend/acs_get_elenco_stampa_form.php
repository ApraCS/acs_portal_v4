<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$cfg_mod = $s->get_cfg_mod();

$initial_data_txt = $s->get_initial_calendar_date(); //recupero data iniziale calendario
$m_week = strftime("%V", strtotime($initial_data_txt)); 
$m_year = strftime("%G", strtotime($initial_data_txt));

$ar_area_spedizione = $s->get_options_area_spedizione();
$ar_area_spedizione_txt = '';

$ar_ar2 = array();
foreach ($ar_area_spedizione as $a)
	$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";

$ar_area_spedizione_txt = implode(',', $ar_ar2);

$da_form = acs_m_params_json_decode();
$m_params = acs_m_params_json_decode();

if ($m_params->from == 'INFO'){
  $spedizione = array();
  $itin = new Itinerari;
} else {
	//da DAY o LIST
	//recupero la spedizione
	$sottostringhe = explode("|", $da_form->rec_id);
	switch ($da_form->rec_liv){
		case "liv_0": 	//spedizione
			$m_sped = $sottostringhe[2];
			break;
		case "liv_1":
			$tmp_liv = $sottostringhe[2];
			$tmp_liv_exp = explode("___", $tmp_liv);
			$m_sped = $tmp_liv_exp[1];
			break;
	}
	$spedizione = $s->get_spedizione($m_sped);
	
	//dalla spedizione recupero l'itinerario
	$itin = new Itinerari;
	$itin->load_rec_data_by_k(array('TAKEY1' => $spedizione['CSCITI']));	
}


//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($s->get_cod_mod());


// ******************************************************************************************
// ELENCO CLIENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_el_clienti'){
	
	switch ($da_form->rec_liv){
		case "liv_0": 	//spedizione
			$dt	  = $spedizione['CSDT'];
			$ar_clienti = $s->get_el_clienti_dest_by_sped($m_sped, $dt);
			break;
		case "liv_1":	//carico
			$tpca = $tmp_liv_exp[2];
			$aaca = $tmp_liv_exp[3];
			$nrca = $tmp_liv_exp[4];
			$dt	  = $spedizione['CSDT']; 
			$ar_clienti = $s->get_el_clienti_dest_by_sped_car($m_sped, $tpca, $aaca, $nrca, $dt);
			break;		
	}	
	
	echo acs_je($ar_clienti);
	
exit; }

// ******************************************************************************************
// ELENCO AGENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_el_agenti'){

	switch ($da_form->rec_liv){
		case "liv_0": 	//spedizione
			$dt	  = $spedizione['CSDT'];
			$ar_clienti = $s->get_el_agenti_by_sped($m_sped, $dt);
			break;
		case "liv_1":	//carico
			$tpca = $tmp_liv_exp[2];
			$aaca = $tmp_liv_exp[3];
			$nrca = $tmp_liv_exp[4];
			$dt	  = $spedizione['CSDT'];
			$ar_clienti = $s->get_el_agenti_by_sped_car($m_sped, $tpca, $aaca, $nrca, $dt);
			break;
	}

	echo acs_je($ar_clienti);

	exit; }

// ******************************************************************************************
// APERTURA WINDOW ELENCO CLIENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_elenco_clienti'){ ?>
{"success": true, "items": 
  [
	{
		xtype: 'gridpanel',
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_el_clienti',
							type: 'ajax',
					        
							 actionMethods: {
								read: 'POST'
							},
			
							reader: {
								type: 'json',
							}					        
					        
						, extraParams: <?php echo acs_raw_post_data(); ?>
						
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
						, doRequest: personalizza_extraParams_to_jsonData						
											        
						},
	        			fields: ['K_COD', 'TDCCON', 'TDDCON', 'TDCDES', 'TDLOCA', 'LOC_OUT']
	    			}),
	    			
		        columns: [{
			                header   : 'Codice',
			                dataIndex: 'TDCCON', 
			                width: 100, renderer: function (value, metaData, record, row, col, store, gridView){
			                	if (record.get('TDCDES').trim().length > 0)
			                		return value.trim() + ' - ' + record.get('TDCDES').trim();
			                	else
			                		return value.trim(); 
			                }
			             }, {
			                header   : 'Denominazione',
			                dataIndex: 'TDDCON', 
			                flex: 1.4
			             }, {
			                header   : 'Localit&agrave;',
			                dataIndex: 'LOC_OUT', 
			                flex: 1
			             }, {
				            xtype:'actioncolumn', 
				            header: 'Azioni',
		                	tdCls : 'add_action_spaces',				            
				            width:50,
		            		items: [{
		                		icon: <?php echo img_path("icone/16x16/print.png") ?>,

		                		margin: "0 0 0 30",
		                		tooltip: 'Visualizza report',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			m_win.fireEvent('codSelected', 'per_cliente', rec.get('K_COD'), 'N');
		                		}
		                	  }, {
		                		icon: <?php echo img_path("icone/16x16/address_black.png") ?>,		                		
		                		tooltip: 'Visualizza e invia e-mail',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			m_win.fireEvent('codSelected', 'per_cliente', rec.get('K_COD'), 'Y');
		                		}
		                	  }
		                	]
		                }
		              ]  	
	} 
   ]
 }  	
<?php exit; } ?>
<?php
// ******************************************************************************************
// APERTURA WINDOW ELENCO AGENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_elenco_agenti'){ ?>
{"success": true, "items": 
  [
	{
		xtype: 'gridpanel',
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_el_agenti',
							type: 'ajax',
					        
							 actionMethods: {
								read: 'POST'
							},
			
							reader: {
								type: 'json',
							}					        
					        
						, extraParams: <?php echo acs_raw_post_data(); ?>
						
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
						, doRequest: personalizza_extraParams_to_jsonData						
											        
						},
	        			fields: ['K_COD', 'TDCAG1', 'TDDAG1']
	    			}),
	    			
		        columns: [{
			                header   : 'Codice',
			                dataIndex: 'TDCAG1', 
			                width: 100
			             }, {
			                header   : 'Denominazione',
			                dataIndex: 'TDDAG1', 
			                flex: 1.4
			             }, {
				            xtype:'actioncolumn', 
				            header: 'Azioni',
		                	tdCls : 'add_action_spaces',				            
				            width:50,
		            		items: [{
		                		icon: <?php echo img_path("icone/16x16/print.png") ?>,
		                		tooltip: 'Visualizza report',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			m_win.fireEvent('codSelected', 'per_agente', rec.get('K_COD'), 'N');
		                		}
		                	  }, {
		                		icon: <?php echo img_path("icone/16x16/address_black.png") ?>,
		                		tooltip: 'Visualizza e invia e-mail',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			m_win.fireEvent('codSelected', 'per_agente', rec.get('K_COD'), 'Y');
		                		}
		                	  }
		                	]
		                }
		              ]  	
	} 
   ]
 }  	
<?php exit; } ?>


<?php
// ******************************************************************************************
// DEFAULT: APERTURA FORM PARAMETRI
// ******************************************************************************************
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
            url: 'acs_print_lista_consegne.php',
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "REPORT_SCARICHI");  ?>
			
				<?php if ($m_params->from != 'INFO'){ ?>
				
					{
			            text: 'Per cliente',
			            iconCls: 'icon-address_black-24',
			            scale: 'medium',			            
			            handler: function() {
			            
			            	var m_form = this.up('form');
			            
		                   	my_listeners = {
	        					codSelected: function(type, cod, autoEmail){	
	        						
						                m_form.submit({
					                        url: 'acs_print_wrapper.php',
					                        target: '_blank', 
					                        standardSubmit: true,
					                        method: 'POST',
					                        params: {
					                        	r_type: type,
					                        	r_cod_selected: cod,
					                        	r_auto_email: autoEmail,
					                        	form_values: Ext.encode(m_form.getValues())
					                        },
					                  });
	        						
	        							        									            
					        		}
			    				};			            
			            
			            			            
				  			acs_show_win_std('Elenco clienti', 
				  			'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_elenco_clienti', 
				  			this.up('form').getValues(), 
				  			500, 450, my_listeners, 'icon-address_black-16');				  						            			                
			            }
			        } , 				
				
				<?php }?>

				<?php if ($m_params->from != 'INFO'){ ?>
				
					{
			            text: 'Per agente',
			            iconCls: 'icon-address_black-24',
			            scale: 'medium',			            
			            handler: function() {
			            
			            	var m_form = this.up('form');
			            
		                   	my_listeners = {
	        					codSelected: function(type, cod, autoEmail){	
	        						
						                m_form.submit({
					                        url: 'acs_print_wrapper.php',
					                        target: '_blank', 
					                        standardSubmit: true,
					                        method: 'POST',
					                        params: {
					                        	r_type: type,
					                        	r_cod_selected: cod,
					                        	r_auto_email: autoEmail,
					                        	form_values: Ext.encode(m_form.getValues())
					                        },
					                  });
	        						
	        							        									            
					        		}
			    				};			            
			            
			            			            
				  			acs_show_win_std('Elenco agenti', 
				  			'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_elenco_agenti', 
				  			this.up('form').getValues(), 
				  			500, 450, my_listeners, 'icon-address_black-16');				  						            			                
			            }
			        } , 				
				
				<?php }?>
				
				
				
				
			
				{
		            text: 'Visualizza',
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',
			        itemId: 'b_visualizza',
		            handler: function() {
		                this.up('form').submit({
		                
		                <?php if ($m_params->desk == 'PVEN'){ ?>
		                
	                        url: 'acs_print_lista_consegne_programmate_INFO.php',
	                        
	                     <?php }else{?>  
	                     
	                     	 url: 'acs_print_wrapper.php',
	                     
	                     <?php }?> 
	                     
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues()),
	                        	menu_type: menu_type
	                        }
	                  });
		                
		            }
		        } 
	        
	        
	        ],             
            
            items: [
            
<?php if ($m_params->from == 'INFO'){ ?>
				{
                	xtype: 'hidden',
                	name: 'from',
                	value: '<?php echo $m_params->from ?>'
                },
                {
                	xtype: 'hidden',
                	name: 'filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                }, {
                	xtype: 'hidden',
                	name: 'tipo',
                	value: 'lista_consegne_programmate_INFO'
                }
<?php } else { ?>
				{
                	xtype: 'hidden',
                	name: 'tipo',
                	value: 'lista_consegne_programmate'
                }	            
<?php } ?>            
            
                , {
                	xtype: 'hidden',
                	name: 'rec_id',
                	value: '<?php echo $da_form->rec_id ?>'
                }, {
                	xtype: 'hidden',
                	name: 'rec_liv',
                	value: '<?php echo $da_form->rec_liv ?>'
                }, {
                	xtype: 'hidden',
                	name: 'tipo_elenco',
                	value: '<?php echo $da_form->tipo_elenco ?>'
                }, {                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 1',
			        items: [{
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa importi',
			                boxLabel: 'Si',
			                name: 'stampa_importi',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_importi',
			                inputValue: 'N'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			        }, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a radio button
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa pallet',
			                boxLabel: 'Si',
			                name: 'stampa_pallet',
			                inputValue: 'Y'
			            }, {			            	
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_pallet',
			                inputValue: 'N'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			        }, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a radio button
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [

			            <?php if ($cfg_mod['gestione_per_riga'] != 'Y') { ?>
			            
				            {
				                fieldLabel: 'Dettaglio ordini',
				                boxLabel: 'Si',
				                name: 'stampa_dettaglio_ordini',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'No',
				                checked: true,			                
				                name: 'stampa_dettaglio_ordini',
				                inputValue: 'N'
				            }
				         <?php } else { ?>
				            {
				                fieldLabel: 'Dettaglia per',
				                boxLabel: 'Ordine',
				                checked: true,
				                name: 'stampa_dettaglio_ordini',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'Riga',			                
				                name: 'stampa_dettaglio_ordini',
				                inputValue: 'R'
				            }				         
				         <?php } ?>  
			            
			            ]
			        }
			        
			        
			        ]			        
			        
			        
			        
			        
			        
			    }  
			    
			    
			    
			    
				,{                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 1',
			        items: [{
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa telefono',
			                boxLabel: 'Si',
			                name: 'stampa_tel',
			                checked: true,			                
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',			                
			                name: 'stampa_tel',
			                inputValue: 'N'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			      	}, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Data',
			                boxLabel: 'Progr.',
			                name: 'data_riferimento',
			                inputValue: 'data_programmata'
			            }, {
			                boxLabel: 'Sped.',
			                checked: true,			                
			                name: 'data_riferimento',
			                inputValue: 'data_spedizione'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			      	}, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Indirizzo',
			                boxLabel: 'Sped.',
			                name: 'indirizzo_di',
			                inputValue: 'ind_spedizione',
			                checked: true
			            }, {
			                boxLabel: 'Dest.',			                
			                name: 'indirizzo_di',
			                inputValue: 'ind_destinazione'
			            }]
			        }]			        
			        
			    }	
			    
			    
			    
			    
			    
			         
			         
			         
			         
			         
			         
			         
				, {                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 1',
			        items: [		
			        
			        
					    {
					            xtype: 'fieldset',
					            flex: 1,
					            title: '',
					            defaultType: 'radio', // each item will be a radio button
					            layout: 'hbox',
					            defaults: {
					                anchor: '100%',
					                hideEmptyLabel: false
					            },
					            items: [
					            
									          
										{
							                fieldLabel: 'Rilevaz.scarico',
							                checked: false,			                
							                boxLabel: 'Si',
							                name: 'stampa_rilevazione_scarico',
							                inputValue: 'Y',
											labelWidth: 90
							            }, {
							                boxLabel: 'No',
							                checked: true,
							                name: 'stampa_rilevazione_scarico',
							                inputValue: 'N',
							                labelWidth: 5
							            }				          
									          
					            
					            ]
					        },			        
			        
			        	    
			    
					    {
					            xtype: 'fieldset',
					            flex: 1,
					            title: '',
					            defaultType: 'radio', // each item will be a radio button
					            layout: 'hbox',
					            defaults: {
					                anchor: '100%',
					                hideEmptyLabel: false
					            },
					            items: [
					            
							            
									 {
							                fieldLabel: 'Km Spedizione',
							                checked: false,			                
							                boxLabel: 'Si',
							                name: 'stampa_km_spedizione',
							                inputValue: 'Y',
											checked: true,
											labelWidth: 90
											
							            }, {
							                boxLabel: 'No',
							                name: 'stampa_km_spedizione',
							                inputValue: 'N',
							                labelWidth: 5
							            }				          
									          
					            
					            ]
					        }, {
					            xtype: 'fieldset',
					            flex: 1,
					            title: '',
					            defaultType: 'radio', // each item will be a radio button
					            layout: 'hbox',
					            defaults: {
					                anchor: '100%',
					                hideEmptyLabel: false
					            },
					            items: [
					            
							            
									 {
							                fieldLabel: 'Data scarico',
							                checked: false,			                
							                boxLabel: 'Si',
							                name: 'stampa_data_scarico',
							                inputValue: 'Y',
											labelWidth: 90
							            }, {
							                boxLabel: 'No',
											checked: true,							                
							                name: 'stampa_data_scarico',
							                inputValue: 'N',
							                labelWidth: 5
							            }				          
									          
					            
					            ]
					        }	
			    	]
			    }				         
			         
			         
			         

	    , {                 
			xtype: 'container',
	        layout: 'hbox',
	        margin: '0 0 1',
	        items: [			    
			    

		<?php if ($m_params->from != 'INFO'){ ?>			    

			    
					    {
					            xtype: 'fieldset',
					            flex: 1,
					            title: '',
					            defaultType: 'radio', // each item will be a radio button
					            layout: 'hbox',
					            defaults: {
					                anchor: '100%',
					                hideEmptyLabel: false
					            },
					            items: [
					            
			            
					            , {
					                fieldLabel: '% magg vol',
					                width: 140,
					                labelWidth: 90,
					                boxLabel: 'Si',
					                xtype: 'numberfield',	
					                name: 'perc_magg_volume',
					                value: '<?php echo $itin->rec_data['TAPMIN'] ?>'
					            }		
					            
					            ]
					  },
					  
			<?php  } ?>					  

					    {
					            xtype: 'fieldset',
					            flex: 1,
					            title: '',
					            defaultType: 'radio', // each item will be a radio button
					            layout: 'hbox',
					            defaults: {
					                anchor: '100%',
					                hideEmptyLabel: false,
					                padding: '1 0 2 0'
					            },
					            items: [
									{
						                fieldLabel: 'Note cli/des',
						                checked: true,			                
						                labelWidth: 90,
						                boxLabel: 'Si',
						                name: 'stampa_segnalazioni_cli_des',
						                inputValue: 'Y'
						            }, {
						                boxLabel: 'No',			                
						                name: 'stampa_segnalazioni_cli_des',
						                inputValue: 'N',
						                width: 60,
						                labelWidth: 10
						            }				          
					            ]
					     }, {
					            xtype: 'fieldset',
					            flex: 1,
					            title: '',
					            defaultType: 'radio', // each item will be a radio button
					            layout: 'hbox',
					            defaults: {
					                anchor: '100%',
					                hideEmptyLabel: false,
					                padding: '1 0 2 0'
					            },
					            items: [	
					            			{
								                fieldLabel: 'Stampa griglia',
								                checked: true,			                
								                labelWidth: 90,
								                boxLabel: 'Si',
								                name: 'stampa_griglia',
								                inputValue: 'Y'
								            }, {
								                boxLabel: 'No',			                
								                name: 'stampa_griglia',
								                inputValue: 'N',
								                width: 60,
								                labelWidth: 10
								            }
					            ]
					     }
					  
					  

					  
					  	
					        
			        
			    	]
			    }
			         
			        
			        
				, {                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 1',
			        items: [		
			        
				    	{
					            xtype: 'fieldset',
					            flex: 1,
					            title: '',
					            defaultType: 'radio', // each item will be a radio button
					            layout: 'hbox',
					            defaults: {
					                anchor: '100%',
					                hideEmptyLabel: false
					            },
					            items: [			        		         
			         
			         
									 	{
											 name: 'f_tipo_ordine',
											 xtype: 'combo',
											 fieldLabel: 'Tipo ord. da incl.',
											 displayField: 'text',
											 labelAlign:'left',
											 width: '225',											 
											 valueField: 'id',
											 emptyText: '- seleziona -',
											 forceSelection:true,
											 allowBlank: true,
									 		 multiSelect: true,														
											 store: {
											 		autoLoad: true,
													editable: false,
													autoDestroy: true,	 
													fields: [{name:'id'}, {name:'text'}],
													data: [
																 <?php echo acs_ar_to_select_json($s->options_select_tipi_ordine('N', $m_sped), '') ?>
																] 
											}							 
										}, {
											 name: 'f_tipo_ordine_da_azzerare',
											 xtype: 'combo',
											 fieldLabel: 'Da azzerare',
											 displayField: 'text',
											 labelAlign:'right',
											 width: '225',											 
											 valueField: 'id',
											 emptyText: '- seleziona -',
											 forceSelection:true,
											 allowBlank: true,
									 		 multiSelect: true,														
											 store: {
											 		autoLoad: true,
													editable: false,
													autoDestroy: true,	 
													fields: [{name:'id'}, {name:'text'}],
													data: [
																 <?php echo acs_ar_to_select_json($s->options_select_tipi_ordine('N', $m_sped), '') ?>
																] 
											}							 
										}
										
										
								]
							}				         
			         
					]
				}			         
			         
			                       

                
            ]
            
           
           , listeners: {
           		afterrender: function(compo){
           			var me = this;

<?php
if (isset($js_parameters->rep_scar_def_id)){
		//a livello di profilo e' stata impostata una memorizzazione.... la imposto e la apro subito
		$f8 = new F8();
		$f8_row = $f8->get_memorizzazione_data_by_id($js_parameters->rep_scar_def_id, (object)array("mod" => $s->get_cod_mod(), "func" => "REPORT_SCARICHI"));
		$f8_dfmemo = $f8_row['DFMEMO']; 
?>

    		memorizzazione = Ext.decode('<?php echo $f8_dfmemo?>'.replace(/\\"/g, '"'));
    			  	
    	  	p_ar = new Array();
    	  	Ext.Object.each(memorizzazione, function(key, value) {
    	  		p_ar[key] = value;
    	  	});
    
    		//DRY: E' QUELLO CHE FACCIO IN DOPPIO CLICK SU RICHIAMA!!!
    	  	//per tutti gli input della form
    	  	me.getForm().getFields().items.forEach(function(n) {
    	  	  if (n.xtype == 'datefield'){
    	  	    if (!Ext.isEmpty(p_ar[n.name]) && p_ar[n.name].length > 0)
    	  	  		n.setValue(acs_date_from_AS(p_ar[n.name], 'd/m/Y'));
    	  	  	else
    	  	  		n.setValue(null);	
    	  	  } else if (n.xtype == 'hidden') {
    	  		//non ricompilo i campi hidden
    	  	  } else {
    	  		n.setValue(p_ar[n.name]);
    	  	  }	
    	  	});
		
				var b_visualizza = me.down('#b_visualizza');
				b_visualizza.fireHandler();
				me.up('window').hide();
<?php } ?>				
           	}  //afterrender
           		
           } //listeners
           
        } //form

	
]}