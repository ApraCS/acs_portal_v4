<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();
$main_module =  new Spedizioni();
$cfg_mod = $main_module->get_cfg_mod();


$m_table_config = array(
    'module'      => $main_module,
    't_panel' =>  "EMPRF - Profili email",
    'tab_name' =>  $cfg_mod['file_tabelle'],
    'descrizione' => "Gestione tabella profili email",
    'form_title' => "Dettagli tabella profili email",
    'fields_preset' => array(
        'TATAID' => 'EMPRF'
    ),
    
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TAINDI', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TAINDI', 'TARIF1', 'TARIF2', 'TATELE', 'TALOCA'),
    
    'fields' => array(				
        'TAKEY1' => array('label'	=> 'Codice',  'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
		    'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
            'TAMAIL' => array('label'	=> 'Note',  'maxLength' => 100),
            'TAINDI' => array('label'	=> 'Indirizzo server',  'maxLength' => 60),
            'TARIF1' => array('label'	=> 'Username',  'maxLength' => 20),
            'TARIF2' => array('label'	=> 'Password',  'maxLength' => 20),
            'TATELE' => array('label'	=> 'Cartella',  'maxLength' => 20),
            'TALOCA' => array('label'	=> 'Cartella importati',  'maxLength' => 20),
            //immissione
            'immissione' => array(
                'type' => 'immissione', 'fw'=>'width: 70',
                'config' => array(
                    'data_gen'   => 'TADTGE',
                    'user_gen'   => 'TAUSGE'
                )
            
            ),
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione'),
		)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
