<?php

require_once("../../config.inc.php");
require_once("acs_panel_todolist_data.php");

$main_module = new Spedizioni();
$s			 = new Spedizioni();

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// AGGIORNAMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_upd'){
   
    $ar_upd_1 = array();
    if(strlen(trim($m_params->form_values->f_utente_assegnato)) > 0)
        $ar_upd_1['ASUSAT'] = $m_params->form_values->f_utente_assegnato;
    if (strlen(trim($m_params->form_values->f_scadenza)) > 0)
        $ar_upd_1['ASDTSC'] = (int)$m_params->form_values->f_scadenza;
    if (strlen(trim($m_params->form_values->f_note)) > 0)
        $ar_upd_1['ASNOTE'] = $m_params->form_values->f_note;
       
    foreach($m_params->list_selected_id as $v){
        
        if (count($ar_upd_1) > 0){
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_assegna_ord']} AO
                    SET  " . create_name_field_by_ar_UPDATE($ar_upd_1) . "
                    WHERE ASIDPR = {$v}";                
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd_1);
            echo db2_stmt_errormsg($stmt);
        }
        
        //salvo eventuali note memo
        if (strlen(trim($m_params->form_values->f_memo)) > 0){
            
            $na = new SpedAssegnazioneOrdini($main_module);
            $memo = $na->get_memo($v);
            
            if(strlen($memo) > 0){
                
                $ar_upd = array();
                $ar_upd['NTMEMO'] = trim($m_params->form_values->f_memo);
                
                $sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} NT
                SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE NTDT = '{$id_ditta_default}' AND NTKEY1 = '{$v}'
                AND NTSEQU = 0 AND NTTPNO = 'ASMEM'";
                
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_upd);
                echo db2_stmt_errormsg($stmt);
            }else {
                
                $sqlMemo = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(NTDT, NTMEMO, NTKEY1, NTSEQU, NTTPNO)
                VALUES(?, ?, ?, 0, 'ASMEM')";
                
                $stmtMemo = db2_prepare($conn, $sqlMemo);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmtMemo, array($id_ditta_default, trim($m_params->form_values->f_memo), $v));
                
            }
            
        }
        
    }
    
   
    
    echo acs_je(array('success' => true));
    exit;
    
}


// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
	?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            autoScroll: true,
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
            items: [
            
            		{
                	xtype: 'hidden',
                	name: 'tipo_op',
                	value: ''
                	}, 
            
            {
					            xtype: 'combo',
								name: 'f_cliente_cod',
								fieldLabel: 'Cliente',
								minChars: 2,			
								//allowBlank: false,            
					            store: {
					            	pageSize: 1000,            	
									proxy: {
							            type: 'ajax',
							            url : 'acs_get_select_json.php?select=search_cli_anag',
							            reader: {
							                type: 'json',
							                root: 'root',
							                totalProperty: 'totalCount'
							            }
							        },       
									fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
					            },
					                        
								valueField: 'cod',                        
					            displayField: 'descr',
					            typeAhead: false,
					            hideTrigger: true,
					            anchor: '100%',
					            
					            
					            listConfig: {
					                loadingText: 'Searching...',
					                emptyText: 'Nessun cliente trovato',
					                
					                // Custom rendering template for each item
					                getInnerTpl: function() {
					                    return '<div class=\"search-item\">' +
					                        '<h3><span>{descr}</span></h3>' +
					                        '[{cod}] {out_loc}' +
					                    '</div>';
					                }                
					                
					            },
					            
					            pageSize: 1000
					        }					        
					        
						,{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {
							flex: 1,							
							name: 'f_agente', anchor: '-15',
							xtype: 'combo',
							fieldLabel: 'Agente',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_agenti(), '') ?> 	
								    ] 
								}						 
							}
												
						]							
						}	
		                , {
                				name: 'f_tipo_ordine',
                				xtype: 'combo',
                            	anchor: '100%',
                				fieldLabel: 'Tipo ordine',
                				displayField: 'text',
                				valueField: 'id',
                				emptyText: '- seleziona -',
                				forceSelection: true,
                			    allowBlank: true,
                					     		
                				store: {
                					autoLoad: true,
                					editable: false,
                					autoDestroy: true,
                				    fields: [{name:'id'}, {name:'text'}],
                				    data: [
                                              <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, null, null, null, 0, '', 'Y'), ''); ?>
                					    ]
                				}
                			}							
							
							
							
							
						, {
							flex: 1,							
							name: 'f_stato_ordine', anchor: '100%',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							}							

							
							
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
        					{
        			            xtype: 'combo',
        			            anchor: '-15', flex: 1,
        			            store: Ext.create('Ext.data.ArrayStore', {
        			                fields: [ 'cod', 'descr' ],
        			                data: <?php
        			                    $user_to = array();
                			            $users = new Users;                			           
                			            $ar_users = $users->find_all();
                			            foreach ($ar_users as $ku=>$u)
                			                $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
                			            echo acs_je($user_to); ?>
        			            }),
        			            displayField: 'descr',
        			            valueField: 'cod',
        			            fieldLabel: 'Utente assegnato',
        			            queryMode: 'local',
        			            selectOnTab: false,
        			            name: 'f_utente_assegnato',
        			            allowBlank: true,
        						forceSelection: true,			            
        			            value: '<?php echo $v_utente_assegnato; ?>'
        			        }
						  ]							
						}							
							
							
							
					, { 
						flex: 1,
						xtype: 'fieldcontainer',
						layout: 'hbox',						
						items: [						
								{						
									name: 'f_num_ordine',
									xtype: 'textfield',
									fieldLabel: 'Nr. Ordine',
									value: '',
									flex: 1			
								 }, {						
									name: 'f_riferimento',
									xtype: 'textfield',
									fieldLabel: 'Riferimento', labelAlign: 'right',
									value: '',
									flex: 1,					
								 }					
				
						]						  						  
					}							
							
							
							
							
						, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [							
							
						{
						     name: 'f_data_prod_da'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data prod/dispon'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}, {
						     name: 'f_data_prod_a'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: ' al'
						   , labelAlign: 'right'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}
						
						]
						}
					
					
						, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [							
							
						{
						     name: 'f_data_reg_da'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data registr.'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}, {
						     name: 'f_data_reg_a'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: ' al'
						   , labelAlign: 'right'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}
						
						]
						}						
						
						
						
						
					       
					       
					       
					       
					  // ------------------------- ALTRI FILTRI -------------------------     
						,{
				            xtype:'fieldset',
				            title: 'Altri filtri',
				            collapsible: true,
				            collapsed: true, // fieldset initially collapsed	            
				            defaultType: 'textfield',
				            layout: 'anchor',
				            defaults: {
				                anchor: '100%'
				            },
				            items :[
					        					        
						  {
							flex: 1,
							name: 'f_area', anchor: '100%',
							xtype: 'combo',
							fieldLabel: 'Area spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
								}						 
							}

						, {
							flex: 1,
							name: 'f_itinerario',
							xtype: 'combo', anchor: '100%',
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
								    ] 
								}						 
							}				            
				            
						, {
							flex: 1,							
							name: 'f_referente_cliente',
							xtype: 'combo',
							fieldLabel: 'Referente cli.',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_referente_cliente(), '') ?> 	
								    ] 
								}						 
							}
			            
						, {
		                    xtype: 'checkboxgroup',
		                    layout: 'hbox',
		                    fieldLabel: 'Solo stadio',
		                    flex: 3,
		                    defaults: {
		                     padding: '0 10 10 0'
		                    },
		                    items: [{
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Ice'
				                  , inputValue: '1'
				                  , width: 80                
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Slush'
				                  , inputValue: '2'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Water'
				                  , inputValue: '3'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Altro'
				                  , inputValue: '0'
				                  , width: 80                        
				                 }
		                   ]
		                }					           
				            
				            ]
				          }					       
					    // FINE ALTRI FILTRI   
					 
					        
				],
	
			buttons: [
			
					
					{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{ xtype: 'tbfill' },
						{xtype : 'button',
	            		text: 'Showroom',
	            		width : 140,
	            		iconCls: 'icon-windows-32',
	            		scale: 'large',
	            		handler: function() {
	            			var form = this.up('form').getForm();
	            			form.findField('tipo_op').setValue('EXPO1');
	            			acs_show_panel_std('acs_panel_todolist.php?tab_id=panel-todo-EXPO1&tab_title=Showroom', 'panel-todo-EXPO1', {form_open: form.getValues()});
		            		this.up('window').close();            	                	                
        	            		}
        	          }, {xtype : 'button',
        	            text: 'Report showroom',
        	            iconCls: 'icon-print-32',
        	            width : 140,
        	            scale: 'large',
        	            handler: function() {
        	            	form = this.up('form').getForm();
        	            	form.findField('tipo_op').setValue('EXPO1');
                            this.up('form').submit({
                            url: 'acs_panel_expo_report.php',
                            target: '_blank', 
                            standardSubmit: true,
                            method: 'POST',                        
                            params: {
                                form_values: Ext.encode(form.getValues())
        				    }
                  			}); 
        	            	            	                	                
        	            }
        	         }
						   
						]},
						
							
			{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "ESPO_PAN"); ?> 
						
						{
						xtype : 'button',
						margin : '0 0 0 200',
        	            text: 'Showroom planning',
        	            width : 140,
        	            iconCls: 'icon-delivery-32',
        	            scale: 'large',
        	            handler: function() {
        	            	var form = this.up('form').getForm();
        	            	form.findField('tipo_op').setValue('EXPO2');
        	            	acs_show_panel_std('acs_panel_todolist.php?tab_id=panel-todo-EXPO2&tab_title=Showroom planning', 'panel-todo-EXPO2', {form_open: form.getValues()});
        		            this.up('window').close();            	                	                
        	            }
        	          }, {
        	            xtype : 'button',
        	            text: 'Report planning',
        	            iconCls: 'icon-print-32',
        	            scale: 'large',
        	            width : 140,
        	            handler: function() {
        	            	form = this.up('form').getForm();
        	            	form.findField('tipo_op').setValue('EXPO2');
                            this.up('form').submit({
                            url: 'acs_panel_expo_report.php',
                            target: '_blank', 
                            standardSubmit: true,
                            method: 'POST',                        
                            params: {
                                form_values: Ext.encode(form.getValues())
        				    }
                  			}); 
        	            	            	                	                
	            }
	        } 
						   
						]}
						
						
						   
						]}
					
					
					
					
					
					
					
					
					
					
				
		
			
			
			
			
			
	        
	        ],
				
        }
]}
<?php	
	exit;
} //open_form_filtri


// ******************************************************************************************
// FORM richiesta parametro
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_cliente'){
	?>
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            items: [        
							{
					            xtype: 'combo',
								name: 'f_cliente_cod',
								fieldLabel: 'Cliente',
								minChars: 2,			
								allowBlank: false,            
					            store: {
					            	pageSize: 1000,            	
									proxy: {
							            type: 'ajax',
							            url : 'acs_get_select_json.php?select=search_cli_anag',
							            reader: {
							                type: 'json',
							                root: 'root',
							                totalProperty: 'totalCount'
							            }
							        },       
									fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
					            },
					                        
								valueField: 'cod',                        
					            displayField: 'descr',
					            typeAhead: false,
					            hideTrigger: true,
					            anchor: '100%',
					            
					            
					            listConfig: {
					                loadingText: 'Searching...',
					                emptyText: 'Nessun cliente trovato',
					                
					                // Custom rendering template for each item
					                getInnerTpl: function() {
					                    return '<div class=\"search-item\">' +
					                        '<h3><span>{descr}</span></h3>' +
					                        '[{cod}] {out_loc}' +
					                    '</div>';
					                }                
					                
					            },
					            
					            pageSize: 1000
					        }, 					
						        
						        
					],
				buttons: [{
					            text: 'Visualizza',
						        iconCls: 'icon-folder_search-24',		            
						        scale: 'medium',		            
					            handler: function() {
						            var form = this.up('form').getForm();
						            var desc_cli = form._fields.items[0].rawValue;
						             acs_show_win_std('Ricerca ordine origine - ' + desc_cli, 'acs_modifica_ordine_origine.php?fn=ricerca_ordini_grid', {
			        				form_values: form.getValues()}, 800, 450, null, 'icon-folder_search-16');
					                
					            }
					        }]             
					
	        }
	]}	
	<?php	
		exit;
	} 

	
// ******************************************************************************************
// FORM MODIFICA TODO
// ******************************************************************************************
	if ($_REQUEST['fn'] == 'open_mod'){

   //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    //default
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
                    
    $ar_users_json = acs_je($user_to);
    
    if(count($m_params->list_selected_id) == 1){
        
        $sql = "SELECT AO.*, NT_MEMO.NTMEMO AS MEMO
        FROM {$cfg_mod_Spedizioni['file_assegna_ord']} AO
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
        ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = AO.ASIDPR AND NT_MEMO.NTSEQU=0
        WHERE ASIDPR = '{$m_params->list_selected_id[0]}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
    }
    
   
    $attav = $s->get_TA_std('ATTAV', 'EXPO2');
    $flag_rife = trim($attav['TAFG03']);
    
    
   
    ?>
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            flex : 1,
	            layout: 'anchor',
	            items: [        
						{
			            xtype: 'combo',
			            flex : 1,
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            fieldLabel: 'Utente assegnato',
			            labelWidth : 110,
			            queryMode: 'local',
			            selectOnTab: false,
			            name: 'f_utente_assegnato',
			            allowBlank: true,
						forceSelection: true,
						anchor: '-5',		
						queryMode: 'local',
     					minChars: 1,	            
			            value: <?php echo j(trim($row['ASUSAT'])); ?>,
			            listeners: { 
			 				beforequery: function (record) {
    	         				record.query = new RegExp(record.query, 'i');
    	         				record.forceAll = true;
    				 		}
 						}
			        	}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Inizio lavori'
					   , labelWidth : 110
					   , name: 'f_scadenza'
					   , format: 'd/m/Y'
					   , minValue: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'	
					   , value : '<?php echo print_date($row['ASDTSC'], "%d/%m/%Y"); ?>'							   						   
					   , submitFormat: 'Ymd'
					   , allowBlank: true
					   , anchor: '-5'
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   
					}, 
					
					<?php  if(in_array($flag_rife, array('A', 'C'))){?>
					 {
					    xtype: 'fieldcontainer',
						flex: 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
        					 {
        					xtype : 'combo',
                		 	name: 'f_note', 
                		 	fieldLabel: 'Installatore',
                		 	labelWidth : 110,
                			displayField: 'text',
                		    valueField: 'text',	
                		    anchor: '-5',	
                		    flex: 1,
                		    <?php if($flag_rife == 'C'){?>
                		    	editable: false,
                      		<?php } ?>			
                			emptyText: '- seleziona -',
                		   	queryMode: 'local',
                     		minChars: 1,	
                     		value : <?php echo j(trim($row['ASNOTE'])); ?>,
                			store: {
        			
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    		    data: [	<?php echo acs_ar_to_select_json($main_module->find_TA_std('TODRF', 'EXPO2'), ''); ?>]
                			}
                			
                			,listeners: { 
        			 			beforequery: function (record) {
            	         			record.query = new RegExp(record.query, 'i');
            	         			record.forceAll = true;
            				 }
                    		}	
        		 	
        		 			 },
        		 			 
        		 			 {
        						 xtype: 'button',
        						 margin: '0 5 0 5',
        			             scale: 'small',			                 
        			             iconCls: 'icon-gear-16',
        			             iconAlign: 'top',		
        			             anchor: '-5',		                
        			             handler : function() {
        									acs_show_win_std('Gestione installatori', 
										   'acs_gest_TODRF.php?fn=open_tab', {}, 950, 500, null, 'icon-gear-16');
        								} //handler function()
        						 
        						 }
							]},
					<?php }else{?>
					  {
						name: 'f_note',
						labelWidth : 110,
						xtype: 'textfield',
						fieldLabel: 'Installatore',
					    maxLength: 100, 
					    anchor: '-5',
					    value : <?php echo j(trim($row['ASNOTE'])); ?>							
					  },
					<?php }?>
					
					
				
					{
						name: 'f_memo',
						xtype: 'textarea',
						height : 50,
						labelWidth : 110,
						fieldLabel: 'Memo',
					    maxLength: 100, 
					    anchor: '-5',
					    value : <?php echo j($row['MEMO']); ?>							
					} 		
						 		
						        
						        
					],
				buttons: [{
					            text: 'Conferma',
						        iconCls: 'icon-button_blue_play-24',		            
						        scale: 'medium',		            
					            handler: function() {
						            var form = this.up('form').getForm();
						            var loc_win = this.up('window');
						            Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upd',
							        jsonData: {
							           form_values: form.getValues(),
							           list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>						        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							          var jsonData = Ext.decode(result.responseText);
						              loc_win.fireEvent('afterOkSave', loc_win);	           
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
						            
						           
					                
					            }
					        }]             
					
	        }
	]}	
	<?php	
		exit;
	}
		
// ******************************************************************************************
// ALLESTIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_genera_allestimento'){
    
 
    foreach ($m_params->list_selected_id as $ordine){
        
        $sh = new SpedHistory();
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'GEN_ALLESTIMENTO',
                "k_ordine"	=> $ordine,
                "vals" => array(
                    "RIDART" =>$m_params->form_values->f_riferimento,
                    "RICITI" =>$m_params->form_values->f_punto_vendita,
                    "RIDTVA" =>$m_params->form_values->f_data
                )
            )
            );
        
    } // per ogni ordine selezionato
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}
	
if ($_REQUEST['fn'] == 'open_form_al'){

    $list_selected_id = $m_params->list_selected_id;
    
    //Dal primo ordine recupero il cliente
    $ord = $s->get_ordine_by_k_docu($list_selected_id[0]);
    $cod_cli = $ord['TDCCON'];
    
    ?>
  
  {"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            items: [
            
            {
			   xtype: 'datefield'
			   , startDay: 1 //lun.
			   , fieldLabel: 'Data registrazione'
			   , labelWidth : 110
			   , name: 'f_data'
			   , format: 'd/m/Y'
			   , value : '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'							   						   
			   , submitFormat: 'Ymd'
			   , allowBlank: true
			   , anchor: '-5'
			   , listeners: {
			       invalid: function (field, msg) {
			       Ext.Msg.alert('', msg);}
				}
			   
			},
            {						
				name: 'f_riferimento',
				xtype: 'textfield',
				fieldLabel: 'Riferimento',
				flex: 1,
				anchor: '-5',
				labelWidth : 110					
			 },
			      	 {
			xtype: 'fieldcontainer',
			flex : 1,
			anchor: '-10',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_punto_vendita',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'pven',
				    fieldLabel: 'Punto vendita',
         			labelWidth : 110,	
                    anchor: '-5',
                 	flex : 1,
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_punto_vendita').setValue(record_selected.cod);	
												form.findField('pven').setValue(record_selected.denom);
                                                from_win.close();
												}										    
									};
									;
							  
									acs_show_win_std('Seleziona punto vendita', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : <?php echo j($cod_cli); ?>, type : 'P'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
        							 }
        						}
        				}
        				
        				]
        		},
            ],
            	buttons: [{
		            text: 'Conferma',
			        iconCls: 'icon-button_blue_play-24',		            
			        scale: 'medium',		            
		            handler: function() {
			            var form = this.up('form').getForm();
			            var loc_win = this.up('window');
			            Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_allestimento',
				        jsonData: {
				           form_values: form.getValues(),
				           list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>						        
				        },
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				          var jsonData = Ext.decode(result.responseText);
			              loc_win.fireEvent('afterOkSave', loc_win);	           
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });
			            
			           
		                
		            }
		        }] }
            
            
            
        ]}  
    
<?php 
  }
  
  
// ******************************************************************************************
// EDIT EVENT
// ******************************************************************************************
  if ($_REQUEST['fn'] == 'exe_assegna_allestimento'){
      
      $k_ordine_contract = $m_params->form_values->f_k_ordine_contract;
      
      // $oe_contract = $s->k_ordine_td_decode_xx($k_ordine_contract);
      
      foreach ($m_params->list_selected_id as $ordine){
          
          $sh = new SpedHistory();
          $sh->crea(
              'pers',
              array(
                  "messaggio"	=> 'ASS_ALLESTIMENTO',
                  "k_ordine"	=> $ordine,
                  "vals" => array(
                      "RIDART" =>$k_ordine_contract
                  )
              )
              );
          
      } // per ogni ordine selezionato
      
      $ret = array();
      $ret['success'] = true;
      echo acs_je($ret);
      exit;
  }
// ******************************************************************************************
// FORM APERTURA
// ******************************************************************************************
  if ($_REQUEST['fn'] == 'open_assegna'){
      //recupero l'elenco degli ordini interessati
      $list_selected_id = $m_params->list_selected_id;
      
      //Dal primo ordine recupero il cliente
      $ord = $s->get_ordine_by_k_docu($list_selected_id[0]);
      $cod_cli = $ord['TDCCON'];
      ?>
{"success":true, "items": [    
    {
    	xtype: 'form',
    	bodyStyle: 'padding: 10px',
    	bodyPadding: '5 5 0',
    	frame: true,
    	title: '',    
    	defaults:{ anchor: '-10' , labelWidth: 130 },    
    	
    	items: [
    		{
    			name: 'f_k_ordine_contract',						
    			xtype: 'textfield',
    			fieldLabel: 'Ordine contract',
    		    maxLength: 100,
    		    allowBlank: false,
    		    readOnly: true					
    		}    				
    	],
    	
    	buttons: [
    				{
		         		text: 'Ricerca ordine', 
		         		itemId: 'ricerca_ordine',
		        		iconCls: 'icon-folder_search-32', scale: 'large',	   	 	
		        		handler: function () {
		        		
    		        		var m_form = this.up('form');
    		        		
    		        		var my_listeners = {
    	    		  			afterOkSave: function(from_win, k_ordine){
            						from_win.close();
            						m_form.getForm().findField('f_k_ordine_contract').setValue(k_ordine);
            						var b_conferma = m_form.down('#b_conferma');
    	 			                b_conferma.fireHandler();
    				        		}
    		    				};	
    		        		
    				         acs_show_win_std('Ricerca su archivio ordini', 
    				         	'../base/acs_seleziona_ordini_gest.php?fn=open', {
    				         		cod_cli: <?php echo j($cod_cli) ?>,
    				         		doc_gest_search: 'ALLESTIMENTO_abbina',
    				         		show_parameters: false,
    				         		auto_load: true
    				         	}, 800, 550,  my_listeners, 'icon-clessidra-16');	
			           }
		    		}, '->',
    			  {
    	            text: 'Conferma',
    	            iconCls: 'icon-save-32', scale: 'large',
    	            itemId: 'b_conferma',	
    	            handler: function() {
    	            	var form = this.up('form').getForm();
    	            	var loc_win = this.up('window')
    
    					if(form.isValid()){
    					    Ext.getBody().mask('Loading... ', 'loading').show();
    						Ext.Ajax.request({
    						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_allestimento',
    						        jsonData: {
    						        	form_values: form.getValues(),
    						        	list_selected_id: <?php echo acs_je($list_selected_id); ?>    						        	
    						        },
    						        method     : 'POST',
    						        waitMsg    : 'Data loading',
    						        success : function(result, request){	
    						            Ext.getBody().unmask();            	  													        
    						            var jsonData = Ext.decode(result.responseText);
    						            if (jsonData.success == true){
    						            	loc_win.fireEvent('afterOkSave', loc_win);
    						            } else {
    						            	Ext.Msg.alert('Message', 'Error');
    						            }	
    						        },
    						        failure    : function(result, request){
    						            Ext.getBody().unmask();
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
    						    });	    
    				    }            	                	                
    	            }
    	        }],  
    	        
    	 	listeners: {
    	 		afterrender: function(comp){
    	 			//in automatico apro la form di ricerca ordine contract
    	 			var btn_ricerca_ordine = comp.down('#ricerca_ordine');
    	 			btn_ricerca_ordine.fireHandler();
    	 		}
    	 	}                  
    				
            } //form
    ]}
<?php exit; } ?>