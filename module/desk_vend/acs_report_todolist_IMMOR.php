<?php
require_once "../../config.inc.php";
set_time_limit(240);
$main_module = new Spedizioni();
$s = new Spedizioni();


$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");


$ar_email_json = acs_je($ar_email_to);

// ******************************************************************************************
// FORM richiesta parametro
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
?>	
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
            
            items: [        
						  {
							flex: 1,
							name: 'f_area',
							xtype: 'combo',
							fieldLabel: 'Area spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
								}						 
							}					        
					        
							
						 , {
							flex: 1,
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}							
					        
					        
				],
			buttons: [{
				            text: 'Visualizza',
					        iconCls: 'icon-folder_search-24',		            
					        scale: 'medium',		            
				            handler: function() {
				                this.up('form').submit({
			                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
			                        target: '_blank', 
			                        standardSubmit: true,
			                        method: 'POST'
			                  });
				                
				            }
				        }]             
				
        }
]}	
<?php	
	exit;
} //get_json_data 
?>
<?php
// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
	require_once("acs_panel_todolist_data.php");
	
	function m_compila_tab_ordini($ar, $row){
		
		switch (trim($row['TDCLOR'])){
			case 'O':
				$tpord = 'O';
				break;
			case 'M':
				$tpord = 'M';
				break;
			case 'P':
				$tpord = 'P';
				break;
			case 'A':
				$tpord = 'A';
				break;				
			default:
				$tpord = 'CV';
		}		
		
		$stato = $row['TDSTAT'];
		$ar[$tpord][$stato] += 1;
		$ar['STATI'][$stato] += 1;
		
		return $ar;
	}	
	
?>
<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
div#my_content h1{font-size: 22px; padding: 5px;}
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table.acs_report{border-collapse:collapse; width: 95%; margin-left: 20px; margin-right: 20px;}
table.acs_report td, table.acs_report th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{color: white; background-color: #000000; padding: 5px; margin-top: 10px; margin-bottom: 10px;}
table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}

table.acs_report.tot_aspe td{background-color: #e0e0e0;}
table.acs_report.tot_gen td{background-color: #e0e0e0;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<div id='my_content'>

<h1>Riepilogo ordini in fase di immissione/manutenzione per area di spedizione/utente</h1>
<?php
 //Recupero dei dati
 $livs['0'] = 'IMMOR'; 
 $filtro = imposta_filtri($livs);
  
 if (strlen(trim($_REQUEST['f_area'])) > 0)
 	$filtro['TAASPE'] = $_REQUEST['f_area'];
 if (strlen(trim($_REQUEST['f_divisione'])) > 0)
 	$filtro['TDCDIV'] = $_REQUEST['f_divisione'];
  
 $items 	= get_elenco_ordini_arrivi($filtro, " SEQST, TA_ITIN.TAASPE, ASUSAT ");

 $ar = array();
 $ar_tot['TOTALI']['tab_ord']["STATI"] = array();
 $ar_tot['TOTALI']['tab_ord']["O"] = array();
 $ar_tot['TOTALI']['tab_ord']["M"] = array();
 $ar_tot['TOTALI']['tab_ord']["P"] = array();
 $ar_tot['TOTALI']['tab_ord']["A"] = array();
 $ar_tot['TOTALI']['tab_ord']["CV"] = array();
 
 while ($row = db2_fetch_assoc($items)) {
 	
 	$s_ar = &$ar; 	

 	//stacco per utente/area spedizione
 	$r_livs['liv1_v'] = trim($row['TAASPE']);	//UTENTE ASSEGNATO / AREA SPEDIZIONE 	
 	$r_livs['liv2_v'] = trim($row['ASUSAT']);	//UTENTE ASSEGNATO / AREA SPEDIZIONE
 	
 	//liv1 (area)
	$liv = $r_livs['liv1_v']; 
 	if (is_null($s_ar[$liv])){
 		$s_ar[$liv]["task"] = $main_module->decod_std('ASPE', $row['TAASPE']);
 		$s_ar[$liv]["tab_ord"]["O"] = array();
 		$s_ar[$liv]["tab_ord"]["M"] = array();
 		$s_ar[$liv]["tab_ord"]["P"] = array();
 		$s_ar[$liv]["tab_ord"]["A"] = array();
 		$s_ar[$liv]["tab_ord"]["CV"] = array();
 	} 	
 	$s_ar[$liv]['tab_ord'] = m_compila_tab_ordini($s_ar[$liv]['tab_ord'], $row);
 	$s_ar = &$s_ar[$liv]['children'];

 	//liv2 (utente)
 	$liv = $r_livs['liv2_v'];
 	if (is_null($s_ar[$liv])){
 		$s_ar[$liv]["task"] = $liv;
 		$s_ar[$liv]["tab_ord"]["O"] = array();
 		$s_ar[$liv]["tab_ord"]["M"] = array();
 		$s_ar[$liv]["tab_ord"]["P"] = array();
 		$s_ar[$liv]["tab_ord"]["A"] = array();
 		$s_ar[$liv]["tab_ord"]["CV"] = array();
 	}
 	$s_ar[$liv]['tab_ord'] = m_compila_tab_ordini($s_ar[$liv]['tab_ord'], $row);
 	$s_ar = &$s_ar[$liv]['children'];
 	
 	//generali
 	$ar_tot['TOTALI']['tab_ord'] = m_compila_tab_ordini($ar_tot['TOTALI']['tab_ord'], $row);
 	$tab_stati[$row['TDSTAT']] = $row['TDDSST']; 	
 	
 }	 

 
 
//stampo il report
function cmp_by_tipo_ord($a, $b){
	return strcmp($a['carico_lotto'], $b['carico_lotto']); 	
}
 
 
function stampa_tab($ar, $title_cell = '', $table_class = ''){
	global $ar_tot;
?>
	<TABLE border=1 class='acs_report <?php echo $table_class; ?>'>
	
	<!--  intestazione stati -->
	<?php
 	 //calcolo rowspan (quanti tipi hordini ha)
 	 $c_rowspan = 0;	
	 foreach($ar['tab_ord'] as $k_tipo => $r){
	 	if ($k_tipo != 'STATI' && (int)$ar['tab_ord'][$k_tipo] > 0)
	 		$c_rowspan++;
	 }	
	?>	
	
	<TR>
	 <!--  <Th rowspan=<?php echo $c_rowspan; ?>><h2><?php echo $title_cell; ?></h2></Th> -->
	 <th width="20%">Utente</th>
	 <th width="80%">Denominazione</th>
	 <Th width=30>Tipologia</Th>
			<?php foreach($ar_tot['TOTALI']['tab_ord']['STATI'] as $k_stato => $r){ ?>
				 <TH width=50 style="text-align: center;"><?php echo $k_stato; ?></TH>
			<?php } ?>
				 <TH width=50 style="text-align: center;">TOT</TH>	  
	</TR>
			 
			 
			<?php $n_riga = 0; ?> 
			<!--  righe tab -->
			<?php
			 //ordini per O, M, P, A, CV
			?>			
			<?php foreach($ar['tab_ord'] as $k_tipo => $r){ ?>
			 <?php if($k_tipo == 'STATI' || (int)$ar['tab_ord'][$k_tipo] == 0) continue; ?>
			 	
			 <TR>
			 
			 <?php $n_riga++; ?>
			 <?php if ($n_riga == 1 && ($table_class == 'tot_aspe' || $table_class == 'tot_gen' )){ ?>
				<Th colspan=2 rowspan=<?php echo $c_rowspan; ?>><h2><?php echo $title_cell; ?></h2></Th>			 
			 <?php } else if ($n_riga == 1) {
			 	$user = new Users();
			 	$user->get_by_cod($title_cell);
			 	?>
				<Th rowspan=<?php echo $c_rowspan; ?>><h2><?php echo $title_cell; ?></h2></Th>			 
				<Th rowspan=<?php echo $c_rowspan; ?>><h2><?php echo trim($user->rec_data['UTDESC']); ?></h2></Th>			 
			 <?php }?>
			 
			 
			  <TD align=center><?php echo $k_tipo; ?></TD>
			    <?php $s_row = 0;?>
				<?php foreach($ar_tot['TOTALI']['tab_ord']['STATI'] as $k_stato => $n){?>
				 <?php $s_row += $ar['tab_ord'][$k_tipo][$k_stato]; ?>
				 <TD width=50 class=number><?php echo $ar['tab_ord'][$k_tipo][$k_stato]; ?></TD>
				<?php } ?>
			   <TD width=50 class=number><?php echo $s_row; ?></TD>  
			 </TR>
			<?php } ?>
			 
			</TABLE>
		
<?php		
} 
 
 
foreach($ar as $_aspe => $ar_aspe){
	echo "<br/>&nbsp;<h1>" . $ar_aspe['task'] . "</h1>";
	
	foreach($ar_aspe['children'] as $k_ute_aspe => $ar_ute_aspe){
		stampa_tab($ar_ute_aspe, "{$ar_ute_aspe['task']}");
		echo "<br/>";	
	}
	
	//tot per area spedizione	
	stampa_tab($ar_aspe, "Totali per area di spedizione " . $ar_aspe['task'], 'tot_aspe');	
	
}	


//totale generale
echo "<br/><h1>Totali generali</h1>";
stampa_tab($ar_tot['TOTALI'], 'Totali generali', 'tot_gen');
 
?>

<br/>&nbsp;<br/>
<div style="width: 100%;">
<div style="width: 50%; float: left;">
<h1>Legenda stati</h1>
 <table class='acs_report'> 	
 	<?php foreach($ar_tot['TOTALI']['tab_ord']['STATI'] as $k_stato => $r){ ?>
	 <tr>
	  <td width=20><?php echo $k_stato; ?></td>
	  <td><?php echo $tab_stati[$k_stato]; ?></td>
	 </tr>
 	<?php } ?>
</table>
</div>

<div style="width: 50%; float: left;">
<h1>Legenda tipologie</h1>
 <table class='acs_report'> 	
	<?php foreach($ar_tot['TOTALI']['tab_ord'] as $k_tipo => $r){ ?>
	 <?php if($k_tipo == 'STATI') continue; ?>
	 <tr>
	  <td width=20><?php echo $k_tipo; ?></td>
	  <td>
	   
	   <?php
	    $ar_k_tipologie = array();
	   	foreach(str_split($k_tipo) as $k_tipo_c => $rc)
	   		$ar_k_tipologie[] = $main_module->decod_std('TIPOV', $rc);
	   	echo implode(", ", $ar_k_tipologie);	
	   ?>
	   	
	  </td>
	 </tr>
 	<?php } ?>
</table>
</div>
</div>

 </div>
</body>
</html> 
<?php
 exit;	
}
?>