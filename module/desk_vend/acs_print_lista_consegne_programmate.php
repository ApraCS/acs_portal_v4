<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();




function cmp_by_sped_princ_collegata($a, $b)
{
	global $spedizione;
	if ($a['CSPROG'] == $spedizione) return 1; //principale
	return 2; //collegata
}


//recupero settimana spedizione (da testate)
//$week = strftime("%V", strtotime($m_data));
//$year = strftime("%G", strtotime($m_data));

$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


 $sottostringhe = explode("|", $_REQUEST['rec_id']);
 
 switch ($_REQUEST['rec_liv']){
 	case "liv_0": 
		$m_sped = $sottostringhe[2];
 		$spedizione = $s->get_spedizione($m_sped);
 		
 		//se e' impostata aggiungo in stampa anche la sped collegata
 		if ($spedizione['CSNSPC'] > 0)
 			$m_sped_print = implode(',', array($m_sped, $spedizione['CSNSPC']));
 		else
 			$m_sped_print = implode(',', array($m_sped));
 		
		$email_subject = "Lista consegne programmate del " . print_date($spedizione['CSDTSP']);				
		break;
 	case "liv_1": 
		$tmp_carico = $sottostringhe[2];
		$tmp = explode("___", $tmp_carico);
		$m_data = $tmp[0];
		$m_sped = $tmp[1];
		$m_tpca = $tmp[2];
		$m_aaca = $tmp[3];
		$m_nrca = $tmp[4];		
 		$spedizione = $s->get_spedizione($m_sped);
		$email_subject = "Lista consegne programmate del " . print_date($spedizione['CSDTSP']) . "  (carico " . 
				implode("_", array((int)$m_aaca, $m_tpca, (int)$m_nrca)) . ")";				
		break;
 	case "liv_2": 
		$tmp_carico = $sottostringhe[2];
		$tmp = explode("___", $tmp_carico);
		$m_data = $tmp[0];
		$m_sped = $tmp[1];
		$m_tpca = $tmp[2];
		$m_aaca = $tmp[3];
		$m_nrca = $tmp[4];		
		
		$tmp_cliente = $sottostringhe[4];
		$tmp = explode("___", $tmp_cliente);
		$m_ccon = $tmp[1];				
		
		break;		
	default: die("error");			
 }



	$itinerario = new Itinerari();
	$itinerario->load_rec_data_by_k(array("TAKEY1"=>$spedizione['CSCITI']));
	$itinerario_parameters = $itinerario->parse_YAML_PARAMETERS($itinerario->rec_data);
	
	$aspe = new AreeSpedizione();
	$aspe->load_rec_data_by_itin($spedizione['CSCITI']);
	
	$vettore = new Vettori();
	$vettore->load_rec_data_by_k(array("TAKEY1"=>$spedizione['CSCVET']));	
	
	//$vettore_parameters = (array)json_decode(trim($vettore->get_YAML_PARAMETERS($vettore->rec_data)));	
	$vettore_parameters = $vettore->parse_YAML_PARAMETERS($vettore->rec_data);

	$ar_email_to = array();
	
	$ar_initial_value = '';
	
	//Specifico per report (per cliente o per agente)
	switch($_REQUEST['r_type']){
		case 'per_cliente':
			$r_cod_selected_ar = explode("|", $_REQUEST['r_cod_selected']);
			$email_cod = $s->get_email_cliente_dest($r_cod_selected_ar[0], $r_cod_selected_ar[1], $r_cod_selected_ar[2]);
			$ar_email_to[] = array($email_cod, "CLIENTE  (" .  trim($email_cod) . ")");
			$ar_initial_value = $email_cod;
			break;
		case 'per_agente':
			$r_cod_selected_ar = explode("|", $_REQUEST['r_cod_selected']);
			$des_agente = $s->get_des_agente($r_cod_selected_ar[0], $r_cod_selected_ar[1]) . " [{$r_cod_selected_ar[1]}]";
			
			$email_cod = $s->get_email_agente($r_cod_selected_ar[0], $r_cod_selected_ar[1]);
			$ar_email_to[] = array($email_cod, "AGENTE  (" .  trim($email_cod) . ")");
			$ar_initial_value = $email_cod;
				
			
			break;			
	}
	
	
	
	
	$ar_email_to[] = array(trim($itinerario->rec_data['TAMAIL']), "ITINERARIO " . j(trim($itinerario->rec_data['TADESC'])) . " (" .  trim($itinerario->rec_data['TAMAIL']) . ")");
	
	//se l'itinerario ha email di gruppo
	if (strlen(trim($itinerario_parameters['email_gruppo'])) > 0)
		$ar_email_to[] = array(trim($itinerario_parameters['email_gruppo']), "GRUPPO " . j(trim($itinerario->rec_data['TADESC'])) . " (" . j(trim($itinerario_parameters['email_gruppo'])) . ")" );
	
	$ar_email_to[] = array(trim($vettore->rec_data['TAMAIL']), "VETTORE " . j(trim($vettore->rec_data['TADESC'])) . " (" .  trim($vettore->rec_data['TAMAIL']) . ")");
	//se il vettore ha email di gruppo
	if (strlen(trim($vettore_parameters['email_gruppo'])) > 0)
		$ar_email_to[] = array(trim($vettore_parameters['email_gruppo']), "GRUPPO " . j(trim($vettore->rec_data['TADESC'])) . " (" . j(trim($vettore_parameters['email_gruppo'])) . ")" );			
	
	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);	 
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   <?php @include '../../personal/report_css_logo_cliente.php'; ?>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}
   
<?php if ($_REQUEST['stampa_dettaglio_ordini'] == "Y" || $_REQUEST['stampa_dettaglio_ordini'] == "R") { ?>
   tr.liv1 td{background-color: #ffffff;}
   tr.liv2 td{padding-bottom: 6px; padding-top: 6px;}
   tr.liv3 td{padding-bottom: 4px; padding-top: 4px;}
<?php } else { ?>
   tr.liv1 td{background-color: #ffffff;}
   tr.liv1 td{padding-bottom: 4px; padding-top: 4px;}
<?php } ?>

<?php if ($_REQUEST['stampa_griglia'] == "N") { ?>
   tr.liv1 td{background-color: #ffffff; border: 0px;}
<?php } ?>
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
      
   table.int1 td, table.int1 th{vertical-align: top;}
   
   table.int1 tr.rigavuota td{border: 0px;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {border-collapse: unset;}	    
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {

	//apertura automatica finestra invio email
	<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
		Ext.get('p_send_email').dom.click();
	<?php } ?>
	
	});    
    

  </script>


 </head>
 <body>
 	
 	
<!--  	
<div class="page-utility noPrint">
	<A HREF="javascript:window.print();"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>
	<img id="p_send_email" src=<?php echo img_path("icone/48x48/address_black.png") ?>>	
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>
</div>

-->
 
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>
</div>

<?php


    //bug parametri linux
    global $is_linux;
    if ($is_linux == 'Y')
        $_REQUEST['form_values'] = strtr($_REQUEST['form_values'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
    
    $form_values = json_decode($_REQUEST['form_values']);
    
    if ($form_values->tipo_elenco == 'GATE')
        $td_sped_field = 'TDNBOF';
    else
        $td_sped_field = 'TDNBOC';
    

	 
	$sql_SELECT = " TD.*, SP.* "; 
	$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . " 


				   LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				     ON TD.TDDT=SP.CSDT AND {$td_sped_field} = SP.CSPROG AND SP.CSCALE = '*SPR'


				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS 
					  ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA				     
				   ";
	
	
	if ($_REQUEST['stampa_dettaglio_ordini']=="R"){

		global $backend_ERP;		
		switch ($backend_ERP){
		
			case 'GL':
				$sql_SELECT .= " , RD.MECAR0 AS COD_ART, ART.MADES0 AS DES_ART, RD.MEQTA0 AS QTA_RIGA, RD.MEUNM0 AS UM_RIGA ";
				$sql_ORDER  = ", RD.MERID0";
				$sql_FROM .= "
					INNER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					  ON  TD.TDOTID = RD.MECAD0 
					  AND TD.TDOINU = RD.METND0 
					  AND TD.TDOADO = RD.MEAND0 
					  AND TD.TDONDO = RD.MENUD0
					  AND TD.TDPROG = RD.MERID0
					LEFT OUTER JOIN E42_\$FILRV.\$MAARTLA ART
					  ON RD.MECAR0 = ART.MACAR0
					";
			break;
			
			default: //SV2
				$sql_SELECT .= " , RD.RDART AS COD_ART, RD.RDDART AS DES_ART, RD.RDQTA AS QTA_RIGA, RD.RDUM AS UM_RIGA ";
				$sql_ORDER  = "";
				$sql_FROM .= "
					INNER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					  ON  TD.TDDT   = RD.RDDT 
					  AND TD.TDOTID = RD.RDTIDO 
					  AND TD.TDOINU = RD.RDINUM 
					  AND TD.TDOADO = RD.RDAADO 
					  AND TD.TDONDO = RD.RDNRDO
 				";
						
		} //switch	
		
	}	
	
	$sql_WHERE = " WHERE " . $s->get_where_std() . " AND TDSWSP='Y' ";

	/* NON FILTRO MAI PER DATA, COSI' VENGONO FUORI EVENTUALI ANOMALIE */
	//if (isset($m_data)) $sql_WHERE .= " AND TDDTEP = $m_data ";
	



	if (isset($m_sped_print)) $sql_WHERE .= " AND {$td_sped_field} IN (" . $m_sped_print. ")";
	 else if (isset($m_sped)) $sql_WHERE .= " AND {$td_sped_field} = " . $m_sped;	



	if (isset($m_tpca)) $sql_WHERE .= " AND TDTPCA = " . sql_t_trim($m_tpca);	
	if (isset($m_tpca)) $sql_WHERE .= " AND TDAACA = " . $m_aaca;	
	if (isset($m_nrca)) $sql_WHERE .= " AND TDNRCA = " . $m_nrca;	
	if (isset($m_ccon)) $sql_WHERE .= " AND TDCCON = " . sql_t_trim($m_ccon);

	switch($_REQUEST['r_type']){
		case 'per_cliente':
			$r_cod_selected_ar = explode("|", $_REQUEST['r_cod_selected']);
			$sql_WHERE .= "	AND TDDT = " . sql_t($r_cod_selected_ar[0]);
			$sql_WHERE .= "	AND TDCCON = " . sql_t($r_cod_selected_ar[1]);
			$sql_WHERE .= "	AND TDCDES = " . sql_t($r_cod_selected_ar[2]);
		break;
		case 'per_agente':
			$r_cod_selected_ar = explode("|", $_REQUEST['r_cod_selected']);
			$sql_WHERE .= "	AND TDDT = " . sql_t($r_cod_selected_ar[0]);
			$sql_WHERE .= "	AND TDCAG1 = " . sql_t($r_cod_selected_ar[1]);
			break;		
	}

	

	$sql_WHERE .= sql_where_by_combo_value("TD.TDOTPD", $form_values->f_tipo_ordine);
	


	$sql = "SELECT {$sql_SELECT} " . $sql_FROM . $sql_WHERE	. " ORDER BY TDDTEP, TDCITI, {$td_sped_field}, PSFG02, TDTPCA, TDAACA, TDNRCA, TDSECA, TDDCON/*, TDSCOR*/ {$sql_ORDER}";



	
	$stmt = db2_prepare($conn, $sql);		
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
 
    $liv0_in_linea = null;
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$liv0 = implode("|", array($r['TDDTEP'], $r['CSCITI'], $r['CSCVET'], $r['CSCAUT'], $r['CSCCON'], $r['CSPROG']));
		$liv1 = implode("|", array($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA']));		
		$liv2 = implode("|", array($r['TDCCON'], $r['TDCDES']));		
		$liv3 = implode("|", array($r['TDDT'], $r['TDOTID'], $r['TDOINU'], $r['TDOADO'], $r['TDONDO']));		
		
		//data|itinerario|vettore
		$t_ar = &$ar;
		$l = $liv0;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['CSPROG'] = $r['CSPROG'];		
			//$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar_liv0 = &$t_ar[$l]['val']; 					  								  
			$t_ar = &$t_ar[$l]['children'];
		
		//carico
		$l = $liv1;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>implode("_", array($r['TDAACA'], $r['TDTPCA'], $r['TDNRCA'])), "record" => $r,
 								  "val" => array(), "children"=>array());
			//$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar_liv1 = &$t_ar[$l]['val'];
			$t_ar[$l]['carico'] = $s->get_carico_td($r);
			$t_ar[$l]['orario_carico'] = $r['CSHMPG'];			 								  
			$t_ar = &$t_ar[$l]['children'];
		 								  								  
		//cliente|dest
		$l = $liv2;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			//$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar_liv2 = &$t_ar[$l]['val'];
			$t_ar = &$t_ar[$l]['children'];								  								  

		//ordine
		$l = $liv3;
		if (!isset($t_ar[$l])){
			//Solo una volta per ogni ordine 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
 			//articolo
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r, $form_values);
			$t_ar_liv0 = somma_valori($t_ar_liv0, $r, $form_values);
			$t_ar_liv1 = somma_valori($t_ar_liv1, $r, $form_values);
			$t_ar_liv2 = somma_valori($t_ar_liv2, $r, $form_values);
		}									  
			$t_ar = &$t_ar[$l]['children'];
			
			//accodo le righe ordine
			$t_ar[] = $r;
		
	} //while


//STAMPO
 $cl_liv_cont = 0;
 if ($_REQUEST['stampa_dettaglio_ordini'] == "Y" || $_REQUEST['stampa_dettaglio_ordini'] == "R") $liv3_row_cl = ++$cl_liv_cont;
 $liv2_row_cl = ++$cl_liv_cont; 
 $liv1_row_cl = ++$cl_liv_cont;
 
//ordinamento: prima la sped principale (eventualmente la secondaria)
 usort($ar, "cmp_by_sped_princ_collegata"); 
 
echo "<div id='my_content'>";

foreach ($ar as $kl0 => $l0){	
	echo liv0_intestazione_open($l0['record'], ++$c_sped);

	  		foreach ($l0['children'] as $kl1 => $l1){
				echo liv1_intestazione_open($l1, $liv1_row_cl);	  			
			
			  		foreach ($l1['children'] as $kl2 => $l2){
						echo liv2_intestazione_open($l2, $liv2_row_cl);
						
						if ($_REQUEST['stampa_dettaglio_ordini']=="Y" || $_REQUEST['stampa_dettaglio_ordini']=="R")
				  		foreach ($l2['children'] as $kl3 => $l3){
							echo liv3_intestazione_open($l3, $liv3_row_cl);
							
							if ($_REQUEST['stampa_dettaglio_ordini']=="R"){ //righe ordine
						  		foreach ($l3['children'] as $kriga => $l_riga)
									echo liv_riga_intestazione_open($l_riga, $liv3_row_cl);								
							}							
						}				  		

						echo riga_vuota();
						
						
				  		if ($_REQUEST['stampa_rilevazione_scarico']=="Y")
				  			echo liv2_rilevazione_scarico();
							
					}			
			}		
	
	echo liv0_intestazione_close($l0['record']);	
}
echo "</div>";

 		
?>

 </body>
</html>







<?php




function somma_valori($ar, $r, $form_values){
	
 //il volume e' aumentato in base alla percentuale passata (recuperata prima dall'itinerario)
 $ar['VOLUME'] 	+= $r['TDVOLU'] * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
 $ar['COLLI'] 	+= $r['TDTOCO'];
 $ar['PALLET'] 	+= $r['TDBANC']; 
 $ar['PESO'] 	+= $r['TDPLOR']; 
 
 
 //a meno che il tipo ordine non sia tra quelli da azzerare
 if (is_null($form_values->f_tipo_ordine_da_azzerare)) $form_values->f_tipo_ordine_da_azzerare = array();
 
 if (array_search($r['TDOTPD'], $form_values->f_tipo_ordine_da_azzerare, true) === false) //non trovato
 	$ar['IMPORTO'] += $r['TDTIMP'];
 else {
 	global $cfg_mod_Spedizioni;
 	 	
 	//verifico in config se comunque devo sommarlo in base a tipo ordine/causale
 	if (is_null($cfg_mod_Spedizioni["report_scarichi_escludi_somma_importi"][trim($r['TDOTPD'])]))
 		$ar_escludi_se_causale = array();
 	else
 		$ar_escludi_se_causale = $cfg_mod_Spedizioni["report_scarichi_escludi_somma_importi"][trim($r['TDOTPD'])];
 	
 	if (empty($ar_escludi_se_causale) == false && array_search(trim($r['TDCAUT']), $ar_escludi_se_causale, true) === false) //non trovato
 		$ar['IMPORTO'] += $r['TDTIMP'];
 }
 
 return $ar;  
}




function liv_riga_intestazione_open($l, $cl_liv){


	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 4;
	else
		$n_col_span = 3;
	
	if ($_REQUEST['stampa_data_scarico']=='Y')
		$n_col_span++;
	

	$ret = "
	<tr class=liv{$cl_liv}>
	<td colspan=1>&nbsp;</TD>
	<td colspan={$n_col_span}>" . trim($l['DES_ART']) . " [" . trim($l['COD_ART']) . "]" . "</TD>
    <td colspan=1 class=number>" . n($l['QTA_RIGA']) . " [" . trim($l['UM_RIGA']) . "]" . "</TD>";
	
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>&nbsp;</TD> ";
	$ret .="
		 <td colspan=1 class=number>&nbsp;</TD>
		 <td colspan=1 class=number>&nbsp;</TD>
		 ";
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>&nbsp;</TD>";

	$ret .= "</tr>";
	return $ret;
}



function liv3_intestazione_open($l, $cl_liv){
    
	if ($_REQUEST['stampa_dettaglio_ordini']=="R")
		$add_grassetto = " grassetto ";
	else
		$add_grassetto = "";

	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 4;
	else
		$n_col_span = 3;
	
	if ($_REQUEST['stampa_data_scarico']=='Y')
		$n_col_span++;
	

	if ($_REQUEST['stampa_pallet']=='Y') $n_col_span_add++;
	if ($_REQUEST['stampa_importi']=='Y') $n_col_span_add++;
	
	$ret = "";
	
	
	$art = implode("_", array($l['record']['TDOTID'], $l['record']['TDOADO'],  $l['record']['TDONDO'], $l['record']['TDOTPD']));
	
	
	//20181210: DAVIDE IMPOSTA TDFU05='Z' se ci sono righe con import a 0:
	// segnalo l'anomalia nella colonna importo
	if (trim($l['record']['TDFU05']) == 'Z')
	  $segnala_ord_con_righe_a_0 = '(*) ';
	else 
	  $segnala_ord_con_righe_a_0 = '';
	
	$ret .= "
		<tr class=\"liv{$cl_liv} $add_grassetto\">
		<td colspan=1>" . $art . " - " . $l['record']['TDDVN1'] . "</TD>
		 <td colspan={$n_col_span}>Rif: " . $l['record']['TDVSRF'] . "</TD>
		 <td>" . $l['record']['TDSTAT'] . "</TD>
		 <td>" . $l['record']['TDCAUT'] . "</TD>
		 <td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>		 
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";
	$ret .="
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD> 	 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>{$segnala_ord_con_righe_a_0}" . n($l['val']['IMPORTO'], 2) . "</TD>";		 
	
	$ret .= "</tr>";
 return $ret;
}


function liv2_intestazione_open($l, $cl_liv){

	global $main_module, $cfg_mod_Spedizioni;
	
	
	//scorro gli ordini per cercare la data di scarico maggiore
	$data_scarico = 0;
	foreach($l['children'] as $r){
		$data_scarico = max((int)$data_scarico, $r['record']['TDDTIS']);
	}

	if ($_REQUEST['indirizzo_di'] == 'ind_destinazione'){
		$m_loca = 'TDLOCA';
		$m_prov = 'TDPROV';
		$m_cap  = 'TDCAP';
	} else {
		$m_loca = 'TDDLOC';
		$m_prov = 'TDPROD';
		$m_cap  = 'TDDCAP';
	}
		

	$td_cli_class = '';
	if ($_REQUEST['r_type'] == 'per_cliente')
		$td_cli_class = 'grassetto';

	
	//preparo eventuali note cliente/destinazione
	$txt_ar_note = get_my_note_cli_des($main_module, $l); 
	
	$txt_note_cell_cli = '';
	if (!is_null($txt_ar_note) && $cfg_mod_Spedizioni['report_scarichi_note_sotto_cliente'] == 'Y'){
		$txt_note_cell_cli = '<br><div class=cli_note>[' . implode("<br/>", $txt_ar_note) . "]</div>"; 
	}

	$ret = "";
	
	$ret .= "
		<tr class=\"tr-cli liv{$cl_liv}\">
		 <td colspan=1 class=\"{$td_cli_class}\">" . $l['record']['TDDCON'] . $txt_note_cell_cli . "</TD>
		 <td colspan=1>" . $l['record'][$m_loca] . "</TD>		 
		 <td colspan=1>" . $l['record'][$m_prov] . "</TD>		 
		 <td colspan=1>" . $l['record'][$m_cap] . "</TD>";
	
	
	if ($_REQUEST['stampa_data_scarico']=='Y')  $ret .= "<td colspan=1>" . print_date($data_scarico) . "</TD> ";	
	if ($_REQUEST['stampa_tel']=='Y')  $ret .= "<td colspan=1>" . $l['record']['TDTEL'] . "</TD> ";	
	if ($_REQUEST['stampa_dettaglio_ordini']=='Y')  $ret .= "<td>&nbsp;</TD><td>&nbsp;</TD> ";
	$ret .= "
   		
		 <td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>		 
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";		 
	$ret .= "
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD>	 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";
		 		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'], 2) . "</TD>";		 
	
	$ret .= "</tr>";
	
	
	if (!is_null($txt_ar_note) && $cfg_mod_Spedizioni['report_scarichi_note_sotto_cliente'] != 'Y'){
			$n_colspan = 6;
			if ($_REQUEST['stampa_tel']=='Y') $n_colspan++;
			if ($_REQUEST['stampa_data_scarico']=='Y') $n_colspan++;
			if ($_REQUEST['stampa_importi']=='Y') $n_colspan++;
			if ($_REQUEST['stampa_pallet']=='Y') $n_colspan++;
							
			$ret .= "<tr>
   				<td align=right valign=top>Segnalazioni</td>
   				<td colspan={$n_colspan} valign=top>" . implode("<br/>", $txt_ar_note) . "
   				</tr>";					
	}
	
	
 return $ret;
}





function liv2_rilevazione_scarico(){

	$n_colspan = 4 + 3;
	
	if ($_REQUEST['stampa_tel']=='Y') $n_colspan++;
	if ($_REQUEST['stampa_data_scarico']=='Y') $n_colspan++;
	if ($_REQUEST['stampa_importi']=='Y') $n_colspan++;
	if ($_REQUEST['stampa_pallet']=='Y') $n_colspan++;
	


	$ret = "
		<tr>
			<td colspan={$n_colspan} style='text-align: center; padding: 30px; padding-bottom: 70px; font-size: 16px;'>
			Data scarico: _____/_____/__________      Ora Scarico ______:_______
			</TD>
		</tr>
	";

	return $ret;
}




function riga_vuota(){

	$n_colspan = 4 + 3;

	if ($_REQUEST['stampa_tel']=='Y') $n_colspan++;
	if ($_REQUEST['stampa_data_scarico']=='Y') $n_colspan++;
	if ($_REQUEST['stampa_importi']=='Y') $n_colspan++;
	if ($_REQUEST['stampa_pallet']=='Y') $n_colspan++;



	$ret = "
		<tr class=rigavuota>
			<td colspan={$n_colspan}>&nbsp;
			</TD>
		</tr>
		";

	return $ret;
}






function liv1_intestazione_open($l, $cl_liv){

	if ($l['orario_carico'] > 0)
		$orario_carico = ' [Orario: ' . print_ora($l['orario_carico']) . "]";
	else $orario_carico = '';

	if (strlen(trim($l['carico']['PSNOTE'])) > 0)
		$note_carico = "&nbsp;&nbsp;&nbsp;<b> [" . trim($l['carico']['PSNOTE']) . "]</b>";
	else $note_carico = '';

	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 5;
	else
		$n_col_span = 4;
	
	if ($_REQUEST['stampa_dettaglio_ordini']=='Y')  
		$n_col_span = 7;
	
	
	if ($_REQUEST['stampa_data_scarico']=='Y')
		$n_col_span++;
	
	$span_carico_class = '';
	if ($_REQUEST['stampa_dettaglio_ordini'] != "Y" && $_REQUEST['stampa_dettaglio_ordini'] != "R")
		$span_carico_class = 'grassetto';
	
	$ret = "
		<tr class=liv{$cl_liv}>
		 <td colspan={$n_col_span}><span class=\"{$span_carico_class}\">CARICO " . $l['descr'] . "</span>" . $orario_carico . $note_carico . "</TD>
		  
			
			<td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";		 
	$ret .= "
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD>		 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'],2) . "</TD>";		 

	$ret .="</tr>";
 return $ret;
}



function liv0_intestazione_open($r, $c_sped = 1){
	global $s, $spedizione, $m_sped;

	global $conn, $cfg_mod_Spedizioni,$id_ditta_default;
	
	$ret = "";
	
	//se sono in una spedizione successiva (collegata) lascio un po' di spazio
	if ($c_sped > 1) $ret .= "<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;"; 
	
	
	if ($_REQUEST['data_riferimento'] == 'data_spedizione')
		$f_data = 'TDDTSP';
	else
		$f_data = 'TDDTEP';
	
	//itinerario e area da spedizione attuale
	$itinerario = new Itinerari();
	$itinerario->load_rec_data_by_k(array("TAKEY1"=>$r['CSCITI']));
	
	$aspe = new AreeSpedizione();
	$aspe->load_rec_data_by_itin($r['CSCITI']);	

	
	$ret .= "
  		<div class=header_page>
			<H2>LISTA CONSEGNE PROGRAMMATE</H2>
			<h3>" . trim($itinerario->rec_data['TADESC']) . " | " . trim($aspe->rec_data['TADESC']) .  "</span></H3>
			
		</div>	
		<table class=int0 width=\"100%\">
			 <tr>
			  <td width=\"50%\">Spedizione Nr ".$m_sped." del " . print_date($r[$f_data]) . "</td>";

	
	  $out_km_spedizione = '&nbsp;';
	  if ($_REQUEST['stampa_km_spedizione']=='Y' && n($r['CSKMTR'], 0) > 0){
	  	$out_km_spedizione = "[Km previsti " . n($r['CSKMTR'], 0) . "]";
	  }	  
	
	  if ($_REQUEST['r_type'] == 'per_agente'){
			global $des_agente;
			$ret .= "<td>Agente: </td>
					 <td width=\"50%\">" . $des_agente . "</td>
					 </tr> 
					 <TR>
					  <TD colspan=2 style='font-weight: normal; font-size: 0.9em;'>{$out_km_spedizione}</td>
					  <TD>&nbsp;</TD>";					 
	  }
	  else {	
	
		$ret .= "
	 		  <td>Vettore: </td>	 		
			  <td width=\"50%\">"  . 
			  	$s->decod_std('AUTR', $r['CSCVET']) . " - " . $s->decod_std('AUTO', $r['CSCAUT']) . " " .	$s->decod_std('COSC', $r['CSCCON']) .
			   "</td>
			 </tr> 
			 <TR>
			  <TD colspan=2 style='font-weight: normal; font-size: 0.9em;'>{$out_km_spedizione}</td>
			  <TD style='font-weight: normal; font-size: 0.9em;'>";		

		if (strlen(trim(stampa_dati_vettore($r['CSCVET']))) > 0){
    		$ret .= " Tel. " . stampa_dati_vettore($r['CSCVET']);
		}
		$ret .= " " . stampa_dati_targa($r['CSTARG']);		
    						
        $ret .= "</TD>";			   
	  }	

	    
		$ret .= "</TR>";
		
		
		//dati integrativi di spedizione
		$t_sped_id = sprintf("%08s", $r['CSPROG']);		
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($id_ditta_default, 'SPINT', $t_sped_id));
		$row = db2_fetch_assoc($stmt);
			
		if ($row) {
			$ret .= "
			<tr>
			<td colspan=3 style=\"font-weight: normal; font-size: 0.9em;\">
			<span>
			E-mail: {$row['TAMAIL']} &nbsp;
			- E-mail alt.: {$row['TAINDI']} &nbsp;
			- Dogana: {$row['TALOCA']} &nbsp;
			- Nr booking: {$row['TARIF2']}
			</span>
			</td>
			</tr>
			";
		}
		
		if (trim($row['TADESC']) != '' ) {
			$ret .= "
			<tr>
			<td colspan=3 style=\"font-weight: normal; font-size: 0.9em;\">
			<span>
			Note: " . trim($row['TADESC']) . "
			</span>
			</td>
			</tr>
			";
		}
		
			
			
		$ret .= "</table>
			
				<p style=\"text-align: right; vertical-align:bottom; font-size:13px; \">Data elaborazione: " .  Date('d/m/Y H:i') . "</p>	
			
			<table class=int1>
			 <tr>
			  <th>Cliente</th>
			  <th>Localit&agrave;</th>
			  <th>Pr</th>			  			  
			  <th>Cap</th>";
	if ($_REQUEST['stampa_data_scarico']=='Y')  $ret .= "<th>Data scarico</T> ";
	if ($_REQUEST['stampa_tel']=='Y')  $ret .= "<th>Telefono</T> ";	
	if ($_REQUEST['stampa_dettaglio_ordini']=='Y')  $ret .= "<th>St.</th><th>Cau.</th> ";
	$ret .= "
			<th>Colli</th>
			  ";
			  if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<th>Pallet</T> ";
	$ret .= "
			  <th>Volume</th>		  			  
			  <th>Peso</th>			  
			  ";			  
			  
	if ($_REQUEST['stampa_importi']=='Y') $ret .="<th>Importo</th>";			  
	
	$ret .= "</tr>";	
 return $ret;	
}

function liv0_intestazione_close($r){
	return "</table>";
}

function stampa_dati_vettore($cod_vettore){
	$vt = new Vettori();
	$vt->load_rec_data_by_k(array('TAKEY1' => $cod_vettore));
	return $vt->rec_data['TATELE'];
}

function stampa_dati_targa($targa){
  if (strlen(trim($targa)) > 0){
	return "<span style='margin-left: 90px;'>Targa: " . trim($targa) . "</span>";
  }
  else return "";	
}



function get_my_note_cli_des($main_module, $l){
	$txt_ar = null;
	
	if ($_REQUEST['stampa_segnalazioni_cli_des'] == 'Y'){
		//stampo, se ci sono, le note per cliente/destinazione
		if (
				$main_module->ha_note_cliente($l['record']['TDDT'], $l['record']['TDCCON']) ||
				$main_module->ha_note_cliente_dest($l['record']['TDDT'], $l['record']['TDCCON'], $l['record']['TDCDES']) ||
				$main_module->ha_note_cliente($l['record']['TDDT'], implode('_', array($l['record']['TDDT'], trim($l['record']['TDCCON']), trim($l['record']['TDCDES']), $l['record']['CSPROG'])), '')				 
		){
	
			$txt_ar = array();
				
			$txt_note_cli = $main_module->get_note_cliente_portal($l['record']['TDDT'], $l['record']['TDCCON'], '');
			if (strlen(trim($l['record']['TDCDES'])) > 0)
				$txt_note_des = $main_module->get_note_cliente_portal($l['record']['TDDT'], $l['record']['TDCCON'], $l['record']['TDCDES']);
				
			if (strlen(trim($txt_note_cli)) > 0){
				//$txt_ar[] = "[ NOTE CLIENTE ] ";
				$txt_ar[] = $txt_note_cli;
			}
			if (strlen(trim($txt_note_des)) > 0){
				// $txt_ar[] = "[ NOTE DESTINAZIONE ] ";
				$txt_ar[] = $txt_note_des;
			}
			
			$txt_note_cli_des_spe = $main_module->get_note_cliente_portal($l['record']['TDDT'], implode('_', array($l['record']['TDDT'], trim($l['record']['TDCCON']), trim($l['record']['TDCDES']), $l['record']['CSPROG'])), '');
			if (strlen(trim($txt_note_cli_des_spe)) > 0){
				$txt_ar[] = $txt_note_cli_des_spe;
			}
				
			
	
			$note_gest = $main_module->get_note_cliente_gest($l['record']['TDDT'], $l['record']['TDCCON']);
			while ($row = db2_fetch_assoc($note_gest)){
				// if (count($txt_ar) == 0 ) $txt_ar[] = "[ NOTE ANAGRAFICHE GESTIONALI ]";
				$txt_ar[] = trim($row['RDDES1']);
			}
	
				
	
		}	
	}
	
	return $txt_ar;
}




?>		