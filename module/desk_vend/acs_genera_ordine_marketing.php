<?php

require_once("../../config.inc.php");
require_once("../desk_gest/acs_panel_ins_new_anag_include.php");

$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_genera'){
    
    $form_values = $m_params->form_values;
    $ritime = microtime(true);
    
    foreach($m_params->list_selected_id as $v){
                
        $ar_upd_RI['RACLIE']  = $v->cliente;
        $ar_upd_RI['RADEST']  = $v->cod_d;
        $ar_upd_RI['RATIME'] = $ritime;
        $ar_upd_RI['RADT'] = $id_ditta_default;
        $sql = "INSERT INTO {$cfg_mod_Gest['file_richieste_anag_cli']} (" . create_name_field_by_ar($ar_upd_RI) . ") VALUES (" . create_parameters_point_by_ar($ar_upd_RI) . ")";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd_RI);
        echo db2_stmt_errormsg($stmt);
        
    }
    //print_r($sql); exit;
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "RITIME"    => $ritime,
            "messaggio"	=> 'GEN_ORD_MARK',
            "vals" => array(
                "RITIDO"     => $form_values->f_tipo,
                "RIAADO"     => $form_values->f_anno,
                "RINRDO"     => $form_values->f_numero,
                "RIDTEP"     => $form_values->f_data,
                "RICITI"     => $form_values->f_def,
            )
        )
        );
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;     
    
}

if ($_REQUEST['fn'] == 'get_json_data_cli'){
    
    $form_open = $m_params->open_request->form_open;
    $sql_where = sql_where_params($form_open);
    
    $area_manager = $form_open->CCARMA;
    $agente = $form_open->CCAGE;
    if(count($area_manager) > 0)
        $sql_where .= sql_where_by_combo_value('CCARMA', $area_manager);
    if(count($agente) > 0){
        if(count($agente) == 1)
            $sql_where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
        if(count($agente) > 1)
            $sql_where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
    }
    
    
   $sql =  "SELECT GC.*, GC_P.C_PV AS C_PV, GC_P.NR_BOX AS NR_BOX, CCAG1 AS AGE1, CFDTGE,
            SUBSTRING(TAREST, 30, 60) AS IND_D, SUBSTRING(TAREST, 90, 60) AS LOC_D,
            SUBSTRING(TAREST, 161, 2) AS PRO_D 
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            {$sql_join}
            LEFT OUTER JOIN( 
                SELECT COUNT(*) AS C_PV, SUM(GCNBOX) AS NR_BOX, GCPRAB
                FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
                WHERE GCDT = '{$id_ditta_default}' AND GCTPAN = 'DES' AND GCTPIN = 'P' AND GCSOSP <> 'S'
                GROUP BY GCPRAB) GC_P
            ON GC_P.GCPRAB = GC.GCPROG
            LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
                ON CC.CCDT = GC.GCDT AND CC.CCPROG = GC.GCPROG AND CC.CCSEZI = 'BCCO' AND CCCCON = 'STD' 
            LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA
                ON TA.TADT = GC.GCDT AND TA.TAID = 'VUDE' AND TA.TANR = '=AG' AND TA.TACOR1 = GC.GCCDCF AND TA.TATP <>'S'
            LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
			     ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
            WHERE GCDT = '{$id_ditta_default}' AND GCTPAN = 'CLI' {$sql_where}
            ORDER BY GCDCON DESC";

   $sql_p = "SELECT COUNT(*) AS C_PP
             FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
             WHERE GCDT = '{$id_ditta_default}' AND GCTPAN = 'DES' AND GCTPIN = 'P'
             AND GCGMTP = 'P' AND GCPRAB = ?";
   
   $stmt_p = db2_prepare($conn, $sql_p);
   echo db2_stmt_errormsg();
   
   $sql_e = "SELECT COUNT(*) AS C_ES
             FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
             WHERE GCDT = '{$id_ditta_default}' AND GCTPAN = 'DES' AND GCTPIN = 'P'
             AND GCESPO = 'Y' AND GCPRAB = ?";
   
   $stmt_e = db2_prepare($conn, $sql_e);
   echo db2_stmt_errormsg();
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['cliente'] = $row['GCCDCF'];
        $nr['d_cli'] = $row['GCDCON'];
        $agente = get_TA_sys('CUAG', $row['AGE1']);
        $nr['agente'] = $row['AGE1'];
        $nr['d_age'] = $agente['text'];
        $nr['pven'] = $row['C_PV'];
        $nr['nazi'] = $row['GCNAZI'];
        $nr['prov'] = trim($row['PRO_D']);
        $nr['indi'] = trim($row['IND_D']);
        $nr['loca'] = trim($row['LOC_D']);
        $nr['data_ge'] = $row['CFDTGE'];
        if($nr['indi'] != '' || $nr['loca'] != '')
            $nr['cod_d'] = '=AG';
        $nr['k_cli'] = $row['GCDT']."_".$row['GCCDCF'];
        if($row['C_PV'] > 0){
            $result = db2_execute($stmt_p, array($row['GCPROG']));
            $row_p = db2_fetch_assoc($stmt_p);
            $nr['promo'] = $row_p['C_PP'];
            $result = db2_execute($stmt_e, array($row['GCPROG']));
            $row_e = db2_fetch_assoc($stmt_e);
            $nr['espo'] = $row_e['C_ES'];
            if($row['NR_BOX'] == 0)
                $nr['box'] = 1;
            else
                $nr['box'] = $row['NR_BOX']; 
        }
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}




if ($_REQUEST['fn'] == 'open_form'){ ?>

{"success":true, "items": [

        {
            xtype: 'form',
            autoScroll : true,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            items: [ 
                	  {
        				name: 'f_tipo',
        				xtype: 'combo',
                    	anchor: '-5',	  
        				fieldLabel: 'Tipo',
        				displayField: 'text',
        				labelWidth : 50,
        				valueField: 'id',
        				emptyText: '- seleziona -',
        				allowBlank: false,	
        				forceSelection: true,
        			    flex: 1, 
        			    queryMode: 'local',
                        minChars: 1,      	     		
        				store: {
        					autoLoad: true,
        					editable: false,
        					autoDestroy: true,
        				    fields: [{name:'id'}, {name:'text'}],
        				    data: [
                                   <?php echo acs_ar_to_select_json($main_module->find_TA_std('TPMRK', null, 'N', 'N', null, null, null, 'N', 'Y'), ""); ?>
                			      ]
        				},listeners: { beforequery: function (record) {
        			         record.query = new RegExp(record.query, 'i');
        			         record.forceAll = true;
        		             }}
        			},
                	{
						name: 'f_anno',
						xtype: 'numberfield',
						labelWidth : 50,
						fieldLabel: 'Anno',
						hideTrigger : true,
					    anchor: '-5',	
					    allowBlank: false						
					},{
						name: 'f_numero',
						xtype: 'numberfield',
						labelWidth : 50,
						hideTrigger : true,
						fieldLabel: 'Numero',
					    anchor: '-5',	
					    allowBlank: false						
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Data'
					   , labelWidth : 50
					   , name: 'f_data'
					   , format: 'd/m/Y'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					},
					{
        				name: 'f_def',
        				xtype: 'combo',
                    	anchor: '-5',	  
        				fieldLabel: 'Default',
        				displayField: 'text',
        				labelWidth : 50,
        				valueField: 'id',
        				emptyText: '- seleziona -',
        				allowBlank: false,	
        				forceSelection: true,
        			    flex: 1, 
        			    queryMode: 'local',
                        minChars: 1,      	     		
        				store: {
        					autoLoad: true,
        					editable: false,
        					autoDestroy: true,
        				    fields: [{name:'id'}, {name:'text'}],
        				    data: [
                                      <?php echo acs_ar_to_select_json($main_module->find_TA_std('CRMRK', null, 'N', 'N', null, null, null, 'N', 'Y'), ""); ?>
        					    ]
        				},listeners: { beforequery: function (record) {
        			         record.query = new RegExp(record.query, 'i');
        			         record.forceAll = true;
        		             }}
        			},	 	
                	
				],
				
				buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "GEN_ORD_MRK");  ?>
			
        			{
        	            text: 'Elenco clienti',
        	            scale : 'large',
        	            iconCls : 'icon-bookmark-32',
        	            handler: function() {
                	        var form = this.up('form').getForm();
        		            var loc_win = this.up('window');
        			            if(form.isValid()){
        			            
        			            
        			            Ext.Ajax.request({
						        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check_ordine',
							        method     : 'POST',
				        			jsonData: {
				        					form_values: form.getValues()
									},							        
							        success : function(result, request){
								  		  var jsonData = Ext.decode(result.responseText);

			                                if (jsonData.success == false){
			                                    acs_show_msg_error(jsonData.msg_error);
			                                    return;
			                                }else{
												acs_show_win_std('Elenco clienti per generazione ordini materiali promozionali', 'acs_genera_ordine_marketing.php?fn=open_grid', {
                        	            		form_values: form.getValues(),
                        	            		form_open : <?php echo acs_je($m_params->form_values); ?>}, 
                        	            		1100, 400, null, 'icon-gift-16');
				                            }
										  							        
							        },
							        failure    : function(result, request){
							        	Ext.getBody().unmask();
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
				   			 });
        			  
        	            				            
        			        }
        						                        	                	                
        	            }
        	        }
        ],             
				
        }
]}
<?php 

exit;
}

if ($_REQUEST['fn'] == 'exe_check_ordine'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    
    $sql = "SELECT TDTIDO
            FROM {$cfg_mod_Spedizioni['file_testate_doc_gest_cliente']} TD0
            WHERE TDDT = '{$id_ditta_default}' AND TDAADO = {$form_values->f_anno} 
            AND TDNRDO = '{$form_values->f_numero}' AND TDTPDO = '{$form_values->f_tipo}'";

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if($row == false || $row['TDTIDO'] != 'VO'){
        $ret['success'] = false;
        $ret['msg_error'] = 'Ordine con tipologia diversa da VO';
        echo acs_je($ret);
        return;
    }
       
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


if ($_REQUEST['fn'] == 'open_grid'){
    ?>

{"success":true, "items": [
			 {
			xtype: 'grid',
			loadMask: true,	
			autoScroll: true,
			multiSelect : true,
			plugins: [
		    Ext.create('Ext.grid.plugin.CellEditing', {
	            clicksToEdit: 1
	            })
	      	],
      	    stateful: true,
			stateId: 'elenco_clienti_opm',
			stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow'],
			features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
        	}],	
	  		selModel: {selType: 'checkboxmodel', checkOnly: true},
			store: {
						xtype: 'store',
						autoLoad: true,
			             	proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_cli', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							           extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>,
									},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['espo', 'promo', 'agente', 'd_age', 'cliente', 'd_cli', 'pven',
		        			         'k_cli', 'nazi', 'prov', 'indi', 'loca', 'cod_d', 'data_ge', 'box']							
									
			}, //store
			<?php  $pven  = "<img src=" . img_path("icone/48x48/clienti.png") . " height=20>"; ?>
			<?php  $promo = "<img src=" . img_path("icone/48x48/gift.png") . " height=20>"; ?>
			<?php  $espo  = "<img src=" . img_path("icone/48x48/blackboard.png") . " height=20>"; ?>
			
			columns: [	
		        {
                	header   : 'Codice',
                	dataIndex: 'agente',
                	width: 50,
                	filter: {type: 'string'}, filterable: true
	            },
	            {
	                header   : 'Agente',
	                dataIndex: 'd_age',
	                width: 100,
	                filter: {type: 'string'}, filterable: true
	            },
				{
	                header   : 'Codice',
	                dataIndex: 'cliente',
	                width: 70,
	                filter: {type: 'string'}, filterable: true
	            },
	            {
	                header   : 'Cliente',
	                dataIndex: 'd_cli',
	                flex : 1,
	                filter: {type: 'string'}, filterable: true
	            },{
	                header   : 'Indirizzo',
	                dataIndex: 'indi',
	                width: 100
	            },{
	                header   : 'Localit&agrave;',
	                dataIndex: 'loca',
	                width: 100
	            },
	             {
                	header   : 'Prov.',
                	dataIndex: 'prov',
                	width: 50,
                	filter: {type: 'string'}, filterable: true
	            },
	             {
                	header   : 'Naz.',
                	dataIndex: 'nazi',
                	width: 50,
                	filter: {type: 'string'}, filterable: true
	            },
	            {text: '<?php echo $pven; ?>',	
    			   width: 30, 
    			   dataIndex: 'pven',
    			   tooltip: 'Cliente con punto vendita',
    			   renderer: function(value, p, record){
    				  if(record.get('pven') > 0)
    	    			 return '<img src=<?php echo img_path("icone/48x48/clienti.png") ?> width=15>';
    	    		 	
    	    	   }
    		     },
    		      {text: 'Nr', 	
    			   width: 30, 
    			   dataIndex: 'pven',
    			   tooltip: 'Numero punti vendita',
    			   
    		     },
    		     { text: '<?php echo $promo; ?>',	
    			   width: 30, 
    			   dataIndex: 'promo',
    			   tooltip : 'Genera ordini materiali promozionali',
    			   renderer: function(value, p, record){
    				  if(record.get('pven') > 0 && record.get('promo') > 0)
    	    			 return '<img src=<?php echo img_path("icone/48x48/gift.png") ?> width=15>';
    	    		 	
    	    	   }
    		     },
    		      { text: '<?php echo $espo; ?>',	
    			   width: 30, 
    			   dataIndex: 'espo',
    			   tooltip : 'Cliente con espositori',
    			   renderer: function(value, p, record){
    				  if(record.get('espo') > 0)
    	    			 return '<img src=<?php echo img_path("icone/48x48/blackboard.png") ?> width=15>';
    	    		 	
    	    	   }
    		     },{
                	header   : 'Box',
                	dataIndex: 'box',
                	width: 40,
                	filter: {type: 'string'}, filterable: true
	            },{
                	header   : 'Co.D',
                	dataIndex: 'cod_d',
                	tooltip : 'Codice destinazione',
                	width: 40,
                	filter: {type: 'string'}, filterable: true
	            },{
                	header   : 'Immissione',
                	dataIndex: 'data_ge',
                	width: 75,
                	renderer: date_from_AS,
                	filter: {type: 'string'}, filterable: true
	            },
	         ],
	          listeners: {
	          
	          celldblclick: {				  							
				  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				  iEvent.stopEvent();
				  var col_name = iView.getGridColumns()[iColIdx].dataIndex;
				  var rec = iView.getRecord(iRowEl);
				 
				  if(col_name == 'pven' && rec.get('pven') > 0){
				  
			     		list_selected_id = [];
			     		list_selected_id.push({k_cli_des : rec.get('k_cli')});
			     		
		     			my_listeners = {
        					afterAssegna: function(from_win, dest){	
        						rec.set('cod_d', dest);        						
        						from_win.close();
				        		}
		    				};
    			      
    			        acs_show_win_std('Assegna destinazione cliente', '../desk_vend/acs_form_json_assegna_destinazione_finale.php', {
	            		list_selected_id : list_selected_id}, 
	            		800, 300, my_listeners, 'iconDestinazioneFinale');
				  }
						    	
						    	
			    }}
	          },
	         buttons: [	
				{
			            text: 'Conferma',
				        iconCls: 'icon-button_blue_play-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                var win = this.up('window');
			            	var grid = this.up('window').down('grid');
			            	id_selected = grid.getSelectionModel().getSelection();
    			     		list_selected_id = [];
    			     		for (var i=0; i<id_selected.length; i++){
    			     		    list_selected_id.push(id_selected[i].data);
    			     		}
    			     		
    			     		if (list_selected_id.length == 0){
                    			acs_show_msg_error('Selezionare almeno un cliente');
                    			return false;			
                    		}
    						
							Ext.Ajax.request({
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera',
						        jsonData: {
						        	form_values: <?php echo acs_je($m_params->form_values); ?>,
						        	list_selected_id : list_selected_id,
						        	 
						        	},
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
						             if(jsonData.success == false){
					      	    		 acs_show_msg_error('Errore in fase di generazione');
							        	 return;
				        			  }else{
				        			    acs_show_msg_info('Ordine promozionale generato');
				        			     win.close(); 
				        			  }
						        	   										        	
						        }
						    });	            			
	            					                   	                	                
	            		
			        }
		        }
		        
		        ] 	
	         
	         	
		} <!-- end grid -->
]}
<?php 

exit;
}