<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$itin_obj = new Itinerari();
$itin_obj->load_rec_data_by_k(array('TAKEY1' => $itin_id));
$start_point = $s->get_start_map('AR', $itin_obj->rec_data['TAASPE']);


?>



<!DOCTYPE html>
<html>
<head>
 
<link rel="stylesheet" href="core.css">


<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/optimap_style.css">
<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/jquery-ui-1.8.16.custom.css">


<script src="<?php echo site_protocol();?>ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo site_protocol();?>ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>


  
<script type="text/javascript" src="<?php echo site_protocol();?>www.google.com/jsapi"></script>


<script type="text/javascript" src="../../../extjs/ext-all.js"></script>

 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/google-code-prettify/prettify.js"></script>
 
 <!--  optimap -->
 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/optimap/BpTspSolver.js"></script>
 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/optimap/tsp.js"></script>  

 <style type="text/css">
  .page-sub-title{font-size: 0.8em;} 
  span.clienti{font-size: 0.7em; color: gray;}
  span.clienti_alt{font-size: 0.7em; color: gray;}
  span.ind{font-size: 0.7em; color: black;}
  .ui-accordion .ui-accordion-content-active {max-height: 500px;}  
  input.calcButton{font-size: 12px; width: 170px;}  
  
  
 /* PER COMPATTARE VISUALIZZAZIONE ELENCO SCARICHI */ 
  .ui-accordion .ui-accordion-content {padding: 5px;}
  .ui-helper-reset {   
    line-height: 1;
  }  
  ul.unsortable li {
    margin: 0 3px 3px;
    padding: 1px;
  }  
  #sortable li {
    margin: 0 1px 1px;
    padding: 2px 2px 5px 5px;
  }  
  ul#sortable{font-size: 18px;}
  span.clienti{font-size: 10px; color: gray;}
  span.ind{font-size: 10px; color: black;}
  
  /* PER MOSTRARE solo il primo cliente */
  span.clienti {
    block-size: 1em;
    display: block;
    overflow: hidden;
  }
  
  span.clienti_alt {
    display: none;
  }  
    
 </style>
 
<script type="text/javascript">

  var markers_data = [];
  var applica_sequenza_iniziale = false;
  
  jQuery.noConflict();
  function onBodyLoad() {
	  window.moveTo(0, 0);
	  window.resizeTo(screen.availWidth, screen.availHeight);
  
	  google.load("maps", "3", {callback: init, other_params:"key=<?php echo site_gmap_key(); ?>"});
  }

  function init() {
  	if (google.loader.ClientLocation != null) {
		latLng = new google.maps.LatLng(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
		loadAtStart(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude, 8);
	} else {
		loadAtStart(37.4419, -122.1419, 8);
	}


  	//aggiungo start_point
	 tsp.addWaypoint(new google.maps.LatLng(<?php echo $start_point['LAT']; ?>, <?php echo $start_point['LNG']; ?>), '<?php echo $start_point['DESC']; ?>', addWaypointSuccessCallbackZoom);  		 
	 //tsp.addWaypoint(new google.maps.LatLng(37.4419, -122.1419, 8), 'aaaaaa', addWaypointSuccessCallbackZoom);

	//aggiungo i punti di scarico  	
    //var xhr = jQuery.getJSON('acs_form_json_progetta_spedizioni.php?fn=grid_data&for_gmap=Y&<?php echo "sped_id={$sped_id}&data={$data}&tpca={$carico_exp['TDTPCA']}&aaca={$carico_exp['TDAACA']}&nrca={$carico_exp['TDNRCA']}" ?>');  	
    //xhr.done(loadResults);

    //carico la soluzione iniziale    
    //xhr.done(loadPrimaSoluzione);
	 var geocoder = new google.maps.Geocoder();
	 geocoder.geocode( { 'address': <?php echo acs_je($_REQUEST['ind']) ?>}, function(results, status) {
		 if (status == google.maps.GeocoderStatus.OK) {
			 console.log(results);
			 console.log(results[0].geometry.location.lat());
			 console.log(results[0].geometry.location.lng());
			 tsp.addWaypoint(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()), <?php echo acs_je($_REQUEST['ind']) ?>, addWaypointSuccessCallbackZoom);
		 			 
			 directions(1, document.forms['travelOpts'].walking.checked, document.forms['travelOpts'].bicycling.checked, document.forms['travelOpts'].avoidHighways.checked, document.forms['travelOpts'].avoidTolls.checked)			 	
		} else {
		 alert("Geocode was not successful for the following reason: " + status);
	 	}
	});
	    
}


  function loadPrimaSoluzione (data) {
	  var numPerm = new Array(data.points.length + 2);
	  numPerm[0] = 0;
	  for (var i = 1; i <= data.points.length; i++) {
	    numPerm[i] = i;
	  }
	  numPerm[numPerm.length - 1] = numPerm.length - 1;
	  //bestPath = new Array(data.points.length + 2);
	  bestPath = numPerm;

	  applica_sequenza_iniziale = true;
		directions(0, document.forms['travelOpts'].walking.checked, document.forms['travelOpts'].bicycling.checked, document.forms['travelOpts'].avoidHighways.checked, document.forms['travelOpts'].avoidTolls.checked);
					  
  }



  function loadResults (data) {
      addressCount = data.points.length;
      recordAggiunti = 0;
            
      if (data.points.length > 0) {
        items = data.points;
        
        for (var i = 1; i <= items.length; i++) {          

		  	record = items[i-1];
        	record['counter'] = i;
			record['lat'] = record['LAT'];
			record['lng'] = record['LNG'];
			record['localita'] = record['TDDLOC'].trim();

			if (record['TDNAZD'] == 'ITA' || record['TDNAZD'] == 'IT')
				record['localita'] += " (" + record['TDPROD'].trim() + ")";
			else				
				record['localita'] += " (" + record['TDDNAD'].trim() + ")";				
			
			record['localita'] += "<br><span class=ind>[" + record['TDDCAP'].trim() + ", " + record['TDIDES'].trim() + "]</span>"; 
			record['clienti'] = record['TDDCON'].trim();

			//scorro markers_data per verificare se (in base alle coordinate) il punto esiste gia'
			record['DUPLICATO'] = 'N';
			for (var j = 0; j < markers_data.length; j++) {
				if ((markers_data[j]['lat'] == record['lat'] && markers_data[j]['lng'] == record['lng'])){
					record['DUPLICATO'] = 'Y';
					//markers_data[j]['clienti'] += '<br> >' + record['clienti'].trim();
					markers_data[j]['view_clienti_alt'] = '<span class=span_clienti_alt><A class=view_clienti_alt onclick="show_clienti_alt(this);"> &nbsp;[Altri] </A></span>';					
					markers_data[j]['clienti_alt'] += '>' + record['clienti'].trim() + '<br>';
				} 
			}
			
			if (record['DUPLICATO'] != 'Y'){
				recordAggiunti++;
	        	record['counter'] = recordAggiunti;
	        	record['view_clienti_alt'] = '';	        
	        	record['clienti_alt'] = ''		
		        	        	
				record['icon'] = {
						'url' : 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + recordAggiunti + '|FF0000|0000FF',
						'size': new google.maps.Size(32, 32)
				};	        	
				if (record['ha_coordinate'] == 'Y')
					markers_data.push(record);
				else 
					errors_data.push(record);
			}
        }

  		addPoints(markers_data);
        
      }

    }



  function show_clienti_alt(el){
	  jQuery(el).parent().parent().parent().find(".clienti_alt").toggle();
  }
  

  function addPoints(markers_data){
	for (var j = 0; j < markers_data.length; j++) {	  
	  tsp.addWaypoint(new google.maps.LatLng(markers_data[j]['LAT'], markers_data[j]['LNG']), markers_data[j]['localita'] + '<br><span class=clienti>' + markers_data[j]['clienti'] + markers_data[j]['view_clienti_alt'] +  '</span>'  + '<span class=clienti_alt>' + markers_data[j]['clienti_alt'] + '</span>', addWaypointSuccessCallbackZoom);
	}	  
  }	  





  
function toggle(divId) {
  var divObj = document.getElementById(divId);
  if (divObj.innerHTML == "") {
    divObj.innerHTML = document.getElementById(divId + "_hidden").innerHTML;
    document.getElementById(divId + "_hidden").innerHTML = "";
  } else {
    document.getElementById(divId + "_hidden").innerHTML = divObj.innerHTML;
    divObj.innerHTML = "";
  }
}

jQuery(function() {
	
  jQuery( "#accordion" ).accordion({
    collapsible: true,
	autoHeight: false,
	clearStyle: true,
	active:"h3.auto_open"
  });
  jQuery("input:button").button();
  jQuery("#dialogProgress" ).dialog({
    height: 140,
	modal: true,
	autoOpen: false
  });
  jQuery("#savingProgress" ).dialog({
	    height: 140,
		modal: true,
		autoOpen: false
	  });  
  jQuery("#progressBar").progressbar({ value: 0 });
  jQuery("#dialogTomTom" ).dialog({
    height: 480,
	width: 640,
	modal: true,
	autoOpen: false
  });
  jQuery("#dialogGarmin" ).dialog({
    height: 480,
	width: 640,
	modal: true,
	autoOpen: false
  });


  jQuery( "a.view_clienti_alt" ).click(function() {
	  console.log('aaaaaaaaaaaaaaaaaaaaaa');
	});
  
});

(function() {
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  po.src = 'https://apis.google.com/js/plusone.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

/* 
  var ar_clienti_alt = document.getElementsByClassName('view_clienti_alt');
  console.log(ar_clienti_alt);
  for (var i = 0; i < ar_clienti_alt.length; i++) {
	  ar_clienti_alt[i].addEventListener('onclick', (function(i) {
		  	console.log('aaaaaaaaaaaaaabbbbbbbbbbbb');
	  })(i), false);
  }
*/  
  
})();




</script>
 
 
 
</head>



<body onLoad="onBodyLoad()">


    <div id="header">
     <table width="100%">
      <tr><td><img src=<?php echo img_path("icone/48x48/cartello.png") ?>></td>
          <td>
          	<span class="page-title"><strong>Desktop Ordini di Vendita</strong> - Calcola percorso</span>
          </td>
          <td>
			 <div style="float: right;">
			  <!-- <input id="button1" class="calcButton" type="button" value="Calcola percorso ottimale" onClick="directions(0, document.forms['travelOpts'].walking.checked, document.forms['travelOpts'].bicycling.checked, document.forms['travelOpts'].avoidHighways.checked, document.forms['travelOpts'].avoidTolls.checked)">-->
			  <!-- <input id="revBt" class="calcButton" type="button" value="Inverti sequenza" onClick="reverseRoute()"> -->  
			  <!-- <input id="revBt" class="calcButton" type="button" value="Applica sequenza" onClick="applicaSequenza()"> -->  
			  <input style="display: none" id="button2" class="calcButton" type="button" value="Calculate Fastest A-Z Trip" onClick="directions(1, document.forms['travelOpts'].walking.checked, document.forms['travelOpts'].bicycling.checked, document.forms['travelOpts'].avoidHighways.checked, document.forms['travelOpts'].avoidTolls.checked)">
			  <input  style="display: none;" id='button3' class="calcButton" type='button' value='Start Over Again' onClick='startOver()'>
			 </div>          
          </td>
      </table>
    </div>


<div id="fb-root"></div>


<table class='mainTable'>
<tr>
  <td class='left' style='vertical-align: top'>
  <div id="leftPanel">
  <div id="accordion" style='width: 300pt'>
  <h3 style="display: none"><a href="#" class='accHeader'>Destinations</a></h3>
  <div>
    <form name="address" onSubmit="clickedAddAddress(); return false;">
    Add Location by Address: 
    <table><tr><td><input name="addressStr" type="text"></td>
    <td><input type="button" value="Add!" onClick="clickedAddAddress()"></tr>
    </table>
    </form> or <a href="#" onClick="toggle('bulkLoader'); document.listOfLocations.inputList.focus(); document.listOfLocations.inputList.select(); return false;">
    Bulk add by address or (lat, lng)</a>.
    <div id="bulkLoader"></div>
  </div>

  <h3 style="display: none"><a href="#" class='accHeader'>Route Options</a></h3>
  <div>
    <form name="travelOpts">
    <input id="walking" type="checkbox"/> Walking<br>
    <input id="bicycling" type="checkbox"/> Bicycling<br>
    <input id="avoidHighways" type="checkbox"/> Avoid highways<br>
    <input id="avoidTolls" type="checkbox"/> Avoid toll roads
    </form>
  </div>

  <h3 style="display: none;"><a href="#" class='accHeader'>Export</a></h3>
  <div>
    <div id="exportGoogle"></div>
    <div id="exportDataButton"></div>
    <div id="exportData"></div>
    <div id="exportLabelButton"></div>
    <div id="exportLabelData"></div>
    <div id="exportAddrButton"></div>
    <div id="exportAddrData"></div>
    <div id="exportOrderButton"></div>
    <div id="exportOrderData"></div>
    <div id="garmin"></div>
    <div id="tomtom"></div>
    <div id="durations" class="pathdata"></div>
    <div id="durationsData"></div>
  </div>

  <h3 class="auto_open"></h3>
  <div>
    <div id="routeDrag"></div>
    <div style="display: none;" id="reverseRoute"></div>
  </div>

  <h3 style="display: none;"><a href="#" class='accHeader'>Help</a></h3>
  <div>
  <p>To add locations, simply left-click the map or enter an address
  either in the single address field, or in the bulk loader. </p>
  <p>The first location you add is considered to be the start 
  of your journey. If you click 'Calculate Fastest Roundtrip', it will
  also be the end of your trip. If you click 'Calculate Fastest A-Z Trip',
  the last location (the one with the highest number), will be the final
  destination.</p>
  <p>To remove or edit a location, click its marker.</p>
  <p>If more than 15 locations are specified, you are not guaranteed
  to get the optimal solution, but the solution is likely to be close
  to the best possible.</p>
  <p>You can re-arrange
  stops after the route is computed. To do this, open the 'Edit Route'
  section and drag or delete locations.</p>
  </div>

  <h3 style="display: none;"><a href="#" class='accHeader'>About</a></h3>
  <div>
  <p><span class="red">Version 4</span>&nbsp;<a href="http://gebweb.net/blogpost/2012/01/25/optimap-version-4-is-here/">Read about the new
  version, and post comments, bugs and suggestions</a>.
  <p>How it works: <a href="http://gebweb.net/blogpost/2007/07/05/behind-the-scenes-of-optimap/">Behind the Scenes of OptiMap</a></p>
  <p>Use on your website: <a href="http://gebweb.net/blogpost/2007/08/26/optimize-your-trips/">Optimize Your Trips</a></p>
  <p>
   The solver <a href="http://code.google.com/p/google-maps-tsp-solver/">
   source code</a> is available under the MIT license. If you are
   interested in
   knowing about updates to this code, please subscribe to
   <a href="http://groups.google.com/group/google-maps-tsp-solver">
   this mailing list</a>.</p>
  <p>
   You can specify a default starting position and zoom level,
   by adding http GET parameters center and zoom. E.g
   <a href="http://gebweb.net/optimap/index.php?center=(60,10)&amp;zoom=6">http://gebweb.net/optimap/index.php?center=(60,10)&amp;zoom=6</a>.</p>
  <p>Up to 100 locations are accepted.</p>
  </div>
  
  </div>
  
<div id="donateDiv">

<div id="flattrDonate">
<a class="FlattrButton" style="display:none;" rev="flattr;button:compact;" href="http://www.optimap.net"></a>
<noscript><a href="http://flattr.com/thing/470626/OptiMap" target="_blank">
<img src="http://api.flattr.com/button/flattr-badge-large.png" alt="Flattr this" title="Flattr this" border="0" /></a></noscript>
</div>

  </div>
  </div>

  </td>
  <td class='right' style='vertical-align: top'>
  <div id="map" class="myMap"></div>
  <div id="path" class="pathdata"></div>
  <div id="my_textual_div"></div>
  </td>
</tr>
</table>

<!-- Hidden stuff -->
<div id="bulkLoader_hidden" style="visibility: hidden;">
  <form name="listOfLocations" onSubmit="clickedAddList(); return false;">
  <textarea name="inputList" rows="10" cols="70">One destination per line</textarea><br>
  <input type="button" value="Add list of locations" onClick="clickedAddList()">
</form></div>
<div id="exportData_hidden" style="visibility: hidden;"></div>
<div id="exportLabelData_hidden" style="visibility: hidden;"></div>
<div id="exportAddrData_hidden" style="visibility: hidden;"></div>
<div id="exportOrderData_hidden" style="visibility: hidden;"></div>
<div id="durationsData_hidden" style="visibility: hidden;"></div>

<div id="dialogProgress" title="Calcolo percorso...">
<div id="savingProgress" title="Salvataggio percorso...">
<div id="progressBar"></div>
</div>

<div id="dialogTomTom" title="Export to TomTom">
<iframe name='tomTomIFrame' style='width: 580px; height: 400px'></iframe> 
</div>

<div id="dialogGarmin" title="Export to Garmin">
<iframe name='garminIFrame' style='width: 580px; height: 400px'></iframe>
</div>

</body>
</html>
