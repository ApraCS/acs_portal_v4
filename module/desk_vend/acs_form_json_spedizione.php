<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();





/********************************************************
 * RICHESTA salvataggio spedizione collegata
 ********************************************************/
if ($_REQUEST['fn'] == 'exe_verifica_salva_sped_coll'){

	$m_params = acs_m_params_json_decode();
	
	//sped_id 	(sped. di partenza)
	//sped_coll	(sped. collegata)
	
	//Verifiche di ammissibilita'
	// 1) sped.collegata non sia gia' abbinata ad altra spedizione (eventualmente prima deve essere rimosso il collegamento. Funzione da fare)
	// 2) le due spedizioni abbiano stesso itinerario/vettore e giorno di evasione programmata (o di spedizione?)

	//Imposto il collegamento tra le due spedizioni (se risulta ammissibile) - campo CSNSPC
	// 1) Nella sped. di partenza abbino la sped. collegata
	// 2) Nella sped. collegata abbino la sped. di partenza
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} SET CSNSPC = ?, CSFG02 = ?
			WHERE CSDT = '{$id_ditta_default}' AND CSCALE = '*SPR' AND CSPROG = ? ";	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($m_params->sped_id,   'S', $m_params->sped_coll));
	$result = db2_execute($stmt, array($m_params->sped_coll, '', $m_params->sped_id));		
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



/********************************************************
 * RICHESTA rimozione spedizione collegata
 ********************************************************/
if ($_REQUEST['fn'] == 'exe_rimuovi_sped_coll'){

	$m_params = acs_m_params_json_decode();

	//sped_id 	(sped. di partenza)	
	$sped = $s->get_spedizione($m_params->sped_id);
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} SET CSNSPC = 0
			WHERE CSDT = '{$id_ditta_default}' AND CSCALE = '*SPR' AND CSPROG = ? ";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($m_params->sped_id));
	$result = db2_execute($stmt, array($sped['CSNSPC']));
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}
	



// ******************************************************************************************
// SELEZIONE ITINERARIO da area di spedizione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'select_itin'){
	//recupero area spedizione selezionata
	$rec_id_exp = explode("|", $m_params->rec_id);
	$aspe = trim($rec_id_exp[2]);
	 ?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',

            items: [
            			{xtype: 'hidden', name: 'create_new', 		value: 'Y'},
            			{xtype: 'hidden', name: 'from_area', 		value: 'Y'},
            			{xtype: 'hidden', name: 'rec_id', 			value: <?php echo j($m_params->rec_id); ?>},
            			{xtype: 'hidden', name: 'rec_liv', 			value: <?php echo j($m_params->rec_liv); ?>},
            			{xtype: 'hidden', name: 'tree_id', 			value: <?php echo j($m_params->tree_id); ?>},            			            			
						{
								name: 'itin_id',
								xtype: 'combo',
								fieldLabel: 'Itinerario',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}, 'TAVOLU', 'TAPESO', 'TABANC', 'TATISP', 'TATITR', 'TARGA'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('ITIN', null, 'N', 'N', null, $aspe), ""); ?>								     
								    ]
								},
								
						 }                	
                	]
                	
			, buttons: [{
	            text: 'Continua',
	            handler: function() {
	            	var form = this.up('form').getForm();
	                
				    m_win = this.up('window');
				    var loc_m_win = this.up('window');	                
					if(form.isValid())
						loc_m_win.close();
						//apro la form per la creazione della spedizione
							acs_show_win_std('Creazione nuova spedizione', 'acs_form_json_spedizione.php', 
								    			  		form.getValues(), 600, 550);							

		           }            	                	                
	        }]
                	
                	
                	
               }
           ]
       }         	
                	            


<?php exit; }



$sottostringhe = explode("|", $m_params->rec_id);
 
$itin_id = $m_params->itin_id;
$rec_liv = $m_params->rec_liv;

$fn = "upd_spedizione";

if ($m_params->create_new != "Y"){
	if ($m_params->from_area_itin == 'Y'){	
		$ar_liv3 = $sottostringhe[6];
		$ar_liv3_exp = explode("-", $ar_liv3);
		$sped_id = $ar_liv3_exp[1];		
	}
	else if ($m_params->from_sped_id == 'Y')
		$sped_id = $m_params->sped_id;
	else
		$sped_id = $sottostringhe[2];
			
	$sped 		= $s->get_spedizione($sped_id);
	$itin_id 	= $sped['CSCITI']; 
	
}
else {
	$sped = array();
	
	if (isset($m_params->data)){
		$sped['CSDTSP'] = $m_params->data;
	}
		
	$fn = 'crt_spedizione';	
}


if ($m_params->from_area_itin == 'Y'){
	$itin_id = $sottostringhe[4];	
}
if ($m_params->from_area == 'Y'){
	$itin_id = $m_params->itin_id;
}

$tree_id = $m_params->tree_id; //id (extjs) albero su cui ho fatto click
$rec_id = $m_params->rec_id; //id (extjs) albero su cui ho fatto click


//verifico integrita' della spedizione, altrimenti non puo' essere modificata
$sql = "SELECT TDDTEP, TDCITI, COUNT(*) AS N_ROW
			 FROM {$cfg_mod_Spedizioni['file_testate']}
			WHERE " . $s->get_where_std() . " AND TDNBOC = ?
			GROUP BY TDDTEP, TDCITI ";
$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, array($sped_id));

	//se ho piu' di un risultato.... anomalie
	$r_anomalie = array();
	while ($row = db2_fetch_assoc($stmt)){
		$r_anomalie[] = $row;
	}
	
	if (count($r_anomalie) > 1){
		//riporto l'errore
		$txt_errore = "Ordini:
		Impossibile modificare la spedizione.....

		Riepilogo ordini per data/itinerario:

				";
		
		foreach($r_anomalie as $r){
			$txt_errore .= "
Data: " . print_date($r['TDDTEP']) . " - Itinerario: " . $r['TDCITI'] . " - Num.Ordini: " . $r['N_ROW'];	
		}
		
		$txt_errore .= "
";
		
		?>
		
		{"success":true, "items": [
			{
	            xtype: 'textareafield',
	            name: 'f_text',
	            fieldLabel: 'Errore',
	            height: 200,
	            anchor: '96%',
			    allowBlank: false,
			    value: <?php echo acs_je($txt_errore); ?>            
        	}
        ]}		
		
		<?php 		
		exit;
	}


if ($m_params->create_new != "Y"){	
	//recupero la max data disponibilita' degli ordini della spedizione,
	//perche' non potro' selezionare una data prima di questa
	$sql = "SELECT MAX(TDDTDS) AS MAX_DISPO
			 FROM {$cfg_mod_Spedizioni['file_testate']} 
			 WHERE " . $s->get_where_std() . " AND TDNBOC = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($sped_id));
	$row = db2_fetch_assoc($stmt);
	
	if ($row['MAX_DISPO'] > 0)
	 $min_data_AS = $row['MAX_DISPO'];
	else
	 $min_data_AS = oggi_AS_date();
} else {
	
	if (isset($m_params->data)){
		$min_data_AS = $m_params->data;
		$max_data_AS = $m_params->data;
	} else {
		$min_data_AS = oggi_AS_date();
	}
}	 	


$it = new Itinerari();
$it->load_rec_data_by_k(array('TAKEY1' => $itin_id));
if (trim($it->rec_data['TARIF1']) != '')
	$itin_per_assegna = trim($it->rec_data['TARIF1']);
else 
	$itin_per_assegna = trim($itin_id);


//dall'area di spedizione (recuperata da itinerario) recuper i giorni dopo dtep nell'assegnazione dell'area di spedizione
$aspe = new AreeSpedizione();
$aspe->load_rec_data_by_itin($itin_id);
$aspe_parameters = $aspe->parse_YAML_PARAMETERS($aspe->rec_data);
$min_gg_params = explode(",", $aspe_parameters['data_sped_min_gg']);

if (count($min_gg_params) != 3){
	//non ho i valori impostati
	$gg_dopo_dtep = 1;
} else {
	$gg_dopo_dtep = $min_gg_params[0];
}



?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
			listeners: {
	 			afterrender: function (comp) {
	 			
		 			m_w = comp.up('window');	 			
	 			
					<?php if ($m_params->create_new != "Y"){ ?>	 			
		 				m_w.setTitle(m_w.title + ' #<?php echo $sped_id; ?>');	 			
	 				<?php } ?>
	 				
	 					m_w.setTitle(m_w.title + ', Itinerario: ' + <?php echo j($s->decod_std('ITIN', $itin_id) . " [" . trim($itin_id) . "]");?>);
	 			}
	 		},	 			
            
            
            
            
            
            items: [
            	<?php if ($sped['CSFG04'] == 'F'){ ?>
            		{
						html: '<h1><center>SPEDIZIONE FLIGHT</center></h1>',
						height: 25
					},
				<?php } ?>	
            
            
            		{
                	xtype: 'hidden',
                	name: 'fn',
                	value: <?php echo j($fn) ?>
                	},{
                	xtype: 'hidden',
                	name: 'sped_id',
                	value: '<?php echo $sped_id ?>'
                	},{
                	xtype: 'hidden',
                	name: 'itin_id',
                	value: '<?php echo $itin_id ?>'
                	},
                	
                	
                	// IN UPD_SPEDIZIONE PER ORA NON PERMETTO LA MODIFICA DELLA DATA
					<?php 
						if ($m_params->create_new == "Y")
							 $data_disabled = false;
						else
							$data_disabled = true;
						
					?>                		

				, {
				        xtype: 'fieldcontainer',
				        fieldLabel: '',
						defaultType: 'textfield',
	 			        layout: 'hbox',
	 			        items: [					
					
						<?php echo_je(a15(n_o(extjs_datefield('f_data', 'Data prod/dispon', $sped['CSDTSP'], 
 			        	        array('disabled'   => $data_disabled,
 			        	              'minValue'   => $min_data_AS,
 			        	              'maxValue'   => $max_data_AS))))) ?>,
					]
				}
						

					   , {
					        xtype: 'fieldcontainer',
					        fieldLabel: 'Data spedizione',
							defaultType: 'textfield',
		 			        layout: 'hbox',		 			        
		 			        items: [
		 			        
								<?php if ($sped['CSFG04'] == 'F'){
    								    //recupero la max data ev. prog degli ordini (per TDNBOF)
    								    //perche' non potro' selezionare una data prima di questa
    								    $sql = "SELECT MAX(TDDTEP) AS MAX_TDDTEP
    			                                 FROM {$cfg_mod_Spedizioni['file_testate']}
    			                                 WHERE " . $s->get_where_std() . " AND TDNBOF = ?";
    								    $stmtD = db2_prepare($conn, $sql);
    								    $result = db2_execute($stmtD, array($sped_id));
    								    $rowD = db2_fetch_assoc($stmtD);
    								    
    								    $data_spedizione_AB = false;
    								    $data_spedizione_minValue = date('Ymd', strtotime($rowD['MAX_TDDTEP'] . " + {$gg_dopo_dtep} days"));
								    								   
								} else {
    	                               $data_spedizione_AB = true;
	                                   $data_spedizione_minValue = date('Ymd', strtotime($sped['CSDTSP'] . " + {$gg_dopo_dtep} days"));	  
								}
		 			        
								 
		 			        	 echo_je(a15(extjs_datefield('f_data_spedizione', null, $sped['CSDTPG'], 
		 			        	         array('allowBlank' => $data_spedizione_AB, 'minValue'   => $data_spedizione_minValue))));
		 			        	 echo ",";       
		 			        	 echo_je(a15(n_o(extjs_timefield('f_ora', 'Ora spedizione', $sped['CSHMPG'], 
		 			        	        array('labelAlign'   => 'right', 'margin' => '0 0 0 15')))));
		 			        	 ?>       
		 			        
		 			        ]
		 			      }						
						
						
					   , {
					        xtype: 'fieldcontainer',
					        fieldLabel: 'Data inizio carico',
							defaultType: 'textfield',
		 			        layout: 'hbox',
		 			        bodyPadding: 2,		 			        
		 			        items: [
		 			        		 		
		
		 			        		 			        
		 			        	<?php echo_je(a15(n_o(extjs_datefield('f_data_inizio_carico', null, $sped['CSDTIC'], 
		 			        	        array('minValue'   => date('Ymd', strtotime($sped['CSDTSP'] . " -7 days"))))))) ?>,
		 			        	        
								<?php echo_je(a15(n_o(extjs_timefield('f_ora_inizio_carico', 'Ora inizio carico', $sped['CSHMIC'], 
		 			        	        array('labelAlign'   => 'right', 'margin' => '0 0 0 15'))))) ?>
		 			        
		 			        ]
		 			      }							
						
						
					   , {
					        xtype: 'fieldcontainer',
					        fieldLabel: 'Data scarico',
							defaultType: 'textfield',
		 			        layout: 'hbox',		
		 			        items: [
		 			        
		 			          <?php echo_je(a15(n_o(extjs_datefield('f_data_inizio_scarico', null, $sped['CSDTSC'], 
		 			        	         array('minValue'   => $sped['CSDTSP']))))) ?>,
		 			        
		 			         <?php echo_je(a15(n_o(extjs_timefield('f_ora_inizio_scarico', 'Ora scarico', $sped['CSHMSC'], 
		 			                     array('labelAlign'   => 'right', 'margin' => '0 0 0 15'))))) ?>
		 			        	        
		 			        ]
		 			      },
						
						
								{
										name: 'f_itvet',
										xtype: 'combo',
										fieldLabel: 'Vettore / Mezzo',
										forceSelection: true,								
										displayField: 'text',
										valueField: 'id',								
										emptyText: '- seleziona -',
								   		allowBlank: false,
								   		anchor: '-15',	
									    value: <?php if ($m_params->create_new != "Y") 
									    				echo j(implode("|", array(trim($sped['CSCITI']), trim($sped['CSCVET']), trim($sped['CSCAUT']), trim($sped['CSCCON']))));
													 else	echo j(''); ?>,
										store: {
											editable: false,
											autoDestroy: true,
										    fields: [{name:'id'}, {name:'text'}, 'TAVOLU', 'TAPESO', 'TABANC', 'TATISP', 'TATITR', 'TARGA'],
										    data: [								    
										     <?php echo acs_ar_to_select_json($s->find_TA_ITVE('ITVE', $itin_id, implode("|", array(trim($sped['CSCITI']), trim($sped['CSCVET']), trim($sped['CSCAUT']), trim($sped['CSCCON'])))), "", false); ?>	
										    ]
										},
										listeners: {
											'select': function(combo, row, index) {
											     		//aggiorno VOLUME/PESO/PALLETT DA itve
											     			var m_form = this.up('form').getForm();
											     			m_form.findField('f_volume_disp').setValue(row[0].data.TAVOLU);
											     			m_form.findField('f_pallet_disp').setValue(row[0].data.TABANC);					     		
											     			m_form.findField('f_peso_disp').setValue(row[0].data.TAPESO);									     		
											     			m_form.findField('f_tipologia').setValue(row[0].data.TATISP);					     		
											     			m_form.findField('f_trasporto').setValue(row[0].data.TATITR);									     			
											     			m_form.findField('f_targa').setValue(row[0].data.TARGA);									     			
						            					}
										}
										
								 }
						 
						 
						 
						 , {
									name: 'f_descr',
									xtype: 'textfield',
									fieldLabel: 'Descrizione',
								    value: <?php echo j(trim($sped['CSDESC'])); ?>,
								    maxLength: 100, anchor: '-15'							
						}, {
					        xtype: 'fieldcontainer',
					        fieldLabel: 'Targa',
							defaultType: 'textfield',
		 			        layout: 'hbox',
		 			        items: [
								{
									name: 'f_targa',
									xtype: 'textfield',
									fieldLabel: '',
								    value: <?php echo j(trim($sped['CSTARG'])); ?>,
								    maxLength: 20,  anchor: '-15',
								    width: 165
								    							
								}, {
									name: 'f_porta',
									xtype: 'combo',
									fieldLabel: 'Porta',
									labelAlign: 'right',
									displayField: 'text',
									valueField: 'id',
									anchor: '35',
									margin: '0 0 0 15',
									labelWidth: 100,
									forceSelection: false, //puo indicare anche una porta non in elenco
								   	allowBlank: true,							
								    maxLength: 10,
								    value: <?php echo j(trim($sped['CSPORT'])); ?>,							
									store: {
										autoLoad: true,
										editable: false,
										autoDestroy: true,	 
									    fields: [{name:'id'}, {name:'text'}],
									    data: [								    
										     <?php echo acs_ar_to_select_json($s->get_elenco_porte_itinerario($itin_id), ""); ?>	
										    ] 
									}						 
								  }		
		 			        ] 
						 }, {
							name: 'f_stato',
							xtype: 'combo',
							fieldLabel: 'Stato spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,							
						    anchor: '-15',
						    value: <?php echo j(trim($sped['CSSTSP'])); ?>,							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('STASP'), ""); ?>	
								    ] 
							}						 
						  }, {
							name: 'f_tipologia',
							xtype: 'combo',
							fieldLabel: 'Tipologia sped.',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
						   	allowBlank: false,							
						    anchor: '-15',
						    value: <?php echo j(trim($sped['CSTISP'])); ?>,							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('TSPE'), ""); ?>	
								    ] 
							}						 
						  }, {
							name: 'f_trasporto',
							xtype: 'combo',
							fieldLabel: 'Tipologia trasp.',
							displayField: 'text',
							valueField: 'id',
						   	allowBlank: true,							
							emptyText: '- seleziona -',
							forceSelection:true,
						    anchor: '-15',
						    value: <?php echo j(trim($sped['CSTITR'])); ?>,							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'), ""); ?>	
								    ] 
							}						 
						  }
						  
						  
						  
						
					   , {
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
		 			        layout: 'hbox',
		 			        items: [						  
						  
						  
								  , {
									name: 'f_pallet_disp',
									xtype: 'numberfield',
									fieldLabel: 'Pallet disponibili',
								    anchor: '-15',
								    value: <?php echo j(trim($sped['CSBAND'])); ?>,							
								  }, {
									name: 'f_volume_disp',
									xtype: 'numberfield',
									fieldLabel: 'Volume disponib.',
									labelAlign: 'right',
								    anchor: '-15',
								    margin: '0 0 0 15',
								    value: <?php echo j(trim($sped['CSVOLD'])); ?>,							
								  }
							]
						}		  
						  

						
						
					   , {
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
		 			        layout: 'hbox',
		 			        items: [
							  , {
								name: 'f_peso_disp',
								xtype: 'numberfield',
								fieldLabel: 'Peso disponibili',
							    anchor: '-15',
							    value: <?php echo j(trim($sped['CSPESO'])); ?>,							
							  }

							, {
						        xtype: 'fieldcontainer',
						        fieldLabel: 'Sped.collegata',
						        labelAlign: 'right',
								defaultType: 'textfield',
			 			        layout: 'hbox',
			 			        items: [
									{
				                	xtype: 'hidden',
				                	name: 'f_sped_coll',
				                	value: <?php echo j(trim($sped['CSNSPC'])); ?>
				                	}, {
										name: 'f_sped_coll_out',
										xtype: 'displayfield',
										editable: false,
										fieldLabel: '',
										width: 40,
									    //value: <?php echo j("<img src=" . img_path("icone/48x48/add_link.png") . " width=16>&nbsp;&nbsp;" . trim($sped['CSNSPC'])); ?>,
									    value: <?php echo j(trim($sped['CSNSPC'])); ?>,
						    							
									  }, {
										name: 'f_sped_coll_out_set',										  
										xtype: 'displayfield',
										editable: false,
										fieldLabel: '',
										padding: '0 0 0 10',
									    value: <?php echo j("<img src=" . img_path("icone/48x48/add_link.png") . " width=16>"); ?>,
										listeners: {
										            render: function( component ) {
										                component.getEl().on('dblclick', function( event, el ) {
										                
										                if (parseInt(component.up('form').getForm().findField('sped_id').getValue().length) == 0){
										                	acs_show_msg_error('Operazione non ammessa in fase di generazione nuova spedizione');
										                	return false;
										                } 
										                										                  
										                  my_listeners = {
										                  	onSpedSelected: function(rec_sped_selected){
										                  		//console.log(rec_sped_selected);
										                  		
										                  		//eseguo l'abbinamento della spedizione (verificando, resettando, ...)	            
																Ext.Ajax.request({
																				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_verifica_salva_sped_coll',
																		        jsonData: {sped_id: component.up('form').getForm().findField('sped_id').getValue(),
																		        		   sped_coll: rec_sped_selected.get('CSPROG')
																		        		  },
																		        method     : 'POST',
																		        waitMsg    : 'Data loading',
																		        success : function(result, request){									        
																		            
																		            jsonData = Ext.decode(result.responseText);
																		            
																		            if (jsonData.success===false){
																						Ext.Msg.alert('Message', 'Errore. Ritornato false');
																						return false;																		            
																		            } 
																		            
																					if (jsonData.success===true){
																                  		component.up('form').getForm().findField('f_sped_coll').setValue(rec_sped_selected.get('CSPROG'));
																                  		component.up('form').getForm().findField('f_sped_coll_out').setValue(rec_sped_selected.get('CSPROG'));
										                  	    						//component.setValue(rec_sped_selected.get('CSPROG'));
																						return false;																		            
																		            } 																		            
																		            
																		        }, scope: this,
																		        failure    : function(result, request){
																		        	console.log('failure');									        
																		            Ext.Msg.alert('Message', 'Errore');
																		        }
																    });
									                  	    
										                  		}
										                  };
										                
										                  acs_show_win_std('Seleziona spedizione collegata', 'acs_form_json_assegna_spedizione.php', 
										                  		{
										                  			list_selected_id:[], cod_iti: '<?php echo $itin_per_assegna; ?>',
										                  		 	scelta_sped_collegata_per_sped_id: '<?php echo $sped_id ?>', 
										                  		 	//non usata //data: '<?php echo $sped['CSDTSP']; ?>'
										                  		 },										                  		 
										                  		1050, 400, my_listeners)  
										                });
										            }
										        }										    
									    									  
									  }, {
										name: 'f_sped_coll_out_del',										  
										xtype: 'displayfield',
										editable: false,
										fieldLabel: '',
										padding: '0 0 0 10',
									    value: <?php echo j("<img src=" . img_path("icone/48x48/sub_red_delete.png") . " width=16>"); ?>,

									    
										listeners: {
										            render: function( component ) {
														component.getEl().on('dblclick', function( event, el ) {
										                if (parseInt(component.up('form').getForm().findField('sped_id').getValue().length) == 0){
										                	acs_show_msg_error('Operazione non ammessa in fase di generazione nuova spedizione');
										                	return false;
										                } 
														
														//se confermato rimuovo il legame tra le spedizioni
														    Ext.Msg.confirm('Richiesta conferma', 'Confermi rimozione legame tra le due spedizioni?', function(btn, text){
														      if (btn == 'yes'){
														      	//Ext.getBody().mask('Loading... ', 'loading').show();
														        Ext.Ajax.request({
														            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_rimuovi_sped_coll',
														            jsonData: {sped_id:component.up('form').getForm().findField('sped_id').getValue()},
														            method: 'POST',
														            success: function ( result, request) {
														                var jsonData = Ext.decode(result.responseText);
														                
																			component.up('form').getForm().findField('f_sped_coll').setValue(0);
																			component.up('form').getForm().findField('f_sped_coll_out').setValue(0);														                
														                
														            },
														            failure: function ( result, request) {
														            }
														        });														      		

														      } else {
														        //nothing
														      }
														    });														
														
														});										            
										             }
										}									    
									    
									    
									  } 			        
			 			        ]
							 } 							  
							  
							  		 			        
		 			        ]
		 			     }						  
						
						
						
						  
						  
						  
						  
						 , {
					        xtype: 'fieldcontainer',
					        fieldLabel: 'KM',
							defaultType: 'textfield',
		 			        layout: 'hbox',
		 			        items: [
			 			        {
									name: 'f_km_trasporto',
									xtype: 'numberfield',
									fieldLabel: '',
								    value: <?php echo j(trim($sped['CSKMTR'])); ?>,							
							  	}, {
									name: 'f_gg_trasporto',
									xtype: 'numberfield',
									fieldLabel: 'GG trasporto',
								    anchor: '-15',
								    margin: '0 0 0 15',
								    value: <?php echo j(trim($sped['CSGGTR'])); ?>,
								    labelAlign: 'right'								 							
								  } 
						  	]
		 			      }, {
					        xtype: 'fieldcontainer',
					        fieldLabel: 'Costo trasporto',
							defaultType: 'textfield',
		 			        layout: 'hbox',
		 			        items: [
								 {
									name: 'f_costo_trasporto',
									xtype: 'numberfield',
									allowDecimals: true,
									fieldLabel: '',
								    value: <?php echo j(trim($sped['CSCSTR'])); ?>,							
								  }, {
									name: 'f_costo_deposito',
									xtype: 'numberfield',
									allowDecimals: true,
									fieldLabel: 'Costo deposito',
									margin: '0 0 0 15',
								    value: <?php echo j(trim($sped['CSCSTD'])); ?>,
								    labelAlign: 'right'							
								  }		 			        
		 			        ]
						 }, {
					        xtype: 'fieldcontainer',
					        fieldLabel: 'Seq. avanzam.<br/> GG scarico',
							defaultType: 'textfield',
		 			        layout: 'hbox',
		 			        defaults: {
		 			        	width: 40,
		 			        	margin: '0 5 0 0',
		 			        	xtype: 'textfield',
		 			        	fieldLabel: ''
		 			        },
		 			        items: [
								 {
									name: 'f_seq_gg_scarico_1',
								    value: <?php echo j(trim($sped['CSSEQ1'])); ?>,							
								  }, {
									name: 'f_seq_gg_scarico_2',
								    value: <?php echo j(trim($sped['CSSEQ2'])); ?>,							
								  },{
									name: 'f_seq_gg_scarico_3',
								    value: <?php echo j(trim($sped['CSSEQ3'])); ?>,							
								  },{
									name: 'f_seq_gg_scarico_4',
								    value: <?php echo j(trim($sped['CSSEQ4'])); ?>,							
								  },{
									name: 'f_seq_gg_scarico_5',
								    value: <?php echo j(trim($sped['CSSEQ5'])); ?>,							
								  }	 			        
		 			        ]
						 },
						 {
							name: 'f_cointainer',
							xtype: 'textfield',
							fieldLabel: 'Nr Container',
						    value: <?php echo j(trim($sped['CSNCON'])); ?>,
						    maxLength : 50,
						    anchor: '-15'
						  },
						  {
							name: 'f_sigillo',
							xtype: 'textfield',
							fieldLabel: 'Nr Sigillo',
						    value: <?php echo j(trim($sped['CSNSIG'])); ?>,		
						    maxLength : 50,
						    anchor: '-15'			
						  }
						   
				],
			buttons: [
<?php if ($m_params->create_new != "Y"){ ?>			
			{
	            text: 'Altro',
	            scale: 'medium',
	            iconCls: 'icon-comment_add-24',
	            handler: function() {
	            	var form = this.up('form').getForm();
					acs_show_win_std('Modifica spedizioni - Informazioni aggiuntive', 
									 'acs_form_json_spedizione_int.php', 
									 form.getValues(), 500, 300, null, 'icon-comment_add-16');	            	
	            }
	         }, {
	            text: 'Modifica itinerario',
	            scale: 'medium',
	            iconCls: 'icon-compass-24',
	            handler: function() {
	            	var form = this.up('form').getForm();
					acs_show_win_std('Modifica itinerario spedizione #' + <?php echo $sped_id ?>, 
									 'acs_form_json_spedizione_mod_itin.php', 
									 form.getValues(), 500, 300, null, 'icon-compass-16');	            	
	            }
	         }, { xtype: 'tbfill' }, 
<?php } ?>	         
	         
	         {
	            text: 'Salva',
	            scale: 'medium',
	            iconCls: 'icon-save-24',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	
	            	//se era da programmae (stato = 'DP') e ho la data o l'ora di spedizione
	            	// => la modifica in programmata (stato = 'PR')
		            	stato_sped = form.findField('f_stato').getValue();
		            	ora_sped   = form.findField('f_ora').getValue();
		            	data_sped  = form.findField('f_data_spedizione').getValue();
		            	
		            	<?php if ($sped['CSFG04'] != 'F'){ ?>
		            		if (stato_sped == 'DP' && (data_sped !== null || ora_sped !== null )){
		            	 		form.findField('f_stato').setValue('PR');
		            		}
		            	<?php } ?>	
	            	

					if(form.isValid()){
					    var m_button = this;
						m_button.disable();
					  

<?php if ($m_params->from_area_itin != 'Y'){ ?>

				    m_win = this.up('window');
				    var loc_m_win = this.up('window');

	                form.submit({
		                            success: function(form,action) {
		                            
									   //se la mi ha passato il listener onSpedSelected
									   // eseguo l'evento ritornando il record selezionato
									   if (typeof(m_win.initialConfig.listeners) !== 'undefined' &&
									   		typeof(m_win.initialConfig.listeners.acsaftersave) !== 'undefined')
									    {
									    	
											loc_m_win.fireEvent('acsaftersave');
											loc_m_win.close();
									   		return false;						
										}	
		                            
		                            
		                            	m_tree = Ext.getCmp('<?php echo $tree_id; ?>');
		                            	m_rec = Ext.getCmp('<?php echo $rec_id; ?>');		                            	 
										m_store = m_tree.getStore();

										loc_m_win.close();
										
										if (m_store.treeStore === undefined)
											m_store.load();
										else
											m_store.treeStore.load();		                            	
		                            },
		                            failure: function(form,action){
		                                acs_show_msg_error(action.result.error_log);
		                                m_button.enable();
		                            }
		                        });
<?php } ?>
		
<?php if ($m_params->from_area_itin == 'Y'){ ?>
	                
				    m_win = this.up('window');
				    var loc_m_win = this.up('window');	                
					if(form.isValid())	
	                form.submit({
                            //waitMsg:'Loading...',
                            success: function(form,action) {
                            	m_tree = Ext.getCmp('<?php echo $tree_id; ?>');
                            	m_rec = m_tree.getStore().treeStore.getNodeById('<?php echo $rec_id; ?>');
                            	
                            	<?php if ($rec_liv == 'liv_3'){ ?>
                            	 m_rec = m_rec.parentNode;
                            	<?php } ?>
                            	
								loc_m_win.close();                            	
                            	
								m_rec.collapse()		                                
                                m_rec.data.loaded = false; //forzo aggiornamento
                                m_rec.expand();		                            	
                            },
                            failure: function(form,action){
                                acs_show_msg_error(action.result.error_log);
                                m_button.enable();
                            }
                        });
<?php } ?>		
		
		                        
								
		           //MATTEO: AGGIUNGERE!!!!              
		           // this.up('window').close();
		           } //form isValid            	                	                
	            }
	        }
	       ],             
				
        }
]}