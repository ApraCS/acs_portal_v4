<?php
 if ($m_params->form_open->tipo_op != 'SBLOC') { ?>
	    	    {text: 'O',					width: 35, dataIndex: 'n_O', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('tipo_liv') == 'ordine'){
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';						
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;
					 		 				
				}},
	    	    {text: 'P',					width: 35, dataIndex: 'n_P', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('tipo_liv') == 'ordine'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},						
<?php } else { ?>
	    	    {text: 'OP',					width: 35, dataIndex: 'n_O', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('tipo_liv') == 'ordine'){
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';						
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else {
	        	    	v = 0;
	        	    	if (!Ext.isEmpty(rec.get('n_O'))) v = v + parseInt(rec.get('n_O'));
	        	    	if (!Ext.isEmpty(rec.get('n_P'))) v = v + parseInt(rec.get('n_P'));
	        	    	if (v>0) return v;
	        	    	return null;
	        	    }
					 		 				
				}},

<?php } ?>				
	    	    {text: 'M',					width: 35, dataIndex: 'n_M', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('tipo_liv') == 'ordine'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},
	    	    {text: 'A',					width: 35, dataIndex: 'n_A', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('tipo_liv') == 'ordine'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},				
	    	    {text: 'CV',				width: 35, dataIndex: 'n_CV', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('tipo_liv') == 'ordine'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;
				}}	    
	    			 