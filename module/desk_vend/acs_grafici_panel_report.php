<?php

require_once "../../config.inc.php";
ini_set('max_execution_time', 300);

$s = new Spedizioni();
$main_module = new Spedizioni();

	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);

	
	//parametri per report
	$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
	$form_values = json_decode($form_values);
	
	
	//TOTALE GENERALE (DA USARE NEI TOP20)
	$ar_ret_gen = get_json_data_chart2($form_values, 'TOTALE_GENERALE');
	$totale_generale = $ar_ret_gen[0];
	
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div#my_content h1{font-size: 22px; padding: 5px;}
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}
	   

   /*tr.liv_totale td{background-color: #cccccc; font-weight: bold;}*/
   tr.liv_totale td{font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {

	//apertura automatica finestra invio email
	<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
		Ext.get('p_send_email').dom.click();
	<?php } ?>
	
	});    
    

  </script>


 </head>
 <body>
 
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

 <div id='my_content'>
 
 
 <h1>Riepilogo portafoglio programmazione <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo "righe "; ?>ordini clienti </h1>
 <div style="text-align: right;">Data elaborazione: <?php echo Date('d/m/Y H:i'); ?></div>
 <br/>
 

 	<table class="int1" width="100%">
		<tr>
		 <td>
		 	<?php echo print_table_aree_spedizione(); ?><br/>
		 	<?php echo print_table_divisioni(); ?><br/>
		 	<?php echo print_table_stabilimenti(); ?><br/>
		 	<?php echo print_table_mercato(); ?><br/>
		 	<?php echo print_table_referente_cliente(); ?><br/>
		 	<?php echo print_table_referente_ordine(); ?>
		 </td>
		 <td><?php echo print_table_itinerari(); ?></td>		 
		 <td><?php echo print_table_agenti(); ?></td>		 
		</tr>
		
		<tr>
		 <td colspan=3>&nbsp;</td>		 
		</tr>		
		
		<tr>
		 <td>
		 	<?php echo print_table_tree_tipologia_tipo(); ?>		 	
		 </td>
		 <td><?php echo print_table_tree_stato_tipologia(); ?></td>
		 <td><?php echo print_table_variante1(); ?></td>		 
		</tr>
		
				
		<tr>
		 <td colspan=3>&nbsp;</td>		 
		</tr>		
		
		<tr>
		 <td>
		 	<?php echo print_table_clienti(20); ?>		 	
		 </td>
		 <td>
		 	<?php echo print_table_nazioni(20); ?>
		 </td>
		 <td><?php echo print_table_condizioni_pagamento(); ?></td>		 
		</tr>		
		
		
	</table>

	
	<br/>
	
	
 </div>

 </body>
</html>
<?php



function print_table_referente_cliente(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'TDDOCL');

	$ret = "";
	$ret .= print_tabella('Referente cliente', $ar_ret, 'N');
	return $ret;
}


function print_table_referente_ordine(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'TDDORE');

	$ret = "";
	$ret .= print_tabella('Referente ordine', $ar_ret, 'N');
	return $ret;
}


function print_table_variante1(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'TDDVN1');

	$ret = "";
	$ret .= print_tabella('Variante', $ar_ret, 'N');
	return $ret;
}


function print_table_tree_tipologia_tipo(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'tree_tipologia_tipo');

	$ret = "";
	$ret .= print_tabella_tree('Tipologia / Tipo ordine cliente', $ar_ret);
	return $ret;
}


function print_table_tree_stato_tipologia(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'tree_stato_tipologia');

	$ret = "";
	$ret .= print_tabella_tree('Stato / Tipologia ordine cliente', $ar_ret);
	return $ret;
}


function print_table_aree_spedizione(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'TAASPE');

	$ret = "";
	$ret .= print_tabella('Aree di spedizone', $ar_ret);
	return $ret;
}

 function print_table_divisioni(){
 	
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDCDIV');
 	
 	$ret = "";
 	$ret .= print_tabella('Divisioni', $ar_ret);
 	return $ret;
 }
 
 function print_table_agenti(){
 
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDCAG1');
 
 	$ret = "";
 	$ret .= print_tabella('Agenti', $ar_ret);
 	return $ret;
 }

 function print_table_clienti($top = 0){
 
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDCCON', $top);
 
 	$ret = "";
 	$ret .= print_tabella('Top 20 - Clienti', $ar_ret, 'Y', $top);
 	return $ret;
 }
 

 function print_table_nazioni($top = 0){
 
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDNAZI', $top);
 
 	$ret = "";
 	$ret .= print_tabella('Top 20 - Nazioni', $ar_ret, 'Y', $top);
 	return $ret;
 }
 
 
 
 function print_table_stabilimenti(){
 
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDSTAB');
 
 	$ret = "";
 	$ret .= print_tabella('Stabilimenti', $ar_ret);
 	return $ret;
 }

 
 function print_table_condizioni_pagamento(){
 
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDCPAG');
 
 	$ret = "";
 	$ret .= print_tabella('Pagamenti', $ar_ret);
 	return $ret;
 } 
 
 
 function print_table_itinerari(){
 
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDCITI');
 
 	$ret = "";
 	$ret .= print_tabella('Itinerari', $ar_ret);
 	return $ret;
 }
 
 
 function print_table_trasportatori(){
 
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDVET1');
 
 	$ret = "";
 	$ret .= print_tabella('Trasportatori', $ar_ret);
 	return $ret;
 } 

 function print_table_mercato(){
 
 	global $form_values;
 	$ar_ret = get_json_data_chart2($form_values, 'TDCAVE');
 
 	$ret = "";
 	$ret .= print_tabella('Mercato', $ar_ret);
 	return $ret;
 }
 
 
 
 
 
 function print_tabella($title, $ar_ret, $stampa_cod = 'Y', $top = 0){
 	global $totale_generale;
 	
 	
 	//scorro per tot import e nr
 	$tot_imp_rec = $tot_nr_rec = 0; //somma di quelli che mostro (per devo aggiungere ALTRO)
 	foreach($ar_ret as $r){
 		$tot_imp_rec += $r['S_IMP'];
 		$tot_nr_rec  += $r['NUM_ORD'];
 	}


 	//i totali li prendo da tot_generale
 	$tot_imp = $totale_generale['S_IMP'];
 	$tot_nr  = $totale_generale['NUM_ORD'];
 	
 	if ($top > 0){
 		//creo riga "altro"
 		$ar_ret[] = array(	'TDASPE' => 'ALTRO', 
 							'TDASPE_D' => 'ALTRO', 
 							'S_IMP' => ($tot_imp - $tot_imp_rec),
 							'NUM_ORD' => ($tot_nr - $tot_nr_rec) 				
 						);		
 	}
 	
 	
 	
 	$ret = "";
 	$ret .= "
	 <table>
   		
	  <tr class=liv3>
		 <td colspan=6>" . $title . "</td>
   	  </tr>	
   		
	  <tr>
 		";
 	
 	if ($stampa_cod == 'Y')
 		$ret .= "
 		<th>Cod</th>
 		<th>Descrizione</th>
 		";
 	else
 		$ret .= "<th colspan=2>Descrizione</th>";
 	
 		
 	$ret .= "	
	   <th class=number>Importo</th>
	   <th class=number>%</th>
	   <th class=number>Nr</th>
	   <th class=number>%</th>
	  </tr>
	 ";
 	
 	foreach($ar_ret as $r){
 		
 		$r_perc_imp = $r_perc_nr = 0;
 		if ($tot_imp != 0)
 			$r_perc_imp = ($r['S_IMP'] / $tot_imp) * 100;
 		if ($tot_nr != 0)
 			$r_perc_nr = ($r['NUM_ORD'] / $tot_nr) * 100; 		
 		
 		$ret .= "
			<tr>
			";

 		if ($stampa_cod == 'Y')
			 $ret .= "
		 		<td>{$r['TDASPE']}</td>
		 		<td>{$r['TDASPE_D']}</td>
 			";
 		else
 			$ret .= "<td colspan=2>{$r['TDASPE_D']}</td>";
 		
 		$ret .= " 			
			 <td class=number>" . n($r['S_IMP']) . "</td>
			 <td class=number>" . n($r_perc_imp) . "</td>
			 <td class=number>" . n($r['NUM_ORD'], 0) . "</td>
			 <td class=number>" . n($r_perc_nr) . "</td>
			</tr>
		";
 	} 	
 	

   //totale
 	$ret .= "
 	  <tr class=liv_totale>
 		<td colspan=2 class=number>Totale</td>
 		<td class=number>" . n($tot_imp) . "</td>
		<td class=number>&nbsp;</td>
		<td class=number>" . n($tot_nr, 0) . "</td>
		<td class=number>&nbsp;</td>
	  </tr>
	";
 	
 	
   $ret .= "					  
	 </table>
	";
   return $ret;	
 }


 
 
 
 
 
 function print_tabella_tree($title, $ar_ret){
 	
 	//scorro per tot import e nr
 	$tot_imp = $tot_nr = 0;
 	foreach($ar_ret as $r){
 		$tot_imp += $r['S_IMP'];
 		$tot_nr  += $r['NUM_ORD'];
 	}
 

 	//creo alberatura
 	$ar = array();
 	foreach($ar_ret as $row){ 	
 		
 		$s_ar = &$ar; 		
 		
	 	//stacco per TDASPE (campo1_name)
	 	$r_livs['liv1_v'] = trim($row['TDASPE']);
	 	
	 	$liv = $r_livs['liv1_v'];
	 	if (is_null($s_ar[$liv])){
	 		$s_ar[$liv] = $row; 
	 	} else {
	 		//incremento
	 		$s_ar[$liv]['S_IMP'] += $row['S_IMP'];
	 		$s_ar[$liv]['NUM_ORD'] += $row['NUM_ORD'];
	 	} 
	 		
	 	$s_ar = &$s_ar[$liv]['children'];
	 	
	 	if (is_null($s_ar[trim($row['CAMPO2_NAME'])]))
	 		$s_ar[trim($row['CAMPO2_NAME'])] = $row;
	 	else {
	 		$s_ar[trim($row['CAMPO2_NAME'])]['S_IMP'] += $row['S_IMP'];
	 		$s_ar[trim($row['CAMPO2_NAME'])]['NUM_ORD'] += $row['NUM_ORD'];
	 	}
	 	
 	}
 	
 	

 
 	$ret = "";
 	$ret .= "
	 <table>
   
	  <tr class=liv3>
		 <td colspan=6>" . $title . "</td>
   	  </tr>
   
	  <tr>
	   <th>Cod</th>
	   <th>Descrizione</th>
	   <th class=number>Importo</th>
	   <th class=number>%</th>
	   <th class=number>Nr</th>
	   <th class=number>%</th>
	  </tr>
	 ";
 
 	foreach($ar as $r){
 			
 		$r_perc_imp = $r_perc_nr = 0;
 		if ($tot_imp != 0)
 			$r_perc_imp = ($r['S_IMP'] / $tot_imp) * 100;
 		if ($tot_nr != 0)
 			$r_perc_nr = ($r['NUM_ORD'] / $tot_nr) * 100;
 			
 		$ret .= "
 		<tr class=liv2>
 		<td>{$r['TDASPE']}</td>
 		<td>{$r['TDASPE_D']}</td>
 		<td class=number>" . n($r['S_IMP']) . "</td>
			 <td class=number>" . n($r_perc_imp) . "</td>
			 <td class=number>" . n($r['NUM_ORD'], 0) . "</td>
			 <td class=number>" . n($r_perc_nr) . "</td>
			</tr>
		";
 		
 		
 		
 		//livello2
 		foreach($r['children'] as $r2){
 		
 			$r2_perc_imp = $r2_perc_nr = 0;
 			if ($tot_imp != 0)
 				$r2_perc_imp = ($r2['S_IMP'] / $tot_imp) * 100;
 			if ($tot_nr != 0)
 				$r2_perc_nr = ($r2['NUM_ORD'] / $tot_nr) * 100;
 		
 			$ret .= "
 			<tr>
 			<td>{$r2['CAMPO2_NAME']}</td>
 			<td>{$r2['CAMPO2_DES']}</td>
 			<td class=number>" . n($r2['S_IMP']) . "</td>
			 <td class=number>" . n($r2_perc_imp) . "</td>
			 <td class=number>" . n($r2['NUM_ORD'], 0) . "</td>
			 <td class=number>" . n($r2_perc_nr) . "</td>
			</tr>
		";
 		} //livello2
 		
 	} //livello1
 

 	//totale
 	$ret .= "
 	  <tr class=liv_totale>
 		<td colspan=2 class=number>Totale</td>
 		<td class=number>" . n($tot_imp) . "</td>
		<td class=number>&nbsp;</td>
		<td class=number>" . n($tot_nr, 0) . "</td>
		<td class=number>&nbsp;</td>
	  </tr>
	";
 	
 	
 	$ret .= "
	 </table>
	";
 	return $ret;
 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
//TODO: DRY: da acs_grafici_panel
function get_json_data_chart2($form_values, $campo, $top = 0){
  global $cfg_mod_Spedizioni, $main_module, $s, $conn;
 
 //$memo_encoded = strtr(acs_je($m_params->memo), array("'" => "\'", '"' => '\"'));
 
 $filter_where = '';
 
 if (isset($form_values->f_aspe) && strlen($form_values->f_aspe) > 0)
 	$filter_where .= " AND TA_ITIN.TAASPE = " . sql_t($form_values->f_aspe);
 
 if (isset($form_values->f_tipo_ordine) && count($form_values->f_tipo_ordine) > 0)
 	$filter_where .= " AND TDOTPD IN (" . sql_t_IN($form_values->f_tipo_ordine) . ")";
 
 if (isset($form_values->f_stato_ordine) && count($form_values->f_stato_ordine) > 0)
 	$filter_where .= " AND TDSTAT IN( " . sql_t_IN($form_values->f_stato_ordine) . ")";
 
 if (isset($form_values->f_divisione) && strlen($form_values->f_divisione) > 0)
 	$filter_where .= " AND TDCDIV = " . sql_t($form_values->f_divisione);
 
 if (isset($form_values->f_tipologia_ordine) && count($form_values->f_tipologia_ordine) > 0)
 	$filter_where .= " AND TDCLOR IN( " . sql_t_IN($form_values->f_tipologia_ordine) . ")";
  
 if (isset($form_values->tipo) && strlen($form_values->tipo) > 0 && $form_values->tipo != 'tutti'){
 	if ($form_values->tipo == 1) //solo
 		$filter_where .= " AND ( TDFN02 = " . sql_t($form_values->tipo) . " OR TDFN03 = " . sql_t($form_values->tipo) . ") ";
 	if ($form_values->tipo == 0) //esclusi
 		$filter_where .= " AND ( TDFN02 = " . sql_t($form_values->tipo) . " AND TDFN03 = " . sql_t($form_values->tipo) . ") ";
 }
 
 
 
 if (isset($form_values->f_agente) && strlen($form_values->f_agente) > 0 && $form_values->f_agente != 'tutti')
 	$filter_where .= " AND TDCAG1 = " . sql_t($form_values->f_agente);
 
 
 switch ($campo){
 	case 'TDSTAB':
 	case 'TDCITI':
 	case 'TDVET1':
 	case 'TDDVN1':
 	case 'TDCAVE':
 	case 'TDDOCL': 		
 		$campo_name = $campo;
 		$campo_des = $campo;
 		break;
 	case 'TDDORE':
 		$campo_name = $campo;
 		$campo_des = $campo;
 		break; 		
 	case 'TDCAG1':
 		$campo_name = $campo;
 		$campo_des  = 'TDDAG1';
 		break;
	case 'TDCCON':
		$campo_name = $campo;
		$campo_des  = 'TDDCON';
		break;
	case 'TDCPAG':
		$campo_name = $campo;
		$campo_des  = 'TDDPAG';
		break;		
	case 'TDNAZI':
		$campo_name = $campo;
		$campo_des  = 'TDDNAZ';
		break;		
 	case 'TDCDIV':
 		$campo_name = $campo;
 		$campo_des  = 'TDDDIV';
 		break; 			
 	case 'tree_tipologia_tipo':
 		$campo_name  = 'TDCLOR';
 		$campo_des   = 'TDCLOR';
 		$campo2_name = 'TDOTPD';
 		$campo2_des  = 'TDDOTD';
 		break;
 	case 'tree_stato_tipologia':
 		$campo_name  = 'TDSTAT';
 		$campo_des   = 'TDDSST'; 		
 		$campo2_name  = 'TDCLOR';
 		$campo2_des   = 'TDCLOR';
 		break;
 	case 'TOTALE_GENERALE':
 		$campo_name  = 'TDDT';
 		$campo_des   = 'TDDT';
 		break;
 	default:
 		$campo_name = 'TAASPE';
 		$campo_des = 'TAASPE';
 		break;
 }
 
 
 //programmati o hold?
 if ($form_values->programmati_hold == 'HOLD')
 	$filter_where .= " AND TDSWSP = 'N' ";	//hold
 else if ($form_values->programmati_hold == 'PROG')
 	$filter_where .= " AND TDSWSP = 'Y' ";	//tutti 
 else
 	$filter_where .= " AND 1=1 ";	//programmati
 
 $campo2_select = $campo2_group = '';
 if (isset($campo2_name)){
 	$campo2_select = "{$campo2_name} AS CAMPO2_NAME, {$campo2_des} AS CAMPO2_DES,";
 	$campo2_group  = ", {$campo2_name}, {$campo2_des}";
 } 
 
 $sql = "
 	SELECT {$campo_name} AS TDASPE, {$campo_des} AS TDASPE_D, {$campo2_select}
 			COUNT(*) AS NUM_ORD, SUM(TDTIMP) AS S_IMP
 	FROM {$cfg_mod_Spedizioni['file_testate']} TD
 	INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
	 	ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
  
 	WHERE " . $main_module->get_where_std() . " " . $filter_where . "
 	GROUP BY {$campo_name}, {$campo_des} {$campo2_group}
 	ORDER BY SUM(TDTIMP) DESC, COUNT(*) DESC 	
 ";
 
 
 
 if ($top > 0)
 	$sql .= " FETCH FIRST {$top} ROWS ONLY ";
 
 $stmt = db2_prepare($conn, $sql);
 echo db2_stmt_errormsg();
 $result = db2_execute($stmt);
 
 $ar_records = array();
 while ($r = db2_fetch_assoc($stmt)){
 
 //decodifico descrizione
 	if ($campo == 'TDSTAB')
	 	$r['TDASPE_D'] = $s->decod_std('START', $r['TDASPE']);
 	else if ($campo == 'TDCITI')
 		$r['TDASPE_D'] = $s->decod_std('ITIN', $r['TDASPE']);
 	else if ($campo == 'TDVET1')
 		$r['TDASPE_D'] = $s->decod_std('AVET', $r['TDASPE']);
 	else if ($campo == 'TDCAG1')
 		$r['TDASPE_D'] = $r['TDASPE_D'];
 	else if ($campo == 'TDCDIV')
 		$r['TDASPE_D'] = $r['TDASPE_D']; 	
 	else if ($campo == 'TDCCON')
 		$r['TDASPE_D'] = $r['TDASPE_D'];
 	else if ($campo == 'TDCPAG')
 		$r['TDASPE_D'] = $r['TDASPE_D']; 	
 	else if ($campo == 'TDNAZI')
 		$r['TDASPE_D'] = $r['TDASPE_D']; 	 	
 	else if ($campo == 'TDCAVE'){
 		$r['TDASPE_D'] = $s->decod_std('MERCA', $r['TDASPE']);
 	}
 	else if ($campo == 'tree_tipologia_tipo'){
 		$r['TDASPE_D'] = $s->decod_std('TIPOV', $r['TDASPE']);
 	}
 	else if ($campo == 'tree_stato_tipologia'){
 		$r['CAMPO2_DES'] = $s->decod_std('TIPOV', $r['CAMPO2_NAME']);
 	}
 	else
		$r['TDASPE_D'] = $s->decod_std('ASPE', $r['TDASPE']);
 
 		$ar_records[] = $r;
 	}
 
 				$tot_ar_ret = 0;
 				foreach($ar_records as $tot_ar)
 					$tot_ar_ret += $tot_ar['NUM_ORD'];
 
 					$ar_ret = array();
 					foreach($ar_records as $ar1){
 					//$ar1['NUM_ORD'] = round(($ar1['NUM_ORD'] * 100 / $tot_ar_ret),2);
 						$ar_ret[] = $ar1;
 					}
 		return $ar_ret;		
	} //get_json_data_chart2 
 
 
 
 
 
 ?>
		