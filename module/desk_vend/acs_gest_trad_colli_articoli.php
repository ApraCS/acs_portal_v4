<?php


require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_salva_traduzione'){
   
    $m_params = acs_m_params_json_decode();

    
    if(isset($m_params->rrn) && trim($m_params->rrn) != ''){
            
           if($m_params->type == 'C'){
             
                $ar_ins = array();
                
                 $tarest = "";
                 $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_3)));
                 $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_4)));
                 $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_5)));
             
                $ar_ins["TADESC"] = acs_toDb(trim($m_params->form_values->f_text_1)); 
                $ar_ins["TADES2"] = acs_toDb(trim($m_params->form_values->f_text_2));
                $ar_ins["TAREST"] = $tarest;
               
                $sql = "UPDATE {$cfg_mod_Spedizioni['file_tab_sys']} TA
                        SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                        WHERE RRN(TA) = '{$m_params->rrn}' 
                ";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
            } else{
            
            
            $ar_ins = array();
            
            /*$ar_ins['BSUSAL']   = trim($auth->get_user());
             $ar_ins['BSDTAL']   = oggi_AS_date();
             $ar_ins['BSORAL']   = oggi_AS_time();*/
            $ar_ins["ALDAR1"] = acs_toDb($m_params->form_values->f_text_1);
            $ar_ins["ALDAR2"] = acs_toDb($m_params->form_values->f_text_2);
            $ar_ins["ALDAR3"] = acs_toDb($m_params->form_values->f_text_3);
            $ar_ins["ALDAR4"] = acs_toDb($m_params->form_values->f_text_4);
       
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . " 
                     WHERE RRN(AL) = '{$m_params->rrn}' 
                    ";
             
             $stmt = db2_prepare($conn, $sql);
             echo db2_stmt_errormsg();
             $result = db2_execute($stmt, $ar_ins);
             echo db2_stmt_errormsg($stmt);
            }
            
            
        }
        
        
        else{
            
            
          if($m_params->type == 'C'){
                
                $ar_ins = array();
                
                $tarest = "";
                $tarest .= sprintf("%-30s",  acs_toDb(trim($m_params->form_values->f_text_3)));
                $tarest .= sprintf("%-30s",  acs_toDb(trim($m_params->form_values->f_text_4)));
                $tarest .= sprintf("%-30s",  acs_toDb(trim($m_params->form_values->f_text_5)));
                
                $ar_ins["TADT"]   = $id_ditta_default;
                $ar_ins["TANR"]  = $m_params->codice;
                $ar_ins["TALINV"]  = $m_params->lng;
                $ar_ins["TAID"]  = 'PUCI';
                $ar_ins["TADESC"] = acs_toDb(trim($m_params->form_values->f_text_1));
                $ar_ins["TADES2"] = acs_toDb(trim($m_params->form_values->f_text_2));
                $ar_ins["TAREST"] = $tarest;
                
                
                $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tab_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
                
            }else{
            $ar_ins = array();
           
            /*$ar_ins['BSUSAL']   = trim($auth->get_user());
            $ar_ins['BSDTAL']   = oggi_AS_date();
            $ar_ins['BSORAL']   = oggi_AS_time();*/
            $ar_ins["ALDAR1"] = acs_toDb($m_params->form_values->f_text_1);
            $ar_ins["ALDAR2"] = acs_toDb($m_params->form_values->f_text_2);
            $ar_ins["ALDAR3"] = acs_toDb($m_params->form_values->f_text_3);
            $ar_ins["ALDAR4"] = acs_toDb($m_params->form_values->f_text_4);
            $ar_ins["ALDT"]   = $id_ditta_default;
            $ar_ins["ALART"]  = $m_params->codice;
            $ar_ins["ALLING"] = $m_params->lng;
            
            
            $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_anag_art_trad']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            
            }
 
}

        $ret = array();
        $ret['success'] = true;
        echo acs_je($ret);
        exit;


}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();

    
    if($m_params->type == 'C'){
        
        $sql = "SELECT RRN(TA) AS RRN, TADESC,TADES2,TAREST
                FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA
                WHERE TADT = '{$id_ditta_default}'
                AND TANR = '{$m_params->codice}'
                AND TAID = 'PUCI'
                AND TALINV = '{$m_params->lng}'";
        
        
    }else{
  
        $sql = "SELECT RRN(AL) AS RRN, ALDAR1, ALDAR2, ALDAR3, ALDAR4
                FROM {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
                WHERE ALDT = '{$id_ditta_default}' 
                AND ALART = '{$m_params->codice}'
                AND ALLING = '{$m_params->lng}'";
 
    }
    

    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt); 
 
    
    ?>
 {"success":true, "items": [
        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            
					buttons: [{
			            text: 'Salva',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var form_values = form.getValues();
							 var loc_win = this.up('window');
							
							 
								if(form.isValid()){
									Ext.Ajax.request({
									        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_salva_traduzione',
									        jsonData: {
									        	form_values: form.getValues(),
									        	codice : '<?php echo $m_params->codice; ?>',
									        	lng : '<?php echo $m_params->lng; ?>',
									        	rrn : '<?php echo $row['RRN']; ?>',
									        	type : '<?php echo $m_params->type; ?>'
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){	            	  													        
									            var jsonData = Ext.decode(result.responseText);
									          	loc_win.fireEvent('afterSave', loc_win);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									            console.log('errorrrrr');
									        }
									    });	  
							    }							 
				
			            }
			         }],   		            
		
		            items: [{
    					xtype: 'fieldset',
    	                title: <?php echo j('<b>' . "[". trim($m_params->codice) . "] {$m_params->desc}" . '</b>') ?>,
    	                layout: 'anchor',
    	                flex:1,
    	                items: [
    	                
    	                <?php if($m_params->type == 'C'){
    	                
    	                    $trad3 = substr(acs_u8e($row['TAREST']), 0, 30);
    	                    $trad4 = substr(acs_u8e($row['TAREST']), 30, 30);
    	                    $trad5 = substr(acs_u8e($row['TAREST']), 60, 30);
    	                  
    	                   ?>
    	                
    	                  
    	                    {
    	                        name: 'f_text_1',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim(acs_u8e($row["TADESC"]))); ?>,
    	                    },	{
    	                        name: 'f_text_2',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim(acs_u8e($row["TADES2"]))); ?>,
    	                    },
    	                    {
    	                        name: 'f_text_3',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim($trad3)); ?>,
    	                    },{
    	                        name: 'f_text_4',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim($trad4)); ?>,
    	                    },{
    	                        name: 'f_text_5',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim($trad5)); ?>,
    	                    }
    	                
    	                
    	                <?php }else{
    	                
    	                    for($i=1; $i<= 4;$i++){ ?>
                    	
                    	{
    						name: 'f_text_<?php echo $i ?>',
    						xtype: 'textfield',
    						fieldLabel: '',
    						anchor: '-15',
    					    maxLength: 80,					    
    					    value: <?php echo j(trim(acs_u8e($row["ALDAR{$i}"]))); ?>,							
    					},	
    					
    									
					<?php }  }?>	
								
									
					 
	             ]}
								 
		           			]
		              }
		
			]
}	
	
	
	
<?php 
exit;
}