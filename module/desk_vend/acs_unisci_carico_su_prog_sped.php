<?php

require_once "../../config.inc.php";

require_once "acs_booking_include.php";

$s = new Spedizioni();
$m_params = acs_m_params_json_decode();


switch ($m_params->tipo_elenco){
	case 'GATE':
		$f_data = 'TDDTSP';
		break;
}


//***********************************************************************
// ESECUZIONE UNIONE SU NUOVO PROGRAMMA DI SPEDIZIONE
//***********************************************************************
if ($_REQUEST['fn'] == 'exe_unisci_carico'){

    //Se una spedizione selezionata e' flight (CSFG04='F') allora accodo i carichi selezionati su questa.
    //Altrimenti ne creo una nuova
    
    $n_sped_flight = 0;
    foreach ($m_params->from_gate->ar_carichi_selected as $sped_k_carico){        
        $sped_id     = $sped_k_carico->sped_id;
        $sped =      $s->get_spedizione($sped_id);
        if ($sped['CSFG04'] == 'F') {
            $n_sped_flight++;
            $sped_flight_id = $sped['CSPROG'];
        }
    }
        
    
    //Se ho piu' di una spedizione flight -> errore
    if ($n_sped_flight > 1) {
        $ret = array();
        $ret['success'] = false;
        $ret['message'] = 'Selezionare al massimo una sola spedizione FLIGHT';
        echo acs_je($ret);
        exit;
    }
    
    if ($n_sped_flight == 0) {
        
        //CREO NUOVA SPEDIZIONE FLIGHT    
            
            //dal primo sped/carico recupero la spedizione e ne creo una nuova in duplica
            $sped_id_copy_from = $m_params->from_gate->ar_carichi_selected[0]->sped_id;
            $sped_copy_from = $s->get_spedizione($sped_id_copy_from);
            
            $new_sped_id = $s->crt_spedizione(array(
                'itin_id'   => $sped_copy_from['CSCITI'],
                'f_itvet'   => implode('|', array('', $sped_copy_from['CSCVET'], $sped_copy_from['CSCAUT'])),
                'f_tipologia' => $sped_copy_from['CSTISP'],
                'f_data'        => $m_params->from_gate->data,
                'f_spedizione'  => $m_params->from_gate->data,
                'f_ora'         => $sped_copy_from['CSHMPG']
            ));
            
            //copiare anche la descrizione della spedizione? note? Km????
            $ar_upd = array(
              'CSFG04' => 'F',
              'CSCCON' => $sped_copy_from['CSCCON'], //perche crt_spedizione lo mette sempre a '-'
              'CSVOLD' => sql_f($m_params->form_values->f_volume),
              'CSPESO' => sql_f($m_params->form_values->f_peso),
              'CSBAND' => sql_f($m_params->form_values->f_pallet),
              'CSDESC' => $m_params->form_values->f_descr,
              'CSTARG' => $m_params->form_values->f_targa,
              
              'CSTITR' => $sped_copy_from['CSTITR'],
              'CSKMTR' => $sped_copy_from['CSKMTR'],
              'CSGGTR' => $sped_copy_from['CSGGTR'],
                
              'CSDTPG' => $m_params->from_gate->data,
              'CSPORT' => $sped_copy_from['CSPORT']
            );
            
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} 
                    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                    WHERE CSDT = '{$id_ditta_default}' AND CSPROG = {$new_sped_id}";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd);
            
            // ***** Creo record integrativo per spedizione (SPINT) *******
            // (copio da sped di provenienza, sostituendo la nota inserita dall'utente)
            
            //recuper i dati gi' inseriti
            $from_sped_id_8 = sprintf("%08s", $sped_id_copy_from);
            $sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt, array($id_ditta_default, 'SPINT', $from_sped_id_8));
            $row_SPINT_from = db2_fetch_assoc($stmt);
            
            
            $new_sped_id_8 = sprintf("%08s", $new_sped_id);
            
            $ar_ins = array();
            $ar_ins['TADT'] 	= $id_ditta_default;
            $ar_ins['TATAID'] 	= 'SPINT';
            $ar_ins['TAKEY1'] 	= $new_sped_id_8;
            $ar_ins['TAMAIL'] 	= $row_SPINT_from['TAMAIL'] . '';
            $ar_ins['TALOCA'] 	= $row_SPINT_from['TALOCA'] . '';
            $ar_ins['TARIF2'] 	= $row_SPINT_from['TARIF2'] . '';
            $ar_ins['TAINDI'] 	= $row_SPINT_from['TAINDI'] . '';
            $ar_ins['TADESC'] 	= utf8_decode($m_params->form_values->f_nota);
            
            $ar_ins['TAUSGE'] 	= $auth->get_user();
            $ar_ins['TADTGE']   = oggi_AS_date();
            $ar_ins['TAORGE'] 	= oggi_AS_time();
            
            $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            
            
            //duplica richiesta di booking
            if ($m_params->form_values->f_duplica_booking == 'Y'){
                //booking
                $richiesta_booking = get_last_booking_request_by_sped($sped_id);
                $ar_ins = $richiesta_booking;
                $ar_ins['BSTIME'] 	= microtime(true);
                $ar_ins['BSUSGE']   = trim($auth->get_user());
                $ar_ins['BSDTGE']   = oggi_AS_date();
                $ar_ins['BSORGE']   = oggi_AS_time();
                $ar_ins['BSNBOC']   = $new_sped_id;
                
                $sql = "INSERT INTO {$cfg_mod_Booking['file_richieste']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
                //COPIO LE ANAGRAFICHE (TRASP, ..)
                //Recupero email a cui inviare segnalazione
                $sql_ind = "SELECT * FROM {$cfg_mod_Booking['file_indirizzi']} IG
                    WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$sped_id_copy_from}' AND IGTAID= 'BKSPE'";
                $stmt_ind = db2_prepare($conn, $sql_ind);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_ind);
                while ($row_ind = db2_fetch_assoc($stmt_ind)) {
                    $ar_ins = $row_ind;
                    $ar_ins['IGKEY1'] = $new_sped_id;
                    $ar_ins['IGUSGE'] = trim($auth->get_user());
                    $ar_ins['IGDTGE'] = oggi_AS_date();
                    $ar_ins['IGORGE'] = oggi_AS_time();
                    $ar_ins['IGUSUM'] = trim($auth->get_user());
                    $ar_ins['IGDTUM'] = oggi_AS_date();
                    $ar_ins['IGORUM'] = oggi_AS_time();
                    
                    $sql = "INSERT INTO {$cfg_mod_Booking['file_indirizzi']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                    $stmt = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt, $ar_ins);
                    echo db2_stmt_errormsg($stmt);
                }
            }
            
            
    } else {
        //N_SPED_FLIGHT = 1
        $new_sped_id = $sped_flight_id;
    }

    
    
    //scorro i carichi/sped selezionati sul gate e sui relativi ordini imposto TDNBOF = nuova spedizione
    foreach ($m_params->from_gate->ar_carichi_selected as $sped_k_carico){

        $sped_id     = $sped_k_carico->sped_id;
        $k_carico    = $sped_k_carico->k_carico;
        $ar_carico   = $s->k_carico_td_decode($k_carico);
        
        //recupero l'elenco degli ordini da modificare
        $sql = "SELECT TDDOCU FROM {$cfg_mod_Spedizioni['file_testate']} TD
							WHERE " . $s->get_where_std() . "
							AND TDNBOF = " . $sped_id . "
							AND TDTPCA = " . sql_t($ar_carico['TDTPCA']) . "
							AND TDAACA = " . $ar_carico['TDAACA'] . "
							AND TDNRCA = " . $ar_carico['TDNRCA'] . "
							";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        while ($row_td = db2_fetch_assoc($stmt)){
            //update
            $sqlU = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDNBOF = ? WHERE TDDOCU = ?";
            $stmtU = db2_prepare($conn, $sqlU);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtU, array($new_sped_id, $row_td['TDDOCU']));
            
            //HISTORY
            $sh = new SpedHistory();
            $sh->crea(
                'sincronizza_ordine',
                array("k_ordine" 	=> $row_td['TDDOCU'])
                );
            
            
        } //per ogni ordine
    } //per ogni carico/sped       
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


//***********************************************************************
// ESECUZIONE RIPRISTINO SU PROGRAMMA DI PRODUZIONE (TDNBOF lo reimposto a TDNBOC)
//***********************************************************************
if ($_REQUEST['fn'] == 'exe_ripristina_carico'){    
    
    //scorro i carichi/sped selezionati sul gate e sui relativi ordini imposto TDNBOF = nuova spedizione
    foreach ($m_params->ar_carichi_selected as $sped_k_carico){
        
        $sped_id     = $sped_k_carico->sped_id;
        $k_carico    = $sped_k_carico->k_carico;
        $ar_carico   = $s->k_carico_td_decode($k_carico);
        
        //recupero l'elenco degli ordini da modificare
        $sql = "SELECT TDDOCU FROM {$cfg_mod_Spedizioni['file_testate']} TD
							WHERE " . $s->get_where_std() . "
							AND TDNBOF = " . $sped_id . "
							AND TDTPCA = " . sql_t($ar_carico['TDTPCA']) . "
							AND TDAACA = " . $ar_carico['TDAACA'] . "
							AND TDNRCA = " . $ar_carico['TDNRCA'] . "
							";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        while ($row_td = db2_fetch_assoc($stmt)){
            //update
            $sqlU = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDNBOF = TDNBOC WHERE TDDOCU = ?";
            $stmtU = db2_prepare($conn, $sqlU);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtU, array($row_td['TDDOCU']));
            
            //ToDo: scrivere log su RI
            
        } //per ogni ordine
    } //per ogni carico/sped
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}



// ******************************************************************************************
// PANNELLO DI CONFERMA (UNISCI)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_unisci'){
  $go_to_fn = 'exe_unisci_carico';    
  
  //verifico se dovro' creare una nuova spedizione flight (chiedendo peso/volume)
  // o accodo a una esistente
  
  $n_sped_flight = 0;
  $tras_piva = null;
  $tras_piva_error = false;
  $sped_con_booking = 0;
  $sped_senza_booking = 0;
  $ar_sped = array();
  foreach ($m_params->ar_carichi_selected as $sped_k_carico){
      $sped_id     = $sped_k_carico->sped_id;
      $sped =      $s->get_spedizione($sped_id);
      
      
      //Aggiungo le note
      $from_sped_id = sprintf("%08s", $sped_id);
      
      //recuper i dati gia' inseriti
      $sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt, array($id_ditta_default, 'SPINT', $from_sped_id));
      $row = db2_fetch_assoc($stmt);
            
      if ($sped['CSFG04'] == 'F') {
          $n_sped_flight++;
          $sped_flight_id = $sped['CSPROG'];
      } else {
          $sped['NOTA'] = $row['TADESC'];
          $ar_sped[$sped['CSPROG']] = $sped;
          
          //booking
          $richiesta_booking = get_last_booking_request_by_sped($sped_id);
          $ar_sped[$sped['CSPROG']]['BOOKING'] = $richiesta_booking;
          
          if (isset($richiesta_booking['BSTIME'])){
              $sped_con_booking ++;
              //ha booking, verifico se le p.iva sono uguali
              
              //trasportatore
              $sql_tras="SELECT * FROM {$cfg_mod_Booking['file_indirizzi']} IG
                    WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$sped_id}' 
                    AND IGTAID= 'BKSPE' AND IGKEY2='TRAS'";              
              $stmt_tras= db2_prepare($conn, $sql_tras);
              echo db2_stmt_errormsg();
              $result = db2_execute($stmt_tras);
              $row_tras = db2_fetch_assoc($stmt_tras);

              $ar_sped[$sped['CSPROG']]['BOOKING']['TRAS_PIVA'] = $row_tras['IGPIVA'];
              
              //verifico se le partite ive corrispondono
              if (is_null($tras_piva))
                  $tras_piva = trim($row_tras['IGPIVA']);
              else {
                  if ($tras_piva != trim($row_tras['IGPIVA']))
                      $tras_piva_error = true;
              }
              
          } else {
              $sped_senza_booking++;
          }
          
          if (strlen($descrizione_proposta) == 0)
              $descrizione_proposta = trim($sped['CSDESC']);
          
          if (strlen($nota_proposta) == 0)
              $nota_proposta = trim($sped['NOTA']);
          
          if (strlen($targa_proposta) == 0)
              $targa_proposta = trim($sped['CSTARG']);          
              
          $pallet_proposta  = max($pallet_proposta, $sped['CSBAND']);    
          $volume_proposta  = max($volume_proposta, $sped['CSVOLD']);
          $peso_proposta    = max($peso_proposta, $sped['CSPESO']);
              
      }      
  }
      
    ?>
    {"success":true, "items": [
     {
     	xtype: 'form',
     	scrollable: true,
     	bodyStyle: 'padding: 10px', bodyPadding: '5 5 0', 
     	layout : {type :'vbox', pack: 'start', align: 'stretch'},
     	defaults: {margin: '10 10 10 10',  hideTrigger: false},
     	items: [
     	
     	<?php if ($n_sped_flight == 0) {
     	    $html_descrizioni = '';
     	    foreach($ar_sped as $from_sped){
     	      $html_descrizioni .= "<br><B>Sped. #{$from_sped['CSPROG']}<br>" . trim($from_sped['CSDESC']) . "<br>[Targa: " . trim($from_sped['CSTARG']) . "]</B><br>";
     	      
     	      if (strlen(trim($from_sped['NOTA'])) > 0)
     	       $html_descrizioni .= "Nota: " . trim($from_sped['NOTA']) . "<br>";
     	      
     	      if (!is_null($from_sped['BOOKING']) && isset($from_sped['BOOKING']['BSTIME'])){
     	          $html_descrizioni .= 'Booking: ' . $from_sped['BOOKING']['BSSTAT_O'] . " " . $from_sped['BOOKING']['TRAS_PIVA'];
     	          $html_descrizioni .= ' - P.iva trasp.: ' . $from_sped['BOOKING']['TRAS_PIVA'];
     	      }
     	      
     	      $html_descrizioni .= "<br>";     	      
     	    }
     	    
     	    
     	    if ($sped_con_booking > 0 && $sped_senza_booking > 0)
     	        $html_descrizioni .= "<h3><font color=red>Attenzioni: booking presente solo su alcune spedizioni</h3>";
     	    if ($tras_piva_error == true)
 	            $html_descrizioni .= "<h3><font color=red>Attenzioni: P.IVA trasportatori non coincidenti su richieste booking</h3>";
     	    
     	    ?>
     	  {
     	    xtype: 'container', layout: {type :'hbox', pack: 'start', align: 'stretch'}, flex: 1, items: [
     	    
     	     {
     	      xtype: 'container', layout: {type :'vbox', pack: 'start'}, flex: 1.5, 
     	      defaults: {hideTrigger: true},
     	      items: [  	     	
                 {	border: false,
                 	html: '<h1>Unisci su singolo programma di spedizione</h1>', padding: 10
                 }
                 
                 , {
					name: 'f_descr',
					xtype: 'textfield', fieldLabel: 'Descrizione',
				    value: <?php echo j($descrizione_proposta)?>,
				    maxLength: 100,	
				    width: '90%'						
				 }
				 
				 , {
					name: 'f_nota',
					xtype: 'textfield', fieldLabel: 'Nota',
				    value: <?php echo j($nota_proposta)?>,
				    maxLength: 100,	
				    width: '90%'						
				 }
                 , {             	
                 	xtype: 'numberfield', 
                 	name: 'f_peso',
                 	fieldLabel: 'Peso',
                 	width: 170,
                 	value: <?php echo j($peso_proposta)?>,
                 }, {             	
                 	xtype: 'numberfield', 
                 	name: 'f_volume',
                 	fieldLabel: 'Volume',
                 	width: 170,
                 	value: <?php echo j($volume_proposta)?>,
                 }, {             	
                 	xtype: 'numberfield', 
                 	name: 'f_pallet',
                 	fieldLabel: 'Pallet',
                 	width: 170,
                 	value: <?php echo j($pallet_proposta)?>,
                 }, {             	
                 	xtype: 'textfield', 
                 	name: 'f_targa',
                 	fieldLabel: 'Targa',
                 	width: 250, maxLength: 20,
                 	value: <?php echo j($targa_proposta)?>,
                 }
                 
                 <?php if ($sped_con_booking > 0){ ?>
                 
					, {
		                            xtype: 'checkbox'
		                          , fieldLabel: 'Duplica booking da sped. principale'
		                          , name: 'f_duplica_booking' 
		                          , boxLabel: 'Si'
		                          , checked: true
		                          , inputValue: 'Y'
		             }                 
                 
                 <?php } ?>
                 
                 
               ]
             },
             
             {
     	      xtype: 'container', layout: {type :'vbox', pack: 'start', align: 'stretch'}, flex: 1, items: [  	               
                 {
                 	html: 'Riepilogo spedizioni di partenza', border: false
                 }, {
                 	html: <?php echo j($html_descrizioni) ?>, border: false
                 }
              ]
             }
             
            ]
          }
        <?php } else { ?>
 			{	flex: 1, border: false,
             	html: 'Accoda a programma di spedizione'
             }        
        <?php } ?>     
             
             , {	height: 60,
                xtype: 'button',
                text: 'Conferma', 
                width: '100%',
                margin: '10 10 10 10',
                handler: function(){
                	var loc_win = this.up('window');
                	var m_form = this.up('form');
                	
            			 Ext.Ajax.request({
            			        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=<?php echo $go_to_fn ?>',
            			        timeout: 2400000,
            			        method     : 'POST',
                    			jsonData: {
                    				from_gate: <?php echo json_encode($m_params) ?>,
                    				form_values: m_form.getValues()
                    			},							        
            			        success : function(result, request){
            			        	var jsonData = Ext.decode(result.responseText);
            			        	if (jsonData.success == true)
            			    			loc_win.fireEvent('onSelected', loc_win);
            			    		else
            			    			acs_show_msg_error(jsonData.message);	 			            			
                        		},
            			        failure    : function(result, request){
            			            Ext.Msg.alert('Message', 'No data to be loaded');
            			        }
            			    });	
                	
                	
                }
             }
         ]
       }
    ]}
<?php    
 exit;
}


// ******************************************************************************************
// PANNELLO DI CONFERMA (RIPRISTINA)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_ripristina'){
    $go_to_fn = 'exe_ripristina_carico';
    ?>
    {"success":true, "items": [
     {
     	xtype: 'panel',
     	layout : {type :'vbox', pack: 'start', align: 'stretch'},
     	items: [
             {	flex: 1, margin: '10 10 10 10', border: false,
             	html: 'Ripristina spedizione in base a programma di produzione'
             },
             {	height: 60,
                xtype: 'button',
                text: 'Conferma',
                width: '100%',
                margin: '10 10 10 10',
                handler: function(){
                	var loc_win = this.up('window');
                	
            			 Ext.Ajax.request({
            			        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=<?php echo $go_to_fn ?>',
            			        timeout: 2400000,
            			        method     : 'POST',
                    			jsonData: <?php echo json_encode($m_params) ?>,							        
            			        success : function(result, request){
            			        	var jsonData = Ext.decode(result.responseText);
            			        	if (jsonData.success == true)
            			    			loc_win.fireEvent('onSelected', loc_win);
            			    		else
            			    			acs_show_msg_error(jsonData.message);	 			            			
                        		},
            			        failure    : function(result, request){
            			            Ext.Msg.alert('Message', 'No data to be loaded');
            			        }
            			    });	
                	
                	
                }
             }
         ]
       }
    ]}
<?php    
 exit;
}
