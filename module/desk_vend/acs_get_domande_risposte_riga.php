<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();



if ($_REQUEST['fn'] == 'exe_modifica'){
    $ret = array();
    $ar_ins = array();
    
    
    if($m_params->new == 'Y'){
        
        $sql = "SELECT *
        FROM {$cfg_mod_Spedizioni['file_righe_dom_risp']} VD
        WHERE RRN(VD)  = '{$m_params->RRN}'";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        for($i = 1; $i <= 60; $i++){
            $dom = 'VDVAR'.sprintf("%02s", $i);
            $num = 'VDVAN'.sprintf("%02s", $i);
            $swn = 'VDSWN'.sprintf("%02s", $i);
            $risp = 'VDVAL'.sprintf("%02s", $i);
            if(trim($row[$dom]) == ''){
                
                if(isset($m_params->form_values->dom_c) && $m_params->form_values->dom_c!= '')
                    $ar_ins[$dom] = $m_params->form_values->dom_c;
                if($m_params->form_values->var_type == 'N'){
                    $ar_ins[$num] = sql_f($m_params->form_values->f_van_n);
                    $ar_ins[$swn] = 'N';
                }
                if(isset($m_params->form_values->ris_c) && $m_params->form_values->ris_c != '')
                    $ar_ins[$risp] = $m_params->form_values->ris_c;
                
                break;
            }
                
        }
        
        
        
       
        
    }elseif($m_params->elimina == 'Y'){
        
        $i = $m_params->num;
        $dom = 'VDVAR'.sprintf("%02s", $i);
        $num = 'VDVAN'.sprintf("%02s", $i);
        $num_v = "f_N_{$i}";
        $swn = 'VDSWN'.sprintf("%02s", $i);
        $risp = 'VDVAL'.sprintf("%02s", $i);
        
          $ar_ins[$dom] = '';
          if(isset($m_params->form_values->$num_v) && $m_params->form_values->$num_v != ''){
              $ar_ins[$num] = 0;
              $ar_ins[$swn] = '';
          }
          $ar_ins[$risp] = '';
    }else{
        
        for($i = 1; $i <= 60; $i++){
            $dom = 'VDVAR'.sprintf("%02s", $i);
            $dom_v = "f_D_{$i}";
            $num = 'VDVAN'.sprintf("%02s", $i);
            $num_v = "f_N_{$i}";
            $swn = 'VDSWN'.sprintf("%02s", $i);
            $risp = 'VDVAL'.sprintf("%02s", $i);
            $risp_v = "f_R_{$i}";
            
                if(isset($m_params->form_values->$dom_v) && $m_params->form_values->$dom_v != '')
                    $ar_ins[$dom] = $m_params->form_values->$dom_v;
                if(isset($m_params->form_values->$num_v) && $m_params->form_values->$num_v != ''){
                    $ar_ins[$num] = sql_f($m_params->form_values->$num_v);
                    $ar_ins[$swn] = 'N';
                }
                if(isset($m_params->form_values->$risp_v) && $m_params->form_values->$risp_v != '')
                    $ar_ins[$risp] = $m_params->form_values->$risp_v;
         
         
        }
        
        
    }
    
    
    
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_righe_dom_risp']} VD
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(VD) = '{$m_params->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'get_data_puvn'){
    
    $ar = array();
    $m_params = acs_m_params_json_decode();
    
    if(strlen($m_params->domanda)>0){
        $where .= " AND TACOR2 = '{$m_params->domanda}'";
    }else{
        $where .= " ";
    }
    
    
    $sql = "SELECT *
    FROM {$cfg_mod_Admin['file_tab_sys']}
    WHERE TADT = '$id_ditta_default' AND TAID = 'PUVN' AND TATP <> 'S' AND TALINV=''
    {$where} ORDER BY TANR, TADESC";
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $text = "[" . trim($row['TANR']) . "] " . trim($row['TADESC']);
        
        $ret[] = array( "id" 	=> trim($row['TANR']),
            "text" 	=> acs_u8e($text) );
    }
    
    
    
    
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_salva_traduzione'){
    
    $m_params = acs_m_params_json_decode();
    
    $icon = "";
    $sql_where = ""; 
    $ar_ins = array();
    
    $tarest = "";
    $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_3)));
    $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_4)));
    $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_5)));
    
    $ar_ins["TADESC"] = acs_toDb(trim($m_params->form_values->f_text_1));
    $ar_ins["TADES2"] = acs_toDb(trim($m_params->form_values->f_text_2));
    $ar_ins["TAREST"] = $tarest;
    
    
    if(isset($m_params->rrn) && trim($m_params->rrn) != ''){
              
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_tab_sys']} TA
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(TA) = '{$m_params->rrn}'
            ";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
       
     }else{
             
            $ar_ins["TADT"]   = $id_ditta_default;
            $ar_ins["TANR"]  = $m_params->codice;
            $ar_ins["TALINV"]  = $m_params->lng;
            $ar_ins["TAID"]  = $m_params->taid;
            
            if(isset($m_params->tacor2) && strlen($m_params->tacor2) > 0){
                $ar_ins["TACOR1"]  = "";
                $ar_ins["TACOR2"]  = $m_params->tacor2;
                
            }
                     
            $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tab_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
        
        
    }
    
   
    if(isset($m_params->tacor2) && strlen($m_params->tacor2) > 0){
        $sql_where = " AND TACOR1 = '' AND TACOR2 = '{$m_params->tacor2}'"; 
        
    }
    
    $sql_v = "SELECT TADESC
    FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA
    WHERE TADT = '{$id_ditta_default}'
    AND TANR = '{$m_params->codice}'
    AND TAID = '{$m_params->taid}'
    AND TALINV = '{$m_params->lng}'";
    
    $stmt_v = db2_prepare($conn, $sql_v);
    $result = db2_execute($stmt_v);
    $row_v = db2_fetch_assoc($stmt_v);
    $globe_icon = trim($row_v['TADESC']);
    
    if($globe_icon != '')
		$icon = "<img src=" . img_path("icone/48x48/globe.png") . " width=16>";
	else				
	    $icon = "<img src=" . img_path("icone/48x48/globe_error.png") . " width=16>";
	 
    $ret = array();
    $ret['success'] = true;
    $ret['icon'] = $icon;
    $ret['tip'] = trim($row_v['TADESC']);
    echo acs_je($ret);
    exit;
    
    
}



if ($_REQUEST['fn'] == 'open_form'){

$k_ordine = $m_params->k_ordine;
$oe = $main_module->k_ordine_td_decode_xx($k_ordine);
$prog = $m_params->prog;

$trad = 'Y';

$ord = $main_module->get_ordine_by_k_docu($k_ordine);
$lingua = $ord['TDRFLO'];

if (trim($lingua) == '')    //non traduciamo se non e' impostata la lingua
    $trad = 'N';

	$sql = "SELECT VD.*, RRN(VD) AS RRN
			FROM {$cfg_mod_Spedizioni['file_righe_dom_risp']} VD
			WHERE VDDT = '{$oe['TDDT']}' AND VDPROG = {$prog} AND VDTIPO = ''";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']));
	$row = db2_fetch_assoc($stmt);

	
	?>


{"success":true, "items": [

        {
            xtype: 'form',
            autoScroll : true,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
            
            items: [
            
            <?php 
               $n = 0;
               for($i = 1; $i <= 60; $i++){
               
                $dom = 'VDVAR'.sprintf("%02s", $i);
           
                if(trim($row[$dom]) != ''){
                    $n = $i;
                    
                    if($trad == 'Y'){
                        
                        $sql_d = "SELECT TADESC
                        FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA
                        WHERE TADT = '{$id_ditta_default}'
                        AND TANR = '".trim($row[$dom])."'
                        AND TAID = 'PUVR'
                        AND TALINV = '{$lingua}'";
                        
                        $stmt_d = db2_prepare($conn, $sql_d);
                        $result = db2_execute($stmt_d);
                        $row_d = db2_fetch_assoc($stmt_d); 
                        
                        $globe_dom = trim($row_d['TADESC']);
                    }
             
                    
            	$val_dom = find_TA_sys('PUVR', trim($row[$dom]));
            	$value_D = "{$i}) [".$row[$dom]."] ".$val_dom[0]['text'];
            	$swn = 'VDSWN'.sprintf("%02s", $i);
            	
            	if(trim($row[$swn]) == 'N'){
            		$risp = 'VDVAN'.sprintf("%02s", $i);
            		$value_R =  $row[$risp];
            	}else{ 
            		$risp = 'VDVAL'.sprintf("%02s", $i);
            		$val_risp = find_TA_sys('PUVN', trim($row[$risp]), null, trim($row[$dom]));
            		$value_R = "[".$row[$risp]."] ".$val_risp[0]['text'];
            		
            		
            		if($trad == 'Y'){
            		    
            		    $sql_r = "SELECT TADESC
            		    FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA
            		    WHERE TADT = '{$id_ditta_default}'
            		    AND TANR = '".trim($row[$risp])."'
            		    AND TAID = 'PUVN'
                        AND TACOR1  = ''
                        AND TACOR2 = '".trim($row[$dom])."'
            		    AND TALINV = '{$lingua}'";
            		    
            		    $stmt_r = db2_prepare($conn, $sql_r);
            		    $result = db2_execute($stmt_r);
            		    $row_r = db2_fetch_assoc($stmt_r);
            		    $globe_risp = trim($row_r['TADESC']);
            		}
          
            		
            	}
            	
            	if($i % 2 == 0)
            	    $class = "row_p";
            	else
            	    $class = "row_d";
            	
            	//$last_num = $n;
            
            ?>
            
                       {
						xtype: 'fieldcontainer',
						cls: '<?php echo $class; ?>',	
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 <?php if($trad == 'Y'){ ?>
							{										  
								xtype: 'displayfield',
								fieldLabel: '',
								padding: '0 0 0 10',
								width : 30,
								<?php if($globe_dom != ''){?>
							    	value: <?php echo j("<img src=" . img_path("icone/48x48/globe.png") . " width=16>"); ?>,
								    tip: <?php echo j($globe_dom); ?>,
								<?php }else{?>
									value: <?php echo j("<img src=" . img_path("icone/48x48/globe_error.png") . " width=16>"); ?>,
								<?php }?>
								listeners: {
						            render: function( component ) {
						            
						            <?php if($globe_dom != ''){?>
						                 Ext.create('Ext.tip.ToolTip', {
                                        target: component.getEl(),
                                        html: component.tip 
                                        });
                                        
                                     <?php }?>
						            
						            	m_form = this.up('form').getForm();
						            	component.getEl().on('dblclick', function( event, el ) {
										
									    my_listeners = {
				        					afterSave: function(from_win, src){
				        					   		component.setValue(src.icon);
				        					   		from_win.close();
									        		}										    
									    },
										
											 acs_show_win_std('Traduzione in ' + <?php echo j($lingua); ?>, 
					    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
					    					{codice: <?php echo j($row[$dom]); ?>, desc : <?php echo j($val_dom[0]['text']); ?>, lng: <?php echo j($lingua); ?>, taid : 'PUVR'}, 400, 270,  my_listeners, 'icon-globe-16');
											
											
										});										            
						             }
								}
						
						  },
						  <?php }?>
						  
						  <?php if($m_params->modifica == 'Y'){?>
						  
						  {
								name: 'f_D_<?php echo $i; ?>',
								xtype: 'combo',
								flex: 1,
								fieldLabel : <?php echo j("{$i})") ?>,
								labelWidth : 20,
								margin : '0 5 0 0',
								value: <?php echo j(trim($row[$dom])) ?>,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',
								queryMode: 'local',
								minChars: 1,							
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}, {name : 'tarest'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 1, '', 'Y'), ''); ?>	
								    ]
								}, listeners: {
								
								    select: function(combo, row, index) {
    	            	               var num = row[0].get('tarest');
    	            	               if(num == 'N'){
    	            	                  this.up('window').down('#c_risp_<?php echo $i?>').hide();
    	            	                  this.up('window').down('#c_num_<?php echo $i?>').show();
    	            	               }else{ 
    	            	                  this.up('window').down('#c_risp_<?php echo $i?>').show(); 
    	            	                  this.up('window').down('#c_num_<?php echo $i?>').hide(); 
    	            	              
     								   }
							     	},
                                	 change: function(field,newVal) {	
                                  		if (!Ext.isEmpty(newVal)){
                                        	combo_risp = this.up('window').down('#c_risp_<?php echo $i?>');                      		 
                                         	combo_risp.store.proxy.extraParams.domanda = newVal;
                                        	combo_risp.store.load();                             
                                         }
                                         
            
                                        }, beforequery: function (record) {
                                        record.query = new RegExp(record.query, 'i');
                                        record.forceAll = true;
                                    }
                           }
								
						   }
						  
						  <?php }else{?>
						  
						    {						
								name: 'f_D',
								xtype: 'displayfield',
								value: <?php echo j($value_D) ?>,
								labelWidth : 250,
								flex: 1,
								hideTrigger:true				
							}
						  
						  <?php }?>
						 
						  
						   
							  
							<?php  if($trad == 'Y'){?>
							, {										  
								xtype: 'displayfield',
								fieldLabel: '',
								width : 30,
							  <?php if($globe_risp != ''){?>
							    	value: <?php echo j("<img src=" . img_path("icone/48x48/globe.png") . " width=16>"); ?>,
								    tip: <?php echo j($globe_risp); ?>,
								<?php }else{?>
									value: <?php echo j("<img src=" . img_path("icone/48x48/globe_error.png") . " width=16>"); ?>,
								<?php }?>
							    listeners: {
						            render: function( component ) {
						           
						           <?php if($globe_risp != ''){?>
						           		Ext.create('Ext.tip.ToolTip', {
                                        target: component.getEl(),
                                        html: component.tip 
                                        }); 
                                   <?php }?>     
						            
						            	m_form = this.up('form').getForm();
						            	component.getEl().on('dblclick', function( event, el ) {
										
									     my_listeners = {
				        					afterSave: function(from_win, src){
				        					   		component.setValue(src.icon);
				        					   		from_win.close();
									        		}										    
									    },
										
									   	 acs_show_win_std('Traduzione' , 
					    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
					    					{codice: <?php echo j($row[$risp]); ?>, desc : <?php echo j($val_risp[0]['text']); ?>, lng: <?php echo j($lingua); ?>, taid : 'PUVN', tacor2 : <?php echo j($row[$dom])?>}, 400, 270,  my_listeners, 'icon-globe-16');
											
											
										});									            
						             }
								}
							    
							    
							    }
							    <?php }?> 
							    
							      <?php if($m_params->modifica == 'Y'){ ?>
							          
							           , {						
        								name: 'f_N_<?php echo $i; ?>',
        								xtype: 'numberfield',
        								itemId: 'c_num_<?php echo $i?>',
        								value: <?php echo j($value_R) ?>,
        								labelWidth : 250,
        								flex: 1,
        								decimalPrecision : 3,
        								hidden : true,
        								hideTrigger:true				
        							    },{
        								name: 'f_R_<?php echo $i; ?>',
        								xtype: 'combo',
        								flex: 1,
        								fieldLabel : '',
        								itemId: 'c_risp_<?php echo $i?>',
        								forceSelection: true,	
        								labelAlign : 'right',							
        								displayField: 'text',
        								valueField: 'id',								
        								emptyText: '- seleziona -',
        								queryMode: 'local',
        								minChars: 1,	
        						   		//allowBlank: false,								
        							    anchor: '-15',
        							    store: {
									        autoLoad: true,
											proxy: {
									            type: 'ajax',
									            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_puvn',
									            actionMethods: {
            							          read: 'POST'
           							        },
							                	extraParams: {
							    		    		domanda: <?php echo j($row[$dom])?>,
							    				},				            
									            doRequest: personalizza_extraParams_to_jsonData, 
									            reader: {
               						            type: 'json',
               									method: 'POST',						            
                						            root: 'root'						            
                						        }
									        },       
											fields: ['id', 'text'],		             	
							            }
        								, listeners: {
                                        	    beforequery: function (record) {
                                                record.query = new RegExp(record.query, 'i');
                                                record.forceAll = true;
                                                
                                            },
                                            afterrender: function(comp){
                    						   data = [{id: <?php echo j(trim($row[$risp]))?>, text: <?php echo j($value_R) ?>}
                                                 ];
                                               comp.store.loadData(data);
                                               comp.setValue(<?php echo j(trim($row[$risp]))?>);
                                               <?php if(trim($row[$swn]) == 'N'){?>
                                           		 comp.hide();
                                           		 comp.up('window').down('#c_num_<?php echo $i?>').show();  
                                               <?php }?>
                                                           
                                              } 
                                   }
            						   },
            						   
            						   { xtype: 'button'
                                     , margin: '0 0 0 5'
                                     , scale: 'small'
                                     , iconCls: 'icon-sub_red_delete-16'
                                     , iconAlign: 'top'
                                     , width: 25
                                     , handler : function() {
                                          var form = this.up('form').getForm();
                                          var loc_win = this.up('window');
                                          Ext.Ajax.request({
            						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
            						        jsonData: {
            						        	form_values : form.getValues(),
            						        	RRN : <?php echo j($row['RRN'])?>,
            						        	elimina : 'Y',
            						        	num : <?php echo j($i)?>
            						        	
            						        },
            						        method     : 'POST',
            						        waitMsg    : 'Data loading',
            						        success : function(result, request){
            						            var jsonData = Ext.decode(result.responseText);
            						           loc_win.close();
            						           acs_show_win_std('Configurazione ordine', 
            						    		'acs_get_domande_risposte_riga.php?fn=open_form', {
            						    			k_ordine: <?php echo j($k_ordine); ?>, 
            						    			prog: <?php echo j($prog); ?>, 
            						    			from_righe_info : 'Y',
            						    			modifica : 'Y'
            						    		}, 900, 450, null, 'icon-pencil-16');	
            						                    
            						        },
            						        failure    : function(result, request){
            						            Ext.Msg.alert('Message', 'No data to be loaded');
            						        }
            						    });
                                           
                                         }
                                         
                                    }
            							      
							     
							       
							      <?php }else{?>
							      
							      ,{						
    								name: 'f_R',
    								xtype: 'displayfield',
    								value: <?php echo j($value_R) ?>,
    								labelWidth : 250,
    								flex: 1,
    								hideTrigger:true				
    							   }
							      
							      <?php }?>
							      
							    
						]
				},
				
				<?php } } ?>	  	
				 
				    {						
						name: 'f_numeratore',
						xtype: 'textfield',
						value: <?php echo j($n) ?>,
						hidden : true				
					   }
					                    
            ]   
            <?php if($m_params->modifica == 'Y'){?>
            , buttons: [					
					{
			            text: 'Aggiungi',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			            	var num = form.getValues().f_numeratore;
			            	
			            	<?php $config = array('output_domanda' => true);?>
			                acs_show_win_std('Seleziona valori'
                                        , '../base/decodifica_dom_ris.php?fn=open_win'
                                        , {tipo_opz_risposta : <?php echo acs_je($config); ?>, file_TA : 'sys'}, 600, 220, {
                                         'afterSave': function(from_win, formValues){
                                     		Ext.Ajax.request({
                						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
                						        jsonData: {
                						        	form_values : formValues,
                						        	numeratore : num,
                						        	new : 'Y',
                						        	RRN : <?php echo j($row['RRN'])?>
                						        },
                						        method     : 'POST',
                						        waitMsg    : 'Data loading',
                						        success : function(result, request){
                						          from_win.destroy();
                						          loc_win.destroy();	
                            				      acs_show_win_std('Configurazione ordine', 
            						    		  'acs_get_domande_risposte_riga.php?fn=open_form', {
            						    			k_ordine: <?php echo j($k_ordine); ?>, 
            						    			prog: <?php echo j($prog); ?>, 
            						    			from_righe_info : 'Y',
            						    			modifica : 'Y'
            						    		}, 900, 450, null, 'icon-pencil-16');		            
                						        },
                						        failure    : function(result, request){
                						            Ext.Msg.alert('Message', 'No data to be loaded');
                						        }
                						    });	
			   								
											}}
                                        	, 'icon-search-16');
			                
			            }
			        }, '->', {
			            text: 'Salva',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			                Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        jsonData: {
						        	form_values : form.getValues(),
						        	RRN : <?php echo j($row['RRN'])?>
						        	
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						          var jsonData = Ext.decode(result.responseText);
						          loc_win.close();
						                     
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			                
			            }
			        } 
		        
		        
		        ] 
            <?php }?>
           
}
	
]}

<?php 
exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    $sql_where = "";
    
    if(isset($m_params->tacor2) && strlen($m_params->tacor2) > 0)
        $sql_where = " AND TACOR2 = '{$m_params->tacor2}'";
        
    $sql = "SELECT RRN(TA) AS RRN, TADESC,TADES2,TAREST
            FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA
            WHERE TADT = '{$id_ditta_default}'
            AND TANR = '{$m_params->codice}'
            AND TAID = '{$m_params->taid}'
            AND TALINV = '{$m_params->lng}'
            {$sql_where}";
            
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt); 
    
    $title = "<b>[". trim($m_params->codice) ."] {$m_params->desc}<b>";
    $trad3 = substr(acs_u8e($row['TAREST']), 0, 30);
    $trad4 = substr(acs_u8e($row['TAREST']), 30, 30);
    $trad5 = substr(acs_u8e($row['TAREST']), 60, 30);
 
    
    ?>
 {"success":true, "items": [
        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            
					buttons: [{
			            text: 'Salva',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var form_values = form.getValues();
							 var loc_win = this.up('window');
							
							 
								if(form.isValid()){
									Ext.Ajax.request({
									        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_salva_traduzione',
									        jsonData: {
									        	form_values: form.getValues(),
									        	codice : '<?php echo $m_params->codice; ?>',
									        	lng : '<?php echo $m_params->lng; ?>',
									        	rrn : '<?php echo $row['RRN']; ?>',
									        	taid : '<?php echo $m_params->taid; ?>',
									        	tacor2 : '<?php echo $m_params->tacor2; ?>',
									        	
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){	            	  													        
									            var jsonData = Ext.decode(result.responseText);
									          	loc_win.fireEvent('afterSave', loc_win, jsonData);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									            console.log('errorrrrr');
									        }
									    });	  
							    }							 
				
			            }
			         }],   		            
		
		            items: [{
    					xtype: 'fieldset',
    	                title: <?php echo j($title)?>,
    	                layout: 'anchor',
    	                flex:1,
    	                items: [
    	     
    	                    {
    	                        name: 'f_text_1',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim($row["TADESC"])); ?>,
    	                    },	{
    	                        name: 'f_text_2',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim($row["TADES2"])); ?>,
    	                    },
    	                    {
    	                        name: 'f_text_3',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim($trad3)); ?>,
    	                    },{
    	                        name: 'f_text_4',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim($trad4)); ?>,
    	                    },{
    	                        name: 'f_text_5',
    	                        xtype: 'textfield',
    	                        fieldLabel: '',
    	                        anchor: '-15',
    	                        maxLength: 30,
    	                        value: <?php echo j(trim($trad5)); ?>,
    	                    }
    	                	
								
									
					 
	             ]}
								 
		           			]
		              }
		
			]
}	
	
	
	
<?php 
exit;
}

