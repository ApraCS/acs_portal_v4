<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();
 
$itin_id 	= trim($m_params->itin_id);
$sped_ar 	= trim($m_params->sped_ar);
$data 		= $m_params->data;
$tree_id 	= $m_params->tree_id;

$itin_descr	= $s->decod_std('ITIN', $itin_id);

$itin_obj = new Itinerari();
$itin_obj->load_rec_data_by_k(array('TAKEY1' => $itin_id));

//parametri del modulo (non legati al profilo)
$mod_js_parameters = $s->get_mod_parameters();


//***********************************************************************
// ELENCO SPEDIZIONI/CARICHI
//***********************************************************************
if ($_REQUEST['fn'] == 'unisci_carico'){
	
	$s_from = explode("|", $m_params->sped_car_from);
	$sped_from 			= $s_from[0];
	$carico_txt_from 	= $s_from[1];
	$carico_ar_from		= $s->k_carico_td_decode($carico_txt_from);
	
	$s_to = explode("|", $m_params->sped_car_to);
	$sped_to 			= $s_to[0];
	
	$sped               = $s->get_spedizione($sped_to);
	$sped_from_row      = $s->get_spedizione($sped_from);
		
	//recupero l'elenco degli ordini da modificare
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} TD
							WHERE " . $s->get_where_std() . "
							AND TDNBOC = " . $sped_from . "
							AND TDTPCA = " . sql_t($carico_ar_from['TDTPCA']) . "
							AND TDAACA = " . $carico_ar_from['TDAACA'] . "
							AND TDNRCA = " . $carico_ar_from['TDNRCA'] . "
							";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	while ($row_test = db2_fetch_assoc($stmt)){
		$k_ord = $row_test['TDDOCU'];
		$ord = $s->get_ordine_by_k_docu($k_ord);

		
		//update				
		$sqlU = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDNBOC = ?, TDNBOF=? WHERE TDDOCU = ?";		
		$stmtU = db2_prepare($conn, $sqlU);
		
		//Mantieni allineato TDNBOC|TDNBOF
		if ($ord['TDNBOC'] == $ord['TDNBOF'])
		  $allinea_TDNBOF = $sped_to;
		else
		  $allinea_TDNBOF = $ord['TDNBOF'];   //lascio l'attuale
		        		
		$result = db2_execute($stmtU, array($sped_to, $allinea_TDNBOF, $k_ord));
		
		//devo prima allineare i dati di testata per essere sicuro che venga spostato dal trigger sulla sped selezionata
		$s->exe_allinea_dati_spedizione_su_testata($sped, $k_ord);

		$sh = new SpedHistory();
		$sh->crea(
				'assegna_spedizione',
				array(
						"k_ordine" 	=> $k_ord,
						//"data"		=> $sped['CSDTSP'],
						///"k_carico"	=> $r->to_carico,
						///"nuovo_stato_ordine" => $nuovo_stato_ordine,
		
						//"seca"		=> $ar_upd['TDSECA'],
						///"RIIVES"	=> $ar_upd['RIIVES'],
						///"RICVES"	=> $ar_upd['RICVES'],
		
						"sped"		=> $sped,
						///"registra_data_confermata" => $registra_data_confermata,
						//"data_confermata" => $sped['CSDTSP'],
						"op"		=> "ASS",
						"ex"		=> "(PRG UN.CARICO: #{$ord['TDNBOC']})",
						"seca"		=> $ord['TDSECA'] //attualmente va passata altrimenti vienere resettata
				)
		);

	}
	

	$s->accoda_descrizione_sped($sped, $sped_from_row);			
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


//***********************************************************************
// ELENCO SPEDIZIONI/CARICHI
//*********************************************************************** 
if ($_REQUEST['fn'] == 'grid_elenco'){

	
	$sped_ar = $_REQUEST['sped_ar'];
	if (strlen($sped_ar)>0){
		$where_sped =  " AND CSPROG IN ({$sped_ar})";
	}
	
		//recupero l'elenco delle spedizioni/carichi per itinerario/giorno
		$sql = "SELECT CSPROG, CSCVET, CSCAUT, CSCCON, CSTISP, CSTITR, CSVOLD, CSPESO,
						TD.TDTPCA, TD.TDAACA, TD.TDNRCA,
						sum(TDVOLU) as S_VOL, sum(TDPLOR) as S_PESO, sum(TDTIMP) as S_IMPORTO
				FROM {$cfg_mod_Spedizioni['file_calendario']} SP
				  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD ON SP.CSPROG = TD.TDNBOC
				WHERE CSDT='$id_ditta_default' AND CSCALE = '*SPR' $where_sped AND CSCITI = ? AND CSDTSP = ? 
			GROUP BY CSPROG, CSSTSP, CSCVET, CSCAUT, CSCCON, TD.TDTPCA, TD.TDAACA, TD.TDNRCA, CSTISP, CSTITR, CSVOLD, CSPESO
			HAVING MAX(TD.TDONDO) IS NOT NULL OR CSSTSP NOT IN('GA', 'AN') 			
			ORDER BY CSPROG, TD.TDTPCA, TD.TDAACA, TD.TDNRCA
			";

		
		
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($_REQUEST['itin_id'], $_REQUEST['data']));
    $data = array();

	$ar = array();	
	$ar_liv_mapping = array();	
	$n_children = "children";	
	while ($row = db2_fetch_assoc($stmt)) {

		$row['k_sped_carico'] = implode("|", array($row['CSPROG'], $s->k_carico_td_encode($row)));
		$row['carico'] = $s->k_carico_td_encode($row);
		$row['vmc'] = $s->decod_vmc_by_sped_row($row, " - ");
		
		//definizione livello di alberatura
		$liv0_v = trim($row['CSPROG']); //TIPO OPERAZIONE
		$liv1_v = trim($row['carico']); //AREA SPEDIZIONE

		// LIVELLO 0 (SPEDIZIONE)
		$s_ar = &$ar;
		$m_ar = &$ar_liv_mapping;
		$liv 	= $liv0_v;
		$liv_c 	= &$liv0_c;
		if (is_null($m_ar[$liv])){
			if (!isset($m_ar['cont'])) $m_ar['cont'] = 0;
			$liv_c = $m_ar['cont']++;
			$m_ar[$liv] = array("cont" => 0, $n_children => array());
			$s_ar[$liv_c] = array("cod"=>$liv, "liv"=>"liv_0");
			
			$s_ar[$liv_c]['expanded']	= true;		
			$s_ar[$liv_c]['iconCls']	= 'iconSpedizione';	
			
			$s_ar[$liv_c]["CSPROG"]  	= $row['CSPROG'];
			$s_ar[$liv_c]["CSTISP"]  	= $row['CSTISP'];
			$s_ar[$liv_c]["CSTITR"]  	= $row['CSTITR'];
			
			$s_ar[$liv_c]["CSVOLD"]  	= $row['CSVOLD'];
			$s_ar[$liv_c]["CSPESO"]  	= $row['CSPESO'];

			$s_ar[$liv_c]["TDTPCA"]  	= $row['TDTPCA'];
			$s_ar[$liv_c]["TDAACA"]  	= $row['TDAACA'];			
			$s_ar[$liv_c]["TDNRCA"]  	= $row['TDNRCA'];						
		}
		$tmp_ar_id[] = $liv;
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
		$s_ar[$liv_c]["task"]   = "Spedizione <b>#{$row['CSPROG']}</b> {$row['CSTISP']} {$row['CSTITR']}";
		
		$s_ar[$liv_c]["vmc"]  	= $row['vmc'];
				
		$s_ar[$liv_c]["S_VOL"]  		+= $row['S_VOL'];		
		$s_ar[$liv_c]["S_PESO"]  		+= $row['S_PESO'];
		$s_ar[$liv_c]["S_IMPORTO"]  	+= $row['S_IMPORTO'];				
		
		

		$s_ar = &$s_ar[$liv_c][$n_children];
		$m_ar = &$m_ar[$liv][$n_children];
			
		
		// LIVELLO 1 (CARICO)
		$liv 	= $liv1_v;
		$liv_c 	= &$liv1_c;
		if (is_null($m_ar[$liv])){
			if (!isset($m_ar['cont'])) $m_ar['cont'] = 0;
			$liv_c = $m_ar['cont']++;
			$m_ar[$liv] = array("cont" => 0, $n_children => array());
			$s_ar[$liv_c] = array("cod"=>$liv, "liv"=>"liv_1");
			
			$s_ar[$liv_c]["CSPROG"]  	= $row['CSPROG'];			
			$s_ar[$liv_c]["TDTPCA"]  	= $row['TDTPCA'];
			$s_ar[$liv_c]["TDAACA"]  	= $row['TDAACA'];
			$s_ar[$liv_c]["TDNRCA"]  	= $row['TDNRCA'];			

		}
		$tmp_ar_id[] = $liv;
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
		$s_ar[$liv_c]["task"] = "";
		$s_ar[$liv_c]["carico"] = "Carico <b>{$liv}</b>";					
		$s_ar[$liv_c]["leaf"] = true;		
		$s_ar[$liv_c]["k_sped_carico"] = $row['k_sped_carico'];
//		$s_ar[$liv_c]["vmc"]  	= $row['vmc'];				

		$s_ar[$liv_c]["S_VOL"]  		+= $row['S_VOL'];
		$s_ar[$liv_c]["S_PESO"]  		+= $row['S_PESO'];
		$s_ar[$liv_c]["S_IMPORTO"]  	+= $row['S_IMPORTO'];		
		
		$s_ar = &$s_ar[$liv_c][$n_children];
		$m_ar = &$m_ar[$liv][$n_children];
		
	}
	
	
	$ret = array();
	foreach($ar as $kar => $r){
		$ret[] = $r;
	}
	
	$ar_ret = array();
	$ar_ret['children'] = $ret;
	
	echo acs_je($ar_ret);
	
	exit();
}

//***********************************************************************
// DESTINAZIONI PER SPEDIZIONE(/CARICO)
//*********************************************************************** 
if ($_REQUEST['fn'] == 'grid_data'){
	
	if (strlen(trim($_REQUEST['nrca'])) == 0){ //se non ho passato numero carico
		$sql_where_carico = '';
		$sql_params_carico = array();
	} else {
		$sql_where_carico = '  AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ? ';
		$sql_params_carico = array($_REQUEST['tpca'], (int)$_REQUEST['aaca'], (int)$_REQUEST['nrca']);
	}

	$itin_obj = new Itinerari();
	$itin_obj->load_rec_data_by_k(array('TAKEY1' => $_REQUEST['itin_id']));	
	
	$sql = "SELECT TDDT, TDNBOC, TDNAZI, TDSECA, TDCCON, TDCDES, TDTDES, TDVET1, TDVET2, TDDLOC, TDPROD, TDDCAP, TDNAZD, TDDNAD, TDDCON, TDIDES, TDTPCA, TDAACA, TDNRCA, 
				SUM(TDVOLU) AS TDVOLU, SUM(TDTOCO) AS TDTOCO, sum(TDPLOR) as S_PESO, sum(TDTIMP) as S_IMPORTO,
				SUM(TDFN02) AS T_CLI_BLOC, SUM(TDFN03) AS T_ORD_BLOC, TDFN18
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			WHERE " . $s->get_where_std() . "
			AND TDNBOC = ? AND TDDTEP = ? {$sql_where_carico}
				GROUP BY TDDT, TDNBOC, TDNAZI, TDSECA, TDCCON, TDDCON, TDIDES, TDCDES, TDTDES, TDVET1, TDVET2, TDDLOC, TDPROD, TDDCAP, TDNAZD, TDDNAD, TDTPCA, TDAACA, TDNRCA, TDFN18
				ORDER BY TDSECA, TDDCON
			";	
	
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array_merge(array((int)$_REQUEST['sped_id'], $_REQUEST['data']), $sql_params_carico));
	
	if (!$result)
		echo db2_stmt_errormsg($stmt);	

	$data = array();
	while ($row = db2_fetch_assoc($stmt)){
		
		$tmp_TDIDES = $row['TDIDES']; //creo variabile di appoggio, altrimenti errore caratteri speciali in gmap_ind
		$row['TDIDES'] = acs_u8e($tmp_TDIDES);		
		$row['TDDLOC'] = acs_u8e($row['TDDLOC']);		
		$row['TDPROD'] = acs_u8e($row['TDPROD']);		
		
		//$row['TDDCON']	= "<H3><SPAN>" . acs_u8e($row['TDDCON']) . "</h3></span>{$row['TDLOCA']}  {$row['TDPROV']} {$row['TDCAP']}";
		$row['k_sped_carico'] = implode("|", array($row['TDNBOC'], $s->k_carico_td_encode($row)));		
		$row['k_cli_des'] = implode("_", array($row['TDDT'], $row['TDCCON'], $row['TDCDES']));
		$row['k_cli_des_ext'] = implode("_", array($row['k_cli_des'], $row['TDTDES'], $row['TDVET1'], $row['TDVET2']));				
//		$row['DEC_DEST']	= "<H3><SPAN>" . acs_u8e($row['TDDLOC']) . "</h3></span>{$row['TDPROD']} {$row['TDDCAP']}";
		$row['DEC_DEST']	= $row['TDDLOC'];
		$row['TDDCON']		= acs_u8e($row['TDDCON']);
		$row['gmap_ind']	= acs_u8e(implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($tmp_TDIDES, 0, 30)))));
			
		if (in_array($row['TDTDES'], array('1', '2'))){
			//$row['DEC_DEST'] = "<img class=\"cell-img\" src=" . img_path("icone/16x16/exchange.png") . ">" . $row['DEC_DEST'];

			if ($row['TDFN18'] == '1') //coincide con quello indicato in anagrafica cliente
				$row['DEC_DEST'] = "<img class=\"cell-img\" src=" . img_path("icone/16x16/exchange_black.png") . ">" . $row['DEC_DEST'];
			else
				$row['DEC_DEST'] = "<img class=\"cell-img\" src=" . img_path("icone/16x16/exchange.png") . ">" . $row['DEC_DEST'];			
		}

		if ($row['TDTDES'] == '1') $row['VETTORE_ID'] = $row['TDVET1'];
		if ($row['TDTDES'] == '2') $row['VETTORE_ID'] = $row['TDVET2'];		
		
		//recupero coordinate
		$coordinate = $s->get_coordinate($row['TDDT'], $row['TDCCON'], $row['TDCDES'], $row['VETTORE_ID'], $row['gmap_ind']);
		if ($coordinate){
			$row['GLID']    = $coordinate['GLID'];			
			$row['LAT'] 	= trim($coordinate['GLLAT']);		
			$row['LNG'] 	= trim($coordinate['GLLNG']);
			$row['ha_coordinate'] = $coordinate['GLVAL'];
			$row['ind_richiesta'] = acs_u8e($coordinate['GLADR']);			
			if ($coordinate['GLMANU'] == 'Y')
				$row['history_coordinate'] = "Immesse il " . print_date($coordinate['GLDTGE']) . " " . print_ora($coordinate['GLHRGE']) . " da " . trim($coordinate['GLUSGE']);
			else			
				$row['history_coordinate'] = "Acquisite il " . print_date($coordinate['GLDTGE']) . " " . print_ora($coordinate['GLHRGE']) . " da " . trim($coordinate['GLUSGE']);				
		} else {
			$row['ha_coordinate'] = '';
		}
		
		//verifico se nella data e' presente in piu' spedizioni/carichi
		$row['ha_anomalia_sped_car'] = $s->ha_anomalie_sped_car($_REQUEST['data'], $row['TDDT'], $row['TDCCON'], $row['TDCDES'], trim($itin_obj->rec_data['TAASPE']));
		
		$data[] = $row;		
	}
	
	if ($_REQUEST['for_gmap'] == 'Y'){
		$ret = array();
		$ret['points'] = $data;
		echo acs_je($ret);			
	} else 		
		echo acs_je($data);
	
	exit();
}


//***********************************************************************
//PANNELLO PRINCIPALE
//***********************************************************************


//verifico se ci sono dati ridondanti (e devo accendere il bottone)
$stmt_anomalie_data_itin = $s->scarichi_ridondanti($data, $itin_obj->rec_data['TAASPE']);
$c_anomalie_data_itin = 0;
while ($row = db2_fetch_assoc($stmt_anomalie_data_itin)){
	//lo segnalo solo se ha scarichi in questo itinerario
	if ($s->ha_scarichi_in_itinerario($data, $row['TDDT'], $row['TDCCON'], $row['TDCDES'], $itin_id)){
		$c_anomalie_data_itin++;
	}
}




//recupero l'elenco delle spedizioni/carichi per itinerario/giorno
$sql = "SELECT CSPROG, CSCVET, CSCAUT, CSCCON, 
				TD.TDTPCA, TD.TDAACA, TD.TDNRCA
				FROM {$cfg_mod_Spedizioni['file_calendario']} SP
				  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD ON SP.CSPROG = TD.TDNBOC
				WHERE CSDT='$id_ditta_default' AND CSCALE = '*SPR' AND CSCITI = ? AND CSDTSP = ?
			GROUP BY CSPROG, CSCVET, CSCAUT, CSCCON, TD.TDTPCA, TD.TDAACA, TD.TDNRCA
			ORDER BY CSPROG, TD.TDTPCA, TD.TDAACA, TD.TDNRCA
			";

$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, array($itin_id, $data));

$ar_grids = array();
$ar_stores = array();


	$start_point_coordinate = $s->get_start_map('AR', $itin_obj->rec_data['TAASPE']);
	


	$code_gmap = ", {
	                    iconCls: 'icon-gmaps_logo-16',
	                    itemId: 'gmap_sped',
	                    text: 'Google Maps',
	                    disabled: false,
	                    scope: this,
	                    handler: function(bt){	                    	
	                    	
		                    my_storeId = bt.up('grid').store.storeId;
							if (my_storeId == 'ext-empty-store' || typeof(my_storeId)==='undefined') {

								acs_show_msg_error('Nessuna spedizione/carico selezionata');
								
								return;
							}	                    
	                    
	                    
						    var map;
						    var item, items, markers_data = [], errors_data = [];   
						    var responseCount = 0, addressCount;
						    var geocoder = new google.maps.Geocoder();
						    var data;
						    
		       				selected_id = Ext.pluck(bt.up('grid').getStore().data.items, 'data'),
		       				
		       				//se hatto tutti le coordinate mostro la mappa
		       				manca_coordinate = 'N';
						        for (var i = 0; i < selected_id.length; i++) {          
								  if ( selected_id[i]['ha_coordinate'] != 'Y')
								   manca_coordinate = 'Y';         
						        }		       				
		       				
						    if (manca_coordinate == 'N'){
						    	grid_id = bt.up('grid').up('panel').id;

						    	if (grid_id == 'grid_lx')
						    	 seconda_grid = '#grid_rx';
						    	else
						    	 seconda_grid = '#grid_lx';

								sec_grid = bt.up('window').down(seconda_grid).items.items[0];						    	 
						    	 
						    	gmapPopup('gmap_sped.php?data={$data}&sped_car=' + bt.up('grid').getStore().storeId + '&sped_car2=' + sec_grid.getStore().storeId + '&itin_id={$itin_id}' + '&grid_id=' + bt.up('grid').id + '&refresh_ext_id=' + bt.up('window').down('panel').id);
						    	return;
						    }	    
		       				
						    //avviso che sto geolocalizzando i punti non ancora geolocalizzati						    
						    //Ext.Msg.alert('Avvio geolocalizzazione', 'Non tutte le destinazioni sono state geolocalizzate. Avvio geolocalizzazione in corso');
						    
		       				addressCount = selected_id.length;
						      if (selected_id.length > 0) {
						        items = selected_id;
						
						        for (var i = 1; i <= items.length; i++) {          
								  get_geocode(items[i-1], i);          
						        }
						      }
						    
						      
							function get_geocode(record, counter){
								if (record['ha_coordinate'] == 'Y'){
									responseCount++;
									return;
								}	
								

						        geocoder.geocode( { 'address': record['gmap_ind'].trim()}, function(results, status) {
						
						        	responseCount++;	
						        	if (status == google.maps.GeocoderStatus.OK) {
						        	    var latitude = results[0].geometry.location.lat();
						        	    var longitude = results[0].geometry.location.lng();
						
						        	    record['counter'] = counter;
						        	    record['valido']  = 'Y';
										record['lat'] = latitude;
										record['lng'] = longitude;
										record['title'] = record['TDDCON'].trim();
										record['icon'] = {
												'url' : 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + counter + '|FF0000|0000FF',
												'size': new google.maps.Size(32, 32)
										};
						
										markers_data.push(record);
										        	    
						        	} else {						        		

						            	
										if (status == google.maps.GeocoderStatus.ZERO_RESULTS || 
											status == google.maps.GeocoderStatus.INVALID_REQUEST) {
							            	record['counter'] = counter;
							        	    record['valido']  = 'N';
							        	    markers_data.push(record);
							            	errors_data.push(record);										
										}						            							            	

						            }
						
									esecuzione_finale();
						        	
						        });			            		
							}

							function esecuzione_finale(){
								if (responseCount == addressCount){
									//ho finito tutte le richieste
						      		//map.addMarkers(markers_data);
						            //loadErrorResults();
						            
									Ext.Ajax.request({
									        url        : 'acs_op_exe.php?fn=progetta_spedizioni_memorizza_coordinate',
									        method     : 'POST',
									        waitMsg    : 'Data loading',
						        			jsonData: {
						        				selected_id: markers_data,
											},							        
									        success : function(result, request){
						            			bt.up('grid').getStore().load();									        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });							

						            
								}			 
							}							
							
	                    	
	                    	
	                    	
	                    	
	                    	
						}	                    				                			            
	                }";
 


	$code_percorso = ", {
		iconCls: 'icon-cartello-16',
		itemId: 'gmap_percorso',
		text: 'Percorso',
		disabled: false,
		scope: this,
		handler: function(bt){
		
                    my_storeId = bt.up('grid').store.storeId;
					if (my_storeId == 'ext-empty-store' || typeof(my_storeId)==='undefined') {
						acs_show_msg_error('Nessuna spedizione/carico selezionata');
						return;
					}	                    
	
		
		selected_id = Ext.pluck(bt.up('grid').getStore().data.items, 'data'),
		 
		//se hatto tutti le coordinate mostro la mappa
		manca_coordinate = 'N';
		for (var i = 0; i < selected_id.length; i++) {
			if ( selected_id[i]['ha_coordinate'] != 'Y')
				manca_coordinate = 'Y';
		}
		 
		if (manca_coordinate == 'N'){
			gmapPopup('gmap_percorso.php?cod_iti={$itin_id}&data={$data}&sped_car=' + bt.up('grid').getStore().storeId + '&grid_id=' + bt.up('grid').id);			
			return;
		}
		 
		//avviso che sto geolocalizzando i punti non ancora geolocalizzati
		Ext.Msg.alert('Attenzione.', 'Non tutte le destinazioni sono state geolocalizzate.');		
		}
	}";
	
	
	
	
	

	$code_km = ", {
		iconCls: 'icon-pneumatico-16',
		itemId: 'route_sped',
		text: 'Calcola Km',
		disabled: false,
		scope: this,
		handler: function(bt){
		
	                    my_storeId = bt.up('grid').store.storeId;
						if (my_storeId == 'ext-empty-store' || typeof(my_storeId)==='undefined') {
							acs_show_msg_error('Nessuna spedizione/carico selezionata');
							return;
						}	                    
		

        var start_point_coordinate = [];
        start_point_coordinate['LAT'] = {$start_point_coordinate['LAT']};
		start_point_coordinate['LNG'] = {$start_point_coordinate['LNG']};	
			
			
		var map;
		var item, items, markers_data = [], errors_data = [];
		var responseCount = 0, addressCount;
		var geocoder = new google.maps.Geocoder();
		var data;

		
		selected_id = Ext.pluck(bt.up('grid').getStore().data.items, 'data');
		selected_id = [];
		bt.up('grid').items.items[0].getStore().each(function(rec){selected_id.push(rec)}); 
	
		//se hatto tutti i km mostro il totale e lo aggiorno su sped
		manca_km = 'N';
		for (var i = 0; i < selected_id.length; i++) {
			if ( selected_id[i]['ha_km'] != 'Y')
			manca_km = 'Y';
		}

		if (manca_km == 'N'){
			alert('ho tutti i km!!!!!!!');
			return;
		}


		addressCount = selected_id.length;
		if (selected_id.length > 0) {
			items = selected_id;
			
			for (var i = 1; i <= items.length; i++) {
				get_km(items[i-1], items[i-2], i, items.length);
			}
		}


		
		function get_km(record, record_prec, counter, tot_scarichi){			
			//??????
			if (record.data['ha_km'] == 'Y'){
				responseCount++;
				return;
			}
			
			to = new google.maps.LatLng(record.data['LAT'], record.data['LNG']);
			
			if (counter == 1)
				from = new google.maps.LatLng(start_point_coordinate['LAT'], start_point_coordinate['LNG']);
			else
				from = new google.maps.LatLng(record_prec.data['LAT'], record_prec.data['LNG']);
			
			if (from.lat() != to.lat() || from.lng() != to.lng()){				
					var service = new google.maps.DistanceMatrixService();
					service.getDistanceMatrix(
					  {
					    origins: [from],
					    destinations: [to],
					    travelMode: google.maps.TravelMode.DRIVING,
					    unitSystem: google.maps.UnitSystem.METRIC,
					    durationInTraffic: true,
					    avoidHighways: false,
					    avoidTolls: false
					  }, function(response, status){
						responseCount++;
						record.data['response'] = response;
						record.data['status'] = status;
						record.data['from'] = from;
						record.data['to'] = to;
						record.set('km_n', response.rows[0].elements[0].distance.value);
						record.set('km_t', response.rows[0].elements[0].distance.text);
		
						if (counter == tot_scarichi){
							//devo aggiungere anche il ritordon in start_point
							var service = new google.maps.DistanceMatrixService();
							
								service.getDistanceMatrix(
								  {
								    origins: [new google.maps.LatLng(record.data['LAT'], record.data['LNG'])],
								    destinations: [new google.maps.LatLng(start_point_coordinate['LAT'], start_point_coordinate['LNG'])],
								    travelMode: google.maps.TravelMode.DRIVING,
								    unitSystem: google.maps.UnitSystem.METRIC,
								    durationInTraffic: true,
								    avoidHighways: false,
								    avoidTolls: false
								  }, function(response, status){
										record.set('km_n', record.get('km_n') + response.rows[0].elements[0].distance.value);						  
										record.set('km_t', record.get('km_t') + '<br>' + response.rows[0].elements[0].distance.text);
										markers_data.push(record);
										esecuzione_finale_km();
								  });					
							
						}
						else {						
							markers_data.push(record);
							esecuzione_finale_km();
						}
					  });
				} else {
					//from == to
						responseCount++;					
						record.set('km_n', 0);
						record.set('km_t', '0 km');					
						
						if (counter == tot_scarichi){
							//devo aggiungere anche il ritordon in start_point
							var service = new google.maps.DistanceMatrixService();
							
								service.getDistanceMatrix(
								  {
								    origins: [new google.maps.LatLng(record.data['LAT'], record.data['LNG'])],
								    destinations: [new google.maps.LatLng(start_point_coordinate['LAT'], start_point_coordinate['LNG'])],
								    travelMode: google.maps.TravelMode.DRIVING,
								    unitSystem: google.maps.UnitSystem.METRIC,
								    durationInTraffic: true,
								    avoidHighways: false,
								    avoidTolls: false
								  }, function(response, status){
										record.set('km_n', record.get('km_n') + response.rows[0].elements[0].distance.value);						  
										record.set('km_t', record.get('km_t') + '<br>' + response.rows[0].elements[0].distance.text);
										markers_data.push(record);
										esecuzione_finale_km();
								  });					
							
						}
						else {
							markers_data.push(record);
							esecuzione_finale_km();
						}						
						
						
				}
		}
		

		
		
		function esecuzione_finale_km(){
		  if (responseCount == addressCount){
			bt.up('grid').getView().refresh();

/*			
			Ext.Ajax.request({
				url        : 'acs_op_exe.php?fn=progetta_spedizioni_memorizza_km',
				method     : 'POST',
				waitMsg    : 'Data loading',
				jsonData: {
					selected_id: markers_data,
				},
				success : function(result, request){
					bt.up('grid').getStore().load();
				},
				failure    : function(result, request){
					Ext.Msg.alert('Message', 'No data to be loaded');
				}
			});
*/			
		  }						
		} //esecuzione finale


  }
}";






	
	
	
for ($i = 1; $i <= 2; $i++) {
	
	if ($i == 2) $light_class = "_rx";
		else $light_class = "";
	
	$ar_grids[] = "
	  {
			xtype: 'grid',
			cls: 'acs-light{$light_class}',
			title: ' - ', 
			loadMask: true,			

			features: [{
			        ftype: 'summary'
			    }],			
						
	        viewConfig: {
	        
	            plugins: {
	                ptype: 'gridviewdragdrop',
	                dragGroup: 'sped_grid',
	                dropGroup: 'sped_grid'
	            },
	            
	            
	            listeners: {
	            
	            	//verifico che lo spostamento sia valido
	            	beforedrop: function(node, data, dropRec, dropPosition) {
	            		if (this.store.storeId == 'ext-empty-store' || typeof(this.store.storeId)==='undefined') {
	            			acs_show_msg_error('Selezionare una spedizione/carico valida');	            		
	            			return false;
	            		} else
	            			return true;	            			
					},
	            
		  			itemcontextmenu : function(grid, rec, node, index, event) {
		  				  event.stopEvent();
					      var record = grid.getStore().getAt(index);		  
					      var voci_menu = [];
					      
					      voci_menu.push({
				      		text: 'Assegna destinazione cliente',
				    		iconCls : 'iconDestinazioneFinale',      		
				    		handler: function() {
				    			grid.up('#progetta-main-panel').show_win_destinazione_finale_da_progetta(grid, rec, node, index, event, 'PROGETTA');
				    		}
						  },{
				      		text: 'Sequenza ordini',   
				    		iconCls : 'icon-filter-16',      		
				    		handler : function() {
				    				//console.log(rec);
									acs_show_win_std('Ordini di ' + rec.get('TDDCON') + '- ' +rec.get('TDDLOC'), 'acs_sequenza_ordini.php', {rec_data: rec.data, data:  {$data}, itin_id: '{$itin_id}'}, 800, 400)
									}  //handler function()
						  });

					      var menu = new Ext.menu.Menu({
					            items: voci_menu
						}).showAt(event.xy);						  
					      
		  			},	            

			  			
		  			
		  			celldblclick: {
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
			  			rec = iView.getRecord(iRowEl);					  	
					  	
						if (col_name=='DEC_DEST'){
							iEvent.preventDefault();						
							gmapPopup('gmap.php?lat=' + rec.get('LAT') + '&lng=' + rec.get('LNG'));
							return false;						
						}			  			
			  			
			  			
			  			
						if (col_name == 'ha_coordinate'){
							iEvent.preventDefault();
							
							
		                	if ( iView.up('window').down('#panel_spedizioni').attendi_salvataggio == true ){
		                		acs_show_msg_error('Salvare o annullare le modifiche apportate prima di procedere');
		                		return;
		                	}							
							

							//form per gestire le coordinate
								var globe_win = new Ext.Window({
								  width: 450
								, height: 230
								, minWidth: 100
								, minHeight: 100
								, plain: true
								, title: 'Coordinate geografiche'
								, iconCls: 'icon-globe-16'			
								, layout: 'fit'
								, border: true
								, closable: true
								, items: [
									{
							            xtype: 'form',
							            bodyStyle: 'padding: 10px',
							            bodyPadding: '5 5 0',
							            frame: true,
							            title: '',
							            url: 'acs_op_exe.php',
							            
							            items: [{
						                	xtype: 'hidden',
						                	name: 'fn',
						                	value: 'upd_coordinate'
						                	}, {
						                	xtype: 'hidden',
						                	name: 'GLID',
						                	value: rec.get('GLID')
						                	}, {
											name: 'f_lat',
											xtype: 'textfield',
											fieldLabel: 'Latitudine',
										    value: rec.get('LAT'),
						                	allowBlank: false										    					
										}, {
											name: 'f_lng',
											xtype: 'textfield',
											fieldLabel: 'Longitudine',
										    value: rec.get('LNG'),
						                	allowBlank: false										    					
										}, {
											name: 'note_richiesta',
											xtype: 'textfield',
											fieldLabel: '',
										    value: rec.get('history_coordinate'),
						                	allowBlank: true,
						                	disabled: true,
						   					anchor: '-15'						                											    					
										}, {
											name: 'ind_richiesta',
											xtype: 'textfield',
											fieldLabel: '',
										    value: rec.get('ind_richiesta'),
						                	allowBlank: true,
						                	disabled: true,
						   					anchor: '-15'						                											    					
										}, {
							            xtype: 'fieldset',
							            flex: 1, width: '100%',
							            title: 'Check',
							            itemId: 'check_coo',
							            hidden: true,
							            items: [{
											name: 'f_check_lat',
											xtype: 'textfield',
											fieldLabel: 'Latitudine',
						                	allowBlank: true,
						                	disabled: true										    		
										}, {
											name: 'f_check_lng',
											xtype: 'textfield',
											fieldLabel: 'Longitudine',
						                	allowBlank: true,
						                	disabled: true										    					
										}]
							            }
					            ],
										
										buttons: [{
								            text: 'Sostituisci',
								            disabled: true,
								            itemId: 'check_sost',
								            handler: function() {
								            		var form = this.up('form').getForm();
								            		form.findField('f_lat').setValue(form.findField('f_check_lat').getValue());		
								            		form.findField('f_lng').setValue(form.findField('f_check_lng').getValue());
												}
											}
								            
								            , {
								            text: 'Check',
								            handler: function() {
								            	// RECUPERO LE COORDINATE DEL PUNTO								            
								            	var form = this.up('form').getForm();
								            	var form_view = this.up('form');
												ind_richiesta = form.findField('ind_richiesta').getValue();

												this.up('form').down('#check_coo').show();
												this.up('window').setHeight(300);
												
						        geocoder = new google.maps.Geocoder();								            	
						        geocoder.geocode( { 'address': ind_richiesta.trim()}, function(results, status) {
	
						        	if (status == google.maps.GeocoderStatus.OK) {
						        	    var latitude = results[0].geometry.location.lat();
						        	    var longitude = results[0].geometry.location.lng();
										
										form.findField('f_check_lat').setValue(latitude);						        	    
										form.findField('f_check_lng').setValue(longitude);
										
										lat_orig = parseFloat(form.findField('f_lat').getValue());
										lng_orig = parseFloat(form.findField('f_lng').getValue());
										lat_check = parseFloat(form.findField('f_check_lat').getValue());
										lng_check = parseFloat(form.findField('f_check_lng').getValue());										
										
										//se i valori sono diversi da quelli salvati abilito il bottone 'sostiuisci'
										if (lat_orig != lat_check || lng_orig != lng_check)
											form_view.down('#check_sost').enable();
										else
											form_view.down('#check_sost').disable();

										        	    
						        	} else {						        		
									            	
										if (status == google.maps.GeocoderStatus.ZERO_RESULTS || 
											status == google.maps.GeocoderStatus.INVALID_REQUEST) {
							            	record['counter'] = counter;
							        	    record['valido']  = 'N';
										}						            							            	

						            }
						        	
						        });								            	
								            	
								            	
								            	
								            	
								            	
								            	
								            	
								            	
								            	
								            	
								            	
								            	
								            	
								            	
											}										
										}, {
								            text: 'Salva',
								            handler: function() {
								            	var form = this.up('form').getForm();
							
												if(form.isValid()){
	                								form.submit({
	                								  success: function(form,action) {
	                								   globe_win.close();
	                								   iView.store.load();
													  }
													});												
												}
											}
										}]
										
							          }
								] 
								});
								globe_win.show();							
							
							
							return false;						
						}					  	
					  	
					  }
		  			
		  			},	            
	            
	            
	                drop: function(node, data, dropRec, dropPosition) {
	                
						//ricalcolo il volume
							
							var panel_spedizioni = this.panel.up('window').down('#panel_spedizioni');
							grid_elenco = this.panel.up('window').down('#grid_elenco');
							grid_lx = this.panel.up('window').down('#grid_lx');
							grid_rx = this.panel.up('window').down('#grid_rx');
							store_elenco = grid_elenco.getStore();
							
							panel_spedizioni.blocca_applica_sequenza = false;
							
							//aggiorno in base a grid sx							
							rec_lx = grid_elenco.getRootNode().findChild('k_sped_carico',grid_lx.items.items[0].store.storeId,true);
							 if (rec_lx != null){

									//sommo in base ai valori presenti nella grid									
									var rows = 	Ext.pluck(grid_lx.items.items[0].getStore().data.items, 'data');
									volu = peso = importo = 0;
							
	                    			Ext.each(rows, function(row) {
		                    			volu = volu + row.TDVOLU;
		                    			peso = peso + row.S_PESO;
		                    			importo = importo + row.S_IMPORTO;
		                    			
		                    			//se ho cambiato sped/carico .... blocco applica sequenza
		                    			if (row.k_sped_carico != grid_lx.items.items[0].store.storeId)
											panel_spedizioni.blocca_applica_sequenza = true;		                    			
	                    			});	
																					
									rec_lx.set('S_VOL', volu);
									rec_lx.set('S_PESO', peso);
									rec_lx.set('S_IMPORTO', importo);
									
								//aggiorno il padre (livello spedizioni)
									recP = rec_lx.parentNode;

									volu = peso = importo = 0;									
	                    			Ext.each(recP.childNodes, function(row) {
		                    			volu = volu + parseFloat(row.get('S_VOL'));
		                    			peso = peso + row.data.S_PESO;
		                    			importo = importo + row.data.S_IMPORTO;	                    			
	                    			});					
	                    						
									recP.set('S_VOL', 		volu);
									recP.set('S_PESO',		peso);
									recP.set('S_IMPORTO', 	importo);
							}
										
							
							//aggiorno in base a grid rx
							rec_rx = grid_elenco.getRootNode().findChild('k_sped_carico', grid_rx.items.items[0].store.storeId, true);

							if (rec_rx != null) {
									//sommo in base ai valori presenti nella grid
									
									var rows = 	Ext.pluck(grid_rx.items.items[0].getStore().data.items, 'data');
									volu = peso = importo = 0;
							
	                    			Ext.each(rows, function(row) {
		                    			volu = volu + row.TDVOLU;
		                    			peso = peso + row.S_PESO;
		                    			importo = importo + row.S_IMPORTO;
		                    			
		                    			//se ho cambiato sped/carico .... blocco applica sequenza
		                    			if (row.k_sped_carico != grid_rx.items.items[0].store.storeId)
											panel_spedizioni.blocca_applica_sequenza = true;		                    			
	                    			});							
															
									rec_rx.set('S_VOL', volu);
									rec_rx.set('S_PESO', peso);
									rec_rx.set('S_IMPORTO', importo);
									
								//aggiorno il padre (livello spedizioni)
									recP = rec_rx.parentNode;

									volu = peso = importo = 0;									
	                    			Ext.each(recP.childNodes, function(row) {
		                    			volu = volu + row.data.S_VOL;
		                    			peso = peso + row.data.S_PESO;
		                    			importo = importo + row.data.S_IMPORTO;	                    			
	                    			});								
									recP.set('S_VOL', 		volu);
									recP.set('S_PESO',		peso);
									recP.set('S_IMPORTO', 	importo);								
								}	
									
								grid_lx.items.items[0].getView().refresh();
								grid_rx.items.items[0].getView().refresh();									

								//totali										
								// (lx)
								if (rec_lx != null) {
									recP = rec_lx.parentNode;
									m_text = '[';							
									m_text += 'Volume: ' + floatRenderer4(recP.get('S_VOL'))  ;
									m_text += ' - Peso: ' + floatRenderer0(recP.get('S_PESO')) + ']';
									
									grid_lx.down('#text_totali').setText(m_text);
									
									grid_lx.down('#text_totali').removeCls('acs_warning');
									if ( (parseFloat(recP.get('S_VOL')) > parseFloat(recP.get('CSVOLD')) && parseFloat(recP.get('CSVOLD')) > 0) || 
										 (parseFloat(recP.get('S_PESO')) > parseFloat(recP.get('CSPESO')) && parseFloat(recP.get('CSPESO')) > 0) )
										grid_lx.down('#text_totali').addCls('acs_warning');									
									
								}		

								// (rx)
								if (rec_rx != null) {
									recP = rec_rx.parentNode;							
									m_text = '[';							
									m_text += 'Volume: ' + floatRenderer4(recP.get('S_VOL'))  ;
									m_text += ' - Peso: ' + floatRenderer0(recP.get('S_PESO')) + ']';
									
									
									grid_rx.down('#text_totali').setText(m_text);
									
									grid_rx.down('#text_totali').removeCls('acs_warning');
									if ( (parseFloat(recP.get('S_VOL')) > parseFloat(recP.get('CSVOLD')) && parseFloat(recP.get('CSVOLD')) > 0) || 
										 (parseFloat(recP.get('S_PESO')) > parseFloat(recP.get('CSPESO')) && parseFloat(recP.get('CSPESO')) > 0) )
										grid_rx.down('#text_totali').addCls('acs_warning');										
								}	
									
									

						//evidezio se ci sono dei clienti in entrambe le spedizioni
								//sommo in base ai valori presenti nella grid
								if (rec_lx != null && rec_rx != null) {									
									rows_lx = 	Ext.pluck(grid_lx.items.items[0].getStore().data.items, 'data');
									rows_rx = 	Ext.pluck(grid_rx.items.items[0].getStore().data.items, 'data');
									
									//pulisco
                    				grid_lx.items.items[0].getStore().each(function(rec){rec.set('m_cls', '');});									
                    				grid_rx.items.items[0].getStore().each(function(rec){rec.set('m_cls', '');});                    				

									grid_lx.items.items[0].getStore().each(function(rec) {
										
										mr = grid_rx.items.items[0].getStore().findRecord('k_cli_des', rec.get('k_cli_des'));

										if (mr != null) {
											rec.set('m_cls', 'tpSfondoRosa');
											mr.set('m_cls',  'tpSfondoRosa');
										}									
									
									});
								}	
								
						
						//attivo il botton di conferma
						this.panel.up('window').down('#panel_spedizioni').attendi_salvataggio = true;						
						this.panel.up('window').down('#panel_spedizioni').down('#conferma_spedizioni').enable();
						this.panel.up('window').down('#panel_spedizioni').down('#ripristina_modifica_spedizioni').enable();						
							
						if (this.panel.up('window').down('#panel_spedizioni').blocca_applica_sequenza == true){
								grid_lx.down('#applica_sequenza').disable();
								grid_rx.down('#applica_sequenza').disable();
								
								grid_lx.down('#assegna_carico').disable();
								grid_rx.down('#assegna_carico').disable();
								
						} else {
								grid_lx.down('#applica_sequenza').enable();
								grid_rx.down('#applica_sequenza').enable();

								grid_lx.down('#assegna_carico').enable();
								grid_rx.down('#assegna_carico').enable();								
						}
						
	                }
	            }
	        },			
	        

			columns: [
				{header: 'S', dataIndex: 'TDSECA', width: 25},			
				{header: 'Denominazione', dataIndex: 'TDDCON', flex: 1,
						renderer: function (value, metaData, record, row, col, store, gridView){
						
							if (parseInt(record.get('ha_anomalia_sped_car')) == 1)
								metaData.tdCls += ' tpSfondoGrigio';
						
    						metaData.tdCls += ' ' + record.get('m_cls');
    						return value;	
    					}								 
				},				
				{header: 'Localit&agrave;', dataIndex: 'DEC_DEST', flex: 1},
				{header: '', dataIndex: 'ha_coordinate',  width: 23,
						renderer: function (value, metaData, rec, row, col, store, gridView){
	    					if (rec.get('ha_coordinate') == 'Y')
								return '<img src=" .  img_path("icone/48x48/globe.png")  . " width=15>';    							
    						
							if (rec.get('ha_coordinate') == 'N')
								return '<img src=" .  img_path("icone/48x48/sub_red_delete.png")  . " width=15>';								
    						return '';	
						
    					}								 
				},				
				{header: 'CAP', dataIndex: 'TDDCAP', width: 50},
				{header: 'Pv', dataIndex: 'TDPROD', width: 30},
				{header: 'Vol.', dataIndex: 'TDVOLU', width: 60, renderer: floatRenderer2, align: 'right', summaryType: 'sum', 
					summaryRenderer: function(value, summaryData, dataIndex) {
					            return floatRenderer2(value); 
					        }				
				},
				{header: 'Vol. Pr.', dataIndex: 'TDVOLU', width: 50, align: 'right', renderer: function(v, a, b, c, d, store, e, f){
										
					if (c==0)
						store.tmp_value = 0;
					store.tmp_value += parseFloat(v);
					return floatRenderer2(store.tmp_value);
										
				}},										
				{header: 'Peso', dataIndex: 'S_PESO', width: 60, renderer: floatRenderer0, align: 'right', summaryType: 'sum',
					summaryRenderer: function(value, summaryData, dataIndex) {
					            return floatRenderer0(value); 
					        }				
				}, 				
				{header: 'Km', dataIndex: 'km_n', width: 60, summaryType: 'sum', align: 'right',
					summaryRenderer: function(value, summaryData, dataIndex) {
					            return floatRenderer0(value / 1000); 
					        }, 
						renderer: function (value, metaData, rec, row, col, store, gridView){
    						return rec.get('km_t');	
						
    					}					        				
				}				
			],

			dockedItems: [{
	            dock: 'top',
	            xtype: 'toolbar',
	            items: [{
	                    iconCls: 'iconCarico',
	                    itemId: 'assegna_carico',
	                    text: 'Nr Carico',
	                    disabled: false,
	                    scope: this,
	                    handler: function(bt){
	                    
	                    my_storeId = bt.up('grid').store.storeId;
						if (my_storeId == 'ext-empty-store' || typeof(my_storeId)==='undefined') {
							acs_show_msg_error('Nessuna spedizione/carico selezionata');
							return;
						}	                    
	                    

             " . (!isset($mod_js_parameters->ASS_CAR_bl) || $mod_js_parameters->ASS_CAR_bl != 'Y' ? "						
						//verifico che non ci siano clienti o ordini bloccati
						ar_dest = Ext.pluck(bt.up('grid').getStore().data.items, 'data');
						for (var i = 0; i < ar_dest.length; i++) {          
						  if ( parseInt(ar_dest[i]['T_CLI_BLOC']) > 0 || parseInt(ar_dest[i]['T_ORD_BLOC']) > 0 ){
							acs_show_msg_error('Operazione non ammessa con clienti o ordini bloccati');
							return;
						  }	         
						}" : "") . "
						
																		
	                    
	                   	my_listeners = {
        					beforeClose: function(){	
        						//dopo che ha chiuso la maschera del carico aggiorno la pagina di composizione
        						grid_elenco = bt.up('window').down('#grid_elenco');
								grid_lx = bt.up('window').down('#grid_lx').items.items[0];
								grid_rx = bt.up('window').down('#grid_rx').items.items[0];
								
        						grid_elenco.getStore().load();
								grid_lx.reconfigure( new Ext.data.ArrayStore({} ) );
								grid_rx.reconfigure( new Ext.data.ArrayStore({} ) );
        									            
				        		}
		    				};		
	                    
		    				//verifico se ha gia' il carico
		    				m_carico = bt.up('grid').getStore().storeId.split('|')[1];
		    				if (parseFloat(m_carico.split('_')[2]) > 0){
								acs_show_msg_error('Carico gi&agrave; assegnato');								
	                    		return false;		    				 
		    				}

		    				
	                    	list_selected_id = [];
	                    	list_selected_id.push(bt.up('grid').getStore().storeId);
							acs_show_win_std('Assegna nuovo numero carico', 'acs_form_json_assegna_carico.php', {
								data: {$data}, 
								by_sped_car: 'Y', 
								selected_id: list_selected_id,
								storeId_rx: bt.up('window').down('#grid_rx').items.items[0].getStore().storeId
							}, 600, 450, my_listeners, 'iconCarico')	                    
						}
	                   }
	                    
	                    
	                 , {
	                    iconCls: 'icon-save',
	                    itemId: 'applica_sequenza',
	                    text: 'Applica seq.',
	                    disabled: false,
	                    scope: this,
	                    handler: function(bt){	                    	
	                    	
		                    my_storeId = bt.up('grid').store.storeId;
							if (my_storeId == 'ext-empty-store' || typeof(my_storeId)==='undefined') {
								acs_show_msg_error('Nessuna spedizione/carico selezionata');
								return;
							}	                    
	                    
	                    
	                    	//senza carico non si puo' applicare la sequenza
	                    	sped_car_id_exp = bt.up('grid').getStore().storeId.split('|');
	                    	carico_exp = sped_car_id_exp[1].split('_');
	                    	if (parseFloat(carico_exp[2]) == 0){
	                    		acs_show_msg_error('Prima di applicare la sequenza e\' necessario associare un carico');
	                    		return false;	                    	
	                    	}

	                    
	                    
							Ext.Ajax.request({
							        url        : 'acs_op_exe.php?fn=progetta_spedizioni_applica_sequenza',
							        method     : 'POST',
							        waitMsg    : 'Data loading',
				        			jsonData: {
				        				selected_id: Ext.pluck(bt.up('grid').getStore().data.items, 'data'),
				        				cod_iti: '{$itin_id}', data: {$data},
				        				sped_car_id: bt.up('grid').getStore().storeId
									},							        
							        success : function(result, request){
							        		bt.up('grid').getStore().load();
							        		
							        		//adesso aggiorno sempre il day in chiusura di progettazione
											// aggiorno il day di partenza
											// dw = Ext.getCmp('{$tree_id}');
											// dw.getStore().treeStore.load();
							        		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });							
							
			                
			            }
	                }" . $code_gmap . $code_percorso . $code_km  . ", {
						xtype: 'label',
						itemId: 'text_totali',
						text: '',
		                fieldLabel: 'Totali',
		                flex: 1
		            }, ";
	
			if($i == 2 ){
				$ar_grids[] .= " {
				                    iconCls: 'icon-blog_add-16',
				                    itemId: 'unisci_sx',
				                    text: 'Unisci',
									tooltip: 'Unisci carico a spedizione di sinistra',
				                    disabled: false,
				                    scope: this,
				                    handler: function(bt){
									  ///xxx
									  
									   var storeId_lx = bt.up('window').down('#grid_lx').items.items[0].getStore().storeId;
									   var storeId_rx = bt.up('window').down('#grid_rx').items.items[0].getStore().storeId;
						
										if (storeId_rx == 'ext-empty-store' || typeof(storeId_rx)==='undefined') {
											acs_show_msg_error('Nessuna spedizione/carico selezionata');								
											return;
										}
										if (storeId_lx == 'ext-empty-store' || typeof(storeId_lx)==='undefined') {
											acs_show_msg_error('Selezionare anche spedizione/carico a destra a cui abbinare il carico attuale');								
											return;
										}						
						
						
									   Ext.Msg.confirm('Richiesta conferma', 'Confermi operazione?', function(btn, text){																							    
									   if (btn == 'yes'){																	         	
							         	
							         	Ext.getBody().mask('Loading...', 'loading').show();
										Ext.Ajax.request({
										   url        : '" . $_SERVER['PHP_SELF'] . "?fn=unisci_carico',
										   method: 'POST',
										   jsonData: {
										   	sped_car_from:  storeId_rx,
										    sped_car_to:	storeId_lx
										   }, 
										   
										   success: function(response, opts) {
										   		Ext.getBody().unmask();
				        						var grid_elenco = bt.up('window').down('#grid_elenco');
												var grid_lx = bt.up('window').down('#grid_lx').items.items[0];
												var grid_rx = bt.up('window').down('#grid_rx').items.items[0];
												
				        						grid_elenco.getStore().load();
												grid_lx.reconfigure( new Ext.data.ArrayStore({} ) );
												grid_rx.reconfigure( new Ext.data.ArrayStore({} ) );														   		
										   }, 
										   failure: function(response, opts) {
										      Ext.getBody().unmask();
										      alert('error in get image');
										   }
										});						         	
							         	
										}
									   });										
									} //handler
								}";
				
				}
				
				$ar_grids[] .= " ]
						}]
			
						
						
					}				
				";    
}


?>




{"success":true, "items": [
	{
		xtype: 'panel',
		id: 'progetta-main-panel',
		
		itin_id: '<?php echo $itin_id; ?>',
		data: <?php echo $data; ?>,
		
		layout: {
			type: 'border',
			align: 'stretch',
			pack: 'start'
		},
		defaults: {
			collapsible: true,
    		split: true
		},
		
		
		verifica_on_close: function(){
		//	alert('aaaaaaaaaaaa');
			return true;
		},
		
		
		acs_refresh_all: function(){
			grid_elenco = this.down('#grid_elenco');
			grid_lx = this.down('#grid_lx').items.items[0];
			grid_rx = this.down('#grid_rx').items.items[0];

			grid_elenco.getStore().load();
			grid_lx.getStore().load();
			grid_rx.getStore().load();
		},		
		
		show_win_destinazione_finale_da_progetta: function(grid, rec, node, index, event, tipo){
		
  			list_selected_id = [];
  			list_selected_id.push(rec.data);
		
			// create and show window
			print_w = new Ext.Window({
			  width: 800
			, height: 300
			, minWidth: 300
			, minHeight: 300
			, plain: true
			, title: 'Assegna destinazione cliente'
	    	, iconCls: 'iconDestinazioneFinale'		
			, layout: 'fit'
			, border: true
			, closable: true  
			});
			print_w.show();
	
			//carico la form dal json ricevuto da php
			Ext.Ajax.request({
			        url        : 'acs_form_json_assegna_destinazione_finale.php',
			        method     : 'POST',
			        waitMsg    : 'Data loading',
			        jsonData: {list_selected_id: list_selected_id, grid_id: grid.id, tipo: tipo}, 
			        success : function(result, request){
			            var jsonData = Ext.decode(result.responseText);
			            print_w.add(jsonData.items);
			            print_w.doLayout();				            
			        },
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			});		
		},
		
		
		listeners: {
		
 			afterrender: function (comp) { 			
 				comp.up('window').setTitle(<?php echo j("Composizione spedizioni del ".print_date($data). " - Itinerario: {$itin_descr}"); ?>);
 				
 				m_grids = comp.initialConfig.ar_grids;	                   		
		                   		
		        m_grid_panel_lx = comp.down('#grid_lx');
		        m_grid_panel_rx = comp.down('#grid_rx');
		                   										
		        m_grid_panel_lx.add(m_grids[0]);		                   		
		        m_grid_panel_rx.add(m_grids[1]);

		        Ext.each(comp.initialConfig.ar_stores,  function(store) {  
								        s = Ext.create('Ext.data.Store', store);
								        // s.load();
								    });
								    
				//carico la spedizione/carico selezionata di partenza
				//TODO
		                   		
                m_grid = m_grid_panel_lx.items.items[0];
 				
 			}
 		
           },
	    
		ar_grids: [
			<?php echo implode(",", $ar_grids) ?>		
		],
	    
		ar_stores: [
			<?php echo implode(",", $ar_stores) ?>		
		],
	    
		
		items: [
			    
		  	{
				region: 'north',
				flex: 2,		  	
							
							
				tools:[{
				    id:'help',
				    float: 'left',
				    qtip: 'Riepilogo scarichi con anomalie',
				    hidden: true, //ADESSO E' NEI BOTTONI IN BASSO
				    handler: function(event, toolEl, panel){
				        // refresh logic
				        acs_show_win_std('Scarichi ridondanti', 'acs_json_scarichi_ridondanti.php', {data: <?php echo $data ?>, itin_id: <?php echo j($itin_id); ?>, aspe: <?php echo j(trim($itin_obj->rec_data['TAASPE'])); ?>}, 800, 450);
				    }
				}],   
											
							
				xtype: 'treepanel',
				id: 'grid_elenco',
				selType: 'cellmodel',
				cls: 'tree-calendario',		
				title: 'Elenco spedizioni/carichi',
		        collapsible: true,
		        useArrows: true,
		        rootVisible: false,
		        multiSelect: false,
		        singleExpand: false,
		        loadMask: true,
		        
		        viewConfig: {
					getRowClass: function(record, index, rowParams, store) {
							  if (record.get('liv') == 'liv_1') return 'liv_2';          
				        },		        
		        },								
						        
				store: new Ext.create('Ext.data.TreeStore', {
						autoLoad: true,
				        fields: ['liv', 'selected_grid', 'TDTPCA', 'TDAACA', 'TDNRCA', 'task', 'CSPROG', 'k_sped_carico', 'carico', 'vmc', 'CSTISP', 'CSTITR', {name: 'S_IMPORTO', type: 'float'}, {name: 'S_VOL', type: 'float'}, {name: 'S_PESO', type: 'float'}, {name: 'CSVOLD', type: 'float'}, {name: 'CSPESO', type: 'float'}],
				        proxy: {
				            type: 'ajax',
				            url: '<?php  echo $_SERVER['REQUEST_URI'] ?>',
				            
							extraParams: {
								fn: 'grid_elenco',
								data: <?php echo $data ?>,
								itin_id: '<?php echo $itin_id ?>',
								sped_ar: '<?php echo $sped_ar ?>'
							},				            

					        reader: {
	                      	  root: 'children'
	                    	},
							
				        },
				        
				        folderSort: false,
				        
						listeners: {
						            load: function () {
						                //per ogni spedizione/carico devo verificare che ci sia lo store relativo
						                rootNode = this.getRootNode();						                
						                
						                results = new Array();
						                rootNode.eachChild(function(rec_liv0){
						                	rec_liv0.eachChild(function(rec_liv1){
						                	
						                		a = Ext.data.StoreManager.lookup( rec_liv1.get('k_sped_carico') );
						                		
						                		if (a == null){
						                	
									               a = Ext.create('Ext.data.Store', {
														storeId: rec_liv1.get('k_sped_carico'),
															
														autoLoad: false,
												
														proxy: {
															url: '<?php echo $_SERVER['REQUEST_URI']; ?>',
															method: 'POST',
															type: 'ajax',
															
															//Add these two properties
															actionMethods: {
																read: 'POST'					
															},
												
															reader: {
																type: 'json',
																method: 'POST',
																root: 'root'					
															},
														extraParams: {
															fn: 'grid_data',
															data: <?php echo $data; ?>,
															sped_id: rec_liv1.get('CSPROG'),
															tpca: rec_liv1.get('TDTPCA'),
															aaca: rec_liv1.get('TDAACA'),
															nrca: rec_liv1.get('TDNRCA'),
															itin_id: '<?php echo $itin_id ?>'
															}				
														},
															
														fields: ['k_sped_carico', 'k_cli_des', 'm_cls', 'TDSECA', 'TDCCON', 'TDCDES', 'TDDCON', {name: 'TDVOLU', type: 'float'}, {name: 'S_PESO', type: 'float'}, {name: 'S_IMPORTO', type: 'float'}, 'TDDLOC', 'DEC_DEST', 'TDDCAP', 'TDIDES', 'TDPROD', 'VETTORE_ID', 'TDDT', 'LAT', 'LNG', 'ha_coordinate', 'history_coordinate', 'gmap_ind', 'GLID', 'km_n', 'km_t', 'ind_richiesta', 'ha_anomalia_sped_car', 'T_CLI_BLOC', 'T_ORD_BLOC']
																	
													});						                								                	
						                		} //se devo crearlo

						                		a.load();						                		
						                		
						                	});
						                });
						                
						                
						                
						            }
						         }				        
				        
				        
				    }),		//store
				
				columns: [
					{
			            xtype:'actioncolumn',
			            header: 'Sx-Dx', 
			            width:50,
			            
						renderer: function (value, metaData, record, row, col, store, gridView){
    						if (record.get('selected_grid') == 'selected_lx') metaData.tdCls += ' selected_lx';
    						if (record.get('selected_grid') == 'selected_rx') metaData.tdCls += ' selected_rx';    						
    						return value;	
    					},			            
			            
			            items: [{
			                icon: <?php echo img_path("icone/16x16/sub_yellow_prev.png") ?>,
			                
							getClass: function(value, meta, record) {
										if (record.get('liv') != 'liv_1' || record.get('selected_grid') != '')
							                return 'x-hide-visibility';
							        },			                
			                
			                tooltip: 'Visualizza a sinistra',
			                handler: function(grid, rowIndex, colIndex) {
			                
			                	if ( grid.up('window').down('#panel_spedizioni').attendi_salvataggio == true ){
			                		acs_show_msg_error('Salvare o annullare le modifiche apportate prima di procedere');
			                		return;
			                	}

			                    var rec = grid.getStore().getAt(rowIndex);
		                   		recP = rec.parentNode;
			                	grid_elenco = grid.up('window').down('#grid_elenco');			                	
								old_rec_selected = grid_elenco.getRootNode().findChild('selected_grid', 'selected_lx' ,true);
								if (old_rec_selected != null) old_rec_selected.set('selected_grid', '');			                    
			                    rec.set('selected_grid', 'selected_lx');
			                    
		                   		m_panel = grid.up('#progetta-main-panel');
		                   		m_grids = m_panel.initialConfig.ar_grids;	                   		
		                   		
		                   		m_grid_panel_lx = m_panel.down('#grid_lx');
								
		                   		
		                   		m_grid = m_grid_panel_lx.items.items[0];
		                   				                   		
		                   		m_store = Ext.data.StoreManager.lookup( rec.get('k_sped_carico') );
		                   		m_grid.reconfigure( m_store );
		                   		
		                   		//imposto il titolo
		                   		m_titolo = '<img src=<?php echo img_path("icone/16x16/spedizione.png") ?> width=18> Spedizione #' + recP.get('CSPROG') + ' - ' + recP.get('CSTISP') + ' - ' + recP.get('CSTITR');
		                   		m_titolo = m_titolo + ', ' + rec.get('carico');
		                   		m_titolo = m_titolo + ', ' + rec.get('vmc');
		                   		m_grid.setTitle(m_titolo);		           
		                   		
		                   		m_grid_panel_lx.down('#text_totali').setText('');
		                   		
		                   		
								//evidezio se ci sono dei clienti in entrambe le spedizioni
									grid_lx = m_panel.down('#grid_lx');		                   		
									grid_rx = m_panel.down('#grid_rx');
									
									//pulisco
                    				grid_lx.items.items[0].getStore().each(function(rec){rec.set('m_cls', '');});									
                    				grid_rx.items.items[0].getStore().each(function(rec){rec.set('m_cls', '');});                    				

									grid_lx.items.items[0].getStore().each(function(rec) {
										mr = grid_rx.items.items[0].getStore().findRecord('k_cli_des', rec.get('k_cli_des'));

										if (mr != null) {
											rec.set('m_cls', 'tpSfondoRosa');
											mr.set('m_cls',  'tpSfondoRosa');
										}									
									
									});
		                   		
		                   		         			                    
			                }
			            },{
			                icon: <?php echo img_path("icone/16x16/sub_green_next.png") ?>,			                
			                
							getClass: function(value, meta, record) {
										if (record.get('liv') != 'liv_1' || record.get('selected_grid') != '')
							                return 'x-hide-visibility';
							        },			                
			                
			                tooltip: 'Visualizza a destra',
			                handler: function(grid, rowIndex, colIndex) {
			                
			                	if ( grid.up('window').down('#panel_spedizioni').attendi_salvataggio == true ){
			                		acs_show_msg_error('Salvare a annullare le modifiche apportate');
			                		return;
			                	}			                
			                
			                    var rec = grid.getStore().getAt(rowIndex);
			                    recP = rec.parentNode;
			                	grid_elenco = grid.up('window').down('#grid_elenco');			                	
								old_rec_selected = grid_elenco.getRootNode().findChild('selected_grid', 'selected_rx' ,true);
								if (old_rec_selected != null) old_rec_selected.set('selected_grid', '');			                    
			                    rec.set('selected_grid', 'selected_rx');			                    
				                    
		                   		m_panel = grid.up('#progetta-main-panel');
		                   		m_grids = m_panel.initialConfig.ar_grids;	                   		
		                   		
		                   		m_grid_panel_rx = m_panel.down('#grid_rx');
		                   		
		                   		m_grid = m_grid_panel_rx.items.items[0];
		                   		
		                   		m_store = Ext.data.StoreManager.lookup( rec.get('k_sped_carico') );
		                   		m_grid.reconfigure( m_store );
		                   				                    
		                   		//imposto il titolo
		                   		m_titolo = '<img src=<?php echo img_path("icone/16x16/spedizione.png") ?> width=18> Spedizione #' + recP.get('CSPROG') + ' - ' + recP.get('CSTISP') + ' - ' + recP.get('CSTITR');
		                   		m_titolo = m_titolo + ', ' + rec.get('carico');
		                   		m_titolo = m_titolo + ', ' + rec.get('vmc');
		                   		m_grid.setTitle(m_titolo);		                    
		                   		
		                   		m_grid_panel_rx.down('#text_totali').setText('');
		                   		
								//evidezio se ci sono dei clienti in entrambe le spedizioni
									grid_lx = m_panel.down('#grid_lx');		                   		
									grid_rx = m_panel.down('#grid_rx');
									
									//pulisco
                    				grid_lx.items.items[0].getStore().each(function(rec){rec.set('m_cls', '');});									
                    				grid_rx.items.items[0].getStore().each(function(rec){rec.set('m_cls', '');});                    				

									grid_lx.items.items[0].getStore().each(function(rec) {
										mr = grid_rx.items.items[0].getStore().findRecord('k_cli_des', rec.get('k_cli_des'));

										if (mr != null) {
											rec.set('m_cls', 'tpSfondoRosa');
											mr.set('m_cls',  'tpSfondoRosa');
										}									
									
									});
	
		                   		
			                }                
			            }]
			        },		
				
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'task', 
			            width: 200,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            text: 'Spedizione'			            
			        },
			        
					{header: 'Carico', dataIndex: 'carico', flex: 1},			        
			        
				
					{header: 'Vettore - Mezzo', dataIndex: 'vmc', flex: 2},					
					{header: 'Vol.', dataIndex: 'S_VOL', width: 80, align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){							
							if (record.get('liv') == 'liv_0') metaData.tdCls += ' grassetto';						
    						if (parseFloat(record.get('S_VOL')) >  parseFloat(record.get('CSVOLD')) && parseFloat(record.get('CSVOLD')) > 0)
    							metaData.tdCls += ' tpSfondoRosa';
    						return floatRenderer4(value);	
    					}							
					},
					{header: 'Max', dataIndex: 'CSVOLD', width: 50, align: 'right', renderer: floatRenderer2 },
					{header: 'Peso', dataIndex: 'S_PESO', width: 50, align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
							if (record.get('liv') == 'liv_0') metaData.tdCls += ' grassetto';												
    						if (parseFloat(record.get('S_PESO')) >  parseFloat(record.get('CSPESO')) && parseFloat(record.get('CSPESO')) > 0)
    							metaData.tdCls += ' td_bg_red_01';
    						return floatRenderer0(value);	
    					}					
					},
					{header: 'Max', dataIndex: 'CSPESO', width: 50, align: 'right', renderer: floatRenderer0 },					
					{header: 'Importo', dataIndex: 'S_IMPORTO', width: 80, align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
							if (record.get('liv') == 'liv_0') metaData.tdCls += ' grassetto';												
    						return floatRenderer2(value);	
    					}					
					}					
														
				],
				
				listeners: {
				
					
			itemcontextmenu : function(grid, rec, node, index, event) {	
				event.stopEvent();
				var voci_menu = [];
				
				if(rec.get('liv') == 'liv_0'){
				voci_menu.push({
	          		text: 'Modifica spedizione',
	        		iconCls : 'iconSpedizione',	          		
	        		handler: function() {
	        			my_listeners = {
	        					acsaftersave: function(){
	        							grid.getStore().load();	
					        		}
			    				};												
											
	    			  	acs_show_win_std('Modifica spedizione', 'acs_form_json_spedizione.php', 
	    			  		{		
	    			  		 sped_id: rec.get('CSPROG'),						    			  		 
	    			  		 from_sped_id: 'Y'
	    			  		}, 600, 550, my_listeners, 'iconSpedizione');
	        		}
	    		});
				}
				  
			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				       }).showAt(event.xy);	
								
							
					
					}
				
				}
	
				
			},			    
			    
			    
		    
		    
		    {
		    	region: 'center',
		    	id: 'panel_spedizioni',
		    	attendi_salvataggio: false,
		    	blocca_applica_sequenza: false,		    	
				xtype: 'panel',
				flex: 3,
				layout: {
					type: 'hbox',
					align: 'stretch',
					pack: 'start'
				},
				
				
				dockedItems: [{
			            dock: 'bottom',
			            xtype: 'toolbar',
			            items: [
							{
			                    iconCls: 'icon-save',
			                    itemId: 'conferma_spedizioni',
			                    text: 'CONFERMA SPEDIZIONI CORRENTI',
			                    disabled: true,
			                    scope: this,
			                    handler: function(bt){

			                    	bt.disable();
			                    	Ext.getBody().mask('Loading... ', 'loading').show();
			                    
									grid_elenco = bt.up('window').down('#grid_elenco');
									grid_lx = bt.up('window').down('#grid_lx').items.items[0];
									grid_rx = bt.up('window').down('#grid_rx').items.items[0];			                   			          
			                   			          
									Ext.Ajax.request({
									        url        : 'acs_op_exe.php?fn=progetta_spedizioni',
									        method     : 'POST',
									        waitMsg    : 'Data loading',
						        			jsonData: {
						        				selected_id: [Ext.pluck(grid_lx.getStore().data.items, 'data'),
															  Ext.pluck(grid_rx.getStore().data.items, 'data')],
												sped_car_id: [grid_lx.getStore().storeId,
															  grid_rx.getStore().storeId],			  						        				
						        				cod_iti: '<?php echo $itin_id; ?>', 
						        				data: <?php echo $data; ?>
											},							        
									        success : function(result, request){
												grid_elenco.getStore().load();
						                    	grid_lx.getStore().load();
						                    	grid_rx.getStore().load();
						                    	
						                    	bt.up('window').down('#panel_spedizioni').attendi_salvataggio = false;
												bt.up('window').down('#panel_spedizioni').down('#conferma_spedizioni').disable();			                    
												bt.up('window').down('#panel_spedizioni').down('#ripristina_modifica_spedizioni').disable();
												
												bt.enable();
												Ext.getBody().unmask();
												
												//aggiorno il day di partenza
												dw = Ext.getCmp('<?php echo $tree_id; ?>');
												dw.getStore().treeStore.load();
												
												grid_lx.down('#applica_sequenza').enable();
												grid_rx.down('#applica_sequenza').enable();
												
												grid_lx.down('#assegna_carico').enable();
												grid_rx.down('#assegna_carico').enable();
																								
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });			                   			          
									
			                   			                   		
			                    } 
			                    
			                 }, {
			                    iconCls: 'icon-reset',
			                    itemId: 'ripristina_modifica_spedizioni',
			                    text: 'Ripristina spedizioni correnti',
			                    disabled: true,
			                    scope: this,
			                    handler: function(bt){

									grid_elenco = bt.up('window').down('#grid_elenco');
									grid_lx = bt.up('window').down('#grid_lx').items.items[0];
									grid_rx = bt.up('window').down('#grid_rx').items.items[0];
									
									
									//reset di grid_lx e grid_rx
										grid_lx.reconfigure( new Ext.data.ArrayStore({} ) );
										grid_rx.reconfigure( new Ext.data.ArrayStore({} ) );
										
										grid_lx.down('#text_totali').setText('');												   
										grid_rx.down('#text_totali').setText('');									
										
										grid_lx.setTitle('');												   
										grid_rx.setTitle('');									
									
									grid_elenco.getStore().load();
									
			                    	//grid_lx.getStore().load();
			                    	//grid_rx.getStore().load();
			                    	
			                    	bt.up('window').down('#panel_spedizioni').attendi_salvataggio = false;
									bt.up('window').down('#panel_spedizioni').down('#conferma_spedizioni').disable();			                    
									bt.up('window').down('#panel_spedizioni').down('#ripristina_modifica_spedizioni').disable();
												   
									
									
																					                   		
			                    } 
			                    
			                 }
			                 
			                 
<?php 
	//in base al profilo
	$js_parameters = $auth->get_module_parameters($s->get_cod_mod());
	if ($js_parameters->gest_SPED != 0){
?>			                 
			                 
			                 
			                 , {
			                    iconCls: 'iconSpedizione',
			                    itemId: 'crea_nuova_spedizione',
			                    text: 'Crea nuova spedizione',
			                    disabled: false,
			                    scope: this,
			                    handler: function(bt){
			                    
			                    		pmp = bt.up('window').down('#progetta-main-panel');
			                    		grid_elenco_tree = bt.up('window').down('#grid_elenco');
			                    
											// create and show window
										print_w = new Ext.Window({
										  width: 600
										, height: 550
										, minWidth: 300
										, minHeight: 300
										, plain: true
										, title: 'Creazione nuova spedizione'
										, layout: 'fit'
										, border: true
										, closable: true
										//, items: formPanel					
										});
										print_w.show();
						                	
						                
						                	
										//carico la form dal json ricevuto da php
										Ext.Ajax.request({
										        url        : 'acs_form_json_spedizione.php',
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        jsonData: {create_new: 'Y',
										        		   from_area_itin: 'N',
										        		   rec_id: null, 
										        		   rec_liv: null,
										        		   tree_id: 'grid_elenco',
										        		   itin_id: pmp.itin_id,
										        		   data: pmp.data}, 
										        success : function(result, request){
										            var jsonData = Ext.decode(result.responseText);
										            print_w.add(jsonData.items);
										            print_w.doLayout();				            
										        },
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });	                           
			                    
												                   		
			                    } 
			                    
			                 }    //iconSpedizione
			                 
		<?php }?>
		
		
<?php 
	//segnalazione scarichi ridondanti
	if ($c_anomalie_data_itin > 0){
?>			                 		
			                 , {
			                    iconCls: 'iconScarichiRidondanti',
			                    itemId: 'segnala_scarichi_ridondanti',
			                    text: 'Scarichi ridondanti (<?php echo $c_anomalie_data_itin; ?>)',
			                    disabled: false,
			                    scope: this,
			                    style:'background-color: #EFEFEF; border: 1px solid red;',
			                    handler: function(bt){
								        acs_show_win_std('Riepilogo scarichi (cliente/destinazione) presenti su pi&ugrave; carichi', 'acs_json_scarichi_ridondanti.php', {data: <?php echo $data ?>, itin_id: <?php echo j($itin_id); ?>, aspe: <?php echo j(trim($itin_obj->rec_data['TAASPE'])); ?>}, 800, 300, {}, 'iconScarichiRidondanti');			                    
			                    	}
			                    }


		
		<?php } ?>
					                 
			                  
			            ]
			    }],				
				
				items: [
					{	xtype: 'panel',
						flex: 2,
						id: 'grid_lx',
						layout: {
									type: 'fit'
								},
						items: []
					},
					
				    {	xtype: 'panel',
						flex: 2,
						id: 'grid_rx',
						layout: {
									type: 'fit'
								},
						items: []
					}				
				]		    
		    
		    }
		    
		]
	}
  ]	
}		
















<?php exit(); ?>
{"success":true, "items": [
	{
		xtype: 'panel',
	
		autoScroll:true,		
		id: 'panel-progetta-spedizioni',

		layout: {
				type: 'table',
				columns: 2,
				tdAttrs: {
				        valign: 'top'
				    },				
				tableAttrs: {
				        style: {
				            width: '100%'
				        }
				    }				    
				    
		},
		defaults: {
		        style: 'padding: 8'
		    },		
		dockedItems: [{
	            dock: 'top',
	            xtype: 'toolbar',
	            items: [
					{
	                    iconCls: 'icon-save',
	                    itemId: 'save',
	                    text: 'CONFERMA SPEDIZIONI',
	                    disabled: false,
	                    scope: this,
	                    handler: function(bt){

	                    	m_panel = bt.up('panel');
	                    	m_grids = m_panel.initialConfig.items2
							Ext.each(m_grids, function(grid) {
	                    		m_panel.add(grid);
	                    	});	                    	
	                    	
	                    	
	                    	ar_grids = bt.up('panel').query('grid');
	                    	ar = new Array();	                    	
	                    	Ext.each(ar_grids, function(grid) {
	                    		ar.push(Ext.pluck(grid.getStore().data.items, 'data'));
	                    	});
	                    	
							Ext.Ajax.request({
							        url        : 'acs_op_exe.php?fn=progetta_spedizioni',
							        method     : 'POST',
							        waitMsg    : 'Data loading',
				        			jsonData: {
				        				ar_spedizioni: ar,
				        				cod_iti: '<?php echo $itin_id; ?>', data: <?php echo $data; ?>
									},							        
							        success : function(result, request){				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	                    	
	                    	
	                    	
	                    }
	                }    		
				]	
		}],
		items2: [
			<?php echo implode(",", $ar_grids) ?>		
		]
		
		
	}	
]}