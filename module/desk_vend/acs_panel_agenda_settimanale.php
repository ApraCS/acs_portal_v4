<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

$all_params = array();
$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());

$panel_id = 'panel-week';

if ((int)$all_params['f_data'] == 0)
	$data = oggi_AS_date();
else $data = $all_params['f_data'];


// ******************************************************************************************
// eventi calendario
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_get_events'){
	
	//data iniziale e di fine
	$from_calendar_format = 'Y-m-d';
	$d_start 	= to_AS_date_from_format($from_calendar_format, $_REQUEST["startDate"]);
	$d_end 		= to_AS_date_from_format($from_calendar_format, $_REQUEST["endDate"]);	
	
	$sql_FROM 	= " FROM {$cfg_mod_Spedizioni['file_calendario']} SP
				 	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
						ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 		
		  				ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			   			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			   		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			   			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
				";
	
	$sql_WHERE 	= " WHERE " . $s->get_where_std() . " AND TDSWSP='Y' 
					AND {$all_params['sceltadata']} >= {$d_start} AND {$all_params['sceltadata']} <= {$d_end}";
	
	
	//filtri in base a selezione inziale su form
	if (isset($all_params['f_area_spedizione']) && strlen(trim($all_params['f_area_spedizione'])) > 0)
		$sql_WHERE .= " AND TA_ITIN.TAASPE = " . sql_t($all_params['f_area_spedizione']);
	//tipologia trasporto (multipla)
	if (count($all_params['f_trasporto']) > 0)
		$sql_WHERE .= " AND SP.CSTITR IN (" . sql_t_IN($all_params['f_trasporto']) . ")";	
	//trasportatore (multipla)
	if (count($all_params['f_trasportatore']) > 0)
		$sql_WHERE .= " AND TA_TRAS.TAKEY1 IN (" . sql_t_IN($all_params['f_trasportatore']) . ")";	
	//divisione
	if (count($all_params['f_divisione']) > 0)
		$sql_WHERE .= " AND TD.TDCDIV = " . sql_t($all_params['f_divisione']);	
	
	if (count($all_params['f_cal_itinerario']) > 0)
		$sql_WHERE .= " AND TD.TDCITI IN (" . sql_t_IN($all_params['f_cal_itinerario']) . ")";	
	
	
	
	$sql_ORD	= " ORDER BY {$all_params['sceltadata']}, " . scegli_campo_ora($all_params['sceltadata']) . ", TA_ITIN.TADESC ";

	$sql = "SELECT {$all_params['sceltadata']} AS DATA, CSSTSP, CSDESC, CSPROG, CSCITI, TA_ITIN.TADESC AS D_ITIN, CSDTSC, CSHMSC, CSCVET, CSCAUT, CSCCON, CSHMPG, CSTISP " . 
					$sql_FROM . $sql_WHERE .
			" GROUP BY {$all_params['sceltadata']}, CSSTSP, CSDESC, CSPROG, CSCITI, TA_ITIN.TADESC, CSDTSC, CSHMSC, CSCVET, CSCAUT, CSCCON, CSHMPG, CSTISP " . $sql_ORD;


	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);	
	
	$fine_ora = array();
	$durata_in_minuti = 15;
	$sped_esportate = array();
	

	$ar_spedizioni = array();

	
	while ($r = db2_fetch_assoc($stmt)) {
		
		if (trim($r['CSSTSP']) == 'AN')
			$r['cid'] = 998; //annullata
		else if (trim($r['CSSTSP']) == 'AGG')
			$r['cid'] = 1099; //aggiuntive
		else
			$r['cid'] = stringToInteger(trim($r['CSCITI'])); //per itinerario
		
		//se non e' annullata ed ha uno stato di quelli da evidenziare (config.php) assegno il relativo calendario
		if (trim($r['CSSTSP']) != 'AN' &&  isset($cfg_mod_Spedizioni["agenda_settimanale"]["cal_per_stato"][trim($r['CSSTSP'])])){
			$r['cid'] = $cfg_mod_Spedizioni["agenda_settimanale"]["cal_per_stato"][trim($r['CSSTSP'])]['id'];
		}
		
			
		$ar_spedizioni[] = $r;
		
		//accodo alle spedizioni gia' esportate
		$sped_esportate[] = (int)$r['CSPROG'];		
	}	
	
	
	//*****************************
	//recupero tutte le spedizioni disponibili in base ai filtri (non gia' mostrate)
	//*****************************	

	if ($all_params['rb_anche_pianificate'] == 'Y'){	
	
	switch ($all_params['sceltadata']){
		case "TD.TDDTSP": $campo_data_sped = 'SP.CSDTPG'; break;
		default: $campo_data_sped = $all_params['sceltadata'];
	}
	
	$sql = "SELECT SP.*,
			{$campo_data_sped} AS DATA, TA_ITIN.TADESC AS D_ITIN, SP.* 	 
			FROM {$cfg_mod_Spedizioni['file_calendario']} SP
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
				ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
				ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'				
	";
	
			
	$sql_WHERE = " WHERE 1=1 ";		
			
	if ($all_params['rb_anche_annullate'] != 'Y'){
		$sql_WHERE 	.= " AND CSSTSP <> 'AN' ";
	}			
	
	$sql_WHERE 	.= "  
		AND {$campo_data_sped} >= {$d_start} AND {$campo_data_sped} <= {$d_end}
		";
	
	if (count($sped_esportate) > 0)
		$sql_WHERE .= "  AND CSPROG NOT IN ( " . implode(", ", $sped_esportate) . ") ";
	
	//filtri in base a selezione inziale su form
	if (isset($all_params['f_area_spedizione']) && strlen(trim($all_params['f_area_spedizione'])) > 0)
		$sql_WHERE .= " AND TA_ITIN.TAASPE = " . sql_t($all_params['f_area_spedizione']);
	//tipologia trasporto (multipla)
	if (count($all_params['f_trasporto']) > 0)
		$sql_WHERE .= " AND SP.CSTITR IN (" . sql_t_IN($all_params['f_trasporto']) . ")";
	//trasportatore (multipla)
	if (count($all_params['f_trasportatore']) > 0)
		$sql_WHERE .= " AND TA_TRAS.TAKEY1 IN (" . sql_t_IN($all_params['f_trasportatore']) . ")";

	if (count($all_params['f_cal_itinerario']) > 0)
		$sql_WHERE .= " AND SP.CSCITI IN (" . sql_t_IN($all_params['f_cal_itinerario']) . ")";
	

	$sql .= $sql_WHERE;

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	

	while ($r = db2_fetch_assoc($stmt)) {
		 
		if (trim($r['CSSTSP']) == 'AN')
			$r['cid'] = 998; //annullata
		else if (trim($r['CSSTSP']) == 'AGG')
			$r['cid'] = 1099; //aggiuntive
		else
			$r['cid'] = 99; //pianificata
				
		//se non e' annullata ed ha uno stato di quelli da evidenziare (config.php) assegno il relativo calendario
		if (trim($r['CSSTSP']) != 'AN' &&  isset($cfg_mod_Spedizioni["agenda_settimanale"]["cal_per_stato"][trim($r['CSSTSP'])])){
			$r['cid'] = $cfg_mod_Spedizioni["agenda_settimanale"]["cal_per_stato"][trim($r['CSSTSP'])]['id'];
		}		
		
		
		$ar_spedizioni[] = $r;
	}
	
	} //if rb_anche_pianificate == 'Y'


	
	//ordino per ora/itinerario
	function cmp_by_itin_d($a, $b){		
		global $all_params;
		
		//print_r($all_params);
		//print_r($a);
		//print_r($b);
		
		if ($a['DATA'] != $b['DATA'])
			return strcmp(strtoupper($a['DATA']), strtoupper($b['DATA']));
		if ($a[scegli_campo_ora($all_params['sceltadata'])] != $b[scegli_campo_ora($all_params['sceltadata'])])
			if ((int)$a[scegli_campo_ora($all_params['sceltadata'])] > (int)$b[scegli_campo_ora($all_params['sceltadata'])])
				return 1; else return -1;		
		if ($a['D_ITIN'] != $b['D_ITIN'])		
			return strcmp(strtoupper($a['D_ITIN']), strtoupper($b['D_ITIN']));
		return strcmp(strtoupper($a['cid']), strtoupper($b['cid']));		
	}
	usort($ar_spedizioni, "cmp_by_itin_d");


	
	
	/* creo le note */
	$sql_note 	= "SELECT * 
					FROM {$cfg_mod_Spedizioni['file_calendario']} SP_CAL
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT 
							  ON SP_CAL.CSDT = NT.NTDT AND SP_CAL.CSDTRG=NTKEY1 AND NTTPNO='NSPDY'					
				   WHERE CSDT = '{$id_ditta_default}' AND SP_CAL.CSCALE = '*CS'
					AND CSDTRG >= {$d_start} AND CSDTRG <= {$d_end} ORDER BY CSDTRG";
	$stmt_note = db2_prepare($conn, $sql_note);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_note);
	while ($r_note = db2_fetch_assoc($stmt_note)) {
		$n_row = 1; //altezza riga evento
		if (!isset($fine_ora["{$r_note['CSDTRG']}"]))
			$fine_ora["{$r_note['CSDTRG']}"] = date('His', strtotime(print_ora('000000', 6, 'Y')));
		
		$e = array();
		$e['id'] = $r_note['CSDTRG'] + 900000000;
		$e['title'] = "[note] " . acs_u8e(trim($r_note['NTMEMO']));
		
		$durata_minuti_evento = $n_row * $durata_in_minuti;
		$ora_start	= date('His', strtotime(print_ora($fine_ora["{$r_note['CSDTRG']}"], 6, 'Y')));
		$ora_end	= date('His', strtotime(print_ora($ora_start, 6, 'Y')) + $durata_minuti_evento*60);
		
		//aggiorno fine ora
		$fine_ora["{$r_note['CSDTRG']}"] = date('His', strtotime(print_ora($ora_end, 6, 'Y')));
		
		$e['start'] = print_date($r_note['CSDTRG'], '%Y-%m-%d') . " " . print_ora($ora_start, 6, 'Y');
		$e['end'] 	= print_date($r_note['CSDTRG'], '%Y-%m-%d') . " " . print_ora($ora_end, 6, 'Y');
		$e['notes'] = "Have fun";
		$e['risorsa'] = '';
		$e['stabilimento'] = '';
		$e['cid'] = 91099;
		
		$ret['events'][] = $e;
	}
	
	
	
	
	foreach ($ar_spedizioni as $r){
		if (!isset($fine_ora["{$r['DATA']}"]))
			$fine_ora["{$r['DATA']}"] = date('His', strtotime(print_ora('000000', 6, 'Y')));
			
		$e = array();
		$e['id'] = (int)$r['CSPROG']; //id calendario
		$e['title'] = "<b>" . trim($r['D_ITIN']) . "</b>" . " [" . print_ora($r[scegli_campo_ora($all_params['sceltadata'])]) . " " . trim($r['CSTISP']) . "] " . trim($main_module->decod_vmc_by_sped_row($r));
		if (strlen(trim($r['CSDESC'])) > 0){
			$n_row = 1.5;
			$e['title'] .= " - <b>" . acs_u8e(trim($r['CSDESC'])) . "</b>";
		} else $n_row = 1;
		
		$durata_minuti_evento = $n_row * $durata_in_minuti;
		$ora_start	= date('His', strtotime(print_ora($fine_ora["{$r['DATA']}"], 6, 'Y')));
		$ora_end	= date('His', strtotime(print_ora($ora_start, 6, 'Y')) + $durata_minuti_evento*60);
		
		//aggiorno fine ora
		$fine_ora["{$r['DATA']}"] = date('His', strtotime(print_ora($ora_end, 6, 'Y')));
		
		
		$e['start'] = print_date($r['DATA'], '%Y-%m-%d') . " " . print_ora($ora_start, 6, 'Y');
		$e['end'] 	= print_date($r['DATA'], '%Y-%m-%d') . " " . print_ora($ora_end, 6, 'Y');
		$e['notes'] = "Have fun";
		$e['risorsa'] = '';
		$e['stabilimento'] = '';
		$e['cid'] = $r['cid'];
		
		$ret['events'][] = $e;		
	}
	
	
 echo acs_je($ret);	
exit; }




// ******************************************************************************************
// FORM PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_form_params'){
?>
{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "OPEN_WEEK");  ?>{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            
			            	var form = this.up('form').getForm();
			            	m_win = this.up('window');
			            	form_values = form.getValues();
			            	
			            	//in visualizzazione non tengo conto della data finale (usata solo nel report)
			            	form_values['f_data_a'] = null;
			            	 
			            	if(form.isValid()){
			            	
					        	mp = Ext.getCmp('m-panel');
					        	av_panel_week = Ext.getCmp(<?php echo j($panel_id); ?>);					        	
					
					        	if (av_panel_week){
					        		//av_panel_week.show();
					        		mp.remove(av_panel_week);		    	
					        	} 
					
					        		//carico la form dal json ricevuto da php
					        		Ext.Ajax.request({
					        		        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_calendar_panel',
					        		        jsonData   : form_values,
					        		        method     : 'POST',
					        		        waitMsg    : 'Data loading',
					        		        success : function(result, request){
					        		            var jsonData = Ext.decode(result.responseText);
					        		            mp.add(jsonData.items);
					        		            mp.doLayout();
					        		            mp.show();
					        		            av_prenotazione_carichi = Ext.getCmp(<?php echo j($panel_id); ?>).show();
					        		            
					        		            m_win.close();        		            
					        		        },
					        		        failure    : function(result, request){
					        		            Ext.Msg.alert('Message', 'No data to be loaded');
					        		        }
					        		    });
					
					        	 
							
			            	}
			            }
			         }],   		            
		            
		            items: [
		            
		            
						 {
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
				 	        layout: 'hbox',
				 	        items: [		            		            		            
				            	{
								   xtype: 'datefield'
								   , fieldLabel: 'Data'
								   , startDay: 1 //lun.
								   , name: 'f_data'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '-15'
								   , margin: "10 10 0 10"
								}, {
				                    xtype: 'radiogroup',
				                    layout: 'vbox',
				                    fieldLabel: '',
				                    flex: 1,
				                    allowBlank: false,
				                    items: [{
					                            xtype: 'radio'
					                          , name: 'sceltadata' 
					                          , boxLabel: 'Data spedizione'
					                          , inputValue: 'TD.TDDTSP'
					                          , checked: true                          
					                        },
					                        {
					                            xtype: 'radio'
					                          , name: 'sceltadata' 
					                          , boxLabel: 'Data scarico'
					                          , inputValue: 'SP.CSDTSC'                          
					                        }
				                   ]
				                }
							]
						}		
						
						
						
						, {
							name: 'f_area_spedizione',
							xtype: 'combo',
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: false,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('ASPE'), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_trasporto',
							xtype: 'combo',
							fieldLabel: 'Tipologia trasporto',
							displayField: 'text',
							valueField: 'id',
						   	allowBlank: true,							
							emptyText: '- seleziona -',
							multiSelect: true,
							forceSelection:true,
						    anchor: '-15',
						    margin: "20 10 10 10",
						    value: <?php echo j(trim($sped['CSTITR'])); ?>,							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'), ""); ?>	
								    ] 
							}						 
						  }, {
							name: 'f_trasportatore',
							xtype: 'combo',
							fieldLabel: 'Trasportatore',
							displayField: 'text',
							valueField: 'id',
						   	allowBlank: true,							
							emptyText: '- seleziona -',
							multiSelect: true,
							forceSelection:true,
						    anchor: '-15',
						    margin: "20 10 10 10",							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('AVET'), ""); ?>	
								    ] 
							}						 
						  }
						  
						  
						, {
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}	

							
							
							
						, {
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
				 	        layout: 'hbox',
				 	        items: [							
								{
				                    xtype: 'radiogroup',
				                    layout: 'hbox',
				                    fieldLabel: 'Pianificate',
				                    flex: 1,
				                    allowBlank: false,
				                    margin: "20 10 10 10",
				                    items: [{
					                            xtype: 'radio'
					                          , name: 'rb_anche_pianificate' 
					                          , boxLabel: 'Si'
					                          , inputValue: 'Y'
					                          , width: 40                          
					                        },
					                        {
					                            xtype: 'radio'
					                          , name: 'rb_anche_pianificate' 
					                          , boxLabel: 'No'
					                          , inputValue: 'N'
					                          , checked: true					                          
					                          , width: 40					                                                    
					                        }
				                   ]
				                }, {
				                    xtype: 'radiogroup',
				                    layout: 'hbox',
				                    fieldLabel: 'Anche annullate',
				                    flex: 1,
				                    allowBlank: false,
				                    margin: "20 10 10 10",
				                    items: [{
					                            xtype: 'radio'
					                          , name: 'rb_anche_annullate' 
					                          , boxLabel: 'Si'
					                          , inputValue: 'Y'
					                          , width: 40                          
					                        },
					                        {
					                            xtype: 'radio'
					                          , name: 'rb_anche_annullate' 
					                          , boxLabel: 'No'
					                          , inputValue: 'N'
					                          , checked: true					                          
					                          , width: 40					                                                    
					                        }
				                   ]
				                }
				            ]
				         }
							

							
												  
						  
						  ]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
<?php exit; }


// ******************************************************************************************
// PANEL CALENDARIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_calendar_panel'){

function itin_cmp_by_desc($a, $b)
{
	if (strtoupper($a['TADESC']) == strtoupper($b['TADESC'])) {
		return 0;
	}
	return (strtoupper($a['TADESC']) < strtoupper($b['TADESC'])) ? -1 : 1;
}


//costruisco l'elenco dei calendari (itinerari)
$itinerari = new Itinerari;
$itinerari = $itinerari->find_all();
usort($itinerari, "itin_cmp_by_desc");
$c = 0;
foreach($itinerari as $i){
	if ($i['TAASPE'] == $all_params['f_area_spedizione'])
		$ar_it[] = array("id" => stringToInteger(trim($i['TAKEY1'])), "cod_itinerario" =>trim($i['TAKEY1']), "title" => $i['TADESC'], "color" => 21);
}

if ($all_params['rb_anche_pianificate'] == 'Y'){
	//calendario dei "pianificati"
	$ar_it[] = array("id" => 999, "title" => 'Pianificati', "color" => 6);
}		


	//calendario delle "Annullate"
	// lo mostro sempre perche' potrei avere delle annullate con ordini abbinati (e quindi vengono fuori)
	$ar_it[] = array("id" => 998, "title" => 'Annullate', "color" => 3);
	$ar_it[] = array("id" => 1099, "title" => 'Aggiuntive',  "color" => 28);
	$ar_it[] = array("id" => 91099, "title" => 'Note',  "color" => 25);
	
	//itinerari evidenziati per stato (definiti su config.php)
	if (isset($cfg_mod_Spedizioni["agenda_settimanale"]["cal_per_stato"])){
		foreach ($cfg_mod_Spedizioni["agenda_settimanale"]["cal_per_stato"] as $k_stato => $stato){
			$ar_it[] = array("id" => $stato['id'], "title" => $stato['title'], "color" => $stato['color']);
		}
	}
	


$calendar_store = acs_je(array("data" => array("calendars" => $ar_it)));


?>
{"success":true, "items": [
 {
  xtype: 'panel',
  id: <?php echo j($panel_id); ?>,
  title: 'Week',
  closable: true,
  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
  calendarStore: new Extensible.calendar.data.MemoryCalendarStore(<?php echo $calendar_store; ?>),
  
				tbar: new Ext.Toolbar({
            		items:['<b>Agenda settimanale spedizioni</b>', 
            			, '->'
            			, {
								name: 'f_cal_itinerario',
								xtype: 'combo',
								fieldLabel: 'Filtra itinerari',
								forceSelection: true,								
								displayField: 'title',
								valueField: 'cod_itinerario',								
								emptyText: '- seleziona -',
						   		allowBlank: true,
						   		multiSelect: true,
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'cod_itinerario'}, {name:'title'}],
								    data: <?php echo acs_je($ar_it); ?>
								},
								
								listeners: {
								  //on lose focus 
								  blur: function(combo, eOpts){
								   var store = combo.getStore();
								   var values = [];
								   calendari = this.up('panel').query('[xtype="extensible.calendarpanel"]');	                    	
								   Ext.each(calendari, function(cal) {
								   	cal.activeView.store.proxy.extraParams.f_cal_itinerario = combo.getValue();								   	
								   	cal.setStartDate(cal.startDate);
								   });
								  }
								 } 								
								
								
						 }
						, '->'
           				, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ 
								   calendari = this.up('panel').query('[xtype="extensible.calendarpanel"]');	                    	
								   Ext.each(calendari, function(cal) {								   	
								   	cal.setStartDate(cal.startDate);
								   });           						
           						
           					}}
       					<?php echo make_tbar_closable() ?>
         			]            
        		}),  
  
  
  listeners: {                    
    afterrender: function (comp) {
    				var use_calsendar_store = null;
    						//uso sempre gli stessi calendari
                    		elenco_calendari = comp.query('[xtype="extensible.calendarlist"]');
					        	Ext.each(elenco_calendari, function(rf) {
					        		//console.log('setStore -> calendarlist');

									call = comp.down('[xtype="extensible.calendarpanel"]');
									rf.setStore(call.calendarStore);					        		
					        		
					        		rf.refresh();
					        		//console.log(rf);
					        	});
                    		calendarios = comp.query('[xtype="extensible.calendarpanel"]');
					        	Ext.each(calendarios, function(rf) {
					        		//console.log('setStore -> calendarpanel');					        	
					        		rf.setStore(comp.calendarStore);
					        		//rf.refresh();					        		
					        		//console.log(rf);
					        		});					        	
					        	    
                    		comp.show();
                     		
                 	},
  }
  
  
  , items: [
  		
  
 {
	xtype: 'panel',
	layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
	flex: 1,	
    collapsible: false,
    collapsed: false,
    			
	items: [	  
    
    
		    //sidebar con calendario data e elenco calendario (itinerari)
			{
				xtype: 'panel',
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				width: 150,								
				items: [
					{
                        xtype: 'extensible.calendarlist',
                        autoScroll: true,
                        title: 'Itinerari',
                		collapsible: false,
                		collapsed: false,	                        
                        //store: new Extensible.calendar.data.MemoryCalendarStore(<?php echo $calendar_store; ?>),
                        //store: myCalendarStore,	
                        store: new Extensible.calendar.data.MemoryCalendarStore(),
                        border: false,
                        flex: 1
                    }
				]
			}, 	    
    
    
    	// **************************************************************************
    	// calendario
    	// **************************************************************************
  
			{
		        xtype: 'extensible.calendarpanel',		        
		        flex: 1,
		        border: false,
		        frame: true,
				title: '',
				
                collapsible: false,
                collapsed: false,	
                
				enableEditDetails: false,
				readOnly: true,  
				
				showMonthView: false,
				showMultiWeekView: false,				               

				//definizione calendari
        		calendarStore: new Extensible.calendar.data.MemoryCalendarStore(<?php echo $calendar_store; ?>),
        		//calendarStore: myCalendarStore,
        		//calendarStore: new Extensible.calendar.data.MemoryCalendarStore(),
        						
        			//recupero eventi
					eventStore: new Extensible.calendar.data.EventStore({

					       autoLoad: true,
        				   autoSync: true,

		                    proxy: {
		                    	url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
		                        type: 'ajax',
								method: 'POST',
								
								extraParams: <?php echo acs_raw_post_data() ?>,
								
									/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */					
						            doRequest: personalizza_extraParams_to_jsonData,	   								
								
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'events'
								},
						            		                        
		                        
					            writer: {
					                type: 'json',
					                writeAllFields: true
					            },		
					            
					            api: {
					                read: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
					                create: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_crt_events',
					                update: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_upd_events',
					                destroy: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_del_events',
					            },
					            				                        
		                        
		                    }					          
					    }),


                    activeItem: 1, // week view 
                    
                    showNavBar: true,
                    
                    weekViewCfg: {
                        showHeader: true,
                        showWeekLinks: true,
                        showWeekNumbers: true,
						startDate: js_date_from_AS(<?php echo $data; ?>),
						viewStartHour  : 0,
						scrollStartHour: 0,
						showTime: false,
						showTodayText: false,						
						showHourSeparator: false,
						hourHeight: 200,
						minEventDisplayMinutes: 15,
						startDay: 1 //lunedi
                    },                      
                    
                    
                    dayViewCfg: {
                        showHeader: false,
                        showWeekLinks: true,
                        showWeekNumbers: true,
						startDate: js_date_from_AS(<?php echo $data; ?>),
						viewStartHour  : 0,
						scrollStartHour: 0,
						ddGroup: 'DayViewDD',
						showTime: false,
						showTodayText: false,						
						showHourSeparator: false,
						hourHeight: 100,
						minEventDisplayMinutes: 15							
                    }                      
			    
                , listeners: {
                
                		'itemcontextmenu' : function(grid, rec, node, index, event) {
                			//console.log('dx click');
                		},
                
                        'eventclick': {
                			fn: function(panel, rec, el){	
                			
                				var voci_menu = [];
                				
                				//NOTE GIORNO
                				if (parseInt(rec.get('CalendarId')) == 91099){
								  	acs_show_win_std('Gestione note', 'acs_form_json_note_std.php?fn=gest', {
								  		n_key: {
								  			dt: <?php echo j($id_ditta_default);?>,
								  			key1: rec.get('EventId')-900000000, 
								  			tpno: 'NSPDY'
								  		}
								  	 }, 600, 450, {
								  	 	afterSave: function(){
								  	 		panel.reloadStore();
								  	 	}
								  	 }, 'icon-shopping_cart_gray-16');
           				
                				} else {
                				
	                				if (parseInt(rec.get('CalendarId')) != 999)                				
							  		    voci_menu.push({
							          		text: 'Dettagli spedizione',
							        		iconCls : 'iconSpedizione',          		
							        		handler: function() {
												mp = Ext.getCmp('m-panel');
												mp.add(
											  		show_el_ordini(panel, rec, null, null, null, 'acs_get_elenco_ordini_json.php?tipo_cal=per_sped&sped_id=' + rec.get('EventId'), Ext.id())
								            	).show();					        							        	
							        		}
							    		});
	
	                				
							  		    voci_menu.push({
							          		text: 'Modifica spedizione',
							        		iconCls : 'iconSpedizione',          		
							        		handler: function() {
							        		
							        			
								                   	my_listeners = {
							        					acsaftersave: function(){
							        							panel.reloadStore();
											        		}
									    				};												
												
									    			  	acs_show_win_std('Modifica spedizione', 'acs_form_json_spedizione.php', 
									    			  		{		
									    			  		 sped_id: rec.get('EventId'),						    			  		 
									    			  		 from_sped_id: 'Y'
									    			  		}, 600, 550, my_listeners, 'iconSpedizione');
																				    			  													
							        		
							        		
							        		}
							    		});
							    		
	                				if (parseInt(rec.get('CalendarId')) != 999)						    		
							  		    voci_menu.push({
							          		text: 'Report scarichi',
							        		iconCls : 'iconPrint',          		
							        		handler: function() {
												
									    			  	acs_show_win_std('Parametri report scarichi', 'acs_get_elenco_stampa_form.php', 
									    			  		{		
									    			  		 rec_id: '|SPED|' + rec.get('EventId'),						    			  		 
									    			  		 rec_liv: 'liv_0',
									    			  		 tipo_elenco: 'GATE'
									    			  		}, 600, 400, null, 'iconPrint');
																				    			  													
							        		
							        		
							        		}
							    		});						    		
						    			
						    		}						    	           
					    		
					    		ext_el = Ext.get(el);
								var menu = new Ext.menu.Menu({
							      items: voci_menu
								}).showAt(ext_el.getXY());
								return false;					    		
							}	   						
                		}                	
                }			    
		        , editModal: true
		    } //calendar panel 
		    
		]
	}	    
  ]
  
 }
]
}
<?php exit; } ?>


<?php


function to_AS_date_from_format($format, $data){
	switch ($format){
		case "m-d-Y":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[0], $d_ar[1], $d_ar[2]);
			return to_AS_date($d);
		case "Y-m-d":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[1], $d_ar[2], $d_ar[0]);
			return to_AS_date($d);
				
	}
}

function scegli_campo_ora($tipo_data){
 switch ($tipo_data){
	case 'TD.TDDTSP': return 'CSHMPG';
	case 'SP.CSDTSC': return 'CSHMSC'; 
 }
}

?>