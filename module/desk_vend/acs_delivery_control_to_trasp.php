<?php

require_once "../../config.inc.php";


//configurazione delle diverse tipologie di esportazione
$cfg_delivery_control_to_trasp = array(
    "UR21HF" => array(
        "carico"  => array("file" => "{$libreria_predefinita}.WPI8PS0", "id" => "PSID"),
        "testata" => array("file" => "{$libreria_predefinita}.WPI8TD0", "id" => "TDID"),
        "colli"   => array("file" => "{$libreria_predefinita}.WPI8PL0", "id" => "PLID")
    )
);



$s = new Spedizioni(array('no_verify' => 'Y'));
ini_set('max_execution_time', 6000);

$m_params = acs_m_params_json_decode();
$ts = $m_params->tms;


//ToDO: 
//parametrizzare: in base a config trasportatore (dalla sped.) dovrei richiarmare UR21HE (mbm) o UR21HF (SAV)
$tipologia_export = "UR21HF";



//********************************************************************
// generazione in base a configuratore su trasportatore
//********************************************************************
if ($_REQUEST['fn'] == 'exe_gen_to_trasp'){
//********************************************************************

    
	$seq = array();
	global $conn;
	//Eseguo la call
	//costruzione del parametro
	
	foreach($m_params->ar_sped_id as $sped_id){	
    	$cl_p = str_repeat(" ", 246);
    	 $cl_p .= sprintf("%-2s",  $id_ditta_default);
    	 $cl_p .= sprintf("%09d", $sped_id);
    	 $cl_p .= sprintf("%-2s", "");
    	 $cl_p .= sprintf("%04d", 0);
    	 $cl_p .= sprintf("%06d", 0);
    	 $cl_p .= sprintf("%-1s", "");
    	 $cl_p .= sprintf("%-1s", "");
    	 $cl_p .= sprintf("%-2s", "");
    	 $cl_p .= sprintf("%-10s", trim($auth->get_user()));
    	 $cl_p .= sprintf("%-30s", "");
    	 $cl_p .= sprintf("%-50s", "");
    	 $cl_p .= sprintf("%-9s", "");
    	 $cl_p .= sprintf("%-9s", "");
    	 $cl_p .= sprintf("%-30s", $ts);  //progressivo timestamp
    	 $cl_p .= sprintf("%-91s", "");
    
    	 
    	 if ($useToolkit == 'N'){
    	     //per test in Apra
    	     $qry1	 =	"CALL {$libreria_predefinita_EXE}.{$tipologia_export}('{$cl_p}')";
    	     $stmt1   = 	db2_prepare($conn, $qry1);
    	     $result1 = 	db2_execute($stmt1);
    	 } else {
    	     $call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
    	     $call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
    	     $call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
    	     $cl_in = array();
    	     $cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
    	     $call_return = $tkObj->PgmCall($tipologia_export, $libreria_predefinita_EXE, $cl_in, null, null);
    	 }	 
	} //foreach sped_id

	 $ret = array();
	 $ret['success'] = true;
	 echo acs_je($ret);
	 exit;	 
}




//********************************************************************
// trasmissione file (in base a configurazione del trasportatore)
//********************************************************************
if ($_REQUEST['fn'] == 'exe_send_to_trasp'){
//********************************************************************

  $cfg_delivery_control_ftp_trasp = array(); //azzero per sicurezza: in precedenza veniva definito in config.inc.php
    
  //recupero il trasportore e la modalita di invio
  $sped_id = $m_params->sped_id;
  $sped = $s->get_spedizione($sped_id);
  
  //dal vettore recupero il trasportatore
  $trasp = new Trasportatori();
  $trasp->load_rec_data_by_vettore(trim($sped['CSCVET']));
  $trasp_code = $trasp->rec_data['TAKEY1'];
  
  //recupero la configurazione Delivery Control del trasportatore
  $sql = "SELECT * from {$cfg_mod_Spedizioni['file_note']} NT 
          WHERE NTDT={$id_ditta_default}
            AND NTTPNO = 'C_TRA' AND NTKEY1 = ? AND NTKEY2 = 'DC'";
  
  $stmt = db2_prepare($conn, $sql);
  echo db2_stmt_errormsg();
  $result = db2_execute($stmt, array($trasp_code));
  $row = db2_fetch_assoc($stmt);
  
  if (!$row){
      //Manca la config di invio per il trasportatore      
      echo acs_je(array('success' => false, 'message' => 'Manca configurazione DC per il trasportatore'));
      exit;      
  }
  
  $dc_config = json_decode($row['NTMEMO']);  
  
  $cfg_delivery_control_ftp_trasp = array(
      'host'     => $dc_config->host,
      'user'     => $dc_config->user, 'psw' => $dc_config->psw,
      'to_dir'   => $dc_config->to_dir,
      'from_dir' => $dc_config->from_dir,
      'from_dir_done' => $dc_config->from_dir_done
  );
    
  //se devo generare uno file zip
  $res = _create_zip_file($ts);
  
  if ($res['success'] == false){
      echo acs_je($res);
      exit;
  }
  
  $zip_path = $res['zip_path'];
  
  //Invio in base a regola definita su trasportatore
  switch ($dc_config->mode) {
      case 'ftp':
          
          $cfg_delivery_control_ftp_trasp = array(
                'host'     => $dc_config->host,
                'user'     => $dc_config->user, 'psw' => $dc_config->psw,
                'to_dir'   => $dc_config->to_dir,
                'from_dir' => $dc_config->from_dir,
                'from_dir_done' => $dc_config->from_dir_done
              );
          
          $res = _send_via_ftp(array($zip_path));
          break;
      
      case 'sftp':
          //$res = _send_via_sftp(array($zip_path));
          break;
      
      case 'mail':
          break;
          //$res = _send_via_email_to_user('m.arosti@apracs.it', array($zip_path));
          
      default:
          $res = array(
            'success' => false,
            'message' => 'Modalit&agrave; di invio a trasportatore non definita'
          );
      break;
  }
  
  echo acs_je($res);  
  exit;
}










function _create_zip_file($ts){
    $res = _create_new_empty_zip_file($ts);
    if ($res['success'])
      $zip      = $res['zip_handler'];  
    else 
      return $res;    

    //accodo contenuto
    $res = _add_content_to_zip($zip, $ts);
    
    $ret = array();
    $ret['success'] = true;
    $ret['zip_path']  = $zip->filename;
    $ret['num_files'] = $zip->numFiles;
    
    //chiudo zip
    $zip->close();        

    return $ret;
}





function _create_new_empty_zip_file($ts){
    $now = DateTime::createFromFormat('U.u', microtime(true));
    $sf = $now->format("YmdHisu");
    
    //scrivo file zip contentente i file
    $zip = new ZipArchive();
    $filename = '/tmp/dc_zip_'.$ts.'.zip';
    
    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
        $ret = array();
        $ret['success'] = false;
        $ret['message'] = "Impossibile creare file zip {$filename}";
        return $ret;
    } else {
        $ret = array();
        $ret['success'] = true;
        $ret['zip_path'] = $filename;
        $ret['zip_handler'] = $zip;
    }
  return $ret;  
}



function _add_content_to_zip(&$zip, $ts){
    global $cfg_delivery_control_to_trasp, $tipologia_export;
    
    $now = DateTime::createFromFormat('U.u', microtime(true));
    $sf = $now->format("YmdHisu");
    
    foreach ($cfg_delivery_control_to_trasp[$tipologia_export] as $k1=>$v1){
        $contenuto_file = contenuto_file($ts, $k1);
        $zip->addFromString(strtoupper($k1."_ITF").".{$sf}.csv", $contenuto_file);
        $nome_file = $ts."_".$k1.".csv";
        $local_dir= "/tmp/";
        $remote_dir="item";
    }
    
    $ret = array();
    $ret['success'] = true;
    $ret["numfiles"] = $zip->numFiles;
    $ret["status"]   = $zip->status;
    return $ret;
}





function _send_via_email_to_user($to, $ar_attachments){
  
    $mail = prepare_new_mail();
    
    $mail->addAddress($to);    
    
    $mail->IsHTML(true);
    $mail->Subject = 'Invio file delivery contro';
    $mail->Body = 'Invio file di test per spunta vettore';  
        
    foreach($ar_attachments as $a)
        $mail->AddAttachment($a);
    
    if($mail->Send())
      return array('success' => true);
    else
      return array('success' => false, 'message' => 'Errore in invio email');  
    
}


//**************************************************************
function _send_via_ftp($ar_attachments){
//**************************************************************
    $currentIncludePath = get_include_path();
    
    global $cfg_delivery_control_ftp_trasp; //Da parametrizzare in base a trasp: Todo!!!!    
    
    $conn_ftp = ftp_connect($cfg_delivery_control_ftp_trasp['host']);
    $login_result = ftp_login($conn_ftp, $cfg_delivery_control_ftp_trasp['user'], $cfg_delivery_control_ftp_trasp['psw']);    
    if (!$login_result) {
        return array('success' => false, 'message' => 'Sftp login failed');
    }
    
    $now = DateTime::createFromFormat('U.u', microtime(true));
    $sf = $now->format("YmdHisu");
    
    foreach($ar_attachments as $filename){
        if (ftp_put($conn_ftp, "{$cfg_delivery_control_ftp_trasp['to_dir']}/{$sf}.zip", $filename, FTP_BINARY))
        {
            $ret['message'] = "Successfully uploaded {$cfg_delivery_control_ftp_trasp['to_dir']}/{$sf}.zip from {$filename}";
            $ret['success'] = true;
        }
        else
        {
            $ret['message'] = "Error uploading $filename to {$cfg_delivery_control_sftp['to_dir']}/{$sf}.zip";
            $ret['success'] = false;
            return $ret;
        }
    }
    return $ret;
}


//**************************************************************
function _send_via_sftp($ar_attachments){
//**************************************************************
    $currentIncludePath = get_include_path();
    
    set_include_path($currentIncludePath.  PATH_SEPARATOR . ROOT_ABS_PATH . "utility/phpseclib1.0.5");
    
    require('Crypt/Base.php');
    require('Crypt/RC4.php');
    require('Crypt/Hash.php');
    require('Math/BigInteger.php');
    require('Net/SSH2.php');
    require('Net/SFTP.php');
    require('Crypt/Random.php');
    require('Crypt/Rijndael.php');
    
    define('NET_SFTP_LOGGING', NET_SFTP_LOG_COMPLEX);
    
    global $cfg_delivery_control_sftp_trasp; //Da parametrizzare in base a trasp: Todo!!!!
    
    $sftp = new Net_SFTP($cfg_delivery_control_sftp_trasp['host']);
    if (!$sftp->login($cfg_delivery_control_sftp_trasp['user'], $cfg_delivery_control_sftp_trasp['psw'])) {
        return array('success' => false, 'message' => 'Sftp login failed');
    }
    
    $now = DateTime::createFromFormat('U.u', microtime(true));
    $sf = $now->format("YmdHisu");
    
    foreach($ar_attachments as $filename){
        if ($sftp->put("{$cfg_delivery_control_sftp_trasp['to_dir']}/{$sf}.zip", $filename, NET_SFTP_LOCAL_FILE))
      {
        $ret['message'] = "Successfully uploaded";
        $ret['success'] = true;
      }
      else
      {
        $ret['message'] = "Error uploading $filename to {$cfg_delivery_control_sftp['to_dir']}/{$sf}.zip";
        $ret['success'] = false;
        return $ret;
      }
    }
  return $ret;    
}






















function contenuto_file($ts, $tabella){
    global $cfg_delivery_control_to_trasp, $tipologia_export, $conn, $s;
    
    $tp_export_config = $cfg_delivery_control_to_trasp[$tipologia_export];

    $sql="SELECT * FROM {$tp_export_config[$tabella]['file']} WHERE {$tp_export_config[$tabella]['id']}='{$ts}'";  

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	if (isset($tp_export_config[$tabella]['campi_data']))
	    $campi_data = $tp_export_config[$tabella]['campi_data'];
	else
		$campi_data = array();

	while($row = db2_fetch_assoc($stmt)){
	    
		array_shift($row); //toglie il primo elemento
		
		foreach ($row as $kr=>$v) { //se e' di tipo data ed e' uguale a 0 sostituisco il valore con blank
			if (in_array($kr, $campi_data)) {
				$my_v = trim($v);
				if (strlen($my_v) == 8)
					$row[$kr] = print_date($my_v, "%Y/%m/%d");
				else if (strlen($my_v) == 20) {					
					$row[$kr] = print_date(substr($my_v, 0, 8), "%Y/%m/%d") ."-". implode(".", array(
						substr($my_v, 8, 2),substr($my_v, 10, 2),substr($my_v, 12, 2),substr($my_v, 14, 6)
					));
				}
				else 
					$row[$kr] = "";

			} //conversione data
		}
		
		//sostituisco le coordinate
		if (isset($tp_export_config[$tabella]['campi_coordinate'])){
			$gmap_ind = implode(",", array_map('trim', array($row['ADNAZI'], $row['ADCAP'], $row['ADPROV'], $row['ADCITT'] , substr($row['ADINDI'], 0, 30))));
			$row_coordinate = $s->get_coordinate_by_ind($gmap_ind);
			if (is_null($row_coordinate) == false && $row_coordinate['GLLAT'] && $row_coordinate['GLLNG']) {
			    $row[$tp_export_config[$tabella]['campi_coordinate']['long']] = $row_coordinate['GLLNG'];
			    $row[$tp_export_config[$tabella]['campi_coordinate']['lati']] = $row_coordinate['GLLAT'];
			}
		}
		
		$contenuto_r[] = implode('|', array_map('trim', $row));
	}

	return implode("\n", $contenuto_r) . "\n";

}


function contenuto_file_all($tabella){
	global $cfg_delivery_control, $conn;

	$sql="SELECT * FROM {$cfg_delivery_control[$tabella]['file']}";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	if (isset($cfg_delivery_control[$tabella]['campi_data']))
		$campi_data = $cfg_delivery_control[$tabella]['campi_data'];
	else
		$campi_data = array();

	while($row = db2_fetch_assoc($stmt)){
		///array_shift($row); //toglie il primo elemento

		foreach ($row as $kr=>$v) { //se e' di tipo data ed e' uguale a 0 sostituisco il valore con blank
			if (in_array($kr, $campi_data) && trim($v) == '0')
				$row[$kr] = "";
		}

		$contenuto_r[] = implode('|', array_map('trim', $row));
	}

	return implode("\n", $contenuto_r) . "\n";

}
