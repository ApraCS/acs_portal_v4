<?php

require_once "../../config.inc.php";


$s = new Spedizioni();
$main_module = new Spedizioni();



function parametri_sql_where($form_values){

	global $s;

	$sql_where.= " WHERE " . $s->get_where_std();

	$sql_where.= sql_where_by_combo_value('TDCDIV', $form_values->f_divisione);

	$sql_where.= sql_where_by_combo_value('TDASPE', $form_values->f_areaspe);

	$sql_where.= sql_where_by_combo_value('TDCLOR', $form_values->f_tipologia_ordine);

	$sql_where.= sql_where_by_combo_value('TDSTAT', $form_values->f_stato_ordine);

	$sql_where.= sql_where_by_combo_value('RDFORN', $form_values->f_fornitore);

	$sql_where.= sql_where_by_combo_value('RDRIFE', $form_values->f_stato_fornitore);

	$sql_where.= sql_where_by_combo_value('TDLAVA', $form_values->f_avan);

	//controllo HOLD
	$filtro_hold=$form_values->f_hold;

	if($filtro_hold== "Y")
		$sql_where.=" AND TDSWSP = 'Y'";
	if($filtro_hold== "N")
		$sql_where.=" AND TDSWSP = 'N'";

	if (isset($form_values->f_indice_rottura)  && strlen($form_values->f_indice_rottura) > 0 )
		$sql_where .= " AND TDIRLO LIKE '%{$form_values->f_indice_rottura}%'";
	if ($form_values->f_indice_rottura_assegnato == "Y")
		$sql_where .= " AND TDIRLO <> '' ";
	if ($form_values->f_indice_rottura_assegnato == "N")
		$sql_where .= " AND TDIRLO = '' ";
	

	if ($form_values->f_ordine_ab == "Y")
		$sql_where .= " AND RDDES2 <> '' ";
	if ($form_values->f_ordine_ab == "N")
		$sql_where .= " AND RDDES2 = '' ";


						return $sql_where;
}


if ($_REQUEST['fn'] == 'exe_conferma_fornitura'){

	$m_params = acs_m_params_json_decode();

	$all_ord=$m_params->all_ord;

	$use_session_history = microtime(true);


	foreach ($all_ord as $v){
		 

		$sh = new SpedHistory();
		$sh->crea(
				'conferma_fornitura',
				array(
						"k_ordine"		=> $v->k_ordine, //il tddocu
						"use_session_history" => $use_session_history,
						"num_prog"		=>  $v->k_prog_MTO,
						"tipo_op"		=>  $v->tipo_op

							
				)

				);

	}


	$sh = new SpedHistory();
	$sh->crea(
			'conferma_fornitura',
			array(
					"k_ordine"		=> '', //il tddocu
					"tipo_op"		=>  $v->tipo_op,
					"end_session_history" => $use_session_history
			)
			);
	exit;
}





if ($_REQUEST['fn'] == 'get_json_data_stato_ordine'){

	global $cfg_mod_Spedizioni;
	global $s;

	$m_params = acs_m_params_json_decode();

	$divisione_selected=$m_params->divisione;
	
    echo acs_je($s->get_options_stati_ordine('Y', array('divisione' => $divisione_selected)));
	
    exit();
}


if ($_REQUEST['fn'] == 'get_json_data_tipologia'){

	global $cfg_mod_Spedizioni;
	global $s;

	$m_params = acs_m_params_json_decode();

	$divisione_selected=$m_params->divisione;

	echo acs_je(find_TA_std('TIPOV', null, 1, null, null, null, null, array('divisione' => $divisione_selected)));

	exit();
}



if ($_REQUEST['fn'] == 'get_json_data_stato'){

	global $cfg_mod_Spedizioni;

	$m_params = acs_m_params_json_decode();

	//$divisione_selected=$m_params->divisione;

	$sql = "SELECT DISTINCT RDRIFE FROM {$cfg_mod_Spedizioni['file_righe']}
	WHERE RDTPNO='MTO'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();
	$ar_tot = array();

	while ($r = db2_fetch_assoc($stmt)) {

		$r['RDRIFE'] = trim($r['RDRIFE']);
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}

if ($_REQUEST['fn'] == 'get_json_data_elenco_fornitori'){

	global $cfg_mod_Spedizioni;

	$ret = array();

	$m_params = acs_m_params_json_decode();

	$sql_where = sql_where_by_combo_value('RDRIFE', $m_params->stato_forn);
	
    $sql = "SELECT DISTINCT RDFORN, RDDFOR
	FROM {$cfg_mod_Spedizioni['file_righe']}
	WHERE RDTPNO = 'MTO' {$sql_where}
 	ORDER BY RDDFOR";


	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);


	while ($row = db2_fetch_assoc($stmt)){
			
		$ret[] = array("id"=>trim($row['RDFORN']), "text" =>trim($row['RDDFOR']));
	}

	echo acs_je($ret);
	exit();
}

if ($_REQUEST['fn'] == 'get_parametri_form'){

	//Elenco opzioni "Area spedizione"
	$ar_area_spedizione = $s->get_options_area_spedizione();
	$ar_area_spedizione_txt = '';

	$ar_ar2 = array();
	foreach ($ar_area_spedizione as $a)
		$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
		$ar_area_spedizione_txt = implode(',', $ar_ar2);

			
		?>

{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: false,
	autoScroll:true,
	title: '',
	//url: 'acs_riepilogo_spedizioni_carichi.php?fn=get_json_data_carichi',
	buttonAlign:'center',
	buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "REP_SOLL_FORN");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	             handler: function() {
	                form = this.up('form').getForm();
	                   	                
		               if (form.isValid()){
		               
		                 if (Ext.isEmpty(form.findField("f_fornitore").getValue())){
		              	acs_show_msg_error('Inserire un fornitore per procedere');
		              	return false;
		              } 
		               	                	                
			               acs_show_panel_std('acs_sollecito_forniture.php', 'sollecito_forniture', {form_values: form.getValues()});
			               this.up('window').close(); 	  
		                }	
	                
	                               
	            }
	        },{
	            text: 'Report fornitore',
	            iconCls: 'icon-print-32',
	            scale: 'large',	            
	            handler: function() {
	                    form = this.up('form').getForm();
	                    	if (form.isValid()){
          						acs_show_win_std('Report fornitore',
           					    'acs_sollecito_forniture_report.php?fn=open_form',
           					 	{ filter: form.getValues() },
           					 	400, 150, {}, 'icon-print-16');
       					 	}	
	          	                	                
	            }
	        } ],             
            
            items: [
	            
	             {
					xtype: 'fieldset',
					layout: 'anchor',
					
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
	             			{
							name: 'f_areaspe',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-5',
						    margin: "10 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 
								    ] 
							}						 
						}, 
						  	{
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-5',
						    margin: "15 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?>	
								    ] 
							},  
							
							listeners: {
            					
            					change: function(field,newVal) {
	            				var form = this.up('form').getForm();
	            				form.findField('f_tipologia_ordine').store.proxy.extraParams.divisione = newVal;
	            				form.findField('f_tipologia_ordine').store.load();
	            				form.findField('f_stato_ordine').store.proxy.extraParams.divisione = newVal;
	            				form.findField('f_stato_ordine').store.load();
	            				//form.findField('f_stato_fornitore').store.proxy.extraParams.divisione = newVal;
	            				//form.findField('f_stato_fornitore').store.load();
	            				//form.findField('f_fornitore').store.proxy.extraParams.divisione = newVal;
	            				//form.findField('f_fornitore').store.load();
	            					}
         				}					 
						}, 
						{
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							 margin: "20 10 10 10",	
							 anchor: '-5',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
						    store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tipologia',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								},
								   extraParams: {
        		  					divisione: ''
        		 				},
        		 					doRequest: personalizza_extraParams_to_jsonData        		
							},
							
							fields: [{name:'id'}, {name:'text'}],
							}						 
							}, {
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							anchor: '-5',
							margin: "20 10 10 10",
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_stato_ordine',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								},
								   extraParams: {
        		  					divisione: ''
        		 				},
        		 					doRequest: personalizza_extraParams_to_jsonData        		
							},
							
							fields: [{name:'id'}, {name:'text'}],
							}					 
							},
							
							 {
					        name: 'f_stato_fornitore',
							xtype: 'combo',
							fieldLabel: 'Stato fornitore',
							displayField: 'RDRIFE',
							valueField: 'RDRIFE',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: false,		
						    anchor: '-5',
						    multiSelect: true,
						    margin: "20 10 5 10",				
					    	store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_stato',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								},
								   extraParams: {
        		  					
        		  					divisione: ''
        		 				}
							},
							
							fields: ['RDRIFE'],
										
										
						}, //store
						 listeners: {
            					afterrender: function (combo) {
            				      var cmbStore = combo.store;
							      cmbStore.on('load', function() {
							      var values = ['Da ordinare', 'Critico', 'Mancante'];
   							      combo.setValue(values)
								})
            					},
            					change: function(field,newVal) {
	            				var form = this.up('form').getForm();
	            				form.findField('f_fornitore').store.proxy.extraParams.stato_forn = newVal;
	            				form.findField('f_fornitore').store.load();
	            					}
         				}
				
					 
					}	,

                           {
							name: 'f_fornitore',
							xtype: 'combo',
							fieldLabel: 'Fornitore',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-5',
						    margin: "20 10 10 10",						   													
							store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_elenco_fornitori',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								},
								   extraParams: {
        		  					stato_forn: ''
        		  				
        		 				},
        		 					doRequest: personalizza_extraParams_to_jsonData        		
							},
							
							fields: [{name:'id'}, {name:'text'}],
							}						 
						},
					{
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Hold:',
		                    	width: 150
		                    }, {
			                boxLabel: 'Inclusi',
			                name: 'f_hold',
			                checked: true,			                
			                inputValue: 'A'
			            }, {
			                boxLabel: 'Esclusi',
			                checked: false,			                
			                name: 'f_hold',
			                inputValue: 'Y',
			                width: 95, labelWidth: 85
			            }, {
			                boxLabel: 'Solo',
			                checked: false,			                
			                name: 'f_hold',
			                inputValue: 'N'		                
			            } ]
			        }	, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,	
					    defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Indice di produzione',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'f_indice_rottura_assegnato',
			                checked: true,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Con indice',
			                checked: false,			                
			                name: 'f_indice_rottura_assegnato',
			                inputValue: 'Y',
			                width: 95, labelWidth: 85
			            }, {
			                boxLabel: 'Senza',
			                checked: false,			                
			                name: 'f_indice_rottura_assegnato',
			                inputValue: 'N'		                
			            }, {
							name: 'f_indice_rottura',
							xtype: 'textfield',
							fieldLabel: 'Indice',
							value: '',
			                width: 130, labelWidth: 60														
						 } ]
			        }, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,	
					    defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Con ordine abbinato',
		                    	width: 150
		                    }, {
			                boxLabel: 'Si',
			                name: 'f_ordine_ab',
			                checked: false,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Solo',
			                checked: false,			                
			                name: 'f_ordine_ab',
			                inputValue: 'Y',
			                width: 95, labelWidth: 85
			            }, {
			                boxLabel: 'Escludi',
			                checked: true,			                
			                name: 'f_ordine_ab',
			                inputValue: 'N'		                
			            }]
			        }, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,	
					    defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Avanzamento',
		                    	width: 150
		                    }, {
							name: 'f_avan',
							xtype: 'textfield',
							fieldLabel: 'Codice',
							value: '',
			                width: 145, labelWidth: 60														
						 } ]
			        }
			       	
	             
	             ]
	             },                                     
            ]
        }

	
]}

<?php	
 exit;	
}




if ($_REQUEST['fn'] == 'get_data_grid'){

	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;
	
	 $sql_where=parametri_sql_where($form_values);
	 

	 $sql = "SELECT *
	 FROM {$cfg_mod_Spedizioni['file_righe']}
	 INNER JOIN {$cfg_mod_Spedizioni['file_testate']}
	 ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO AND TDPROG=RDPROG
	 {$sql_where} AND RDTPNO = 'MTO' ";
	 
	/*print_r($sql);
	  exit;*/
	 
	 $stmt = db2_prepare($conn, $sql);
	 echo db2_stmt_errormsg();
	 $result = db2_execute($stmt);
	 
	 $data = array();
	 
	 while ($row = db2_fetch_assoc($stmt)) {
	 	//if (strlen(trim($row['RDDES1'])) > 0)
	 	//	$row['RDDES1'] = trim($row['RDDES1']) . "<br>" . trim($row['RDDES2']);
	 	$row['RDDES1'] = acs_u8e($row['RDDES1']);
	 	$row['RDDART'] = acs_u8e($row['RDDES1']);
	 	$row['k_ordine']=$row['TDDOCU'];  
	 	$row['ord']=implode("_", array($row['TDOADO'], $row['TDONDO']));
	 	//Da TDDOCU ricavo ditta origine
	 	$ordine_exp = $s->k_ordine_td_decode_xx($row['TDDOCU']);
	 	$row['ditta_origine'] = $ordine_exp['TDDT'];
	 	$row["raggr"] =  $row['TDCLOR'];
	 	$row['qtip_tipo'] = acs_u8e($row['TDDOTD']);
	 	$row['stato']=$row['TDSTAT'];
	 	$row['qtip_stato'] = acs_u8e($row['TDDSST']);
	 
	 	$data[] = $row;
	 }
	 		
	 echo acs_je($data);
	
	 exit();
				
}


$m_params = acs_m_params_json_decode();


$cod_forn= $m_params->form_values->f_fornitore;

$sql_desc_forn = "SELECT RDDFOR
FROM {$cfg_mod_Spedizioni['file_righe']}
WHERE RDFORN = '{$cod_forn}' FETCH FIRST 1 ROWS ONLY";


$stmt_desc_forn = db2_prepare($conn, $sql_desc_forn);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_desc_forn);

$row_desc_forn = db2_fetch_assoc($stmt_desc_forn);

$denom_forn= $row_desc_forn['RDDFOR'];

$sql_check_forn = "SELECT COUNT(*) AS C_RIGHE FROM {$cfg_mod_Spedizioni['file_tabelle']} TA_OACQF
WHERE TA_OACQF.TATAID = 'OACQF' AND TA_OACQF.TAKEY1='{$cod_forn}'";

$stmt_check_forn = db2_prepare($conn, $sql_check_forn);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_check_forn);
 
$row_check_forn = db2_fetch_assoc($stmt_check_forn);
$row_check_forn['C_RIGHE']= $row_check_forn['C_RIGHE'];
 


			?>
				
{"success":true, "items": [

        {
        
        xtype: 'grid',
        title: 'Reminder',
        selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},
        <?php echo make_tab_closable(); ?>,
		multiSelect: true,
		tbar: new Ext.Toolbar({
	            items:['<b>Segnalazione articoli critici - Fornitore: <?php echo $denom_forn; ?> </b> ' , '->'
	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
      
					store: {
						
						xtype: 'store',
						autoLoad:true,
					    groupField: 'RDTPNO',			
										        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'ditta_origine', 'TDDTEP', 'RDFMAN', 'RDDTDS', 'RDDFOR', 'RDDTEP', 'RDTPNO',
		            			 'RDOADO', 'RDONDO', 'RDRIFE', 'RDART', 'RDDART', 'RDDES1', 'RDDES2', 'RDUM', 
		            			 'RDDT', {name: 'RDQTA', type: 'float'}, 'k_ordine', 'ord', 'TDDCON', 'TDVSRF', 'RDPMTO',
		            			 'TDOTPD', 'raggr', 'qtip_tipo', 'stato', 'qtip_stato'
		        			]
		    			},
		    		
			        columns: [
			       		 {
			                header   : 'Stato',
			                dataIndex: 'RDRIFE', 
			                width     : 95
			             },
			             { 
			                header   : 'Ordine',
			                dataIndex: 'ord', 
			                width     : 100
			             },
			              {
			                header   : 'Cliente',
			                dataIndex: 'TDDCON', 
			                flex    : 130
			             }, {
			                header   : 'Riferimento',
			                dataIndex: 'TDVSRF', 
			                flex    : 80
			             }, {
			                header   : 'Descrizione',
			                dataIndex: 'RDDES1', 
			                flex    : 150,
			                renderer: function(value, p, record){
			                	return record.get('RDDES1') + " [" + record.get('RDART') + "]"
			    			}			                			                
			             },  
			              { header: 'Tp',
			          		width: 30, 
			          		dataIndex: 'TDOTPD', 
			          		tdCls: 'tipoOrd', 
	        	     		renderer: function (value, metaData, record, row, col, store, gridView){						
							metaData.tdCls += ' ' + record.get('raggr');
								if (record.get('qtip_tipo') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '"';								
																			
							return value;			    
	    			}},   {header: 'St', 			width: 30, dataIndex: 'stato',
	    			    renderer: function(value, metaData, record){
	    			    
							if (record.get('qtip_stato') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_stato')) + '"';	    			    
	    			    
							if (value == 'V') return '';
							if (value == 'R') return '';
							if (value == 'G') return '';
							if (value == 'B') return '';
							return value;
	    			    }	    	     
	    	    },{
			                header   : 'Um',
			                dataIndex: 'RDUM', 
			                width    : 30
			             }, {
			                header   : 'Q.t&agrave;',
			                dataIndex: 'RDQTA', 
			                width    : 50,
			                align: 'right', renderer: floatRenderer2
			             }, {
			                header   : 'Consegna',
			                dataIndex: 'RDDTEP', 
			                flex    : 40,
			                renderer: date_from_AS
			             },{
			                header   : 'Disp. Sped.',
			                dataIndex: 'RDDTDS', 
			                width     : 70,
			                renderer: date_from_AS
			             }, {
			                header   : 'Documento',
			                dataIndex: 'RDDES2', 
			                flex    : 60,
			             /*  renderer: function(value, p, record){
			                	if (record.get('RDDES2').trim()!='') {//replace this with your logic.
			                	
               				 return '';
            			       }
			    			}*/	
			             }
			            
			         ],  

						listeners: {
						
						
						beforeselect: function(grid, record, index, eOpts) {
            			if (record.get('RDDES2').trim()!='') {//replace this with your logic.
               				 return false;
            			}
       				 },
			        	
							  celldblclick: {								
								  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){									  
								  	rec = iView.getRecord(iRowEl);								  	
								  	show_win_disponibilita(rec.get('ditta_origine'), rec.get('RDART'), rec, rec.get('RDTPNO'));
								  }
							  }	  
					}, viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           if (record.get('RDFMAN')=='M')
					           		return ' segnala_riga_rosso';
					           if (record.get('RDFMAN')=='C')
					           		return ' segnala_riga_giallo';				           		
					           if (record.get('RDDTDS')==record.get('TDDTEP'))
					           		return ' segnala_riga_viola';	
					           if (record.get('RDDES2').trim()!='')
					           		return ' segnala_riga_disabled';				           		
					           return '';																
					         }   
					    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [ '->',
                
                { xtype: 'button',
               <?php 
                if($row_check_forn['C_RIGHE'] == 0) {?>
                 disabled:true,
                 tooltip: 'Fornitore non abilitato all\'annullamento',
                 <?php }?>
                  text: 'Annulla richiesta fornitura',
		           iconCls: 'icon-sub_red_delete-32',
		           scale: 'large',	                     
		           handler: function() {
		              
               			 var ord = this.up('grid');
                            
                           ordine_selected = ord.getSelectionModel().getSelection();
                            list_selected_ord = [];
                            
                              for (var i=0; i<ordine_selected.length; i++) {
				            list_selected_ord.push({
				            	k_ordine: ordine_selected[i].get('k_ordine'),
				            	k_prog_MTO: ordine_selected[i].get('RDPMTO'),
				            	tipo_op: 'ANN'
				            	});}
				            
				            if (list_selected_ord.length == 0){
									  acs_show_msg_error('Selezionare almeno un ordine');
									  return false;
								  }
								  
								  
								   
																			   Ext.Msg.confirm('Richiesta conferma', 'Confermi ANNULLAMENTO richiesta ordine a fornitore?', function(btn, text){																							    
																			   if (btn == 'yes'){																	         	
																	         	
																				Ext.Ajax.request({
																				   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_fornitura',
																				   method: 'POST',
																				   jsonData: {
																				   	all_ord: list_selected_ord
																				   }, 
																				   
																				   success: function(response, opts) {
																				   	
																					   ord.close(); 				                      
																				   }, 
																				   failure: function(response, opts) {
																				    
																				      Ext.Msg.alert('Message', 'No data to be loaded');
																				   }
																				});						         	
																	         	
																				}
																			   });  
								 
	        						
			
			            }

			     },
			     
			     {
                     xtype: 'button',
                <?php 
                 if($row_check_forn['C_RIGHE'] == 0) {?>
                 disabled:true,
                 tooltip: 'Fornitore non abilitato alla conferma ordine',
                 <?php }?>
					 text: 'Conferma fornitura',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		              
                   var ord = this.up('grid');
                            
                           ordine_selected = ord.getSelectionModel().getSelection();
                            list_selected_ord = [];
                            
                            for (var i=0; i<ordine_selected.length; i++) {
				            list_selected_ord.push({
				            	k_ordine: ordine_selected[i].get('k_ordine'),
				            	k_prog_MTO: ordine_selected[i].get('RDPMTO'),
				            	tipo_op: 'CON'
				            	});}
				            
				            if (list_selected_ord.length == 0){
									  acs_show_msg_error('Selezionare almeno un ordine');
									  return false;
								  }
								  
								  
								   Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_fornitura',
									        method     : 'POST',
						        			jsonData: {
						        				all_ord: list_selected_ord
											},							        
									        success : function(result, request){
									        ord.close(); 	
						            			//ord.getStore().load();									        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
			
			            }

			     }]
		   }]
         
	        																			  			
	            
        }  

]
}


