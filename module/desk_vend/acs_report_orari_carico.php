<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'open_form'){
    
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	         
						 {
							name: 'f_area',
							xtype: 'combo',
							fieldLabel: 'Area di spedizione',
							forceSelection: true,								
							displayField: 'text',
							valueField: 'id',
							multiSelect : true, 
							labelWidth : 120,								
							emptyText: '- seleziona -',
					   		queryMode: 'local',
                 		    minChars: 1, 	
            				anchor: '-15',
            				width : 150	,				
							store: {
								editable: false,
								autoDestroy: true,
							    fields: [{name:'id'}, {name:'text'}],
							     data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 
								    ] 
							}
							,listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
	            				 }
	            				 
	         				 }
							
					 },
					  
	            ],
	            
				buttons: [	
						{
		        		  text: 'Report',
			              scale: 'large',
			              iconCls: 'icon-print-32',
			              handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	data : <?php echo j($m_params->data); ?>
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
		        		}
		       ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}

// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff; font-weight: bold;}   
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
    
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {
        border-collapse: unset;
	     	
    }
        .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 


<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);
$data_evasione = $_REQUEST['data']; 
$sql_where = " AND TDDTSP = {$data_evasione}";
$sql_where.= sql_where_by_combo_value('TDASPE', $form_values->f_area);


$sql = "SELECT TD.*, SP.*, TA_ITIN.TADESC AS ITIN, TA_TRAS.TADESC AS TRASP, TA_VETT.TADESC AS VETTORE, TA_AUTO.TADESC AS MEZZO
        FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
        INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
            ON TDDT = SP.CSDT AND TDNBOF = SP.CSPROG AND SP.CSCALE = '*SPR'
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
            ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_AUTO
			ON TA_AUTO.TADT = SP.CSDT AND TA_AUTO.TAKEY1 = SP.CSCAUT AND TA_AUTO.TATAID = 'AUTO'        
        WHERE " . $main_module->get_where_std() . " {$sql_where}
        ORDER BY TDASPE, CSDTIC, CSHMIC, TDNRCA";


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);

$ar = array();
while ($row = db2_fetch_assoc($stmt)) {
     
    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    $cod_liv0 = $row['TDASPE'];
    $cod_liv1 = $row['TDNBOF'];
    //$cod_liv2 = implode("_", array($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA']));  //carico  	
   //AREA
    $liv =$cod_liv0;
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] = $row['TDASPE'];
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    
    //SPEDIZIONE
    $liv=$cod_liv1;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new = $row;
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] =  $row['TDNBOF'];
       
        $ar_r["{$liv}"] = $ar_new;
        
    }
    
    $ar_r=&$ar_r[$liv];
   
    //CARICO
   /* $liv=$cod_liv1;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new = $row;
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] =  $row['TDNRCA'];
        
        $ar_r["{$liv}"] = $ar_new;
        
    }
    
    $ar_r=&$ar_r[$liv];*/
   
    
}//while


echo "<div id='my_content'>"; 


foreach($ar as $k => $v){
    $row_aspe = $main_module->get_TA_std('ASPE', trim($v['TDASPE']));
    $d_aspe = "[".trim($v['TDASPE'])."] ".trim($row_aspe['TADESC']);
    echo "<div class=header_page>";
    echo "<H2>Spedizioni del ".print_date($data_evasione)." {$d_aspe}</H2>";
    echo "</div>";
    
    echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";
    echo "<table class=int1>";
    
    echo "<tr class='liv_data'>
              <th>Itinerario</th>
              <th>Descrizione spedizione</th>
              <th>Data/ora carico</th>
              <th>Carico</th>
              <th>Trasportatore</th>
              <th>Mezzo</th>
              <th>Telefono</th>
              <th>ID Spedizione</th>";
    echo "</tr>";
 
    foreach($v['children'] as $k1 => $v1){ 
        $format_carico = array();
        $itinerario = "[".trim($v1['TDCITI'])."] {$v1['ITIN']}";
        $spedizione = trim($v1['CSDESC']);
        if($v1['CSDTIC'] > 0 || $v1['CSDTIC'] > 0)
            $data_ora = print_date($v1['CSDTIC'])." ".print_ora(sprintf("%06s", $v1['CSHMIC']));
        else 
           $data_ora = print_date($v1['TDDTSP']);
        $carico = $main_module->get_el_carichi_by_TDNBOF($v1['TDNBOF']);
       // $carico = sprintf("%06s", $v1['TDNRCA']);
        $ar_carico = explode("," , $carico);
        foreach($ar_carico as $c){
            $c = sprintf("%06s", $c);
            if($c > 0){
                $c1 = substr($c, 0, 2);
                $c2 = substr($c, 2, 2);
                $c3 = substr($c, 4, 2);
                $format_carico[] = "{$c1}.{$c2}/<span style=\"font-size: 1.5em;\">{$c3}</span>"; 
            }
        }
        
        $carico = implode(',', $format_carico);
        if(trim($v1['TRASP']) == '' )
            $trasportatore = trim($v1['VETTORE']);
        else 
            $trasportatore = trim($v1['TRASP']);
     
        echo "<tr class=liv1>
                <td>{$itinerario}</td>
                <td>{$spedizione}</td>
                <td>{$data_ora}</td>
                <td>{$carico}</td>";
        echo "<td>{$trasportatore}</td>";
        echo "<td>{$v1['MEZZO']}</td>";
        echo "<td>{$v1['TDTEL']}</td>";
        echo "<td>{$v1['TDNBOF']}</td>";
        echo "</tr>";
    }
      
    echo "</table>";
    echo "<div class=\"page-break\"></div>";
}


echo "</div>";

 		
?>

 </body>
</html>


<?php
exit;
}
