<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$main_module = new Spedizioni();
//parametri del modulo (non legati al profilo)
$mod_js_parameters = $main_module->get_mod_parameters();


//funcioni generiche
switch($_REQUEST['fn']) {
	case "toggle_fl_solo_con_carico_assegnato":
		$s->toggle_fl_solo_con_carico_assegnato();
	break;
}

/* CAMPO DI CUI MOSTRARE IL DETTAGLIO */
$s->set_calendar_field_dett($_REQUEST['set_field']); //se mi chiede di cambiare campo di dettaglio
$field_dett = $s->imposta_field_dett(); //recupero il campo di dettaglio

/* DATE CALENDARIO */
$n_giorni 	= 14;
$s->set_initial_calendar_date($_REQUEST['move']); //se si muove sul calendario
$da_data_txt = $s->get_initial_calendar_date(); //recupero data iniziale calendario
$a_data_txt = date('Y-m-d', strtotime($da_data_txt . " +{$n_giorni} days"));
$da_data 	= new DateTime($da_data_txt);
$a_data 	= new DateTime($a_data_txt);

$root_id = $_REQUEST["node"]; //radice o sottonodo?
$its = $s->get_elenco_itinerari($da_data, $n_giorni, $root_id, $field_dett);
$appLog->add_bp('get_elenco_itinerari - END');

$ars_ar = $s->crea_array_valori($its, $field_dett, $root_id);
$appLog->add_bp('crea_array_valori - END');

$ars = $s->crea_alberatura_calendario_json($ars_ar, $field_dett, $root_id, $da_data_txt, $n_giorni);
$appLog->add_bp('crea_alberatura - END');

//output del risultato	
echo $ars;

$appLog->save_db();
?>
