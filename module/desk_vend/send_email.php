<?php

require_once("../../config.inc.php");
require_once('utility/PHPMailer-master/PHPMailerAutoload.php');

$mail = prepare_new_mail();
$from = trim($auth->get_email());

//Gestione indirizzi multipli, seprarati da ";"
foreach (explode(';', $_REQUEST['to']) as $addr){
  if (strlen(trim($addr)) > 0)
	$mail->addAddress(trim($addr));	
}


//bug passaggio parametri linux
if ($is_linux == 'Y')
    $_REQUEST['f_text'] = strtr($_REQUEST['f_text'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
    

$mail->Subject = $_REQUEST['f_oggetto'];
$mail->ConfirmReadingTo = $from;

// emogrifier function
$xmldoc = new DOMDocument();
$xmldoc->strictErrorChecking = false;
$xmldoc->formatOutput = true;
$xmldoc->loadHTML(UTF8_DECODE($_REQUEST['f_text']));
$xmldoc->normalizeDocument();

// need to check all objects exist
$head = $xmldoc->documentElement->getElementsByTagName('body');

if ($head->length > 0) {
	$style = $head->item(0)->getElementsByTagName('style');

	if ($style->length > 0) {
		$style = $head->item(0)->removeChild($style->item(0));

		$css = trim($style->nodeValue);
		$html = $xmldoc->saveHTML();
		
		$body = acs_emogrify($html, $css);
	}
}
// end emogrifier function

$mail->Body = $body;
$mail->AltBody = 'Questa applicazione mail non supporta il linguaggio html';

$ret = array();
if(!$mail->send()){
	$ret['success'] = false;
	$ret['error_message'] = 'Errore: ' . $mail->ErrorInfo;
}else
	$ret['success'] = true;

echo acs_je($ret);




exit;
//*********************************************************************************
// con mail()
//*********************************************************************************

	$clrf = "\r\n";
 
	require_once("../../config.inc.php");
	
	$main_module = new Spedizioni();	

	$from = trim($auth->get_email());

	$to = trim($_REQUEST['to']) . " <" .trim($_REQUEST['to']) . ">";
	$subject = $_REQUEST['f_oggetto'];
	$message = "<html>" . $_REQUEST['f_text'] . "</html>";
	$headers = "From: {$from} <{$from}>" . $clrf;
	$headers .= "Reply-To: {$_REQUEST['to']} <{$_REQUEST['to']}>" . $clrf;	
	
	// To send HTML mail, the Content-type header must be set
	$headers .= 'MIME-Version: 1.0' . $clrf;
	$headers .= 'X-Mailer: PHP/' . phpversion()  . $clrf;
	
	//richiesta conferma di lettura
	$headers .= "Disposition-Notification-To: {$from}" . $clrf;
	
	//richiesta di recapito (sembra non funzionare)
	$headers .= "Return-Receipt-To: {$from}" . $clrf;

	$headers .= 'Content-type: text/html; charset=UTF-8';	
	
	//invio e-mail
	mail($to, $subject, $message, $headers);
	

 $ret = array();
 $ret['success'] = true;
 echo acs_je($ret);

?>
