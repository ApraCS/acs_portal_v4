<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$sql_where = '';
$parametri = array();
$date = oggi_AS_date();

if (isset($_REQUEST['f_spedizione'])){
	$sql_where .= " AND TA.TATISP = ?";
	$parametri[] = $_REQUEST['f_spedizione'];
}
if (isset($_REQUEST['f_trasporto'])){
	$sql_where .= " AND TA.TATITR = ?";
	$parametri[] = $_REQUEST['f_trasporto'];
}
if (strlen($_REQUEST['f_itinerario']) > 0){
	$sql_where .= " AND TA.TAKEY1 = ?";
	$parametri[] = $_REQUEST['f_itinerario'];
}

if (isset($_REQUEST['f_trasportatore']) > 0){
	$sql_where .= " AND TA_VETT.TACOGE = ?";
	$parametri[] = $_REQUEST['f_trasportatore'];
}

$sql = "SELECT TA.*
		FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
        	ON TA_VETT.TADT = TA.TADT AND TA_VETT.TAKEY1 = TA.TAKEY2 AND TA_VETT.TATAID = 'AUTR'
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
            ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'                                  
		WHERE 1 = 1
		AND TA.TATAID = 'ITVE'
		AND TA.TADT = '{$id_ditta_default}'
		$sql_where
		ORDER BY TA.TATISP, TA.TATITR	
		";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, $parametri);
		
?>

<html>
<head>
<style type="text/css">

        table.no_tabella{border:none;}
        table.no_tabella th{background: none; text-align: left;}        
        table{border-collapse:collapse; width:100%;}
        td{padding: 0 3px 0 3px;}
        th{background: #D3D3D3;} 
        tr.titoli th{text-align: center; white-space:nowrap; border: 1px solid #000000;}
        .titoli td{white-space:nowrap;}
        .tipologia {font-size: 28;}
        span.quantita{width:120px; display: inline-block;}
        span.denominazione{width:280px; display: inline-block;}
        span.trasportatore{width:650px; display: inline-block;}
        .numeri{text-align: right;}
        .title{font-size: 36;}
        .errori{color: #FF4500;}
        tr.righe td{background: #DCDCDC;}
        
        @media print 
		{
	    	.noPrint{display:none;}
		}    
		
</style>

<title>
Configurazione programmazione trasporti
</title>

</head>

<body>

<div class = "page-utility noPrint" >
	<A HREF="javascript:window.print()"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>	
	<A HREF="#" onclick="window.open('data:application/vnd.ms-excel,' + '<html><head>' + escape(document.getElementsByTagName('style')[0].outerHTML) + '</head>' + '<body>' + escape(document.getElementById('my_content').outerHTML) + '</body></html>');"><img src=<?php echo img_path("icone/48x48/save.png") ?>></A>
</div>

<div id='my_content'>

<b class = "title"> Configurazione programmazione trasporti </b>

<br>
<br>

<?php

$count = 0;
$var_itin = '';
$var_trasp = '';
$trasp = new Trasportatori();

		
while ($r = db2_fetch_assoc($stmt)) {

$id1_vett = $r["TATISP"];
$id1_tras = $r["TATITR"];
if($count == 0)
{
	echo creazione_anagrafica($r);
	$id2_vett = $r["TATISP"];
	$id2_tras = $r["TATITR"];
	$count++;
}

if(($id1_vett != $id2_vett)||($id1_tras != $id2_tras))
{
	echo "</table> <br> <table>";	
	echo creazione_anagrafica($r);
	$contatore = 0;
	$conta_riga = 0;
}

$id2_vett = $r["TATISP"];
$id2_tras = $r["TATITR"];
		
if($id1_vett == $id2_vett && $id1_tras == $id2_tras)
{	
	echo creazione_tabella($r, $var_itin, $var_trasp, $contatore, $conta_riga);
		
	$var_itin = $r["TAKEY1"];	
	$trasp->load_rec_data_by_vettore(trim($r['TAKEY2']));
	$var_trasp = $trasp->rec_data['TAKEY1'];
	$contatore++;
	$conta_riga++;
	
}

} 

echo "</table> </div> </body>"; 


function creazione_anagrafica($r){
	global $s;
	$ana = "<table class = \"no_tabella \">
			
			<tr>
			<td  width = \"30%\"> Tipologia Spedizione </td>
			<td> Tipologia Trasporto </td>
			</tr>
		
			<tr>
			<th class = \"tipologia \" width = \"30%\" > [" . trim($r["TATISP"]) . "] " . $s->decod_std('TSPE', $r["TATISP"]) . " </th>
			<th class = \"tipologia \"> [" . trim($r["TATITR"]) . "] "  . $s->decod_std('TTRA', $r["TATITR"]) . " </th>
			</tr>		
		
			</table>
			
			<br>
			
			<table>
	
			<tr class = \"titoli \">
			<th colspan=\"2\"> Itineraio </th>
			<th> Sequenza </th>
			<th colspan=\"2\"> Trasportatore </th>
			<th colspan=\"2\"> Vettore </th>
			<th colspan=\"2\"> Mezzo </th>
			<th> Volume </th>
			<th> Peso </th>
			<th> Pallet </th>
			</tr>";
	
	return $ana;
}

function creazione_tabella($r, $var_itin, $var_trasp, $contatore, $conta_riga){
	global $s;
	$trasp = new Trasportatori();
	$trasp->load_rec_data_by_vettore(trim($r['TAKEY2']));
	
	// print itinerario
	if ($var_itin == $r["TAKEY1"] && $contatore != 0 && $_REQUEST['visualizza_itin_trasp'] != 'Y'){		
		$print_itin = "<td> </td>
					   <td> </td>";
	}else if ($r["TAKEY1"] == ''){ 
		$print_itin = "<td class =\"errori\"></td>
					   <td class =\"errori\">Itinerario n/d</td>";
	}else{
		$print_itin = "<td> ". $r["TAKEY1"] . "</td>
					   <td>" . $s->decod_std('ITIN', $r["TAKEY1"]) ." </td>";
	}	
			
	// print trasportatori
	if ($var_trasp == $trasp->rec_data['TAKEY1'] && $contatore != 0 && $_REQUEST['visualizza_itin_trasp'] != 'Y'){
		$print_trasp = "<td> </td>
						<td> </td>";
	}else if ($trasp->rec_data['TAKEY1'] == ''){
		$print_trasp = "<td class =\"errori\"></td>
						<td class =\"errori\">Trasportatore n/d</td>";
	}else{
		$print_trasp = "<td> ". $trasp->rec_data['TAKEY1'] . "</td>
						<td>" . $trasp->rec_data['TADESC'] ." </td>";
	}
	
	if (($conta_riga%2) == 1)
		$colorazione = "class =\"righe\" ";
	else
		$colorazione = "";
	
	$tab = "<tr " . $colorazione . ">" . $print_itin .
			"<td class = \"numeri\">" . $r["TASTAL"] ." </td>"        		
			. $print_trasp .        			
       		"<td>" . $r["TAKEY2"] ." </td>	        			
        	<td>" . $s->decod_std('AUTR', $r["TAKEY2"]) ." </td>
       		<td>" . $r["TAKEY3"] ." </td>	        			
        	<td>" . $s->decod_std('AUTO', $r["TAKEY3"]) ." </td>        		
        	<td class = \"numeri\">" . $r["TAVOLU"] ." </td>	
        	<td class = \"numeri\">" . $r["TAPESO"] ." </td>
        	<td class = \"numeri\">" . $r["TABANC"] ." </td>
			</tr>";

	return $tab;
}

?>

</div>
</body>
</html>