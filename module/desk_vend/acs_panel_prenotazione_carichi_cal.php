<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$all_params = array();
$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());


if ((int)$all_params['data_carico'] == 0)
	$data = $all_params['data_spedizione'];
else $data = $all_params['data_carico'];




function to_AS_date_from_format($format, $data){
	switch ($format){
		case "m-d-Y":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[0], $d_ar[1], $d_ar[2]);
			return to_AS_date($d);
		case "Y-m-d":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[1], $d_ar[2], $d_ar[0]);
			return to_AS_date($d);
			
	}
}


function decodifica_data($calendar_datatime){
	$data_ora = explode("T", $calendar_datatime);
	$data_t = $data_ora[0] . '';	
	$data_ext = explode("-", $data_t);
	
	return implode("", $data_ext);
}

function decodifica_ora($calendar_datatime){
	$data_ora = explode("T", $calendar_datatime);
	$ora_t = $data_ora[1] . '';
	$ora = substr($ora_t, 0, 2) . substr($ora_t, 3, 2) . substr($ora_t, 6, 2);	
	return $ora;
}



// ******************************************************************************************
// EDIT EVENT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'edit_event'){
	?>
	
{"success":true, "items": [

	{
		xtype: 'form',
		layout: {
                type: 'vbox',
                align: 'stretch'
            },
		flex: 1, frame: true,
		padding:    '10 10 10 10',
		margins:    '10 10 10 10',
		items: [{
		            xtype: 'textfield',
		            disabled: true,
		            itemId: this.id + '-title',
		            name: Extensible.calendar.data.EventMappings.Title.name,
		            fieldLabel:'Prenotazione',
		            anchor: '100%',
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        },{
		            xtype: 'extensible.daterangefield',
		            itemId: this.id + '-dates',
		            name: 'dates',
		            anchor: '95%',
		            singleLine: true,
		            fieldLabel: 'Orario',
		            showAllDay: false,
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        },{
		            xtype: 'combo',	                     
					itemId: 'risorsa',
	                name: Extensible.calendar.data.EventMappings.Risorsa.name,				
					fieldLabel: 'Risorsa',
					forceSelection: true,
	                anchor: '100%',	
	                delimiter: ' , ', 
	                editable: false, 
	                multiSelect: true,                 
	                
					store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [<?php echo acs_ar_to_select_json($main_module->find_TA_std('RISEN'), ""); ?>]
					},
	            
					valueField: 'id',                       
		            displayField: 'text',
		            
		            padding:    '10 0 10 0',
					margins:    '0 0 0 0'				
		        }],
		        
		buttons: [
			{
                itemId: 'delete-btn',
                iconCls: 'icon-delete',                
                text: 'Elimina prenotazione',
                disabled: false,
                handler: function() {
                	rec = this.up('window').open_vars.record;                	                	
					rec.store.remove(rec);

                            		//aggiorno calendario attuale
                            		this.up('window').open_vars.from_comp.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = this.up('window').open_vars.main_tab_panel.down('grid');												
									grid_sped.getStore().reload();				                    	
						
									this.up('window').close();
                	
                },
                //scope: this,
                minWidth: 150,
                hideMode: 'offsets'
            },
            {
                text: 'Salva',
                iconCls: 'icon-save',
                disabled: false,
                handler: function(){
                	rec = this.up('window').open_vars.record; 
			        var fields = rec.fields,
			            values = this.up('form').getForm().getValues(),
			            name,
			            M = Extensible.calendar.data.EventMappings,			            
			            obj = {};
			
			        fields.each(function(f) {
			            name = f.name;
			            if (name in values) {
			                obj[name] = values[name];
			            }
			        });
			        
			        rangefields = this.up('form').query('[xtype="extensible.daterangefield"]');
			        Ext.each(rangefields, function(rf) {
				        var dates = rf.getValue();
				        obj[M.StartDate.name] = dates[0];
				        obj[M.EndDate.name] = dates[1];
				        obj[M.IsAllDay.name] = dates[2];
				    });    
			        
			            rec.beginEdit();
        				rec.set(obj);
        				rec.endEdit();
        					
                            		//aggiorno calendario attuale
                            		this.up('window').open_vars.from_comp.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = this.up('window').open_vars.main_tab_panel.down('grid');												
									grid_sped.getStore().reload();				                    	
						
									this.up('window').close();
        					
                
                
                }
                //scope: this
            }
		],        
		        
		        
		listeners: {
				afterrender: function(comp){					
					console.log(comp.up('window'));
					rec = comp.up('window').open_vars.record;
					comp.loadRecord(rec);
					
					rangefields = comp.query('[xtype="extensible.daterangefield"]');
					Ext.each(rangefields, function(rf) {
						console.log(rf);
						
							//carico i valori per il datarange
							rf.setValue(rec.data);
							
							//disabilito i campi data (puo' modificare solo l'ora)
							Ext.each(rf.query('datefield'), function(dtrf) {
								dtrf.disable();
							});
							
					});
				 
				}
				
                , 'eventdelete': {
                        fn: function(win, rec){
                        	console.log('----- eventdelete ----');
                        	console.log(rec);
                            this.eventStore.remove(rec);
                            this.eventStore.sync();
                            win.hide();
                            this.showMsg('Event '+ rec.data.Title +' was deleted');
                        },
                        scope: this
                    }				
				
							 					 
		}		        
		        
	}

]}	
	
	
<?php	
	exit;
}



// ******************************************************************************************
// AGGIORNA PORTA/ORA SU SPEDIZIONE/STABILIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_sped_set_ora'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	$m_params = acs_m_params_json_decode();

	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDBCAR = ?, TDDTIC = ?, TDHMIC = ?, TDDTFC = ?, TDHMFC = ? 
			WHERE " . $main_module->get_where_std() . " AND TDNBOF = ? AND TDSTAB = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($m_params->porta,
							 $m_params->data, $m_params->ora,
							 $m_params->data, (int)$m_params->ora + 10000,
							 $m_params->sped_id, $m_params->c_stabilimento));
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// AGGIORNA DATA DI CARICO SU SPEDIZIONE/STABILIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_cambia_data_carico'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	$m_params = acs_m_params_json_decode();

	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDDTIC	 = ?, TDDTFC = ?, TDBCAR = '', TDHMIC = 0, TDHMFC = 0
			WHERE " . $main_module->get_where_std() . " AND TDNBOF = ? AND TDSTAB = ?";
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt, array( $m_params->to_date, $m_params->to_date,
												$m_params->record->TDNBOC, $m_params->record->TDSTAB));

			$ret['success'] = true;
	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// AGGIORNA EVENTO (resize, ....)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_update_event'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	$m_params = acs_m_params_json_decode();

//	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} SET CSHMPG = ?, CSHMFG = ? WHERE CSPROG = ? AND CSCALE = '*SPR'";
//	$stmt = db2_prepare($conn, $sql);
//	$result = db2_execute($stmt, array($m_params->start, $m_params->end, $m_params->rec->id));

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// CREA EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_crt_events'){
	global $cfg_mod_Spedizioni;
	$ret = array();

	//$all_params = ...;

	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
			SET  TDBCAR = ?, TDDTIC = ?, TDHMIC = ?, TDDTFC = ?, TDHMFC = ?
			WHERE TDNBOF = ? AND TDSTAB = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array(
			$all_params['Porta'], decodifica_data($all_params['StartDate']),decodifica_ora($all_params['StartDate']),
			decodifica_data($all_params['EndDate']),decodifica_ora($all_params['EndDate']),
			$all_params['CreatedBy'], $all_params['Stabilimento']));

	//registro attivita utente
		$au = new AttivitaUtente();
		$au->crea($main_module, 'PREN_CARICO', $id_ditta_default,
				array($all_params['CreatedBy'],  $all_params['c_trasp'], $all_params['Stabilimento'])				
		);	
	
			$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// AGGIORNA EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_upd_events'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	
	//$all_params = ...;	
	
	if (isset($all_params['f_porta']))
		$to_porta = sql_t($all_params['f_porta']);
	 else $to_porta = 'TDBCAR';;
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} 
				SET  TDBCAR = {$to_porta}, TDDTIC = ?, TDHMIC = ?, TDDTFC = ?, TDHMFC = ?
				WHERE TDNBOF = ? AND TDSTAB = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array( 
										decodifica_data($all_params['StartDate']),decodifica_ora($all_params['StartDate']),
										decodifica_data($all_params['EndDate']),decodifica_ora($all_params['EndDate']),
										$all_params['EventId'], $all_params['Stabilimento']));
										
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// DELETE EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_del_events'){
	global $cfg_mod_Spedizioni;
	$ret = array();
	$m_params = acs_m_params_json_decode();
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDBCAR = ?, TDDTIC = ?, TDHMIC=?, TDDTFC=?,TDHMFC =? 
				 WHERE TDNBOF = ? AND TDSTAB = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array('', 0, 0, 0, 0, $all_params['EventId'], $all_params['Stabilimento']));

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// EVENTI CALENDARIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_get_events'){
	global $conn;
	global $cfg_mod_Spedizioni;	
	
	
 $ret = array();
 $ret['events'] = array(); 
 
 //devo per forza aver specificato unaporta
 if (strlen($_REQUEST['f_porta']) == 0){
 	echo acs_je($ret); exit;
 } 

 	if (strlen($_REQUEST['f_porta']) == 0) $_REQUEST['f_porta'] = 'NNNNNNNNNNNNNNNN';
 
 	
 		//data iniziale e di fine
 			$from_calendar_format = 'Y-m-d'; 		
 			$d_start 	= to_AS_date_from_format($from_calendar_format, $_REQUEST["startDate"]);
 			$d_end 		= to_AS_date_from_format($from_calendar_format, $_REQUEST["endDate"]); 			

 	
 			$sql = "SELECT TA_TRAS.TAKEY1 AS C_TRASP, TA_TRAS.TADESC AS D_TRASP, TDNBOF, SP.CSCITI, SP.CSDTSC, SP.CSHMSC,
 					SP.CSCVET, SP.CSCAUT, SP.CSCCON,  
 					TDDTIC, TDHMIC, TDDTFC, TDHMFC, TDSTAB, TA_TRAS.TADESC AS D_TRASP, SUM(TDVOLU) AS S_TDVOLU, SUM(TDTOCO) AS T_TDTOCO
 					FROM {$cfg_mod_Spedizioni['file_testate']} TD
					INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				   		ON TDDT=SP.CSDT AND SP.CSPROG = TD.TDNBOF AND CSCALE = '*SPR'

					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
					   ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
					   ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'		   
				   
				   
					WHERE " . $main_module->get_where_std() . "
	 				AND TDDTIC >= {$d_start} AND TDDTIC <= {$d_end}
	 				AND TDSTAB = " . sql_t($_REQUEST['c_stabilimento']) . " AND TDBCAR = '{$_REQUEST['f_porta']}' AND TDHMIC > 0
	 				GROUP BY TA_TRAS.TAKEY1, TA_TRAS.TADESC, TDNBOF, TDDTIC, TDHMIC, TDDTFC, TDHMFC, TDSTAB, SP.CSCITI, SP.CSDTSC, SP.CSHMSC, TA_TRAS.TADESC,
	 						SP.CSCVET, SP.CSCAUT, SP.CSCCON
				";

 			$stmt = db2_prepare($conn, $sql);
 			echo db2_stmt_errormsg();
 			$result = db2_execute($stmt);
 
 			$somma_volume = 0;
 			$somma_colli  = 0;
 			while ($r = db2_fetch_assoc($stmt)) {

 				$e = array();
 					$e['id'] = (int)$r['TDNBOF']; //id calendario 					
 					$e['title'] = $main_module->decod_std('ITIN', $r['CSCITI']) . " [" . print_date($r['CSDTSC'], "%d/%m") . " " . print_ora($r['CSHMSC']) . "] " . $main_module->decod_vmc_by_sped_row($r) . " (#{$r['TDNBOF']})";
 					$e['start'] = print_date($r['TDDTIC'], '%Y-%m-%d') . " " . print_ora($r['TDHMIC']) . ":00";
 					$e['end'] 	= print_date($r['TDDTFC'], '%Y-%m-%d') . " " . print_ora($r['TDHMFC']) . ":00"; 					
 					$e['notes'] = "Have fun";
 					$e['risorsa'] = '';
 					$e['stabilimento'] = trim($r['TDSTAB']);
 					$c['trasp'] = trim($r['C_TRASP']); 	

 					if (strlen(trim($all_params['f_trasportatore'])) > 0){
 						//se sono entrato filtrando un trasportatore, se non e' mio
 						// l'evento non deve essere visibile (descrizione) e modificabile
 						if (trim($r['C_TRASP']) == $all_params['f_trasportatore'])
	 						$e['cid'] = 1; //id calendario
	 					else {
	 						$e['cid'] = 3;
	 						$e['title'] = "#{$r['TDNBOF']}";
	 					}	 						
 						
 					} else {
	 					if (trim($r['C_TRASP']) == $all_params['c_trasp'])
	 						$e['cid'] = 1; //id calendario
	 					else 
	 						$e['cid'] = 1; //2 (al momento non differenzio il mio trasp dagli altri, se posso gestirli)
 					}
 					
 					$ret['events'][] = $e;
 					
 			  $somma_volume += $r['S_TDVOLU']; 					
 			  $somma_colli  += $r['S_TDTOCO'];
 					
 			}
 
	$ret['totals']['TDVOLU'] = $somma_volume;
	$ret['totals']['TDTOCO'] = $somma_colli;
 
 echo acs_je($ret);	
 exit;	
} //fn=cal_get_events





// ******************************************************************************************
// ELENCO SPEDIZIONI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_spedizioni'){
	global $conn;
	global $cfg_mod_Spedizioni;
	
	$ret = array();
	
	
	//TODO: DRY******************************************
		//parametri dalla form di ingresso
		$form_ep = (object)$all_params['form_ep'];
		
		//data (NON DEVO FILTRARE PER DATA) PERCHE' LO PRENDO GIA' DAL RECORD DELLO STABILIMENTO
		
		//area spedizione
		$sql_form_where .= sql_where_by_combo_value('TA_ASPE.TAKEY1', $form_ep->f_area_spedizione);
		
		//tipologia trasporto (multipla)
		$sql_form_where .= sql_where_by_combo_value('SP.CSTITR', $form_ep->f_trasporto);
			
		//divisione
		$sql_form_where .= sql_where_by_combo_value('TDCDIV', $form_ep->f_divisione);
			
		//trasportatore
		$sql_form_where .= sql_where_by_combo_value('TA_TRAS.TAKEY1', $form_ep->f_trasportatore);
			
		//stabilimento
		$sql_form_where .= sql_where_by_combo_value('TDSTAB', $form_ep->f_stabilimento);

	//***********************************************************	
	
	
	
	
	$s_field = explode(" ", "CSCITI TDNBOF TDBCAR TDHMIC TDSTAB TDDTSP");
	$sql = "SELECT " . implode(", ", $s_field) . ", TA_TRAS.TADESC AS D_TRASP FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				   ON TDDT=SP.CSDT AND SP.CSPROG = TD.TDNBOF AND CSCALE = '*SPR'
				   
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
				ON TA_ASPE.TADT = TA_ITIN.TADT AND TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE
				   
				   
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			   ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			   ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'		   
				   
			WHERE " . $main_module->get_where_std() . "
			  AND (TDDTIC = {$data} OR (TDDTIC = 0 AND TDDTSP = {$data}))
			  /* AND TA_TRAS.TAKEY1 = " . sql_t($all_params['c_trasp']) . " */
			  AND TD.TDSTAB = " . sql_t($all_params['c_stabilimento']) . "
			  {$sql_form_where} 
			GROUP BY " . implode(", ", $s_field) . ", TA_TRAS.TADESC"; 

	
	$stmt = db2_prepare($conn, $sql);	
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while ($row = db2_fetch_assoc($stmt)) {
		$row['id']     = $row['TDNBOF'];
		$row['TDNBOC'] = $row['TDNBOF'];  //SI DEVE CONTINUARE A CHIAMARE TDNBOF ALTRIMENTI GLI EVENTI DEL CALENDARIO NON FUNZIONANO
		$row['CSPORT'] = $row['TDBCAR'];
		$row['CSHMPG'] = $row['TDHMIC'];
		$row['D_CSCITI'] = $main_module->decod_std('ITIN', $row['CSCITI']);
		$row['EL_CARICHI_D'] = $main_module->get_el_carichi_by_TDNBOF($row['TDNBOF'], 'TN', 'N', 'N', trim($all_params['c_stabilimento']));

		if ((int)$row['id'] == (int)$all_params['sped_id_selected'])
			$row['rowCls'] = 'selected';
					
		$ret[] = $row;
	}
	

	echo acs_je($ret);
	exit;
} //fn=cal_get_events



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
	
$panel_id = rand();

?>
{"success":true, "items":
 {
  xtype: 'panel',
  id: 'panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>',
  title: 'Upload <?php echo $all_params['c_stabilimento']; ?>',
  closable: false,
  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
  
  listeners: {                    
    afterrender: function (comp) {
                    		comp.show();
                 	},
  },
  
  
  items: [
  
  // ------------------------ menu laterale -----------------------------
  		, {
			xtype: 'panel',
			layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
			flex: 0.4,			
			items: [			
					
					//form filtri
					, {
						xtype: 'form',
						items: [

			
							 , {
								     name: 'f_data'
								   , disabled: false                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: '<b>Data di carico</b>'
								   , margins:    '10 10 10 10'
								   , padding:    '15 10 10 10'
								   , labelAlign: 'right'
								   , format: 'd/m/Y'						   						   
								   , submitFormat: 'Ymd'
								   , allowBlank: false
								   , anchor: '-15'
								   , value: '<?php echo print_date($data, "%d/%m/%Y"); ?>'
								   , listeners: {
								       		invalid: function (field, msg) {
								       			Ext.Msg.alert('', msg);
								       		},
								       		
								            change: function(){      
								                  		
								            	var form = this.up('form').getForm();
										
												if(form.isValid()){
												
													f_d = form.getValues()['f_data'];
													f_d_Y = f_d.substr(0, 4);
													f_d_M = f_d.substr(4, 2) - 1;
													f_d_D = f_d.substr(6, 2);																		
													var d1 = new Date(f_d_Y, f_d_M, f_d_D);
													var to_date = Ext.Date.format(d1, 'Ymd'),
												
													//sul giorno selezionato eseguo la procedura per associare
													// le porte e orari di default
													
													panel_main_fo_presca = this.up('#panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>');
													
											        Ext.Ajax.request({
											            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_porte_orari_default',
											            method: 'POST',
									        			jsonData: form.getValues(),						            
											            success: function ( result, request) {
											                var jsonData = Ext.decode(result.responseText);
										                    	
							                            	//aggiorno tutte
													        cal_porte = panel_main_fo_presca.query('[xtype="extensible.calendarpanel"]');	                    	
											                Ext.each(cal_porte, function(cal) {cal.setStartDate(d1);});
																
																//aggiorno elenco spedizioni del giorno selezionato
																grid_sped = panel_main_fo_presca.down('grid');
																grid_sped.getStore().proxy.extraParams.data_carico = to_date;
																grid_sped.getStore().reload();
											                
											                											                															
											            },
											            failure: function ( result, request) {
											            }
											        });									
													
				
											    }    								                  		
												                  		
								                  		            	
								            } //change															       		
								       		
									 },
									 	
								}			
			
							
						
						]
						
 	
					}
					
  
  					//----- elenco ricezioni ------------
  					, {					
						xtype: 'grid',
						flex: 1,
						//id: 'fo_presca_grid_spedizioni',
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},

	        			viewConfig: {
	        
				            plugins: {
				                ptype: 'gridviewdragdrop',
				                ddGroup: 'DayViewDD',
				                enableDrop: false
				            }
				            
				            
					       , getRowClass: function(record, index) {
					        	if (record.get('rowCls') == 'selected')
					        		return ' segnala_riga_giallo';															
					         }   				            
				            
				            
				        },
     					
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_spedizioni',
								method: 'POST',
								type: 'ajax',
								
								extraParams: <?php echo acs_raw_post_data() ?>,
								
									/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */					
						            doRequest: function(operation, callback, scope) {
						                var writer  = this.getWriter(),
						                    request = this.buildRequest(operation, callback, scope);
						
						                if (operation.allowWrite()) {
						                    request = writer.write(request);
						                }
						
						                Ext.apply(request, {
						                    headers       : this.headers,
						                    timeout       : this.timeout,
						                    scope         : this,
						                    callback      : this.createRequestCallback(request, operation, callback, scope),
						                    method        : this.getMethod(request),
						                    disableCaching: false // explicitly set it to false, ServerProxy handles caching
						                });
						
										/* extraParams lo passo come jsonData */
						                request.jsonData = this.extraParams;
						
						                Ext.Ajax.request(request);
						                return request;
						            },	   								
										
														
								
								
								
								

								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
					            						
							},
								
							fields: ['id', 'CSCITI', 'D_CSCITI', 'TDNBOC', 'TDSTAB', 'TDCCON', 'TDDCON', 'CSPORT', {name: 'CSHMPG', type: 'int'}, 'D_TRASP', 'TDDTSP', 'rowCls', 'EL_CARICHI_D']
										
										
						}, //store
						multiSelect: true,
						
					
						columns: [{header: 'Sped.', dataIndex: 'TDNBOC', width: 50, hidden: true},
								  {header: 'Itinerario', dataIndex: 'D_CSCITI', flex: 8},
								  {header: 'Carichi', dataIndex: 'EL_CARICHI_D', width: 60,
								      renderer: function(value, metaData, record){				    			    	
											metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';    			    	
				    			    	 	return value;				    			    	 			    	
				    			    	}
								  },
								  {header: 'Trasp.', dataIndex: 'D_TRASP', width: 65},
								  {header: 'Porta', dataIndex: 'CSPORT', width: 65},
								  {header: 'Ora', dataIndex: 'CSHMPG', width: 50, renderer: time_from_AS, align: 'right'}]
								  
					 , listeners: {
					 
					 
					    //per dd
					    render: function(grid){ 
							
										    grid.getDragData = function(e) {
										        console.log("dragging");
										        var target = Ext.get(e.getTarget());
										        thisProxy = Ext.ensible.cal._statusProxyInstance;
										        var record = this.getSelectionModel().getSelectedNode();
										        eventName = "plop"; /*we don't care*/
										        eventData = new Ext.ensible.cal.EventRecord();
										        eventStore = myCal.calendarStore; /*myCal is a CalendarPanel*/
										        eventRecord = new Ext.ensible.cal.EventRecord({
										            'EventId': 1500,
										            'cid': '',
										            'Title': eventName,
										            'StartDate': new Date(),
										            'EndDate': new Date(),
										            'notes': '',
										        });
										        eventStore.insert(0, eventRecord);
										        eventRecord = eventStore.getAt(0);
										        newDDel = document.createElement("div");
										        this.ddel.className = 'jibber-calendar-week-bd-evt-' + eventRecord.data['EventId'];
										        return {
										            ddel: this.ddel,
										            item: target,
										            proxy: thisProxy,
										            data: eventData,
										            type: 'eventdrag',
										        };
										    }								
							
					    	
					    }, //renderer
					    
					 
					 
					 
					 
					 

						itemcontextmenu : function(grid, rec, node, index, event) {
						  				  event.stopEvent();
									      var record = grid.getStore().getAt(index);		  
									      var voci_menu = [];
									      
									      voci_menu.push({
								      		text: 'Modifica data di carico',
								    		iconCls : 'icon-calendar-16',
								    		menu: new  Ext.create('Ext.menu.DatePicker', {
								    			
								    			maxDate: js_date_from_AS(rec.get('TDDTSP')),
								    			value: 	js_date_from_AS(rec.get('TDDTSP')),
								    			
											    handler: function(dp, date){
											        
											        
											        to_date = Ext.Date.format(date, 'Ymd'),
											        
											        panel_main_fo_presca = grid.up('#panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>');
											        
											        Ext.Ajax.request({
											            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cambia_data_carico',
											            method: 'POST',
									        			jsonData: {record: record.data, to_date: to_date}, 						            
											            success: function ( result, request) {
											                var jsonData = Ext.decode(result.responseText);
				
											                //aggiorno la vista
																							                            	//aggiorno tutte
													        	cal_porte = panel_main_fo_presca.query('[xtype="extensible.calendarpanel"]');	                    	
											                	Ext.each(cal_porte, function(cal) {cal.setStartDate(date);});
																
																
																//aggiorno elenco spedizioni del giorno selezionato
																grid_sped = panel_main_fo_presca.down('grid');
																grid_sped.getStore().proxy.extraParams.data_carico = to_date;
																grid_sped.getStore().reload();							                
											                
											                											                															
											            },
											            failure: function ( result, request) {
											            }
											        });
											    
											    
											        											
											    }
											})      		
	
										  });
				
									      var menu = new Ext.menu.Menu({
									            items: voci_menu
										}).showAt(event.xy);
								   }					 
					 	
					 	
					 }			  
						 
					} //menu laterale
  			]
  		}	
  
  
  
  // ------------------------ panel e calendario --------------------------------
  
	, {
	  xtype: 'panel', flex: 1,
	  title: <?php echo j("Programmazione porta/orario di carico per stabilimento/data di carico [Stabilimento " . $main_module->decod_std('START', $all_params['c_stabilimento']) . "]"); ?>,	  
	  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
	  cls: 'acs-light',	  	  	
	  
		tools: [{ 
		        qtip: 'Chiudi',
		        handler: function(e, toolEl, panel, tc){
		            m_tab = Ext.getCmp('panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>');
		            m_tab.close();
		        }
		    }],	  
	    
	  items: [  
  
  
	  	<?php
	  	//visualizzo il calendario per ogni porta 
	  	$el_porte = $main_module->get_elenco_porte_stabilimento($all_params['c_stabilimento']);
	  	foreach ($el_porte as $kp => $porta){  	
	  	?>
  
  
		 , {
		        xtype: 'extensible.calendarpanel',		        
		        flex: 1,
		        border: false,
		        frame: true,
		        title_initial: <?php echo j("Porta: " . $porta['text']); ?>,		        
				title: <?php echo j("Porta: " . $porta['text'] . "<div align=right>Mc: 0</div>"); ?>,
				
                collapsible: false,
                collapsed: false,	
                
				enableEditDetails: false,
				readOnly: false,                 
                
					viewConfig: {
				            // enable dd sharing with the grid
				            ddGroup: 'DayViewDD'
				        },                
                


        			//recupero eventi
					eventStore: new Extensible.calendar.data.EventStore({

					       autoLoad: true,
        				   autoSync: true,

		                    proxy: {
		                    	url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
		                        type: 'ajax',
								method: 'POST',		                        
		                        extraParams: {
		                        	f_porta: <?php echo j($porta['id']); ?>
		                          , c_stabilimento: <?php echo j($all_params['c_stabilimento']); ?>	 
		                          , c_trasp: <?php echo j($all_params['c_trasp']); ?>
		                          , f_trasportatore: <?php echo j($all_params['form_ep']->f_trasportatore); ?>		                          		                          
		                        },
		                        reader: {
            						type: 'json',		                        
		                            root: 'events'
		                        },
		                        
		                        
					            writer: {
					                type: 'json',
					                writeAllFields: true
					            },		
					            
					            api: {
					                read: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
					                create: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_crt_events',
					                update: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_upd_events',
					                destroy: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_del_events',
					            },
					            				                        
		                        
		                    }
		                    					          
					    }),


                    activeItem: 0, // day view 
                    
                    showNavBar: false,                      
                    
                    dayViewCfg: {
                        showHeader: false,
                        showWeekLinks: true,
                        showWeekNumbers: true,
						startDate: js_date_from_AS(<?php echo $data; ?>),
						viewStartHour  : 0,
						scrollStartHour: 6,
						ddGroup: 'DayViewDD',
						showTime: false,
						showTodayText: false,						
						showHourSeparator: true,
						hourHeight: 30							
                    },                      
			    
			    
			    
					//definizione calendari        
        			calendarStore: new Extensible.calendar.data.MemoryCalendarStore({
            			data: {
						    "calendars" : [{
						        "id"    : 1,
						        "title" : "Std",
						        "color" : 26
						    },{
						        "id"    : 2,
						        "title" : "non usato",
						        "color" : 2
						    },{
						        "id"    : 3,
						        "title" : "Non modificabili",
						        "color" : 22
						    },{
						        "id"    : 4,
						        "title" : "Altro",
						        "color" : 26
						    }]
						}
        			}),			
        			    
			    
                  listeners: {
                  
						'afterrender': function(comp){
							comp.store.on('load', function(store, records, options) {
							  comp.setTitle(comp.title_initial);
							  comp.setTitle(comp.title + '<div align=right>Mc: ' + floatRenderer2(store.proxy.reader.jsonData.totals.TDVOLU) + ', Colli: ' + floatRenderer0(store.proxy.reader.jsonData.totals.TDTOCO) + '</div>');
							}, comp);
						},                  
                  
                  
						'eventresize': {
                            fn: function(dd, rec, dt){
                            	console.log('eventresize');                            	
                            		//aggiorno calendario attuale
                            		this.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = dd.up('#panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>').down('grid');												
									grid_sped.getStore().reload();				                    	
                            	
                            }
                         }
                         
						, 'eventmove': {
                            fn: function(dd, rec, dt){
                            	console.log('eventmove');                            	
                            		//aggiorno calendario attuale
                            		this.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = dd.up('#panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>').down('grid');												
									grid_sped.getStore().reload();				                    	
                            	
                            }
                         }
                         
                         
					   , 'eventrdrag': {
                            fn: function(dd, rec, dt, a, b){
                            	console.log('------ eventrdrag ------');
                            }
                         }
                         
					   , 'eventadd': {
                            fn: function(dd, rec, dt, a, b){
                            	console.log('------ eventadd ------');
                            	
                           		//Aggiorno tutti i calendari
                            	//aggiorno tutte
						        cal_porte = this.up('#panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>').query('[xtype="extensible.calendarpanel"]');	                    	
				                Ext.each(cal_porte, function(cal) {cal.store.reload();});
                           		
												
								//aggiorno elenco spedizioni del giorno selezionato
								grid_sped = this.up('#panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>').down('grid');												
								grid_sped.getStore().reload();				                    	
                            	
                            }
                         }                         

                         
                        , 'eventclick': {
                			fn: function(panel, rec, el){                			
                			
		                		if (parseInt(rec.get(Extensible.calendar.data.EventMappings.CalendarId.name)) == 3)
                                	return false; //non posso spostare gli eventi non miei                			
                			
		                		acs_show_win_std('Modifica prenotazione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=edit_event', null, 680, 220, null, 'icon-inbox-16', null, null, {
		                			record: rec, 
		                			from_comp: panel,
		                			main_tab_panel: panel.up('#panel-prenotazione_carichi_cal_<?php echo $panel_id; ?>')
		                			});			                			                            	                           
                                return false; //per non attivare evento std
                            },
                            scope: this
                        }	
                         
                       //per prevenire la creazione di nuovi eventi  
					   , 'dayclick': {fn: function(dd, rec, dt, a, b){return false;}}
					   , 'rangeselect': {fn: function(dd, rec, dt, a, b){return false;}}
					   
					   //per calendario readonly
                        , 'beforeeventmove': {
                			fn: function(panel, rec, el){                			
		                		console.log('beforeeventmove');			                			                            	                           
		                		if (parseInt(rec.get(Extensible.calendar.data.EventMappings.CalendarId.name)) == 3)
                                	return false; //non posso spostare gli eventi non miei
                            },
                            scope: this
                        }
                        , 'beforeeventresize': {
                			fn: function(panel, rec, el){                			
		                		console.log('beforeeventresize');			                			                            	                           
		                		if (parseInt(rec.get(Extensible.calendar.data.EventMappings.CalendarId.name)) == 3)
                                	return false; //non posso spostare gli eventi non miei
                            },
                            scope: this
                        }                        					                                                    
                        
                                           	
                  },			    
			    
			    
			    
			    
		        
		        // this is a good idea since we are in a TabPanel and we don't want
		        // the user switching tabs on us while we are editing an event:
		        editModal: true
		    }
  

  		
  	<?php } //per ogni porta?>	
  		
  	  ]
    } //panel con calendari porte
  
  ]
  
  
  
 }
}