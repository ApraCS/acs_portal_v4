<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();
$s = new Spedizioni();
set_time_limit(120);

$m_params = acs_m_params_json_decode();	

$f8 = new F8();
$ar_parameters = array("op" => "M", "mod" => 'DESK_ACQ', "func" => 'PARAM_DISP_GIAC', "name" => '*DEF');

$data = $f8->get_memorizzazioni_data((object)$ar_parameters);
$param = $data[0]['DFMEMO'];

// ******************************************************************************************
// exe modifica punto riordino su articolo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'grid_conferma_ordine'){
	$m_params = acs_m_params_json_decode();	
?>

{"success":true, "items":

{
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    dockedItems: [{
			xtype: 'toolbar'
					, dock: 'bottom'
					, items: [    
					
					{
		                     xtype: 'button',
			            	 scale: 'large',
			            	 iconCls: 'icon-shopping_cart-32',
		                     text: 'Genera ordini',
					            handler: function() {
        			            
        			            	list_selected_id = [];
        							var grid = this.up('grid');
        							var m_win = this.up('window');
        							var mostra_errore = false;	 				
        							grid.getStore().each(function(rec){ 
        							  if(rec.get('ar_config_row') == '' && !Ext.isEmpty(rec.get('CONFIG'))) mostra_errore = true;
        							  if (rec.get('OLD_RIORD').trim() == '' && parseFloat(rec.get('QTA_RIORD')) > 0)
        							  	list_selected_id.push(rec.data); 
        							},this);
							
							         if(mostra_errore == true){
							         	acs_show_msg_error('Articolo da configurare');
						             }else{
						                
						                my_listeners = {
    				        			    afterGenera: function(from_win){
    				        					grid.getStore().each(function(rec){ 
        							  			 rec.set('ar_config_row', '');
                    							},this);
    	 									from_win.close();
    	 									m_win.fireEvent('afterExecute', m_win);
	 											
								        	}
										}
						                
						                acs_show_win_std('Intestazione nuovo ordine', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form', {list_selected_id : list_selected_id}, 350, 520, my_listeners, 'icon-shopping_cart-16');
									 
									 }
				           }
								       }  			       
			]
		}],	                    
		
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		            listeners: {
		              'edit': function(editor, e, opts) {
		              	
		              	//se una data, converto il valore in Ymd
		              	if (e.field == 'CONSEGNA_STD'){
		              		e.record.set(e.field, Ext.Date.format(e.value, 'Ymd'));
		              	}
		              
		             	e.grid.store.save();
		             	
		             	//Forzoicalcolo totale riga
		             	e.record.set('PRZ_LIST_TOT');
		             	
		                //this.isEditAllowed = false;
		              }
		            }
		          })
		      ],	    
	    
		features: [
					{ftype: 'summary'},
					
					/*
					, {
			            //id: 'group',
			            ftype: 'groupingsummary',
			            groupHeaderTpl: '{[values.rows[0].data.FORNITORE_D]} [{name}]',
			            hideGroupedHeader: false
			        }*/					
						
			{
				ftype: 'filters',
				encode: false, //settingsGrid.store.commitChanges() json encode the filter query
				local: true,   // defaults to false (remote filtering)
							
				// Filters are most naturally placed in the column definition, but can also be added here.
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    
	    
		store: {
			xtype: 'store:array',
			autoLoad:true,
			autoSave: true,
			
			data: <?php echo acs_je($m_params->rec_data); ?>, 
			////groupField: 'FORNITORE_C',
				
			fields: ['DT', 'FORNITORE_C', 'FORNITORE_D', 'ARTICOLO_C', 'ARTICOLO_D', 'UM', 
						{name: 'DISPONIBILITA', type: 'int'},
						{name: 'GIACENZA', type: 'float'},
						{name: 'ORDINATO', type: 'float'},
						{name: 'IMPEGNATO', type: 'float'},
						{name: 'SCORTA', type: 'float'},
						{name: 'LOTTO_RIORD', type: 'float'},
						{name: 'SCOST_RIORD', type: 'float'},
						{name: 'QTA_RIORD', type: 'float'}, 
						'OLD_RIORD', 'CONSEGNA_STD', 'ULTIMO_IMP', 'QTA_RIORD', 'PRZ_LIST', 
						{name: 'PRZ_LIST_TOT', type: 'float',
							convert: function(val,row) {
    							return parseFloat(row.get('QTA_RIORD')) * parseFloat(row.get('PRZ_LIST'));
    						}
    					}, 'CONFIG', 'ar_config_row', 'idsc'	
					]
						
		}, //store	    
	    			<?php $cfg = "<img src=" . img_path("icone/48x48/game_pad.png") . " height=20>"; 	?> 		
		columns: [				    
				  	{header: 'Codice', width: 100, dataIndex: 'ARTICOLO_C', filter: {type: 'string'}}
				  , {header: 'Articolo', flex: 1, dataIndex: 'ARTICOLO_D', filter: {type: 'string'}, 	
				  		 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					           return 'Totale ordine a fornitore'; 
					     }
					}
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'QTA_RIORD', tdCls: 'grassetto', 
				  	
					renderer: function (value, metaData, record, row, col, store, gridView){						
			                	if (record.get('OLD_RIORD').trim() != '')
			                		metaData.tdCls += ' grassetto sfondo_grigio';
			                	
			                	return floatRenderer0N(value);
			        },				  	
				  	
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true,
			                hideTrigger:true,
			                keyNavEnabled : false,
							mouseWheelEnabled : false
			            }				  
				   }
				  , {header: 'Consegna<br/>richiesta',	width: 90, dataIndex: 'CONSEGNA_STD', renderer: date_from_AS, tdCls: 'grassetto',
					editor: {
			                xtype: 'datefield',
			                allowBlank: false			                
			            }				  				   
				  
				  }
				  , {header: 'Prezzo<br/>unitario',	 	width: 72, dataIndex: 'PRZ_LIST', align: 'right', renderer: floatRenderer2N, tdCls: 'grassetto'}				  
				  , {header: 'Importo',	 	width: 72, dataIndex: 'PRZ_LIST_TOT', align: 'right', renderer: floatRenderer2N,
							 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
								            return floatRenderer2(value); 
								        }				  
				  }
				  
				  <?php if($m_params->ord_ven == 'Y'){?>
				  , {header: '<?php echo $cfg; ?>',	 
				     width: 50, 
				     dataIndex: 'config',
				     renderer: function(value, metaData, record){
                         if(record.get('CONFIG') != ''){ 
                            if(record.get('ar_config_row') != '') metaData.tdCls += ' sfondo_verde';
                            else metaData.tdCls += ' sfondo_rosso';
                            
                            return '<img src=<?php echo img_path("icone/48x48/game_pad.png") ?> width=15>';
		    		    
		    		     }
		    		  }
				     }
				  <?php }?>				  
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    },
		    
		    
         listeners: {
         	       
	          celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							var rec = iView.getRecord(iRowEl);
							var grid = iView;
							  
		                   	var my_listeners = {
            					afterConferma: function(from_win, idsc){
            						rec.set('idsc', idsc);
            						from_win.destroy();
            						Ext.Ajax.request({
        							       url : '../base/acs_configuratore_articolo.php?fn=get_dom_risp',
						                   method: 'POST',
				        			       jsonData: {
				        			       		idsc : rec.get('idsc')
				        			       },					
         		                           timeout: 2400000,
        							       method     : 'POST',
        							       waitMsg    : 'Data loading',  
        							       success : function(result, request){
        										jsonData = Ext.decode(result.responseText);
         		                                rec.set('ar_config_row', jsonData.ar_config_row);
        							        },
        							        failure    : function(result, request){
        							            Ext.Msg.alert('Message', 'No data to be loaded');
        							        }
    							    });
            						
            						 						        									            
    				        		}
    		    				};							
						
						    if(rec.get('CONFIG') != ''){						    
						    	//se e' gia' stato configurato mostro le variabili di configurazione
						    	if (!Ext.isEmpty(rec.get('ar_config_row'))){
									acs_show_win_std('Dettaglio configurazione articolo', 
                                		  '../base/acs_configuratore_articolo.php?fn=open_form', 
                                		  {	idsc : rec.get('idsc')}, 
                                		  600, 400, {}, 'icon-folder_search-16');						    		
						    	} else {
						    		//avvio configurazione
						  	    	acs_show_win_std('Configuratore articolo', 
                		  				'../base/acs_configuratore_articolo.php?fn=open_tab', 
                		  				{	
                		  					c_art :  rec.get('ARTICOLO_C'), 
                		  					gestione_dim: 'N',
                		  					ar_config_row: rec.get('ar_config_row')
                		  				}, 600, 400, my_listeners, 'icon-copy-16');
                		  		}		  	      
						    }
						    
						     
							  						  
							 
						  }
			   		  },
			   		  
			   		  itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     voci_menu.push({
		         				text: 'Duplica',
		        				iconCls : 'icon-leaf-16',             		
		        				handler: function () {
		        				  var new_rec = rec.copy();
				 				  grid.getStore().add(new_rec);    						        		  		
				                }
			    		  });
			    				
			    				
			    			if (!Ext.isEmpty(rec.get('ar_config_row'))){
    					     voci_menu.push({
    		         				text: 'Azzera configurazione articolo',
    		        				iconCls : 'iconDelete',             		
    		        				handler: function () {
    		        				rec.set('ar_config_row', '');    						        		  		
    				                }
    			    		  });			    			
			    			}	 
			    				
				      		var menu = new Ext.menu.Menu({
				            items: voci_menu
							}).showAt(event.xy);	
					     
					     }
         
         }		    
		    
    							
	}    
    

}


<?php exit; }
// ******************************************************************************************
// exe modifica punto riordino su articolo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_punto_riordino'){
	$m_params = acs_m_params_json_decode();

		//modifico il dato su anagrafica articoli e su wmaf20
		// ANAG. ARTICOLI
		$sql = "UPDATE {$cfg_mod_DeskAcq['file_anag_art']} SET ARLOTT=? WHERE ARDT=? AND ARART=?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($m_params->f_qta, trim($m_params->f_dt), trim($m_params->f_rdart)));
			echo db2_stmt_errormsg($stmt);;
			
		// WMAF20
			$sql = "UPDATE {$cfg_mod_DeskAcq['file_WMAF20']} SET M2LOTT=? WHERE M2DT=? AND M2ART=?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($m_params->f_qta, trim($m_params->f_dt), trim($m_params->f_rdart)));
			echo db2_stmt_errormsg($stmt);;
	
	$ret['success'] = true;
	echo acs_je($ret);
	
	exit;
}
	


// ******************************************************************************************
// FORM PARAMETRI per modifica punto riordino
// ******************************************************************************************
if ($_REQUEST['fn'] == 'form_punto_riordino'){
	$m_params = acs_m_params_json_decode();	
	?>
{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            
			            	var form = this.up('form').getForm();
			            	m_win = this.up('window');
			            	form_values = form.getValues();
			            	
			            	 
			            	if(form.isValid()){
					
					        		//carico la form dal json ricevuto da php
					        		Ext.Ajax.request({
					        		        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_punto_riordino',
					        		        jsonData   : form_values,
					        		        method     : 'POST',
					        		        waitMsg    : 'Data loading',
					        		        success : function(result, request){
					        		            var jsonData = Ext.decode(result.responseText);
					        		            mp.add(jsonData.items);
					        		            mp.doLayout();
					        		            mp.show();

												m_win.fireEvent('afterExecute', form_values);					        		            
					        		            
					        		            m_win.close();        		            
					        		        },
					        		        failure    : function(result, request){
					        		            Ext.Msg.alert('Message', 'No data to be loaded');
					        		        }
					        		    });
					
					        	 
							
			            	}
			            }
			         }],   		            
		            
		            items: [
		            

				            	{
								   xtype: 'hiddenfield'
								   , name: 'f_dt'
								   , value: <?php echo j(trim($m_params->dt)); ?>
								}, {
								   xtype: 'hiddenfield'
								   , name: 'f_rdart'
								   , value: <?php echo j(trim($m_params->rdart)); ?>
								}, {
								   xtype: 'displayfield'
								   , name: 'f_no_use'
								   , allowBlank: false
								   , fieldLabel: 'Articolo'
								   , value: <?php echo j(trim($m_params->rddart) . " [" . trim($m_params->rdart) . "]"); ?>
								   , anchor: '-15'
								   , margin: "10 10 0 10"
								   , disabled: false							   
								}, {
								   xtype: 'numberfield'
								   , name: 'f_qta'
								   , allowBlank: false
								   , fieldLabel: 'Punto di riordino'
								   , margin: "10 10 0 10"
								   , hideTrigger:true
                                   , keyNavEnabled : false
                            	   , mouseWheelEnabled : false
								   , value: <?php echo j(trim($m_params->qta)); ?>
								}
												  
						  
						  ]
		              }			  
			  
			  
			  ]
			   
		}


 ]
}

<?php exit; }

/*************************************************************************
 * REPORT (sulle righe interessate alla generazione degli ordini)
*************************************************************************/
if ($_REQUEST['fn'] == 'get_report'){
	
	$m_params = json_decode($_REQUEST['jsonData']);
	$my_array = array();
	foreach($m_params->el_row as $ro){
		$r = (array)$ro;
		
		//raggruppo per fornitore (e data consegna std???)
		
		$liv0_v = trim($r['M2FOR1']);	//fornitore
		$liv1_v = trim($r['M2DTE20']);	//consegna std	
		
		// LIVELLO 0 - fornitore
		$s_ar = &$my_array;
		$liv_c     = $liv0_v;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_0";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
			$s_ar[$liv_c]["task"] = $r['M2RGS1'];

		}
		$s_ar = &$s_ar[$liv_c]["children"];
				
		// LIVELLO 1
		$liv_c     = $liv1_v;
		$tmp_ar_id[] = 'liv2;' .$liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_2";
			$s_ar[$liv_c]["liv_cod"]  	= "{$liv_c}";
			$s_ar[$liv_c]["task"] 		= "{$liv_c}";
		}
				
		//accodo la riga
		$r['COSTO_TOT']					= (float)$r['M2PRZ'] * (float)$r['M2QOI20'];
		$s_ar[$liv_c]["children"][]		= $r;
		
	}
	
	//print_r($my_array);
	
	//stampo il report -----------------------------------------------------------------------------------------
	?>
<html>

<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table{border-collapse:collapse; width: 100%;}
div.acs_report table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{color: white; background-color: #000000; padding: 5px; margin-top: 10px; margin-bottom: 10px; font-size: 14px;}
table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}

div.acs_report h1{font-size: 22px; margin-top: 10px; margin-bottom: 10px;}
div.acs_report h2{font-size: 16px;}
 
@media print
{
	.noPrint{display:none;}
}




</style>



  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>



</head>


 <body>
 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'N';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content' class=acs_report> 
 
 <h1>Riepilogo richieste ordine materiali promozionali generate [<?php  echo trim($auth->get_user()); ?>]</h1>
 
  <table class=acs_report>  
 	<?php foreach($my_array as $k0 => $l0){ ?>
 		<tr class="t-l1">
 			<td colspan=7><h2>Fornitore: <?php echo "{$l0['task']} [{$l0['liv_cod']}]"; ?></h2></td>
 		</tr> 			
 		<tr>
 			<th>Consegna richiesta</th>
 			<th>Articolo</th>
 			<th>Codice</th>
 			<th>UM</th>
 			<th>Q.t&agrave; ordinata</th>
 			<th>Costo unitario</th>
 			<th>Costo totale</th>
 		</tr>
 			
 			<?php $costo_tot_for = 0; ?>
 			
			<?php foreach($l0["children"] as $k1 => $l1){ ?>
			 			
				<?php foreach($l1["children"] as $k2 => $l2){ ?>
				
					<?php $costo_tot_for	+= $l2['COSTO_TOT']; ?>				
					<?php $costo_tot 		+= $l2['COSTO_TOT']; ?>
				
					<tr>
						<td><?php echo print_date($k1); ?></td>
						<td><?php echo trim($l2['M2DART']); ?></td>
						<td><?php echo trim($l2['M2ART']); ?></td>
						<td><?php echo trim($l2['M2UM']); ?></td>
						<td class=number><?php echo n($l2['M2QOI20']); ?></td>
						<td class=number><?php echo n($l2['M2PRZ'], 2); ?></td>
						<td class=number><?php echo n($l2['COSTO_TOT'], 2); ?></td>						
					</tr>
				<?php } ?>			 			
			 			
 			<?php } ?>

 			<!--  totale fornitore -->
					<tr>
						<td colspan=6>Totale fornitore</td>
						<td class=number><?php echo n($costo_tot_for, 2); ?></td>						
					</tr> 			
 			
 			
 	<?php } ?>
 	
 			<!--  totale generale -->
 					<tr>
						<td colspan=7>&nbsp;</td>						
					</tr>
 					<tr class="liv_totale">
						<td colspan=6>Totale generale</td>
						<td class=number><?php echo n($costo_tot, 2); ?></td>						
					</tr> 	
 	
 	
   </table>
  </div> 	
 </body>
</html>	
	<?php
	// ----------------------------------------------------------------------------------------- stampo il report
	exit;	
}



/*************************************************************************
 * GENERAZIONE ORDINI MTO
*************************************************************************/
if ($_REQUEST['fn'] == 'exe_generazione_ordini'){
	
	/*
	 Viene passato l'elenco delle chiavi ditta/articolo e relativa qta ordinata
	 1) aggirno le qta' su wmaf20 (usando i campi 20)
	 2) creo una riga su RI per ogni fornitore/data
	*/	
	
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;
	
	$ar_ord = array();	
	
	$m_params = acs_m_params_json_decode();
	$list_selected_id = $m_params->list_selected_id; //elenco chiavi di fabbisogno	

	$sql = "UPDATE {$cfg_mod_DeskAcq['file_WMAF20']} 
			SET M2QOI20=?, M2FTQ20 = 'I'
			 , M2DTE20=?, M2PRZ=? 
			WHERE M2DT=? AND M2ART=? ";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	
	foreach($list_selected_id as $kr => $r){
		$r = (array)$r;
		$dt 	= trim($r['DT']);
		$art	= trim($r['ARTICOLO_C']);
		$qta	= (float)$r['QTA_RIORD'];
		$cons	= (string)$r['CONSEGNA_STD'];
		$forn	= (string)$r['FORNITORE_C'];
		$prz	= (float)$r['PRZ_LIST'];
		$result = db2_execute($stmt, array($qta, $cons, $prz, $dt, $art));
		echo db2_stmt_errormsg($stmt);

		//mi creo un array per ditta/fornitore/consegna		
		$ar_ord[$dt][$forn][$cons] +=1;		
	}

	
	//Prima di generare gli ordini, recupero l'elenco delle righe interessate (perche' dovro' fornire un report)
	//TODO: gestire la multisessione?
	$sql2 = "SELECT * FROM {$cfg_mod_DeskAcq['file_WMAF20']} WHERE M2FTQ20 = 'I'";
	$stmt2 = db2_prepare($conn, $sql2);
	$result2 = db2_execute($stmt2);
	while ($r = db2_fetch_assoc($stmt2)) {
		$ret_row[] = $r;
	}
	
	
	
	//per ogni riga dell'array creo una riga nell'RI
	foreach($ar_ord as $kdt => $ar_dt){
		foreach($ar_dt as $kfor => $ar_for){
			foreach($ar_for as $kdata => $ar_data){
				//scrivo RI per il fornitore/data inline
				//HISTORY
				$sh = new SpedHistory($main_module);
				$sh->crea(
						'dora_gen_ord_mpr',
						array(
								"ditta"		=> $kdt,
								"fornitore" => $kfor,
								"data_cons"	=> $kdata
						)
				);
			}
		}
	}
	
	
	
	$ret = array();
	$ret['success']	= true;
	$ret['el_row']	= $ret_row;
	echo acs_je($ret);
	
	exit;
}



/*************************************************************************
 * GET JSON DATA
 *************************************************************************/
if ($_REQUEST['fn'] == 'get_json_data_grid'){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;
	
	$sql_where_flt	= "";
	//$sql_where 		= " AND M2TPAR = 'V' ";
	$sql_where 		= " AND 1=1 ";
	
	if (isset($cfg_mod_Spedizioni["add_where_to_elenco_articoli_promo"]))
	    $sql_where .= " " . $cfg_mod_Spedizioni["add_where_to_elenco_articoli_promo"] . " ";
	
	$sql = "SELECT M2QMAN, M2LOTT, M2FTQ20, M2QOI20, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2QFAB, W2.M2PRZ,
			W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W2.M2UM, T_BREF.TACINT AS REF_NAME, T_GRPA.TADESC AS GR_PIANIF_DESC,
			SUM(W3.M3QTA) AS S_M3QTA, W2.M2TAB1 AS GR_PIANIF, ANAG_ART.ARSOSP, ANAG_ART.ARESAU, ANAG_ART.ARMODE
			FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_WMAF30']} W3
				ON W2.M2DT = W3.M3DT AND W2.M2ART = W3.M3ART
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
				ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE
			LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
				ON CF.CFDT = W2.M2DT AND CF.CFCD = W2.M2FOR1 AND CF.CFTICF = 'F'
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
				ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
				ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
				ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART
			WHERE M2DT = '{$id_ditta_default}' AND ARART LIKE 'MP%' AND ARSOSP='' AND ARESAU NOT IN ('C', 'R')
	        {$sql_where}
			GROUP BY 
 				M2QMAN, M2LOTT, M2FTQ20, M2QOI20, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2QFAB, W2.M2PRZ,
				W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W2.M2UM, T_BREF.TACINT, T_GRPA.TADESC,
				W2.M2TAB1, ANAG_ART.ARSOSP, ANAG_ART.ARESAU, ANAG_ART.ARMODE,
				TA_ITIN.TASITI
				
			ORDER BY TA_ITIN.TASITI, CF.CFRGS1, W2.M2DRIO, W2.M2TAB1, W2.M2ART
			";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
	
	while ($r = db2_fetch_assoc($stmt)) {
		$r['DT'] 			= acs_u8e($r['M2DT']);
		$r['FORNITORE_C'] 	= acs_u8e($r['M2FOR1']);		
		$r['FORNITORE_D'] 	= acs_u8e($r['CFRGS1']);
		$r['ARTICOLO_C'] 	= acs_u8e($r['M2ART']);		
		$r['ARTICOLO_D'] 	= acs_u8e($r['M2DART']);
		$r['UM'] 			= acs_u8e($r['M2UM']);
					
		$r['ORDINATO'] 		= acs_u8e($r['M2QORD']);
		$r['IMPEGNATO'] 	= $r['M2QFAB'];
		$r['SCORTA'] 		= acs_u8e($r['M2SMIC']);
		
		$r['GIACENZA'] 		 = 0;
		$r['DISPONIBILITA']  = 0;
		
		if (!is_object($m_params->param)) 
		  $param = json_decode($m_params->param);
		else
		  $param = $m_params->param;
		
		if($param->disp_giac == 'N'){
		    if($r['FORNITORE_C'] == 0){
		        $r['GIACENZA'] 		 = 0;
		        $r['DISPONIBILITA']  = 0;
		   }else{
		       $r['GIACENZA'] 		= acs_u8e($r['M2QGIA']);
		       $r['DISPONIBILITA']	= acs_u8e((float)$r['M2QGIA'] + (float)$r['M2QORD'] - (float)$r['M2QFAB']);
		       
		   }
		}else{
		    $r['GIACENZA'] 		= acs_u8e($r['M2QGIA']);
		    $r['DISPONIBILITA']	= acs_u8e((float)$r['M2QGIA'] + (float)$r['M2QORD'] - (float)$r['M2QFAB']);
		}
		
		$r['LOTTO_RIORD']	= (int)$r['M2LOTT'];
		
		$r['ar_config_row']	= '';
				
		$r['CONSEGNA_STD']	= acs_u8e($r['M2DRIO']);		
		$r['ULTIMO_IMP']	= acs_u8e($r['M2UIMP']);
		
		$r['DISPONIBILITA_MANCANTE']	= $r['M2QMAN'];
		
		//valore di scostamento dalla disponibilita'
		if ((int)$r['LOTTO_RIORD'] != 0){
			$r['SCOST_RIORD'] = ( (float)$r['DISPONIBILITA'] - (float)$r['LOTTO_RIORD'] ) / (float)$r['LOTTO_RIORD'] * 100;
		}
		
		
		$r['QTA_RIORD']		= $r['M2QOI20'];
		$r['OLD_RIORD']		= $r['M2FTQ20'];
		$r['PRZ_LIST']		= $r['M2PRZ'];
		$r['ARESAU']		= trim($r['ARESAU']);
		$r['CONFIG']		= trim($r['ARMODE']);
		$ar_ret[] = $r;
	}

	echo acs_je($ar_ret);
	
    exit();
}








/***********************************************************************************
 * GRID
 ***********************************************************************************/
if ($_REQUEST['fn'] == 'get_json_grid'){

 $m_params = acs_m_params_json_decode();
 
 //recuper data/ora ultimo aggiornamento 
	$sql = "SELECT min(M2dtge) as M2DTGE, min(m2orge) AS M2ORGE FROM {$cfg_mod_DeskAcq['file_WMAF20']}		";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$r = db2_fetch_assoc($stmt);
	if ($r != false){
		$situazione_txt = " [Situazione al " . print_date($r['M2DTGE']) . " " . print_ora($r['M2ORGE']) . "]";
	} else 
		$situazione_txt = '';
	

?>

{"success":true, "items":

  {
    xtype: 'panel',
	id: 'panel-riordino_materiali_promo',	    
	title: 'Marketing',
	closable: true,
	 	
    layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},    
    items: [
    
    {
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    
	    
        dockedItems: [{
            xtype: 'toolbar'
            , dock: 'top'
            , items: [{xtype: 'label', cls: 'strong', text: 'Controllo disponibilita/riordino materiali promozionali' + <?php echo j($situazione_txt); ?>}, {xtype: 'tbfill'}
            ]
	    	},{
		        dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                {
                 text: '',
	             iconCls: 'icon-gear-32',
	             scale: 'large',	                     
                 handler: function() {
                 var grid = this.up('grid');
                 
                	my_listeners = {
    					afterFilter: function(from_win, form_values){
    					  grid.store.proxy.extraParams.param = form_values;
    					  grid.getStore().removeAll();
    				      grid.getStore().load();
    				      
    				      from_win.close();						        									            
			        		}
	    				};			
                 
                 acs_show_win_std('Parametri disponibilitÓ materiali marketing', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_params', {}, 450, 200, my_listeners, 'icon-gear-16');
                 
                 }
                
                	 
                }, 
			        '->', {
                     xtype: 'button',
                     text: 'Genera ordine cliente',
		             iconCls: 'icon-shopping_cart-32',
		             scale: 'large',	                     
                        handler: function() {
                            list_selected_id = [];
							grid = this.up('grid');
                            grid.getStore().each(function(rec)  
							{  
							  if (rec.get('OLD_RIORD').trim() == '' && parseFloat(rec.get('QTA_RIORD')) > 0)
							  	list_selected_id.push(rec.data); 
							},this);
							
			            	my_listeners = {
		        					afterExecute: function(from_win){
		        						  grid.getStore().load();
		        						  from_win.close();						        									            
						        		}
				    				};			
                        
                            acs_show_win_std('Riepilogo articoli', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_conferma_ordine', {rec_data: list_selected_id, ord_ven : 'Y'}, 800, 500, my_listeners, 'icon-shopping_cart-16');
		
		           
		           }}]
		
		}],	                    
		
	    
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		              clicksToEdit: 1,
		            listeners: {
		              'beforeedit': function(e, eEv) {
		                var me = this;
		                
		                rec = eEv.record;                
						if (rec.data.OLD_RIORD.trim() == '')		                
		                	return true;
		                else
		                	return false;
		              },
		              'edit': function(e) {
		                //this.isEditAllowed = false;
		              }
		            }
		          })
		      ],	    
	    
		features: [
			{
				ftype: 'filters',
				encode: false, // json encode the filter query
				local: true,   // defaults to false (remote filtering)
							
				// Filters are most naturally placed in the column definition, but can also be added here.
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    
	    
		store: {
			xtype: 'store',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
				
				method: 'POST',
				type: 'ajax',
	            timeout: 2400000,				
				 actionMethods: {
			          read: 'POST'
			        },
			        
			           extraParams: {
						param: <?php echo acs_je($param); ?>
    				},
        		doRequest: personalizza_extraParams_to_jsonData,
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				}				
			},
				
			fields: ['DT', 'FORNITORE_C', 'FORNITORE_D', 'ARTICOLO_C', 'ARTICOLO_D', 'UM', 
						{name: 'DISPONIBILITA', type: 'int'}, {name: 'DISPONIBILITA_MANCANTE', type: 'int'},
						{name: 'GIACENZA', type: 'float'},
						{name: 'ORDINATO', type: 'float'},
						{name: 'IMPEGNATO', type: 'float'},
						{name: 'SCORTA', type: 'float'},
						{name: 'LOTTO_RIORD', type: 'float'},
						{name: 'SCOST_RIORD', type: 'float'},
						{name: 'QTA_RIORD', type: 'float'}, 
						'OLD_RIORD', 'CONSEGNA_STD', 'ULTIMO_IMP', 'QTA_RIORD', 'PRZ_LIST', 'CONFIG', 'ar_config_row', 'ARESAU']
						
		}, //store	    
	    			 		
		columns: [
					<?php $cfg = "<img src=" . img_path("icone/48x48/game_pad.png") . " height=20>"; 	?> 		
		
				    {header: 'Fornitore', flex: 1, dataIndex: 'FORNITORE_D', filter: {type: 'string'}}
				,{text: 'C', 
				tooltip: 'Ciclo di vita',	 	
				width: 30, 
				dataIndex: 'ARESAU',
				renderer: function(value, metaData, record, row, col, store, gridView){
    		    	if (record.get('ARESAU') == 'E'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In esaurimento') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/clessidra.png") ?> width=15>';}
					if (record.get('ARESAU') == 'A'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In avviamento') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';}
				    if (record.get('ARESAU') == 'C'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In codifica') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=15>';}
				   if (record.get('ARESAU') == 'R'){	    			    	
			   		   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Riservato') + '"';
			   		   return '<img src= <?php echo img_path("icone/48x48/folder_private.png") ?> width=15>';}
				   if (record.get('ARESAU') == 'F'){	    			    	
			   		   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Fine codifica') + '"';
			   		   return '<img src= <?php echo img_path("icone/48x48/button_blue_play.png") ?> width=15>';}
				    
				    //return value;
				    }
				}
				  , {header: 'Codice', width: 100, dataIndex: 'ARTICOLO_C', filter: {type: 'string'}}
				  , {header: '<?php echo $cfg; ?>',	 
				     width: 30, 
				     dataIndex: 'config',
				     renderer: function(value, metaData, record){
                         if(record.get('CONFIG') != '') return '<img src=<?php echo img_path("icone/48x48/game_pad.png") ?> width=15>';
		    		    
		    		  }
				    }
				  , {header: 'Articolo', flex: 1, dataIndex: 'ARTICOLO_D', filter: {type: 'string'}}
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Disponib.',	width: 72, dataIndex: 'DISPONIBILITA', align: 'right',
					    renderer: function (value, metaData, record, row, col, store, gridView){
			                	metaData.tdCls += ' grassetto';

					  				if (parseFloat(value) < 0)
					  					 metaData.tdCls += ' sfondo_rosso';					  					 														  
					  				else if (parseFloat(value) == 0 || parseFloat(record.get('DISPONIBILITA_MANCANTE')) > 0 )
					  					 metaData.tdCls += ' sfondo_giallo';					  					 
					  				if (parseFloat(value) > 0)
					  					 metaData.tdCls += ' sfondo_verde';					  					 
			                			                
					  			return floatRenderer0N(value);									
						} 
				    }				  
				  , {header: 'Giacenza',	width: 72, dataIndex: 'GIACENZA', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Ordinato',	width: 72, dataIndex: 'ORDINATO', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Impegnato',	width: 72, dataIndex: 'IMPEGNATO', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Scorta',	 	width: 72, dataIndex: 'SCORTA'	, align: 'right', renderer: floatRenderer0N}				  				  				  
				  , {header: 'Punto di<br/>riordino',	width: 72, dataIndex: 'SCOST_RIORD', align: 'right',

						renderer: function (value, metaData, record, row, col, store, gridView){
								if (parseFloat(value) != 0){
				                	if (value < -5)
				                		metaData.tdCls += ' grassetto sfondo_rosso';
									else if (value <= 5)				                	
										metaData.tdCls += ' grassetto sfondo_giallo';
								}		
				                	
				                	return floatRenderer0N(record.get('LOTTO_RIORD'));
				        },				  	
				  	 
				  	}
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'QTA_RIORD', 
				  	
					renderer: function (value, metaData, record, row, col, store, gridView){
			                	if (record.get('OLD_RIORD').trim() != '')
			                		metaData.tdCls += ' grassetto sfondo_grigio';
			                	else
			                	    metaData.tdCls += ' grassetto sfondo_arancione';
			                	
			                	return floatRenderer0N(value);
			        },				  	
				  	
					editor: {
			                xtype: 'numberfield',
			                hideTrigger:true,
			                keyNavEnabled : false,
							mouseWheelEnabled : false,
			                allowBlank: true
			            }				  
				   }
				  , {header: 'Consegna<br/>richiesta',	width: 90, dataIndex: 'CONSEGNA_STD', renderer: date_from_AS}
				  , {header: 'Ultimo<br/>impegno',	width: 90, dataIndex: 'ULTIMO_IMP', renderer: date_from_AS}
				  , {header: 'Prezzo<br/>unitario',	 	width: 72, dataIndex: 'PRZ_LIST', align: 'right', renderer: floatRenderer2N}
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    },
		    
		    
         listeners: {
   		  celldblclick: {								
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
			  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
			  	rec = iView.getRecord(iRowEl);
			  	
			  	if (col_name == 'SCOST_RIORD'){

							                   	my_listeners = {
						        					afterExecute: function(form_values){
						        						rec.set('LOTTO_RIORD', form_values.f_qta);
						        						
						        						//ricalcolo lo scostamento
						        						
						        						//TODO: verificare perche' sembra che non reimposta correttamente il valore (o almeno lo sfondo)
						        						//if (parseFloat('LOTTO_RIORD') != 0)
														//	rec.set('SCOST_RIORD', ( parseFloat(rec.get('DISPONIBILITA')) - parseFloat(rec.get('LOTTO_RIORD')) ) / parseFloat(rec.get('LOTTO_RIORD')) * 100);
														
														rec.set('SCOST_RIORD', 0);							        						
						        						
						        						//iView.refreshNode(rec);
						        						//iView.getView().refreshRow(rec);						        									            
										        		}
								    				};			  	
			  	
			  		//modifica punto di riordino
					acs_show_win_std('Modifica punto di riordino', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_punto_riordino', {dt: rec.get('DT'), rdart: rec.get('ARTICOLO_C'), rddart: rec.get('ARTICOLO_D'), qta: rec.get('LOTTO_RIORD')}, 600, 200, my_listeners, 'icon-shopping_cart_gray-16');			  		
			  		return false;
			  	} 		  
							  
				  acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('DT'), rdart: rec.get('ARTICOLO_C')}, 1200, 600, null, 'icon-shopping_cart_gray-16');
			  }
   		  },
   		  
   		    
         }		    
		    
    							
	}    
    
    ] //items
  } //main panel 

}
<?php
exit; }

if ($_REQUEST['fn'] == 'exe_ins_righe_ven'){
    $sh = new SpedHistory();
    
    //per ogni riga ordine da generare
    foreach($m_params->list_selected_id as $v){
        $riga_con_config_par = '';
        $use_session_history = microtime(true);
        
        //passo eventuali domande/risposte di configurazione
        if (count((array)$v->ar_config_row) > 0)
            foreach((array)$v->ar_config_row as $kd => $kv){
                if (strlen($kv) > 0)
                    $riga_con_config_par = 'P';
    
            } //per ogni riga di configurazione
 
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> "INS_RIGA_VEN",
                "k_ordine"	=> $m_params->k_ordine,
                "vals" => array(
                    "RIART"  => $v->ARTICOLO_C,
                    "RIDART" => $v->ARTICOLO_D,
                    "RIQTA"  => sql_f($v->QTA_RIORD),
                    "RIIMPO" => sql_f($v->PRZ_LIST),
                    "RIAZDV" => $riga_con_config_par,
                    "RINOTE" => $v->idsc,
                )
                
            )
         );
    } //per ogni riga da generare
    
    
 
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit();
    
}


if ($_REQUEST['fn'] == 'open_form'){ ?>

{"success":true, "items": [
	  {
		xtype: 'form',
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        frame: true,
        flex : 1,
        items: [
			{name: 'tido', xtype: 'hidden', value: 'VO'}
 	     ,  {xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,		
			labelWidth : 120,	
			allowBlank: false,            
            store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            listeners: {
	            change: function(field,newVal) {
	            	var form = this.up('form').getForm(); 	            	
	            	  form.findField('f_destinazione_cod').setValue('');	
					  form.findField('f_destinazione').setValue('');
                      form.findField('out_ind_dest').setValue('');
                      form.findField('out_loc_dest').setValue('');
                      form.findField('f_pven_cod').setValue('');	
					  form.findField('f_pven').setValue('');
                      form.findField('out_ind_pven').setValue('');
                      form.findField('out_loc_pven').setValue('');
	            }, 
				select: function(combo, row, index) {
	            	var form = this.up('form').getForm();
	     			form.findField('out_ind').setValue(row[0].data.out_ind);									     			
	     			form.findField('out_loc').setValue(row[0].data.out_loc);

 					//imposto quella a standard
					form.findField('f_destinazione_cod').store.load({
	 					callback: function(records, operation, success) {
 							
 							if (records.length == 1){
 								this.setValue(records[0].get('cod'));
 							}
    					}, scope: form.findField('f_destinazione_cod') 	
 					});
 								
 		
				} 		
 		
 		
	        },
            
            
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class=\"search-item\">' +
                        '<h3><span>{descr}</span>[{cod}]</h3>' +
 						' {out_ind}<br/>' +
                        ' {out_loc}' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000
        }
 		
 		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind', 	
 			labelWidth : 120,	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc', 
 			labelWidth : 120,
 			anchor: '100%',		
 		}

	, {
			xtype: 'fieldcontainer',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_destinazione_cod',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'f_destinazione',
				    fieldLabel: 'Destinazione',
                 	labelWidth : 120,
                 	flex : 1,
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_destinazione_cod').setValue(record_selected.cod);	
												form.findField('f_destinazione').setValue(record_selected.denom);
                                                form.findField('out_ind_dest').setValue(record_selected.IND_D);
                                                form.findField('out_loc_dest').setValue(record_selected.LOC_D);
                                                from_win.close();
												}										    
									};
									
                                    var cliente = form.findField('f_cliente_cod').value;
							  
									acs_show_win_std('Seleziona destinazione', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : cliente, 
									 type : 'D',
									 includi_AG: 'Y',
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
				}
				
				]
		}
 		
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_dest', 	
 			labelWidth : 120,	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_dest', 		
 			labelWidth : 120,
 			anchor: '100%',
 		} 		
 		
		, {
				name: 'tpdo',
				xtype: 'combo',
				labelWidth : 120,
            	anchor: '100%',
				fieldLabel: 'Tipo ordine',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			    allowBlank: false,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
                              <?php echo acs_ar_to_select_json($s->find_TA_std('TPMRK', null, 'N', 'N', null, null, null, 'N', 'Y'), ""); ?>
					    ]
				}				     		
 	
			}	
     			     		
		, {
				     name: 'vsrf'                		
				   , xtype: 'textfield'
				   , fieldLabel: 'Riferimento'
				   , maxLength: 30
				   , anchor: '100%'
				   , labelWidth : 120
				}
					     		
		, {
				name: 'mode',
				labelWidth : 120,
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Modello',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,		
                queryMode: 'local',
				minChars: 1,
				value : 'MP',	     														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					    <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, 'MOD', null, null, 0, '', 'Y'), '');  ?>  
					    ] 
				},
		        listeners: {
            		beforequery: function (record) {
        			record.query = new RegExp(record.query, 'i');
        			record.forceAll = true;
            		}
                 }						 
			}

		, {
				name: 'prio',
				xtype: 'combo',
				labelWidth : 120,
            	anchor: '100%',				
				fieldLabel: 'Priorit&agrave',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,
				value: <?php echo j($cfg_mod_Spedizioni["protocollazione"]["prio"]["DEF"]); ?>,					     																
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}, {name:'tarest'}],
				    data: [								    
					      <?php echo acs_ar_to_select_json(find_TA_sys('BPRI', null, null, 'VO', null, null, 1), ''); ?> 	
					    ] 
				}						 
			}	

				, {
				     name: 'dtep'                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Consegna richiesta'
				   , labelWidth : 120
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: false	
				   , anchor: '100%'			   		
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
					     		
				   , validator: function(value){
				   			var loc_form = this.up('form').getForm();
				   			
				   			if (Ext.isEmpty(loc_form.findField('prio').valueModels))
				   			  return true;

					        if(Ext.isEmpty(value)){
    					     	if (Ext.Array.contains(['>', '<', 'W', '='], loc_form.findField('prio').valueModels[0].get('tarest')))
    					        	return 'Indicare una data consegna richiesta';
					        }
					     	return true;
					    }					     		
					     		
				}					     		

			, {
				     name: 'dtrg'                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , labelWidth : 120
				   , fieldLabel: 'Data registrazione'
				   , format: 'd/m/Y'
				   , anchor: '100%'
				   , submitFormat: 'Ymd'
				   , allowBlank: false
				   , value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'				   		
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
				}	
					     		
		, {
				name: 'stdo',
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Stato',
				displayField: 'text',
				labelWidth : 120,
				valueField: 'id',
				emptyText: '- seleziona -',
				value: 'AA',
				forceSelection: true,
			   	allowBlank: false,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					      <?php echo acs_ar_to_select_json($s->find_TA_std('STMRK', null, 'N', 'N', null, null, null, 'N', 'Y'), ""); ?>	
					    ] 
				},
				listeners : {  
					beforeselect: function(combo, record, index ) {	            	
						//if (record.get('t_indi') != 'AA') return false;
								
				}}						 
			}
		,{
			xtype: 'fieldcontainer',
			flex : 1,
			anchor: '-10',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_pven_cod',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'f_pven',
				    fieldLabel: 'Punto vendita',
                 	labelWidth : 120,
                 	flex : 1,
                 	allowBlank : false,
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_pven_cod').setValue(record_selected.cod);	
												form.findField('f_pven').setValue(record_selected.denom);
                                                form.findField('out_ind_pven').setValue(record_selected.IND_D);
                                                form.findField('out_loc_pven').setValue(record_selected.LOC_D);
                                                from_win.close();
												}										    
									};
									
                                    var cliente = form.findField('f_cliente_cod').value;
							  
									acs_show_win_std('Seleziona punto vendita', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : cliente, type : 'P'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
				}
				
				]
		}
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_pven', 	
 			labelWidth : 120,	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_pven', 		
 			labelWidth : 120,
 			anchor: '100%'
				
			}	
				
				
				
				
	]
	
	, dockedItems: [
	
                {xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->',
                    {
                    iconCls: 'icon-button_black_repeat-16',
 					// scale: 'large',
                    itemId: 'listino',
                    text: 'Conferma generazione ordine cliente',
                    disabled: false,
                    scope: this,
 					call_protocolla: function(form, m_button, loc_win){
 					     Ext.Ajax.request({
							        url        : 'acs_op_exe.php?fn=exe_protocollazione',
 		                            timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										jsonData = Ext.decode(result.responseText); 	
										call_return = jsonData.call_return;	
 										var k_ordine = call_return.substr(345, 50);
             				            var anno   = k_ordine.substr(10, 4);
             				            var numero = k_ordine.substr(15, 6);
             				            var tipo = form.findField('tpdo').getValue();
             				            var ordine = anno + '_' + numero + '_' + tipo
             				            Ext.Ajax.request({
            							       url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ins_righe_ven',
							                   method: 'POST',
					        			       jsonData: {
					        			       		list_selected_id: <?php echo acs_je($m_params->list_selected_id)?>,
					        			       		k_ordine : k_ordine
					        			       },					
             		                           timeout: 2400000,
            							       method     : 'POST',
            							       waitMsg    : 'Data loading',  
            							       success : function(result, request){
            										jsonData = Ext.decode(result.responseText);
             		                                m_button.enable();
            	 		                            acs_show_msg_info('Protocollazione terminata. <br> Geneato ordine ' + ordine);
													loc_win.fireEvent('afterGenera', loc_win);
            							        },
            							        failure    : function(result, request){
            							            Ext.Msg.alert('Message', 'No data to be loaded');
            							        }
            							    });
 		
	 		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	 		
 					},
 		
 		
                    handler: function(b){
                     
 						var form = b.up('form').getForm();
 						 						
 						var m_button = b;
 						m_button.disable();
 						var loc_win = b.up('window'); 						
 						
 						if(form.isValid()){
 		
 									Ext.Ajax.request({
							        url        : 'acs_panel_protocollazione.php?fn=check_cliente_riferimento',
 		                            timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										jsonData = Ext.decode(result.responseText);
 		                                if (jsonData.num_find > 0){
 											Ext.Msg.confirm('Richiesta conferma', 'Esistono ordini gi&agrave; presenti con cliente/riferimento: <br> '+ jsonData.message +'<br> Vuoi procedere?',
                                 				 function(btn, text){																							    
                            								if (btn == 'yes'){																	         	
                            										b.call_protocolla(form, m_button,loc_win);							         	
                            																	      				         	
                            								}else{
                             	    								m_button.enable();
                            								}
                            					   });
 		
 		
 										} else 
 											b.call_protocolla(form, m_button, loc_win);
	 		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	 
 		
 		
 							 				
 						} else {
 							m_button.enable();
 						}
 		
                    }
                }]
            } 
	
	]
	
	
	}
]
}

<?php }

if ($_REQUEST['fn'] == 'open_params'){ 
    
    $param = json_decode($param);
    
    
    if($param->disp_giac == 'Y'){
        $checked_si = 'true';
        $checked_no = 'false';
    } else {
        $checked_si = 'false';
        $checked_no = 'true';
    }

    
    ?>
    
    
{"success":true, "items":    
    
    				{
            xtype: 'form', 
            //layout: 'fit',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            items: [
            {
                xtype: 'radiogroup',
                anchor: '100%',
                labelWidth : 300,
                fieldLabel: 'Visualizza disponibiltÓ/giacenza per fornitore mancante',
                items: [
                    {
                        xtype: 'radio'
                      , name: 'disp_giac' 
                      , boxLabel: 'Si'
                      , inputValue: 'Y'
                      , checked: <?php echo $checked_si; ?>   
                    }
                    
                    
                    , {
                        xtype: 'radio'
                      , name: 'disp_giac' 
                      , boxLabel: 'No'
                      , inputValue: 'N'
                      , checked: <?php echo $checked_no; ?>                       
                    }
                   
               ]
            }
            ],
            buttons: [
           
            {
	            text: 'Conferma',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',	                     
	            handler: function() {
	                var loc_win = this.up('window');
	                var form_values = this.up('form').getForm().getValues();
	                
	                
	                Ext.Ajax.request({
					        url        : '../base/f8.php?fn=f8_m_exe',
					        method     : 'POST',
		        			jsonData: {
								form_values: form_values,
								data: <?php echo acs_je($ar_parameters) ?>
							},							        
					        success : function(result, request){
					            jsonData = Ext.decode(result.responseText);
							    loc_win.fireEvent('afterFilter', loc_win, form_values);
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                             
	            }
	        }], 
            
            }
    
    
    }
    
   
   
   <?php  
}
    
   



