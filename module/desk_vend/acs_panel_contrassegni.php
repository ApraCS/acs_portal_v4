<?php

require_once "../../config.inc.php";

$s = new Spedizioni();


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_set_flag_incassato'){
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDFN16=1 WHERE TDDOCU=?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	
	
	$m_params = acs_m_params_json_decode();	
	foreach ($m_params->list_selected_id as $k_ordine){
		$result = db2_execute($stmt, array($k_ordine));				
	}
	
 echo acs_je(array('success' => true));
 exit;
}




// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){

	//bug parametri linux
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['form_ep'] = strtr($_REQUEST['form_ep'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	
	$form_ep = (array)json_decode($_REQUEST['form_ep']);
	
	if (strlen(trim($form_ep['f_cliente_cod'])) > 0)
		$m_where .= " AND TDCCON = " . sql_t(sprintf("%09s", $form_ep['f_cliente_cod']));	
	if (strlen(trim($form_ep['f_data_prod_da'])) > 0)
		$m_where .= " AND TDDTEP >= {$form_ep['f_data_prod_da']}";
	if (strlen(trim($form_ep['f_data_prod_a'])) > 0)
		$m_where .= " AND TDDTEP <= {$form_ep['f_data_prod_a']}";	
	

	if (isset($form_ep['f_referente_cliente']) && count($form_ep['f_referente_cliente']) > 0){
		if (is_array($form_ep['f_referente_cliente']))
			if (count($form_ep['f_referente_cliente']) == 1)
				$m_where .= " AND TDCOCL = " . sql_t($form_ep['f_referente_cliente'][0]);
			else
				$m_where .= " AND TDCOCL IN (" . sql_t_IN($form_ep['f_referente_cliente']) . ") ";
			else
				$m_where .= " AND TDCOCL = " . sql_t($form_ep['f_referente_cliente']);
	}	

	
	if (strlen(trim($form_ep['f_area'])) > 0)
		$m_where .= " AND TA_ITIN.TAASPE = '{$form_ep['f_area']}'";
	if (strlen(trim($form_ep['f_rischio'])) > 0)
		$m_where .= " AND TDRISC = '{$form_ep['f_rischio']}'";	

	
	if (isset($form_ep['f_pagamento']) && count($form_ep['f_pagamento']) > 0){
		if (is_array($form_ep['f_pagamento']))
			if (count($form_ep['f_pagamento']) == 1)
				$m_where .= " AND TDCPAG = " . sql_t($form_ep['f_pagamento'][0]);
			else
				$m_where .= " AND TDCPAG IN (" . sql_t_IN($form_ep['f_pagamento']) . ") ";
		else
			$m_where .= " AND TDCPAG = " . sql_t($form_ep['f_pagamento']);
	}
	
		
	if (isset($form_ep['f_itinerario']) && count($form_ep['f_itinerario']) > 0){
		if (is_array($form_ep['f_itinerario']))
			if (count($form_ep['f_itinerario']) == 1)
				$m_where .= " AND TDCITI = " . sql_t($form_ep['f_itinerario'][0]);				
			else
				 $m_where .= " AND TDCITI IN (" . sql_t_IN($form_ep['f_itinerario']) . ") ";
		else
			$m_where .= " AND TDCITI = " . sql_t($form_ep['f_itinerario']);
	}
	
	if (isset($form_ep['f_vettore']) && count($form_ep['f_vettore']) > 0){
		if (is_array($form_ep['f_vettore']))
			if (count($form_ep['f_vettore']) == 1)
				$m_where .= " AND SP.CSCVET = " . sql_t($form_ep['f_vettore'][0]);
			else
				$m_where .= " AND SP.CSCVET IN (" . sql_t_IN($form_ep['f_vettore']) . ") ";
			else
				$m_where .= " AND SP.CSCVET = " . sql_t($form_ep['f_vettore']);
	}
	

	if (count($form_ep['f_stadio']) > 0){
		if (is_array($form_ep['f_stadio']))
			$where .= " AND CAL.CSFG01 IN (" . sql_t_IN($form_ep['f_stadio']) . ") ";
		else
			$where .= " AND CAL.CSFG01 = " . sql_t($form_ep['f_stadio']);
	}	
	
	
	
	
	//costruzione sql
	
	global $cfg_mod_Spedizioni;
	
	
	global $auth, $s;
	$js_parameters = $auth->get_module_parameters($s->get_cod_mod());	
	if ($js_parameters->alert_CF == 1)
		$m_where .= " AND TDDTCF > 0";
	
	
	//solo quelle non gia' spuntate
	$m_where .= " AND TDFN16=0";
	
	$sql = "SELECT TDDTEP, TDDTDS, TDCCON, TDDCON
			, TDDT, TDOTID, TDOINU, TDOADO, TDONDO, TDDOCU
			, TDVSRF, TDOTPD, TDSTAT, TDDOCL, TDCOCL, CSCITI
			, TDAACA, TDTPCA, TDNRCA, CSCVET, TDTOTD, max(NTMEMO), TDTDES
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			 INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP ON TD.TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
							  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT 
							  ON TDDOCU=NTKEY1 AND NTTPNO='NCONT'							  
					WHERE TD.TDDT='$id_ditta_default' {$m_where} 
					GROUP BY TDDTEP, TDDTDS, TDCCON, TDDCON, TDDT, TDOTID, TDOINU, TDOADO, TDONDO, TDDOCU, TDVSRF, TDOTPD, TDSTAT, TDDOCL, TDCOCL, CSCITI, TDAACA, TDTPCA, TDNRCA, CSCVET, TDTOTD, TDTDES
					ORDER BY TDDTEP, TDONDO
					";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$ar = array();
	$ar_tot = array();	
	
	$autDeroghe = new SpedAutorizzazioniDeroghe();
	$as			= new SpedAssegnazioneOrdini();
	
	while ($r = db2_fetch_assoc($stmt)) {		
		$k_ordine = $r['TDDOCU'];
		$r['k_ordine'] = $k_ordine;		
		$r['k_ordine_out'] = implode('_' , array(trim($r['TDOADO']), trim($r['TDONDO']), trim($r['TDOTPD'])));				
		$r['k_carico_out'] = $s->k_carico_td_encode($r);
		
		
		//Note per ordine
		$ar_note = $s->get_commento_ordine_by_k_ordine($k_ordine);
		$r['NOTE'] = implode('<br>', $ar_note['text']);
		if (strlen(trim($r['NTMEMO'])) > 0)
			$r['ha_note'] = 1; 
		$r['TDDCON'] = acs_u8e($r['TDDCON']);
		$r['VETTORE_D'] = $s->decod_std('AUTR', $r['CSCVET']);
		
		if ($r['TDTDES'] == '1' || $r['TDTDES'] == '2'){
			$r['conto_deposito'] = 1;
		}		
		
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}











// ******************************************************************************************
// GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_grid'){
	
	$m_params = acs_m_params_json_decode();
	
?>
	
{"success":true, "items":
 {
  xtype: 'panel',
  title: 'Contrassegni',
  id: 'panel-contrassegni',
  <?php echo make_tab_closable(); ?>,
  
        tbar: new Ext.Toolbar({
            items:['<b>Gestione contrassegni</b>', '->',
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('grid').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
         ]            
        }),  
  
  autoScroll: true,    
  items: [		
	{
		xtype: 'grid',
		loadMask: true,
		multiSelect: true,	
		
		
			viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = '';
		           if (record.get('fl_incassato') == 'Y')
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    },
			
		
		features: [
		new Ext.create('Ext.grid.feature.Grouping',{
			groupHeaderTpl: 'Raggruppamento: {name}',
			hideGroupedHeader: false
		}),
		{
			ftype: 'filters',
			// encode and local configuration options defined previously for easier reuse
			encode: false, // json encode the filter query
			local: true,   // defaults to false (remote filtering)
	
			// Filters are most naturally placed in the column definition, but can also be added here.
		filters: [
		{
			type: 'boolean',
			dataIndex: 'visible'
		}
		]
		}],
	
		store: {
			xtype: 'store',
				
			groupField: 'TDDTEP',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {form_ep: <?php echo j(acs_je($m_params->form_open)); ?>}				
			},
				
			fields: [
				'RDFORN', 'RDDFOR', 'RDTPNO', 'TDDTEP', 'TDDTDS', 'TDCCON', 'TDDCON', 'TDOADO', 'TDONDO', 'TDDOCU', 'TDVSRF', 'TDOTPD', 'TDSTAT', 'TDDOCL', 'TDCOCL', 'CSCITI', 'VETTORE_D',
				'RDART', 'RDDES1', 'RDUM', 'RDDTDS', 'RDODER', 'RDRIFE', 'RDDES2', 'RDQTA', 'k_ordine', 'k_ordine_out', 'k_carico_out', 'NOTE', 'ha_note', 'tooltip_deroghe', 'ha_POSTM', 'TDTOTD', 'NTMEMO',
				'fl_incassato', 'conto_deposito'
					]
						
						
		}, //store
	
		columns: [
		{header: 'Data', renderer: date_from_AS, dataIndex: 'TDDTEP', width: 80, filter : {width: 150}},						
		{header: 'Cliente',  dataIndex: 'TDDCON', flex: 1, filter: {type: 'string'}, filterable: true},
		{header: 'Ordine',  dataIndex: 'k_ordine_out', width: 100, filter: {type: 'string'}, filterable: true},
		{header: 'St',  dataIndex: 'TDSTAT', width: 40, filter: {type: 'string'}, filterable: true},
		{header: 'Importo', align: 'right', dataIndex: 'TDTOTD', width: 70, filter: {type: 'numeric'}, filterable: true, renderer: floatRenderer2},
		{header: 'Carico',  dataIndex: 'k_carico_out', width: 100, filter: {type: 'string'}, filterable: true},		
		{header: 'Vettore',  dataIndex: 'VETTORE_D', width: 100, filter: {type: 'string'}, filterable: true},
		{header: '<img src=<?php echo img_path("icone/48x48/exchange.png") ?> width=25>', tooltip: 'In conto deposito',  dataIndex: 'conto_deposito', width: 30, filter: {type: 'string'}, filterable: true,
				renderer: function(value, metaData, record){				
			    	if (record.get('conto_deposito')==1) return '<img src=<?php echo img_path("icone/48x48/exchange.png") ?> width=18>';			    	
			    }
		},
		
		{header: 'Riferimento',  dataIndex: 'TDVSRF', flex: 1, filter: {type: 'string'}, filterable: true},
		{header: '<img src=<?php echo img_path("icone/48x48/comment_edit.png") ?> width=25>', tooltip: 'Ha note',  dataIndex: 'ha_note', width: 30, filter: {type: 'string'}, filterable: true,
				renderer: function(value, metaData, record){
				
					if (record.get('ha_note')==1){
	    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('NTMEMO')) + '"';					
					}				
				
			    	if (record.get('ha_note')==1) return '<img src=<?php echo img_path("icone/48x48/comment_edit.png") ?> width=18>';			    	
			    	}		
		},
		
		
		{header: 'Referente Cliente',  dataIndex: 'TDDOCL', flex: 0.8, filter: {type: 'string'}, filterable: true}		
		],
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){

					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    
				    if (col_name == 'k_ordine_out'){						
						mp = Ext.getCmp('m-panel');
						mp.add(
						  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_json.php?tipo_cal=iti_data&cod_iti=' + rec.get('CSCITI') + '&data=' + rec.get('TDDTEP'), Ext.id())
			            ).show();
						return false; //previene expand/collapse di default						
				    }
				    
				
					rec = iView.getRecord(iRowEl);	
					show_win_art_critici(rec.get('k_ordine'), 'MTO');	
				}
			},

		  itemcontextmenu : function(grid, rec, node, index, event) {			  	

				  event.stopEvent();
			      var record = grid.getStore().getAt(index);		  
			      var voci_menu = [];
				      
				  id_selected = grid.getSelectionModel().getSelection();
				  list_selected_id = [];
			  	  for (var i=0; i<id_selected.length; i++) 
				    list_selected_id.push(id_selected[i].data.id);
				  
	      
						  voci_menu.push({
				      		text: 'Gestione note',
				    		iconCls: 'iconArtCritici',
				    		handler: function() {

									if (id_selected.length > 1){
										acs_show_msg_error('Selezionare un singolo ordine');
										return false;			
									}				  

									
									//costruisco la chiave della nota
									r = id_selected[0];
									
									
			                   	my_listeners = {
		        					afterSave: function(){	
		        						id_selected[0].set('ha_note', 1);
		        						
		        						//recupero la cella (ToDo: la recupero perche' in posizione 9: recuperare by name)
		        						var el = Ext.get(Ext.get(node)['dom']['children'][9]);
    									
    									
    									
										//carico la form dal json ricevuto da php
										Ext.Ajax.request({
										        url        : 'acs_form_json_note_std.php?fn=get_nota_txt',
										        jsonData: {n_key: {
										  			key1: id_selected[0].get('k_ordine'), 
										  			tpno: 'NCONT'
										  		}},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										    	    var jsonData = Ext.decode(result.responseText);
										            el.set({'data-qtip': jsonData.txt});				            
										        },
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });				    			
    									
    									
		        						
		        						// metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('NTMEMO')) + '"';
						        		}
				    				};		
									
									
								  acs_show_win_std('Gestione note', 'acs_form_json_note_std.php?fn=gest', {
								  		n_key: {
								  			key1: id_selected[0].get('k_ordine'), 
								  			tpno: 'NCONT'
								  		}
								  	}, 900, 550, my_listeners, 'icon-shopping_cart_gray-16');    							    			
				    		}
						  });
						  
						  
						  
						  
						  voci_menu.push({
				      		text: 'Segna come incassato',
				    		iconCls: 'iconArtCritici',
				    		handler: function() {

				    		
				    		
							  list_selected_id = [];
						  	  for (var i=0; i<id_selected.length; i++) 
							    list_selected_id.push(id_selected[i].get('k_ordine'));
									
			                   	my_listeners = {
		        					afterSave: function(){	
		        						id_selected[0].set('ha_note', 1);		        									            
						        		}
				    			};		
								

				    			
								//se confermato rimuovo il legame tra le spedizioni
									    Ext.Msg.confirm('Richiesta conferma', 'Confermi?', function(btn, text){
									      if (btn == 'yes'){
									      	//Ext.getBody().mask('Loading... ', 'loading').show();
									        Ext.Ajax.request({
									            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_set_flag_incassato',
										            jsonData: {list_selected_id: list_selected_id},
										            method: 'POST',
										            success: function ( result, request) {
										                var jsonData = Ext.decode(result.responseText);
										                
										                for (var i=0; i<id_selected.length; i++) 
							    						  list_selected_id.push(id_selected[i].set('fl_incassato', 'Y'));
										            },
										            failure: function ( result, request) {
										            }
										        });														      		

										      } else {
										        //nothing
										      }
										    });						    			
    			
    			
				    			
				    			
							

							
									
    							    			
				    		}
						  });						  
						  
						  
				      
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);		  
				
				
				}, //itemcontextmenu			
			
			
	        itemclick: function(view,rec,item,index,eventObj) {
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');  
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');      	

	        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdi.store.load();	        	
	  			wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdc.store.load();
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				      //wd.setSource(src.dettagli);
				   }
				});		
				
				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}				
														        	
	         }			
			
		}
		 
	}
]
 } 

}	
	
	
<?php 	
  exit();
}