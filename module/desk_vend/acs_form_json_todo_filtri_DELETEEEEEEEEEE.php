<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();

///$get_todo_filtro_ordini = $s->get_todo_filtro_ordini();


//non piu' utilizzato: salvo i filtri in extraParams
// ******************************************************************************************
// DATI PER GRID (
// ******************************************************************************************
if ($_REQUEST['fn'] == 'save_filtri'){
	$s->set_todo_filtro_ordini((array)json_decode($_REQUEST['form_ep']));
	
	$ret['success'] = true;
	echo acs_je($ret);	
	exit;
}


?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
            items: [{
		                    xtype: 'checkboxgroup',
		                    layout: 'hbox',
		                    fieldLabel: 'Solo stadio',
		                    flex: 3,
		                    defaults: {
//		                     width: 105,
		                     padding: '0 10 10 0'
		                    },
		                    items: [{
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Ice'
				                  , inputValue: '1'
				                  , width: 80                
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Slush'
				                  , inputValue: '2'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Water'
				                  , inputValue: '3'
				                  , width: 80                        
				                 }
		                   ]
		                }, {
					            xtype: 'combo',
								name: 'f_cliente_cod',
								fieldLabel: 'Cliente',
								minChars: 2,			
								allowBlank: false,            
					            store: {
					            	pageSize: 1000,            	
									proxy: {
							            type: 'ajax',
							            url : 'acs_get_select_json.php?select=search_cli_anag',
							            reader: {
							                type: 'json',
							                root: 'root',
							                totalProperty: 'totalCount'
							            }
							        },       
									fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
					            },
					                        
								valueField: 'cod',                        
					            displayField: 'descr',
					            typeAhead: false,
					            hideTrigger: true,
					            anchor: '100%',
					            
					            
					            listConfig: {
					                loadingText: 'Searching...',
					                emptyText: 'Nessun cliente trovato',
					                
					                // Custom rendering template for each item
					                getInnerTpl: function() {
					                    return '<div class=\"search-item\">' +
					                        '<h3><span>{descr}</span></h3>' +
					                        '[{cod}] {out_loc}' +
					                    '</div>';
					                }                
					                
					            },
					            
					            pageSize: 1000
					        }, 
					        
					        
						  {
							flex: 1,
							name: 'f_area',
							xtype: 'combo',
							fieldLabel: 'Area spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
								}						 
							}					        
					        
							
						 , {
							flex: 1,
							name: 'f_itinerario',
							xtype: 'combo',
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
								    ] 
								}						 
							}							
					        
					        
				],
			buttons: [{
	            text: 'Conferma',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	
	        	    arrivi = Ext.getCmp('grid-arrivi');
	        	                	
					m_p = Ext.merge({}, form.getValues());
					if (typeof(form.getValues().f_stadio) === 'undefined' && typeof(form.getValues().f_cliente_cod) === 'undefined')
					 icon_status = 'disattivo'
					else
					 icon_status = 'attivo'

					arrivi.store.proxy.extraParams = m_p;					
					arrivi.store.load();
	            	if (Ext.get("todo_fl_solo_con_carico_assegnato") != null){
	            	 //aggiorno l'icona del filtro (se ho applicato filtri o meno)
	            	  if(icon_status == 'disattivo')
	            	 	Ext.get('todo_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_disattivo.png"); ?>;
	            	  else
	            	  	Ext.get('todo_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_attivo.png"); ?>;
	            	}            	            	

		            this.up('window').close();            	                	                
	            }
	        }],             
				
        }
]}