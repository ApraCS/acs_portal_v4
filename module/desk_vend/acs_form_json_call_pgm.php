<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

?>

{"success":true, "items": [

        {
            xtype: 'form',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			                        
            
            items: [
				{
                	xtype: 'hidden',
                	name: 'fn',
                	value: 'exe_call_pgm'
                	},            

			, {
				name: 'f_pgm',
            	anchor: '100%',				
				xtype: 'combo',
				fieldLabel: 'Processo',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}, {name:'info'}],
				    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_std('BATCH', null, 5), '') ?> 	
					    ] 
				}						 
			}, {
					fieldLabel: 'Parametri',
                	xtype: 'textfield',
                	name: 'user_parameters',
                	disabled: false
            }, {
                	xtype: 'textfield',
                	name: 'info',
                	disabled: true
            }			
			

            ],
            
            
			buttons: [{
	            text: 'Esegui',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',	            
	            handler: function() {
	            	var form = this.up('form').getForm();
					
				Ext.MessageBox.show({
				           msg: 'Richiamo procedura in corso',
				           progressText: 'Running...',
				           width:300,
				           wait:true, 
				           waitConfig: {interval:200},
				           icon:'ext-mb-download', //custom class in msg-box.html
				           //animateTarget: 'mb7'
				       });					
					
					
					form.submit(
								{
		                            success: function(form,action) {
		                            
		                            	//chiudo il msg box di attesa
										Ext.MessageBox.hide();		                            

		                            	Ext.MessageBox.show({
									           title: 'Esito progedura',
									           msg: action.result.esito_LK_AREA,
									           //msg: 'Procedura terminata',
									           buttons: Ext.MessageBox.OK
									       });										
												                            	
		                            },
		                            failure: function(form,action){
		                            	//chiudo il msg box di attesa
										Ext.MessageBox.hide();
												                            
		                                acs_show_msg_error('errore nel richiamo della funzione');
		                            }
		                        }					
					);
            	                	                
	            }
	        }],             
				
        }
]}