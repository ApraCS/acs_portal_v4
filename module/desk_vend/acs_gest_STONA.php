<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
    'tab_name' =>  $cfg_mod_Spedizioni['file_tabelle'],
    
    'TATAID' => 'STONA',
    'descrizione' => 'Gestiona tabella stati ordine non ammessi in assegna spedizione',
    
    'fields_key' => array('TAKEY1', 'TAKEY2', 'TAASPE'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Stato'),
        'TAKEY2' => array('label'	=> 'Area sped.'),
        'TADESC' => array('label'	=> 'Descrizione'),
        'TAASPE' => array('label'	=> 'Area sped.')
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';