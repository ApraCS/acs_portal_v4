<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();


?>

{"success":true, "items": [

				{
					xtype: 'treepanel',
					id: 'calendario',
					selType: 'cellmodel',
					cls: 'tree-calendario',		
			        title: 'Plan',
			        collapsible: true,
			        useArrows: true,
			        rootVisible: false,
			        store: storecal,
			        multiSelect: true,
			        singleExpand: false,
			        loadMask: true,
			        
					listeners: {
							load: function(sender, node, records) {									
						        	var c = Ext.getCmp('calendario');
						        	
						        	Ext.getBody().unmask();
						        	
									if (c.store.proxy.reader.jsonData.toggle_fl_solo_con_carico_assegnato == 1)
										Ext.get('cal_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_attivo.png"); ?>;
									else
										Ext.get('cal_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_disattivo.png"); ?>;																		
						        	
									c.headerCt.getHeaderAtIndex(5).setText(c.store.proxy.reader.jsonData.columns.c1);						        	
									c.headerCt.getHeaderAtIndex(6).setText(c.store.proxy.reader.jsonData.columns.c2);									
									c.headerCt.getHeaderAtIndex(7).setText(c.store.proxy.reader.jsonData.columns.c3);						        	
									c.headerCt.getHeaderAtIndex(8).setText(c.store.proxy.reader.jsonData.columns.c4);									
									c.headerCt.getHeaderAtIndex(9).setText(c.store.proxy.reader.jsonData.columns.c5);									
									c.headerCt.getHeaderAtIndex(10).setText(c.store.proxy.reader.jsonData.columns.c6);									
									c.headerCt.getHeaderAtIndex(11).setText(c.store.proxy.reader.jsonData.columns.c7);									
									c.headerCt.getHeaderAtIndex(12).setText(c.store.proxy.reader.jsonData.columns.c8);						        	
									c.headerCt.getHeaderAtIndex(13).setText(c.store.proxy.reader.jsonData.columns.c9);									
									c.headerCt.getHeaderAtIndex(14).setText(c.store.proxy.reader.jsonData.columns.c10);						        	
									c.headerCt.getHeaderAtIndex(15).setText(c.store.proxy.reader.jsonData.columns.c11);									
									c.headerCt.getHeaderAtIndex(16).setText(c.store.proxy.reader.jsonData.columns.c12);									
									c.headerCt.getHeaderAtIndex(17).setText(c.store.proxy.reader.jsonData.columns.c13);									
									c.headerCt.getHeaderAtIndex(18).setText(c.store.proxy.reader.jsonData.columns.c14);									
							},

								
							celldblclick: {								

								  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
								  //fn: function(view,rec,item,index,eventObj) {
								  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
								  	rec = iView.getRecord(iRowEl);
							    	
									
									if (col_name=='task' && rec.get('liv')=='liv_totale'){										
										storecal.proxy.url= 'acs_get_calendario_tree_json.php?set_field=' + rec.get('id');
										storecal.load();
							        	storecal.proxy.url= 'acs_get_calendario_tree_json.php';										
									}
									

									if (col_name!='task' && rec.get('liv')=='liv_totale'){										
										window.open ('acs_print_riepilogo_programmate.php?da_data=' +rec.get('da_data') + '&col_name=' + col_name,"mywindow"); 										
									}
									 						    	
							    	
							    	if (col_name!='task' && (rec.get('liv')=='liv_2' || rec.get('liv')=='liv_3' || rec.get('liv')=='liv_4')){							    							    									    		
            							mp = Ext.getCmp('m-panel');
            							mp.add(
											show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_from_calendar_json.php?m_id=' + rec.get('id') + '&da_data=' +rec.get('da_data') + '&col_name=' + col_name, Ext.id())
						            	).show();
									  return false; //previene expand/collapse di default
							    	}
							    	
							    }
							}						
					   },
			
					viewConfig: {
					        //row class CALENDARIO
					        getRowClass: function(record, index) {
					        	v = record.get('liv');
						        	if (v=="liv_totale"){
						        		
							        	c = Ext.getCmp('calendario');						        						        	
										if ('tot_' + c.store.proxy.reader.jsonData.field_dett == record.get('id'))
							        		v = v + ' dett_selezionato';												        		
						        	} //la riga di totale selezionata					        	
					            return v;					            
					         	}   
					    },			
			
			
					<?php
							$ss = "";
							// $ss .= "<img src=" . img_path("icone/48x48/button_grey_repeat.png") . " height=30>";		
							$ss .= "<img id=\"cal_prev_1w\"src=" . img_path("icone/48x48/button_grey_rew.png") . " height=30>";
							$ss .= "<img id=\"cal_prev_1d\"src=" . img_path("icone/48x48/button_grey_first.png") . " height=30>";						
							$ss .= "<img id=\"cal_next_1d\" src=" . img_path("icone/48x48/button_grey_last.png") . " height=30>";		
							$ss .= "<img id=\"cal_next_1w\" src=" . img_path("icone/48x48/button_grey_ffw.png") . " height=30>";
							$ss .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							
							//FILTRO PER SOLO ORDINI CON CARICO ASSEGNATO
							$img_name = ($main_module->get_fl_solo_con_carico_assegnato() == 1) ? "filtro_attivo" : "filtro_disattivo";
							$ss .= "<span id=\"cal_fl_solo_con_carico_assegnato\" class=acs_button style=\"\">";							
							 $ss .= "<img id=\"cal_fl_solo_con_carico_assegnato_img\" src=" . img_path("icone/48x48/{$img_name}.png") . " height=30>";														
							 //$ss .="<span id=\"cal_fl_solo_con_carico_assegnato_lbl\">Solo con carico assegnato</span>";
							$ss .= "</span>";
							
							//img per intestazioni colonne flag
							$cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=25>";
							$cf2 = "<img src=" . img_path("icone/48x48/power_black.png") . " height=25>";
							$cf3 = "<img src=" . img_path("icone/48x48/warning_black.png") . " height=25>";
							$cf4 = "<img src=" . img_path("icone/48x48/button_blue_pause.png") . " height=25>";																					
					?>		
			
			
			        //the 'columns' property is now 'headers'
			        columns: [{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'task', 
			            flex: 25,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            text: '<?php echo $ss; ?>'
			        },{
         			    text: '<?php echo $cf1; ?>',
         			    width: 30,
            		    tdCls: 'tdAction',         			
			            menuDisabled: true, sortable: false,            		        
						renderer: function(value, p, record){if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
        			  },{
         			    text: '<?php echo $cf2; ?>',
         			    width: 30,
            		    tdCls: 'tdAction',         			
			            menuDisabled: true, sortable: false,            		        
						renderer: function(value, p, record){if (record.get('fl_ord_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';}         			    
        			  },{
         			    text: '<?php echo $cf3; ?>',
         			    width: 30,
            		    tdCls: 'tdAction',         			
			            menuDisabled: true, sortable: false,            		        
						renderer: function(value, p, record){if (record.get('fl_art_mancanti')==1) return '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=18>';}         			    
        			  },{
         			    text: '<?php echo $cf4; ?>',
			            dataIndex: 'fl_da_prog',         			    
         			    width: 30,
            		    tdCls: 'tdAction',         			
			            menuDisabled: true, sortable: false,            		        
						renderer: function(value, p, record){if (record.get('fl_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';}         			    
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_1',
            		   align: 'right',
            		   id: "col_cal_d_1",
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_2',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_3',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_4',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_5',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_6',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_7',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass            		   
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_8',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_9',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_10',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_11',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_12',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_13',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass            		               		   
        			  },{
         			   text: '&nbsp;<br>&nbsp;',
            		   width: 50,
            		   dataIndex: 'd_14',
            		   align: 'right',
            		   menuDisabled: true, sortable: false,
            		   renderer: getCellClass            		   
        			  }        			          			  
			        ]        
      		}

]}