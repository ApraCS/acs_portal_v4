<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

$stato_abilita_chiusura = $cfg_mod_Spedizioni['stato_abilita_chiusura_oe'];  //ToDo: rendere parametrico


// ******************************************************************************************
// Apertura: richiamo generazione dati su Sv2
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_open'){
    $ret = array();
    $sh = new SpedHistory();
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'CALL_OENTRY',
            "k_ordine" => $m_params->k_ordine,
            "vals" => array()
        )
     );
    
    $ord = $s->get_ordine_by_k_docu($m_params->k_ordine);
    
    $ret['success'] = true;
    $ret['ord'] = $ord;
    
    echo acs_je($ret);
    exit;
}



// ******************************************************************************************
// Chiusura
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_close'){
    $ret = array();
    $sh = new SpedHistory();
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'FINE_OENTRY',
            "k_ordine" => $m_params->k_ordine,
            "vals" => array()
            )
        );
    $ret['success'] = true;
    echo acs_je($ret);
 exit;   
}





$row  = $s->get_ordine_by_k_ordine($m_params->k_ordine);
$data = print_date($row['TDODRE']);
$cliente = "[".trim($row['TDCCON'])."] ".trim($row['TDDCON']);
$ordine = trim($row['TDOADO'])."_".trim($row['TDONDO'])."_".trim($row['TDOTPD']);
$stato_ord  = trim($row['TDSTAT']);
$riferimento  = trim($row['TDVSRF']);

$link_action = 'acs://C:/tecne/Metron4/ApraToMetron.exe_'.trim($row['TDONDO']);


$link_open = '<a style = "margin-left: 50%;
                     padding: 10px 10px;
                     font-weight: bold; font-size: 13px; 
                     border-style: solid; border-width: 2px;
                     color: green;" 
            href="acs://C:/tecne/Metron4/ApraToMetron.exe_'.trim($row['TDONDO']).'" target= "_blank">APRI Sessione order enrty</a>';

$link_close = '<a style = "margin-left: 50%;
                     padding: 10px 10px;
                     font-weight: bold; font-size: 13px;
                     border-style: solid; border-width: 2px;
                     color: red;"
            onclick="console.log(\'aaaaa\');">CHIUDI Sessione order enrty</a>';



$link_close = '<form>
            <button type="submit" 
                formtarget="_blank"
                formaction="' . $link_action . '">
                APRI Sessione order enrty
            </button>
         </form>';

$testo = "Cliente {$cliente} <br> Ordine selezionato :  <b>{$ordine}</b> del {$data} Rif.: <b>{$riferimento}</b>";


//abilita/disabilita bottoni
if ($stato_ord == $stato_abilita_chiusura){
    $button_open_disabled  = true;
    $button_close_disabled = false;
} else {
    $button_open_disabled  = false;
    $button_close_disabled = true;
}

if($cfg_mod_Spedizioni['software_progettazione_grafica'] == 'METRON'){
    $button_open_disabled  = false;
}else{
    $button_open_disabled  = true;
}
        		            
        		              

?>

{"success":true, "items": [

      {
			xtype: 'form',
			layout : {
               type :'vbox',
               pack: 'start', 
               align: 'stretch'
            },

			defaults: {
				xtype: 'button', 				
				bodyStyle:'padding:10px; margin: 5px 10px 5px 10px; font-size: 15px;',
				fieldStyle: 'font-weight: bold;'
			},

			items: [ 			
				{flex: 1, xtype: 'panel', html: <?php echo j($testo); ?>, border: false, margin: 0},
				/*{itemId: 'stato_out', flex: 1, xtype: 'panel', html: 'Stato: <b><?php echo $stato_ord;?></b>', border: false,
					padding: 0, margin: 0, bodyStyle:'padding: 0 10 0 10; margin: 0px 10px 0px 10px; font-size: 20px;',},*/
				{flex: 1, 
					text: '<h1 style="font-size: 20px; color: green;">APRI Sessione order entry</h1>',
					itemId: 'b_open',
					disabled: <?php echo j($button_open_disabled); ?>, 
					margin: '15px',
					handler: function(){
					
						var me = this;
					
    					Ext.Ajax.request({
        				   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_open',
        				   method: 'POST',
        				   jsonData: {k_ordine: <?php echo j($m_params->k_ordine); ?>}, 
        				   
        				   success: function(response, opts) {
        				   	  Ext.getBody().unmask();
        					  var jsonData = Ext.decode(response.responseText),
        					  	  rec_ord  = jsonData.ord;

        					  var b_open  = me.up('window').down('#b_open'),
        					      b_close = me.up('window').down('#b_close'),
        					      html_stato = me.up('window').down('#stato_out'),
        					      m_form  = me.up('form');
        					  
        					  //eventualmente aggiorniamo stato su ToDo tree di partenza
        					  m_form.update_record_on_todo_grid();
        					  
        					  html_stato.update('Stato: <b>' + rec_ord.TDSTAT + '</b>');
        					  
        					  if (rec_ord.TDSTAT == <?php echo j($stato_abilita_chiusura)?>){
        					  	b_open.disable();
        					  	b_close.enable(); 
        					  } else {
        					  	b_open.enable();
        					  	b_close.disable(); 
        					  }
        					  
        					  //apertura ordine su programma grafico	
        					  window.open(<?php echo j($link_action)?>, '_blank'); 
        		              
        				   }, 
        				   failure: function(response, opts) {
        				      Ext.getBody().unmask();
        				      alert('error');
        				   }
        				});	
					}
				},
				{flex: 1, 
					text: '<h1 style="font-size: 20px; color: red;">CHIUDI Sessione order entry</h1>',
					disabled: <?php echo j($button_close_disabled); ?>,
					itemId: 'b_close', 
					margin: '15px',
					handler: function(){
								var loc_win = this.up('window');
									
									Ext.Ajax.request({
                    				   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_close',
                    				   method: 'POST',
                    				   jsonData: {k_ordine: <?php echo j($m_params->k_ordine); ?>}, 
                    				   
                    				   success: function(response, opts) {
                    				   	  Ext.getBody().unmask();
                    					  var jsonData = Ext.decode(response.responseText);									   	  
                    		              loc_win.close();  
                    				   }, 
                    				   failure: function(response, opts) {
                    				      Ext.getBody().unmask();
                    				      alert('error');
                    				   }
                    				});	
					}
				}, {
					xtype: 'container',
					layout : {
                       type :'vbox',
                       pack: 'start', 
                       align: 'stretch'
                    },
					margin: '0 15 20 30', height: 200,
					items:  
								
        				{
        				
        					xtype: 'grid',
        					flex: 1,	
        					title: 'Articoli ordinati',				        					
        					autoScroll: true,
        					
        					store: Ext.create('Ext.data.Store', {
        			
        						autoLoad:true,				        
        	  					proxy: {
        								url: 'acs_get_order_rows.php?type=MTO&solo_con_documento=Y&nord=' + <?php echo j($m_params->k_ordine); ?>,
        								type: 'ajax',
        								reader: {
        						            type: 'json',
        						            root: 'root'
        						        }
        							},
        		        			fields: [
        		            			'ditta_origine', 'TDDTEP', 'RDFMAN', 'RDDTDS', 'RDDFOR', 'RDDTEP', 'RDTPNO', 'RDOADO', 'RDONDO', 'RDRIFE', 'RDART', 'RDDART', 'RDDES1', 'RDDES2', 'RDUM', 'RDDT', {name: 'RDQTA', type: 'float'}
        		        			]
        		    			}),
        					
        					columns: [
        			             {
        			                header   : 'Disp. Sped.',
        			                dataIndex: 'RDDTDS', 
        			                width     : 70,
        			                renderer: date_from_AS
        			             },			        
        			             {
        			                header   : 'Stato',
        			                dataIndex: 'RDRIFE', 
        			                width     : 95
        			             }, {
        			                header   : 'Fornitore',
        			                dataIndex: 'RDDFOR', 
        			                flex    : 50
        			             }, {
        			                header   : 'Descrizione',
        			                dataIndex: 'RDDES1', 
        			                flex    : 150,
        			                renderer: function(value, p, record){
        			                	return record.get('RDDES1') + " [" + record.get('RDART') + "]"
        			    			}			                			                
        			             }, {
        			                header   : 'Um',
        			                dataIndex: 'RDUM', 
        			                width    : 30
        			             }, {
        			                header   : 'Q.t&agrave;',
        			                dataIndex: 'RDQTA', 
        			                width    : 50,
        			                align: 'right', renderer: floatRenderer2
        			             }, {
        			                header   : 'Consegna',
        			                dataIndex: 'RDDTEP', 
        			                flex    : 30,
        			                renderer: date_from_AS
        			             }, {
        			                header   : 'Documento',
        			                dataIndex: 'RDDES2', 
        			                flex    : 60
        			             }
        			            
        			         ]
        				
        				} //Grid articoli critici gia' ordinati a fornitore
        			}	
			],
			
			//update stato ordine in grid ToDo di apertura
			update_record_on_todo_grid: function(){				
				<?php if (strlen($m_params->grid_id) > 0 && strlen($m_params->id_tree_record) > 0){ ?>
					var m_from_grid = Ext.getCmp(<?php echo j($m_params->grid_id); ?>);
					if (!Ext.isEmpty(m_from_grid)){											    	  	 						
 						m_from_grid.panel.refresh_stato_per_record(<?php echo j($m_params->id_tree_record); ?>);
 					}
				<?php } ?>				
			}			
			
		}


    
	
     
]}