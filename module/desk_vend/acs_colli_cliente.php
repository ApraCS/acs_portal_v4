<?php

require_once "../../config.inc.php";
require_once("acs_colli_cliente_include.php");


$s = new Spedizioni();
$main_module = new Spedizioni();


if ($_REQUEST['fn'] == 'get_parametri_form'){

	//Elenco opzioni "Area spedizione"
	$ar_area_spedizione = $s->get_options_area_spedizione();
	$ar_area_spedizione_txt = '';

	$ar_ar2 = array();
	foreach ($ar_area_spedizione as $a)
		$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
		$ar_area_spedizione_txt = implode(',', $ar_ar2);

		 
		?>

{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: false,
	title: '',
    buttonAlign:'center',
	buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "REP_COLLI_CLI");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	             handler: function() {
	                form = this.up('form').getForm();
	                            	                
		               if (form.isValid()){	                	                
			               acs_show_panel_std('acs_colli_cliente.php', 'colli_cliente', {form_values: form.getValues()});
			               this.up('window').close(); 	  
		                }	
	                
	                               
	            }
	        }],             
            
            items: [
	            
	             {
					xtype: 'fieldset',
					layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data'
						   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , margin: "5 250 5 10"
			
						},
	             {
							name: 'f_areaspe',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 
								    ] 
							}						 
						},   	{
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "15 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?>	
								    ] 
							}						 
						}, 
						{
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							 margin: "20 10 10 10",	
							 anchor: '-15',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							anchor: '-15',
							margin: "20 10 10 10",
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							},{
						flex: 1,
			            xtype: 'combo',
						name: 'f_cliente',
						fieldLabel: 'Cliente',
						 anchor: '-15',
						 margin: "20 20 10 10",	
						minChars: 2,			
            
			            store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_get_select_json.php?select=search_cli',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['cod', 'descr', 'out_loc', 'ditta_orig'],		             	
			            },
                        
						valueField: 'cod',                        
			            displayField: 'descr',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun cliente trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{descr}</span></h3>' +
			                        '[{cod}] {out_loc} {ditta_orig}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000
        		}, {
							name: 'f_hold',
							xtype: 'radiogroup',
							fieldLabel: 'Hold',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "20 10 10 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Inclusi'
		                          , inputValue: 'A'
		                          , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Esclusi'
		                          , inputValue: 'Y'
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'N'
		                          
		                        }]
						}, 
					{
							name: 'f_evasi',
							xtype: 'radiogroup',
							fieldLabel: 'Ordini evasi',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "20 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: 'T'
		                          , width: 50
		                         <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] != 'Y') { ?>			                
			                      , checked: true
							<?php } ?>
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Da evadere'
		                          , inputValue: 'N'
								  <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') { ?>			                
			                      , checked: true
							      <?php } ?>
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Evasi'
								  , checked: false	
		                          , inputValue: 'Y'
		                          
		                        }]
						}
	             
	             ]
	             },                                     
            ]
        }

	
]}

<?php	
 exit;	
}

if ($_REQUEST['fn'] == 'get_data_tree'){

	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;
	
	 $sql_where=parametri_sql_where($form_values);
	 
	 $data_limite=$form_values->f_data;
	 
	 $ar = array();
				
				
				if($_REQUEST['node'] == 'SCADUTI'){
					
						
					$sql= "SELECT TDGGRI, TDSTAT, TDDSST, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ, SUM(TDTOCO) AS COLLI, SUM (TDCOPR) DISP,
					SUM(RD_PROD.RDQTA) AS COLLI_TOT_PROD, SUM(RD_PROD.RDQTA2) AS COLLI_DISP_PROD,
				    SUM(RD_RIV.RDQTA) AS COLLI_TOT_RIV, SUM(RD_RIV.RDQTA2) AS COLLI_DISP_RIV, 
				    SUM(TDVOLU) AS VOLUME, SUM(TDTIMP) AS IMPORTO,
				    TDBLEV, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO, TDFMTS, TDFN15, TDFN04, TDCITI
					FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
					LEFT OUTER JOIN
						(
			            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
			            FROM {$cfg_mod_Spedizioni['file_righe']} RD_PROD
			            WHERE RD_PROD.RDTPNO='COLLI' AND RD_PROD.RDRIGA=0 AND RD_PROD.RDRIFE = 'PRODUZIONE'
			            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
			            ) RD_PROD
			            ON TDDT=RD_PROD.RDDT AND TDOTID=RD_PROD.RDOTID AND TDOINU=RD_PROD.RDOINU AND TDOADO=RD_PROD.RDOADO AND TDONDO=RD_PROD.RDONDO
					LEFT OUTER JOIN
						(
			            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
			            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
			            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE <> 'PRODUZIONE'
			            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
			            ) RD_RIV
					ON TDDT=RD_RIV.RDDT AND TDOTID=RD_RIV.RDOTID AND TDOINU=RD_RIV.RDOINU AND TDOADO=RD_RIV.RDOADO AND TDONDO=RD_RIV.RDONDO					   
					   
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
						ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
					$sql_where AND TDDTEP < $data_limite GROUP BY TDGGRI, TDCITI, TDBLEV, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO, TDFMTS, TDFN15, TDFN04, TDSTAT, TDDSST, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ ORDER BY TDGGRI";
						
					
				
					$stmt= db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt);
					
					while ($r = db2_fetch_assoc($stmt)) {
				
						$tmp_ar_id = array('SCADUTI');
						$ar_new = array();
						$ar_r= &$ar;
				
						if($r['TDGGRI'] >=0 && $r['TDGGRI']<=5){
							$cod_liv1= "D_0_A_5";
							$ar_new['task'] = 'Da meno di 5 gg';
						}
						if($r['TDGGRI'] >5 && $r['TDGGRI']<=10){
							$cod_liv1= "D_5_A_10";
							$ar_new['task'] = 'Da meno di 10 gg';
						}
						if($r['TDGGRI'] >10 && $r['TDGGRI']<=15){
							$cod_liv1= "D_10_A_15";
							$ar_new['task'] = 'Da meno di 15 gg';
						}
						if($r['TDGGRI'] >15 && $r['TDGGRI']<=20){
							$cod_liv1= "D_15_A_20";
							$ar_new['task'] = 'Da meno di 20 gg';
						}
						if($r['TDGGRI'] >20 && $r['TDGGRI']<=25){
							$cod_liv1= "D_20_A_25";
							$ar_new['task'] = 'Da meno di 25 gg';
						}
						if($r['TDGGRI'] >22 && $r['TDGGRI']<=30){
							$cod_liv1= "D_25_A_30";
							$ar_new['task'] = 'Da meno di 30 gg';
						}
					    if($r['TDGGRI'] >30){
							$cod_liv1= "D_30_A_9999";
							$ar_new['task'] = 'Da oltre 30 gg';
					    }
					    
					    $cliente_dest=array();
					    $cliente_dest[0]=trim($r['TDCCON']);
					    $cliente_dest[1]=trim($r['TDCDES']);
					    	
					    $cod_liv2 = implode("_", $cliente_dest); //CLIENTE_DESTINAZIONE
					    
					    $cod_liv3=$r['TDSTAT'];
					    
					
				
							//$ar_r=&$ar_r['children'];
							$liv =$cod_liv1;
							$tmp_ar_id[] = $liv;
							if (!isset($ar_r["{$liv}"])){
							
							$ar_new['id'] = implode("|", $tmp_ar_id);
					        $ar_new['liv'] = 'liv_2';
					        $ar_new['gg_ritardo'] =  $r['TDGGRI'];
					      
							$ar_r["{$liv}"] = $ar_new;
								}
							$ar_r = &$ar_r["{$liv}"];
							sum_columns_value($ar_r, $r);
							
							
							$ar_r=&$ar_r['children'];
							$liv =$cod_liv2;
							$tmp_ar_id[] = $liv;
							if (!isset($ar_r["{$liv}"])){
								$ar_new = array();
								$ar_new['id'] = implode("|", $tmp_ar_id);
								$ar_new['task'] = $r['TDDCON'];
								$ar_new['data'] = $r['TDDTEP'];
								$ar_new['cod_iti'] = $r['TDCITI'];
								$ar_new['loca'] =$s->scrivi_rif_destinazione($r['TDLOCA'], $r['TDNAZI'], $r['TDPROV'], $r['TDDNAZ']);
								$ar_new['stato'] = $r['TDSTAT'];
						        $ar_new['gg_ritardo'] =  $r['TDGGRI'];
								$ar_new['liv'] = 'liv_3';
                                $ar_r["{$liv}"] = $ar_new;
							}
							$ar_r = &$ar_r["{$liv}"];
							sum_columns_value($ar_r, $r);
							
							$ar_r=&$ar_r['children'];
							$liv =$cod_liv3;
							$tmp_ar_id[] = $liv;
							if (!isset($ar_r["{$liv}"])){
								$ar_new = array();
								$ar_new['id'] = implode("|", $tmp_ar_id);
								$ar_new['task'] = "[".$r['TDSTAT']."] ".$r['TDDSST'];
								$ar_new['stato'] = $r['TDSTAT'];
								$ar_new['gg_ritardo'] =  $r['TDGGRI'];
							    $ar_new['liv'] = 'liv_4';
								$ar_new['leaf']=true;
								$ar_r["{$liv}"] = $ar_new;
							}
							$ar_r = &$ar_r["{$liv}"];
							sum_columns_value($ar_r, $r);
														
				
				
					}
					
					$ret = array();
					foreach($ar as $kar => $v){
						$ret[] = array_values_recursive($ar[$kar]);
					}
					
					echo acs_je(array('success' => true, 'children' => $ret));
					
					exit;
						
				}
				
				
				
				
				
				
		if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){

		$sql_group_by.=" GROUP BY CS.CSAARG, CS.CSNRSE, TDBLEV, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO, TDFMTS, TDFN15, TDFN04 ORDER BY CS.CSAARG, CS.CSNRSE";
		$data= "";
		$cliente= "";
		$dest= "";
		$stato= "";
		$cod_iti= "";
	
		}
		else{
			$anno_set= explode("_", $_REQUEST["node"]);
			$anno= $anno_set[0];
			$set= explode("|", $anno_set[1]);
			$sql_where.=" AND CS.CSAARG=$anno AND CS.CSNRSE=$set[0]";
			$sql_group_by.=" GROUP BY CS.CSAARG, CS.CSNRSE, TDBLEV, TDCITI, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO, TDFMTS, TDFN15, TDFN04, TDDTEP, TDSTAT, TDDSST, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ, TDONDO ORDER BY TDDTEP";
			$data= ", TDDTEP";
			$cod_iti= ", TDCITI";
			$cliente= ", TDCCON, TDDCON";
			$dest= ", TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ";
			$stato= ", TDSTAT, TDDSST";
			
			
		}
		
		
		//prima riga scaduti totali
				if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
					
					$ar['TOTALE']=array();
					$ar['TOTALE']['id'] = "TOTALE";
					$ar['TOTALE']['task'] = "TOTALE";
					$ar['TOTALE']['liv'] = 'liv_totale';
					$ar['TOTALE']['expanded']= true;
					
		
				$sql_1="SELECT SUM(TDTOCO) AS COLLI, SUM (TDCOPR) DISP,
				SUM(RD_PROD.RDQTA) AS COLLI_TOT_PROD, SUM(RD_PROD.RDQTA2) AS COLLI_DISP_PROD,
				SUM(RD_RIV.RDQTA) AS COLLI_TOT_RIV, SUM(RD_RIV.RDQTA2) AS COLLI_DISP_RIV,
				SUM(TDFN02) AS TDFN02, SUM(TDFN04) AS TDFN04,
				SUM(TDVOLU) AS VOLUME, SUM(TDTIMP) AS IMPORTO
				FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
					LEFT OUTER JOIN
						(
			            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
			            FROM {$cfg_mod_Spedizioni['file_righe']} RD_PROD
			            WHERE RD_PROD.RDTPNO='COLLI' AND RD_PROD.RDRIGA=0 AND RD_PROD.RDRIFE = 'PRODUZIONE'
			            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
			            ) RD_PROD
			            ON TDDT=RD_PROD.RDDT AND TDOTID=RD_PROD.RDOTID AND TDOINU=RD_PROD.RDOINU AND TDOADO=RD_PROD.RDOADO AND TDONDO=RD_PROD.RDONDO
				LEFT OUTER JOIN
					(
		            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
		            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
		            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE <> 'PRODUZIONE'
		            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
		            ) RD_RIV
					ON TDDT=RD_RIV.RDDT AND TDOTID=RD_RIV.RDOTID AND TDOINU=RD_RIV.RDOINU AND TDOADO=RD_RIV.RDOADO AND TDONDO=RD_RIV.RDONDO				
					
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
					ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
		        $sql_where AND TDDTEP < $data_limite";
				
		     //   print_r($sql_1);
		    
				
		        $stmt_1 = db2_prepare($conn, $sql_1);
		        echo db2_stmt_errormsg();
		        $result = db2_execute($stmt_1);
		        $r_1= db2_fetch_assoc($stmt_1);
			
					$ar_new = array();
					$ar_new['id'] = 'SCADUTI';
					$ar_new['colli_tot'] += $r_1['COLLI'];
					$ar_new['colli_disp'] +=$r_1['DISP'];
					$ar_new['colli_non_disp'] += ($r_1['COLLI'] - $r_1['DISP']);
					$ar_new['importo'] +=$r_1['IMPORTO'];
					$ar_new['volume'] +=$r_1['VOLUME'];
					$ar_new['fl_da_prog'] += $r_1['TDFN04'];
					$ar_new['fl_cli_bloc'] += $r_1['TDFN02'];
					$ar_new['task'] = 'Scaduti';
					$ar_new['liv'] = 'liv_1';
					$ar['TOTALE']['children']["SCADUTI"] = $ar_new;
			
					sum_columns_value($ar['TOTALE'], $r_1);
			
					}
		
		
			
				$sql= "SELECT CS.CSAARG, CS.CSNRSE $data $cliente $dest $stato $cod_iti, SUM(TDTOCO) AS COLLI, SUM (TDCOPR) AS DISP, 
				SUM(RD_PROD.RDQTA) AS COLLI_TOT_PROD, SUM(RD_PROD.RDQTA2) AS COLLI_DISP_PROD,
				SUM(RD_RIV.RDQTA) AS COLLI_TOT_RIV, SUM(RD_RIV.RDQTA2) AS COLLI_DISP_RIV, 
				SUM(TDVOLU) AS VOLUME, SUM(TDTIMP) AS IMPORTO,
				TDBLEV, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO, TDFMTS, TDFN15, TDFN04
				FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
					LEFT OUTER JOIN
						(
			            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
			            FROM {$cfg_mod_Spedizioni['file_righe']} RD_PROD
			            WHERE RD_PROD.RDTPNO='COLLI' AND RD_PROD.RDRIGA=0 AND RD_PROD.RDRIFE = 'PRODUZIONE'
			            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
			            ) RD_PROD					   
			            ON TDDT=RD_PROD.RDDT AND TDOTID=RD_PROD.RDOTID AND TDOINU=RD_PROD.RDOINU AND TDOADO=RD_PROD.RDOADO AND TDONDO=RD_PROD.RDONDO
				LEFT OUTER JOIN
					(
		            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
		            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
		            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE <> 'PRODUZIONE'
		            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
		            ) RD_RIV
					ON TDDT=RD_RIV.RDDT AND TDOTID=RD_RIV.RDOTID AND TDOINU=RD_RIV.RDOINU AND TDOADO=RD_RIV.RDOADO AND TDONDO=RD_RIV.RDONDO
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
					ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
		        $sql_where AND TDDTEP >= $data_limite $sql_group_by";
					
				
				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt);
				
				//print($sql);

				while ($r = db2_fetch_assoc($stmt)) {
					
					$tmp_ar_id = array();

					$ar_r= &$ar;
					
	
					//stacco dei livelli
					$anno_set= array();
					$anno_set[0]=trim($r['CSAARG']);
					$anno_set[1]=trim($r['CSNRSE']);
					
					$cod_totale ="TOTALE"; //totale generale
					$cod_liv0 = implode("_", $anno_set); //NUMERO ANNO_SETTIMANA					
					$cod_liv1 =trim($r['TDDTEP']); //data
					
					$cliente_dest=array();
					$cliente_dest[0]=trim($r['TDCCON']);
					$cliente_dest[1]=trim($r['TDCDES']);
					
					$cod_liv2 = implode("_", $cliente_dest); //CLIENTE_DESTINAZIONE
					$cod_liv3 =trim($r['TDSTAT']); //stato ordine
					
					
					$liv =$cod_totale;
					//$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();					
					}
					
					//facciamo sempre perche' TOTALE viene creato da SCADUTI
					
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);

					$ar_r=&$ar_r['children'];
					$liv =$cod_liv0;
					$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
							if ($ar_new['prod']==''){
							$ar_new['prod']=0;
							}
						$anno=trim($r['CSAARG']);
						$sett=trim($r['CSNRSE']);
						$ar_new['task'] = $anno." Settimana <b>".$sett."</b>";
					    $ar_new['liv'] = 'liv_1';
						$ar_r["{$liv}"] = $ar_new;
						
					}
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);
	    			
					if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
						continue;
					}
					
					//data
					$ar_r=&$ar_r['children'];
					$liv =$cod_liv1;
					$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
					    setlocale(LC_TIME, it_IT);
					    $ar_new['task'] = ucfirst(strftime("%a %d/%m", strtotime($liv)));
					    $ar_new['data'] = $r['TDDTEP'];
					    $ar_new['liv'] = 'liv_2';
					    $ar_r["{$liv}"] = $ar_new;
					}
					
				    $ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);
					
					//cliente_destinazione
					$ar_r=&$ar_r['children'];
					$liv =$cod_liv2;
					$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						$ar_new['task'] = $r['TDDCON'];
						$ar_new['cod_cli'] = $r['TDCCON'];
						$ar_new['data'] = $r['TDDTEP'];
						$ar_new['loca'] =$s->scrivi_rif_destinazione($r['TDLOCA'], $r['TDNAZI'], $r['TDPROV'], $r['TDDNAZ']);
						$ar_new['cod_iti'] = $r['TDCITI'];
					    $ar_new['liv'] = 'liv_3';
						$ar_r["{$liv}"] = $ar_new;
					}
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);
				
		
					//stato
					$ar_r=&$ar_r['children'];
					$liv =$cod_liv3;
					$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						$ar_new['task'] = "[".$r['TDSTAT']."] ".$r['TDDSST'];
						$ar_new['data'] = $r['TDDTEP'];
						$ar_new['stato'] = $r['TDSTAT'];
						$ar_new['cod_cli'] = $r['TDCCON'];
						$ar_new['liv'] = 'liv_4';
						$ar_new['leaf']=true;
						$ar_r["{$liv}"] = $ar_new;
					}
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);
					

				} //while
				
				
				if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
					$ar= $ar['TOTALE']['children'][$_REQUEST['node']]['children'];
				}
				
				
				$ret = array();
				foreach($ar as $kar => $v){
					$ret[] = array_values_recursive($ar[$kar]);
				}

				echo acs_je(array('success' => true, 'children' => $ret));
				
				exit;
				
}
				
				?>
				
				
				


{"success":true, "items": [

        {
            xtype: 'treepanel' ,
	        title: 'Customer' ,
	        tbar: new Ext.Toolbar({
	            items:['<b>Analisi disponibilit&agrave; colli</b>', '->'
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	    
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,            
			//multiSelect:true,
			singleExpand: false,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                                
				    fields: ['task', 'colli_tot', 'colli_disp', 'colli_non_disp', 'liv', 
				    'stato', 'data', 'loca', 'gg_ritardo', 'fl_cli_bloc', 'fl_bloc', 
				    'fl_art_manc', 'art_da_prog', 'fl_da_prog', 'cod_iti', 'volume', 'importo', 'cod_cli'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tree',
						actionMethods: {read: 'POST'},
						
						extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>
			        				},
                        
                       	reader: {
                            root: 'children'
                        },
                       	                      
                    	doRequest: personalizza_extraParams_to_jsonData        				
                    }

                }),
                
                <?php 
                //img per intestazioni colonne flag
                $cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=25>";
                $cf2 = "<img src=" . img_path("icone/48x48/power_black.png") . " height=25>";
                $cf4 = "<img src=" . img_path("icone/48x48/button_blue_pause.png") . " height=25>";
                ?>
     
		columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'descr', 
			            flex: 1,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            header: 'Settimana',
			               
			        }, {
                 
                 header: 'Localit&agrave;', 
                 dataIndex: 'loca', 
                  width: 250},
                  {
			    text: '<?php echo $cf1; ?>',
			    width: 30, tooltip: 'Clienti bloccati',
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_cli_bloc')>0) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
		    },
			        {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini bloccati',
	    			    renderer: function(value, metaData, record){
	    			    
	    			    	if (record.get('tooltip_ripro') != "")	    			    	
								//metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_ripro')) + '"';	    			    
	    			    
							/*if (record.get('stato') == 'V') metaData.tdCls += ' tpSfondoVerde';
							if (record.get('stato') == 'R') metaData.tdCls += ' tpSfondoRosa';
							if (record.get('stato') == 'G') metaData.tdCls += ' tpSfondoGiallo';
							if (record.get('stato') == 'B') metaData.tdCls += ' tpSfondoBianco';*/
	    			    
	    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
	    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
	    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
	    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';    			    				    	
	    			    	}},
			          {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', 
    	    		dataIndex: 'art_mancanti', menuDisabled: true, sortable: false,
    	    	    tooltip: 'Ordini MTO',    	    		    
    			    renderer: function(value, p, record){
    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=18>';
    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';			    	
    			    	}},  
    			    	
    			    	{text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
        			dataIndex: 'art_da_prog',
    	    	    tooltip: 'Articoli mancanti MTS', menuDisabled: true, sortable: false,        					    	     
    			    renderer: function(value, p, record){
    			    	if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
    			    	if (record.get('art_da_prog')==2) return '<img src=<?php echo img_path("icone/48x48/warning_red.png") ?> width=18>';    			    	
    			    	if (record.get('art_da_prog')==3) return '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=18>';
    			    	}},
    			    	
    			    	{
			    text: '<?php echo $cf4; ?>',
            dataIndex: 'fl_da_prog', tooltip: 'Ordini da programmare',         			    
			    width: 30,
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_da_prog')>0) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';}         			    
		  },
			        
			        {header: 'Colli',
                    columns: [
                      {header: 'Totali', dataIndex: 'colli_tot', align: 'right',  width: 100, tdCls: ' grassetto'}
                     , {header: 'Disponibili', dataIndex: 'colli_disp', align: 'right',  width: 100, tdCls: 'disponibile'}
                     , {header: 'Non disponibili', dataIndex: 'colli_non_disp', align: 'right',  width: 100, tdCls: 'non_disponibile'}
                  ]
                 },{
                 
                 header: 'Volume', 
                 dataIndex: 'volume', 
                 align: 'right',
                  renderer: floatRenderer2,
                  width: 100}
                  ,{
                     header: 'Importo', 
                 dataIndex: 'importo', 
                 align: 'right',
                  renderer: floatRenderer2,
                  width: 100}
                 
                 
	         ],	//columns	
	         
	         listeners: {
	         
	       		  celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.stopEvent();

						
						
					if (rec.get('liv')=='liv_3' && (col_name =='fl_da_prog') ){
						
							mp = Ext.getCmp('m-panel');
						
					if (parseInt(rec.get('fl_da_prog')) == 1)
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_from_calendar_json.php?get_iti_by_par=Y&col_name=fl_da_prog&cod_iti=' + rec.get('cod_iti') + '&da_data=' + rec.get('data'), Ext.id())
				            ).show();						
						else
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_json.php?tipo_cal=iti_data&cod_iti=' + rec.get('cod_iti') + '&data=' + rec.get('data'), Ext.id())
				            ).show();
						
						return false;
						}
													
						
						
						if ( rec.get('liv')=='liv_4' || rec.get('liv')=='liv_3'){
						
							iEvent.preventDefault();
							acs_show_win_std('Elenco ordini', 'acs_disp_colli_elenco_ordini.php', {
								form_values: form.getValues(),
								stato_selected: rec.get('stato'),
								data_selected:rec.get('data'),
								cliente_selected:rec.get('cod_cli'),
								id_scaduti_rit:rec.get('id')
							}, 1024, 600, {}, 'icon-leaf-16');
							
								return false;
							
						}
						
						

					 }
					}
					
			},
	        
 viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }
	        																			  			
	            
        }  

]
}

