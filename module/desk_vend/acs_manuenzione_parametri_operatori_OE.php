<?php
require_once "../../config.inc.php";
ini_set('max_execution_time', 300);

$s = new Spedizioni();
$main_module = new Spedizioni();



// ******************************************************************************************
// EXE_CALL (lancio programma assegnazione operatori OE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_call'){
	$m_params = acs_m_params_json_decode();

	$divs = array('', '', '', '', '');
	if (count($m_params->form_values->f_divisione) > 0) $divs[0] = $m_params->form_values->f_divisione[0];
	if (count($m_params->form_values->f_divisione) > 1) $divs[1] = $m_params->form_values->f_divisione[1];
	if (count($m_params->form_values->f_divisione) > 2) $divs[2] = $m_params->form_values->f_divisione[2];
	if (count($m_params->form_values->f_divisione) > 3) $divs[3] = $m_params->form_values->f_divisione[3];
	if (count($m_params->form_values->f_divisione) > 4) $divs[4] = $m_params->form_values->f_divisione[4];
	
	$aspes = array('', '', '', '', '');
	if (count($m_params->form_values->f_area) > 0) $aspes[0] = $m_params->form_values->f_area[0];
	if (count($m_params->form_values->f_area) > 1) $aspes[1] = $m_params->form_values->f_area[1];
	if (count($m_params->form_values->f_area) > 2) $aspes[2] = $m_params->form_values->f_area[2];
	if (count($m_params->form_values->f_area) > 3) $aspes[3] = $m_params->form_values->f_area[3];
	if (count($m_params->form_values->f_area) > 4) $aspes[4] = $m_params->form_values->f_area[4];
	
	
	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-20s", $m_params->form_values->OETPCA);
	$cl_p .= sprintf("%-3s",  $divs[0]);
	$cl_p .= sprintf("%-3s",  $divs[1]);
	$cl_p .= sprintf("%-3s",  $divs[2]);
	$cl_p .= sprintf("%-3s",  $divs[3]);
	$cl_p .= sprintf("%-3s",  $divs[4]);
	$cl_p .= sprintf("%-3s",  $aspes[0]);
	$cl_p .= sprintf("%-3s",  $aspes[1]);
	$cl_p .= sprintf("%-3s",  $aspes[2]);
	$cl_p .= sprintf("%-3s",  $aspes[3]);
	$cl_p .= sprintf("%-3s",  $aspes[4]);
	$cl_p .= sprintf("%-8s", $m_params->form_values->OEDTRG);

	if ($useToolkit == 'N') {
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.TPOR20C('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('TPOR20C', $libreria_predefinita_EXE, $cl_in, null, null);
	}

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}



// ******************************************************************************************
// EXE SAVE FROM CALENDAR
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_calendar'){
 $m_params = acs_m_params_json_decode();	
 $cell_values = $m_params->form_values;
 
 //elimino le righe gia presenti per P - Tipo calendario - Data
 $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_parametri_operatori_OE']} 
 			WHERE OEFLPS=? AND OETPCA=? AND OEDTRG=? AND OEDT=? ";
 $stmt = db2_prepare($conn, $sql);
 $result = db2_execute($stmt, array(
 		$m_params->open_request->form_values->OEFLPS,
 		$m_params->open_request->form_values->OETPCA,
 		$m_params->open_request->form_values->OEDTRG,
 		$m_params->open_request->form_values->OEDT
 ));

 
 foreach($cell_values as $kut => $val){
 	$utente_tipologia = explode("|", $kut);
 	
 	if (1==2 || strlen($val) > 0){ //2017-06-08: scrivo solo quelli in cui ho inserito un numero (per Stosa)

 		if (strlen($val) == 0) $val=0;
 		
	 	$ar_ins['OEFLPS'] 	= utf8_decode(trim($m_params->open_request->form_values->OEFLPS));
	 	$ar_ins['OETPCA'] 	= utf8_decode(trim($m_params->open_request->form_values->OETPCA));
	 	$ar_ins['OEDTRG'] 	= utf8_decode(trim($m_params->open_request->form_values->OEDTRG));
	 	$ar_ins['OEDT'] 	= utf8_decode(trim($m_params->open_request->form_values->OEDT));
	 	 	
	 	$ar_ins['OEUSER'] 	= $utente_tipologia[1];
	 	$ar_ins['OETOR1'] 	= $utente_tipologia[2];
	 	$ar_ins['OENRDO'] 	= (int)$val; 	
	 	
	 	//insert riga
	 	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_parametri_operatori_OE']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	 	$stmt = db2_prepare($conn, $sql);
	 	echo db2_stmt_errormsg(); 	
	 	$result = db2_execute($stmt, $ar_ins);
	 	echo db2_stmt_errormsg($stmt);
 	} 	
 }
 
 $ret = array();
 $ret['success'] = true;
 echo acs_je($ret);
 
 exit;
}



// ******************************************************************************************
// FORM DI RICHIESTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_params'){
	$m_params = acs_m_params_json_decode();
	
	$data_consegna_evidenziate_ar = $s->find_TA_std('OEDTE');
	if (count($data_consegna_evidenziate_ar) >=0) 
		$f_data_consegna_evidenziata = $data_consegna_evidenziate_ar[0]['id'];
	else 
		$f_data_consegna_evidenziata = 'null';
	
	if (isset($cfg_mod_Spedizioni["manutenzioni_parametri_operatori_OE"]["open_params"]["STD"]))
		$open_params_def = $cfg_mod_Spedizioni["manutenzioni_parametri_operatori_OE"]["open_params"]["STD"];
	else 
		$open_params_def = array();
	
?>
{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: true,
	title: '',
	url: 'acs_op_exe.php',
	
	layout: { 	type: 'vbox', pack: 'start', align: 'stretch'},	

	items: [		
					{name: 'OEDT', xtype: 'hidden', value: <?php echo j($id_ditta_default)?>},
					{name: 'from_panel_id', xtype: 'hidden', value: <?php echo j($m_params->from_panel_id); ?>},
					{name: 'OEFLPS', xtype: 'hidden', value: 'P'},
					 {
							name: 'OETPCA',
							xtype: 'combo',
							allowBlank: false, 		
							fieldLabel: 'Tipo calendario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
							value: <?php echo j($open_params_def['OETPCA']) ?>,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('DECAL', null, 1, null, null, null, $this), '') ?> 	
								    ] 
							}						 
					 }, {
						     name: 'OEDTRG'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data'
						   , format: 'd/m/Y'
						   //, minValue: '<?php echo print_date($min_data_AS, "%d/%m/%Y"); ?>'							   
						   //, maxValue: '<?php echo print_date($max_data_AS, "%d/%m/%Y"); ?>'						   
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   //, anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
					}
					
<?php if ($m_params->open_type != 'EXE_CALL') { ?>					
					, {
						     name: 'f_data_consegne_evidenziate'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data consegne evidenziate'
						   , format: 'd/m/Y'
						   , value: '<?php echo print_date($f_data_consegna_evidenziata, "%d/%m/%Y"); ?>'
						   //, minValue: '<?php echo print_date($min_data_AS, "%d/%m/%Y"); ?>'							   
						   //, maxValue: '<?php echo print_date($max_data_AS, "%d/%m/%Y"); ?>'						   
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   //, anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
					}
<?php } ?>					
					
					
					, {
							name: 'f_area',
							xtype: 'combo',
							allowBlank: false, 		
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
							multiSelect: true,
							value: <?php echo j($open_params_def['f_area']) ?>,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
							}						 
					 }, {
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							multiSelect: true,
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: false,
						   	value: <?php echo j($open_params_def['f_divisione']) ?>,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}
				],
			buttons: [
			
			
			
			{
	            text: 'Paramentri operatore OE',
	            
	              handler : function() {
	                           var form = this.up('form').getForm().getValues();
								<?php $cl = new SpedParametriOperatoriOE(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "parametrioperatorioe") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "parametrioperatorioe") ?>
								<?php echo $cl->out_Writer_Model("parametrioperatorioe") ?>
								<?php echo $cl->out_Writer_Store("parametrioperatorioe") ?>
								<?php echo $cl->out_Writer_sotto_main("parametrioperatorioe", 0.4) ?>								
								<?php echo $cl->out_Writer_main("Lista parametri operatori Order Entry", "parametrioperatorioe") ?>								
								<?php echo $cl->out_Writer_window("Tabella parametri operatori Order Entry") ?>							

								} //handler function()
	        },	{ xtype: 'tbfill' },
			
			
			
			
<?php if ($m_params->open_type != 'EXE_CALL') { ?>				
			{
	            text: 'Apri',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					if(form.isValid()){	            
						//loc_win.close();	
						acs_show_win_std('Parametri operatori OE - Manutenzione per calendario', 
								'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open', 
								{form_values: form.getValues(), main_panel_id: <?php echo j($m_params->main_panel_id); ?>}, 850, 600, null, 'icon-design-16');
				    }            	                	                
	            }
	        }
<?php } ?>
<?php if ($m_params->open_type == 'EXE_CALL') { ?>
			{
	            text: 'Esegui',
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',	            
	            
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_call',
					        jsonData: {form_values: form.getValues()},
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){
					            console.log('successs');			            
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });				    			
					            	                	                
	            }
	        }

<?php } ?> 
	        
	        ],             
				
        }
]}
<?php exit; } ?>
<?php
// ******************************************************************************************
// MAIN
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open'){	
	$m_params = acs_m_params_json_decode();

	//memorizzo la data consegne evidenziate
	$data_consegna_evidenziate = $s->find_TA_std('OEDTE');
	if (count($data_consegna_evidenziate) == 0)
		$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(TADT, TATAID, TAKEY1) VALUES({$id_ditta_default}, 'OEDTE', '{$m_params->form_values->f_data_consegne_evidenziate}') ";
	else
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']} SET TAKEY1='{$m_params->form_values->f_data_consegne_evidenziate}' WHERE TADT='{$id_ditta_default}' AND TATAID='OEDTE'";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	
	//recupero elenco utenti (essere solo quelli che hanno un default gi� caricato)
	$sql = "SELECT DISTINCT UTCUTE, UTDESC FROM {$cfg_mod_Admin['file_utenti']} UT
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_parametri_operatori_OE']}
				ON UTCUTE = OEUSER
				WHERE OEDT = ? AND OEFLPS = ? AND OETPCA = ? AND OEDTRG = 0 /* prendo solo quelli con default */
				GROUP BY UTCUTE, UTDESC
				ORDER BY UTDESC ";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->form_values->OEDT, $m_params->form_values->OEFLPS, $m_params->form_values->OETPCA));
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_users[trim($r['UTCUTE'])] = $r;
	}
	
	
	
	//tipologie
	$ar_tipologie = $s->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'Y');
	
	//recupero ordini gia' assegnati agli utenti
	$sql = "SELECT ASUSAT, TDCLOR, COUNT(*) AS C_ROW 
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} ON TDDOCU=ASDOCU 
				WHERE ASCAAS='IMMOR' AND TD.TDFN13=1 AND ASDTSC=?
				  " . sql_where_by_combo_value('TDASPE', $m_params->form_values->f_area) . "
				  " . sql_where_by_combo_value('TDCDIV', $m_params->form_values->f_divisione) . " 		
				GROUP BY ASUSAT, TDCLOR";	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->form_values->OEDTRG));
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_ordini_assegnati[trim($r['ASUSAT'])][trim($r['TDCLOR'])] = $r['C_ROW'];
	}
	
	
	//recupero ordini gia' assegnati agli utenti (totali, non per data)
	$sql = "SELECT ASUSAT, TDCLOR, COUNT(*) AS C_ROW
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} ON TDDOCU=ASDOCU
				WHERE ASCAAS='IMMOR' AND TD.TDFN13=1
				" . sql_where_by_combo_value('TDASPE', $m_params->form_values->f_area) . "
				" . sql_where_by_combo_value('TDCDIV', $m_params->form_values->f_divisione) . "
				GROUP BY ASUSAT, TDCLOR";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array());
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_ordini_assegnati_tot[trim($r['ASUSAT'])][trim($r['TDCLOR'])] = $r['C_ROW'];
	}
	

	//recupero ordini gia' assegnati agli utenti (scaduti)
	$sql = "SELECT ASUSAT, TDCLOR, COUNT(*) AS C_ROW
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} ON TDDOCU=ASDOCU
				WHERE ASCAAS='IMMOR' AND TD.TDFN13=1 AND ASDTSC < ? AND ASFLRI <> 'Y' /* SOLO NON RILASCIATI???? */
				" . sql_where_by_combo_value('TDASPE', $m_params->form_values->f_area) . "
				" . sql_where_by_combo_value('TDCDIV', $m_params->form_values->f_divisione) . "
				GROUP BY ASUSAT, TDCLOR";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array(oggi_AS_date()));
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_ordini_assegnati_scad[trim($r['ASUSAT'])][trim($r['TDCLOR'])] = $r['C_ROW'];
	}
	

	//recupero totale ordini da assegnare (REFOR)
	$sql = "SELECT TDCLOR, COUNT(*) AS C_ROW
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} ON TDDOCU=ASDOCU
				WHERE ASCAAS='REFOR' AND ASFLRI<>'Y'
				  " . sql_where_by_combo_value('TDASPE', $m_params->form_values->f_area) . "
				  " . sql_where_by_combo_value('TDCDIV', $m_params->form_values->f_divisione) . "				
				GROUP BY ASUSAT, TDCLOR";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_tot_da_assegnare[trim($r['TDCLOR'])] = $r['C_ROW'];
	}

	
	//recupero totale ordini da assegnare con evidenza(REFOR)
	$data_consegna_evidenziate_ar = $s->find_TA_std('OEDTE'); 
	$f_data_consegna_evidenziata = $data_consegna_evidenziate_ar[0]['id'];
	
	$sql = "SELECT TDCLOR, COUNT(*) AS C_ROW
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} ON TDDOCU=ASDOCU
				WHERE ASCAAS='REFOR' AND ASFLRI <> 'Y' AND TDDTEP <= ?
				  " . sql_where_by_combo_value('TDASPE', $m_params->form_values->f_area) . "
				  " . sql_where_by_combo_value('TDCDIV', $m_params->form_values->f_divisione) . "				
				GROUP BY ASUSAT, TDCLOR";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->form_values->f_data_consegne_evidenziate));
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_tot_da_assegnare_evidenza[trim($r['TDCLOR'])] = $r['C_ROW'];
	}
	

	//recupero valori gia' inseriti in OE0 (per precaricare la form)
	$sql = "SELECT OEUSER, OETOR1, OENRDO
			 	FROM {$cfg_mod_Spedizioni['file_parametri_operatori_OE']} 
				WHERE OEDT=? AND OEDTRG=? AND OEFLPS=? AND OETPCA=?
			";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array(
			$m_params->form_values->OEDT,
			$m_params->form_values->OEDTRG,
			$m_params->form_values->OEFLPS,
			$m_params->form_values->OETPCA,
	));
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_user_val[trim($r['OEUSER'])][trim($r['OETOR1'])] = $r['OENRDO'];
	}
	
	
?>
{"success":true, "items": [
 {
	xtype: 'container',	bodyStyle: 'padding: 10px', bodyPadding: '5 5 0',
	frame: true,
	title: '',
	layout: { 	type: 'vbox',
			    pack: 'start',
			    align: 'stretch'},
	autoScroll:true,

	items: [
		{
		 xtype: 'form',
		 flex: 1,
 		 layout: { 	type: 'vbox',
						    pack: 'start',
						    align: 'stretch'},
		 autoScroll:true,					    
		 items: [

					, {
				        xtype: 'fieldcontainer',
				        fieldLabel: '',
						defaultType: 'textfield',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
						defaults: {style: 'color: red; border-bottom: 1px solid black;', padding: '15 0 5 0'},
	 			        items: [										
                			{flex: 1, xtype: 'label', text: ''}
							<?php	foreach ($ar_tipologie as $kt=>$t){ ?>
								,{width: 110, xtype: 'label', text: <?php echo j(trim($t['text'])); ?>}	
							<?php } ?>                			
						]
					}		 
		 
<?php	foreach ($ar_users as $ku=>$u){ ?>
					, {
				        xtype: 'fieldcontainer',
				        fieldLabel: '',
						defaultType: 'textfield',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
						defaults: {margin: '7 0 0 0'},
	 			        items: [										
                			{flex: 1, xtype: 'label', text: <?php echo j($u['UTCUTE']); ?>, padding: '3px 0px 3px 30px'}
							<?php	foreach ($ar_tipologie as $kt=>$t){

								if (
										n($ar_ordini_assegnati[trim($u['UTCUTE'])][trim($t['id'])], 0) > 0 ||
										n($ar_ordini_assegnati_tot[trim($u['UTCUTE'])][trim($t['id'])], 0) ||
										n($ar_ordini_assegnati_scad[trim($u['UTCUTE'])][trim($t['id'])], 0)
										){
									$txt_count = implode(', ', array(
								  					"<span style='font-size: 11px; font-weight: bold;'>" . n($ar_ordini_assegnati[trim($u['UTCUTE'])][trim($t['id'])], 0) . "</span>",
								  					"<span style='font-size: 10px;'>" . n($ar_ordini_assegnati_tot[trim($u['UTCUTE'])][trim($t['id'])], 0) . "</span>",
								  					"<span style='font-size: 10px; ffont-weight: bold; color: red;'>" . n($ar_ordini_assegnati_scad[trim($u['UTCUTE'])][trim($t['id'])], 0) . "</span>"
								  				));
								} else {
									$txt_count = '';
								}
								
								?>
							 , {
							 	width: 110,
						        xtype: 'fieldcontainer',
						        fieldLabel: '',
								defaultType: 'textfield',
								layout: { 	type: 'hbox',
										    pack: 'start',
										    align: 'stretch'},
								items: [		    							
								    {width: 35, xtype: 'textfield', allowBlank: true, labelWidth: 90, fieldLabel: '',
								    	name: <?php echo j("cellvalue|{$u['UTCUTE']}|" . trim($t['id']) . ""); ?>,
								    	value: <?php echo j($ar_user_val[trim($u['UTCUTE'])][trim($t['id'])])?> 
								    	}
								  , {width: 95, padding: '5px 0px 5px 5px', xtype: 'panel', border: false, 
								  		html: <?php echo j($txt_count); ?>
								  		}
								]
							   }	 									
							<?php } ?>                			
						]
					}					
<?php 	} ?>


			//Totali da assegnare
				, {
				        xtype: 'fieldcontainer',
				        fieldLabel: '',
						defaultType: 'textfield',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
						defaults: {style: 'border-top: 1px solid black;', margin: '7 0 0 0'},
	 			        items: [										
                			{flex: 1, xtype: 'label', text: 'Totali da assegnare', padding: '13px 0px 3px 30px'}
							<?php	foreach ($ar_tipologie as $kt=>$t){ ?>
							 , {
							 	width: 110,
						        xtype: 'fieldcontainer',
						        fieldLabel: '',
								defaultType: 'textfield',
								layout: { 	type: 'hbox',
										    pack: 'start',
										    align: 'stretch'},
								items: [		    							
								  , {width: 35, padding: '15px 0px 5px 5px', xtype: 'label', text: <?php echo j(" (" . n($ar_tot_da_assegnare[trim($t['id'])], 0) . ")"); ?>}
								]
							   }	 									
							<?php } ?>                			
						]
					}

				, {
				        xtype: 'fieldcontainer',
				        fieldLabel: '',
						defaultType: 'textfield',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
	 			        items: [										
                			{flex: 1, xtype: 'label', text: 'Totali entro il <?php echo print_date($f_data_consegna_evidenziata); ?>', padding: '3px 0px 3px 30px'}
							<?php	foreach ($ar_tipologie as $kt=>$t){ ?>
							 , {
							 	width: 110,
						        xtype: 'fieldcontainer',
						        fieldLabel: '',
								defaultType: 'textfield',
								layout: { 	type: 'hbox',
										    pack: 'start',
										    align: 'stretch'},
								items: [		    							
								  , {width: 35, padding: '5px 0px 5px 5px', xtype: 'label', text: <?php echo j(" (" . n($ar_tot_da_assegnare_evidenza[trim($t['id'])], 0) . ")"); ?>}
								]
							   }	 									
							<?php } ?>                			
						]
					}
					
					
		 ]
		 
		, buttons: [{
		            text: 'Salva',
					scale: 'large',	iconCls: 'icon-save-32',
	            				            
		            handler: function() {
		            	var form = this.up('form').getForm();
		            	var loc_win = this.up('window');
						
						if(form.isValid()){
						
									Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_calendar',
									        jsonData: {
												form_values: form.getValues(),
						        				open_request: <?php echo acs_je($m_params) ?> 
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									        	<?php if (strlen($m_params->main_panel_id) > 0) { ?>
									        		tmp_win = Ext.getCmp(<?php echo j($m_params->main_panel_id)?>);									        	
									            	tmp_win.down('grid').store.load();
									            <?php } ?>	
									            loc_win.close();			            
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });				    			
						
				        }

					}
				}
				
				
				
				, {
		            text: 'Salve & Esegui',
					scale: 'large',	iconCls: 'icon-button_red_play-32',
	            				            
		            handler: function() {
		            	var form = this.up('form').getForm();
		            	var loc_win = this.up('window');
						
						if(form.isValid()){
									Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_calendar',
									        jsonData: {
												form_values: form.getValues(),
						        				open_request: <?php echo acs_je($m_params) ?> 
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									        
									        
									        	//Lancio esecuzione (con gli stessi parametri di apertura)
													Ext.Ajax.request({
													        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_call',
													        jsonData: {
										        				form_values: <?php echo acs_je($m_params->form_values) ?> 
													        },
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
									            				loc_win.close();
									            				<?php if (strlen($m_params->form_values->from_panel_id) > 0) { ?>
									            					from_panel = Ext.getCmp(<?php echo acs_je($m_params->form_values->from_panel_id) ?>);
									            					from_panel.store.treeStore.load();
									            				<?php } ?>
									            																	            			            
													        },
													        failure    : function(result, request){
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });	

									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });						
						}
					}
				}				
				
				
				
				
			]		 
				 
		}
	]
	
	
				
 }
]}
<?php exit; } ?>