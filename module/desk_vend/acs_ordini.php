<?php

require_once("../../config.inc.php");


$s = new Spedizioni();
$main_module = new Spedizioni();


function parametri_sql_where($form_values){

	global $s;

	$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);

	$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);

	$sql_where.= sql_where_by_combo_value('TD.TDCITI', $form_values->f_itinerario);

	$sql_where.= sql_where_by_combo_value('SP.CSTISP', $form_values->f_tipologia);

	$sql_where.= sql_where_by_combo_value('SP.CSTITR', $form_values->f_trasporto);

	$sql_where.= sql_where_by_combo_value('TD.TDCAG1', $form_values->f_agente);

	$sql_where.= sql_where_by_combo_value('TD.TDOPRI', $form_values->priorita);

	$sql_where.= " AND ".$s->get_where_std();

	//controllo HOLD
	$filtro_hold=$form_values->f_hold;

	if($filtro_hold== "Y")
		$sql_where.=" AND TDSWSP = 'Y'";
		if($filtro_hold== "N")
			$sql_where.=" AND TDSWSP = 'N'";

			return $sql_where;
}

function div_check0($divisore, $dividendo, $default = 0){
	
	global $s;
	
	if ($dividendo>0){
		$result = $divisore/$dividendo;
	}else{
		$result=$default;
	}
	return $result;
}

//prima tabella
if ($_REQUEST['fn'] == 'get_json_data_1'){
	
	
	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;
	
	
	$sql_where=parametri_sql_where($form_values);
	
	//totale clienti e totale destinazioni
	$sql = "SELECT COUNT(DISTINCT TDCCON) AS CLIENTI, COUNT (DISTINCT CONCAT(TDCCON, TDCDES)) AS DEST
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
    		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG 
	WHERE " . $s->get_where_std()." {$sql_where}";  //elimino nell'anno il valore 0
		
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$row = db2_fetch_assoc($stmt);
	
	//ordini tipologia assistenza
	$sql2 = "SELECT COUNT(DISTINCT TDCCON) AS CLIENTI, COUNT (DISTINCT CONCAT(TDCCON, TDCDES)) AS DEST
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
    		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG 
	WHERE TDCLOR='A' {$sql_where}"; 
	
	$stmt2 = db2_prepare($conn, $sql2);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt2);
	$row2 = db2_fetch_assoc($stmt2);
	
	//ordini con deroghe
	$sql3 = "SELECT COUNT(DISTINCT TDCCON) AS CLIENTI, COUNT (DISTINCT CONCAT(TDCCON, TDCDES)) AS DEST
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_deroghe']} AD  ON AD.ADRGES = TD.TDDOCU
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
    		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG 
	WHERE ADTP = '' {$sql_where}";
	
	$stmt3 = db2_prepare($conn, $sql3);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt3);
	$row3 = db2_fetch_assoc($stmt3);
	
	//ordini con riprogrammazioni
	$sql4 = "SELECT COUNT(DISTINCT TDCCON) AS CLIENTI, COUNT (DISTINCT CONCAT(TDCCON, TDCDES)) AS DEST
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_deroghe']} AD  ON AD.ADRGES = TD.TDDOCU
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
    		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG 
	WHERE ADTP = 'R' {$sql_where}";
	
	$stmt4 = db2_prepare($conn, $sql4);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt4);
	$row4 = db2_fetch_assoc($stmt4);
	
	//ordini bloccati /sbloccati
	$sql5 = "SELECT COUNT(DISTINCT TDCCON) AS CLIENTI, COUNT (DISTINCT CONCAT(TDCCON, TDCDES)) AS DEST
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_deroghe']} AD  ON AD.ADRGES = TD.TDDOCU
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	WHERE (TDBLOC = ? OR TDBLEV=?) {$sql_where}";
	
	$stmt5 = db2_prepare($conn, $sql5);
	echo db2_stmt_errormsg();

	$result = db2_execute($stmt5, array('Y', 'Y')); 
	$row5= db2_fetch_assoc($stmt5);
	
	$result = db2_execute($stmt5, array('S', 'S'));
	$row6= db2_fetch_assoc($stmt5);
	
	//clienti e destinazioni bloccati
	$sql7 = "SELECT COUNT(DISTINCT TDCCON) AS CLIENTI, COUNT (DISTINCT CONCAT(TDCCON, TDCDES)) AS DEST
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	WHERE 1=1 {$sql_where} AND TDFN02  = 1 ";  //elimino nell'anno il valore 0
	
//	print_r($sql7);
//	exit;
	
	
	$stmt7 = db2_prepare($conn, $sql7);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt7);
	$row7 = db2_fetch_assoc($stmt7);
	

    //compongo le mie uniche due righe
		$nr = array();
		$nr[]=array('denom'=> 'Nr clienti', 'tot'=>$row['CLIENTI'], 'blocc' =>$row7['CLIENTI'],
				'ass'=> $row2['CLIENTI'], 'ass_perc'=>div_check0($row2['CLIENTI'], $row['CLIENTI']), 
				'der'=> $row3['CLIENTI'], 'der_perc'=>div_check0($row3['CLIENTI'],$row['CLIENTI']), 
				'rip'=>$row4['CLIENTI'], 'rip_perc'=>div_check0($row4['CLIENTI'], $row['CLIENTI']),
				'bloc'=>$row5['CLIENTI'], 'bloc_perc'=>div_check0($row5['CLIENTI'], $row['CLIENTI']),
				
				'sbloc'=>$row6['CLIENTI'], 'sbloc_perc'=>div_check0($row6['CLIENTI'], $row['CLIENTI'])
		);
		$nr[]=array('denom'=> 'Nr destinazioni programmate', 'tot'=>$row['DEST'], 'blocc' =>$row7['DEST'],
				'ass'=> $row2['DEST'],'ass_perc'=>div_check0($row2['DEST'], $row['DEST']),
				'der'=> $row3['DEST'], 'der_perc'=>div_check0($row3['DEST'], $row['DEST']), 
				'rip'=>$row4['DEST'], 'rip_perc'=>div_check0($row4['DEST'], $row['DEST']),
				'bloc'=>$row5['DEST'], 'bloc_perc'=>div_check0($row5['DEST'], $row['DEST']),
				'sbloc'=>$row6['DEST'], 'sbloc_perc'=>div_check0($row6['DEST'], $row['DEST'])
		);

	echo acs_je($nr);
	exit;
}


//seconda tabella
if ($_REQUEST['fn'] == 'get_json_data_2'){

	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;

	$sql_where=parametri_sql_where($form_values);

	//totale ordini raggruppati per tipologia
	$sql = "SELECT TDCLOR, TA_TIPOV.TADESC AS DESC_TIPO, COUNT(*) AS ORD_TOT, SUM (TDFN01) AS CONT_CAR, SUM (TDFN11) AS CONT_EV
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TIPOV ON TD.TDDT=TA_TIPOV.TADT AND TD.TDCLOR=TA_TIPOV.TAKEY1 AND TA_TIPOV.TATAID='TIPOV'
	WHERE 1=1 {$sql_where}
	GROUP BY TDCLOR, TA_TIPOV.TADESC";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	//ordini con deroghe/riprogrammazioni in base alla tipologia
	$sql2 = "SELECT COUNT(DISTINCT TDDOCU) AS ORD
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_deroghe']} AD  ON AD.ADRGES = TD.TDDOCU
	WHERE ADTP = ? AND TDCLOR=? {$sql_where} ";  //? lo gestisco nell'execute con un array dove passo i valori

	$stmt2 = db2_prepare($conn, $sql2); //faccio solo la prepare
	echo db2_stmt_errormsg();

	//ordini puntuali per giorno in base alla tipologia, data evasione programmata = data evasione confermata (solo ordini evasi)
	$sql3 = "SELECT COUNT(*) AS ORD
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	WHERE TDCLOR=? AND TDFN01=1 AND TDDTEP>TDDTEPI {$sql_where}";

	$stmt3 = db2_prepare($conn, $sql3); //faccio solo  la prepare
	echo db2_stmt_errormsg();


	//ordini puntuali per settimana in base alla tipologia, recupero la settimana e l'anno dal cs0 (solo ordini evasi)
	$sql4 = "SELECT COUNT(*) AS ORD
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS_CAL1 ON TD.TDDT=CS_CAL1.CSDT AND CS_CAL1.CSCALE='*CS' AND CS_CAL1.CSDTRG= TD.TDDTEP
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS_CAL2 ON TD.TDDT=CS_CAL2.CSDT AND CS_CAL2.CSCALE='*CS' AND CS_CAL2.CSDTRG= TD.TDDTEPI
	WHERE TDCLOR=? AND TDFN01=1 AND
	(
 TDDTEPI = 0 OR
 (
 TDDTEPI>0 AND (DATE(TIMESTAMP_FORMAT (CHAR(TDDTEP), 'YYYYMMDD')) > DATE(TIMESTAMP_FORMAT (CHAR(
CASE  WHEN TDDTEPI > 0 THEN TDDTEPI ELSE '19700101' END
), 

'YYYYMMDD')) -7 DAYS 
  AND DATE(TIMESTAMP_FORMAT (CHAR(TDDTEP), 'YYYYMMDD')) < DATE(TIMESTAMP_FORMAT (CHAR(

CASE  WHEN TDDTEPI > 0 THEN TDDTEPI ELSE '19700101' END

), 'YYYYMMDD')) +7 DAYS )
 )
){$sql_where}";

	$stmt4 = db2_prepare($conn, $sql4); //faccio solo la prepare
	echo db2_stmt_errormsg();


	//ordini completi in base alla tipologia, colli = colli spediti (solo ordini evasi)
	$sql5 = "SELECT COUNT(*) AS ORD
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	WHERE TDCLOR=? AND TDFN11=1 AND (TDTOCO=TDCOSP OR TDCOSP=0) {$sql_where}";

	$stmt5 = db2_prepare($conn, $sql5);
	echo db2_stmt_errormsg(); //faccio solo la prepare

	//ORDINI BLOCCATI/sbloccati
	$sql6 = "SELECT COUNT(*) AS ORD
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	WHERE (TDBLOC = ? OR TDBLEV=?) AND TDCLOR=? {$sql_where} ";

	$stmt6 = db2_prepare($conn, $sql6);
	echo db2_stmt_errormsg(); //faccio solo la prepare

	while($row = db2_fetch_assoc($stmt)){

		$nr = array();
		$nr['TDCLOR'] 	 = "[".acs_u8e(trim($row['TDCLOR']))."] ".trim($row['DESC_TIPO']);
		$nr['ORD_TOT'] 	 = trim($row['ORD_TOT']);
		$nr['CONT_CAR'] 	 = trim($row['CONT_CAR']);
		$nr['CONT_EV'] 	 = trim($row['CONT_EV']);

		$result = db2_execute($stmt2, array('', $row['TDCLOR'])); //deroghe
		$row2 = db2_fetch_assoc($stmt2);
		$nr['ORD_DER'] 	 = trim($row2['ORD']);
		$nr['ORD_DER_PERC']=div_check0($row2['ORD'],$row['CONT_CAR']);

		$result = db2_execute($stmt2, array('R', $row['TDCLOR'])); //riprogrammazioni
		$row2 = db2_fetch_assoc($stmt2);
		$nr['ORD_RIP'] 	 = trim($row2['ORD']);
	    $nr['ORD_RIP_PERC'] 	 = div_check0($row2['ORD'], $row['CONT_CAR']);


		$result = db2_execute($stmt3, array($row['TDCLOR'])); //puntuali giorno
		$row3 = db2_fetch_assoc($stmt3);
		$nr['PUNT_G'] 	 = trim($row3['ORD']);
		$nr['PUNT_G_PERC'] 	 = div_check0($row3['ORD'],$row['CONT_CAR']);
		

		$result = db2_execute($stmt4, array($row['TDCLOR'])); //puntuali settimana
		$row4 = db2_fetch_assoc($stmt4);
		$nr['PUNT_S'] 	 = trim($row4['ORD']);
		$nr['PUNT_S_PERC'] 	 = div_check0($row4['ORD'],$row['CONT_CAR']);


		$result = db2_execute($stmt5, array($row['TDCLOR'])); //completi
		$row5 = db2_fetch_assoc($stmt5);
		$nr['COM'] 	 = trim($row5['ORD']);
		$nr['COM_PERC'] 	 = div_check0($row5['ORD'], $row['CONT_EV']);
	
		setlocale(LC_TIME, 'it_IT');
		$g_m= acs_u8e(strftime("%B", strtotime($r['children'][$i]['data']))); //MESE

		$result = db2_execute($stmt6, array('Y', 'Y', $row['TDCLOR'])); //BLOCCATI
		$row6 = db2_fetch_assoc($stmt6);
		$nr['BLOC'] 	 = trim($row6['ORD']);
	    $nr['BLOC_PERC'] 	 = div_check0($row6['ORD'], $row['ORD_TOT']);
	

		$result = db2_execute($stmt6, array('S', 'S', $row['TDCLOR'])); //SBLOCCATI
		$row7 = db2_fetch_assoc($stmt6);
		$nr['SBLOC'] 	 = trim($row7['ORD']);
		$nr['SBLOC_PERC'] 	 = div_check0($row7['ORD'], $row['ORD_TOT']);
	

		$data[] = $nr;

	}



	echo acs_je($data);
	exit;
}

//terza tabella
if ($_REQUEST['fn'] == 'get_json_data_3'){

	$data=array();
	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;


	$sql_where=parametri_sql_where($form_values);
	
	//volume disponibile totale
	$sql_1="SELECT SUM(TDVOLU) AS VOL_TOT 
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
    		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG 
	WHERE TDFN01=1 {$sql_where}";
	
	$stmt_1 = db2_prepare($conn, $sql_1);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_1);
	$row1 = db2_fetch_assoc($stmt_1);
	
	//volume per trasportatore (colonna 1)
	$sql_2="SELECT TA_TRAS.TAKEY1 AS COD_TRAS, SUM(SP.CSVOLD) AS VOL_PER_TRAS, CSTITR AS TIP
	FROM {$cfg_mod_Spedizioni['file_calendario']} SP
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
   			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
   			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
   	WHERE CSPROG IN (
   		SELECT DISTINCT TDNBOC FROM {$cfg_mod_Spedizioni['file_testate']} TD
   		WHERE TDFN01=1 {$sql_where}  		
   	)		
	GROUP BY TA_TRAS.TAKEY1, CSTITR";

	$stmt_2 = db2_prepare($conn, $sql_2);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_2);
	$tr = array();
	while($row2 = db2_fetch_assoc($stmt_2)){
		// nell'array ad ogni codice trasportatore associo il suo volume
		$tr[trim($row2['COD_TRAS']) . "_" . trim($row2['TIP'])] 	 =$row2['VOL_PER_TRAS'];
		
	}

	
	//query principale
	$sql = "SELECT TA_TRAS.TADESC AS TRASP, TA_TRAS.TAKEY1 AS COD_TRAS, SUM(TDVOLU) AS ASS, SUM(TDTOCO) AS COLLI,
	COUNT (DISTINCT CONCAT(TDNBOC, CONCAT(TDCCON, TDCDES))) AS NR_SCAR, 
	COUNT (DISTINCT CONCAT(TDNBOC, CONCAT(TDIDES, CONCAT(TDDCAP, CONCAT(TDDLOC, CONCAT(TDPROD,TDNAZD)))))) AS SCAR,
	TA_TTRA.TADESC AS TIP_TRASP, COUNT(DISTINCT TDNBOC) AS SPED, SP.CSTITR AS TIP
    FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
    		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG 
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
   			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
   			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
   	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TTRA 
   			ON TA_TTRA.TADT = SP.CSDT AND TA_TTRA.TAKEY1 = SP.CSTITR AND TA_TTRA.TATAID = 'TTRA' 
    WHERE TDFN01=1 {$sql_where} 
    GROUP BY TA_TRAS.TADESC, TA_TRAS.TAKEY1, TA_TTRA.TADESC, SP.CSTITR";  
	
	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['COD_TIP'] 	 ="[".trim($row['TIP'])."] " . acs_u8e(trim($row['TIP_TRASP']));
		$nr['TRASP'] 	 = acs_u8e(trim($row['TRASP']));
		$nr['VOL']		 = $tr[trim($row['COD_TRAS']) . "_" . trim($row['TIP'])];
		$nr['ASS']	     = $row['ASS'];
		$nr['ASS_PERC']  = div_check0($row['ASS'], $row1['VOL_TOT']);
        $nr['COLLI']	 = trim($row['COLLI']);
		$nr['SPED']	     = trim($row['SPED']);
		$nr['NR_SCAR']	     = trim($row['NR_SCAR']);
		$nr['SCAR']	     = trim($row['SCAR']);
	    $nr['INC']	 = div_check0($row['ASS'], $nr['VOL']);
	    $nr['COLLI_SCAR']= div_check0($row['COLLI'], $row['SCAR']);
		$nr['COLLI_SPED']= div_check0($row['COLLI'], $row['SPED']);
		$nr['SCAR_SPED'] = div_check0($row['SCAR'], $row['SPED']);
	
		$data[] = $nr;
		
	}


	echo acs_je($data);
	exit;
}


//terza tabella_totale generale
if ($_REQUEST['fn'] == 'get_json_data_tot'){

	$data=array();
	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;


	$sql_where=parametri_sql_where($form_values);

	//volume disponibile totale
	$sql_1="SELECT SUM(TDVOLU) AS VOL_TOT
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	WHERE TDFN01=1 {$sql_where}";

	$stmt_1 = db2_prepare($conn, $sql_1);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_1);
	$row1 = db2_fetch_assoc($stmt_1);

	//volume per trasportatore (colonna 1)
	$sql_2="SELECT SUM(SP.CSVOLD) AS VOL_PER_TRAS
	FROM {$cfg_mod_Spedizioni['file_calendario']} SP
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
	ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
	ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
	WHERE CSPROG IN (
	SELECT DISTINCT TDNBOC FROM {$cfg_mod_Spedizioni['file_testate']} TD
	WHERE TDFN01=1 {$sql_where}
	)";
	
	$stmt_2 = db2_prepare($conn, $sql_2);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_2);
	$tr = array();
	$row2 = db2_fetch_assoc($stmt_2);


	//query principale
	$sql = "SELECT SUM(TDVOLU) AS ASS, SUM(TDTOCO) AS COLLI,
	COUNT (DISTINCT CONCAT(TDNBOC, CONCAT(TDCCON, TDCDES))) AS NR_SCAR,
	COUNT (DISTINCT CONCAT(TDNBOC, CONCAT(TDIDES, CONCAT(TDDCAP, CONCAT(TDDLOC, CONCAT(TDPROD,TDNAZD)))))) AS SCAR,
    COUNT(DISTINCT TDNBOC) AS SPED
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
	ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
	ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TTRA
	ON TA_TTRA.TADT = SP.CSDT AND TA_TTRA.TAKEY1 = SP.CSTITR AND TA_TTRA.TATAID = 'TTRA'
	WHERE TDFN01=1 {$sql_where}";



	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$row = db2_fetch_assoc($stmt);


		$nr = array();
		
		$nr['VOL']		 = $row2['VOL_PER_TRAS'];
		$nr['ASS']	     = $row['ASS'];
		$nr['ASS_PERC']  = div_check0($row['ASS'], $row1['VOL_TOT']);
		$nr['COLLI']	 = trim($row['COLLI']);
		$nr['SPED']	     = trim($row['SPED']);
		$nr['NR_SCAR']	     = trim($row['NR_SCAR']);
		$nr['SCAR']	     = trim($row['SCAR']);
		$nr['INC']	 = div_check0($row['ASS'], $nr['VOL']);
		$nr['COLLI_SCAR']= div_check0($row['COLLI'], $row['SCAR']);
		$nr['COLLI_SPED']= div_check0($row['COLLI'], $row['SPED']);
		$nr['SCAR_SPED'] = div_check0($row['SCAR'], $row['SPED']);
		$nr['liv'] = 'liv_1';

		$data[] = $nr;

	echo acs_je($data);
	exit;
}

//gauge1
if ($_REQUEST['fn'] == 'get_json_data_gauge1'){
	
	
	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;

	$sql_where=parametri_sql_where($form_values);
	
		//ordini puntuali per giorno in base alla tipologia, data evasione programmata = data evasione confermata (solo ordini evasi)
		$sql = "SELECT COUNT(*) AS ORD
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
		WHERE TDFN01=1 AND TDDTEP>TDDTEPI {$sql_where}";
		
		$stmt = db2_prepare($conn, $sql); //faccio solo  la prepare
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt); //puntuali giorno
		$row = db2_fetch_assoc($stmt);
		
		//totale ordini 
		$sql1 = "SELECT SUM (TDFN01) AS CONT_CAR
	       FROM {$cfg_mod_Spedizioni['file_testate']} TD
	       LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	       ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	        WHERE 1=1 {$sql_where}";
		
		$stmt1 = db2_prepare($conn, $sql1);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt1);
		$row1 = db2_fetch_assoc($stmt1);

	    $per_gg['PUNT_GG']= div_check0($row['ORD'], $row1['CONT_CAR'])*100;
		

	echo acs_je(array("root" => $per_gg));
	exit;
}

//gauge2
if ($_REQUEST['fn'] == 'get_json_data_gauge2'){


	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;

	$sql_where=parametri_sql_where($form_values);
	
	//ordini puntuali per settimana
	$sql = "SELECT COUNT(*) AS ORD
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS_CAL1 ON TD.TDDT=CS_CAL1.CSDT AND CS_CAL1.CSCALE='*CS' AND CS_CAL1.CSDTRG= TD.TDDTEP
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS_CAL2 ON TD.TDDT=CS_CAL2.CSDT AND CS_CAL2.CSCALE='*CS' AND CS_CAL2.CSDTRG= TD.TDDTEPI
	WHERE TDFN01=1 AND
	
	(
 TDDTEPI = 0 OR
 (
 TDDTEPI>0 AND (DATE(TIMESTAMP_FORMAT (CHAR(TDDTEP), 'YYYYMMDD')) > DATE(TIMESTAMP_FORMAT (CHAR(
CASE  WHEN TDDTEPI > 0 THEN TDDTEPI ELSE '19700101' END
), 

'YYYYMMDD')) -7 DAYS 
  AND DATE(TIMESTAMP_FORMAT (CHAR(TDDTEP), 'YYYYMMDD')) < DATE(TIMESTAMP_FORMAT (CHAR(

CASE  WHEN TDDTEPI > 0 THEN TDDTEPI ELSE '19700101' END

), 'YYYYMMDD')) +7 DAYS )
 )
)
	{$sql_where}";
	
	$stmt = db2_prepare($conn, $sql); //faccio solo  la prepare
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt); 
	$row = db2_fetch_assoc($stmt);
	
	//totale ordini
	$sql1 = "SELECT SUM (TDFN01) AS CONT_CAR
	       FROM {$cfg_mod_Spedizioni['file_testate']} TD
	       LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	       ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	        WHERE TD.TDDT='$id_ditta_default' {$sql_where}";
	
	$stmt1 = db2_prepare($conn, $sql1);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt1);
	$row1 = db2_fetch_assoc($stmt1);

	$per_set['PUNT_SET']= div_check0($row['ORD'], $row1['CONT_CAR'])*100;

	echo acs_je(array("root" => $per_set));
	exit;
}

//gauge3
if ($_REQUEST['fn'] == 'get_json_data_gauge3'){
	

	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;

	$sql_where=parametri_sql_where($form_values);
	
		//ordini completi, colli = colli spediti (solo ordini evasi) o colli spediti =0
		$sql = "SELECT COUNT(*) AS ORD
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
		WHERE TDFN11=1 AND (TDTOCO=TDCOSP OR TDCOSP=0) {$sql_where}";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg(); //faccio solo la prepare
	    $result = db2_execute($stmt);
	    $row = db2_fetch_assoc($stmt);

	    //totale ordini
	    $sql1 = "SELECT SUM (TDFN11) AS CONT_EV
	       FROM {$cfg_mod_Spedizioni['file_testate']} TD
	       LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
	       ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	        WHERE 1=1 {$sql_where}";
	    
	    $stmt1 = db2_prepare($conn, $sql1);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt1);
	    $row1 = db2_fetch_assoc($stmt1);
	    
	    $compl['COMPL']= div_check0($row['ORD'], $row1['CONT_EV'])*100;
	  

	echo acs_je(array("root" => $compl));
	exit;
}


//radar
if ($_REQUEST['fn'] == 'get_json_data_radar1'){

	$data=array();
	
	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;

	$sql_where=parametri_sql_where($form_values);
	
	
			//volume disponibile per trasportatore (colonna 1)
			$sql="SELECT TA_TRAS.TAKEY1 AS COD_TRAS, SUM(SP.CSVOLD) AS VOL_PER_TRAS, TA_TRAS.TADESC AS TRASP
			FROM {$cfg_mod_Spedizioni['file_calendario']} SP
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
			WHERE CSPROG IN (
			SELECT DISTINCT TDNBOC FROM {$cfg_mod_Spedizioni['file_testate']} TD
			WHERE TDFN01=1 {$sql_where}
			)
			GROUP BY TA_TRAS.TAKEY1,TA_TRAS.TADESC";
			
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);

			while($row = db2_fetch_assoc($stmt)){
				// nell'array ad ogni codice trasportatore associo il suo volume
				$tr[trim($row['TRASP'])] 	 =$row['VOL_PER_TRAS'];

			}

			$sql1 = "SELECT TA_TRAS.TADESC AS TRASP, TA_TRAS.TAKEY1 AS COD_TRAS, SUM(TDVOLU) AS ASS
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
			ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
			WHERE TDFN01=1 {$sql_where}
			GROUP BY TA_TRAS.TADESC, TA_TRAS.TAKEY1";

			$stmt1 = db2_prepare($conn, $sql1);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt1);


			while($row1 = db2_fetch_assoc($stmt1)){
				$tr1[trim($row1['TRASP'])] 	 =$row1['ASS'];

			}


			$sql2 = "SELECT TA_TRAS.TADESC AS TRASP, TA_TRAS.TAKEY1 AS COD_TRAS
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
			ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
			WHERE TDFN01=1 {$sql_where}
			GROUP BY TA_TRAS.TADESC, TA_TRAS.TAKEY1";

			$stmt2 = db2_prepare($conn, $sql2);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt2);

			while($row2 = db2_fetch_assoc($stmt2)){

				$ar_trasp[] = trim($row2['TRASP']);
			}

			foreach($ar_trasp as $k1=>$t1){


				$ar = array();
				
				$ar['TRASPORTATORE'] = $t1;
				$ar["DISPONIBILE"] =round($tr[$t1]);
				$ar["ASSEGNATO"] =round($tr1[$t1]);

				$data[] = $ar;

			}
			
			/*print_r($data);
			exit;*/
			
	
			echo acs_je($data);
			exit;
}

//torta1
if ($_REQUEST['fn'] == 'get_json_data_chart'){
	
	$data=array();

	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;
	
	$sql_where=parametri_sql_where($form_values);

   $sql = "SELECT TA_TTRA.TADESC AS TIP_TRASP, SUM(TDVOLU) AS VOL, SP.CSTITR AS COD_TIP
    FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
    		ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG 
   	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TTRA 
   			ON TA_TTRA.TADT = SP.CSDT AND TA_TTRA.TAKEY1 = SP.CSTITR AND TA_TTRA.TATAID = 'TTRA' 	
    WHERE TDFN01=1 {$sql_where}
    GROUP BY TA_TTRA.TADESC, SP.CSTITR"; 

   
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$nr = array();

		if (trim($row['COD_TIP']) == ''){
			$nr['COD']= "ND";
		}else{
			$nr['COD']	= acs_u8e(trim($row['COD_TIP']));
		}

		if (trim($row['TIP_TRASP']) == ''){
			$nr['DESC']= "Non Definito";
		}else{
			$nr['DESC']	= acs_u8e(trim($row['TIP_TRASP']));
		}

		$nr['NR']	= trim($row['VOL']);


		$data[] = $nr;
	}

	echo acs_je($data);
	exit;

}

//torta2
if ($_REQUEST['fn'] == 'get_json_data_chart1'){

	$data=array();
	
	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;

	$sql_where=parametri_sql_where($form_values);

			$sql = "SELECT SP.CSTISP AS COD_SPE, TA_TSPE.TADESC AS TIP_SPE, SUM(TDVOLU) AS VOL
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
			ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TSPE 
   			ON TA_TSPE.TADT = SP.CSDT AND TA_TSPE.TAKEY1 = SP.CSTISP AND TA_TSPE.TATAID = 'TSPE'
			WHERE TDFN01=1 {$sql_where} 
			GROUP BY SP.CSTISP, TA_TSPE.TADESC";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);

			while ($row = db2_fetch_assoc($stmt)) {
				$nr = array();

				if (trim($row['COD_SPE']) == ''){
					$nr['COD']= "ND";
				}else{
					$nr['COD']	= acs_u8e(trim($row['COD_SPE']));
				}

				if (trim($row['TIP_SPE']) == ''){
					$nr['DESC']= "Non Definito";
				}else{
					$nr['DESC']	= acs_u8e(trim($row['TIP_SPE']));
				}

				$nr['NR']	= trim($row['VOL']);


				$data[] = $nr;
			}

			echo acs_je($data);
			exit;

}

//torta3
if ($_REQUEST['fn'] == 'get_json_data_chart2'){
	
	$data=array();

	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;

	$sql_where=parametri_sql_where($form_values);

			$sql = "SELECT SP.CSCAUT AS COD_MEZZO, TA_AUTO.TADESC AS MEZZO, SUM(TDVOLU) AS VOL
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
			ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_AUTO
			ON TA_AUTO.TADT = SP.CSDT AND TA_AUTO.TAKEY1 = SP.CSCAUT AND TA_AUTO.TATAID = 'AUTO'
			WHERE TDFN01=1 {$sql_where} 
			GROUP BY SP.CSCAUT, TA_AUTO.TADESC";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);

			while ($row = db2_fetch_assoc($stmt)) {
				$nr = array();

				if (trim($row['COD_MEZZO']) == ''){
					$nr['COD']= "ND";
				}else{
					$nr['COD']	= acs_u8e(trim($row['COD_MEZZO']));
				}

				if (trim($row['MEZZO']) == ''){
					$nr['DESC']= "Non Definito";
				}else{
					$nr['DESC']	= acs_u8e(trim($row['MEZZO']));
				}

				$nr['NR']	= trim($row['VOL']);
				

				$data[] = $nr;
			}

			echo acs_je($data);
			exit;

}

if ($_REQUEST['fn'] == 'open_parameters'){
	?>
	
	{"success":true, "items": [
	

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
           
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            multiSelect: true,
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "STATO_ORDINI");  ?> {
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_ordini.php?fn=open_tab', 'panel_stato_ordini', {form_values: form.getValues()});  //recupero i valori del form per passarli nella grid
							this.up('window').close();
			
			            }
			         }
					 
						
						], items: [
		         	  	{
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?>	
								    ] 
							}						 
						}, {
							name: 'f_areaspe',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 
								    ] 
							}						 
						},{
							name: 'f_itinerario',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
								    ] 
							}						 
						},{
							name: 'f_tipologia',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Tipologia sped.',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,							
						    anchor: '-15',
						     margin: "10 10 5 10",	
						    value: <?php echo j(trim($sped['CSTISP'])); ?>,							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('TSPE'), ""); ?>	
								    ] 
							}						 
						  },{
							name: 'f_trasporto',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Tipologia trasp.',
							displayField: 'text',
							valueField: 'id',
						   	allowBlank: true,							
							emptyText: '- seleziona -',
							forceSelection:true,
						    anchor: '-15',
						     margin: "10 10 5 10",	
						    value: <?php echo j(trim($sped['CSTITR'])); ?>,							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'), ""); ?>	
								    ] 
							}						 
						  }, {
							name: 'f_agente',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Agente',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json($s->get_options_agenti(), '') ?>
								    ] 
							}						 
						},{
				name: 'priorita',
				xtype: 'combo',
            	anchor: '100%',		
            	multiSelect: true,		
				fieldLabel: 'Priorit&agrave;',
				displayField: 'text',
				valueField: 'id',
				margin: "10 10 5 10",	
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,
			   	anchor: '-15',														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_sys('BPRI', null, null, 'VO'), '') ?> 	
					    ] 
				}						 
			},
						{
							name: 'f_hold',
							xtype: 'radiogroup',
							fieldLabel: 'Ordini bloc./sbloc.',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "10 10 5 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Includi'
		                          , inputValue: 'A'
		                         , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Escludi'
		                          , inputValue: 'Y'
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'N'
		                          
		                        }]
						}
				 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			  
		}

	
	<?php
	exit;

}




if ($_REQUEST['fn'] == 'open_tab'){
	
	
	$sql = "SELECT TDCLOR, COUNT(*) AS ORD_TIP
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	WHERE " . $s->get_where_std(). " {$sql_where}
	GROUP BY TDCLOR";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	
	$request = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()); //in questa variabile recupero con la REQUEST i valori dell'array del form in questo caso
	?>


{"success":true,

 "items": [{
 xtype: 'panel',
            title: 'Planning Monitor',
            autoScroll: true,
                  tbar: new Ext.Toolbar({
		            items:[
		            '<b>Analisi programmazione evasione ordini/trasporto</b>', '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),   
             <?php echo make_tab_closable(); ?>,
            items: [
            
             {
				         xtype: 'panel',
				         title: 'Indicatori evasione ordini cliente',
				        height: 200,
				        
				         //flex : 1,
				         layout : {
				               type :'hbox',
				               pack: 'start', 
                               align: 'stretch' 
				            },
				          items : [{
            xtype: 'chart',
            style: 'background:#fff',
            animate: {
                easing: 'elasticIn',
                duration: 1000
            },
            store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gauge1', 
								method: 'POST',								
								type: 'ajax',
								
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['PUNT_GG']							
									
			},
            insetPadding: 25,
            margin: '10px 0 0 0',
            flex: 1,
            axes: [{
                type: 'gauge',
                position: 'gauge',
                minimum: 0,
                maximum: 100,
                steps: 10,
                margin: -7,
                title: 'Ritardi su data confermata'
            }],
            series: [{
                type: 'gauge',
                field: 'PUNT_GG',
                donut: false,
                colorSet: ['#E42217', '#ddd'],
                  renderer: function(sprite, record, attributes, index, store) {
	    				var sprite2 = Ext.create('Ext.draw.Sprite', {
						type: 'text',
						surface: sprite.surface,
						text: floatRenderer1(record.get('PUNT_GG')) + '%',
						font: '16px Arial',
						x: 80,
						y: 50,
						width: 100,
						height: 100 
	    				});
	   					sprite2.show(true);
	    				sprite.surface.add(sprite2);    
	    				return attributes;

    },    label: {
			field: 'value',
			display: 'middle'
   			 }
   	
            }]
            
        },{
            xtype: 'chart',
            style: 'background:#fff',
            animate: {
                easing: 'elasticIn',
                duration: 1000
            },
            store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gauge2', 
								method: 'POST',								
								type: 'ajax',
								
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['PUNT_SET']							
									
			},
            insetPadding: 25,
            margin: '10px 0 0 0',
            flex: 1,
            axes: [{
                type: 'gauge',
                position: 'gauge',
                minimum: 0,
                maximum: 100,
                steps: 10,
                margin: 6,
                title: 'Puntuali +/-7 gg su data conf.'
            }],
            series: [{
                type: 'gauge',
                field: 'PUNT_SET',
                donut: 80,
                colorSet: ['#3AA8CB', '#ddd'],
                   renderer: function(sprite, record, attributes, index, store) {
	    				var sprite2 = Ext.create('Ext.draw.Sprite', {
						type: 'text',
						surface: sprite.surface,
						text: floatRenderer1(record.get('PUNT_SET')) + '%',
						font: '16px Arial',
						x: 50,
						y: 50,
						width: 100,
						height: 100 
	    				});
	   					sprite2.show(true);
	    				sprite.surface.add(sprite2);    
	    				return attributes;

    },    label: {
			field: 'value',
			display: 'middle'
   			 }
            }]
        },{
            xtype: 'chart',
            height: 200,
            style: 'background:#fff',
            animate: {
                easing: 'elasticIn',
                duration: 1000
            },
            store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gauge3', 
								method: 'POST',								
								type: 'ajax',
								
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['COMPL']							
									
			},
            insetPadding: 25,
            margin: '10px 0 0 0',
            flex: 1,
            axes: [{
                type: 'gauge',
                position: 'gauge',
                minimum: 0,
                maximum: 100,
                steps: 10,
                margin: 5,
                title: 'Completi'
            }],
            series: [{
                type: 'gauge',
                field: 'COMPL',
                donut: 30,
                colorSet:['#82B525', '#ddd'],
                 renderer: function(sprite, record, attributes, index, store) {
	    				var sprite2 = Ext.create('Ext.draw.Sprite', {
						type: 'text',
						surface: sprite.surface,
						text: floatRenderer1(record.get('COMPL')) + '%',
						font: '16px Arial',
						x: 50,
						y: 50,
						width: 100,
						height: 100 
	    				});
	   					sprite2.show(true);
	    				sprite.surface.add(sprite2);    
	    				return attributes;

    },    label: {
			field: 'value',
			display: 'middle'
   			 }
            }] 
        }]
				        
				        },
                 {
                 
         xtype: 'panel',
         flex : 1,
          layout : {
               type :'vbox',
               pack: 'start', 
               align: 'stretch'
            },
          items : [                    
                    
          
				       {
				        
				        
				         xtype: 'panel',
				         autoScroll: true,
				         flex : 1,			         
				          layout : {
				               type :'vbox',
				               pack: 'start', 
                               align: 'stretch' 
				            },
				          items : [{
            xtype: 'grid',
			title: 'Programmazione per tipologia ordine',
			margin: '10px 0 0 0',
			features: [{
			     		ftype: 'summary',

						}],
			store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_2', 
								method: 'POST',								
								type: 'ajax',
								
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['TDCLOR', 
		        			{name: 'ORD_TOT', type: 'float'},
		        			{name: 'CONT_CAR', type: 'float'},
		        			{name: 'CONT_EV', type: 'float'},
		        			{name: 'ORD_RIP', type: 'float'},
		        			{name: 'ORD_RIP_PERC', type: 'float'},
		        			{name: 'ORD_DER', type: 'float'},
		        			{name: 'ORD_DER_PERC', type: 'float'},
		        			{name: 'PUNT_G', type: 'float'},
		        			{name: 'PUNT_G_PERC', type: 'float'},
		        			{name: 'PUNT_S', type: 'float'},
		        			{name: 'PUNT_S_PERC', type: 'float'},
		        			{name: 'COM', type: 'float'},
		        			{name: 'COM_PERC', type: 'float'},
		        			{name: 'BLOC', type: 'float'},
		        			{name: 'BLOC_PERC', type: 'float'},
		        			{name: 'SBLOC', type: 'float'},
		        			{name: 'SBLOC_PERC', type: 'float'}]							
									
			}, //store
				  	
	        columns: [	
			
			{      header   : 'Tipologia',
			       dataIndex: 'TDCLOR',
			       
	                flex:2
	            },{      header   : 'Totale',
			       dataIndex: 'ORD_TOT',
	                flex:1,
	                 align: 'right', 
	                   summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }
	            },
	            {      header   : 'Con carico',
			       dataIndex: 'CONT_CAR',
	                flex:1.5,
	                 align: 'right', 
	                   summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }
	            },{      header   : 'Di cui <br>&nbsp; &nbsp;Evasi',
			       dataIndex: 'CONT_EV',
	                flex:1,
	                 align: 'right', 
	                   summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }
	            },
	            
	           
  {header: 'Su carico',
                columns: [
 
			{header: 'Con riprogrammazioni <br>&nbsp;',
                    columns: [
                      {header: 'Nr', 
                      dataIndex: 'ORD_RIP',  
                      align: 'right', 
                      width: 70,
                         summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }}
                     , {header: '%', 
                     dataIndex: 'ORD_RIP_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 70,
                     summaryType: function(records, values){

                                sr = 0;
                                st= 0;
                                media=0;
                                Ext.each(records || [], function(rec) {
                                  if (rec.get('confirmed') != false) {
                                    st = st + rec.get('CONT_CAR');
                                    sr = sr + rec.get('ORD_RIP');
                                    media=sr/st;
                                  }
                                }, this);

                                return media;

                            }, 
                     
                     summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }}
                  ]
                 }, 
				 {header: 'Con deroghe <br>&nbsp;', 
	            columns: [ 
                      {header: 'Nr', 
                      dataIndex: 'ORD_DER', 
                      align: 'right',
                      width: 70,
                       summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }
                      }
                     , {header: '%', 
                     dataIndex: 'ORD_DER_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 70,
                     summaryType: function(records, values){

                                sd = 0;
                                st= 0;
                                media=0;
                                Ext.each(records || [], function(rec) {
                                  if (rec.get('confirmed') != false) {
                                    st = st + rec.get('CONT_CAR');
                                    sd = sd + rec.get('ORD_DER');
                                    media=sd/st;
                                  }
                                }, this);

                                return media;

                            }, 
                     
                     summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }}
                  ]
                 },{header: 'Ritardi su data <br> confermata', 
	            columns: [ 
                      {header: 'Nr', 
                      dataIndex: 'PUNT_G', 
                      align: 'right',
                      width: 70, 
                      summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }}
                     , {header: '%', 
                     dataIndex: 'PUNT_G_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 70,
                     summaryType:function(records, values){

                                spg = 0;
                                st= 0;
                                media=0;
                                Ext.each(records || [], function(rec) {
                                  if (rec.get('confirmed') != false) {
                                    st = st + rec.get('CONT_CAR');
                                    spg = spg + rec.get('PUNT_G');
                                    media=spg/st;
                                  }
                                }, this);

                                return media;

                            },
                           
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }}
                  ]
                 },{header: 'Puntuali +/-7 gg <br> su data confermata', 
	            columns: [ 
                      {header: 'Nr',
                      dataIndex: 'PUNT_S', 
                      align: 'right',
                      width: 70,
                       summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }}
                     , {header: '%', 
                     dataIndex: 'PUNT_S_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 70,
                           summaryType: function(records, values){

                                sps = 0;
                                st= 0;
                                media=0;
                                Ext.each(records || [], function(rec) {
                                  if (rec.get('confirmed') != false) {
                                    st = st + rec.get('CONT_CAR');
                                    sps = sps + rec.get('PUNT_S');
                                    media=sps/st;
                                  }
                                }, this);

                                return media;

                            },
                           
                           
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }}
                  ]
                 } 
				 ]
				 }  //fine su carico
                 
                 , {header: 'Evasi',
              			  columns: [
 								{header: 'Completi<br>&nbsp;',
	            					columns: [ 
                      {header: 'Nr',
                       dataIndex: 'COM', 
                       align: 'right',
                       width: 70,
                        summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }}
                     , {header: '%', 
                     dataIndex: 'COM_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 70,
                     summaryType: function(records, values){

                                st = 0;
                                sc= 0;
                                media=0;
                                Ext.each(records || [], function(rec) {
                                  if (rec.get('confirmed') != false) {
                                    st = st + rec.get('CONT_EV');
                                    sc = sc + rec.get('COM');
                                    media=sc/st;
                                  }
                                }, this);

                                return media;

                            },
                           
                           summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }}
                  ]
                 }
			
				 ]
				 },
                  
 
 					 {header: 'Totale',
               				 columns: [
									{header: 'Bloccati<br>&nbsp;',
	            columns: [ 
                      {header: 'Nr',
                       dataIndex: 'BLOC', 
                       align: 'right',
                       width: 70,
                        summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }}
                     , {header: '%', 
                     dataIndex: 'BLOC_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 70,
                           summaryType: function(records, values){

                                st = 0;
                                sb= 0;
                                media=0;
                                Ext.each(records || [], function(rec) {
                                  if (rec.get('confirmed') != false) {
                                    st = st + rec.get('ORD_TOT');
                                    sb = sb + rec.get('BLOC');
                                    media=sb/st;
                                  }
                                }, this);

                                return media;

                            },
                           
                           
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }}
                  ]
                 },
                  {header: 'Sbloccati<br>&nbsp;',
	            columns: [ 
                      {header: 'Nr',
                       dataIndex: 'SBLOC', 
                       align: 'right',
                       width: 70,
                        summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }}
                     , {header: '%', 
                     dataIndex: 'SBLOC_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 70,
                           summaryType: function(records, values){

                                st = 0;
                                ss= 0;
                                media=0;
                                Ext.each(records || [], function(rec) {
                                  if (rec.get('confirmed') != false) {
                                    st = st + rec.get('ORD_TOT');
                                    ss = ss + rec.get('SBLOC');
                                    media=ss/st;
                                  }
                                }, this);

                                return media;

                            },
                           
                            summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }}
                  ]
                 }
			
				 ]
				 }
	            
	          
	         ]																				  			
	            
        }, //fine grid 1
        {   //grid2
            xtype: 'grid',
            margin: '10px 0 0 0',
			title: 'Clienti/Destinazioni primo scarico, con ordini in programmazione',
		    store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_1', 
								method: 'POST',								
								type: 'ajax',
								
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['denom', 'tot', 'blocc', 'ass', 'ass_perc', 'der', 'der_perc', 'rip', 'rip_perc', 
		        			'bloc', 'bloc_perc', 'sbloc', 'sbloc_perc']							
									
			}, //store
				  	
	        columns: [	
			
			{      header   : 'Descrizione',
			       dataIndex: 'denom',
	                flex:2
	            },
	            {
	                header   : 'Totale',
	                dataIndex: 'tot',
	                align: 'right',
	                flex:1
	            },
	            {
	                header   : 'Di cui bloccati',
	                dataIndex: 'blocc',
	                align: 'right',
	                flex:1
	            },
	            {header: 'Con assistenze',
                    columns: [
                      {header: 'Nr', dataIndex: 'ass', align: 'right',  width: 80}
                     , {header: '%', dataIndex: 'ass_perc', align: 'right', renderer: perc2,  width: 80}
                  ]
                 } ,
	            {header: 'Con deroghe',
	            columns: [ 
                      {header: 'Nr', dataIndex: 'der', align: 'right',width: 80}
                     , {header: '%', dataIndex: 'der_perc', align: 'right', renderer: perc2, width: 80}
                  ]
                 },{header: 'Con riprogrammazioni',
                    columns: [
                      {header: 'Nr', dataIndex: 'rip',  align: 'right', width: 80}
                     , {header: '%', dataIndex: 'rip_perc', align: 'right', renderer: perc2, width: 80}
                  ]
                 },{header:'Con ordini bloccati',
                    columns: [
                      {header: 'Nr', dataIndex: 'bloc',  align: 'right', width: 80}
                     , {header: '%', dataIndex: 'bloc_perc', align: 'right', renderer: perc2, width: 80}
                  ]
                 },{header: 'Con ordini sbloccati',
                    columns: [
                      {header: 'Nr', dataIndex: 'sbloc',  align: 'right', width: 80}
                     , {header: '%', dataIndex: 'sbloc_perc', align: 'right', renderer: perc2, width: 80}
                  ]
                 }
	         ]																				  			
	            
        }//fine grid 2 

				         
		]
				        
				        },

        {
				         xtype: 'panel',
				         title: 'Volumi per trasportatore',
				         margin: '10px 0 0 0',
				       // height: 400,
				          autoScroll: true,
				         layout : {
				               type :'vbox',
				               pack: 'start', 
                               align: 'stretch'  
				            },
				          items : [		{
			xtype: 'chart',
			//width: 600,
            height: 300,
            style: 'background:#fff',
            animate: true,
            shadow: true,
            store: {
					xtype: 'store',
					autoLoad:true,
					editable: false,
                    autoDestroy: true, 
			 
			 
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_radar1', 
								method: 'POST',								
								type: 'ajax',
							
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: ['TRASPORTATORE', 'DISPONIBILE', 'ASSEGNATO'],
		        								
									
			},
            legend: {
              position: 'right'  
            },
            axes: [{
                type: 'Numeric',
                position: 'bottom',
                fields: ['DISPONIBILE', 'ASSEGNATO'],
                minimum: 0,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0')
                },
                grid: true,
                title: 'Volume'
            }, {
                type: 'Category',
                position: 'left',
                fields: ['TRASPORTATORE'],
                title: 'Trasportatori'
            }],
            series: [{
                type: 'bar',
                axis: 'bottom',
                xField: 'TRASPORTATORE',
                yField: ['DISPONIBILE', 'ASSEGNATO']
            }]
        }


 , //bar
        
        {
            xtype: 'grid',
			title: 'Analisi spedizioni con carico assegnato',
			margin: '10px 0 0 0',
			//columnLines : true,
			
	 features: [{
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: 'Tipologia trasporto: {name}',
            hideGroupedHeader: true,
            enableGroupingMenu: false
        }],
			store: {
					xtype: 'store',
					
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_3', 
								method: 'POST',								
								type: 'ajax',
								
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							groupField: 'COD_TIP', 
		        			fields: [{name:'COD_TIP', type:'string'}, 
		        			{name: 'TRASP', type: 'string'}, 
		        			{name: 'VOL', type: 'float'},
		        			{name: 'ASS', type: 'float'},
		        			{name: 'ASS_PERC', type: 'float'}, 
		        			{name: 'INC', type: 'float'}, 
		        			{name: 'SPED', type: 'float'},
		        			{name: 'COLLI', type: 'float'}, 
		        			{name: 'COLLI_SPED', type: 'float'},
		        			{name: 'SCAR_SPED', type: 'float'}, 
		        			{name: 'NR_SCAR', type: 'float'},
		        			{name: 'COLLI_SCAR', type: 'float'}]					
									
			}, //store
				  	
	        columns: [	
	        
			{      header   : 'Tipologia Trasporto',
			       dataIndex: 'COD_TIP',
	                flex:1
	            },
			
			{      header   : 'Trasportatori',
			       dataIndex: 'TRASP',
	                flex:1
	            },
	            {
	                header   : 'Volume disponibile M3',
	                dataIndex: 'VOL',
	                renderer: floatRenderer2,
	                align: 'right',
	                 flex:1,
	                summaryType: 'sum', 
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer2(parseFloat(value)); 
                                }
                   
	               
	            }, {header: ' Volume assegnato',
                    columns: [
                      {header: 'M3', 
                      dataIndex: 'ASS',  
                      align: 'right',
                      renderer: floatRenderer2, 
                      width: 100,
                       summaryType: 'sum', 
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer2(parseFloat(value)); 
                                }
                     
                     }, {header: '%', 
                     dataIndex: 'ASS_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 100,
                      summaryType: 'sum', 
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }
                       },{header: 'Impegno %', 
                     dataIndex: 'INC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 100,
                      summaryType:function(records, values){

                                si = 0;
                                st= 0;
                                media=0;
                                Ext.each(records || [], function(rec) {
                                  if (rec.get('confirmed') != false) {
                                    st = st + rec.get('VOL');
                                    si = si + rec.get('ASS');
                                    media=si/st;
                                  }
                                }, this);

                                return media;

                            },
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return perc2(parseFloat(value)); 
                                }
                    
                       }
                  ]
                 },{header: 'Spedizioni',
                    columns: [
                      {header: 'Nr', 
                      dataIndex: 'SPED',  
                      align: 'right',
                      renderer: floatRenderer0N, 
                      width: 100,
                       summaryType: 'sum', 
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }
                     
                     }, {header: 'Colli totali', 
                     dataIndex: 'COLLI', 
                     align: 'right', 
                     renderer: floatRenderer0N, 
                     width: 100,
                      summaryType: 'sum', 
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }
                       },{header: 'Colli per sped.', 
                     dataIndex: 'COLLI_SPED', 
                     align: 'right', 
                     renderer: floatRenderer2, 
                     width: 100,
                      summaryType: 'average', 
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer2(parseFloat(value)); 
                                }
                    
                       },{header: 'Nr scarichi', 
                     dataIndex: 'SCAR_SPED', 
                     align: 'right', 
                     renderer: floatRenderer0N, 
                     width: 100,
                     summaryType: 'average',
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }
                    
                       }
                  ]
                 },{header: 'Scarichi',
                    columns: [
                      {header: 'Nr', 
                      dataIndex: 'NR_SCAR',  
                      align: 'right',
                      renderer: floatRenderer0N, 
                      width: 100,
                       summaryType: 'sum', 
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer0N(parseFloat(value)); 
                                }
                     
                     }, {header: 'Colli per scarico', 
                     dataIndex: 'COLLI_SCAR', 
                     align: 'right', 
                     renderer: floatRenderer2, 
                     width: 100,
                      summaryType: 'average', 
	       
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                        return floatRenderer2(parseFloat(value)); 
                                }
                       }
                  ]
                 }
	          
	         ]																				  			
	            
        }, //fine grid3  
         {
            xtype: 'grid',
			title: 'Totale generale',
			margin: '0 0 0 0',
			//columnLines : true,

			store: {
					xtype: 'store',
					
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tot', 
								method: 'POST',								
								type: 'ajax',
								
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: [
		        			{name: 'VOL', type: 'float'},
		        			{name: 'ASS', type: 'float'},
		        			{name: 'ASS_PERC', type: 'float'}, 
		        			{name: 'INC', type: 'float'}, 
		        			{name: 'SPED', type: 'float'},
		        			{name: 'COLLI', type: 'float'}, 
		        			{name: 'COLLI_SPED', type: 'float'},
		        			{name: 'SCAR_SPED', type: 'float'}, 
		        			{name: 'NR_SCAR', type: 'float'},
		        			{name: 'COLLI_SCAR', type: 'float'}, 'liv']					
									
			}, //store
				  	
	        columns: [	
	      
			
	            {
	                header   : 'Volume disponibile M3',
	                dataIndex: 'VOL',
	                renderer: floatRenderer2,
	                align: 'right',
	                 flex:1
	    
	            }, {header: ' Volume assegnato',
                    columns: [
                      {header: 'M3', 
                      dataIndex: 'ASS',  
                      align: 'right',
                      renderer: floatRenderer2, 
                      width: 100
                     
                     }, {header: '%', 
                     dataIndex: 'ASS_PERC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 100
                       },{header: 'Impegno %', 
                     dataIndex: 'INC', 
                     align: 'right', 
                     renderer: perc2, 
                     width: 100
                    
                       }
                  ]
                 },{header: 'Spedizioni',
                    columns: [
                      {header: 'Nr', 
                      dataIndex: 'SPED',  
                      align: 'right',
                      renderer: floatRenderer0N, 
                      width: 100
                     
                     }, {header: 'Colli totali', 
                     dataIndex: 'COLLI', 
                     align: 'right', 
                     renderer: floatRenderer0N, 
                     width: 100
                       },{header: 'Colli per sped.', 
                     dataIndex: 'COLLI_SPED', 
                     align: 'right', 
                     renderer: floatRenderer2, 
                     width: 100
                    
                       },{header: 'Nr scarichi', 
                     dataIndex: 'SCAR_SPED', 
                     align: 'right', 
                     renderer: floatRenderer0N, 
                     width: 100
                    
                       }
                  ]
                 },{header: 'Scarichi',
                    columns: [
                      {header: 'Nr', 
                      dataIndex: 'NR_SCAR',  
                      align: 'right',
                      renderer: floatRenderer0N, 
                      width: 100
                     
                     }, {header: 'Colli per scarico', 
                     dataIndex: 'COLLI_SCAR', 
                     align: 'right', 
                     renderer: floatRenderer2, 
                     width: 100
                       }
                  ]
                 }
	          
	         ]	,viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           
			    
			           return ret;																
			         }   
			    }																			  			
	            
        } 
      
        ]
				        
				        },		{
				         xtype: 'panel',
				         title:'Grafici sul volume assegnato',
				         margin: '10px 0 0 0',
				          height: 300,
				          autoScroll: true,
				         layout : {
				               type :'hbox',
				               pack: 'start', 
                               align: 'stretch'  
				            },
				          items : [
        {
					xtype: 'chart',
					id: 'chartCmp2',
					animate: true,
					shadow: true, 
					height: 240, 
					flex: 1,				
					
					store: {
						xtype: 'store',
						
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,
							
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['COD','DESC', 'NR']
									
					}, //store					
					
				 
			            insetPadding: 40,
                        margin: '10px 0 0 0',
			            theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NR',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NR'));
			                    });
			                    this.setTitle(storeItem.get('COD') + ' - ' + storeItem.get('DESC') + ': ' + Math.round(storeItem.get('NR') / total * 100) + '% [M3 ' + floatRenderer2(storeItem.get('NR')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'COD',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmp2'); // id of the chart
									var index = cmp.store.findExact('COD', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.COD; // the field containing the label to display on the chart
								}			                    
			                }
			            }],items: [{
     						 type  : 'text',
     						 text  : 'Tipologia trasporto',
      						 font  : '14px Arial',
                             width : 200,
                             height: 30,
                             x : 50, //the sprite x position
                             y : 10  //the sprite y position
                                 }]
				 }, //chart1
				 
				 {
					xtype: 'chart',
					id: 'chartCmp3',
					animate: true,
					shadow: true, 
					height: 240, 
					flex: 1,				
					
					store: {
						xtype: 'store',
						
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart1',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['COD','DESC', 'NR']
									
					}, //store					
					
				 
			            insetPadding: 40,
                        margin: '10px 0 0 0',
			            theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NR',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NR'));
			                    });
			                    this.setTitle(storeItem.get('COD') + ' - ' + storeItem.get('DESC') + ': ' + Math.round(storeItem.get('NR') / total * 100) + '% [M3 ' + floatRenderer2(storeItem.get('NR')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'COD',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmp3'); // id of the chart
									var index = cmp.store.findExact('COD', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.COD; // the field containing the label to display on the chart
								}			                    
			                }
			            }],items: [{
     						 type  : 'text',
     						 text  : 'Tipologia spedizione',
      						 font  : '14px Arial',
                             width : 200,
                             height: 30,
                             x : 50, //the sprite x position
                             y : 10  //the sprite y position
                                 }]
				 }, //chart2
				 
				  {
					xtype: 'chart',
					id: 'chartCmp4',
					animate: true,
					shadow: true, 
					//height: 200, 
					flex: 1,				
					
					store: {
						xtype: 'store',
						
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart2',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
								 extraParams: {
                                    open_request: <?php echo acs_je($request) ?>,
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,
							
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['COD','DESC', 'NR']
									
					}, //store					
					
				 
			           insetPadding: 40,
                       margin: '10px 0 0 0',
			           theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NR',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NR'));
			                    });
			                    this.setTitle(storeItem.get('COD') + ' - ' + storeItem.get('DESC') + ': ' + Math.round(storeItem.get('NR') / total * 100) + '% [M3 ' + floatRenderer2(storeItem.get('NR')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'COD',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmp4'); // id of the chart
									var index = cmp.store.findExact('COD', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.COD; // the field containing the label to display on the chart
								}			                    
			                }
			            }],items: [{
     						 type  : 'text',
     						 text  : 'Mezzo di spedizione',
      						 font  : '14px Arial',
                             width : 200,
                             height: 30,
                             x : 50, //the sprite x position
                             y : 10  //the sprite y position
                                 }]
				 }//chart3
        ]
				        
				        }	        
				          
          
          
          
          
            ]
        }]

       
     }]
        
 }

<?php exit; }
