<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();	


	$raw_post_data = acs_raw_post_data();
	if (strlen($raw_post_data) > 0)
	{	
		$m_params = acs_m_params_json_decode();
		$k_ordine = $m_params->k_ordine;
	}

	if (isset($_REQUEST['k_ordine']))
		$k_ordine = $_REQUEST['k_ordine'];

	$oe = $main_module->k_ordine_td_decode_xx($k_ordine);
	$ord = $main_module->get_ordine_by_k_docu($k_ordine);
	
	if ($m_params->trad == 'Y' && trim($ord['TDRFLO'])== '')    //non traduciamo se non e' impostata la lingua
	    $m_params->trad = 'N';

    
    $title = "Righe ordine {$oe['TDOADO']}_{$oe['TDONDO']} {$oe['TDPROG']}";
    $title .= " {$ord['TDDCON']}";
    if($m_params->trad == 'Y')
        $title_suf = " Traduzione {$ord['TDRFLO']}"; 
	
	// ******************************************************************************************
	// call avanza (per riga)
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'exe_richiesta_avanza'){
		$m_params = acs_m_params_json_decode();		
		$ret = array();
		
		$ao = new SpedAssegnazioneOrdini();
		//($causale_rilascio, $ar_par, $nrec = 0){
		$ao->avanza($m_params->set_causale, array('prog' => $m_params->prog), $m_params->nrec);
		
		$ret['success'] = true;
		echo acs_je($ret);
		exit;
	}
	
	
	
	
// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();

	global $backend_ERP;
	
	switch ($backend_ERP){

		case 'GL':
			$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_art']} ART
						ON RD.MECAR0 = ART.MACAR0
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_art_des_aggiuntive']} ART_DES_AGG
						 ON RD.MECAD0 = ART_DES_AGG.MHCAD0 
						AND RD.METND0 = ART_DES_AGG.MHTND0 
						AND RD.MEAND0 = ART_DES_AGG.MHAND0 
						AND RD.MENUD0 = ART_DES_AGG.MHNUD0
						AND RD.MERID0 = ART_DES_AGG.MHRID0
                    WHERE '1 ' = ? AND RD.MECAD0 = ? AND RD.METND0 = ? AND RD.MEAND0 = ? AND RD.MENUD0 = ?";
			
	
	
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']));
			
			while ($row = db2_fetch_assoc($stmt)) {
			$row['data_selezionata'] = sprintf("%08s", $dtep);
				$row['data_consegna'] = $row['MEDT20'];
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
					$row['RDART'] = acs_u8e($row['MECAR0']);
				$row['RDDART'] = acs_u8e($row['RDDART']);
							
				$row['RDRIGA'] = $row['MERID0'];
				$row['RDQTA'] = $row['MEQTA0'];
				$row['RDQTE'] = $row['MEQTS0'];
				
				$row['progressivo'] = $row['MEPRO0'];
				
				$row['residuo'] = max((int)$row['MEQTA0'] - (int)$row['RDQTE'], 0);
				$row['RDUM'] = $row['MEUNM0'];
				$row['RDDART'] = acs_u8e($row['MADES0']);
				
				if (strlen(trim($row['MHDE20'])) > 0){
					$row['RDDART'] .= "<br/>" . acs_u8e($row['MHDE20']);
				}
				

				if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
					$row['COMMESSA'] = acs_u8e($row['MECME0']);
				}				
			
				$data[] = $row;
			}
							
		break;
		
		
		default: //SV2
		    
		    
		    if($m_params->trad == 'Y'){
		        $join = "LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
		        ON RD.RDDT = TD.TDDT AND RD.RDTIDO=TD.TDOTID  AND RD.RDINUM=TD.TDOINU AND RD.RDAADO =TD.TDOADO AND digits(RD.RDNRDO) =TD.TDONDO
		        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
		        ON AL.ALDT=RD.RDDT AND AL.ALART=RD.RDART AND AL.ALLING=TD.TDRFLO";
		        
		    }else{
		        $join = "";
		    }
		    
			$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                    {$join}
					WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
					AND RD.RDTISR = '' AND RD.RDSRIG = 0";
            
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $oe);
			
			while ($row = db2_fetch_assoc($stmt)) {
				$row['data_selezionata'] = sprintf("%08s", $dtep);
					$row['data_consegna'] = sprintf("%02s", $row['RDAAEP']) . sprintf("%02s", $row['RDMMEP']) . sprintf("%02s", $row['RDGGEP']);
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
					$row['RDART'] = acs_u8e($row['RDART']);
				$row['RDDART'] = acs_u8e($row['RDDART']);
				$row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
				$row['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
				$row['ticket'] = substr($row['RDFIL1'], 64, 10);
				$data[] = $row;
			}
							
		
	} //switch $backend_ERP
	

			//metto prima le righe della data selezionata
				////usort($data, "cmp_by_inDataSelezionata");
	
			$ord_exp = explode('_', $n_ord);
			$anno = $ord_exp[3];
			$ord = $ord_exp[4];			
		  	$ar_ord =  $main_module->get_ordine_by_k_docu($n_ord);

			$m_ord = array(
								'TDONDO' => $ar_ord['TDONDO'],
								'TDOADO' => $ar_ord['TDOADO'],								
								'TDDTDS' => print_date($ar_ord['TDDTDS'])
						   );
			
		echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
		
		$appLog->save_db();		
exit();				
}



?>


{"success": true, 

m_win: {
				width: 900,
				height: 450,
				iconCls: 'icon-folder_search-16',
				title: <?php echo j($title) ?>,
				title_suf: <?php echo j($title_suf) ?>	
			},
items:
	{
		xtype: 'gridpanel',
		
		stateful: true,
        stateId: 'seleziona-righe-ordini-gest',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
        <?php if(trim($ord['TDFG07']) != 'P'){?>
        tbar: new Ext.Toolbar({
	          items:[ '->',{iconCls: 'icon-leaf-16',
	             	  text: 'Righe ordine esteso', 
		           			handler: function() {
				        			acs_show_win_std('Righe ordine oentry', 'acs_get_order_rows_gest_order_entry.php', {
				        				from_righe_info: '<?php echo $m_params->from_info; ?>',
				        				k_ordine: '<?php echo $k_ordine; ?>'
				        		}, 1000, 450, null, 'icon-folder_search-16');          		
				        		}
		           	 }
	           	
	         ]            
	        }),
	        <?php }?>
	    store: Ext.create('Ext.data.Store', {
					
					
					groupField: 'RDTPNO',			
					autoLoad:true,
					loadMask: true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&k_ordine=<?php echo $k_ordine; ?>',
							type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										
										trad: '<?php echo $m_params->trad; ?>'
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
						},
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART', 'RDUM', 'RDQTA'
	            			, 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna', 'data_selezionata' 
	            			, 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo', 'RDTIPR', 'RDNRPR' 
	            			, 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO', 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR'
	            			, 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE', 'ALDAR1', 'TDRFLO', 'RDPROG', 'ticket'
	            			, 'RDDIM1', 'RDDIM2', 'progressivo'
	        
	        			]
	    			}),
	    			
	    	<?php $trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>"; ?>
	    	<?php $cf1 = "<img src=" . img_path("icone/48x48/warning_black.png") . " height=20>"; ?>	
	    			
		        columns: [
					/*{
		                header   : '<br/>&nbsp;Cons.',
		                dataIndex: 'data_consegna', 
		                width: 60,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('inDataSelezionata') == 0) return ''; //non mostro la data se e' quella selezionata																	
						  					return date_from_AS(value);			    
										}		                
		                 
		             },		 */       
		             {
		                header   : '<br/>&nbsp;Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right'
		             },			        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width     : 110
		             }
		             
		             
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    			    	}}			    	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		        			dataIndex: 'ARESAU',		
    	     				tooltip: 'Articoli in esaurimento',		        			    	     
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARESAU') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
		    			    	}}			    			             
		             
		             
		             , {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150               			                
		             },
		             <?php if($m_params->trad == 'Y'){?>
		             
		             {
			            	text: '&nbsp;<br><?php echo $trad; ?>',
			                dataIndex: 'ALDAR1',
			                tooltip: 'Traduzione',
			                menuDisabled: true,
			                width : 30,
			                renderer: function(value, metaData, record){
				    			  if(!Ext.isEmpty(record.get('ALDAR1')) && record.get('RDART').substring(0, 1) != '*'){
									 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('ALDAR1')) + '"';  			    
					    			   return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=15>';
				    			  }else{
					    			 return '';
				    			  }
				    			  
				    		  }
			             },
		             
		             <?php }?>
		             
		             <?php if($backend_ERP == 'GL'){?>
		             
		             {
		                header   : '<br/>&nbsp;Dim.1',
		                dataIndex: 'RDDIM1', 
		                width    : 50,
		                renderer : floatRenderer2
		             },
		             {
		                header   : '<br/>&nbsp;Dim.2',
		                dataIndex: 'RDDIM2', 
		                width    : 50,
		                renderer : floatRenderer2
		             },
		             <?php }?>
		             
		              {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             }, {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                <?php if(trim($ord['TDFG07']) == 'P'){?>
		                hidden : true,
		                <?php }?>
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                <?php if(trim($ord['TDFG07']) == 'P'){?>
		                hidden : true,
		                <?php }?>
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;S',
		                dataIndex: 'RDSTEV', 
		                width    : 20,
		                align: 'right'
		             }, {
		                header   : '<br/>&nbsp;T.R.',
		                dataIndex: 'RDT	PRI', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Pr.',
		                dataIndex: 'RDPRIO', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;St.',
		                dataIndex: 'RDSTAT', 
		                width    : 30
		             }/*, {
		                header   : '<br/>&nbsp;Carico',
		                dataIndex: 'RDNRCA', 
		                <?php if(trim($ord['TDFG07']) == 'P'){?>
		                hidden : true,
		                <?php }?>
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRCA') == 0) return ''; //non mostro																											
						  					return rec.get('RDTICA') + "_" + rec.get('RDNRCA');			    
										}		                
		             }*/
		             
		             
				<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             , {
		                header   : '<br/>&nbsp;Commessa',
		                <?php if(trim($ord['TDFG07']) == 'P'){?>
		                hidden : true,
		                <?php }?>
		                dataIndex: 'COMMESSA', 
		                width    : 80
		             }				
		        <?php } else { ?>
		             /*, {
		                header   : '<br/>&nbsp;Lotto',
		                dataIndex: 'RDNRLO', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRLO') == 0) return ''; //non mostro																											
						  					return rec.get('RDTPLO') + "_" + rec.get('RDNRLO');			    
										}
		             }	*/	        
		        <?php } ?>
		        
		          , {text: '<?php echo $cf1; ?>',	
		                width: 30, tdCls: 'tdAction',
		                dataIndex: 'RDPROG', 
		        		tooltip: 'Visualizza domande/risposte',		        			    	     
		    			renderer: function(value, p, record){
		    			  if (record.get('RDPROG') > 0) return '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=18>';
		    		  }}
		        
		        
<?php if (strlen($m_params->prog) > 0) { ?>
		             , {
		                header   : '<br/>&nbsp;Causale',
		                dataIndex: 'CAUSALE', 
		                width    : 80
		             }
<?php } ?>


					<?php if($backend_ERP == 'GL'){?>
					, {
		                header   : '<br/>&nbsp;Data cons.',
		                dataIndex: 'data_consegna', 
		                width    : 70,
		                renderer : date_from_AS
		             }
					<?php }else{?>
		            , {
		                header   : '<br/>&nbsp;Ticket',
		                dataIndex: 'ticket', 
		                width    : 70
		             }
		             <?php }?>
		            
		         ]	    					
		
		, listeners: {		
	 			/*afterrender: function (comp) {
	 				//comp.up('window').setWidth(950);
	 				//comp.up('window').setHeight(500);
					//window_to_center(comp.up('window'));
					comp.up('window').setTitle('<?php echo "Righe ordine {$oe['TDOADO']}_{$oe['TDONDO']} {$oe['TDPROG']}"; ?>');	 				
	 			  <?php if($m_params->trad == 'Y'){?>
	 			  comp.up('window').setTitle( comp.up('window').title + ' - Traduzione  <?php echo $ord['TDRFLO']; ?>');
	 			  <?php }?>
	 			
	 			},*/
	 			
				 /* celldblclick2222: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	rec = iView.getRecord(iRowEl);
					  	acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('RDDT'), rdart: rec.get('RDART')}, 1100, 600, null, 'icon-shopping_cart_gray-16');
					  }
				  }*/
				  
				 celldblclick: {
			        		  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				   		            
					           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
								  	rec = iView.getRecord(iRowEl);

								  	if(Ext.isEmpty(rec.get('TDRFLO'))){
										var lingua = 'lingua non definita';
										var cod_lingua = null;
									 }else{
										var lingua = rec.get('TDRFLO').trim();
										var cod_lingua = rec.get('TDRFLO').trim();
									}

			    				    if(col_name == 'ALDAR1' && !Ext.isEmpty(cod_lingua) && rec.get('RDART').substring(0, 1) != '*')	
			    					   acs_show_win_std('Traduzione in ' + lingua, 
					    					'acs_gest_trad_colli_articoli.php?fn=open_tab', 
					    					{codice: rec.get('RDART'), desc : rec.get('RDDART'), lng: lingua, trad_ta : 'N'}, 400, 270,  {
				        					'afterSave': function(from_win){
				        						iView.getStore().load();
	 											from_win.close();
				        					}
				        				}, 'icon-globe-16');
				        				
				        			 if(col_name == 'RDPROG' && rec.get('RDPROG') > 0){	
			    					   acs_show_win_std('Configurazione ordine ' +'<?php echo "{$oe['TDOADO']}_{$oe['TDONDO']}"; ?>'  + 'riga ' +rec.get('RDRIGA') , 'acs_get_domande_risposte_riga.php?fn=open_form', {k_ordine: '<?php echo $k_ordine; ?>', prog: rec.get('RDPROG')}, 900, 450, null, 'icon-folder_search-16');          		
				        			  }
				        			  
				        			  if(col_name == 'ticket' && rec.get('ticket') > 0){	
			    					   acs_show_win_std('Ticket [' + rec.get('ticket').trim() + ']', 
            						     'acs_ticket_fuori_std.php?fn=open_tab',
            						     { ticket : rec.get('ticket'), autoload : 'firstRec', only_view : 'Y', hide_grid : 'Y'}, 400, 400, null, 'icon-receipt-16'); 		
				        			  }
			    				
				            	}

				        	 }
				  
				 , itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      var record = grid.getStore().getAt(index);				      		 
				      var voci_menu = [];
				      
<?php if (strlen($m_params->prog) > 0) { ?>

	
	<?php
	$ao = new SpedAssegnazioneOrdini();
	$rowAO = $ao->get_by_prog($m_params->prog);	
	
	#TODO: questo puo' andare bene anche per SBLOC: unificare
	$causali_rilascio = $main_module->find_TA_std('RILAV', $rowAO['ASCAAS'], 'N', 'Y'); //recupero tutte le RILAV
	foreach($causali_rilascio as $ca) {
	?>	
			  
		voci_menu.push({
		      		text: <?php echo j($ca['text']); ?>,
		    		iconCls: 'iconAccept',
		    		handler: function() {
	
								//carico la form dal json ricevuto da php
								Ext.Ajax.request({
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_avanza',
								        jsonData: {
													prog: <?php echo $m_params->prog ?>,
													nrec: rec.get('RDNREC'),
													set_causale: <?php echo j(trim($ca['TAKEY2'])); ?>							        
								        },
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){								        								        
								            var jsonData = Ext.decode(result.responseText);
								            grid.store.load();				            
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
			
				    }
				  });
	  <?php } ?>	
	

<?php } ?>

			
				/*voci_menu.push({
		      		text: 'Configura articolo',
		    		iconCls: 'iconAccept',
		    		handler: function() {
					    acs_show_win_std('Configuratore articolo', 
		  				'../base/acs_configuratore_articolo.php?fn=open_tab', 
		  				{	
		  					c_art : rec.get('RDART'),
		  					gestione_dim: 'N'
		  				}, 600, 400, {}, 'icon-copy-16');
			    	}
				  });*/
				  
				  
				  <?php if(trim($ord['TDFG07']) == 'P'){?>
				  voci_menu.push({
		      		text: 'Modifica riga',
		    		iconCls: 'icon-pencil-16',
		    		handler: function() {
		    		
    		    		var my_listeners = {
      	    	  		   'onCloseSuccess': function(from_win){
      	    	  		        grid.getStore().load();	
      	  	    	  		   	from_win.destroy();
      	    	  		   }
      	  		    	};
		    		
					    acs_show_win_std(null, 'acs_proposte_ordine.php?fn=open_form', {k_ordine : <?php echo j($k_ordine); ?>, rec: rec.data}, null, null, my_listeners);          		
			    	}
				  });
				 
				  
				  voci_menu.push({
		      		text: 'Duplica riga',
		    		iconCls: 'icon-comment_add-16',
		    		handler: function() {
		    		
		    		    var my_listeners = {
      	    	  		   'onCloseSuccess': function(from_win){
      	    	  		        grid.getStore().load();	
      	  	    	  		   	from_win.destroy();
      	    	  		   }
      	  		    	};
		    		
					    acs_show_win_std(null, 'acs_proposte_ordine.php?fn=open_form', {k_ordine : <?php echo j($k_ordine); ?>, rec: rec.data, duplica : 'Y'}, null, null, my_listeners);  
			    	}
				  });
				  
				   
				  voci_menu.push({
		      		text: 'Cancella riga',
		    		iconCls: 'icon-sub_red_delete-16',
		    		handler: function() {
		    		
		    		     Ext.Ajax.request({
    						        url: 'acs_proposte_ordine.php?fn=exe_cancella_riga',
    						        jsonData: {
    						        	rec: rec.data,
    						        	k_ordine : <?php echo j($k_ordine)?>						        	
    						        },
    						        method     : 'POST',
    						        waitMsg    : 'Data loading',
    						        success : function(result, request){	
    						            Ext.getBody().unmask();            	  													        
    						            var jsonData = Ext.decode(result.responseText);
    						            grid.getStore().load();		
    						        },
    						        failure    : function(result, request){
    						            Ext.getBody().unmask();
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
    						    });
					    
			    	}
				  });
				  
				  <?php }?>
				  
				  <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>

                	 voci_menu.push({
                	 		text: 'Programma fabbisogno materiali',
                	 		iconCls : 'icon-shopping_cart_gray-16',          		
                	 		handler: function() {
                	 			acs_show_win_std('Loading.', 'acs_background_mrp_art.php', {dt : <?php echo j($id_ditta_default); ?>, rdart : rec.get('RDART'), progressivo : rec.get('progressivo')},1000, 550, null, 'icon-shopping_cart_gray-16');
                	 		}
                	 	});
                	
                <?php } ?>
				  
				
				
			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				}).showAt(event.xy);
				      
				      
				 } //itemcontextmenu
				  
				  
				  
				  
				  
	 			 			
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';		        	
		         }   
		    }												    
			
		         
	}
}
