<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$email_subject = "Lista consegne da programmare (hold)";


	$itinerario = new Itinerari();
	$itinerario->load_rec_data_by_k(array("TAKEY1" => trim($_REQUEST['cod_iti'])));

	$ar_email_to = array();
	
	$ar_initial_value = '';
	
	//Specifico per report (per cliente o per agente)
	switch($_REQUEST['r_type']){
		case 'per_cliente':
			$r_cod_selected_ar = explode("|", $_REQUEST['r_cod_selected']);
			$email_cod = $s->get_email_cliente_dest($r_cod_selected_ar[0], $r_cod_selected_ar[1], $r_cod_selected_ar[2]);
			$ar_email_to[] = array($email_cod, "CLIENTE  (" .  trim($email_cod) . ")");
			$ar_initial_value = $email_cod;
			break;
		case 'per_agente':
			$r_cod_selected_ar = explode("|", $_REQUEST['r_cod_selected']);
			$des_agente = $s->get_des_agente($r_cod_selected_ar[0], $r_cod_selected_ar[1]) . " [{$r_cod_selected_ar[1]}]";
			
			$email_cod = $s->get_email_agente($r_cod_selected_ar[0], $r_cod_selected_ar[1]);
			$ar_email_to[] = array($email_cod, "AGENTE  (" .  trim($email_cod) . ")");
			$ar_initial_value = $email_cod;
			
			break;			
	}
	
	
	
	
	$ar_email_to[] = array(trim($itinerario->rec_data['TAMAIL']), "ITINERARIO " . j(trim($itinerario->rec_data['TADESC'])) . " (" .  trim($itinerario->rec_data['TAMAIL']) . ")");

	//se il vettore ha email di gruppo
	if (strlen(trim($vettore_parameters['email_gruppo'])) > 0)
		$ar_email_to[] = array(trim($vettore_parameters['email_gruppo']), "GRUPPO " . j(trim($vettore->rec_data['TADESC'])) . " (" . j(trim($vettore_parameters['email_gruppo'])) . ")" );			
	
	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);	 
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {
	
	//invio in formato excel
    if (Ext.get("p_send_excel") != null){		
	  	Ext.get("p_send_excel").on('click', function(){
	  		 html_text = document.getElementById('my_content').outerHTML;
	  		 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore
	  		 style_text = document.getElementsByTagName('style')[0].outerHTML;	 
	  		
	        Ext.Ajax.request({
	            url : 'send_excel.php' ,
	            method: 'POST',
	            jsonData: {html_text: html_text, style_text: style_text},
	            success: function ( result, request) {
	                //console.log(result.responseText);
	            	window.open('data:application/vnd.ms-excel,' + escape(result.responseText));                
	            },
	            failure: function ( result, request) {
	            }
	        });  			  		
	  	});
    }  	


	
	
  	Ext.get("p_send_email").on('click', function(){  		
	//html_text = document.getElementsByTagName('body')[0].innerHTML;
	 html_text = document.getElementById('my_content').outerHTML;
	 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore	 

	 style_text = document.getElementsByTagName('style')[0].outerHTML;
	 
	 html_text += style_text;

	
	var n_doc = document.implementation.createDocument ('http://www.w3.org/1999/xhtml', 'html',  null);
    n_doc.documentElement.innerHTML = html_text;
    
	html_text = n_doc.getElementsByTagName('html')[0].innerHTML;
  		
 		var f = Ext.create('Ext.form.Panel', {
        frame: true,
        title: '',        
        bodyPadding: 5,
        url: 'acs_op_exe.php',
        fieldDefaults: {
//            labelAlign: 'top',
            msgTarget: 'side',
            labelWidth: 55,
            anchor: '100%'            
        },
		layout: {
            type: 'vbox',
            align: 'stretch'  // Child items are stretched to full width
        },        

        items: [{
        	xtype: 'hidden',
        	name: 'fn',
        	value: 'exe_send_email'
        	}, {
            xtype: 'combo',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: [ 'email', 'descr' ],
                data: <?php echo $ar_email_json ?>
            }),
            <?php if (trim($ar_initial_value) != ''){ ?>
            	value: <?php echo j($ar_initial_value); ?>,
            <?php } ?>        	            
            displayField: 'descr',
            valueField: 'email',
            fieldLabel: 'A',
            queryMode: 'local',
            selectOnTab: false,
		    allowBlank: false,            
            name: 'to'
        }, {
            xtype: 'textfield',
            name: 'f_oggetto',
            fieldLabel: 'Oggetto',
            anchor: '96%',
		    allowBlank: false,
		    value: <?php echo j($email_subject) ?>	    
        }
        , {
            xtype: 'htmleditor',
            //xtype: 'textareafield',
            name: 'f_text',
            fieldLabel: 'Testo del messaggio',
            layout: 'fit',
            anchor: '96%',
            height: '100%',
		    allowBlank: false,
		    value: html_text,
		    flex: 1, //riempie tutto lo spazio
			hideLabel: true,
            style: 'margin:0' // Remove default margin		              
        }],

        buttons: [{
            text: 'Invia',
            scale: 'large',
            iconCls: 'icon-email_send-32',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var m_win = this.up('window');

					if (form.isValid()){            	
		                form.submit(
							{
			                 success: function(form,action) {		                            	
									m_win.close();		        
									if (action.result.success == true)										 
										acs_show_msg_info('Invio email completato');											 			 
									else
										acs_show_msg_error('Invio email non riuscito', action.result.error_message);
													            	
			                  },
			                  failure: function(form,action){
									m_win.close();
									acs_show_msg_error('Invio email non riuscito', action.result.error_message);
			                  }
			              }              	
		                );
					}
            	                	                
	             }            
        		}]
    });


		// create and show window
		var win = new Ext.Window({
		  width: 800
		, height: 380
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Invia e-mail'
		, layout: 'fit'
		, border: true
		, closable: true
		, items: f
		});
		win.show().maximize();  
    	
    		
    		
    	});		


		//apertura automatica finestra invio email
		<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
			Ext.get('p_send_email').dom.click();
		<?php } ?>
		
	
	});    
    
    
    
    
    

  	function invia_email2(){
	  html_text = document.getElementsByTagName('html')[0].innerHTML;
  		
      var postform = document.getElementById("my_form")
        if(!postform){
                var postform = document.createElement('form');
                postform.setAttribute('id','my_form');
            postform.setAttribute('name','my_form');
                postform.setAttribute('method','POST');
                        postform.setAttribute('target','my_iframe');
                postform.setAttribute('action', 'send_email.php');
            document.body.appendChild(postform);
        }

	var input = document.createElement('input');
    input.type = 'hidden';
    input.name = 'html_text';
    input.value = html_text; //JSON.stringify(document.getElementsByTagName('html')[0].innerHTM);
    postform.appendChild(input);

    postform.submit();   		
  		
  	}
  </script>


 </head>
 <body>
 	
<div class="page-utility noPrint">
	<A HREF="javascript:window.print();"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>
	<img id="p_send_email" src=<?php echo img_path("icone/48x48/address_black.png") ?>>	
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>
</div> 	

<?php


	 
	$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} 
				   LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				     ON TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
				   ";
	$sql_WHERE = " WHERE " . $s->get_where_std() . " AND TDSWSP='N' ";

	/* NON FILTRO MAI PER DATA, COSI' VENGONO FUORI EVENTUALI ANOMALIE */
	//if (isset($m_data)) $sql_WHERE .= " AND TDDTEP = $m_data ";
	
	if (isset($_REQUEST['cod_iti'])) $sql_WHERE .= " AND TDCITI = " . sql_t(trim($_REQUEST['cod_iti']));	
	if (isset($_REQUEST['k_cli_des']) && strlen(trim($_REQUEST['k_cli_des'])) > 0){
		 $k_cli_des_exp = explode("_", $_REQUEST['k_cli_des']);
		 $sql_WHERE .= " AND TDDT = " . sql_t(trim($k_cli_des_exp[0]));
		 $sql_WHERE .= " AND TDCCON = " . sql_t(trim($k_cli_des_exp[1]));
		 $sql_WHERE .= " AND TDCDES = " . sql_t(trim($k_cli_des_exp[2]));		 		 
	}	
	

	switch($_REQUEST['r_type']){
		case 'per_cliente':
			$r_cod_selected_ar = explode("|", $_REQUEST['r_cod_selected']);
			$sql_WHERE .= "	AND TDDT = " . sql_t($r_cod_selected_ar[0]);
			$sql_WHERE .= "	AND TDCCON = " . sql_t($r_cod_selected_ar[1]);
			$sql_WHERE .= "	AND TDCDES = " . sql_t($r_cod_selected_ar[2]);
		break;
		case 'per_agente':
			$r_cod_selected_ar = explode("|", $_REQUEST['r_cod_selected']);
			$sql_WHERE .= "	AND TDDT = " . sql_t($r_cod_selected_ar[0]);
			$sql_WHERE .= "	AND TDCAG1 = " . sql_t($r_cod_selected_ar[1]);
			break;		
	}

	$sql_WHERE.= sql_where_by_combo_value('TDCLOR', $_REQUEST['f_tipologia_ordine']);
	
	$sql_WHERE.= sql_where_by_combo_value('TDSTAT', $_REQUEST['f_stato_ordine']);
	
	$sql = "SELECT * " . $sql_FROM . $sql_WHERE	. " ORDER BY TDDTEP, TDCITI, TDNBOC, TDSECA, TDCCON";
	
	
	$stmt = db2_prepare($conn, $sql);		
	$result = db2_execute($stmt);
 
    $liv0_in_linea = null;
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$liv0 = implode("|", array($r['CSCITI']));
		$liv1 = implode("|", array($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA']));		
		$liv2 = implode("|", array($r['TDCCON'], $r['TDCDES']));		
		$liv3 = implode("|", array($r['TDDT'], $r['TDOTID'], $r['TDOINU'], $r['TDOADO'], $r['TDONDO']));		
		
		//data|itinerario|vettore
		$t_ar = &$ar;
		$l = $liv0;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r); 					  								  
			$t_ar = &$t_ar[$l]['children'];
		
		//carico
		$l = $liv1;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>implode("_", array($r['TDAACA'], $r['TDTPCA'], $r['TDNRCA'])), "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar[$l]['carico'] = $s->get_carico_td($r);
			$t_ar[$l]['orario_carico'] = $r['CSHMPG'];			 								  
			$t_ar = &$t_ar[$l]['children'];
		 								  								  
		//cliente|dest
		$l = $liv2;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];								  								  

		//ordine
		$l = $liv3;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];			
		
	} //while

/*
	echo "<pre>";
	foreach ($ar as $kl0 => $l0){
		echo "\n". $kl0;
	}	
	exit;
*/	

//	echo "<pre>"; print_r($ar); echo "</pre>";exit();
	
//STAMPO
 $cl_liv_cont = 0;
 if ($_REQUEST['stampa_dettaglio_ordini'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
 $liv2_row_cl = ++$cl_liv_cont; 
 $liv1_row_cl = ++$cl_liv_cont;
 
echo "<div id='my_content'>"; 
foreach ($ar as $kl0 => $l0){
	echo liv0_intestazione_open($l0['record']);

	  		foreach ($l0['children'] as $kl1 => $l1){
				echo liv1_intestazione_open($l1, $liv1_row_cl);	  			
			
			  		foreach ($l1['children'] as $kl2 => $l2){
						echo liv2_intestazione_open($l2, $liv2_row_cl);
						
						if ($_REQUEST['stampa_dettaglio_ordini']=="Y")
				  		foreach ($l2['children'] as $kl3 => $l3)
							echo liv3_intestazione_open($l3, $liv3_row_cl);			
							
					}			
			}		
	
	echo liv0_intestazione_close($l0['record']);	
}
echo "</div>";

 		
?>

 </body>
</html>







<?php




function somma_valori($ar, $r){
 //il volume e' aumentato in base alla percentuale passata (recuperata prima dall'itinerario)
 $ar['VOLUME'] 	+= $r['TDVOLU'] * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
 $ar['COLLI'] 	+= $r['TDTOCO'];
 $ar['PALLET'] 	+= $r['TDBANC'];
 $ar['IMPORTO'] += $r['TDTIMP']; 
 $ar['PESO'] 	+= $r['TDPLOR']; 
 return $ar;  
}

function liv3_intestazione_open($l, $cl_liv){


	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 3;
	else
		$n_col_span = 2;

	$ret = "
		<tr class=liv{$cl_liv}>
		 <td colspan=1>" . implode("_", array($l['record']['TDOADO'],  $l['record']['TDONDO'], $l['record']['TDOTPD'])) . " - " . $l['record']['TDDVN1'] . "</TD>
		 <td colspan=1>Rif: " . $l['record']['TDVSRF'] . "</TD>
		 <td colspan={$n_col_span}>[" . $l['record']['TDSTAT'] . "] ATP: " . print_date($l['record']['TDDTDS']) . "</td>		
		 <td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>		 
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";
	$ret .="
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD> 	 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'], 2) . "</TD>";		 
	
	$ret .= "</tr>";
 return $ret;
}

function liv2_intestazione_open($l, $cl_liv){

	global $main_module;

	if ($_REQUEST['indirizzo_di'] == 'ind_destinazione'){
		$m_loca = 'TDLOCA';
		$m_prov = 'TDPROV';
		$m_cap  = 'TDCAP';
	} else {
		$m_loca = 'TDDLOC';
		$m_prov = 'TDPROD';
		$m_cap  = 'TDDCAP';
	}
		

	$td_cli_class = '';
	if ($_REQUEST['r_type'] == 'per_cliente')
		$td_cli_class = 'grassetto';
	
	$ret = "
		<tr class=liv{$cl_liv}>
		 <td colspan=1 class=\"{$td_cli_class}\">" . $l['record']['TDDCON'] . "</TD>
		 <td colspan=1>" . $l['record'][$m_loca] . "</TD>		 
		 <td colspan=1>" . $l['record'][$m_prov] . "</TD>		 
		 <td colspan=1>" . $l['record'][$m_cap] . "</TD>";
	
	if ($_REQUEST['stampa_tel']=='Y')  $ret .= "<td colspan=1>" . $l['record']['TDTEL'] . "</TD> ";	
	$ret .= "
		 <td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>		 
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";		 
	$ret .= "
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD>	 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";
		 		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'], 2) . "</TD>";		 
	
	$ret .= "</tr>";
	
	if ($_REQUEST['stampa_segnalazioni_cli_des'] == 'Y'){
		//stampo, se ci sono, le note per cliente/destinazione
		if (
			$main_module->ha_note_cliente($l['record']['TDDT'], $l['record']['TDCCON']) ||
			$main_module->ha_note_cliente_dest($l['record']['TDDT'], $l['record']['TDCCON'], $l['record']['TDCDES'])
			){

			$txt_ar = array();
			
			$txt_note_cli = $main_module->get_note_cliente_portal($l['record']['TDDT'], $l['record']['TDCCON'], '');
			if (strlen($l['record']['TDCDES']) > 0)
				$txt_note_des = $main_module->get_note_cliente_portal($l['record']['TDDT'], $l['record']['TDCCON'], $l['record']['TDCDES']);
			
			if (strlen(trim($txt_note_cli)) > 0){
				// $txt_ar[] = "[ NOTE CLIENTE ] ";
				$txt_ar[] = $txt_note_cli;
			}			
			if (strlen(trim($txt_note_des)) > 0){
				// $txt_ar[] = "[ NOTE DESTINAZIONE ] ";
				$txt_ar[] = $txt_note_des;
			}

			$note_gest = $main_module->get_note_cliente_gest($l['record']['TDDT'], $l['record']['TDCCON']);
			while ($row = db2_fetch_assoc($note_gest)){
				// if (count($txt_ar) == 0 ) $txt_ar[] = "[ NOTE ANAGRAFICHE GESTIONALI ]";
				$txt_ar[] = trim($row['RDDES1']);
			}
				
			
			$n_colspan = 6;
			if ($_REQUEST['stampa_tel']=='Y') $n_colspan++;
			if ($_REQUEST['stampa_importi']=='Y') $n_colspan++;
			if ($_REQUEST['stampa_pallet']=='Y') $n_colspan++;
							
			$ret .= "<tr>
   				<td align=right valign=top>Segnalazioni</td>
   				<td colspan={$n_colspan} valign=top>" . implode("<br/>", $txt_ar) . "
   				</tr>";			

		}
							

		
	}
	
	
 return $ret;
}



function liv1_intestazione_open($l, $cl_liv){

	if ($l['orario_carico'] > 0)
		$orario_carico = ' [Orario: ' . print_ora($l['orario_carico']) . "]";
	else $orario_carico = '';

	if (strlen(trim($l['carico']['PSNOTE'])) > 0)
		$note_carico = "&nbsp;&nbsp;&nbsp;<b> [" . trim($l['carico']['PSNOTE']) . "]</b>";
	else $note_carico = '';

	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 5;
	else
		$n_col_span = 4;
	
	$span_carico_class = '';
	if ($_REQUEST['stampa_dettaglio_ordini'] != "Y")
		$span_carico_class = 'grassetto';
	
	$ret = "
		<tr class=liv{$cl_liv}>
		 <td colspan={$n_col_span}><span class=\"{$span_carico_class}\">TOTALI</span></TD>
		 <td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";		 
	$ret .= "
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD>		 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'],2) . "</TD>";		 

	$ret .="</tr>";
 return $ret;
}



function liv0_intestazione_open($r){
	global $s, $spedizione, $itinerario;

	if ($_REQUEST['data_riferimento'] == 'data_spedizione')
		$f_data = 'TDDTSP';
	else
		$f_data = 'TDDTEP';
	
	$ret = "
			<H2>LISTA CONSEGNE DA PROGRAMMARE (HOLD)</H2>
			<H3>" . $itinerario->rec_data['TADESC'] . "</span></H3>
			
			
			<table class=int0 width=\"100%\">
			 <tr>";

	
	  if ($_REQUEST['r_type'] == 'per_agente'){
			global $des_agente;
			$ret .= "<td>Agente: </td>
					 <td width=\"50%\">" . $des_agente . "</td>
					 </tr> 
					 <TR>
					  <TD COLSPAN=2> </td>
					  <TD>&nbsp;</TD>";					 
	  }

		$ret .= "	  						 

			 </TR> 			
			</table>
			
			
			<table class=int1>
			 <tr>
			  <th>Cliente</th>
			  <th>Localit&agrave;</th>
			  <th>Pr</th>			  			  
			  <th>Cap</th>";
	if ($_REQUEST['stampa_tel']=='Y')  $ret .= "<th>Telefono</T> ";	
	
	$ret .= "
			  <th>Colli</th>
			  ";
			  if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<th>Pallet</T> ";
	$ret .= "
			  <th>Volume</th>		  			  
			  <th>Peso</th>			  
			  ";			  
			  
	if ($_REQUEST['stampa_importi']=='Y') $ret .="<th>Importo</th>";			  
	
	$ret .= "</tr>";	
 return $ret;	
}

function liv0_intestazione_close($r){
	return "</table>";
}

function stampa_dati_vettore($cod_vettore){
	$vt = new Vettori();
	$vt->load_rec_data_by_k(array('TAKEY1' => $cod_vettore));
	return $vt->rec_data['TATELE'];
}

function stampa_dati_targa($targa){
  if (strlen(trim($targa)) > 0){
	return "<span style='margin-left: 90px;'>Targa: " . trim($targa) . "</span>";
  }
  else return "";	

}

?>		