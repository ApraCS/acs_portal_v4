<?php
require_once "../../config.inc.php";
require_once("../../utility/function_chart.php");
require_once("acs_analisi_anzianita_stato_ordini_include.php");
$s = new Spedizioni();


if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();

	$m_params = acs_m_params_json_decode();
	$form_values = $m_params->open_parameters->form_values;
	$stato_selected=$m_params->stato_selected;
	
	$sql_where=parametri_sql_where($form_values);
	
	
	$sql= "SELECT SUM (TDTIMP) AS T_IMPORTO, COUNT (TDSTAT) AS T_ORDINI
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	WHERE 1=1 {$sql_where}";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$row_tot = db2_fetch_assoc($stmt);

	$sql = "SELECT TDSTAT, TDDSST, COUNT (TDSTAT) AS NR_ORDINI, SUM (TDTIMP) AS IMPORTO, SUM (TDFN04) AS NR_HOLD, SUM (TDFN04*TDTIMP) AS HOLD_IMP
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	WHERE 1=1 {$sql_where} 
	GROUP BY TDSTAT, TDDSST ORDER BY NR_ORDINI DESC";
	
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$nr = array();
		$nr['TDSTAT'] 		= acs_u8e(trim($row['TDSTAT']));
		$nr['TDDSST']		= acs_u8e(trim($row['TDDSST']));
		$nr['NR_ORDINI']	= trim($row['NR_ORDINI']);
		$nr['PERC_ORD']		= $row['NR_ORDINI'] / $row_tot['T_ORDINI'];
		$nr['IMPORTO']		= trim($row['IMPORTO']);
		$nr['PERC_IMP']		= $row['IMPORTO'] / $row_tot['T_IMPORTO'];
		$nr['NR_HOLD']		= trim($row['NR_HOLD']);
		$nr['HOLD_IMP']		= trim($row['HOLD_IMP']);
		
		
		if ($nr['TDSTAT'] == $stato_selected)
			$nr['liv'] = 'liv_1';
		
		
		$data[] = $nr;
		
	}

	echo acs_je($data);

	exit;
}


// ******************************************************************************************
// DATI PER CHART2 (area o stab)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_chart1'){

	$data = array();
	$m_params = acs_m_params_json_decode();
	$form_values = $m_params->open_request->open_parameters->form_values;
	$stato_selected=$m_params->open_request->stato_selected;

	$sql_where=parametri_sql_where($form_values);
	
	$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $stato_selected);
	
	$sql = "SELECT {$m_params->campo_cod} AS COD, {$m_params->campo_desc} AS DESC, COUNT (*) AS ORD_RIT
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE  ON TD.TDDT=TA_ASPE.TADT AND TD.TDASPE=TA_ASPE.TAKEY1 AND TA_ASPE.TATAID='ASPE' 
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN  ON TD.TDDT=TA_ITIN.TADT AND TD.TDCITI=TA_ITIN.TAKEY1 AND TA_ITIN.TATAID='ITIN'
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TIPOV  ON TD.TDDT=TA_TIPOV.TADT AND TD.TDCLOR=TA_TIPOV.TAKEY1 AND TA_TIPOV.TATAID='TIPOV'
	WHERE 1=1 {$sql_where}
	GROUP BY {$m_params->campo_cod}, {$m_params->campo_desc}";


	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$nr = array();

		if (trim($row['COD']) == ''){
			$nr['COD']= "ND";
		}else{
			$nr['COD']	= acs_u8e(trim($row['COD']));
		}

		if (trim($row['DESC']) == ''){
			$nr['DESC']= "Non Definito";
		}else{
			$nr['DESC']	= acs_u8e(trim($row['DESC']));
		}

		$nr['NR']	= trim($row['ORD_RIT']);


		$data[] = $nr;
	}

	echo acs_je($data);
	exit;
}



$m_params = acs_m_params_json_decode();
$stato_selected=$m_params->stato_selected;
$desc_stato=$m_params->desc_stato;
?>

{"success":true, "items": [


                            {xtype: 'panel',
                          title: '[<?php echo $stato_selected ?>] <?php echo ucfirst(strtolower($desc_stato))?>',
				         autoScroll: true,
				         flex : 2,
				          tbar: new Ext.Toolbar({
		            items:[
		            '<b>Riepilogo incidenza stati ordine - Torte ripartizione stato [<?php echo $stato_selected ?>] <?php echo ucfirst(strtolower($desc_stato))?></b>', '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),
				         
					 <?php echo make_tab_closable(); ?>,			         
				          layout : {
				               type :'hbox',
				                 
				            },
				          items : [{ 
                             
                             
                             xtype: 'grid',
                            
                            
								features: [
									{
			     					ftype: 'summary',

										}],
								store: {
										xtype: 'store',
										autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',
								   
								    extraParams: <?php echo acs_je(acs_m_params_json_decode()) ?>,
                                     
                                 
                                doRequest: personalizza_extraParams_to_jsonData, 

							       actionMethods: {
							          read: 'POST'
							        },
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['TDSTAT', 'TDDSST',
		        			 {name: 'NR_ORDINI', type: 'float'}, 
		        			 {name: 'IMPORTO', type: 'float'},
		        			 {name: 'PERC_ORD', type: 'float'},
		        			 {name: 'PERC_IMP', type: 'float'}, 
		        			 {name: 'NR_HOLD', type: 'float'},
		        			 {name: 'HOLD_IMP', type: 'float'}, 'liv']							
									
			}, //store
				
	        columns: [	
				{
	                header   : 'Codice',
	                dataIndex: 'TDSTAT',
	                width: 50 
	            },{
	                header   : 'Descrizione',
	                dataIndex: 'TDDSST',
	                flex: 10
	            },{
	                header   : 'Nr ordini',
	                dataIndex: 'NR_ORDINI',
	                 align: 'right',
	                flex: 4,
	                summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return parseFloat(value); 
                                } 
	            },{
	                header   : 'Nr ordini %',
	                dataIndex: 'PERC_ORD',
	                renderer: perc2,
	                 align: 'right',
	                flex: 4,
	                summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return perc2(parseFloat(value));  
                                } 
	             
	            },{
	                header   : 'Importo',
	                dataIndex: 'IMPORTO',
	                renderer: floatRenderer0N,
	                 align: 'right',
	                flex: 5,
	                summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer0N(parseFloat(value))
                                       ; 
                                } 
	            },{
	                header   : 'Importo %',
	                dataIndex: 'PERC_IMP',
	                renderer: perc2,
	                 align: 'right',
	                flex: 4,
	                  summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return perc2(parseFloat(value));  
                                } 
	             
	            },{
	                header   : 'Nr Hold',
	                dataIndex: 'NR_HOLD',
	                renderer: floatRenderer0N,
	                flex: 5,
	                 align: 'right',
	                 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer0N(parseFloat(value)); 
                                } 
	            },{
	                header   : 'Importo Hold',
	                dataIndex: 'HOLD_IMP',
	                renderer: floatRenderer0N,
	                flex: 5,
	                 align: 'right',
	                 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer0N(parseFloat(value)); 
                                } 
	            }
	         ], listeners: {

                  itemclick: {								
						  fn: function(iView,rec,item,index,eventObj){
						  	grid_dettagli_riga = iView.up('panel').up('panel').getComponent('dettaglio_per_riga');
						  	grid_dettagli_riga.store.proxy.extraParams.stato = rec.get('TDSTAT');
						  	grid_dettagli_riga.store.load();
						  }
					  }	 
                    } ,  viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           
			    
			           return ret;																
			         }   
			    },
	
				               flex : 1
				            }, //FINE GRID		
				            
				             {
				          
				 
				xtype: 'panel',
				layout: 'accordion',
				width: 220,
				margin: '0px 0px 10px 10px',
                items: [
                {			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', width: 220,
				title: 'Area spedizioni',
				items: [
				<?php echo crea_json_chart("TDASPE", "TA_ASPE.TADESC")?>, //chart spedizioni
				
				]}, 	
				{			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', width: 220,
				title: 'Divisioni',
				items: [
				<?php echo crea_json_chart("TDCDIV", "TDDDIV")?>, //chart divisioni
				
				]}	, 	
				{			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', width: 220,
				title: 'Agenti',
				items: [
				<?php echo crea_json_chart("TDCAG1", "TDDAG1")?>, //chart agenti
				
				]}		
             
                
                ]}, 	            {
				xtype: 'panel',
				layout: 'accordion',
				width: 220,
				margin: '0px 0px 10px 10px',
                items: [
                {			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', width: 220,
				title: 'Itinerario',
				items: [
				<?php echo crea_json_chart("TDCITI", "TA_ITIN.TADESC")?>, //chart itinerario
				
				]}, 
				{			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', width: 220,
				title: 'Tipologia',
				items: [
				<?php echo crea_json_chart("TDCLOR", "TA_TIPOV.TADESC")?>, //chart tipologia
				
				]}	,	
				{			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', width: 220,
				title: 'Tipo',
				items: [
				<?php echo crea_json_chart("TDOTPD", "TDDOTD")?>, //chart tipo
				
				]}	, 	
					
             
                
                ]
				            } //fine primo gruppo di grafici

                
				]}
				]
		}
