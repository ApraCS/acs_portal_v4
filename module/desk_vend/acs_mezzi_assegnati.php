<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

$ar_k_cli_des = explode("_", $m_params->k_cli_des);
$cliente = trim($ar_k_cli_des[1]);
$destinazione = trim($ar_k_cli_des[2]);
$descrizione = get_TA_sys('VUDE', $destinazione, $cliente);
$desc_dest = "[".$destinazione."] ".trim($descrizione['text']);

// ******************************************************************************************
// EXE MODIFICA POSIZIONE (PEDANA)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiorna_mezzi'){
    $m_params = acs_m_params_json_decode();
    
    $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_tabelle']}
            WHERE TADT='{$id_ditta_default}' AND TATAID = 'MZESL' AND TAKEY1='{$cliente}'
            AND TAKEY2 = '{$destinazione}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
    
    foreach($m_params->list_selected_id as $v){
        
        $ar_ins = array();
        $ar_ins['TAUSGE'] 	= $auth->get_user();
        $ar_ins['TADTGE']   = oggi_AS_date();
        $ar_ins['TAORGE'] 	= oggi_AS_time();
        $ar_ins['TADT'] 	= $id_ditta_default;
        $ar_ins['TATAID'] 	= 'MZESL';
        $ar_ins['TAUSGE'] 	= $auth->get_user();
        $ar_ins['TAKEY1'] 	= $cliente;
        $ar_ins['TAKEY2'] 	= $destinazione;
        $ar_ins['TAKEY3'] 	= $v->mezzo;
        $ar_ins['TAKEY4'] 	= $m_params->chk_ie;
        if(strlen($v->note) > 0)
            $ar_ins['TADESC'] = $v->note;
        
        $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg($stmt);
        
    }
 
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'get_json_data'){
    
    $ar = array();
    
    $sql = "SELECT TA.*, TA_MZ.TADESC AS NOTA, TA_MZ.TAKEY4 AS I_E,
            CASE WHEN TA.TAKEY1=TA_MZ.TAKEY3 THEN 'Y' ELSE '' END AS ESCLUSO
            FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_MZ 
             ON TA.TADT = TA_MZ.TADT AND TA_MZ.TATAID = 'MZESL' AND TA.TAKEY1 = TA_MZ.TAKEY3
             AND TA_MZ.TAKEY1 = '{$cliente}' AND TA_MZ.TAKEY2 = '{$destinazione}'
            WHERE TA.TADT = '{$id_ditta_default}' AND TA.TATAID = 'AUTO'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while ($row = db2_fetch_assoc($stmt)) {
        $nr = array();
        $nr['d_mezzo']        = "[".trim($row['TAKEY1'])."] ".trim($row['TADESC']);
        $nr['mezzo']        = trim($row['TAKEY1']);
        $nr['escluso']      = $row['ESCLUSO'];
        $nr['i_e']          = trim($row['I_E']);
        $nr['note']         = $row['NOTA'];
        $nr['cliente']      = $cliente;
        $nr['destinazione'] = $destinazione;
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
    
}


if ($_REQUEST['fn'] == 'open_grid'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
					{
						xtype: 'grid',
				        loadMask: true,	
				        autoScroll : true,
				        selModel: {selType: 'checkboxmodel',  mode: 'SIMPLE'},
        				features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],	
            			plugins: [
            		          Ext.create('Ext.grid.plugin.CellEditing', {
            		            clicksToEdit: 1,
            		          })
            		      ],
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
										 k_cli_des: <?php echo j($m_params->k_cli_des); ?>,
																	
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['i_e', 'escluso', 'mezzo', 'd_mezzo', 'note', 'cliente', 'destinazione']							
									
			}, //store
			
			 columns: [	
				 
			       {
	                header   : 'Mezzo',
	                dataIndex: 'd_mezzo',
	                width : 300,
	                filter: {type: 'string'}, filterable: true,
	                 },
	                   {
	                header   : 'I/E',
	                dataIndex: 'i_e',
	                width : 40,
	                filter: {type: 'string'}, filterable: true,
	                 },
	                 {
	                header   : 'Note',
	                dataIndex: 'note',
	                flex : 1,
	                filter: {type: 'string'}, filterable: true,
	                   editor: {
		                 xtype: 'textfield',
		                 allowBlank: true
	           		}  
	                 }
	             
	         ]
	    
	         
	         , listeners: {
	              afterrender: function(comp){
	                    var win = comp.up('window');
	                    <?php if($descrizione != ''){?>
            	         win.setTitle(win.title + ' destinazione ' + <?php echo j($desc_dest)?>);
            	  		<?php }?>
            	  		
            	  		//autocheck degli ordini che erano stati selezionati
            	  		comp.store.on('load', function(store, records, options) {
            	  		   var recs_selected = [];
        					store.each(function(rec){
    	    					if (rec.get('escluso') == 'Y'){
    	    					  comp.getSelectionModel().select(rec, true);
    	    					
    	    				    }	    						    				
        					});						
    					}, comp);
    					
    					
            	  }
	         
				   
				 }, viewConfig: {
		       		 getRowClass: function(record, index) {	
		       		 
		       		                 		
                															
		        		 }   
		          },
		          
		        dockedItems: [{
                    dock: 'bottom',
                    xtype: 'toolbar',
                    scale: 'large',
                  
                    items: [
                    {
                     xtype: 'button',
               		 text: 'Conferma',
		             iconCls: 'icon-blog_accept-32',
		             scale: 'large',	                     
		             handler: function() {
		                    var loc_win = this.up('window');
		                    chk_ie = loc_win.down('#chk_ie').getValue().f_ie;
		                    var grid = this.up('grid');
		                    var id_selected= grid.getSelectionModel().getSelection();
		                    list_selected_id = [];
          	                for (var i=0; i<id_selected.length; i++){
        		 				record_data = id_selected[i].data;
        		  				list_selected_id.push(record_data);		  
          					 }
          					 
          					 	Ext.Ajax.request({
							        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna_mezzi',
							        jsonData: {
							            k_cli_des: <?php echo j($m_params->k_cli_des); ?>,
							        	list_selected_id: list_selected_id,
							        	chk_ie : chk_ie						        	
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){	            	  													        
							            var jsonData = Ext.decode(result.responseText);
							            loc_win.close();	
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							            console.log('errorrrrr');
							        }
							    });
		            
		            
		            
		            }},
		            
		            <?php 
		            
		            $sql = "SELECT TAKEY4
		                    FROM  {$cfg_mod_Spedizioni['file_tabelle']} 
		                    WHERE TADT = '{$id_ditta_default}' AND TATAID = 'MZESL'
                            AND TAKEY1 = '{$cliente}' AND TAKEY2 = '{$destinazione}'
                            LIMIT 1";
		            
		            $stmt = db2_prepare($conn, $sql);
		            echo db2_stmt_errormsg();
		            $result = db2_execute($stmt);
		            $row = db2_fetch_assoc($stmt);

		            ?>
		            
    		           {
    					xtype: 'radiogroup',
    					itemId : 'chk_ie',
    					items: [{
                                    xtype: 'radio'
                                  , name: 'f_ie' 
                                  , boxLabel: 'Inclusi'
                                  , inputValue: 'I'
                                  , width : 70
                                  <?php if(trim($row['TAKEY4']) == 'I' || !$row){?>
                                  , checked : true
                                  <?php }?>
                                },{
                                    xtype: 'radio'
                                  , name: 'f_ie' 
                                  , boxLabel: 'Esclusi'
                                  , inputValue: 'E'
                                  , width : 70
                                   <?php if(trim($row['TAKEY4']) == 'E'){?>
                                  , checked : true
                                  <?php }?>
                                  
                            }],
                            
                             listeners: {
    				    afterrender : function(comp) {	
    				      var grid = comp.up('window').down('grid');
    				     
    				    }
    				   }							
    				   },
    				  
                    
                    
                      ]
                     }]
				 
		
		
		}
		
	]}
	
	
	<?php 
	
exit;	
    
    
}