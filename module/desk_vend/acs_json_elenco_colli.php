<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();
$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// JSON GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_grid'){
//	print_r($m_params);
//	print_r(acs_je($m_params));
?>	
	
{"success":true, "items":
 {
  xtype: 'grid',
  title: '',
  id: 'panel-alert',
  closable: false,
  autoScroll: true,
  features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true,
						startCollapsed: true
					}),    
 
					store: {
						xtype: 'store',

						listeners: {
				            load: function () {
				                //win.setTitle('Elenco corrente colli ordine ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO);
				            }
				         },
					
												
						autoLoad:true,				        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								timeout: 2400000, 
								jsonData: <?php echo acs_je($m_params); ?>,
								method: 'POST',								
								type: 'ajax',
								reader: {
						            type: 'json',
						            root: 'root'
						        },

								actionMethods: {
									read: 'POST'					
								},
														        
								extraParams: {window_post: <?php echo acs_je(acs_raw_post_data()); ?>},					        
							},
		        			fields: [
		            			'gr_carico', 'TDDOCU', 'PLAADO', 'PLNRDO', 'k_ordine_out', {name: 'PLCOAB', type: 'int'}, 'PLART', 'PLDART', 'PLUM', 'PLQTA', 'PLDTEN', 'PLTMEN', 'PLDTUS', 'PLTMUS', 'PLDTRM', 'PLTMRM',
		            			'PSDTSC', 'PSTMSC', 'PSRESU', 'PSCAUS', 'PSPALM', 'PSLATI', 'PSLONG', 'LOTTO1', 'PLRIGA', 'PLNCOL', 'PLPNET'
		        			], 
		        			groupField: 'gr_carico'
		    			},
			        columns: [			        			        
			            {
			                header   : 'Ordine',
			                dataIndex: 'k_ordine_out', 
			                width: 90,
			             }, {
			                header   : 'Nr collo', align: 'right',
			                dataIndex: 'PLCOAB', 
			                width: 50,
			             }, {
			                header   : 'Codice',
			                dataIndex: 'PLART', 
			                flex    : 50
				         }, {
			                header   : 'Descrizione',
			                dataIndex: 'PLDART', 
			                flex    : 100
			             }, {
			                header   : 'UM',
			                dataIndex: 'PLUM', 
			                flex    : 10
			             }, {
			                header   : 'Quantit&agrave;',
			                dataIndex: 'PLQTA', 
			                flex    : 30,
			                align: 'right'
			             }, {
			                header   : 'Disponibilit&agrave;',
			                dataIndex: 'PLDTEN', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTEN'), record.get('PLTMEN'))
			    			}
			             }, {
			                header   : 'Spedizione',
			                dataIndex: 'PLDTUS', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTUS'), record.get('PLTMUS'))
			    			}			                
			             }, {
			                header   : 'Aggiunte',
			                dataIndex: 'PLDTRM', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTRM'), record.get('PLTMRM'))
			    			}			                			                
			             },{
				                header   : 'Scarico',
				                dataIndex: 'PSDTSC', 
				                flex    : 40,
				                renderer: function(value, metaData, record){
				                	metaData.tdAttr = 'data-qtip="Scanner: ' + Ext.String.htmlEncode(record.get('PSPALM')) + ' - Coordinate: ' + Ext.String.htmlEncode(record.get('PSLATI')) + ' - ' + Ext.String.htmlEncode(record.get('PSLONG')) + '"';
				                	return datetime_from_AS(record.get('PSDTSC'), record.get('PSTMSC'))
				    			}			                			                
				          }, {
				                header   : 'Info',
				                dataIndex: 'PSDTSC', 
				                width: 50,
				                renderer: function(value, metaData, record){

				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)
				                		metaData.tdAttr = 'data-qtip="Segnalazione allo scarico: ' + Ext.String.htmlEncode(record.get('PSCAUS')) + '"';
			                		
				                	if (Ext.isEmpty(record.get('PSRESU'))==false && record.get('PSRESU') == 'E')			    	
							    		return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)			    	
							    		return '<img src=<?php echo img_path("icone/48x48/comments.png") ?> width=18>';
				    			}			                			                
				          }
				          
			                <?php if($m_params->filtro->lotto1 == 'Y'){?> 
                	         , {header: 'Lotto1', dataIndex: 'LOTTO1', width: 70, align: 'right', renderer: floatRenderer0}
                	        <?php }?>
			         ],
			         
        listeners: {
	        itemclick: function(view,rec,item,index,eventObj) {
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');	        	        	

	        	wdi.store.proxy.extraParams.k_ordine = rec.get('TDDOCU');
				wdi.store.load();
	        	wdc.store.proxy.extraParams.k_ordine = rec.get('TDDOCU');
				wdc.store.load();				
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('TDDOCU'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				   }
				});
				
				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('TDDOCU')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('TDDOCU');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}				
	        },
	         itemcontextmenu : function(grid, rec, node, index, event) 
 		      { event.stopEvent();
 		        var voci_menu = [];
 		        
 		            voci_menu.push({
			         		text: 'Elenco completo articoli/componenti',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function() {
			        			acs_show_win_std(null, '../desk_utility/acs_elenco_riepilogo_art_comp.php?fn=open_elenco', 
					        				{k_ordine: rec.get('TDDOCU'), riga : rec.get('PLRIGA'), collo : rec.get('PLNCOL'), gr_collo : '', peso_n : rec.get('PLPNET')});   	     	
			        		}
			    		});
    		    		        
    		        var menu = new Ext.menu.Menu({ items: voci_menu }).showAt(event.xy);
 		        
 		        
 		        }        	
        }			         
	 }
 
 
}  
  	
	
	
<?php	
	exit;
}


// ******************************************************************************************
// JSON GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
    ini_set('memory_limit', '2048M');
	//bug parametri linux
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['window_post'] = strtr($_REQUEST['window_post'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));	
	
	$m_params = (array)json_decode($_REQUEST['window_post']);
	$m_params['sped_lotto'] = (array)$m_params['sped_lotto'];
	$m_params['filtro'] = (array)$m_params['filtro'];	

	$filtro = array();	

	if ($m_params['sped_lotto']['liv'] == 'liv_2')
		$filtro['sped'] 	= $m_params['sped_lotto']['sped_id'];
	else if ($m_params['sped_lotto']['liv'] == 'liv_3'){
		$filtro['sped'] 	= $m_params['sped_lotto']['sped_id'];
		$filtro['lotto'] 	= $m_params['sped_lotto']['lotto'];
	}
	
	$filtro['carico_assegnato'] = $m_params['filtro']['carico_assegnato'];
	$filtro['lotto_assegnato'] = $m_params['filtro']['lotto_assegnato'];
	$filtro['lotto1'] = $m_params['filtro']['lotto1'];
	
	$filtro['stato_spunta'] = $m_params['filtro']['col_name'];
	if(isset($m_params['filtro']['gruppo']))
	    $filtro['gruppo'] = trim($m_params['filtro']['gruppo']);
    if(isset($m_params['filtro']['liv']))
        $filtro['liv'] = trim($m_params['filtro']['liv']);
    if(isset($m_params['filtro']['RDART']))
       $filtro['PLOPE2'] = trim($m_params['filtro']['RDART']);
    
    $filtro['da_data'] = trim($m_params['filtro']['da_data']);
    
    if (isset($m_params['filtro']['open_request'])){
        $open_request = (array)$m_params['filtro']['open_request'];
        
        $filtro_type = $open_request['filtro_type'];
        $filtro_c    = $open_request['filtro_c'];
        switch ($filtro_type) {
            case 'LOTTO':
                //ricevo LP_2016_92601_ (in fondo opzionalmente trovo la ditta origine)
                $ar_filtro_c = explode('_' , $filtro_c);
                $filtro['TDTPLO'] = $ar_filtro_c[0];
                $filtro['TDAALO'] = $ar_filtro_c[1];
                $filtro['TDNRLO'] = $ar_filtro_c[2];
                //if (count($ar_filtro_c) == 4)
                //    $sql_WHERE .= " AND TDDTOR = " . sql_t($ar_filtro_c[3]);
                break;
            case 'CARICO':
                //ricevo CA_2016_92601_ (in fondo opzionalmente trovo la ditta origine)
                $ar_filtro_c = explode('_' , $filtro_c);
                $filtro['TDTPCA'] = $ar_filtro_c[0];
                $filtro['TDAACA'] = $ar_filtro_c[1];
                $filtro['TDNRCA'] = $ar_filtro_c[2];
                if (count($ar_filtro_c) == 4)
                    $filtro['TDDTOR'] = $ar_filtro_c[3];
                break;
        }
        
    }
      
	$stmt = $main_module->get_cols_ordine_adv($filtro);

	$data = array();
	while ($row = db2_fetch_assoc($stmt)) {
		$row['k_ordine_out'] = implode("_", array($row['PLAADO'], $row['PLNRDO'], $row['PLTPDO']));
		$row['gr_carico'] = implode("_", array($row['TDAACA'], $row['TDNRCA'], $row['TDTPCA']));
		$row['PLDART'] = acs_u8e($row['PLDART']);
		$data[] = $row;
	}	

	echo acs_je($data);
	
exit;
}
?>



