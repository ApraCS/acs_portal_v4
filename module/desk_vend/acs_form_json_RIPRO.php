<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$oggi = oggi_AS_date();

//recupero l'elenco degli ordini interessati
$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

//attualmente e' selezione singola
$row_selected = $m_params->list_selected_id[0];


?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            defaults:{ anchor: '-10' , labelWidth: 130 },
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'exe_create_RIPRO'
                	}, {
	                	xtype: 'hidden',
	                	name: 'list_selected_id',
	                	value: '<?php echo $list_selected_id_encoded; ?>'
                	}, {
						name: 'f_causale',
						id: 'f_entry_prog_causale',
						xtype: 'combo',
						fieldLabel: 'Causale di rilascio',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: false,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}, 'TARIF2'],
						    data: [								    
							     <?php echo acs_ar_to_select_json($s->find_TA_std('RIPRO'), ""); ?>	
							    ] 
						},
						
												 
					}, {
						name: 'f_desc',
						id: 'f_entry_prog_note',
						xtype: 'textfield',
						fieldLabel: 'Descrizione',
					    maxLength: 100							
					},
						
						
						 {
						name: 'f_segn',
						xtype: 'textareafield',
						height: 40,
						fieldLabel: 'Segnalazioni',
					    maxLength: 250							
					},
					{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data ATP'
						   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-10'
						   , margin: "5 200 5 0"
			
						}, {
							flex: 1,						 
							name: 'f_bloc',
							xtype: 'checkboxgroup',
							fieldLabel: 'Blocco',
							labelAlign: 'left',
						   	allowBlank: true,
						 	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_dettaglio_destinazioni' 
		                          , boxLabel: ''
		                          , inputValue: 'Y'
		                        }]														
						 }
					
					
					],
			buttons: [{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')

					if(form.isValid()){
					
									//non e' richiesta la cusale successiva....salvo e chiudo
												            	
						                form.submit({
					                            //waitMsg:'Loading...',
					                            success: function(form,action) {
					                            
			                    				window.close();        	
					                            },
					                            failure: function(form,action){
					                                Ext.MessageBox.alert('Erro');
					                            }
					                        });
				            		}            	                	                
	            }
	        }],             
				
        }
]}