<?php


 $todo_data_config = array(
 		//causale da non mostrare nel ToDo perche' hanno loro panel
        'escludi_da_todo' => array('SBLOC', 'IMMOR', 'REFOR', 'EXPO1', 'EXPO2', 'VAROC'),
 		'icona_ricerca_ordine' => array('IMMOR'),
 		'columns_show'	=> array(
 			//'OP'	    => array('SBLOC', 'IMMOR', 'REFOR'),
 		    // 'M' => array('EXPO1', 'EXPO2', 'text'=>'O'),
 		    'OP'	    => array('SBLOC'),
 			'cliente'	=> array('IMMOR'),
 			'seq_carico' => array('SBLOC'),
 			'data_emiss_OF' => array('SBLOC'),
 			'pagamento' => array('SBLOC'),
 			'preferenza' => array('SBLOC'),
 			'scarico' => array('SBLOC'),
 			
 		),
 		
 		'columns_hide'	  => array(
 			//'O'		=> array('SBLOC', 'IMMOR', 'REFOR'),
 			//'P'		=> array('SBLOC', 'IMMOR', 'REFOR'),
 		    
 		    //'M' => array('EXPO1', 'EXPO2'),
 		    'A' => array('EXPO1', 'EXPO2'),
 		    'CV' => array('EXPO1', 'EXPO2'),
 		    'O'		=> array('SBLOC', 'EXPO1', 'EXPO2'),
 		    'OP'		=> array('EXPO2'),
 		    'OM'		=> array('EXPO2'),
 		    'P'		=> array('SBLOC', 'EXPO1', 'EXPO2'),
 		    'stato'     => array('IMMOR'),
 		    'priorita'  => array('IMMOR'),
 		    'dispon'    => array('IMMOR', 'REFOR', 'EXPO1', 'EXPO2'),
 			'art_da_prog' => array('SBLOC'),
 		    'cons_rich'   => array('EXPO2')
 		)
 );

		/*
		 * ELENCO ORDINI ARRIVI
		 */
		
		 function get_elenco_ordini_arrivi($filtro, $order_by = null){
			global $conn;
			global $cfg_mod_Spedizioni, $main_module, $cfg_mod_Admin;
		
			if (is_null($order_by))
				$sql_order_by = 'TA_ATTAV.TAPESO, ASCAAS, ASUSAT, TDDCON, TDCCON, TDDTEP, TDDTDS, TA_ITIN.TAASPE, TA_SEQST.TASITI, TDSTAT, TDONDO';
			else			
				$sql_order_by = $order_by;
		
			//Se ho applicato dei filtri ------------------------------------------------------------------
			$m_params = acs_m_params_json_decode();

			global $is_linux;
			if ($is_linux == 'Y')
				$_REQUEST['todo_params'] = strtr($_REQUEST['todo_params'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));			
			
			$all_params = array_merge((array)$_REQUEST);			
			$all_params = array_merge((array)$_REQUEST,(array)acs_m_params_json_decode(), (array)json_decode($_REQUEST['todo_params']));
			$m_params = (object)$all_params;
			

			if (trim($m_params->tipo_op) == 'SBLOC' || (isset($filtro["ASCAAS"]) && trim($filtro["ASCAAS"]) == 'SBLOC')){
			    $is_SBLOC = 'Y';
			} else {
			    $is_SBLOC = 'N';
			}
			
			$where = sql_where_params($filtro, $m_params);
		
			$group_f = implode(', ', array('TA_ITIN.TAASPE', 'ASCAAS', 'ASUSAT', 'TDSTAT', 'TASITI', 'TDDCON', 'TDCCON', 'TDCDES'));
			$list_f = ' * ';
				
			$list_f	  = " TD.*, AO.*, SP.*,
						  WTDTGE, WTORGE, WTFIDO, WTEXPO, WTDTGE, WTORGE, 
						  TA_ITIN.TAASPE AS TAASPE, TA_ITIN.TASITI AS ITIN_TASITI, TA_ITIN.TADESC AS ITIN_TADESC, TA_SEQST.TASITI AS SEQST ";
			$group_by = "";			
			$sql_join = "";
			
			if (trim($m_params->tipo_op) == 'EXPO2'){
			    $sql_join .= "LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
			                   ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = AO.ASIDPR AND NT_MEMO.NTSEQU=0";
			    $list_f .= " , NT_MEMO.NTMEMO AS MEMO "; 
			}

			if ($is_SBLOC == 'Y') {
			  $sql = "SELECT {$list_f}, 'SBLOC' AS ASCAAS_FROM_TD
				FROM {$cfg_mod_Spedizioni['file_testate']} TD 
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} AO 
					ON TD.TDDOCU = AO.ASDOCU AND AO.ASCAAS = 'SBLOC' AND ASFLRI <> 'Y'
				" . $main_module->add_riservatezza() . "
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
					ON TD.TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL
					ON TD.TDDT = CAL.CSDT AND TD.TDDTEP = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']}
					ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
					ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_SEQST
					ON TD.TDDT = TA_SEQST.TADT AND TA_SEQST.TATAID = 'SEQST' AND TA_SEQST.TAKEY1 = TD.TDSTAT
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_analisi_esposizione']} CLI_ESPO
					ON TD.TDDT = CLI_ESPO.WTDT AND TD.TDCCON = DIGITS(CLI_ESPO.WTCCON)
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ATTAV
					ON TD.TDDT = TA_ATTAV.TADT AND TA_ATTAV.TATAID = 'ATTAV' AND TA_ITIN.TAKEY1 = 'SBLOC'
			    WHERE (" . $main_module->get_where_std(null, array('filtra_su' => 'AS')) . "  $where)
				{$group_by}
				ORDER BY {$sql_order_by}
				";	
			} else {
			    $list_f .= ", UTDESC ";
			    $sql = "SELECT {$list_f}
					FROM {$cfg_mod_Spedizioni['file_assegna_ord']} AO
						INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
							ON TD.TDDOCU = AO.ASDOCU
						" . $main_module->add_riservatezza() . "	
						LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
							ON TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
						LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL
							ON TD.TDDT = CAL.CSDT AND TD.TDDTEP = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
						LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']}
					  		ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
					  	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
						  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
					  	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_SEQST
						  ON TD.TDDT = TA_SEQST.TADT AND TA_SEQST.TATAID = 'SEQST' AND TA_SEQST.TAKEY1 = TD.TDSTAT
					  	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_analisi_esposizione']} CLI_ESPO
						  ON TD.TDDT = CLI_ESPO.WTDT AND TD.TDCCON = DIGITS(CLI_ESPO.WTCCON)
					  	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ATTAV
						  ON TD.TDDT = TA_ATTAV.TADT AND TA_ATTAV.TATAID = 'ATTAV' AND TA_ATTAV.TAKEY1 = AO.ASCAAS	
					    LEFT OUTER JOIN {$cfg_mod_Admin['file_utenti']} UT
						  ON AO.ASUSAT = UT.UTCUTE 
						{$sql_join}
					WHERE (" . $main_module->get_where_std('search', array('filtra_su' => 'AS,TD')) . "  $where)
					{$group_by}
					ORDER BY {$sql_order_by}
					 ";
			}
			
	  $stmt = db2_prepare($conn, $sql);
	  echo db2_stmt_errormsg();
	  $result = db2_execute($stmt);
	 return $stmt;
	}
		
		

	function crea_array_valori_el_ordini_arrivi($its, $tipo_elenco = "", $node, $generazione_completa = 'N', $m_params = null){
				global $auth, $conn, $cfg_mod_Spedizioni, $main_module;
				
				if (is_null($m_params))
					$m_params = (object)array();
				
				$ar = array();
				$ar_tot = array( "TDNBOC" => array() );
				$ar_liv_mapping = array();
				$node_livs = explode("|", $_REQUEST["node"]);
			
				$user = $auth->get_user();
			
				$liv0_v = $liv1_v = $liv2_v = $liv3_v = $liv4_v = $liv5_v = "--------------";
				$liv0_c = $liv1_c = $liv2_c = $liv3_c = $liv4_c = $liv5_c = -1;
			
				$n_children = "children";
			
				$sql_visionato = "UPDATE {$cfg_mod_Spedizioni['file_assegna_ord']} SET ASFVIU='Y' WHERE ASIDPR = ?";
				$stmt_visionato = db2_prepare($conn, $sql_visionato);
			
				while ($row = db2_fetch_assoc($its)) {
			
					//definizione livello di alberatura
					
				    $r_livs = imposta_livs($row, $m_params);
						
					//FLAG BLOCCO
					// 4 - amministrativo e commerciale
					// 3 - amministrativo
					// 2 - commerciale
					// 1 - sbloccato
					$m_tipo_blocco = null;
					if ($row['TDBLEV'] == 'Y' || $row['TDBLOC'] == 'Y')
					{
						if ($row['TDBLEV'] == 'Y' && $row['TDBLOC'] == 'Y')	$m_tipo_blocco = 4;
						else {
							if ($row['TDBLEV'] == 'Y') $m_tipo_blocco = 3;
							else if ($row['TDBLOC'] == 'Y') $m_tipo_blocco = 2;
						}
					}
			
					if (($row['TDBLEV'] != 'Y' && $row['TDBLOC'] != 'Y') && ($row['TDBLEV'] == 'S' || $row['TDBLOC'] == 'S')) //sbloccato
						$m_tipo_blocco 	= 1;
			
			
					$row["fl_bloc_ordine"] = $m_tipo_blocco;
						
					$tmp_ar_id = array();
			
					// LIVELLO 0 (tipo operazione)
					$s_ar = &$ar;
					$liv 	= $r_livs['liv0_v'];
					$tmp_ar_id[] = $liv;
					if (is_null($s_ar[$liv])){
						$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_0", "task"=> $liv);
						$s_ar[$liv]["task"]   = $main_module->decod_std('ATTAV', $liv);
						
						if (strlen($m_params->tipo_op) > 0)
							$s_ar[$liv]["expanded"] = true;						
			
					}
			
					$s_ar[$liv] = el_arrivi_liv($s_ar[$liv], $row, 0, $m_params);
					$s_ar = &$s_ar[$liv][$n_children];
			
					if ($generazione_completa == 'N')
						if (count($node_livs) == 0 || $node=='root') continue;  //carico livello per livello
			
					// LIVELLO 1 (utente assegnato)
					$liv 	= $r_livs['liv1_v'];
						if ($liv != 'NO-LIV'){					
						$tmp_ar_id[] = $liv;
						if (is_null($s_ar[$liv])){
							$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_1", "task"=> $r_livs['liv1_d']);
						}
							
						$s_ar[$liv] = el_arrivi_liv($s_ar[$liv], $row, 1, $m_params);
						$s_ar = &$s_ar[$liv][$n_children];
				
						if ($generazione_completa == 'N')					
							if (count($node_livs) == 1) continue;  //carico livello per livello
					}
						
					// LIVELLO 2 (utente assegnato o in SBLOC il cliente)
					$liv 	= $r_livs['liv2_v'];
					if ($liv != 'NO-LIV'){					
						$tmp_ar_id[] = $liv;
						if (is_null($s_ar[$liv])){
							$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_2", "task"=> $r_livs['liv2_d']);
				
							if ($r_livs['liv0_v'] == 'SBLOC'){
								$s_ar[$liv]["row_cls"] = 'grassetto';
								
								$stmt_note_gest = $main_module->get_note_cliente_gest(trim($row['TDDT']), trim($row['TDCCON']), 'NCBLO');
								$txt_ar = array();
								while ($row_n = db2_fetch_assoc($stmt_note_gest))
									
									$txt_ar[] = trim($row_n['RDDES1']);
								
									
									$txt_note_gest = implode("\n", $txt_ar);
								
								if (strlen(trim($row['TDDSRI'])) > 0){
									$s_ar[$liv]["riferimento"] 	= "[" . trim($row['TDRISC']) . "] " . trim($row['TDDSRI']);
							         }
							      
								
								$s_ar[$liv]["riferimento"] 	.= "<div style='font-weight: normal;'>" .$txt_note_gest."</div>";
								
					     		$s_ar[$liv]["TASITI"]		= $row['ITIN_TASITI'];
								$s_ar[$liv]["dt_orig"]		= $row['TDDTOR']; //ditta origine
								
								// Controllo tra Fido standard -> Saldo esposizione (R/V/G)
								if (strlen(trim($row['WTEXPO'])) > 0){
									if ($row['WTEXPO'] >= ($row['WTFIDO'] * 1.05))
										$s_ar[$liv]["stato"] = 'R';
									if ($row['WTEXPO'] < ($row['WTFIDO'] * 1.05) && $row['WTEXPO'] > ($row['WTFIDO'] * 0.95))
										$s_ar[$liv]["stato"] = 'G';							
									if ($row['WTEXPO'] <= ($row['WTFIDO'] * 0.95))
										$s_ar[$liv]["stato"] = 'V';							
									
									if (strlen($row['WTDTGE']) == 8){
										$s_ar[$liv]["tooltip_ripro"] = '<B>Analisi esposizione del ' . print_date($row['WTDTGE']) . " - " . print_ora($row['WTORGE']) . "</B>";
										$s_ar[$liv]["tooltip_ripro"].= '<br/>Esposizione: ' . n($row['WTEXPO']) . " - Fido: " . n($row['WTFIDO']); 
									}
									
									if ($row['WTFIDO'] == 0)
										$s_ar[$liv]["stato"] = 'B';								
									
								}							
							}
				
							//cliente bloccato
							if ($row['TDCLBL'] == 'Y' || $row['TDCLBL'] == 'E' || $row['TDCLBL'] == 'P')
							{
								$s_ar[$liv]["iconCls"] 	= "iconBlocco";
							}
				
				
						}

						//ToDo: parametrare in ATTAV (sono le voci che mi fermo a livello e stato e poi visualizzo elenco ordini)
						//Assegna causa assistenze e verifica garanzia assistenza
						//if ($r_livs['liv0_v'] == 'ASSCAU')
						if (in_array($r_livs['liv0_v'], array('ASSCAU', 'VERGAR')))
							$s_ar[$liv]["leaf"] = true;
						
							
						$s_ar[$liv] = el_arrivi_liv($s_ar[$liv], $row, 2, $m_params);
						$s_ar = &$s_ar[$liv][$n_children];
				
						if ($generazione_completa == 'N')					
							if (count($node_livs) == 2) continue;  //carico livello per livello
					}
						
						
					// LIVELLO 3 (stato ordine, o su SBLOC area di spedizione)
					$liv 	= $r_livs['liv3_v'];
					if ($liv != 'NO-LIV'){					
						$tmp_ar_id[] = $liv;
						if (is_null($s_ar[$liv])){
							$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_3", "task"=> $r_livs['liv3_d']);
							////$s_ar[$liv]["task"]   = "[" . $row['TDSTAT'] . "] ". acs_u8e($row['TDDSST']);
						}
						$s_ar[$liv] = el_arrivi_liv($s_ar[$liv], $row, 3, $m_params);
						
						if ($r_livs['liv0_v'] == 'EXPO1' || $r_livs['liv0_v'] == 'EXPO2'){
						    $s_ar[$liv]["riferimento"]  = $main_module->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD']);
						    //se ha note cliente o destinazione
					    if ($main_module->ha_note_cliente($row['TDDT'], $row['TDCCON']) ||
					        $main_module->ha_note_cliente_dest($row['TDDT'], $row['TDCCON'], $row['TDCDES'])
					        )
					        $s_ar[$liv]["priorita"] = '<span><a href="javascript:show_win_note_cliente(\'' . $row['TDDT'] . '\', \'' . $row['TDCCON'] . '\', \'' . $row['TDCDES'] . '\')";><img class="cell-img" src=' . img_path("icone/16x16/blog_compose.png") . '></a></span>';
					 
						}
						
						
						if ($r_livs['liv0_v'] == 'REFOR' || $r_livs['liv0_v'] == 'EXPO1')
							$s_ar[$liv]["expanded"] = false;
						else 
							$s_ar[$liv]["expanded"] = true;
						
						$s_ar = &$s_ar[$liv][$n_children];
					}
						
					// mostro subito (sempre) il livello successivo
					//if ($node_livs[0]=='SBLOC' && count($node_livs)==3) continue;

					// LIVELLO 4 (cliente)
					$liv 	= $r_livs['liv4_v'];
					if ($liv != 'NO-LIV'){
						$tmp_ar_id[] = $liv;
						if (is_null($s_ar[$liv])){
							$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_4", "task"=> $r_livs['liv4_d']);
														
							if ($r_livs['liv0_v'] != 'SBLOC'){
							    
							     $s_ar[$liv]["riferimento"] 	= $row['ITIN_TADESC'];
							     $s_ar[$liv]["TASITI"]		= $row['ITIN_TASITI'];
							}
							
							
							    
							
							
						
						}
							
						$s_ar[$liv] = el_arrivi_liv($s_ar[$liv], $row, 4, $m_params);
						$s_ar = &$s_ar[$liv][$n_children];
					}	

			
						
					// LIVELLO 5 (ordine)
					$liv 	= $r_livs['liv5_v'];
					$tmp_ar_id[] = $liv;
					if (is_null($s_ar[$liv])){
						$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_5", "task"=> $r_livs['liv5_d']);
			
						$s_ar[$liv]["leaf"]   	   	= true;
						$s_ar[$liv]["prog"]   	   	= $row['ASIDPR'];
			
						//se l'utente corrente e' il destinataraio del messaggio, spunto la riga come letta
						if (trim($row['ASFVIU']) != 'Y'){
							if (trim($user) == trim($row['ASUSAT'])){
								db2_execute($stmt_visionato, array($row['ASIDPR']));
							}
						}
						
						
					}
					
					$s_ar[$liv] = el_arrivi_liv($s_ar[$liv], $row, 5, $m_params);
					
					if ($r_livs['liv0_v'] == 'IMMOR'){
					    if (trim($row['TDFG03']) == 'Y'){
					        $img_entry_name = "icone/16x16/rss_blue.png";
					        $s_ar[$liv]['task'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '> </span>';
					    }
					    if (in_array($row['TDOWEB'], array('Y', 'C'))){
				            $img_entry_name = "icone/16x16/globe.png";
				            $s_ar[$liv]['task'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '> </span>';
				        }
					    
					}
					
					if ($r_livs['liv0_v'] == 'EXPO1' || $r_livs['liv0_v'] == 'EXPO2'){
					    
					    //icona entry (per attivita' in corso sull'ordine)
					    $sa = new SpedAssegnazioneOrdini();
					    if ($sa->ha_entry_aperte_per_ordine($row['TDDOCU'], 'Y')){
					        $img_entry_name = "icone/16x16/arrivi_gray.png";
					        $s_ar[$liv]['task'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><a href="javascript:show_win_entry_ordine(\'' . $row['TDDOCU'] . '\', \'' . $liv1_v . '\', \'' . $liv2_v . '\')";><img class="cell-img" src=' . img_path($img_entry_name) . '></a> </span>';
					    }
					    
					    //icona riprogrammazione (se e' stato riprogrammato)
					    $sr = new SpedAutorizzazioniDeroghe();
					    if ($sr->ha_RIPRO_by_k_ordine($row['TDDOCU'])){
					        $img_entry_name = "icone/16x16/button_black_repeat_dx.png";
					        $s_ar[$liv]['task'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '></span>';
					        
					        $ar_tooltip_ripro = $sr->tooltip_RIPRO_by_k_ordine($row['TDDOCU']);
					        $s_ar[$liv]["tooltip_ripro"] = "<B>RIPROGRAMMAZIONI:</B><br>" . implode("<br>", $ar_tooltip_ripro);
					    }
					    
					    //icona: ha deroghe?
					    if ($sr->ha_deroghe_by_k_ordine($row['TDDOCU'])){
					         $img_entry_name = "icone/48x48/unlock.png";
					        $ar_deroghe = array();
					        $array_deroghe = $sr->array_deroghe_by_k_ordine($row['TDDOCU']);
					        foreach ($array_deroghe as $r_deroga){
					            $ar_deroghe[] = "> [" . trim($r_deroga['ADUSRI']) . "] " . print_date($r_deroga['ADDTDE']) . " - " . trim($main_module->decod_std('AUDE', $r_deroga['ADAUTOR'])) . ": " .  acs_u8e(trim($r_deroga['ADCOMM']));
					        }
					        $tooltip_ha_deroghe = implode("<br>", $ar_deroghe);
					        
					        $s_ar[$liv]['task'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img width=16 class="cell-img" src=' . img_path($img_entry_name) . '></span>';
					        $s_ar[$liv]["tooltip_ripro"] .= "<br><br><B>DEROGHE:</B><br>" . $tooltip_ha_deroghe;
					    }
					    
					    
					    if($row['TDFN06'] != 0){
					        $img_entry_name = "icone/16x16/puntina_rossa.png";
					        $s_ar[$liv]['task'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '></span>';
					    }
					    
					    if (strlen(trim($row['MEMO'])) > 0)
					        $s_ar[$liv]['task'] .= "<br><span style='margin-left:25%;'><b>{$row['MEMO']}</b></span>";
					    
					}
			
					if ($r_livs['liv0_v'] == 'REFOR'){
						$new_rif = '<b>' . trim(acs_u8e($row['TDDCON'])) . '</b>';
						if (strlen(trim($s_ar[$liv]["riferimento"])) > 0)
							$new_rif .= "<br/>" . $s_ar[$liv]["riferimento"];
						$s_ar[$liv]["riferimento"] = $new_rif;  
					}	
					
						
					
					$s_ar[$liv]['data_emiss_OF'] = $row['TDDEOA'];
					
					if ($r_livs['liv0_v'] == 'SBLOC'){
						
						$as = new SpedAssegnazioneOrdini;
						$ret_stato_tooltip = $as->stato_tooltip_entry_per_ordine($row['TDDOCU'], null, 'N', 'SB', 'Y');
						
						$s_ar[$liv]["tooltip_seq_carico"] = $ret_stato_tooltip['tooltip'];					
						$s_ar[$liv]["seq_carico"] = $ret_stato_tooltip['att_distinct'];
					}
					
			
					$s_ar = &$s_ar[$liv][$n_children];						
						
				} //per ogni record
			
			
				return array("det"=>$ar, "tot"=>$ar_tot);
			}
				
			
			
			

	function el_arrivi_liv($ar, $row, $n_liv, $m_params = array()){
				global $auth, $main_module;
				
				$m_params = (object)$m_params;
				
				//espando automaticamente i sottolivelli dallo stato ordine in giu'
				if ($n_liv >= 4){
					$ar['expanded'] = false;
				}
			
				//DAL CLIENTE IN GIU'
				if ($n_liv >= 2){
					
					//gestione fl_new (riporto da liv cliente in giu', solo se l'utente corrente e' il destin del messaggio)
					if (trim($row['ASFVIU']) != 'Y' && $auth->get_user() == trim($row['ASUSAT'])) $ar["fl_new"]	= "Y";
					
				}
		
				if ($n_liv == 5){ //ordine
			
					//tipo priorita'
					$ar["tp_pri"] = trim($main_module->get_tp_pri(trim($row['TDOPRI']), $row));
						
					$ar["fl_da_prog"] = $row['TDFN04'];
					
					//icona ordine abbinato
			
					//ordine bloccato autorizzato (divieto green)
					if ($row["TDBLOC"] == 'A')
						$ar['task'] .= '&nbsp;<img class="cell-img" style="margin-top: 2px;" src=' . img_path("icone/16x16/divieto_green.png") . '>';
			
						
					if (trim($row['TDABBI']) > 0)
						$ar['task'] .= '&nbsp;&nbsp;&nbsp;<img class="cell-img" src=' . img_path("icone/16x16/link.png") . '>';
						
				
					$ha_commenti = $main_module->has_commento_ordine_by_k_ordine_gest($row['TDDOCU']);
					$ar['note'] =  $ha_commenti;
					/*if ($ha_commenti == FALSE)
						$img_com_name = "icone/16x16/comment_light.png";
					else
						$img_com_name = "icone/16x16/comment_edit_yellow.png";*/
				
					global $giorni_stadio;
					$ar['n_stadio'] = $giorni_stadio[$row['TDDTEP']];
						
					//icona commento ordine
					//$ar['task'] .= '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine(\'' . $main_module->k_ordine_td($row) . '\', \'' . $r_livs['liv1_v'] . '\', \'' . $r_livs['liv2_v'] . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
					
					$ar["riferimento"] 	=  trim($row['TDVSRF']);
					$ar["ASCAAS"] 	=  trim($row['ASCAAS']);
					
					//Aggiunto le note di apertura entry
					if (strlen(trim($row['ASNOTE'])) > 0){
						$row['ASNOTE'] = trim($row['ASNOTE']);
						$row['ASUSAS'] = trim($row['ASUSAS']);
						if (strlen(trim($ar["riferimento"])) > 0)
							$ar["riferimento"] .= "<br>";
					    $ar["riferimento"] 	.= "<b>{$row['ASNOTE']}</b>";
						//$ar["riferimento"] 	.= "<b>[{$row['ASUSAS']}: {$row['ASNOTE']}]</b>";
					}
					
					
					$ar["riferimento"] = acs_u8e($ar["riferimento"]);
					
					$ar["nr"] 			=  $row['TDONDO'];
					
					
					$ar["k_ordine"]		=  $row['TDDOCU'];
					$ar["cod_iti"]		=  $row['CSCITI'];
			
					$ar["tipo"] 		=  $row['TDOTPD'];
					$ar["qtip_tipo"] 	=  acs_u8e($row['TDDOTD']);
					
					$ar["raggr"] 		=  $row['TDCLOR'];
					$ar["priorita"]		=  $row['TDOPRI'];

					$ar["qtip_priorita"]=  acs_u8e($row['TDDPRI']);
					$ar["tp_pri"]		=  $main_module->get_tp_pri(trim($row['TDOPRI']), $row);
					$ar["data_reg"]		=  $row['TDODRE'];
					$ar["cons_rich"]	=  $row['TDODER'];
					$ar["cons_prog"]	=  $row['TDDTEP'];
					$ar["data_disp"] 	=  max($ar["data_disp"], $row['TDDTDS']);
					
					$ar["cliente"] 	= $row['TDDCON'];   //cliente come colonna in IMMOR
					
					$ar["data_assegnazione"]	=  $row['ASDTAS'];
					$ar["ora_assegnazione"]		=  $row['ASHMAS'];
					$ar['f_ril'] =  trim($row['ASFLNR']);
					$ar["cod_pagamento"]	=  trim($row['TDCPAG']);
					if (strlen(trim($row['TDCPAG'] . $row['TDDPAG'])) > 0 )
						$ar["des_pagamento"]	=  "[" . trim($row['TDCPAG']) . "] " . acs_u8e(trim($row['TDDPAG']));
					$ar["preferenza"]		=  trim($row['TDFU03']);
					$ar["preferenza_anag"]	=  trim($row['TDFU04']);

					//se scarico intermedio o sosta tecnica
					if ($row['TDTDES'] == '1' || $row['TDTDES'] == '2' || $row['TDTDES'] == 'T'){
						$ar["scarico_intermedio"] = 1;
					}
						
					
			
					//Segnalazione su evasione programmata
					if ($row['TDDTDS'] > $row['TDDTEP'] && $row['TDDTEP'] > 0)
						$ar["fl_data_disp"] = 1; //segnalo di rosa
						
			
					$ar["TDFN11"]		=  $row['TDFN11']; //evaso
			
					$ar["cons_conf"]	=  $row['TDDTVA'];
			
					$ar["fl_cons_conf"]	=  $row['TDFN06'];
						
					$ar["stato"]		=  $row['TDSTAT'];
					$ar["qtip_stato"]	=  acs_u8e($row['TDDSST']);
						
					if (trim($row['TDFG01']) == 'A' || trim($row['TDFG01']) == 'R') //indice modifica (icona + su new)
						$ar["ind_modif"] = trim($row['TDFG01']);
						
			
						
					if ($ar["fl_new"] != 'Y' && to_AS_date_time($row['TDDTAR'], $row['TDHMAR']) > now_AS_date_time() ){ //attesa maturazione confermata
						$ar["fl_new"]	=  3; //timer, dando prima precedenza a icona new
					}
			
			
						
					//icona a livello ordine
					//DRY: anche in acs_json_analisi_esposizione
					$ar["iconCls"]	=  'icon-blog_compose-16'; //non confermata (default)
					
					if ($row['TDFN13'] == 1){
						$ar["iconCls"]	=  'icon-design-16'; //icona design
					}
					if ((int)$row['TDDTCF'] > 0){
						$ar["iconCls"]	=  'icon-blog_accept-16'; //confermata
					}
					if ((int)$row['TDNRCA'] > 0){
						$ar["iconCls"]	=  'icon-button_black_play-16'; //con carico
					}
					
					if (trim($row['TDOWEB']) == 'C'){
					    $ar["iconCls"]	=  'icon-certificate-16'; //icona certificate
					}
						
			
				}
			
				$ar["volume"] 		+= $row['TDVOLU'];
				$ar["peso"] 		+= $row['TDPLOR'];
				$ar["pallet"]		+= $row['TDBANC'];
				
				if (trim($row['ASCAAS']) != 'SBLOC'){
					$ar["importo"]		+= $row['TDTIMP'];
				} else { //in sbloc prendo il tot documento
					$ar["importo"]		+= $row['TDTOTD'];
				} 
			
			
				
				if($m_params->totalizza == 'I'){
				   
				    switch (trim($row['TDCLOR'])){
				    case 'O':
				        $ar['n_O'] += $row["TDTOTD"];
				        break;
				    case 'P':
				        $ar['n_P'] += $row['TDTOTD'];
				        break;
				    case 'M':
				        $ar['n_M'] += $row['TDTOTD'];
				        break;
				    case 'A':
				        $ar['n_A'] += $row['TDTOTD'];
				        break;
				    default:
				     
				        if (trim($row['TDCLOR']) == 'C' || trim($row['TDCLOR']) == 'V'){
				            $ar['n_CV'] += $row['TDTOTD'];
				       }
			         }
			   }else{
				   
				    switch (trim($row['TDCLOR'])){
				        case 'O':
				            $ar['n_O'] ++;
				            break;
				        case 'P':
				            $ar['n_P']++;
				            break;
				        case 'M':
				            $ar['n_M']++;
				            break;
				        case 'A':
				            $ar['n_A']++;
				            break;
				        default:
				            //$ar['n_CV']++;
				            //$ar['n_' . trim($row['TDCLOR'])]++;
				            if (trim($row['TDCLOR']) == 'C' || trim($row['TDCLOR']) == 'V'){
				                $ar['n_CV']++;
				            }
				    }
				}
			
				$ar["colli"] 		+= $row['TDTOCO'];
				$ar["colli_disp"] 	+= $row['TDCOPR'];
				$ar["colli_sped"] 	+= $row['TDCOSP'];
			
			
				$ar["fl_bloc"] = $main_module->get_fl_bloc($ar["fl_bloc"], $row['fl_bloc_ordine']);
					
				//data scadenza
				if (trim($row['ASCAAS']) != 'SBLOC'){
					if ((int)$ar["data_scadenza"] > 0)
						$ar["data_scadenza"] =  min($ar["data_scadenza"], $row['ASDTSC']);
					else
						$ar["data_scadenza"] = $row['ASDTSC'];
				} else {
			
					//data sblocco (in data_scadenza)
					if ($n_liv >= 2){
						if (trim($row['ASCAAS']) == 'SBLOC'){
							// per sbloc come data_scadenza considero le date di blocco (commerciale/amministrativo)
								
							if ( ($row['fl_bloc_ordine'] == 4 || $row['fl_bloc_ordine'] == 3 || $row['fl_bloc_ordine'] == 2 ) && (strlen($ar["data_scadenza"]) == 0 || (int)$ar["data_scadenza"] == 0))
								$ar["data_scadenza"] = 99999999; //cosi non ho problemi con il min e 0
			
							if ($row['fl_bloc_ordine'] == 4){ //commerciale e amministrativo
								$ar["data_scadenza"] 	=  min($ar["data_scadenza"], $row['TDDTBLO']);
								$ar["data_scadenza"] 	=  min($ar["data_scadenza"], $row['TDDTBLC']);
							}
							if ($row['fl_bloc_ordine'] == 3){ //amministrativo
								$ar["data_scadenza"] 	=  min($ar["data_scadenza"], $row['TDDTBLO']);
							}
							if ($row['fl_bloc_ordine'] == 2){ //commerciale
								$ar["data_scadenza"] 	=  min($ar["data_scadenza"], $row['TDDTBLC']);
							}
			
							//si il blocco e' stato messo in giornata lo mostro
							if ($ar['fl_new'] != 'Y'){
								if ($ar['data_scadenza'] == oggi_AS_date())
									$ar['fl_new'] = 'O';
							}
								
						}
					}
			
			
				}
					
			
				$ar['fl_art_manc'] = max($ar['fl_art_manc'], $main_module->get_fl_art_manc($row));
					
				if ($row['TDFMTS'] == 'Y')
				{
					$ar["art_da_prog"] 	= 1;
				}
			
			
				return $ar;
			}
			
				
		
			

	function crea_alberatura_el_ordini_json_arrivi($ar, $node, $cod_itin, $titolo_panel, $tipo_elenco = "", $ordina_per_tipo_ord = "N"){
		global $main_module;
				
				if ($node != "root"){
						
					$node_value = explode("|", $node);
						
					$ar = $ar["det"]["{$node_value[0]}"]["children"];
						
					for ($i=1; $i<count($node_value); $i++)
						$ar = $ar["{$node_value[$i]}"]["children"];
							
									
								//$ar = $ar["det"]["{$node_value[0]}"]["children"]["{$node_value[1]}"]["children"]["{$node_value[2]}"]["children"];
				} else {
					$ar = $ar["det"];
				}
			
				$ret = array();
			
				foreach($ar as $kar => $r){
						
					/*
						//se sono in un sotto livello.... ordino per TASITI
						if ($node != "root" && $node_value[0] != 'SBLOC'){
						usort($ar[$kar]['children'], "cmp_by_TASITI");
						}
						*/
						
					//$ret[] = $ar[$kar];
					$ret[] = array_values_recursive($ar[$kar]);
				}
			
			
				$ar_ret = array();
				$ar_ret['children'] = $ret;
			
				return acs_je($ar_ret);
			
			}
			
			

	function imposta_livs($row, $m_params = array()){
		global $main_module, $cfg_mod_Spedizioni;
				
		    $m_params = (object)$m_params;
		
			
			$r['liv0_v'] = trim($row['ASCAAS']); //TIPO OPERAZIONE
			
			if (strlen($r['liv0_v']) == 0)
				$r['liv0_v'] = trim($row['ASCAAS_FROM_TD']);
			
			switch($r['liv0_v']){
				case 'SBLOC':
					
					//Salto il livello		
					$r['liv1_v'] = 'NO-LIV';
					//$r['liv1_v'] = trim($row['ASUSAT']);	//UTENTE ASSEGNATO
					//$r['liv1_d'] = trim($row['ASUSAT']);
						
					$r['liv2_v'] = trim($row['TDCCON']) ;	//CLIENTE
					$r['liv2_d'] = acs_u8e($row['TDDCON']);
					
					//Salto il livello in base a config
					if ($cfg_mod_Spedizioni["overflow_SBLOC_per_area"] != 'N'){
					    
					    $r['liv3_v'] = trim($row['TAASPE']); //AREA SPEDIZIONE
					    $r['liv3_d'] = $main_module->decod_std('ASPE', $r['liv3_v']);
					    //passare i parametri nella funzione!!!!!
					    
					    if($m_params->raggruppa == 'D'){
					        $r['liv3_v'] = trim($row['TDCDIV']); //DIVISIONE
					        $r['liv3_d'] = acs_u8e($row['TDDDIV']);
					    }
					} else {
						$r['liv3_v'] = 'NO-LIV';
					}					
					
					//Salto il livello					
					$r['liv4_v'] = 'NO-LIV';	//STATO ORDINE -> RAGGRUPPO TUTTO
						
					//$liv4_v = trim($row['TDSTAT']);	//STATO ORDINE -> RAGGRUPPO TUTTO
					// $liv4_d = "[" . $row['TDSTAT'] . "] ". acs_u8e($row['TDDSST']);
						
					$r['liv5_v'] = implode("|", array(trim($row['TDDOCU']), $row['ASIDPR']));	//ORDINE e PROGRESSIVO SEGNALAZIONE
					$r['liv5_d'] = $main_module->k_ordine_out($row['TDDOCU']) . " " . $row['TDMODI'];					
				break;
				
				case 'IMMOR': //immissione righe ordine
					$r['liv1_v'] = trim($row['ASUSAT']);	//UTENTE ASSEGNATO
					$r['liv1_d'] = trim($row['UTDESC']) . " [".trim($row['ASUSAT'])."]";

					$r['liv2_v'] = trim($row['TDSTAT']);	//STATO ORDINE
					$r['liv2_d'] = "[" . trim($row['TDSTAT']) . "] ". acs_u8e(trim($row['TDDSST']));
					
					
					if (isset($cfg_mod_Spedizioni["IMMOR_mostra_liv_cliente"]) && $cfg_mod_Spedizioni["IMMOR_mostra_liv_cliente"] == "Y"){
					    $r['liv3_v'] = trim($row['TDCCON']) ;	//CLIENTE
					    $r['liv3_d'] = acs_u8e($row['TDDCON']);					    
					} else {
					    //Salto il livello
					    $r['liv3_v'] = 'NO-LIV';
					}
					    
					
						
					//Salto il livello
					$r['liv4_v'] = 'NO-LIV';
				
					$r['liv5_v'] = implode("|", array(trim($row['TDDOCU']), $row['ASIDPR']));	//ORDINE e PROGRESSIVO SEGNALAZIONE
					$r['liv5_d'] = $main_module->k_ordine_out($row['TDDOCU']) . " " . $row['TDMODI'];
					break;				
				
				case 'REFOR': //assegna referente
				    $r['liv1_v'] = trim($row['ASUSAT']);	//UTENTE ASSEGNATO
				    $r['liv1_d'] = trim($row['UTDESC']) . " [".trim($row['ASUSAT'])."]";
				
					$r['liv2_v'] = trim($row['TAASPE']); //AREA SPEDIZIONE
					$r['liv2_d'] = $main_module->decod_std('ASPE', $r['liv2_v']);
						
					//$r['liv3_v'] = trim($row['TDCITI']) ;	//Itinerario
					//$r['liv3_d'] = $main_module->decod_std('ITIN', $r['liv3_v']);
					$r['liv3_v'] = trim($row['TDCOCL']);	//Referente cliente
					$r['liv3_d'] = trim($row['TDDOCL']);					
				
					//Salto il livello
					$r['liv4_v'] = 'NO-LIV';
				
					$r['liv5_v'] = implode("|", array(trim($row['TDDOCU']), $row['ASIDPR']));	//ORDINE e PROGRESSIVO SEGNALAZIONE
					$r['liv5_d'] = $main_module->k_ordine_out($row['TDDOCU']) . " " . $row['TDMODI'];
					break;
					
				case 'EXPO1': 
				    $r['liv1_v'] = 'NO-LIV';
				    $r['liv2_v'] = 'NO-LIV';
				    $r['liv3_v'] = trim($row['TDCCON']) ;	//CLIENTE
				    $r['liv3_d'] = acs_u8e($row['TDDCON']);		
				    
				    //Salto il livello
				    $r['liv4_v'] = 'NO-LIV';
				    
				    $r['liv5_v'] = implode("|", array(trim($row['TDDOCU']), $row['ASIDPR']));	//ORDINE e PROGRESSIVO SEGNALAZIONE
				    $r['liv5_d'] = $main_module->k_ordine_out($row['TDDOCU']) . " " . $row['TDMODI'];
				    break;
				    
				case 'EXPO2':
				    $r['liv1_v'] = trim($row['ASUSAT']);	//UTENTE ASSEGNATO
				    $r['liv1_d'] = trim($row['ASUSAT']);
				    $r['liv2_v'] = 'NO-LIV';
				    $r['liv3_v'] = implode("|", array(trim($row['TDCCON']), trim($row['TDCDES']))) ;	//CLIENTE|destinazione
				    $r['liv3_d'] = acs_u8e($row['TDDCON']);
				    //Salto il livello
				    $r['liv4_v'] = 'NO-LIV';
				    $r['liv5_v'] = implode("|", array(trim($row['TDDOCU']), $row['ASIDPR']));	//ORDINE e PROGRESSIVO SEGNALAZIONE
				    $r['liv5_d'] = $main_module->k_ordine_out($row['TDDOCU']) . " " . $row['TDMODI'];
				    break;
					
				 default:
						$r['liv1_v'] = trim($row['ASUSAT']);	//UTENTE ASSEGNATO
						$r['liv1_d'] = trim($row['ASUSAT']);
					
						$r['liv2_v'] = trim($row['TDSTAT']);	//STATO ORDINE
						$r['liv2_d'] = "[" . trim($row['TDSTAT']) . "] ". acs_u8e(trim($row['TDDSST']));
							
						$r['liv3_v'] = trim($row['TDCCON']) ;	//CLIENTE
						$r['liv3_d'] = acs_u8e($row['TDDCON']);
					
						//Salto il livello
						$r['liv4_v'] = 'NO-LIV';
					
						$r['liv5_v'] = implode("|", array(trim($row['TDDOCU']), $row['ASIDPR']));	//ORDINE e PROGRESSIVO SEGNALAZIONE
						$r['liv5_d'] = $main_module->k_ordine_out($row['TDDOCU']) . " " . $row['TDMODI'];
						break;					
					
			}
		
	return $r;		
}			


function imposta_filtri($livs, $m_params = null){
	global $main_module;

		$filtro['ASCAAS'] = $livs[0];	
	
		switch($filtro['ASCAAS']){
			case 'SBLOC':		
				//if (count($livs) > 1) $filtro['ASUSAT'] = $livs[1];
				if (count($livs) > 2) $filtro['TDCCON'] = $livs[2];
				if (count($livs) > 3){ 
				    if($m_params->raggruppa == 'D'){
			             $filtro['TAASPE'] = $livs[3];
				    }else{
			            $filtro['TDCDIV'] = $livs[3];
			        }
				    }
				
				break;
			
			case 'IMMOR':			
				if (count($livs) > 1) $filtro['ASUSAT'] = $livs[1];
				if (count($livs) > 2) $filtro['TDSTAT'] = $livs[2];
				//if (count($livs) > 3) $filtro['TDCCON'] = $livs[3];
				break;			
			
			case 'REFOR':
				if (count($livs) > 1) $filtro['ASUSAT'] = $livs[1];
				if (count($livs) > 2) $filtro['TAASPE'] = $livs[2];
				//if (count($livs) > 3) $filtro['TDCITI'] = $livs[3];
				if (count($livs) > 3) $filtro['TDCOCL'] = $livs[3];				
				break;
			default:
				if (count($livs) > 1) $filtro['ASUSAT'] = $livs[1];
				if (count($livs) > 2) $filtro['TDSTAT'] = $livs[2];
				if (count($livs) > 3) $filtro['TDCCON'] = $livs[3];
				break;
				
				
		}	
	
	
	return $filtro;
}
		

function get_order_by_by_livs($livs){
	global $main_module;

	$filtro['ASCAAS'] = $livs[0];

	switch($filtro['ASCAAS']){
		case 'SBLOC':
			return 'TA_ATTAV.TAPESO, ASCAAS, ASUSAT, TDDCON, TDCCON, TDDTEP, TDDTDS, TA_ITIN.TAASPE, TDSTAT, TDONDO';	
		case 'IMMOR':
			return 'TA_ATTAV.TAPESO, ASCAAS, UTDESC, SEQST, TDDCON, TDCCON, TDDTEP, TDDTDS, TA_ITIN.TAASPE, TDSTAT, TDONDO';				
		case 'REFOR':
			//return 'ASCAAS, ASUSAT, TA_ITIN.TAASPE, TA_ITIN.TASITI, TDDCON, TDCCON, TDDTEP, TDDTDS, TDSTAT, TDONDO';
			return 'TA_ATTAV.TAPESO, ASCAAS, UTDESC, TA_ITIN.TAASPE, TDCOCL, TDDCON, TDCCON, TDDTEP, TDDTDS, TDSTAT, TDONDO';
		case 'EXPO1':
		    return 'TDDCON, TDCCON, TDDTEP, TDONDO';	
		case 'EXPO2':
		    return 'TDDCON, TDCCON, ASDTSC, TDDTEP, TDODRE';
		default:
			return 'TA_ATTAV.TAPESO, ASCAAS, ASUSAT, SEQST, TDDCON, TDCCON, TDDTEP, TDDTDS, TA_ITIN.TAASPE, TDSTAT, TDONDO';	
	}


	return $filtro;
}


		

function date_from_as($num)
{
 return substr($num, 6, 2) . "/" . substr($num, 4,2) . "/" . substr($num, 2, 2);	
}

function sql_where_params($filtro, $m_params){
    global $cfg_mod_Spedizioni;
        
    $where = '';
    
    //per lo sbloc parto da TD0 in outer join con AS0
    if (trim($m_params->tipo_op) == 'SBLOC' || (isset($filtro["ASCAAS"]) && trim($filtro["ASCAAS"]) == 'SBLOC')){
        $where .= ' AND (TD.TDFN02=1 OR TD.TDFN03=1) ';
        $is_SBLOC = 'Y';
    } else {
        $is_SBLOC = 'N';
        $where .= " AND ASFLRI <> 'Y' "; //flag rilascio
        //normale
                
        if(strlen($m_params->tipo_classe) > 0)
            $where .= " AND TA_ATTAV.TARIF1 = " . sql_t($m_params->tipo_classe);
        
        if (strlen(trim($m_params->tipo_op)) > 0){
            
            if(strlen($m_params->tipo_classe) == 0)
                $where .= " AND ASCAAS = " . sql_t($m_params->tipo_op);
            
        } else 	if (isset($filtro["ASCAAS"])) {
            $where .= " AND ASCAAS='{$filtro["ASCAAS"]}'";
        } else {
            global $todo_data_config;
            $where .= " AND ASCAAS  NOT IN(" . sql_t_IN($todo_data_config['escludi_da_todo']) . ")"; //Lo SBLOC viene solo gestito da panel-espo
        }
        
    }
 
    $where .= sql_where_by_combo_value('CAL.CSFG01',     $m_params->f_stadio);
    $where .= sql_where_by_combo_value('TD.TDCITI',      $m_params->f_itinerario);
    $where .= sql_where_by_combo_value('SP.CSCVET',      $m_params->f_vettore);
    $where .= sql_where_by_combo_value('TDCPAG',         $m_params->f_pagamento);
    $where .= sql_where_by_combo_value('TDCLOR',         $m_params->f_tipologia_ordine);
    $where .= sql_where_by_combo_value('TDOTPD',         $m_params->f_tipo_ordine);
    $where .= sql_where_by_combo_value('TDCAUT',         $m_params->f_causale_trasporto);
    $where .= sql_where_by_combo_value('TD.TDCAG1',      $m_params->f_agente);
    $where .= sql_where_by_combo_value('ASUSAT',         $m_params->f_utente_assegnato);
    $where .= sql_where_by_combo_value('TD.TDCOCL',      $m_params->f_referente_cliente);
    $where .= sql_where_by_combo_value('TA_ITIN.TAASPE', $m_params->f_area);
    $where .= sql_where_by_combo_value('TD.TDCDIV', $m_params->f_divisione);
    $where .= sql_where_by_combo_value('TD.TDRISC',  $m_params->f_rischio);
    $where .= sql_where_by_combo_value('TD.TDSTAT', $m_params->f_stato_ordine);
    
    if (isset($m_params->f_cliente_cod) && strlen($m_params->f_cliente_cod) > 0)
        if ($cfg_mod_Spedizioni['disabilita_sprintf_codice_cliente'] == 'Y')
            $where.= sql_where_by_combo_value('TD.TDCCON',  $m_params->f_cliente_cod);
        else
            $where.= sql_where_by_combo_value('TD.TDCCON',  sprintf("%09s", $m_params->f_cliente_cod));
                
    if (strlen($m_params->f_data_prod) > 0)
        $where .= " AND TD.TDDTEP = " . $m_params->f_data_prod;
    if (strlen($m_params->f_data_prod_da) > 0)
        $where .= " AND TD.TDDTEP >= " . $m_params->f_data_prod_da;
    if (strlen($m_params->f_data_prod_a) > 0)
        $where .= " AND TD.TDDTEP <= " . $m_params->f_data_prod_a;
                            
    //data registrazione
    if (strlen($m_params->f_data_reg_da) > 0)
        $where .= " AND TD.TDODRE >= " . $m_params->f_data_reg_da;
    if (strlen($m_params->f_data_reg_a) > 0)
        $where .= " AND TD.TDODRE <= " . $m_params->f_data_reg_a;
                                    
    if (strlen($m_params->f_data_emiss_OF_da) > 0)
        $where .= " AND TD.TDDEOA >= " . $m_params->f_data_emiss_OF_da;
    if (strlen($m_params->f_data_emiss_OF_a) > 0)
        $where .= " AND TD.TDDEOA <= " . $m_params->f_data_emiss_OF_a;
                                            
    //data/ora assegnazione
    if (strlen($m_params->f_assegnazione_da_data) > 0)
        $where .= " AND ASDTAS >= " . $m_params->f_assegnazione_da_data;
    if (strlen($m_params->f_assegnazione_da_ora) > 0)
        $where .= " AND ASHMAS >= " . $m_params->f_assegnazione_da_ora;
    if (strlen($m_params->f_assegnazione_a_data) > 0)
        $where .= " AND ASDTAS <= " . $m_params->f_assegnazione_a_data;
                                                        
    //data/ora scadenza
    if (strlen($m_params->f_scadenza_da_data) > 0)
        $where .= " AND ASDTSC >= " . $m_params->f_scadenza_da_data;
    if (strlen($m_params->f_scadenza_a_data) > 0)
        $where .= " AND ASDTSC <= " . $m_params->f_scadenza_a_data;
                                                                
    if (isset($m_params->f_num_ordine)  && strlen($m_params->f_num_ordine) > 0 )
        $where .= " AND TDONDO='{$m_params->f_num_ordine}'";
                                                                    
    if (isset($m_params->f_riferimento) && strlen($m_params->f_riferimento) > 0)
        $where .= " AND UPPER(TDVSRF) LIKE '%" . strtoupper($m_params->f_riferimento) . "%'";
                                                                        
   //------------------------------------------------------------------- filtri applicati da form
                                                                       
    if (isset($filtro["TAASPE"]))
        $where .= " AND TA_ITIN.TAASPE='{$filtro["TAASPE"]}'";
    if (isset($filtro["ASUSAT"]))
        $where .= " AND ASUSAT='{$filtro["ASUSAT"]}'";
    if (isset($filtro["TDCCON"]))
        $where .= " AND TDCCON='{$filtro["TDCCON"]}'";
    if (isset($filtro["TDCDES"]))
        $where .= " AND TDCDES='{$filtro["TDCDES"]}'";
    if (isset($filtro["TDCDIV"]))
        $where .= " AND TDCDIV='{$filtro["TDCDIV"]}'";
    
     return $where;
    
}