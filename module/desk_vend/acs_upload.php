<?php
require_once "../../config.inc.php";

$main_module = new Spedizioni();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// DELETE FILE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete_file'){
    if ($m_params->from_visualizza_order_img == 'Y'){
        if ($cfg_mod_Spedizioni["visualizza_order_img"] == "FROM_PC"){
            
            $data = array (
                'function' => 'exe_delete_file',
                'from_visualizza_order_img' => 'Y',
                'file' => $m_params->file_path
                );
            $data = http_build_query($data);
            
            $context_options = array (
                'http' => array (
                    'method' => 'POST',
                    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                    . "Content-Length: " . strlen($data) . "\r\n",
                    'content' => $data
                )
            );
            
            $context = stream_context_create($context_options);
            $fp = file_get_contents($cfg_mod_Spedizioni["url_img_FROM_PC"], false, $context);
            $ret = json_decode($fp);
            echo acs_je($ret);
            return;
        }
    }
    exit;
}




// ******************************************************************************************
// UPLOAD CONTENT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_upload_content'){
    $ret = array();
    //upload nella dir standard ordini (in base a parametro visualizza_order_img)
    if ($m_params->from_visualizza_order_img == 'Y'){
        if ($cfg_mod_Spedizioni["visualizza_order_img"] == "FROM_PC"){
            $data = array (
                'function' => 'exe_upload',
                'k_ordine' => $m_params->k_ordine,
                'file_name' => $m_params->file_name,
                'content_file' => $m_params->content);
            $data = http_build_query($data);
            
            $context_options = array (
                'http' => array (
                    'method' => 'POST',
                    'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                    . "Content-Length: " . strlen($data) . "\r\n",
                    'content' => $data
                )
            );
            
            $context = stream_context_create($context_options);
            $fp = file_get_contents($cfg_mod_Spedizioni["url_img_FROM_PC"], false, $context);
            $ret = json_decode($fp);
            echo acs_je($ret);
            return;
        }
    }    
    exit;
}



// ******************************************************************************************
// UPLOAD FILE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_upload_file'){
    
    $ret = array();
	
	if ($_REQUEST['save_in_tmp'] == 'Y'){
		$tmpfname = tempnam("/tmp", "email_att_");
		move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $tmpfname);
		$ret['success'] = true;
		$ret['tmp_path'] = $tmpfname;
		$ret['upload_file']	= $_FILES['file_to_upload'];
		$ret['new_name']	= $_REQUEST['f_name'];
		echo acs_je($ret);
		return;
	}
	
	//upload nella dir standard ordini (in base a parametro visualizza_order_img)
	if ($_REQUEST['from_visualizza_order_img'] == 'Y'){
	    if ($cfg_mod_Spedizioni["visualizza_order_img"] == "FROM_PC"){
	        
	        $data = array (
	               'function' => 'exe_upload', 
	               'k_ordine' => $_REQUEST['k_ordine'], 
	               'file_name' => $_REQUEST['f_name'],
	               'content_file' => $fileContent = file_get_contents($_FILES['file_to_upload']['tmp_name']));
	        $data = http_build_query($data);
	        
	        $context_options = array (
	            'http' => array (
	                'method' => 'POST',
	                'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
	                . "Content-Length: " . strlen($data) . "\r\n",
	                'content' => $data
	            )
	        );
	        
	        $context = stream_context_create($context_options);
	        $fp = file_get_contents($cfg_mod_Spedizioni["url_img_FROM_PC"], false, $context);
	        $ret = json_decode($fp);
	        echo acs_je($ret);
	        return;
	    }	    
	}
	
	
	//$img_dir = "/SV2/ORD_FILE/";
	$img_dir = $cfg_mod_Spedizioni['allegati_root_C'];
	
	if (!is_dir($img_dir))
	    mkdir($img_dir);
	
	$ordine = $_REQUEST['k_ordine'];
	$ar_ordine = explode('_', $ordine);
	$inum_ordine = $ar_ordine[2];
	$nr_ordine = $ar_ordine[4];
	
	$dir_ordine = $inum_ordine. "/" .substr($nr_ordine, 0 , 2) . "/" .substr($nr_ordine, 2, 2). "/" .trim($nr_ordine) ;
	
	$ar_dir = explode('/', $dir_ordine);
	
	$dir = $img_dir;
	foreach($ar_dir as $comp){
	    
	    $dir .= $comp . "/";
	 
	    if (!is_dir($dir))
	    mkdir($dir);
	    
	}
	

	$file_src = $dir . $_REQUEST['f_name'];
	
	move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $file_src);
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;	
}

// ******************************************************************************************
// OPEN FORM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
    
    
	?>
 
 {"success":true, "items": [
        {
				xtype: 'panel',
				autoScroll : true,
				layout: {type: 'vbox', 
				border: false, pack: 'start', align: 'stretch'},
				
												
				items: [
				{
           
			xtype: 'form',
                        border: false,
                        bodyStyle: {
                            padding: '10px'
                        },
                        items: [
						{xtype: 'hiddenfield', name: 'k_ordine', value: <?php echo j($m_params->k_ordine) ?>},                        
						{xtype: 'hiddenfield', name: 'save_in_tmp', value: <?php echo j($m_params->save_in_tmp) ?>},
						{xtype: 'hiddenfield', name: 'from_righe', value: <?php echo j($m_params->from_righe) ?>},
						{xtype: 'hiddenfield', name: 'nrec', value: <?php echo j($m_params->row->num_ord) ?>},
						{xtype: 'hiddenfield', name: 'from_visualizza_order_img', value: <?php echo j($m_params->from_visualizza_order_img) ?>},
                        {
                        	name: 'file_to_upload',
                            xtype: 'filefield',
                            labelWidth: 80,
                            fieldLabel: 'Seleziona file',
                            anchor: '100%',
                            allowBlank: false,
                            margin: 0,
                            listeners: {
                              'change': function(f, new_val){
                                    var t_filename = this.up('window').down('#filename');
                                    var ar_name = new_val.split('\\');
                                    t_filename.setValue(ar_name[2]);
                              }
            				}
							},
							{
                        	name: 'f_name',
                        	itemId : 'filename',
                            xtype: 'textfield',
                            labelWidth: 80,
                            maxLength : 50,
                            fieldLabel: 'Nome file',
                            anchor: '100%',
                            allowBlank: true,
                            margin : '5 0 0 0'
                            }
							
						],
							
							 buttons: [
							 {
                        text: 'Upload',
                        handler: function () {
                            var form = this.up('form').getForm();
                            var m_win = this.up('window');
 
                            if (!form.isValid()) return;
 
                            form.submit({
                                url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upload_file',
                                waitMsg: 'Uploading your file(s)...',
                                success: function (f, a) {
                                    var data = a.result;
                                    m_win.fireEvent('afterUpload', m_win, data);
                                },
                                failure: function (f, a) {
                                    Ext.Msg.alert('Failure', a.result.msg || a.result.message || 'server error', function () {
                                        //win.close();
                                    });
                                }
                            });
                        }
                    }, {
                        text: 'Chiudi',
                        handler: function () {                       
                            this.up('window').close();
                        }
                    }]
          
			}
				
					
				]
			}, 	
			]}


	
	<?php 
}
