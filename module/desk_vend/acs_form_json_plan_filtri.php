<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();

$get_plan_filtro_ordini = $s->get_plan_filtro_ordini();
//print_r($_SESSION[$s->get_cod_mod]);
//print_r($get_plan_filtro_ordini);


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'save_filtri'){
	$s->set_plan_filtro_ordini((array)json_decode($_REQUEST['form_ep']));
	
	$ret['success'] = true;
	echo acs_je($ret);	
	exit;
}


?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
            items: [{
		                    xtype: 'radiogroup',
		                    layout: 'hbox',
		                    fieldLabel: '',
		                    flex: 3,
		                    defaults: {
		                     width: 105,
		                     padding: '10 10 10 10'
		                    },
		                    items: [{
		                    	xtype: 'label',
		                    	text: 'Carico',
		                    	width: 70
		                    },
							{
				                boxLabel: 'Tutti',
				                name: 'f_carico_assegnato',
				                checked: <?php echo $get_plan_filtro_ordini['f_carico_assegnato'] == '' ? 'true' : 'false' ?>,			                
				                inputValue: ''
				            }, {
				                boxLabel: 'Solo con carico assegnato',
				                checked: <?php echo $get_plan_filtro_ordini['f_carico_assegnato'] == 'Y' ? 'true' : 'false' ?>,			                
				                name: 'f_carico_assegnato',
				                inputValue: 'Y',
				                width: 300
				            }
		                   ]
		                }, {
							flex: 1,
							labelWidth: 70,
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	anchor: '-15',
						   	value: <?php echo j($get_plan_filtro_ordini['f_divisione']); ?>,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,
							labelWidth: 70,
							name: 'f_mercato',
							xtype: 'combo',
							fieldLabel: 'Mercato',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	anchor: '-15',
						   	value: <?php echo j($get_plan_filtro_ordini['f_mercato']); ?>,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('MERCA'), ""); ?> 	
								    ] 
								}
								
																					 
							}
				],
			buttons: [{
	            text: 'Conferma',
	            handler: function() {
	            	var form = this.up('form').getForm();
	                form.submit({
	                				params: {form_ep: JSON.stringify(form.getValues())},
		                            success: function(form,action) {
		                         		   plan = Ext.getCmp('calendario');
		                         		   plan.store.load();
		                            },
		                            failure: function(form,action){
		                                Ext.MessageBox.alert('Erro');
		                            }
		                        });	            	

		            this.up('window').close();            	                	                
	            }
	        }],             
				
        }
]}