<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// DATI PER GRID
// raggruppo per TDDTEP/ITIN/TDNBOC in base a TDNBOF passata (sped_id)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){    
    
    $sql = "SELECT TDDTEP, TDCITI, TA_ITIN.TADESC AS TDCITI_D, TDNBOC
	           FROM {$cfg_mod_Spedizioni['file_testate']} TD
               LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
                ON TD.TDDT=TA_ITIN.TADT AND TD.TDCITI=TA_ITIN.TAKEY1 AND TA_ITIN.TATAID='ITIN'
	           WHERE ". $s->get_where_std()."
                 AND TDNBOF = {$m_params->sped_id}
               GROUP BY TDDTEP, TDCITI, TA_ITIN.TADESC, TDNBOC";    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $row['TDCITI_D'] = utf8_encode($row['TDCITI_D']);
        $ret[] = $row;        
    }
    
    echo acs_je($ret);    
    exit;
}



?>
{"success":true, "items": [
  {
   xtype: 'grid',				               
	
	store: {
			xtype: 'store',
			autoLoad:true,

    	proxy: {
    	   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
    	   extraParams: <?php echo json_encode($m_params) ?>, 
    	   method: 'POST',								
    	   type: 'ajax',
    
           actionMethods: {
              read: 'POST'
            },
    
    	   reader: {
                type: 'json',
    			method: 'POST',						            
                root: 'root'						            
        	}
        	
        	/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
			, doRequest: personalizza_extraParams_to_jsonData
    	},
		
	fields: ['TDDTEP', 'TDCITI_D', 'TDNBOC']																
	}, //store
				
	        columns: [	
				{
	                header   : 'Data<BR/>Ev. Progr.',
	                dataIndex: 'TDDTEP',
	                width: 100,
	                renderer: date_from_AS
	            },{
	                header   : 'Itinerario',
	                dataIndex: 'TDCITI_D',
	                flex: 1
	            },{
	                header   : 'Programma di produzione',
	                dataIndex: 'TDNBOC',
	                flex: 1
	            }	         
	        ], 
	        
	     listeners: {
			celldblclick: {								
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){				
				var rec = iView.getRecord(iRowEl),
				    mp = Ext.getCmp('m-panel');
				    
				mp.add(
					show_el_ordini(iView, rec, null, null, null, 'acs_get_elenco_ordini_json.php?tipo_cal=per_sped&sped_id=' + rec.get('TDNBOC'), Ext.id(), [rec.get('TDNBOC')])
				).show();    
			  }
			}
					  	     
	     } //listeners

	} //grid
 
     ]       
 }