<?php

require_once("../../config.inc.php");
require_once("acs_panel_todolist_data.php");

$main_module = new Spedizioni();
$s			 = new Spedizioni();
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// Scrittura RI per stampa documentazione utente
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_oentry'){

    foreach ($m_params->list_selected_id as $ordine){
        
        $sh = new SpedHistory();
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'IMP_RIGHE_OENTRY',
                "k_ordine"	=> $ordine->k_ordine,
                "vals" => array(
                )
            )
            );
        
    } // per ogni ordine selezionato
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}


// ******************************************************************************************
// Scrittura RI per stampa documentazione utente
// ******************************************************************************************
if ($_REQUEST['fn'] == 'write_RI_documentazione'){

	 //Recupero dei dati
	 $livs['0'] = 'IMMOR';
	 $livs['1'] = $_REQUEST['id_selected'];
	 $filtro 	= imposta_filtri($livs);
	 $order_by = get_order_by_by_livs($livs); 
	 $items 	= get_elenco_ordini_arrivi($filtro, $order_by);
	 
	 $use_session_history = microtime(true);

	 //per ogni ordine
	 while ($r = db2_fetch_assoc($items)){
			$sh = new SpedHistory();
			$sh->crea(
					'stampa_doc_ordine',
					array(
							'use_session_history' => $use_session_history,							
							"prog" 			=> $r['ASIDPR'],
							"k_ordine"		=> $r['ASDOCU']
					)
			);		 	
	 } 
	 
	 $sh->rendi_attiva_sessione($use_session_history, 'N');
	 
	 $ret['success'] = true;
	 echo acs_je($ret);
	exit;
} //get_json_data


// ******************************************************************************************
// RECUPERO seq_carico (SBLOC: e' l'elenco delle check list sull'ordine)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'SBLOC_get_seq_carico'){

	$m_params = acs_m_params_json_decode();
	$k_ordine = $m_params->k_ordine;

	$as = new SpedAssegnazioneOrdini;
	$ret_stato_tooltip = $as->stato_tooltip_entry_per_ordine($k_ordine, null, 'N', 'SB', 'Y');
						
	echo acs_je($ret_stato_tooltip);
	exit;
} //get_json_data



// ******************************************************************************************
// RECUPERO FLAG RILASCIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_richiesta_record_rilasciato'){

	$m_params = acs_m_params_json_decode();
	$prog = $m_params->prog;
	
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_assegna_ord']} WHERE ASIDPR = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($prog));
	
	$r = db2_fetch_assoc($stmt);
	$ret = array();
	$ret['success'] = true;
	$ret['ASFLRI'] = $r['ASFLRI'];
	echo acs_je($ret);
	exit;
} //get_json_data



// ******************************************************************************************
// RECUPERO TESTATA ORDINE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_ordine_by_k_ordine'){
    
    $m_params = acs_m_params_json_decode();
    $ret = array();
    
    if (strlen($m_params->k_ordine) <= 0){
        $ret['success'] = false;
        $ret['message'] = 'Chiave ordine non passata';
        echo acs_je($ret);
        exit;
    }
    
    $ord = $s->get_ordine_by_k_docu($m_params->k_ordine);
    
    if (!$ord){
        $ret['success'] = false;
        $ret['message'] = 'Ordine non presente';
        echo acs_je($ret);
        exit;
    }

    $ret['success'] = true;
    $ret['record'] = $ord;
    echo acs_je($ret);
    exit;
} //get_json_data



// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	ini_set('max_execution_time', 6000);
	$giorni_stadio = $s->get_giorni_stadio($da_data, $n_giorni);
	
	$node = $_REQUEST["node"];
	
	$m_params = acs_m_params_json_decode();
	
	$livs = explode("|", $_REQUEST["node"]);
	
	$filtro = array();
	if ($node != "root")
		$filtro = imposta_filtri($livs);
	
	
	$titolo_panel = "Elenco ";
	$tipo_elenco = "LIST";
	
	$order_by = get_order_by_by_livs($livs);
	
	$items 	= get_elenco_ordini_arrivi($filtro, $order_by);
	$appLog->add_bp('get_elenco_ordini_arrivi - END');
		
	$ars_ar = crea_array_valori_el_ordini_arrivi($items, $tipo_elenco, $node, 'N', $m_params);
	$appLog->add_bp('crea_array_elenco_ordini_arrivi - END');
	
	$ars 	= crea_alberatura_el_ordini_json_arrivi($ars_ar, $node, $filtro["itinerario"], $titolo_panel, $tipo_elenco, $_REQUEST['ordina_per_tipo_ord']);
	$appLog->add_bp('crea_alberatura_elenco_ordini_arrivi - END');
	echo $ars;
	
	$appLog->save_db();	
	exit;
} //get_json_data

// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
            items: [{
		                    xtype: 'checkboxgroup',
		                    layout: 'hbox',
		                    fieldLabel: 'Solo stadio',
		                    flex: 3,
		                    defaults: {
//		                     width: 105,
		                     padding: '0 10 10 0'
		                    },
		                    items: [{
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Ice'
				                  , inputValue: '1'
				                  , width: 80                
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Slush'
				                  , inputValue: '2'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Water'
				                  , inputValue: '3'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Altro'
				                  , inputValue: '0'
				                  , width: 80                        
				                 }
		                   ]
		                }, {
					            xtype: 'combo',
								name: 'f_cliente_cod',
								fieldLabel: 'Cliente',
								minChars: 2,			
								allowBlank: false,            
					            store: {
					            	pageSize: 1000,            	
									proxy: {
							            type: 'ajax',
							            url : 'acs_get_select_json.php?select=search_cli_anag',
							            reader: {
							                type: 'json',
							                root: 'root',
							                totalProperty: 'totalCount'
							            }
							        },       
									fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
					            },
					                        
								valueField: 'cod',                        
					            displayField: 'descr',
					            typeAhead: false,
					            hideTrigger: true,
					            anchor: '100%',
					            
					            
					            listConfig: {
					                loadingText: 'Searching...',
					                emptyText: 'Nessun cliente trovato',
					                
					                // Custom rendering template for each item
					                getInnerTpl: function() {
					                    return '<div class=\"search-item\">' +
					                        '<h3><span>{descr}</span></h3>' +
					                        '[{cod}] {out_loc}' +
					                    '</div>';
					                }                
					                
					            },
					            
					            pageSize: 1000
					        }, 
					        
					        
						  {
							flex: 1,
							name: 'f_area', anchor: '100%',
							xtype: 'combo',
							fieldLabel: 'Area spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
								}						 
							}					        
					        
							
						 , {
							flex: 1,
							name: 'f_itinerario',
							xtype: 'combo', anchor: '100%',
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_rischio', anchor: '100%',
							xtype: 'combo',
							fieldLabel: 'Rischio',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_rischio(), '') ?> 	
								    ] 
								}						 
							}, {
						     name: 'f_data_prod'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data prod/dispon'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '0'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}, {
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
						   	anchor: '0',
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
								}						 
							}
						, {
							flex: 1,						
							name: 'f_causale_trasporto',
							xtype: 'combo',
							fieldLabel: 'Causale trasporto',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
						   	anchor: '0',
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_causale_trasporto('N'), '') ?> 	
								    ] 
								}						 
							}						
						
					, {
						xtype: 'fieldset', title: 'ToDo List',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
						  {
							xtype: 'fieldcontainer',
							layout: { 	type: 'hbox', pack: 'start', align: 'stretch'},
							items: [
								{
								     name: 'f_assegnazione_da_data'                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'Assegnazione dal'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '0', width: 200, labelWidth: 100
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
									 }
								}, {
									xtype: 'timefield'
								   , fieldLabel: ''
								   , labelAlign: 'right'
								   , name: 'f_assegnazione_da_ora'
								   , format: 'H:i'						   
								   , submitFormat: 'His'
								   , allowBlank: true
								   , anchor: '0'
								   , margin: '0 0 0 15', width: 50, labelWidth: 5
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
										}
									}, {
								     name: 'f_assegnazione_a_data'                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'Al', labelAlign: 'right'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '0', width: 150, labelWidth: 50
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
									 }
								}							
							]
						  }, {
							xtype: 'fieldcontainer',
							layout: { 	type: 'hbox', pack: 'start', align: 'stretch'},
							items: [
								{
								     name: 'f_scadenza_da_data'                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'Scadenza dal'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '0', width: 200, labelWidth: 100
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
									 }
								}, {
								     name: 'f_scadenza_a_data'                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'Al', labelAlign: 'right'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '0', width: 215, labelWidth: 115
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
									 }
								}							
							]
						  }
						]
					}	
					
											
					        
					        
				],
			buttons: [{
	            text: 'Conferma',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	
	        	    arrivi = Ext.getCmp('grid-arrivi');
	        	                	
					m_p = Ext.merge({}, form.getValues());
					if (typeof(form.getValues().f_stadio) === 'undefined' && 
						typeof(form.getValues().f_cliente_cod) === 'undefined' &&
						typeof(form.getValues().f_area) === 'undefined' &&
						form.getValues().f_itinerario.length == 0 &&
						typeof(form.getValues().f_rischio) === 'undefined'
						)
					 icon_status = 'disattivo'
					else
					 icon_status = 'attivo'

					arrivi.store.proxy.extraParams = m_p;					
					arrivi.store.load();
	            	if (Ext.get("todo_fl_solo_con_carico_assegnato") != null){
	            	 //aggiorno l'icona del filtro (se ho applicato filtri o meno)
	            	  if(icon_status == 'disattivo')
	            	 	Ext.get('todo_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_disattivo.png"); ?>;
	            	  else
	            	  	Ext.get('todo_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_attivo.png"); ?>;
	            	}            	            	

		            this.up('window').close();            	                	                
	            }
	        }],             
				
        }
]}
<?php	
	exit;
} //open_form_filtri






$m_params = acs_m_params_json_decode();

if(strlen($m_params->form_open->tipo_op) == 0){
$img_name = "filtro_disattivo";
$ss .= "<span id=\"todo_fl_solo_con_carico_assegnato\" class=acs_button style=\"\">";
$ss .= "<img id=\"todo_fl_solo_con_carico_assegnato_img\" src=" . img_path("icone/48x48/{$img_name}.png") . " height=20>";
$ss .= "</span>";

 if ($auth->is_admin()) {  


$ss .= "<span id=\"attav\" class=acs_button style=\"\">";
$ss .= "&nbsp; &nbsp; &nbsp;<img id=\"attav\" src=" . img_path("icone/48x48/gear.png") . " height=20>";
$ss .= "</span>";

 }
 
}

if (in_array($m_params->form_open->tipo_op, $todo_data_config['icona_ricerca_ordine'])){
	
	$img_name = "filtro_disattivo";
	$ss .= "<span id=\"icona_ricerca_ordine\" class=acs_button style=\"\">";
	$ss .= "<img id=\"icona_ricerca_ordine\" src=" . img_path("icone/48x48/{$img_name}.png") . " height=20>";
	$ss .= "</span>";
	
}
 
 switch($m_params->form_open->tipo_op){
 	case '':
 		$tab_id    = 'grid-arrivi';
 		$tab_title = 'To Do List';
 		$win_title =  'Attivit&agrave; utente di acquisizione/avanzamento ordini';
 		break;
 	case 'IMMOR':
 		$tab_id    =  $_REQUEST['tab_id'];
 		$tab_title =  $_REQUEST['tab_title'];
 		$row_ta =   $s->get_TA_std('ATTAV', 'IMMOR');
 		$win_title = $row_ta['TADESC'];
 		break;
 	case 'REFOR':
 		$tab_id    =  $_REQUEST['tab_id'];
 		$tab_title =  $_REQUEST['tab_title'];;
 		$row_ta =  $s->get_TA_std('ATTAV', 'REFOR');
 		$win_title = $row_ta['TADESC'];
 		break;
 	case 'SBLOC':
 		$tab_id    =  $_REQUEST['tab_id'];
 		$tab_title =  $_REQUEST['tab_title'];;
 		$row_ta =  $s->get_TA_std('ATTAV', 'SBLOC');
 		$win_title = $row_ta['TADESC'];
 		break;
 	case 'EXPO1':
 	    $tab_id    =  $_REQUEST['tab_id'];
 	    $tab_title =  $_REQUEST['tab_title'];
 	    $row_ta =  $s->get_TA_std('ATTAV', 'EXPO1');
 	    $win_title = 'Verifica installazioni a carico cliente/azienda';
 	    break;
 	case 'EXPO2':
 	    $tab_id    =  $_REQUEST['tab_id'];
 	    $tab_title =  $_REQUEST['tab_title'];
 	    $row_ta =  $s->get_TA_std('ATTAV', 'EXPO2');
 	    $win_title = 'Attivit&agrave utente di programmazione installazione mostre';
 	    break;
 }

/*if (strlen($_REQUEST['tab_id']) == '0'){	
	$tab_id 		= 'grid-arrivi';
	$tab_title 	= 'To Do List';
}
else { 
	$tab_id 		= $_REQUEST['tab_id'];
	$tab_title	= $_REQUEST['tab_title'];
}*/	


//per passaggio parametri in apertura (es: da panel-espo)

$open_parameters = $m_params->form_open;

//se non ho passato paremtro (es: todolist) instanzio un oggetto vuoto
if (!(bool)$open_parameters)
	$open_parameters = (object)array();


$open_parameters->ordina_per_tipo_ord = '';




?>
{
 "success":true, "items":
 { 
  		xtype: 'treepanel',
	        title: '<?php echo $tab_title; ?>',
	        id: '<?php echo $tab_id; ?>',
	        <?php echo make_tab_closable(); ?>,
	        
	        stateful: true,
	        stateId: 'todo-tree-<?php echo trim($tab_id) ?>',
	        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],

	        tbar: new Ext.Toolbar({
	            items:["<b><?php echo $win_title; ?></b>", '->'
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	        
	        
	        cls: 'elenco_ordini arrivi',
	        collapsible: true,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    model: 'ModelOrdini',
				                                          
                    proxy: {
                        type: 'ajax',
                        timeout: 240000,
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                    extraParams: <?php echo acs_je($open_parameters); ?>
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    }
                }),
	        multiSelect: true,
	        singleExpand: false,
	
	

<?php
if ($m_params->form_open->tipo_op == 'SBLOC')
	$task_title = 'Cliente<br/>Ordini bloccati';
else
	$task_title = "Attivit&agrave;<br>Utente assegnato &nbsp;&nbsp;&nbsp;{$ss}";
?>	        
	        
 <?php  $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>"; ?>
			columns: [	
	    		{text: '<?php echo $task_title; ?>', xtype: 'treecolumn', flex: 1.2, dataIndex: 'task', sortable: false,
	        	  renderer: function (value, metaData, record, row, col, store, gridView){						
					if (record.get('liv') == 'liv_5') metaData.tdCls += ' auto-height';		
					
					if (record.get('tooltip_ripro').length > 0)
	    		 		metaData.tdAttr += 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_ripro')) + '"';
	    		 		
	    		 	var ret_value = value;
	    		 	
	    		 	//segnalo che lo stato e' cambiato
	    		 	if (record.get('rec_stato_updated') == 'Y'){
	    		 		metaData.tdCls += ' tpSfondoRosa';
	    		 		ret_value += '-> <b>[' + record.get('stato') + ']</b>';
	    		 	}
	    		 		
														
					return ret_value;
				}},
	            {
				    text: '<?php echo $note; ?>',
				    width: 32, 
				    tooltip: 'Blocco note',
				    tdCls: 'tdAction',  
				    dataIndex : 'note',       		       		        
					renderer: function(value, metaData, record){
    					if(record.get('liv') == 'liv_5'){
    						if (record.get('note') == true) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
    			    		if (record.get('note') == false) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
    			    	}
    		
			    	}
		    	},
	
				<?php
						//inclusione colonne flag ordini
						$js_include_flag_ordini = array('menuDisabled' => false, 'sortable' => true);
						require("_include_js_column_flag_ordini.php"); 
					?>,		   
	    			    	
	    			    	
	   

<?php if (in_array($m_params->form_open->tipo_op, $todo_data_config['columns_show']['cliente'])) { ?>

	          {text: 'Cliente', 	
	    	    flex: 1, 
	    	    dataIndex: 'cliente'
	    	    },

<?php }?>		    				    		
	
	    	    {
	    	    
	    	 <?php if ($m_params->form_open->tipo_op == 'EXPO1' || $m_params->form_open->tipo_op == 'EXPO2'){?>
	    	    text: 'Localit&agrave;/Installatore', 	
	    		<?php }else{?>
	    		text: 'Riferimento', 	
	     		<?php }?>	    
	    	    flex: 1, 
	    	    dataIndex: 'riferimento', 
	    	    tdCls: 'auto-height rif'

	    	    
	    	    },
	    	    
	    	    
	    	    {text: 'Tp', 			width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd', 
	        	    	renderer: function (value, metaData, record, row, col, store, gridView){						
								metaData.tdCls += ' toDoListTp ' + record.get('raggr');
								
								if (record.get('qtip_tipo') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '"';								
																			
							return value;			    
	    			}},
	    	    {text: 'Data', 			width: 60, dataIndex: 'data_reg', renderer: date_from_AS},
	    	    
<?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['stato'])) { ?>	    	    
	    	    {text: 'St', 			width: 30, dataIndex: 'stato',
	    			    renderer: function(value, metaData, record){
	    			    
							if (record.get('qtip_stato') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_stato')) + '"';	    			    
	    			    
							if (value == 'V') return '';
							if (value == 'R') return '';
							if (value == 'G') return '';
							if (value == 'B') return '';
							return value;
	    			    }	    	     
	    	    },	
<?php }?>  

<?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['priorita'])) { ?>  	    
	    	    {text: 'Pr', 			width: 35, dataIndex: 'priorita', tdCls: 'tpPri',
	    				renderer: function (value, metaData, record, row, col, store, gridView){

	    					if (record.get('qtip_priorita') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_priorita')) + '"';
	    				
	    					if (record.get('tp_pri') == 4)
	    						metaData.tdCls += ' tpSfondoRosa';
	
	    					if (record.get('tp_pri') == 6)
	    						metaData.tdCls += ' tpSfondoCelesteEl';					
	    					
	    					return value;
	    				    
	    	    			}},
<?php }?>	    		

<?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['cons_rich'])) { ?> 
				{text: 'Cons.<br>richiesta',	width: 60, dataIndex: 'cons_rich', renderer: date_from_AS},
<?php }?>
				
				
<?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['dispon'])) { ?>  
	    		{text: 'Dispon.',			width: 70, dataIndex: 'data_disp', renderer: function (value, metaData, record, row, col, store, gridView){						
					if (record.get('fl_data_disp') == '1')
						metaData.tdCls += ' tpSfondoRosa';
					 		 
						return date_from_AS(value, metaData, record);
					}
				},
<?php }?>
				{text: 'Produz./<br>Disponib.',	width: 70, dataIndex: 'cons_prog', renderer: date_from_AS},
				
<?php if (in_array($m_params->form_open->tipo_op, $todo_data_config['columns_show']['data_emiss_OF'])) { ?>
				{text: 'Emiss. OF<br>richiesta',	width: 70, dataIndex: 'data_emiss_OF',
					renderer: function (value, metaData, record, row, col, store, gridView){
	
					  if (record.get('liv') == 'liv_5'){	
	    				t = new Date();
						
						// minori di oggi -> rosa
						// uguale a oggi -> giallo
						if (parseFloat(record.get('data_emiss_OF')) != 0 &&
	    	    				 parseFloat(record.get('data_emiss_OF')) < parseFloat(Ext.Date.format(t, 'Ymd')))
	    					metaData.tdCls += ' tpSfondoRosa grassetto';    				
	    				else if (parseFloat(record.get('data_emiss_OF')) != 0 &&
	    	    				 parseFloat(record.get('data_emiss_OF')) == parseFloat(Ext.Date.format(t, 'Ymd')))
	    					metaData.tdCls += ' tpSfondoGiallo';
	    			  }        			
						
	        	   return date_from_AS(value, metaData, record);
	        								
				}				
				
				 },
<?php } ?>				
							    		
	
<?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['O'])) { ?>	
	    	    {text: 'O',					width: 35, dataIndex: 'n_O', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';						
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;
					 		 				
				}},
<?php } ?>	

<?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['P'])) { ?>
	    	    {text: 'P',					width: 35, dataIndex: 'n_P', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},						

<?php } ?>
	
<?php if (in_array($m_params->form_open->tipo_op, $todo_data_config['columns_show']['OP'])) { ?>	
	    	    {text: 'OP',					width: 35, dataIndex: 'n_O', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';						
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else {
	        	    	v = 0;
	        	    	if (!Ext.isEmpty(rec.get('n_O'))) v = v + parseInt(rec.get('n_O'));
	        	    	if (!Ext.isEmpty(rec.get('n_P'))) v = v + parseInt(rec.get('n_P'));
	        	    	if (v>0) return v;
	        	    	return null;
	        	    }
					 		 				
				}},
<?php } ?>				
               //in expo sono tutte MOSTRE quindi faccio vedere solo questa colonna ma la chiamo O
               
	 <?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['OM'])) { ?>               
		   {
	    	    <?php if ($m_params->form_open->tipo_op == 'EXPO1' || $m_params->form_open->tipo_op == 'EXPO2'){?>
	    	      text: 'O',
	    	    <?php }else{ ?>
	    	      text: 'M',
	    	    <?php }?>
	    	    	width: 35, dataIndex: 'n_M', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},
	<?php } ?>
		
		
	 <?php if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['A'])) { ?>
				
	    	    {text: 'A',					width: 35, dataIndex: 'n_A', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},	
				
	<?php }
	
if (!in_array($m_params->form_open->tipo_op, $todo_data_config['columns_hide']['CV'])) { ?>
	    	    {text: 'CV',				width: 35, dataIndex: 'n_CV', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;
				}},	    
	    			   
	  <?php }?>  			    	    	    
	    	    {text: 'Colli', hidden:true,width: 35, dataIndex: 'colli', align: 'right',  renderer: floatRenderer0},	       			    		    
	    	    {text: 'Volume',			width: 50, dataIndex: 'volume', align: 'right', renderer: floatRenderer2, hidden: true},
	
	    	    <?php if ($js_parameters->p_nascondi_importi != 'Y'){ ?>    	    
	    		{text: 'Importo',			width: 90, dataIndex: 'importo', align: 'right', renderer: floatRenderer2},
	    		<?php } ?>
	
	
  	
  	
<?php if (in_array($m_params->form_open->tipo_op, $todo_data_config['columns_show']['pagamento'])) { ?>
	{text: 'Pagamento', width: 90, dataIndex: 'des_pagamento'},
<?php }?>	
<?php if (in_array($m_params->form_open->tipo_op, $todo_data_config['columns_show']['preferenza'])) { ?>	
	{text: 'P', 		width: 27, dataIndex: 'preferenza', tooltip: 'Preferenza'},
	{text: '(an)', 	width: 32, dataIndex: 'preferenza_anag', tooltip: 'Preferenza anagrafica'},
<?php } ?>	
<?php if (in_array($m_params->form_open->tipo_op, $todo_data_config['columns_show']['scarico'])) { ?>	
	{text: '<img src=<?php echo img_path("icone/48x48/exchange.png") ?> width=25>', width: 27, dataIndex: 'scarico_intermedio', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(value)==1) return '<img src=<?php echo img_path("icone/48x48/exchange.png") ?> width=18>';
	        	    } else return value;
	
		}
	},	
<?php } ?>  	
	    		
	    		
	    		
<?php
 if ($m_params->form_open->tipo_op == 'SBLOC')
  $title_scadenza_blocco = 'Blocco';
 else if ($m_params->form_open->tipo_op == 'EXPO2')
     $title_scadenza_blocco = 'Inizio<br/>lavori';
  else
 $title_scadenza_blocco = 'Scadenza';
?>
  	
	
	    		{text: <?php echo j($title_scadenza_blocco)?>,			width: 70, dataIndex: 'data_scadenza', renderer: function (value, metaData, record, row, col, store, gridView){
	
	    			//per sblocco
	    			if (record.get('id').split("|")[0] == 'SBLOC'){
	
	    				t = new Date();
	    				t1 = new Date();
	    				t2 = new Date();
	    				t3 = new Date();
	    				t1.setMonth(t.getMonth() - 3);
	    				t2.setMonth(t.getMonth() - 6);    				
	    				t3.setMonth(t.getMonth() - 12);
	    				//se da almeno x mese.... grassetto, se da almeno y mesi .... sfondo rosa
						if (parseFloat(record.get('data_scadenza')) != 0 &&
	    	    				 parseFloat(record.get('data_scadenza')) <= parseFloat(Ext.Date.format(t3, 'Ymd')))
	    					metaData.tdCls += ' tpSfondoRosa grassetto';    				
	    				else if (parseFloat(record.get('data_scadenza')) != 0 &&
	    	    				 parseFloat(record.get('data_scadenza')) <= parseFloat(Ext.Date.format(t2, 'Ymd')))
	    					metaData.tdCls += ' tpSfondoRosa';
	    				else if (parseFloat(record.get('data_scadenza')) != 0 &&
	    	    				 parseFloat(record.get('data_scadenza')) <= parseFloat(Ext.Date.format(t1, 'Ymd')))
	    					metaData.tdCls += ' grassetto';        			
						
	        			 return date_from_AS(value, metaData, record);
	    			} //solo SBLOC
	        								
					if (parseFloat(record.get('data_scadenza')) != 0 && parseFloat(record.get('data_scadenza')) < parseFloat(Ext.Date.format(new Date(), 'Ymd')))
						metaData.tdCls += ' tpSfondoRosa';
					if (parseFloat(record.get('data_scadenza')) != 0 && parseFloat(record.get('data_scadenza')) == parseFloat(Ext.Date.format(new Date(), 'Ymd')))
						metaData.tdCls += ' tpSfondoGiallo';						
					 		 
					return date_from_AS(value, metaData, record);
				}
			}	    		
    	],
			enableSort: false, // disable sorting

	        listeners: {
	        
					afterrender: function (comp) {
					
					
					 
					
						if (Ext.get("todo_fl_solo_con_carico_assegnato") != null){		
					        Ext.get("todo_fl_solo_con_carico_assegnato").on('click', function(){
					        	acs_show_win_std('Selezione ordini con attivit&agrave; di avanzamento evasione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_filtri', null, 500, 400, null, 'icon-filter-16');
					        });
					        Ext.QuickTips.register({target: "todo_fl_solo_con_carico_assegnato",	text: 'Attiva/Disattiva filtri su attivit&agrave; elencate'});	        
						}		
						
						if (Ext.get("icona_ricerca_ordine") != null){		
					        Ext.get("icona_ricerca_ordine").on('click', function(){
					        	acs_show_win_std('Seleziona cliente', 'acs_panel_order_entry.php?fn=open_form_cliente', null, 400, 200, null, 'icon-filter-16');
					        });
					        Ext.QuickTips.register({target: "icona_ricerca_ordine",	text: 'Ricarca ordine'});	        
						}	
						
						<?php if ($auth->is_admin() ) {  ?>	
						
						
							if (Ext.get("attav") != null){		
					        Ext.get("attav").on('click', function(){
					        	acs_show_panel_std('acs_gest_ATTAV.php?fn=open_tab', null, {/*sezione: ''*/});
					        	//acs_show_panel_std('acs_gestione_attav.php?fn=open_grid', 'panel_gestione_attav', "");
					        });
					        Ext.QuickTips.register({target: "attav",	text: 'Gestione voci/opzioni ToDo list'});	        
						}	

								<?php }?>	
								
					
					
					},	        
	        

		        	beforeload: function(store, options) {
		        		Ext.getBody().mask('Loading... ', 'loading').show();
		        		},		
			
		            load: function () {
		              Ext.getBody().unmask();
		            },
		        

				  celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	var grid = iView;

						if (rec.get('liv')=='liv_4' && col_name=='tipo' && rec.get('tipo') != ''){
							iEvent.preventDefault();
							show_win_art_reso(rec.get('k_cli_des'));
							return false;
						}
													
						if (col_name=='art_mancanti' && rec.get('liv')=='liv_5'){
							iEvent.preventDefault();						
							show_win_art_critici(rec.get('k_ordine'), 'MTO');
							return false;							
						}	
						if (col_name=='art_da_prog' && rec.get('liv')=='liv_5'){
							iEvent.preventDefault();												
							show_win_art_critici(rec.get('k_ordine'), 'MTS');
							return false;							
						}														
						if (col_name=='colli' && rec.get('liv')=='liv_5'){
							iEvent.preventDefault();						
							show_win_colli_ord(rec, rec.get('k_ordine'));
							return false;
						}															
						if (col_name=='riferimento' && rec.get('liv')=='liv_4'){
							iEvent.preventDefault();						
							gmapPopup("gmap.php?ind=" + rec.get('gmap_ind'));
							return false;						
						}
						
						if (rec.get('liv') == 'liv_5' && col_name == 'note' && !Ext.isEmpty(rec.get('k_ordine'))){
						    var my_listeners = {
			        				afterInsert: function(from_win, src){
			        				     rec.set('note', src.ha_commenti);
 									    }
									}
									
		            		acs_show_win_std('Blocco note ordine ', 'acs_note_by_k_ordine.php?fn=open_bl', {k_ordine : rec.get('k_ordine')}, 800, 400, my_listeners, 'icon-comment_edit-16');	
						}
					 }
					},										
		       


				  itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      var record = grid.getStore().getAt(index);		  
				      var voci_menu = [];
				      

				      //LIVELLO 0
					  if (record.get('id').split("|")[0] == 'IMMOR' && record.get('liv') == 'liv_0'){
						  voci_menu.push({
				      		text: 'Order entry per utente',
				    		iconCls: 'iconPrint',
				    		handler: function() {					 
						        acs_show_win_std('Imposta filtri', 'acs_report_todolist_IMMOR.php?fn=open_form_filtri', null, 500, 220, null, 'icon-filter-16');
						    }
						   }); 					  
					   
						   
					  }
					  
				      //LIVELLO 0
					  if (record.get('id').split("|")[0] == 'REFOR' && record.get('liv') == 'liv_0'){
						  voci_menu.push({
				      		text: 'Assegna operatore Order Entry',
				    		iconCls: 'icon-button_red_play-16',
				    		handler: function() {					 
						        acs_show_win_std('Assegna operatore Order Entry', 'acs_manuenzione_parametri_operatori_OE.php?fn=open_params', {open_type: 'VIEW_CALENDAR', from_panel_id: grid.id}, 500, 320, null, 'icon-button_red_play-16');
						    }
						   });
						   
/*						   
						  voci_menu.push({
				      		text: 'Gestione parametri operatori OE',
				    		iconCls: 'icon-design-16',
				    		handler: function() {					 

								<?php //$cl = new SpedParametriOperatoriOE(); ?>
								<?php //echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "parametrioperatorioe") ?>
								<?php //echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "parametrioperatorioe") ?>
								<?php //echo $cl->out_Writer_Model("parametrioperatorioe") ?>
								<?php //echo $cl->out_Writer_Store("parametrioperatorioe") ?>
								<?php //echo $cl->out_Writer_sotto_main("parametrioperatorioe", 0.4) ?>								
								<?php //echo $cl->out_Writer_main("Lista parametri operatori Order Entry", "parametrioperatorioe") ?>								
								<?php //echo $cl->out_Writer_window("Tabella parametri operatori Order Entry") ?>							
						    }
						   });
*/						   
						   
						   						    					  
					  }					  
					  
					  
					  if (record.get('id').split("|")[0] == 'IMMOR' && record.get('liv') == 'liv_1'){
						  voci_menu.push({
				      		text: 'Elenco ordini assegnati',
				    		iconCls: 'iconPrint',
				    		handler: function() {
				    		
				    		  //grid-arrivi disattivata per IMMOR
								
								var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: 'acs_report_todolist_REFOR_UTENTE.php?fn=open_report',
								        params: {
								                 //todo_params: Ext.JSON.encode(arrivi.store.proxy.extraParams),
								        		 id_selected: rec.get('liv_cod'),
								        		 dett_ord: 'Y'},
								        });
								return(false);
						    }
						   }); 		
						   
						   
						   
						   
						  voci_menu.push({
				      		text: 'Stampa documentazione ordini assegnati',
				    		iconCls: 'iconPrint',
				    		handler: function() {					 
				    		
						    Ext.Msg.confirm('Conferma', 'Confermi generazione documentazione ordini assegnati?', function(btn, text){
						      if (btn == 'yes'){
						      
										Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=write_RI_documentazione',
									        params: {									        
									        	//todo_params: Ext.JSON.encode(arrivi.store.proxy.extraParams),
									        	id_selected: rec.get('liv_cod')
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									            acs_show_msg_info('Richiesta completata');									            			            
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	

						      } else {
						        //nothing
						      }
						    });				    			
						    }
						   });							   
						   
						   
						   
						   			  
					  }					  				      
					  if (record.get('id').split("|")[0] == 'SBLOC' && record.get('liv') == 'liv_0'){
						  voci_menu.push({
				      		text: 'Report clienti/ordini bloccati',
				    		iconCls: 'iconPrint',
				    		handler: function() {					 
						        acs_show_win_std('Report clienti/ordini bloccati', 'acs_report_todolist_SBLOC.php?fn=open_form_filtri', {todo_params: grid.getStore().treeStore.proxy.extraParams}, 500, 220, null, 'icon-filter-16');
						    }
						   }); 					  
					  }
					  
		/* spostata nel menu overflow			  				      
					  if (record.get('id').split("|")[0] == 'SBLOC' && record.get('liv') == 'liv_0'){
						  voci_menu.push({
				      		text: 'Stampa estratto conto',
				    		iconCls: 'iconPrint',
				    		handler: function() {					 
						        acs_show_win_std('Stampa estratto conto', 'acs_report_ec.php?fn=open_form_filtri', {todo_params: arrivi.store.proxy.extraParams}, 500, 220, null, 'icon-filter-16');
						    }
						   }); 					  
					  }
		*/					  
					  
									
<?php  if ($m_params->form_open->tipo_op == 'SBLOC') {
	//SOLO SBLOC: possibilita' di creare (gia' rilasciate) delle operazioni	
	  $op_SB = $s->find_TA_std('ATTAV', null, 'Y', 'Y', null, null, 'SB');	  
	  ?>
				  
	  if (record.get('liv') == 'liv_5'){
	  
	  id_selected = grid.getSelectionModel().getSelection();
	  list_selected_id_checklist = [];
	  
	  for (var i=0; i<id_selected.length; i++) 
		list_selected_id_checklist.push({
					id: id_selected[i].get('id'), 
					dt_orig: id_selected[i].get('dt_orig'), 
					k_ordine: id_selected[i].get('k_ordine')});
					
	  
        my_listeners_inserimento = {
			afterOkSave: function(from_win){
				//dopo che ha chiuso la maschera di assegnazione indice rottura
				for (var i=0; i<id_selected.length; i++){

								 //richiedo aggiornamento campo seq_carico
									Ext.Ajax.request({
											m_rec: id_selected[i], //passo il record che poi' modifichero'
									        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=SBLOC_get_seq_carico',
									        jsonData: {
									        	k_ordine: id_selected[i].get('k_ordine')
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){									            
									            var jsonData = Ext.decode(result.responseText);
												 request.m_rec.set('seq_carico', jsonData.att_distinct);
												 request.m_rec.set('tooltip_seq_carico', jsonData.tooltip);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });				
				
				}
			  from_win.close();	
			} 
			
			
			       
        };    
        
        <?php  if ($cfg_mod_Spedizioni['SBLOC_use_check_list'] == 'Y') { ?>
        
		voci_menu.push({
			text: 'Inserimento Check list',
			iconCls : 'icon-arrivi_gray-16',          		
			handler: function() {
				acs_show_win_std('Inserimento Check list', 
					<?php echo j('acs_form_json_create_entry.php'); ?>, 
					{
						tipo_op: null,
						blocco_op: 'SB',
		  				list_selected_id: list_selected_id_checklist
		  			}, 500, 450, my_listeners_inserimento, 'icon-arrivi_gray-16');						        		          		
        		}
    		});	  
	  
	  
    <?php foreach($op_SB as $op) { ?>
		  voci_menu.push({
      		text: <?php echo j(trim($op['text'])); ?>,
    		iconCls: 'icon-sub_black_accept-16',
    		handler: function() {					 

					Ext.Ajax.request({
					        url        : 'acs_op_exe.php?fn=exe_crea_segnalazione_arrivi_json',
					        jsonData: {
					        	list_selected_id: list_selected_id_checklist,
					        	f_causale: <?php echo j(trim($op['id'])); ?>,
					        	delete_if_exists: 'Y'
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){
					            var jsonData = Ext.decode(result.responseText);
								for (var i=0; i<id_selected.length; i++){
								
								 //richiedo aggiornamento campo seq_carico
									Ext.Ajax.request({
											m_rec: id_selected[i], //passo il record che poi' modifichero'
									        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=SBLOC_get_seq_carico',
									        jsonData: {
									        	k_ordine: id_selected[i].get('k_ordine')
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){									            
									            var jsonData = Ext.decode(result.responseText);
												 request.m_rec.set('seq_carico', jsonData.att_distinct);
												 request.m_rec.set('tooltip_seq_carico', jsonData.tooltip);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
								
								}
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });	
	
		    }
		  });
      <?php } ?>
      
      
       
        
		voci_menu.push({
			xtype: 'menuseparator'
		});
		
		 <?php } ?>
      
		  
	  } //if liv_5
<?php } ?>
									
							
					  id_selected = grid.getSelectionModel().getSelection();
					  list_selected_id = [];
					  for (var i=0; i<id_selected.length; i++) 
						list_selected_id.push({id: id_selected[i].get('id'), 
									prog: id_selected[i].get('prog'),
									dt_orig: id_selected[i].get('dt_orig'), 
									f_ril: id_selected[i].get('f_ril'),
									k_ordine: id_selected[i].get('k_ordine')});

							
																	      
					  if (rec.get('k_ordine').length > 0 && rec.get('rec_stato')!='Y') //sono nel livello ordine
					  voci_menu.push({
			      		text: 'Avanzamento/Rilascio attivit&agrave;',
			    		iconCls: 'icon-arrivi-16',
			    		handler: function() {

				    		//verifico che abbia selezionato solo righe a livello ordine 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('k_ordine').length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  }
							  }
  	
				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('rec_stato') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
										    
										    //verifico se e' cambiato lo stato
										    grid.panel.refresh_stato_per_record(id_selected[i].get('id'));
										    
					                      } //per ogni record selezionato
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {list_selected_id: list_selected_id, grid_id: grid.id},
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
			    		}
					  });
					  
					  
<?php  if ($m_params->form_open->tipo_op == 'SBLOC') { ?>
<?php
$causali_rilascio = $s->find_TA_std('RILAV', 'SBLOC', 'N', 'Y');					  
foreach($causali_rilascio as $ca) {
?>	
					  
	if (rec.get('k_ordine').length > 0 && rec.get('rec_stato')!='Y') //sono nel livello ordine					  
	voci_menu.push({
	      		text: <?php echo j($ca['text']); ?>,
	    		iconCls: 'iconAccept',
	    		handler: function() {					 
	
						Ext.Ajax.request({
						        url        : 'acs_op_exe.php?fn=exe_avanzamento_segnalazione_arrivi_json',
						        jsonData: {
						        	list_selected_id: list_selected_id,
						        	f_entry_prog_causale: <?php echo j($ca['id']); ?>
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
									for (var i=0; i<id_selected.length; i++){
									 id_selected[i].set('rec_stato', 'Y');
									}
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
		
			    }
			  });
  <?php } ?>
  
  
													  
	
  
<?php } ?>					  
					  
		 
					 
					 
<?php  if ($m_params->form_open->tipo_op != 'SBLOC') { ?>
<?php
#TODO: questo puo' andare bene anche per SBLOC: unificare
$causali_rilascio = $s->find_TA_std('RILAV', null, 'N', 'Y'); //recupero tutte le RILAV
foreach($causali_rilascio as $ca) {
?>	
	
	//se ho indicati degli stati di validita (TAMAIL in RILAV) verifico che tutti gli ordini soddisfino
	
	<?php if (strlen(trim($ca['TAMAIL'])) > 0){
	    $exp_stati = explode('|', trim($ca['TAMAIL']));
	    if ($exp_stati[0] == 'E' || $exp_stati[0] == 'I'){
	        echo "var includi_escludi_stati = '{$exp_stati[0]}';";
	        array_shift($exp_stati);
	    } else {
	        echo "var includi_escludi_stati = 'I';";
	    }
	    echo "var ar_abilita_su_stati = " . acs_je($exp_stati) . ";";
	} else {
	    echo "var ar_abilita_su_stati = []";
	} ?>

	
		
	var disabilita_causa_stato = false;
	if (ar_abilita_su_stati.length > 0){
	  for (var i=0; i<id_selected.length; i++){ 
	  
	   //lo stato non appartiene a quelli ammessi
	  	 if (includi_escludi_stati == 'I')
		  if (!Ext.Array.contains(ar_abilita_su_stati, rec.get('stato')))
		  	disabilita_causa_stato = true;
		  
	   //lo stato non appartiene a quelli ammessi
	  	 if (includi_escludi_stati == 'E')
		  if (Ext.Array.contains(ar_abilita_su_stati, rec.get('stato')))
		  	disabilita_causa_stato = true;
		  
	  }	
	}
			
	if (	rec.get('k_ordine').length > 0 &&
		 	rec.get('rec_stato')!='Y' &&
			rec.get('id').split("|")[0] == <?php echo j(trim($ca['id'])); ?> &&
			disabilita_causa_stato == false
		 ) //sono nel livello ordine e nella causale giusta					  
	voci_menu.push({
	      		text: <?php echo j($ca['text']); ?>,
	    		iconCls: 'iconAccept',
	    		handler: function() {
	    		
	    		
				    		//verifico che abbia selezionato solo righe a livello ordine 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('k_ordine').length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  }
							  }
  	
				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('rec_stato') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					         
										    
										    //verifico se e' cambiato lo stato
										    grid.panel.refresh_stato_per_record(id_selected[i].get('id'));										    
										                   
					                      } //per ogni record selezionato
					                  }
					
					         }								
							});				    			
			    			mw.show();

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							           			list_selected_id: list_selected_id, grid_id: grid.id,
												auto_set_causale: <?php echo j(trim($ca['TAKEY2'])); ?>							        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
	    
						    
		
			    }
			  });
  <?php } ?>
  
	
  
<?php } ?>							 
					 
					 
					  


					   //if (record.get('id').split("|")[0] == 'ASSCAU' && record.get('liv') == 'liv_1'){
					   if (['ASSCAU', 'VERGAR', 'REFOR', 'MODCO', 'MODAR', 'ASSBCCO', 'ASSPVEN'].indexOf(record.get('id').split("|")[0]) > -1 && record.get('liv') == 'liv_1'){
						  voci_menu.push({
					      		text: 'Elenco ordini ToDo List',
					    		iconCls: 'iconAccept',
					    		handler: function() {
					    			acs_show_win_std('Selezione ordini', 
					    				   'acs_seleziona_ordini_todo.php?fn=open_form', {
						        				grid_id: null,
						        				tipo_elenco: 'FROM_TODO',
							        			causale: record.get('id').split("|")[0],
							        			grid_proxy_params: grid.getStore().treeStore.proxy.extraParams,
							        			tree_rec_id: record.get('id'),
							        			
											 },		        				 
					        				1024, 600, {}, 'icon-leaf-16');	                		               
					   			}
					   	  });
					   }
	
	
					   	if (['ASSCAU', 'VERGAR', 'REFOR', 'ASSBCCO', 'ASSPVEN'].indexOf(record.get('id').split("|")[0]) > -1 && record.get('liv') == 'liv_2'){
						  voci_menu.push({
					      		text: 'Elenco ordini ToDo List',
					    		iconCls: 'iconAccept',
					    		handler: function() {
					    			acs_show_win_std('Selezione ordini', 
					    				   'acs_seleziona_ordini_todo.php?fn=open_form', {
						        				grid_id: null,
						        				tipo_elenco: 'FROM_TODO',
							        			causale: record.get('id').split("|")[0],
							        			stato: record.get('liv_cod'),
							        			grid_proxy_params: grid.getStore().treeStore.proxy.extraParams,
							        			tree_rec_id: record.get('id'),
							        			
						    					},		        				 
					        				1024, 600, {}, 'icon-leaf-16');	                		               
					   			}
					   	  });
					   }
					   
					   	if (['ASSCAU', 'VERGAR', 'REFOR'].indexOf(record.get('id').split("|")[0]) > -1 && record.get('liv') == 'liv_3'){
						  voci_menu.push({
					      		text: 'Elenco ordini ToDo List',
					    		iconCls: 'iconAccept',
					    		handler: function() {
					    			acs_show_win_std('Selezione ordini', 
					    				   'acs_seleziona_ordini_todo.php?fn=open_form', {
						        				grid_id: null,
						        				tipo_elenco: 'FROM_TODO',
							        			causale: record.get('id').split("|")[0],
							        			grid_proxy_params: grid.getStore().treeStore.proxy.extraParams,
							        			tree_rec_id: record.get('id'),
							        			
						    					},		        				 
					        				1024, 600, {}, 'icon-leaf-16');	                		               
					   			}
					   	  });
					   }					   
	

					  if (record.get('id').split("|")[0] == 'SBLOC' && record.get('liv') == 'liv_2'){
						  voci_menu.push({
					      		text: 'Interrogazione esposizione batch',
					    		iconCls: 'iconAccept',
					    		handler: function() {

						    		//verifico che abbia selezionato solo righe a livello ordine 
									  if (id_selected.length > 1){
											  acs_show_msg_error('Selezionare un singolo cliente');
											  return false;
										  }
										  
						            //apro la visualizzazione e esco
						            acs_show_win_std('Esposizione [' + record.get('liv_cod') + ']', 'acs_json_analisi_esposizione.php?fn=open_view', {list_selected_id: record.get('liv_cod')}, 900, 600, {}, 'icon-info_blue-16');
						            return false;										  
						    		
					    		}
							  });						  
					  }
					  
					  
					  
					  if (record.get('id').split("|")[0] == 'SBLOC' && record.get('liv') == 'liv_2'){
						  voci_menu.push({
					      		text: 'Interrogazione esposizione corrente',
					    		iconCls: 'iconAccept',
					    		handler: function() {

						    		//verifico che abbia selezionato solo righe a livello ordine 
									  if (id_selected.length > 1){
											  acs_show_msg_error('Selezionare un singolo cliente');
											  return false;
										  }
										  
		    			

									//lancio l'elaborazione
					            	cod_cli = record.get('liv_cod').trim();
					            	
					            	//eseguo richiesta e mostro pagina
										Ext.getBody().mask('Loading... ', 'loading').show();	            
											Ext.Ajax.request({
															url: 'acs_json_analisi_esposizione.php?fn=exe_richiesta_aggiornamento',
													        jsonData: {
													        	add_parameters: 'E',
													        	list_selected_id: cod_cli
													        },
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
																Ext.getBody().unmask();									        
													            acs_show_win_std('Analisi esposizine corrente [' + cod_cli + ']', 'acs_json_analisi_esposizione.php?fn=open_view&view=analisi_esposizione', {list_selected_id: cod_cli}, 600, 500, {}, 'icon-info_blue-16');
													        }, scope: this,
													        failure    : function(result, request){
																Ext.getBody().unmask();									        
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });
						    		
					    		}
							  });						  
					  }					  
					  
					  
					  if (rec.get('liv') == 'liv_5'){		 
						voci_menu.push({
								xtype: 'menuseparator'
							});
							  

						if (record.get('id').split("|")[0] == 'IMMOR' || record.get('id').split("|")[0] == 'VAROC'){	
				  		  voci_menu.push({
				         		text: 'Visualizza righe order entry',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Righe ordine oentry', 'acs_get_order_rows_gest_order_entry.php', {
				        				panel_from: 'order_entry',
				        				k_ordine: rec.get('k_ordine')
				        		}, 1000, 450, null, 'icon-folder_search-16');          		
				        		}
				    		});		
				    		
				    		voci_menu.push({
				         		text: 'Messaggio oentry',
				        		iconCls : 'icon-leaf-16',          		
				        		handler: function() {
				        			Ext.Ajax.request({
                				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_oentry',
                				        jsonData: {									        
                				        	list_selected_id: list_selected_id
                				        },
                				        method     : 'POST',
                				        waitMsg    : 'Data loading',
                				        success : function(result, request){
                							var jsonData = Ext.decode(result.responseText);
                							acs_show_msg_info('Operazione completata');        				            									            			            
                				        },
                				        failure    : function(result, request){
                				            Ext.Msg.alert('Message', 'No data to be loaded');
                				        }
                				    });					
					         		
				        		}
				    		});	
				    		
			    		 	 voci_menu.push({
            	         		text: 'Testata ordine',
            	        		iconCls : 'icon-pencil-16',          		
            	        		handler: function () {
            	        			    list_selected_id = grid.getSelectionModel().getSelection();
            			    		  	rec = list_selected_id[0];	
            			    		  	
            			    		  	my_listeners = {
            			    		  			afterOkSave: function(from_win, new_value){
            		        						rec.set('tipo', new_value.TDOTPD);
            		        						rec.set('riferimento', new_value.TDVSRF);
            		        						rec.set('data_reg', new_value.TDODRE);
            		        						rec.set('priorita', new_value.TDOPRI);
            		        						from_win.close();  
            						        		}
            				    				};	
            			    		  	 
            			    		  	acs_show_win_std('Modifica informazioni di testata', 
            		                			'acs_panel_protocollazione_mod_testata.php?fn=open_form', {
            		                				tddocu: rec.get('k_ordine')
            									}, 800, 600, my_listeners, 'icon-shopping_cart_gray-16');
            		                }
            	    		});	 
				    		
				    								
						  }else{
				  		  voci_menu.push({
				         		text: 'Visualizza righe',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {k_ordine: rec.get('liv_cod')}, 900, 450, null, 'icon-folder_search-16');          		
				        		}
				    		});				 
				    	}	
				       if (record.get('id').split("|")[0] == 'EXPO2')					    		
				    			voci_menu.push({
				         		text: 'Modifica ToDo',
				        		iconCls : 'icon-pencil-16',          		
				        		handler: function() {
				        		
				        		id_selected = grid.getSelectionModel().getSelection();
				        		list_selected_id = [];
            		  			for (var i=0; i<id_selected.length; i++){ 
            		  			 if (id_selected[i].data.liv != 'liv_5') {
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  } 
            		  			
            			   			list_selected_id.push(id_selected[i].data.prog);
								}
    
    				        		 var my_listeners = {
    			    		  			afterOkSave: function(from_win){
    			    		  			    grid.getTreeStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
				        		
				        		
				        			acs_show_win_std('Modifica ToDo', 'acs_panel_expo.php?fn=open_mod', {list_selected_id : list_selected_id}, 370, 220, my_listeners, 'icon-pencil-16');          		
				        		}
				    		});
				    		
				    		 if (record.get('id').split("|")[0] == 'EXPO1' || record.get('id').split("|")[0] == 'EXPO2'){					    		
				    			voci_menu.push({
				         		text: 'Genera allestimento',
				        		iconCls : 'icon-button_black_play-16',          		
				        		handler: function() {
				        		
				        		id_selected = grid.getSelectionModel().getSelection();
				        		list_selected_id = [];
            		  			for (var i=0; i<id_selected.length; i++){ 
            		  			 if (id_selected[i].data.liv != 'liv_5') {
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  } 
            		  			
            			   			list_selected_id.push(id_selected[i].data.k_ordine);
								}
    
    				        		 var my_listeners = {
    			    		  			afterOkSave: function(from_win){
    			    		  			    grid.getTreeStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
				        		
				        		
				        			acs_show_win_std('Genera allestimento', 'acs_panel_expo.php?fn=open_form_al', {list_selected_id : list_selected_id}, 370, 220, my_listeners, 'icon-button_black_play-16');          		
				        		}
				    		});
				    		
					 	voci_menu.push({
				         		text: 'Assegna allestimento',
				        		iconCls : 'icon-button_grey_play-16',          		
				        		handler: function() {
				        		
				        		id_selected = grid.getSelectionModel().getSelection();
				        		list_selected_id = [];
            		  			for (var i=0; i<id_selected.length; i++){ 
            		  			 if (id_selected[i].data.liv != 'liv_5') {
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  } 
            		  			
            			   			list_selected_id.push(id_selected[i].data.k_ordine);
								}
    
    				        		 var my_listeners = {
    			    		  			afterOkSave: function(from_win){
    			    		  			    grid.getTreeStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
				        		
				        		
				        			acs_show_win_std('Assegna allestimento', 'acs_panel_expo.php?fn=open_assegna', {list_selected_id : list_selected_id}, 400, 200, my_listeners, 'icon-button_grey_play-16');          		
				        		}
				    		});
				    		}
					  }


				      var menu = new Ext.menu.Menu({
				            items: voci_menu
						}).showAt(event.xy);
					  
				  }, 	
					



		        	
		        	
			        itemclick: function(view,rec,item,index,eventObj) {
			          if (rec.get('liv') == 'liv_5')
			          {	
			        	var w = Ext.getCmp('OrdPropertyGrid');
			        	//var wd = Ext.getCmp('OrdPropertyGridDet');
			        	var wdi = Ext.getCmp('OrdPropertyGridImg');        	
			        	var wdc = Ext.getCmp('OrdPropertyCronologia');

			        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdi.store.load()
						wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdc.store.load()			        												        	
			        	
						Ext.Ajax.request({
						   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
						   success: function(response, opts) {
						      var src = Ext.decode(response.responseText);
						      w.setSource(src.riferimenti);
					          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);						      
						      //wd.setSource(src.dettagli);
						   }
						});
						
        				//files (uploaded) - ToDo: dry 
        				var wdu = Ext.getCmp('OrdPropertyGridFiles');
        				if (!Ext.isEmpty(wdu)){
        					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
        			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
        			        	if (wdu.isVisible())		        	
        							wdu.store.load();
        					}					
        				}							
						
					   }												        	
			         }					            											        	
		        },
			

			viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
		           	
 		           return v;																
		         }		         		        		            
		    },
		    
		    
		    refresh_stato_per_record: function(id_record){
		    	//rileggo il record e verifico se lo stato e' cambiato
		    	var me = this,
		    		m_tree = this.view,
		    		m_store = m_tree.getStore(),
		    		rec_index = m_store.findExact('id', id_record);
		    		
		    	if (!Ext.isEmpty(rec_index)){
					m_tree.select(rec_index);
					m_tree.focusRow(rec_index);
					var m_rec = m_store.getAt(rec_index);
					
					//rileggo lo stato dell'ordine da DB e segnalo se e' cambiato
					
					Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_ordine_by_k_ordine',
				        jsonData: {									        
				        	k_ordine: m_rec.get('k_ordine')
				        },
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
							var jsonData = Ext.decode(result.responseText),
								return_record = jsonData.record;
								
							if (jsonData.success){
    							if (return_record.TDSTAT != m_rec.get('stato')){
    								m_rec.set('stato', return_record.TDSTAT); 													        
            						m_rec.set('rec_stato_updated', 'Y');		
            					}
        					} else {
        						//success = false
        						Ext.Msg.alert('Errore', jsonData.message);
        					}		        				            									            			            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });					
					
					

				}
				
				
		   	}
 }
} 

	           
