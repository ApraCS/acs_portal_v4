<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($s->get_cod_mod());
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// assegna pedane
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_assegna_pedane'){
	$m_params = acs_m_params_json_decode();
	$ar_tddocu = array();
	
	
	foreach((array)$m_params->list_selected_id as $tddocu){	
		$ar_tddocu[] = $tddocu;
	}

	$sql = "SELECT TDDT, TDTPCA, TDAACA, TDNRCA
			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
			WHERE " . $s->get_where_std() . " AND TDDOCU IN (" . sql_t_IN($ar_tddocu) . ")
			GROUP BY TDDT, TDTPCA, TDAACA, TDNRCA";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $id_ditta_default);
	$cl_p .= sprintf("%08d", 0);		
	
	$c = 0;
	while ($row = db2_fetch_assoc($stmt)) {
		$c++;
		//costruzione del parametro
		$cl_p .= sprintf("%-5s", trim($row['TDTPCA']));
		$cl_p .= sprintf("%04d", $row['TDAACA']);
		$cl_p .= sprintf("%06d", $row['TDNRCA']);		
	}
	
	//riempio perche' nei parametri sono sempre previsti 10 carichi
	for ($i=$c++; $i<=10; $i++){
		$cl_p .= sprintf("%-5s", '');
		$cl_p .= sprintf("%04d", 0);
		$cl_p .= sprintf("%06d", 0);		
	}
	
	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
	$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
	
	$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
	$call_return = $tkObj->PgmCall('UR21HD', $libreria_predefinita_EXE, $cl_in, null, null);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);	
	exit;
}


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();

	$sql = "SELECT *
			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI					
			WHERE " . $s->get_where_std() . " AND 1=1 ";
	
	$sql_where = "";
	$ar_sql = array();
	$m_params = acs_m_params_json_decode();
	
	$stato_selected=$m_params->stato_selected;
	
	
	if ($m_params->solo_senza_carico == 'Y')
		$sql_where .= " AND TDNRCA=0 ";
	
	switch ($m_params->tipo_elenco){
		case 'DAY':
			$sql_where .= " AND TDCITI = ? AND TDNBOC = ? AND TDSWSP = 'Y' AND " . $s->get_where_std() ;
			$ar_sql[] = $m_params->cod_iti;
			$ar_sql[] = $m_params->sped_id;
		break;
		case "HOLD":
			$sql_where .= " AND TDCITI = ? AND TDSWSP = 'N' AND " . $s->get_where_std() ;
			$ar_sql[] = $m_params->cod_iti;			
			
			if (strlen($m_params->k_cli_des) > 0){
				$k_cli_des_exp = explode("_", $m_params->k_cli_des);				
				$sql_where .= " AND TDDT = ? AND TDCCON = ? AND TDCDES = ? ";
				$ar_sql[] = $k_cli_des_exp[0];
				$ar_sql[] = $k_cli_des_exp[1];
				$ar_sql[] = $k_cli_des_exp[2];
			}
		break;
		case "DA_PLAN":
			if (substr($m_params->col_name, 0, 2)=="d_"){ //ho selezionato un cella su un giorno
				$add_day = (int)substr($m_params->col_name, 2, 2) - 1;
				$m_data = date('Ymd', strtotime($m_params->da_data . " +{$add_day} days"));
				$sql_where .= " AND TDDTEP = ? AND TDSWSP = 'Y' ";
				$ar_sql[] = $m_data;
				
				//in base a liv aggiungo filtro (per area, itinerario, spedizione)
				$record_id_ar = explode("|", $m_params->record_id);
				if ($m_params->record_liv == 'liv_1' || $m_params->record_liv == 'liv_2' || $m_params->record_liv == 'liv_3'){
					$sql_where .= " AND TA_ITIN.TAASPE = ? ";
					$ar_sql[] = $record_id_ar[2];						
				}
				if ($m_params->record_liv == 'liv_2' || $m_params->record_liv == 'liv_3'){
					$sql_where .= " AND TA_ITIN.TAKEY1 = ? ";
					$ar_sql[] = $record_id_ar[4];
				}
				if ($m_params->record_liv == 'liv_3'){
					$sql_where .= " AND TDNBOC = ? ";
					$ar_sql[] = $record_id_ar[6];
				}
					
			}
			
			//filtro su area/itinerario
			$ar_liv = explode("|", $m_params->record_id);
			if ($m_params->record_liv == 'liv_1'){ //AREA SPEDIZIONE
				$sql_where .= " AND TA_ITIN.TAASPE = ? ";
				$ar_sql[] = $ar_liv[2];				
			}
			if ($m_params->record_liv == 'liv_2'){ //AREA SPEDIZIONE
				$sql_where .= " AND TA_ITIN.TAASPE = ? AND TDCITI = ? ";
				$ar_sql[] = $ar_liv[2];
				$ar_sql[] = $ar_liv[4];
			}		
		
		break;
		case "DA_FLIGHT":
		    if (substr($m_params->col_name, 0, 2)=="d_"){ //ho selezionato un cella su un giorno
		        $add_day = (int)substr($m_params->col_name, 2, 2) - 1;
		        $m_data = date('Ymd', strtotime($m_params->da_data . " +{$add_day} days"));
		        $sql_where .= " AND TDDTSP = ? AND TDSWSP = 'Y' ";
		        $ar_sql[] = $m_data;
		        
		        //in base a liv aggiungo filtro (per area, itinerario, spedizione)
		        $record_id_ar = explode("|", $m_params->record_id);
		        if ($m_params->record_liv == 'liv_1' || $m_params->record_liv == 'liv_2' || $m_params->record_liv == 'liv_3'){
		            $sql_where .= " AND TA_ITIN.TAASPE = ? ";
		            $ar_sql[] = $record_id_ar[2];
		        }
		        if ($m_params->record_liv == 'liv_2' || $m_params->record_liv == 'liv_3'){
		            $sql_where .= " AND TA_ITIN.TAKEY1 = ? ";
		            $ar_sql[] = $record_id_ar[4];
		        }
		        if ($m_params->record_liv == 'liv_3'){
		            $sql_where .= " AND TDNBOC = ? ";
		            $ar_sql[] = $record_id_ar[6];
		        }
		        
		    }
		    
		    //filtro su area/itinerario
		    $ar_liv = explode("|", $m_params->record_id);
		    if ($m_params->record_liv == 'liv_1'){ //AREA SPEDIZIONE
		        $sql_where .= " AND TA_ITIN.TAASPE = ? ";
		        $ar_sql[] = $ar_liv[2];
		    }
		    if ($m_params->record_liv == 'liv_2'){ //AREA SPEDIZIONE
		        $sql_where .= " AND TA_ITIN.TAASPE = ? AND TDCITI = ? ";
		        $ar_sql[] = $ar_liv[2];
		        $ar_sql[] = $ar_liv[4];
		    }
		    
		    break;
		case "DA_INFO":
			$filtro = $s->get_elenco_ordini_create_filtro((array)$m_params->form_values);
			$use_my_stmt = 'Y';
			
			//Per il bottone Programmazione/Monitor devo vedere solo gli ordini plan (TDSWPP = 'Y')
			// altrimenti uso filtro standard (search)
			if ($m_params->menu_type == 'PRO_MON')
			    $search_type = null;
			else
			   $search_type = 'search';
			
			$stmt = $s->get_elenco_ordini($filtro, 'N', $search_type);
		break;
	}
	
	if ($use_my_stmt != 'Y'){
		$sql = $sql . $sql_where;
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql);		
	}

	while ($row = db2_fetch_assoc($stmt)) {
			$nr = array();
				$nr['TDDCON'] 		= acs_u8e(trim($row['TDDCON']));
				$nr['localita'] 	= acs_u8e(trim($s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD'])));
				$nr['TDVSRF']		= acs_u8e(trim($row['TDVSRF']));
				$nr['k_ordine']		= trim($row['TDDOCU']);
				$nr['ordine_out']	= $s->k_ordine_out($row['TDDOCU']);
				$nr['tipoOrd']		= trim($row['TDOTPD']);
				$nr['fl_cli_bloc']	= trim($row['TDFN02']);
				$nr['fl_evaso']		= trim($row['TDFN11']);
				$nr['fl_bloc']		= $s->get_ord_fl_bloc($row);
				$nr['cons_prog']	= $row['TDDTEP'];
				$nr['data_atp']	    = $row['TDDATP'];
				$nr['cons_rich']	= $row['TDODER'];
				$nr['priorita']     = acs_u8e($row['TDOPRI']);
				$nr['colli_sped'] 	= $row['TDCOSP'];
				$nr['proforma'] 	= trim($row['TDPROF']);
				$nr['stato'] 		= trim($row['TDSTAT']);				
				$nr['raggr'] 		= trim($row['TDCLOR']);
				$nr['var1']			= acs_u8e(trim($row['TDDVN1']));
				$nr['k_carico']		= $s->k_carico_td($row);
				$nr['liv']			= 'liv_2'; //serve per assegna carico
				$nr['fl_cons_conf'] = $row['TDFN06'];
				if ((int)$row['TDNRCA'] > 0)
					$nr['carico'] 		= implode("_", array($row['TDAACA'], trim($row['TDNRCA'])));

				if ((int)$row['TDNRLO'] > 0)
					$nr['lotto'] 		= implode("_", array($row['TDAALO'], trim($row['TDNRLO'])));
				
				$nr['rif_scarico']		= trim(acs_u8e($row['TDRFCA']));				
				$nr['indice_rottura'] = trim($row['TDIRLO']);								
				$nr['fl_art_manc'] = $s->get_fl_art_manc($row);
				$nr['art_da_prog'] = $s->get_art_da_prog($row);
				$nr['TDVOLU'] 	   = $row['TDVOLU'];
				$nr['TDSELO'] 	   = trim($row['TDSELO']);	
				
				$nr['TDODRE'] 	   = $row['TDODRE'];	
				$nr['TDTIMP'] 	   = $row['TDTIMP'];	
				
				if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){				
					$nr['cod_articolo'] = trim(acs_u8e($row['RDART']));
					$nr['des_articolo'] = trim(acs_u8e($row['RDDES1']));
					$nr['um'] 			= trim(acs_u8e($row['RDUM']));
					$nr['qta'] 			= trim(acs_u8e($row['RDQTA']));
				}
				
				$data[] = $nr;
	}
	
	echo acs_je($data);
	
exit;
}


// ******************************************************************************************
// EXE MODIFICA POSIZIONE (PEDANA)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_posizione'){
	$m_params = acs_m_params_json_decode();	
	
	$ar_upd = array();
	$ar_upd['TDRFCA']  = $m_params->form_values->f_posizione;
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE TDDOCU = ?";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
			array($m_params->form_values->k_ordine)));
	echo db2_stmt_errormsg($stmt);
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// FORM MODIFICA POSIZIONE (PEDANA)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'form_modifica_posizione'){
	$m_params = acs_m_params_json_decode();
	$ord = $s->get_ordine_by_k_docu($m_params->k_ordine);
?>	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
							var form = this.up('form').getForm();
							var loc_win = this.up('window')			            

							Ext.Ajax.request({
							        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_posizione',
							        jsonData: {
							        	form_values: form.getValues()						        	
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){	            	  													        
							            var jsonData = Ext.decode(result.responseText);
							            loc_win.fireEvent('onClose', loc_win);	
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							            console.log('errorrrrr');
							        }
							    });						         	
			            
			            }
			         }
					 
						
						], items: [
						{
							name: 'k_ordine',						
							xtype: 'hiddenfield',
						    value: <?php echo j($m_params->k_ordine) ?>							
						},
		         	  	{
							name: 'f_posizione',						
							xtype: 'textfield',
							fieldLabel: 'Rifer.scar.',
						    maxLength: 5,
						    value: <?php echo j(trim($ord['TDRFCA']))?>
						}
					]
				}
			]
		}
	]
}			
<?php	
	exit;
}




$m_params = acs_m_params_json_decode();
?>
{"success": true, "items":
	{
		xtype: 'gridpanel',
		multiSelect: true,
		
		<?php if($m_params->from_overflow == 'Y'){?>
		    selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
		<?php }?>
		
        stateful: true,
        stateId: 'seleziona-ordini',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
        
        features: [
	
		{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 	type: 'boolean',
			dataIndex: 'visible'
		     }
		      ]
		}],
		
  	    store: Ext.create('Ext.data.Store', {
													
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							type: 'ajax',
							actionMethods: {
								read: 'POST'
							},							
							reader: {
					            type: 'json',
					            root: 'root'
					        }
					        
					        , extraParams: <?php echo acs_raw_post_data(); ?>
					        
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
							, doRequest: personalizza_extraParams_to_jsonData					        
					        
						},
	        			fields: [
	            			'TDCCON', 'TDDCON', 'TDVOLU', 'TDSELO', 'fl_bloc', 'fl_cli_bloc', 'localita', 'TDVSRF', 'k_ordine', 'ordine_out', 
	            			'tipoOrd', 'cons_prog', 'fl_evaso', 'colli_sped', 'proforma', 'stato', 'raggr', 'var1', 'cod_articolo','des_articolo', 'um', 'qta',
	            			'k_carico', 'fl_bloc', 'iconCls', 'liv', 'fl_cons_conf', 'carico', 'lotto', 'rif_scarico', 'indice_rottura', 'fl_art_manc', 'art_da_prog',
	            			'data_atp', 'cons_rich', 'priorita', 'TDODRE', 'TDTIMP'
	        			]
	    			}),
	    			
		        columns: [{
						    text: '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> height=25>',
						    width: 30, tooltip: 'Clienti bloccati', dataIndex: 'fl_cli_bloc', 
					    	tdCls: 'tdAction',         			
			            	menuDisabled: true, sortable: false,            		        
							renderer: function(value, p, record){if (record.get('fl_cli_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
							
					  }, 
		             {
		                header   : 'Cliente',
		                dataIndex: 'TDDCON', 
		                flex: 1
		                , filter: {type: 'string'}, filterable: true
		             },
		             <?php if($m_params->from_overflow != 'Y'){?>			        
		             {
		                header   : 'Localit&agrave;',
		                dataIndex: 'localita', 
		                flex: 1
		                , filter: {type: 'string'}, filterable: true
		             },	
		             <?php }?>		        
		             {
		                header   : 'Riferimento',
		                dataIndex: 'TDVSRF', 
		                width     : 110
		                , filter: {type: 'string'}, filterable: true
		             }, {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Ordini bloccati', dataIndex: 'fl_bloc', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';			    				    	
    			    	}},	
    			    	
    			    	
			    	    {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', 
			    	    		dataIndex: 'art_mancanti', menuDisabled: true, sortable: false,
			    	    	    tooltip: 'Ordini MTO',    	    		    
			    			    renderer: function(value, p, record){
			    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
			    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
			    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=18>';
			    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
			    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';			    	
			    			    	}},	
			    			    	
    			    	<?php if($m_params->from_overflow != 'Y'){?>		    	
			    	    {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
			        			dataIndex: 'art_da_prog',
			    	    	    tooltip: 'Articoli mancanti MTS', menuDisabled: true, sortable: false,        					    	     
			    			    renderer: function(value, p, record){
			    			    	if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
			    			    	if (record.get('art_da_prog')==2) return '<img src=<?php echo img_path("icone/48x48/warning_red.png") ?> width=18>';    			    	
			    			    	if (record.get('art_da_prog')==3) return '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=18>';
			    			    	}},    			    	
    			    	
    			    	<?php }?>
					 {text: '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Consegna confermata', dataIndex: 'fl_cons_conf', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    	if (record.get('fl_cons_conf')==1) return '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=18>';
    			    	}},  
    			    	
    			    	<?php if($m_params->from_overflow == 'Y'){?>
    			    	 {
        		                header   : 'Data',
        		                dataIndex: 'TDODRE', 
        		                width     : 70,
        		                renderer: date_from_AS
        		             },	
    			    	<?php }?>  			    	
    			    	
    			    			        
		             {
		                header   : 'Ordine',
		                dataIndex: 'ordine_out', 
		                width     : 110
		                , filter: {type: 'string'}, filterable: true
		             },			        
		             {
		                header   : 'Tp', tooltip: 'Tipo',
		                dataIndex: 'tipoOrd',
		                tdCls: 'tipoOrd', 
		                width     : 30,
		                renderer: function (value, metaData, record, row, col, store, gridView){						
							metaData.tdCls += ' ' + record.get('raggr');										
							return value;			    
						}
						, filter: {type: 'string'}, filterable: true
		             }, 
		             <?php if($m_params->from_overflow != 'Y'){?>		
		             {text: 'Vol.',	width: 60, dataIndex: 'TDVOLU', renderer: floatRenderer2, align: 'right'},
		             <?php }?>
		             {text: 'Prod./<br>Disp.sped.',	width: 65, dataIndex: 'cons_prog', renderer: date_from_AS},
		            
		            <?php if($m_params->from_overflow == 'Y'){?>
    			    	 {
        		                header   : 'Importo',
        		                dataIndex: 'TDTIMP', 
        		                width     : 70,
        		                renderer: floatRenderer2,
        		                align: 'right'
        		             },	
    			    	<?php }?> 
		            
		             {text: 'St.',	width: 30, dataIndex: 'stato', tooltip: 'Stato ordine'},
		             		             		             
		             {text: 'Variante',	width: 100, dataIndex: 'var1' 		<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo ", hidden: true" ?>, filter: {type: 'string'}, filterable: true},
		           
		           <?php if($m_params->from_overflow != 'Y'){?>		
		            
		             {
		             <?php if($gestione_punto_vendita == 'Y'){?>
		           	  	text: 'Legame',	
		             <?php }else{?>
		             	text: 'Proforma',
		             <?php }?>
		             width: 100, 
		             dataIndex: 'proforma'	
		             <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') 
		                 echo ", hidden: true" ?>, 
		                 filter: {type: 'string'}, filterable: true},
		             		        
<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             {text: 'Articolo',		width: 80, dataIndex: 'cod_articolo'},
		             {text: 'Descrizione',	width: 120, dataIndex: 'des_articolo'},
		             {text: 'UM',	width: 40, dataIndex: 'um'},
		             {text: 'Q.t&agrave;',	width: 80, dataIndex: 'qta', renderer: floatRenderer0, align: 'right'},
<?php }?>		             		        
		             		            
		             {text: 'Carico',		width: 90, dataIndex: 'carico', filter: {type: 'string'}, filterable: true},
		            
		            <?php if($gestione_punto_vendita != 'Y'){ ?>
		            
		             {text: 'Rifer.<br/>Scar.',		width: 45, dataIndex: 'rif_scarico', filter: {type: 'string'}, filterable: true},
		             {text: 'Ind.<br/>Prod.',	width: 50, dataIndex: 'indice_rottura', filter: {type: 'string'}, filterable: true},
		             {text: 'Rif.<br/>Prod.',	width: 50, dataIndex: 'TDSELO', filter: {type: 'string'}, filterable: true},
		             {text: 'Lotto',		width: 90, dataIndex: 'lotto', filter: {type: 'string'}, filterable: true},
		             {text: 'Data ATP',		width: 70, dataIndex: 'data_atp', renderer: date_from_AS},
		             {text: 'Consegna<br> richiesta',		width: 70, dataIndex: 'cons_rich', renderer: date_from_AS},
		             {text: 'Priorit&agrave;', width: 50, dataIndex: 'priorita', filter: {type: 'string'}, filterable: true}			             
		         <?php }?>
		         <?php }?>
		         ]	    					
		
		, listeners: {		
	 			afterrender: function (comp) {
	 				//comp.up('window').setWidth(950);
	 				//comp.up('window').setHeight(500);
					//window_to_center(comp.up('window'));
					<?php if($m_params->from_overflow == 'Y'){?>
					comp.up('window').setTitle('<?php echo "Elenco bloccati"; ?>');
					<?php }else{?>
					comp.up('window').setTitle('<?php echo "Elenco ordini"; ?>');	 		
					<?php }?>
				
							
	 			}
	 			
	 			
	 			
		  , itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
		  		
			  	id_selected = grid.getSelectionModel().getSelection();
			  	list_selected_id = [];
			  	for (var i=0; i<id_selected.length; i++) 
				   list_selected_id.push({k_ordine: id_selected[i].get('k_ordine'), liv: 'liv_3'});
		  														  
				  var voci_menu = [];
				
				
<?php if ($m_params->tipo_elenco != 'DA_PLAN' || 1==1 ){ ?>				
				
				  voci_menu.push({
		      		text: 'Assegna prod./dispon. alla spedizione',
		    		iconCls: 'iconSpedizione',
		    		handler: function() {
		    		
		    		
				   //verifiche su ordini selezionati
				    	for (var i=0; i<id_selected.length; i++)
				  		if (parseFloat(id_selected[i].get('fl_evaso')) > 0){
				  					acs_show_msg_error('Operazione non ammessa su ordini evasi');
				  			  		return false;			
				  		}				   
				  		
						<?php if ($m_params->tipo_elenco == 'HOLD'){ ?>				  		
				    	for (var i=0; i<id_selected.length; i++)
				  		if (parseFloat(id_selected[i].get('fl_bloc')) > 1 || parseFloat(id_selected[i].get('fl_cli_bloc')) > 0){
				  					acs_show_msg_error('Operazione non ammessa su clienti/ordini bloccati');
				  			  		return false;			
				  		}				  		
						<?php } ?>	  	
						
						
						<?php if ($cfg_mod_Spedizioni['ASS_SPED_disabilita_verifica_colli_spuntati'] != 'Y'){ ?>  	
						  	//impedisco se ha colli spuntati
						  	for (var i=0; i<id_selected.length; i++){
								  //devo prendere solo livelli ordine o cliente
								  if (parseInt(id_selected[i].data.colli_sped) > 0) {
									  acs_show_msg_error('Operazione non ammessa con colli spuntati');
									  return false;
								  } 	  	 
							}
						<? } ?>	
		    		
					
					   my_listeners = {
        					onSpedConfirmed: function(){
        						//refresh selezione ordini
        						grid.store.load();
        					}
        				};
					
		    			acs_show_win_std('Loading...', 'acs_form_json_assegna_spedizione.php', 
		    				{list_selected_id: list_selected_id, cod_iti: grid.getStore().proxy.extraParams.cod_iti, grid_id: grid.id}, 
		    				1100, 600, my_listeners, 'icon-shopping_cart_gray-16');
		    		}
				  });				
				
				
				
				
				
				  voci_menu.push({
		      		text: 'Assegna prod./dispon. alla spedizione (con conferma data)',
		    		iconCls: 'iconConf',
		    		handler: function() {
		    		
		    		
				   //verifiche su ordini selezionati
				    	for (var i=0; i<id_selected.length; i++)
				  		if (parseFloat(id_selected[i].get('fl_evaso')) > 0){
				  					acs_show_msg_error('Operazione non ammessa su ordini evasi');
				  			  		return false;			
				  		}				   
				  		
						<?php if ($m_params->tipo_elenco == 'HOLD'){ ?>				  		
				    	for (var i=0; i<id_selected.length; i++)
				  		if (parseFloat(id_selected[i].get('fl_bloc')) > 1 || parseFloat(id_selected[i].get('fl_cli_bloc')) > 0){
				  					acs_show_msg_error('Operazione non ammessa su clienti/ordini bloccati');
				  			  		return false;			
				  		}				  		
						<?php } ?>	  	
						
						
						<?php if ($cfg_mod_Spedizioni['ASS_SPED_disabilita_verifica_colli_spuntati'] != 'Y'){ ?>  	
						  	//impedisco se ha colli spuntati
						  	for (var i=0; i<id_selected.length; i++){
								  //devo prendere solo livelli ordine o cliente
								  if (parseInt(id_selected[i].data.colli_sped) > 0) {
									  acs_show_msg_error('Operazione non ammessa con colli spuntati');
									  return false;
								  } 	  	 
							}
						<? } ?>	
				    	
		    		
		    		
		    		
					
					   my_listeners = {
        					onSpedConfirmed: function(){
        						console.log('onSpedConfirmed 111 ---------');
        						grid.up('window').close();
								m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								m_grid.getStore().treeStore.load();        						
        						 
        					}
        				};
					        					
					
		    			acs_show_win_std('Loading...', 'acs_form_json_assegna_spedizione.php', 
		    				{
		    					list_selected_id: list_selected_id, 
		    					cod_iti: grid.getStore().proxy.extraParams.cod_iti, 
		    					grid_id: grid.id,
		    					confermaData: 'Y'}, 
		    				1100, 600, my_listeners, 'icon-shopping_cart_gray-16');
		    		}
				  });					
				
				
				
				
				
				

<?php if ($m_params->tipo_elenco == 'DAY') { ?>
				
				  voci_menu.push({
		      		text: 'Assegna nuovo numerco carico',
		    		iconCls: 'iconCarico',
		    		handler: function() {		    				    		
						///show_win_assegna_carico(grid);
						
	                   	my_listeners = {
        					beforeClose: function(){	
        						//dopo che ha chiuso la maschera del carico aggiorno la pagina del day
        						grid.up('window').close();
								m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								m_grid.getStore().treeStore.load();        						        						
				        		}
		    				};		
												
						
						
						
					  	id_selected = grid.getSelectionModel().getSelection();
					  	list_selected_id = [];
					  	for (var i=0; i<id_selected.length; i++) 
						   list_selected_id.push(id_selected[i].get('k_ordine'));

					  			for (var i=0; i<id_selected.length; i++)						   
								  if (
								  
						  		  		//se non sono un amministratore di modulo segnalo anomalia su ordine bloccato
						  		  		<?php if ($js_parameters->p_mod_param != 'Y'){ ?>
								  			parseInt(id_selected[i].get('fl_bloc')) >=1 ||		//ordine bloccato						  		  			parseFloat(id_selected[i].get('fl_bloc')) > 1 ||
						  		  		<?php } ?>
								  										  
								  		parseInt(id_selected[i].get('fl_cli_bloc')) >=1  	 //cliente bloccato
								  		
								  	) {
									  acs_show_msg_error('Operazione non ammessa con clienti/ordini bloccati');
									  return false;
								  }

						
						acs_show_win_std('Assegna nuovo numero carico', 'acs_form_json_assegna_carico.php', {data: id_selected[0].get('cons_prog'), by_sped_car: 'by_ORD', selected_id: list_selected_id}, 600, 450, my_listeners, 'iconCarico')
					}	
				  });				
				
<?php } ?>				
				
<?php  } // != 'DA_PLAN' ?>
				
				
				
<?php if ($m_params->tipo_elenco == 'DA_PLAN' || $m_params->tipo_elenco == 'DA_INFO'){ ?>				
				
				 //GESTIONE INDICE ROTTURA				
		          	my_listeners = {
							onClose: function(from_win){	
								//dopo che ha chiuso la maschera di assegnazione indice rottura
								from_win.close();						
								grid.store.load();			            
				        		}
		    				};	
				 
				
			      voci_menu.push({
		      		text: 'Assegna indice di produzione',
		    		iconCls : 'icon-sticker_black-16',      		
		    		handler: function() {
		    			acs_show_win_std('Assegna indice di produzione', 
		    	    						'acs_form_json_assegna_indice_rottura.php?fn=gest', 
		    	    						{
		    	    							list_selected_id: list_selected_id
		    	    						}, 600, 500, my_listeners);
		    		}
				  });
				  
				  
				  
			  	id_selected_pedane = grid.getSelectionModel().getSelection();
			  	
			  	list_selected_id_pedane = [];
			  	for (var i=0; i<id_selected_pedane.length; i++) 
				   list_selected_id_pedane.push(id_selected_pedane[i].get('k_ordine'));
				  			  
				  
<?php if ($cfg_mod_Spedizioni['abilita_assegna_posizione']=='Y') { ?> 				  
			      voci_menu.push({
		      		text: 'Assegna posizione',
		    		iconCls : 'icon-sticker_black-16',      		
		    		handler: function() {
		    		
						Ext.Msg.confirm('Richiesta conferma', 'Confermi assegnazione pedane per carichi selezionate?', function(btn, text){																							    
						   if (btn == 'yes'){
				         	Ext.getBody().mask('Retrieving image', 'loading').show();
							Ext.Ajax.request({
							   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_pedane',
							   method: 'POST',
							   jsonData: {
							   	list_selected_id: list_selected_id_pedane
							   }, 
							   
							   success: function(response, opts) {
							   	  Ext.getBody().unmask();
			                      grid.store.load();					                      
							   }, 
							   failure: function(response, opts) {
							      Ext.getBody().unmask();
							      alert('error');
							   }
							});						         	
						   
						   }
						});		    		
		    		
		    		}
				  });
				  
				  
			      voci_menu.push({
		      		text: 'Modifica posizione',
		    		iconCls : 'icon-sticker_black-16',      		
		    		handler: function() {
		    		
		    			if (list_selected_id_pedane.length > 1){
							acs_show_msg_error('Operazione ammessa su singolo ordine');
							  return false;		    	
						}	
		    		
							acs_show_win_std('Modifica posizione', 
		    	    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_modifica_posizione', 
		    	    					{
		    	    						k_ordine: list_selected_id_pedane[0]
		    	    					}, 500, 200, my_listeners);   		
		    		
		    		}
				  });					  
				  
				  
<?php } ?>				  			
			      
				  				
				  
<?php } ?>				  
				
				
			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				}).showAt(event.xy);				
			  }	
		 			
	 		, celldblclick: {								
				  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				  	col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
				  	rec = iView.getRecord(iRowEl);	
				 
				  	if (col_name == 'art_mancanti'){
				  	    iEvent.preventDefault();												
						show_win_art_critici(rec.get('k_ordine'), 'MTS');
						return false;				
					}
				  	
				  	}
				  					  	
		    }
	 			
	        , itemclick: function(view,rec,item,index,eventObj) {
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');  
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');      	

	        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdi.store.load();	        	
	  			wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdc.store.load();
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				      //wd.setSource(src.dettagli);
				   }
				});
				
				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}				
				
	         }	 			
	 			
	 			 			
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {		        	
		         }   
		    }
		    
		
		, dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-print-32',
                     text: 'Stampa',
			            handler: function() {
			            Ext.ux.grid.Printer.print(this.up('grid'));
			            //window.open('acs_print_seleziona_ordini_INFO.php?fn=open_report&form_values=<?php echo acs_je($m_params) ?>');
	        				
	        				
			            }
			     }, 
			     
			     <?php if($gestione_punto_vendita != 'Y'){?>
			     {
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-delivery-32',
                     //text: 'Riepilogo spedizioni',
			            handler: function() {
			            
			            var grid = this.up('grid');
			            all_rows =   grid.getStore().getRange();
                        list_rows = [];
                        
                        Ext.each(all_rows, function(item) {
    							// add the fields that you want to include
    						var Obj = {
       							 k_carico: item.get('k_carico')
    								};
    					
   						list_rows.push(Obj); // push this to the array
						}, this);
	
	
						var list_unique=[];
							
							function get_unique_from_array_object(array,property){
    							var unique = {};
    							var distinct = [];
    							for( var i in array ){
       								if( typeof(unique[array[i][property]]) == "undefined"){
          								distinct.push(array[i]);
      								 }
       							unique[array[i][property]] = 0;
    									}
    							return distinct;
										}


                         list_unique=get_unique_from_array_object(list_rows,'k_carico');
	   		          
	   		          //passare il tipo elenco
	   		          
	        			    acs_show_win_std('Carichi per data','acs_riepilogo_spedizioni_carichi.php?', 
	        			    {list_rows:list_unique, tipo_elenco: 'INFO_PLAN' 
	        			    
	        			    <?php if ($m_params->tipo_elenco == 'DA_PLAN'){ ?>	
	        			    
	        			   , solo_plan: 'PLAN'
	        			   
	        			   <?php }?>
	        			    }, 800, 400, {}, 'icon-delivery-16');
			            }
			     }
			     <?php }?>
			     ]
		   }]



	}
}


