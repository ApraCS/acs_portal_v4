<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

?>


<?php 

if ($_REQUEST['fn'] == 'get_json_data_top'){
	
	
	$m_params = acs_m_params_json_decode();
	$p_open_request = $m_params->open_request->open_request->form_values;
	$p_top_request = $m_params->open_request->top_request;
	
	$code = $p_top_request->code;
	$code_d = $p_top_request->code_d;
	$value = $p_top_request->value;
	$top = $p_top_request->top;
	
	$field = "COUNT(*) AS NR_RIGHE, SUM(RDQTA2) AS IMPORTO, $code AS CODICE,  $code_d AS DESCRIZIONE";
	$field_group_by = "$code , $code_d";
	$field_order_by = $value  == 'nr_righe' ? 'NR_RIGHE ': 'IMPORTO';
	
	$m_where = "TDOTID = 'VO' AND TDOINU = '' AND TDOADO = 0 AND TDSWPP = 'R'";
	$m_where .= sql_where_by_combo_value("TDASPE", $p_open_request->area_spedizione);
	$m_where .= sql_where_by_combo_value("TDCCON", $p_open_request->f_cliente_cod);
	$m_where .= sql_where_by_combo_value("TDCITI", $p_open_request->itinerario_spedizione);
	$m_where .= sql_where_by_combo_value("TDCAG1", $p_open_request->agente);
	
	
	//totale
	$sql_totale = "SELECT COUNT(*) AS NR_RIGHE_TOT, SUM(RDQTA2) AS IMPORTO_TOT
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
			INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
		WHERE  $m_where";
	
	$stmt = db2_prepare($conn, $sql_totale);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$r = db2_fetch_assoc($stmt);
	$importo_tot = $r['IMPORTO_TOT'];
	$nr_righe_tot = $r['NR_RIGHE_TOT'];
	

	$sql = "SELECT $field
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
			INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD  
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
		WHERE  $m_where
		GROUP BY $field_group_by
		ORDER BY $field_order_by DESC
		FETCH FIRST {$top} ROWS ONLY ";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);

	$data = array();
	$importo_top = 0;
	$nr_righe_top = 0;

	while ($r = db2_fetch_assoc($stmt)){
		$importo_top += $r['IMPORTO'];
		$nr_righe_top += $r['NR_RIGHE'];
		
		array_push($data, array(
				'codice' => $r['CODICE'],
				'descrizione'=> acs_u8e($r['DESCRIZIONE']),
				'importo' => $r['IMPORTO'],
				'nr_righe' => $r['NR_RIGHE'],
				'perc' => $value  == 'nr_righe' ? $r['NR_RIGHE']/$nr_righe_tot  : $r['IMPORTO']/$importo_tot 
		));
	}

	//altro
	array_push($data, array(
			'descrizione'=> 'ALTRO',
			'importo' => $importo_tot - $importo_top,
			'nr_righe' => $nr_righe_tot - $nr_righe_top,
			'perc' => ($importo_tot - $importo_top)/$importo_tot
	));
	
	
	echo  acs_je($data);
	exit;
	
}

?>


{"success":true, "items": [

        {
            xtype: 'container',
            title: 'grafici resi',
            layout: {type: 'vbox', align: 'stretch', pack : 'start', padding:'5px'},
			items: [
			
				{
					xtype: 'grid'
					, flex: 2
					, features: [{
							ftype: 'summary'
					}]
					, title: ''
					, store: {
						xtype: 'store',
						autoLoad:true,
						proxy: {
							url: 'acs_resi_grafici_top.php?fn=get_json_data_top'
							, method: 'POST'
							, type: 'ajax'
							, extraParams: {open_request: <?php echo acs_raw_post_data(); ?>}
		
	                        /* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
	                        , doRequest: personalizza_extraParams_to_jsonData
							
							//Add these two properties
							, actionMethods: {read: 'POST'}
							, reader: {type: 'json', method: 'POST', root: 'root'}
		
						},
						fields: [ 
							{name: 'codice', type: 'string'},
							{name:'descrizione', type: 'string'},
							{name:'perc', type: 'string'},
							{name:'importo', type: 'float'},
							{name:'nr_righe', type: 'float'}
						]
					}
					, columns: [
							{
								header: 'Codice',
								dataIndex: 'codice',
								flex: 1
							},{
								header: 'Descrizione',
								dataIndex: 'descrizione',
								flex: 2,
								summaryType: 'sum', 
								summaryRenderer: function(value, summaryData, dataIndex) {
                              				return 'TOTALE GENERALE'; 
                              	}	
							}, {
								header: '%',
								dataIndex: 'perc',
								flex: 1,
								renderer: perc2
							},{
								header: 'Importo',
								dataIndex: 'importo',
								flex: 1,
								align: 'right',
								renderer: floatRenderer2,
								summaryType: 'sum', 
								summaryRenderer: function(value, summaryData, dataIndex) {
                              				return floatRenderer2(value); 
                              	}	
							},{
								header: 'Nr righe',
								dataIndex: 'nr_righe',
								flex: 1,
								align: 'right',
								renderer: floatRenderer0,
								summaryType: 'sum', 
								summaryRenderer: function(value, summaryData, dataIndex) {
                              				return floatRenderer0(value); 
                              	}	
							}
					]
				
				}
			
			]
			  
		 }
		  
  	]
	  
}

