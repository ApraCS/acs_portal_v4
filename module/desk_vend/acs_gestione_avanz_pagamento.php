<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'exe_cambia_paga'){
    $ret = array();
	$m_params = acs_m_params_json_decode();
	
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} 
            SET TDFG06='{$m_params->form_values->f_avanz_paga}' 
            WHERE TDDT = '{$id_ditta_default}' AND TDDOCU= '{$m_params->k_ordine}'";
	    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);	

	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}




if ($_REQUEST['fn'] == 'open_tab'){

	$m_params = acs_m_params_json_decode();
	$row =  $s->get_ordine_by_k_ordine($m_params->k_ordine);
		
	?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: true,
		            listeners: {		
	 					afterrender: function (comp) {
    	 					comp.up('window').setTitle('Avanzamento pagamento ordine <?php echo "{$row['TDOADO']}_{$row['TDONDO']}_{$row['TDOTPD']}"; ?>');	 	
    	 					}
	 			    },
					buttons: ['->'
			         
			         , {
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var loc_win = this.up('window');
			            	
			          			if (!form.isValid()) return false;
							
								Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cambia_paga',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    k_ordine : <?php echo acs_je($m_params->k_ordine); ?>,
 			        			    form_values: form.getValues()
 								},							        
 						        success : function(result, request){
 						        	var jsonData = Ext.decode(result.responseText);
 						        	loc_win.fireEvent('afterOkSave', loc_win);
		 			            		 
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
		 			
		
			
			            }
			         }
					 ], items: [
					
						{
							name: 'f_avanz_paga',
							xtype: 'combo',
							fieldLabel: 'Avanzamento pagamento',
							labelWidth : 150,
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',		
						    value : <?php echo j($row['TDFG06']); ?>,			   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								      <?php echo acs_ar_to_select_json($s->find_TA_std('SACAR'), ""); ?>	
								    ]
							}
							
							
												 
						}
						
				
										 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}

	
	<?php
	exit; 

	
	
}
