<?php

require_once "../../config.inc.php";

	$s = new Spedizioni();
	$main_module = new Spedizioni();
	
	set_time_limit(240);	

if ($_REQUEST['fn'] == 'open_report'){
	

	$ar_email_to = array();
	$ar_email_to[] = array(trim($itinerario->rec_data['TAMAIL']), "ITINERARIO " . j(trim($itinerario->rec_data['TADESC'])) . " (" .  trim($itinerario->rec_data['TAMAIL']) . ")");
	$ar_email_to[] = array(trim($vettore->rec_data['TAMAIL']), "VETTORE " . j(trim($vettore->rec_data['TADESC'])) . " (" .  trim($vettore->rec_data['TAMAIL']) . ")");	
	
	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}

	$ar_email_json = acs_je($ar_email_to);

	if ($is_linux == 'Y')
		$_REQUEST['form_values'] = strtr($_REQUEST['form_values'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	
	
	$filter = (array)json_decode($_REQUEST['form_values']);

	
		$filtro = array();
		$filtro["cliente"] = $filter['f_cliente_cod'];

		if (strlen($filter['f_destinazione_cod']) > 0)		
			$filtro["destinazione"] = $filter['f_destinazione_cod'];			
		
		$filtro["riferimento"] = $filter['f_riferimento'];
		$filtro["agente"] = $filter['f_agente'];
		
		//data programmata
		if (strlen($filter['f_data_dal']) > 0)
			$filtro["data_dal"] = date('Ymd', strtotime($filter['f_data_dal']));		
		if (strlen($filter['f_data_al']) > 0)
			$filtro["data_al"] = date('Ymd', strtotime($filter['f_data_al']));		

		//data spedizione
		if (strlen($filter['f_data_sped_dal']) > 0)
			$filtro["data_sped_dal"] = date('Ymd', strtotime($filter['f_data_sped_dal']));
		if (strlen($filter['f_data_sped_al']) > 0)
			$filtro["data_sped_al"] = date('Ymd', strtotime($filter['f_data_sped_al']));
		
		
		//data rilascio
		if (strlen($filter['f_data_ril_dal']) > 0)
			$filtro["data_ril_dal"] = date('Ymd', strtotime($filter['f_data_ril_dal']));
		if (strlen($filter['f_data_ril_al']) > 0)
			$filtro["data_ril_al"] = date('Ymd', strtotime($filter['f_data_ril_al']));		
		
		//AREA
		if (strlen(trim($filter['f_area'])) > 0)
			$filtro['area'] = $filter['f_area'];

		//data ricezione (dal, al)
		if (strlen($filter['f_data_ricezione_dal']) > 0)
			$filtro["data_ricezione_dal"] = date('Ymd', strtotime($filter['f_data_ricezione_dal']));
		if (strlen($filter['f_data_ricezione_al']) > 0)
			$filtro["data_ricezione_al"] = date('Ymd', strtotime($filter['f_data_ricezione_al']));
		
		//data conferma (dal, al)
		if (strlen($filter['f_data_conferma_dal']) > 0)
			$filtro["data_conferma_dal"] = date('Ymd', strtotime($filter['f_data_conferma_dal']));
		if (strlen($filter['f_data_conferma_al']) > 0)
			$filtro["data_conferma_al"] = date('Ymd', strtotime($filter['f_data_conferma_al']));		
		
		
		if (strlen($filter['f_data_ricezione']) > 0)
			$filtro["data_ricezione"] = date('Ymd', strtotime($filter['f_data_ricezione']));		
		if (strlen($filter['f_data_conferma']) > 0)
			$filtro["data_conferma"] = date('Ymd', strtotime($filter['f_data_conferma']));
		
		//data disponibilita (dal, al)
		if (strlen($filter['f_data_disp_dal']) > 0)
			$filtro["data_disp_dal"] = date('Ymd', strtotime($filter['f_data_disp_dal']));
		if (strlen($filter['f_data_disp_al']) > 0)
			$filtro["data_disp_al"] = date('Ymd', strtotime($filter['f_data_disp_al']));
						
		//data evasione richiesta (dal, al)
		if (strlen($filter['f_data_ev_rich_dal']) > 0)
			$filtro["data_ev_rich_dal"] = date('Ymd', strtotime($filter['f_data_ev_rich_dal']));
		if (strlen($filter['f_data_ev_rich_al']) > 0)
			$filtro["data_ev_rich_al"] = date('Ymd', strtotime($filter['f_data_ev_rich_al']));
		
		$filtro['carico_assegnato'] = $filter['f_carico_assegnato'];
		$filtro['lotto_assegnato'] 	= $filter['f_lotto_assegnato'];
		$filtro['proforma_assegnato'] 	= $filter['f_proforma_assegnato'];
		
		$filtro['collo_disp'] = $filter['f_collo_disp'];		
		$filtro['collo_sped'] = $filter['f_collo_sped'];		
		
		$filtro['anomalie_evasione']= $filter['f_anomalie_evasione'];				
		$filtro['ordini_evasi'] 	= $filter['f_ordini_evasi'];		
		$filtro['divisione'] 		= $filter['f_divisione'];
		$filtro['modello'] 			= $filter['f_modello'];		
		$filtro['num_ordine'] 		= $filter['f_num_ordine'];
		$filtro['num_carico'] 		= $filter['f_num_carico'];
		$filtro['num_lotto'] 		= $filter['f_num_lotto'];				
		$filtro['num_proforma'] 	= $filter['f_num_proforma'];
		$filtro['indice_rottura'] 	= $filter['f_indice_rottura'];
		$filtro['indice_rottura_assegnato'] 	= $filter['indice_rottura_assegnato'];
		$filtro['num_sped'] 		= $filter['f_num_sped'];
		$filtro['conf'] 		    = $filter['f_conf'];
		$filtro['itinerario'] 		= $filter['f_itinerario'];		
		$filtro['tipologia_ordine'] = $filter['f_tipologia_ordine'];
		$filtro['stato_ordine'] 	= $filter['f_stato_ordine'];		
		$filtro['priorita'] 		= $filter['f_priorita'];		
		$filtro['solo_bloccati']	= $filter['f_solo_bloccati'];
		$filtro['confermati']		= $filter['f_confermati'];
		$filtro['articolo']		    = $filter['f_articolo'];
		$filtro['hold']	            = $filter['f_hold'];
		
		$filtro['escludi_filtro_DP'] = 'Y'; //voglio anche le spedizioni DP
		$stmt 	= $s->get_elenco_ordini($filtro, 'N', 'search');
		//$stmt 	= $s->get_elenco_ordini($filtro, 'N', '', $_REQUEST['f_sceltadata'], false, 'Y');	//con 'search' ordinava per area/itin, qui vogliono invece per data
	
		
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   <?php @include '../../personal/report_css_logo_cliente.php'; ?>
  
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   table.int1 th.int_data{font-weight: bold; font-size: 13px;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
   tr.ag_liv2 td{background-color: #DDDDDD;}   
   tr.ag_liv_el_cliente td{background-color: #ffffff; font-weight: bold;}   
   tr.ag_liv_el_carico td{background-color: #cccccc;}  
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white; font-size: 18px;} 
   tr.liv_int th{background-color: #cccccc; font-weight: bold;}
   tr.ag_liv_area th{background-color: #b0b0b0; color: black;}
   tr.ag_liv1{background-color: #f3f3f3;}
   tr.ag_liv1 td{font-weight: bold;}
   span.sceltadata{font-size: 0.6em; font-weight: normal;}
   span.denominazione_cliente{font-weight: bold;}   
   
   h2.acs_report_title{font-size: 18px; padding: 10px;}
   
   //div#my_content h2{font-size: 1.6em; padding: 7px;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<div id='my_content'>  
 
<?php


echo "<h2 class='acs_report_title'> Elenco ordini </h2>";



$_REQUEST['el_dettaglio_per_cliente'] = 'Y';
$_REQUEST['el_dettaglio_per_ordine'] = 'Y';
$_REQUEST['dettaglio_per_carico'] = 'N';
$_REQUEST['mostra_importo'] = 'Y';
 



$ar_tot["TOTALI"] = array();

while ($row = db2_fetch_assoc($stmt)) {
	
    
    $nr = array();
    $nr['TDDCON'] 		= acs_u8e(trim($row['TDDCON']));
    $nr['localita'] 	= acs_u8e(trim($s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD'])));
    $nr['TDVSRF']		= acs_u8e(trim($row['TDVSRF']));
    $nr['k_ordine']		= trim($row['TDDOCU']);
    $nr['ordine_out']	= $s->k_ordine_out($row['TDDOCU']);
    $nr['tipoOrd']		= trim($row['TDOTPD']);
    $nr['fl_cli_bloc']	= trim($row['TDFN02']);
    $nr['fl_evaso']		= trim($row['TDFN11']);
    $nr['fl_bloc']		= $s->get_ord_fl_bloc($row);
    $nr['cons_prog']	= $row['TDDTEP'];
    $nr['data_atp']	    = $row['TDDATP'];
    $nr['cons_rich']	= $row['TDODER'];
    $nr['priorita']     = acs_u8e($row['TDOPRI']);
    $nr['colli_sped'] 	= $row['TDCOSP'];
    $nr['proforma'] 	= trim($row['TDPROF']);
    $nr['stato'] 		= trim($row['TDSTAT']);
    $nr['raggr'] 		= trim($row['TDCLOR']);
    $nr['var1']			= acs_u8e(trim($row['TDDVN1']));
    $nr['k_carico']		= $s->k_carico_td($row);
    $nr['liv']			= 'liv_2'; //serve per assegna carico
    $nr['fl_cons_conf'] = $row['TDFN06'];
    if ((int)$row['TDNRCA'] > 0)
        $nr['carico'] 		= implode("_", array($row['TDAACA'], trim($row['TDNRCA'])));
        
    if ((int)$row['TDNRLO'] > 0)
        $nr['lotto'] 		= implode("_", array($row['TDAALO'], trim($row['TDNRLO'])));
        
    $nr['rif_scarico']		= trim(acs_u8e($row['TDRFCA']));
    $nr['indice_rottura'] = trim($row['TDIRLO']);
    $nr['fl_art_manc'] = $s->get_fl_art_manc($row);
    $nr['art_da_prog'] = $s->get_art_da_prog($row);
    $nr['TDVOLU'] 	   = $row['TDVOLU'];
    $nr['TDSELO'] 	   = trim($row['TDSELO']);
        
    if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
        $nr['cod_articolo'] = trim(acs_u8e($row['RDART']));
        $nr['des_articolo'] = trim(acs_u8e($row['RDDES1']));
        $nr['um'] 			= trim(acs_u8e($row['RDUM']));
        $nr['qta'] 			= trim(acs_u8e($row['RDQTA']));
    }
            
            $data[] = $nr;
							

} //while


//echo "<pre>";print_r($ar);exit;





echo "<table class=int1>";


echo "<tr class = liv_int>
 <th><img src=" . img_path("icone/48x48/divieto.png") . " height=20></th>
 <th>Cliente</th>
 <th>Localit&agrave;</th>
 <th>Riferimento</th>
 <th><img src=" . img_path("icone/48x48/power_black.png") . " height=20></th>
 <th><img src=" . img_path("icone/48x48/info_blue.png") . " height=20></th>
 <th><img src=" . img_path("icone/48x48/info_black.png") . " height=20></th>
 <th><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=20></th>
 <th>Ordine</th>
 <th>Tp</th>
 <th>Vol.</th>
 <th>Prod./<br>Disp.sped.</th>
 <th>St.</th>
 <th>Variante</th>";
if($gestione_punto_vendita == 'Y')
    echo "<th>Legame</th>";
else
    echo "<th>Proforma</th>";

if($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
    echo "<th>Articolo</th>
          <th>Descrizione</th>
          <th>UM</th>
          <th>Q.t&agrave;</th>";
    
}    
    
echo "<th>Carico</th>";
if($gestione_punto_vendita != 'Y'){ 
    echo "<th>Rifer.<br/>Scar.</th>
          <th>Ind.<br/>Prod.</th>
          <th>Rif.<br/>Prod.</th>
          <th>Lotto</th>
          <th>Data ATP</th>
          <th>Consegna<br> richiesta</th>
          <th>Priorit&agrave;</th>";	
}
echo "</tr>";



foreach ($data as $kgg => $gg){

  echo "<tr>";
  if($gg['fl_cli_bloc'] > 0){
      echo "<td><img src=" . img_path("icone/48x48/divieto.png") . " height=15></td>";
  }else{
      echo "<td>&nbsp;</td>";
  }
  
  echo "<td>" . $gg['TDDCON'] . "</td>
        <td>" . $gg['localita'] . "</td>
        <td>" . $gg['TDVSRF'] . "</td>";
  get_img_flag($gg);
  echo "<td>" . $gg['ordine_out'] . "</td>
        <td>" . $gg['tipoOrd'] . "</td>
        <td class = number >" . n($gg['TDVOLU'],2) . "</td>
        <td>" . print_date($gg['cons_prog']) . "</td>
        <td>" . $gg['stato'] . "</td>
        <td>" . $gg['var1'] . "</td>";
   
  echo "<td>" . $gg['proforma'] . "</td>";
  if($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
      echo "<td>" . $gg['cod_articolo'] . "</td>";
      echo "<td>" . $gg['des_articolo'] . "</td>";
      echo "<td>" . $gg['um'] . "</td>";
      echo "<td>" . n($gg['qta'],2) . "</td>";
      
  }
   echo "<td>" . $gg['carico'] . "</td>";
  	
   if($gestione_punto_vendita != 'Y'){ 
       
       echo "<td>" . $gg['rif_scarico'] . "</td>";
       echo "<td>" . $gg['indice_rottura'] . "</td>";
       echo "<td>" . $gg['TDSELO'] . "</td>";
       echo "<td>" . $gg['lotto'] . "</td>";
       echo "<td>" . print_date($gg['data_atp']) . "</td>";
       echo "<td>" . print_date($gg['cons_rich']) . "</td>";
       echo "<td>" . $gg['priorita'] . "</td>";
       
       
   }
   
   
  echo "</tr>";
	 
	}

echo "</table></div></body></html>";
	
}	


function get_img_flag($r){
    
    if($r['fl_bloc'] == 4){
        echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
    }else if($r['fl_bloc'] == 3){
        echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
    }elseif($r['fl_bloc'] == 2){
        echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
    }elseif($r['fl_bloc'] == 1){
        echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
    
    
    if($r['fl_art_manc'] == 5){
        echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
    }else if($r['fl_art_manc'] == 20){
        echo "<td><img src=" . img_path("icone/48x48/shopping_cart_red.png") . " height=15></td>";
    }else if($r['fl_art_manc'] == 4){
        echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
    }else if($r['fl_art_manc'] == 3){
        echo "<td><img src=" . img_path("icone/48x48/shopping_cart.png") . " height=15></td>";
    }elseif($r['fl_art_manc'] == 2){
        echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
    }elseif($r['fl_art_manc'] == 1){
        echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
    
    if($r['art_da_prog'] == 3){
        echo "<td><img src=" . img_path("icone/48x48/warning_blue.png") . " height=15></td>";
    }else if($r['art_da_prog'] == 2){
        echo "<td><img src=" . img_path("icone/48x48/warning_red.png") . " height=15></td>";
    }elseif($r['art_da_prog'] == 1){
        echo "<td><img src=" . img_path("icone/48x48/info_black.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
   

    if($r['fl_cons_conf'] > 0){
        echo "<td><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
    
    
    
    
}


?>		