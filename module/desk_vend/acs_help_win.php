<?php
require_once "../../config.inc.php";
$main_module = new Spedizioni();
$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());

$sql = "SELECT * FROM {$cfg_mod_Admin['file_help']} WHERE HEMODU=? AND HEFUNC=?";
$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, array($main_module->get_cod_mod(), $all_params['codice']));
$row = db2_fetch_assoc($stmt);

if ($row==false){
	$value = '';
} else {
	$value = $row['HEMEMO'];
}

?>

<?php if ($_REQUEST['fn'] == 'open_form'){ ?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_memo',
           	layout: {
            	type: 'vbox',
            	align: 'stretch'  // Child items are stretched to full width
        	}, 

        	listeners: {		
 				afterrender: function (comp) {
 					win = comp.up('window');
 					win.setTitle(win.title + ' ' + <?php echo j(" [{$all_params['codice']}]");  ?> );
 				}
 			},	
        	
            items: [{
	        	xtype: 'hidden',
	        	name: 'codice',
	        	value: <?php echo j($all_params['codice']) ?>
	        	}
	        	
	        	, {
		            xtype: 'htmleditor',
		            name: 'f_text',
		            fieldLabel: '',
		            layout: 'fit',
		            anchor: '96%',
		            height: '100%',
				    allowBlank: false,
				    value: <?php echo j($value)?>,
				    flex: 1, //riempie tutto lo spazio
					hideLabel: false,
		            style: 'margin:0' // Remove default margin

		            
<? if (array_search(trim($auth->get_user()), explode(";", $cfg_mod_Admin['user_help_modify'])) === false) { ?>		            		            
		            ,enableFont: false
		            ,enableFormat: false
		            ,enableAlignments: false
		            ,enableColors: false
		            ,enableFontSize: false
		            ,enableLinks: false
		            ,enableSourceEdit: false
		            ,enableLists: false
<?php } ?>		            
		            		              
		        }
		        

			],
			
<? if (array_search(trim($auth->get_user()), explode(";", $cfg_mod_Admin['user_help_modify'])) !== false) { ?>
			buttons: [{
			            text: 'Salva',
			            scale: 'large',
			            iconCls: 'icon-save-32',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var m_win = this.up('window');	            	
			                form.submit(
										{
				                            //waitMsg:'Loading...',
				                            success: function(form,action) {		                            	
												m_win.close();		        										                    	
				                            },
				                            failure: function(form,action){
				                                //Ext.MessageBox.alert('Erro');
				                            }
				                        }	                	
			                );            	                	                
			             }			            
			          }]
<?php  } ?>			          
			            
			
			
			
		}
	]
}
<?php exit; }?>
<?php if ($_REQUEST['fn'] == 'save_memo'){

	//salvo le modifiche
	$sql = "SELECT * FROM {$cfg_mod_Admin['file_help']} WHERE HEMODU=? AND HEFUNC=?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($main_module->get_cod_mod(), $all_params['codice']));
	$row = db2_fetch_assoc($stmt);	

	if ($row==false){
		//insert
		$sql_EXE = "INSERT INTO {$cfg_mod_Admin['file_help']}(HEMODU, HEFUNC, HEMEMO) VALUES(?,?,?)";
		$stmt_EXE = db2_prepare($conn, $sql_EXE);
		$result_EXE = db2_execute($stmt_EXE, array($main_module->get_cod_mod(), $all_params['codice'], utf8_decode($all_params['f_text'])));		
	} else {
		//update
		$sql_EXE = "UPDATE {$cfg_mod_Admin['file_help']} SET HEMEMO=? WHERE HEMODU=? AND HEFUNC=?";
		$stmt_EXE = db2_prepare($conn, $sql_EXE);
		$result_EXE = db2_execute($stmt_EXE, array(utf8_decode($all_params['f_text']), $main_module->get_cod_mod(), $all_params['codice']));		
	}

	$ret['success'] = true;
	echo acs_je($ret);	
	
exit; } ?>