	        <table width="100%" class="tab-legend">
        	<tr>
        		<th width="25%" class="A">Icone intestazione</th>
        		<th width="25%" class="B">Icone calendario</th>
        		<th width="25%" class="A">Icone avanzamento</th>        		
        		<th width="25%" class="B">Icone ordine</th>        		        		        		
        	</tr>
        	<tr>
        		<td valign=top class="A">
        			<p><img src=<?php echo img_path("icone/48x48/home.png") ?>  >Chiudi e vai alla pagina iniziale</p>
        			<p><img src=<?php echo img_path("icone/48x48/search.png") ?>  >Ricerca ordini</p>
        			<p><img src=<?php echo img_path("icone/48x48/tools.png") ?>  >Parametri di accesso modulo</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/help_black.png") ?>  >Documentazione operativa</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/button_grey_repeat.png") ?>  >Chiusura/aggiornamento base dati</p>        			        			
        			<p><img src=<?php echo img_path("icone/48x48/arrivi.png") ?>  >Attivit&agrave; di acquisizione ordini</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/partenze.png") ?>  >Calendario spedizioni</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/archive.png") ?>  >Tabelle gestionali</p>
        			<p><img src=<?php echo img_path("icone/48x48/currency_black_euro.png") ?>  >Controllo costi trasporto</p>        			        			
        			<p><img src=<?php echo img_path("icone/48x48/print.png") ?>  >Report settimanale</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/clock.png") ?>  >Attiva memo email</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/grafici.png") ?>  >Grafici/Report statistici</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/warning_black.png") ?>  >Riepilogo articoli critici/mancanti</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/calendar.png") ?>  >Calcola spedizione promettibile</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/button_blue_play.png") ?>  >Avvio elaborazioni "batch"</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/rss_blue.png") ?>  >Ultima sincronizzazione dati</p>
        			<p><img src=<?php echo img_path("under-construction_q_48.png") ?>  >Sincronizzazione in corso</p>
        			<p><img src=<?php echo img_path("icone/48x48/button_grey_eject.png") ?>  >Chiudi e scollega utente corrente</p>        			        			        			
        		</td>
        		
        		<td valign=top class="B">
        			<p><img src=<?php echo img_path("icone/48x48/divieto.png") ?>  >Clienti bloccati</p>
        			<p><img src=<?php echo img_path("icone/48x48/power_black.png") ?>  >Ordini bloccati</p>	
        			<p><img src=<?php echo img_path("icone/48x48/warning_black.png") ?>  >Ordini con articoli mancanti</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?>  >Ordini non programmati</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/button_grey_first.png") ?>  >Indietro di un giorno
        			<p><img src=<?php echo img_path("icone/48x48/button_grey_last.png") ?>  >Avanti di un giorno</p>        			        			
        			<p><img src=<?php echo img_path("icone/48x48/button_grey_rew.png") ?>  >Indietro di una settimana</p>
        			<p><img src=<?php echo img_path("icone/48x48/button_grey_ffw.png") ?>  >Avanti di una settimana</p>        				
        			<p><img src=<?php echo img_path("icone/48x48/delivery.png") ?>  >Spedizione programmata</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/filtro_attivo.png") ?>  >Disattiva solo ordini con carico</p>
        			<p><img src=<?php echo img_path("icone/48x48/filtro_disattivo.png") ?> >Attiva solo ordini con carico</p>        				
        			<p><img src=<?php echo img_path("icone/48x48/tras_plan.png") ?> >Calendario trasportatori</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/bandiera_scacchi.png") ?> >Avanzamento evasione carichi</p>        			
        		</td>
        		        		      		
        		<td valign=top class="A">
        			<p><img src=<?php echo img_path("icone/48x48/power_black.png") ?> >Blocco esposizione </p>
        			<p><img src=<?php echo img_path("icone/48x48/power_blue.png") ?>  >Blocco commerciale</p>
        			<p><img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?>  >Blocco esposizione/commerciale</p>
        			<p><img src=<?php echo img_path("icone/48x48/power_gray.png") ?>  >Ordine sbloccato</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/info_blue.png") ?>  >Con articoli MTO mancanti</p>        					
   			        <p><img src=<?php echo img_path("icone/48x48/info_gray.png") ?>  >Con articoli MTO da ordinare</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?>  >Con articoli MTO ordinati</p>        					
   			        <p><img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?>  >Con articoli MTO disponibili</p>
        			<p><img src=<?php echo img_path("icone/48x48/power_black.png") ?>  >Ordine con articoli MTS mancanti</p>
        			<p><img src=<?php echo img_path("icone/48x48/star_none_black.png") ?>  >Spunta colli spediti iniziata</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/star_full_green.png") ?>  >Spunta colli spediti completa</p>
        			<p><img src=<?php echo img_path("icone/48x48/star_none.png") ?>  >Spunta colli disponibili iniziata</p>
        			<p><img src=<?php echo img_path("icone/48x48/star_full_yellow.png") ?>  >Spunta colli disponibili completa</p>        			        			        			
        			<p><img src=<?php echo img_path("icone/48x48/folder_accept.png") ?>  >Carico controllato</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/button_black_play.png") ?>  >Carico rilasciato/avviato</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/lock.png") ?>  >Ordini evasi</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/recycle_bin.png") ?>  >Destinazione con articoli da rendere</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/blog_compose.png") ?>  >Segnalazioni per cliente/destinazioni</p>
        		</td>
        		       		

        		<td valign=top class="B">        		        		
        			<p><img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?>  >Data programmazione confermata</p>
        			<p><img src=<?php echo img_path("icone/48x48/attachment.png") ?>  >Copia ordine web allegata</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/design.png") ?>  >Ordine da immettere</p>        			        			
        			<p><img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?>  >Conferma recente (2 gg max.)</p>
        			<p><img src=<?php echo img_path("icone/48x48/mano_ko.png") ?>  >Ordine non conforme</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/timer_blue.png") ?>  >Ordine con attesa avanzamento</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/link.png") ?>  >Ordine con spedizione abbinata ad altro ordine</p>
        			<p><img src=<?php echo img_path("icone/48x48/bandiera_grey.png") ?>  >Ordine contrassegnato</p>
        			<p><img src=<?php echo img_path("icone/48x48/sticker_black.png") ?>  >Indice rottura</p>
        			<p><img src=<?php echo img_path("icone/48x48/sticker_blue.png") ?>  >Indice rottura/contrassegno</p>
        			<p><img src=<?php echo img_path("icone/48x48/comment_edit.png") ?>  >Aggiungi annotazioni ordine</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?>  >Consulta/modifica annotazioni ordine</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/sub_blue_add.png") ?>  >Con aggiunte da trasmettere</p>        			
        			<p><img src=<?php echo img_path("icone/48x48/sub_grey_add.png") ?>  >Con aggiunte trasmesse</p>        			        			
        			<p><img src=<?php echo img_path("icone/48x48/warning_red.png") ?>  >MRP da calcolare</p>        			
        		</td>        		        		
        		
        	</tr>        	
        	
        </table>