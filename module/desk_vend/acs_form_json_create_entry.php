<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$oggi = oggi_AS_date();

$m_params = acs_m_params_json_decode();

//recupero l'elenco degli ordini interessati
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

foreach ($m_params->list_selected_id as $ar_selected_ord)
	$ar_ord[] = $ar_selected_ord->k_ordine;

//recupere gli ordini
global $conn, $cfg_mod_Spedizioni, $auth;
$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} WHERE TDDOCU IN (" . sql_t_IN($ar_ord) . ")";
$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);

//dal primo ordine prendo (in base all'operazione) come utente assegnato ref. cliente o ordine
$row = db2_fetch_assoc($stmt);

$sql = "SELECT * FROM {$cfg_mod_Admin['file_tabelle']} TA_ATTAV	WHERE TADT=? AND TA_ATTAV.TATAID = 'ATTAV' AND TAKEY1=? ";
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$rows = db2_execute($stmt, array($id_ditta_default, $m_params->tipo_op));
$r_ATTAV = db2_fetch_assoc($stmt);

switch ($r_ATTAV['TAFG01']){
	case "C": //ref. cliente
		$v_utente_assegnato = trim($row['TDSTOE']);
		break;
	case "E": //ENTRAMBI
		$v_utente_assegnato= trim($row_ord['TDUSIO']);
		break;
	case "U": //Utente corrente
	    $v_utente_assegnato= trim($auth->get_user());
	    break;
	default: //ref. ordine
		$v_utente_assegnato = trim($row_ord['TDUSIO']);
}


//se dall'ordine non recupero il referente di default, provo a prendere
//dalla sessione l'ultimo utilizzato per questa operazione
if (strlen($v_utente_assegnato) == 0) {
	if (trim($m_params->tipo_op) == 'IMMOR') {
		if (isset($_SESSION['ultimo_utente_assegnato_' . trim($m_params->tipo_op)]))		
			$v_utente_assegnato = $_SESSION['ultimo_utente_assegnato_' . trim($m_params->tipo_op)];	
	}	

}	



//elenco possibili utenti destinatari
$user_to = array();
$users = new Users;
//default 
$ar_users = $users->find_all();

if ($r_ATTAV['TAFG01'] == 'U')      //ToDo: 
    $ar_users = $users->find_all(); //ToDo: filtrare utenti in base a gruppo attivita'
else if (strlen($m_params->tipo_op) > 0)
	$ar_users = $users->find_all_by_ATTAV($m_params->tipo_op, null, $row); //al momento non gestisco l'area di spedizione	
else if (strlen($m_params->blocco_op) > 0)
	$ar_users = $users->find_all(); //ToDo: filtrare utenti in base a gruppo attivita'

foreach ($ar_users as $ku=>$u)
	$user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");

$ar_users_json = acs_je($user_to);

if(isset($m_params->entry_prog)){
$sql = "SELECT AO.*, NT_MEMO.NTMEMO AS MEMO
        FROM {$cfg_mod_Spedizioni['file_assegna_ord']} AO
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
        ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = AO.ASIDPR AND NT_MEMO.NTSEQU=0
        WHERE ASIDPR = '{$m_params->entry_prog}'";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
$row = db2_fetch_assoc($stmt);
}

?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            defaults:{ anchor: '-10' , labelWidth: 130 },
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'exe_crea_segnalazione_arrivi'
                	}, {
	                	xtype: 'hidden',
	                	name: 'list_selected_id',
	                	value: '<?php echo $list_selected_id_encoded; ?>'
                	}, 
                	
                	<?php if (isset($m_params->entry_prog)){ ?>
                	
                			{
			                	xtype: 'hidden',
			                	name: 'f_entry_prog',
			                	value: <?php echo $m_params->entry_prog; ?>
		                	}, {
			                	xtype: 'hidden',
			                	name: 'f_entry_prog_causale',
			                	value: <?php echo j($m_params->entry_prog_causale); ?>
		                	}, {
								name: 'f_entry_prog_note',
								xtype: 'hidden',
							    value: <?php echo j($m_params->entry_prog_note); ?>							
							},			              

					
					<?php } ?>                	
                	
                	{
						name: 'f_causale',
						xtype: 'combo',
						fieldLabel: 'Causale',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: false,							
					    value: <?php echo j($m_params->tipo_op) ?>,							
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}, 'TAFG04'],
						    data: [								    
							     <?php echo acs_ar_to_select_json(
							     			$s->find_TA_std('ATTAV', $m_params->tipo_op, 'Y', 'Y', null, null, $m_params->blocco_op, 'N', 'Y'),
							     	 	""); ?>	
							    ] 
						},
						typeAhead: true,
    					queryMode: 'local',                             
    					anyMatch: true, 
						listeners   : {
							beforequery: function(record){  
							    record.query = new RegExp(record.query, 'ig');
    							record.forceAll = true;
							},
							select: function(field,newVal) {	
                              		if(field.displayTplData[0].TAFG04 == 'Y'){
                              			utente_assegnato = this.up('window').down('#utente_assegnato');
                                    	utente_assegnato.setValue(<?php echo j(trim($auth->get_user())); ?>);
                                    }
                                }
				
						}								 
					}, {
						name: 'f_note',
						xtype: 'textfield',
						fieldLabel: 'Riferimento',
					    maxLength: 100							
					}, {
			            xtype: 'combo',
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            itemId: 'utente_assegnato',
			            fieldLabel: 'Utente assegnato',
			            queryMode: 'local',
			            selectOnTab: false,
			            name: 'f_utente_assegnato',
			            allowBlank: false,
						forceSelection: true,	
						//value : <?php echo j(trim($auth->get_user())); ?>		
					    value: '<?php echo $v_utente_assegnato; ?>' ,
					            
			          
			        }, {
						xtype: 'checkboxgroup',
						fieldLabel: 'Notifica assegnazione',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_notifica_assegnazione' 
                          , boxLabel: 'Si'
                          , inputValue: 'Y'
                        }]							
					}, {
						xtype: 'checkboxgroup',
						fieldLabel: 'Notifica rilascio',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_notifica_rilascio' 
                          , boxLabel: 'Si'
                          , inputValue: 'Y'
                        }]							
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Attiva dal'
					   , name: 'f_attiva_dal'
					   , format: 'd/m/Y'
					   , value: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'
					   , minValue: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   , width: 240, anchor: 'none'
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Scadenza'
					   , name: 'f_scadenza'
					   , format: 'd/m/Y'
					   , minValue: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: true
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   , width: 240, anchor: 'none'
					} , {
						name: 'f_memo',
						xtype: 'textarea',
						height : 50,
						fieldLabel: 'Memo',
					    maxLength: 100, 
					    anchor: '-5',
					    value : <?php echo j($row['MEMO']); ?>							
					} 
				],
			buttons: [{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window');

					if(form.isValid()){	            	
			                form.submit({
				                            //waitMsg:'Loading...',
				                            success: function(form,action) {
											  if (typeof(window.id_selected)!=='undefined')				                            
						                      for (var i=0; i<window.id_selected.length; i++){
						                      	//per i record che erano selezionato verifico se hanno 
						                      	//adesso il flag di rilasciato (e devo sbarrarli)
						                        
												Ext.Ajax.request({
													url: 'acs_panel_todolist.php?fn=exe_richiesta_record_rilasciato',
											        jsonData: {prog: id_selected[i].get('prog')},
											        method     : 'POST',
											        waitMsg    : 'Data loading',
											        success : function(result, request){
											        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
											        }, scope: id_selected[i],
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });					                        
						                      }
				                            
				                              if (!Ext.isEmpty(window.events.afteroksave)) {
											  	window.fireEvent('afterOkSave', window);
											  }	
											  else {
				            				    window.close();
				            				   }		                    				 
		                    				       	
				                            },
				                            failure: function(form,action){
				                                Ext.MessageBox.alert('Erro');
				                            }
				                        });
				            
				    }            	                	                
	            }
	        }],             
				
        }
]}