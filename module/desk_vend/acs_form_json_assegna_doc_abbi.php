<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_annulla_doc'){
    
    $ret = array();
  
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'ANN_ABB_DOC',
            "k_ordine"	=> $m_params->k_ordine,
            
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit();
}


if ($_REQUEST['fn'] == 'exe_assegna_doc'){
    
    $ret = array();
    $ar = explode("_", $m_params->k_docu);
    
    if ($backend_ERP == 'GL'){
        $sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDABBI = ? WHERE TDDOCU = ?";
        $stmt = db2_prepare($conn, $sql);	
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($m_params->k_docu, $m_params->k_ordine));
        echo db2_stmt_errormsg($stmt);
    }
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'ABB_DOCUM',
            "k_ordine"	=> $m_params->k_ordine,
            "vals" => array(
                "RIAACA" 	=> $ar[0],
                "RINRCA"    => $ar[1],
                "RITPCA" 	=> $ar[2]
            ),
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit();
}

if ($_REQUEST['fn'] == 'get_json_data_ordine'){
    
    $rec = $m_params->rec;

    //recupero codice cliente
    $k_cli_des = $rec->k_cli_des;
    $k_cod_cli_exp = explode("_", $k_cli_des);
    $cod_cli = $k_cod_cli_exp[1];

    $ord = $s->get_ordine_by_k_docu($rec->k_ordine);
    
    $sql = "SELECT *
            FROM  {$cfg_mod_Spedizioni['file_testate']}
            WHERE " . $s->get_where_std() . "
            AND TDCCON='{$cod_cli}' AND TDSTAT NOT IN ('CC','CT')
            AND TDDOCU <> '{$rec->k_ordine}' AND TDDTOR = '{$ord['TDDTOR']}'
            AND TDFN11 <> 1 AND TDFN19 <> 1 /*escludi evasi*/";
    
         
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    $ret = array();
        
    while ($r = db2_fetch_assoc($stmt)) {
        
        $ret[] = array("k_ordine" => trim($r['TDOADO'])."_".trim($r['TDONDO'])."_".trim($r['TDOTPD']),
                       "anno"     => trim($r['TDOADO']),
                       "numero"   => trim($r['TDONDO']),
                       "tipo"     => trim($r['TDOTPD']),
                       "data_reg" => trim($r['TDODRE']),
                       "data_eva" => trim($r['TDDTEP']),
                       "importo"  => trim($r['TDINFI']),
                       "stato"    => trim($r['TDSTAT']),
                       "rife"     => acs_u8e(trim($r['TDVSRF'])),
                       "fl_bloc"  => $s->get_ord_fl_bloc($r)
        );
        
    }
    
            
    echo acs_je($ret);
    exit();
}

if ($_REQUEST['fn'] == 'open_form'){
    
    ?>

				
{"success":true, "items": [

		
        {
        
        xtype: 'grid',
        autoScroll : true,
        features: [	
			{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
		}],	
        store: {
						
			xtype: 'store',
			autoLoad:true,
		   	proxy: {
					url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_ordine',
					type: 'ajax',
					 actionMethods: {
				          read: 'POST'
				        },
				        
				     extraParams: {
							 rec : <?php echo acs_je($m_params->rec); ?>
        				},
        				
    				doRequest: personalizza_extraParams_to_jsonData, 
		
				   reader: {
		            type: 'json',
					method: 'POST',						            
		            root: 'root'						            
			        }
				},
    			fields: [
        			'k_ordine', 'anno', 'numero', 'tipo', 'importo', 'data_reg', 'data_eva', 'stato',
        			'rife', 'fl_bloc'
    			]
			},
		
			        columns: [
								        
			       		 {
			                header   : 'Anno',
			                dataIndex: 'anno', 
			                width     : 50,
			                filter: {type: 'string'}, filterable: true
			             },
			             { 
			                header   : 'Numero',
			                dataIndex: 'numero', 
			                width     : 70,
			                filter: {type: 'string'}, filterable: true
			             },
			             {
			                header   : 'Data',
			                dataIndex: 'data_reg', 
			                width     : 70,
			                renderer: date_from_AS,
			                filter: {type: 'string'}, filterable: true
			             },	
			              { 
			                header   : 'Tp',
			                dataIndex: 'tipo', 
			                width     : 30,
			                filter: {type: 'string'}, filterable: true
			             },	{text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini bloccati',
							 renderer: function(value, metaData, record){
	    			    			if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
        	    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
        	    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
        	    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';    			    				    	
	    			    	}},
	    			     { 
			                header   : 'St',
			                dataIndex: 'stato', 
			                width     : 30,
			                filter: {type: 'string'}, filterable: true
			             },{ 
			                header   : 'Riferimento',
			                dataIndex: 'rife', 
			                flex    : 1,
			                filter: {type: 'string'}, filterable: true
			             },
			             { 
			                header   : 'Importo',
			                dataIndex: 'importo',
			                align: 'right', 
			                width: 80,
			                filter: {type: 'string'}, filterable: true,
			                renderer: floatRenderer2
			               
			             },
			             {
			                header   : 'Evasione',
			                dataIndex: 'data_eva', 
			                width     : 70,
			                filter: {type: 'string'}, filterable: true,
			                renderer: date_from_AS
			             },			
			         ],  
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [

				<?php 
				$ord = $s->get_ordine_by_k_docu($m_params->rec->k_ordine);
				if(trim($ord['TDABBI']) != ''){
				?>
                
                   {
                     xtype: 'button',
               		 text: 'Annulla abbinamento',
		             iconCls: 'icon-sub_red_delete-32',
		             scale: 'large',
		             disable : true,	                     
		             handler: function() {
		               var loc_win = this.up('window');	
		               Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_annulla_doc',
					        method     : 'POST',
		        			jsonData: {
		        			    k_ordine: <?php echo j($m_params->rec->k_ordine); ?>,
		 					},							        
					        success : function(result, request){
		            		    loc_win.fireEvent('afterAssegna', loc_win);
		            		   
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
		              
                 
			
			            }

			     },
			     <?php }?>
			     
			     '->',
					   {
                     xtype: 'button',
               		 text: 'Conferma',
		             iconCls: 'icon-blog_accept-32',
		             scale: 'large',	                     
		             handler: function() {
		            
		               var loc_win = this.up('window');	
		               var grid = this.up('grid');
			           var rec = grid.getSelectionModel().getSelection()[0];
                           
			           Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_doc',
					        method     : 'POST',
		        			jsonData: {
		        			    k_ordine: <?php echo j($m_params->rec->k_ordine); ?>,
		        				k_docu : rec.get('k_ordine')
		        			
							},							        
					        success : function(result, request){
		            			loc_win.fireEvent('afterAssegna', loc_win);		
		            		   
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
		              
                 
			
			            }

			     }
					
					
					
					
					]
		   }]
         
	        																			  			
	            
        } 
]}

<?php 

}
