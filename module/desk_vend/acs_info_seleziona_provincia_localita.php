<?php

require_once "../../config.inc.php";
$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data_localita'){
    
    $form_values = acs_object_to_ar($m_params->form_values);
    if (isset($m_params->adv_form_values))
        $form_values = array_merge($form_values, acs_object_to_ar($m_params->adv_form_values));
    
    $filtro = $s->get_elenco_ordini_create_filtro($form_values);
    
    
    $stmt = $s->get_elenco_ordini($filtro, 'N', $tipo = '', 'TDDTEP', false, 'N', 'TDPROD, TDDLOC', 'TDPROD, TDDLOC', '2');
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $row = array_map('rtrim', $row);
        $ret[] = $row;
    }
    
   echo acs_je($ret); 
   exit;
}


// ******************************************************************************************
// FORM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open'){
?>
{success:true,
 m_win: {
  title: 'Selezione localit&agrave;/provincia',
  iconCls: 'icon-globe-16'
 }, 
 items: [
    {
        xtype: 'form',
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        frame: true,
        layout: {
            type: 'vbox',
            align: 'stretch'
 		},
		
        items: [
         	{
    			name: 'f_prov_d',
    			xtype: 'combo',
    			fieldLabel: 'Provincia',							
    			displayField: 'text',
    			valueField: 'id',
    			emptyText: '- seleziona -',
    			forceSelection: true,
    		   	allowBlank: true,
    		   	multiSelect: true,														
    			store: {
    			  autoLoad: true,
    			  editable: false,
    			  autoDestroy: true,	 
    		      fields: [{name:'id'}, {name:'text'}],
    		      data: [								    
    			     <?php echo acs_ar_to_select_json(_get_province_filtrate($m_params), '') ?> 	
    			  ] 
    			},
    			queryMode: 'local',
    			minChars: 1, 	
    			listeners: { 
    				 	beforequery: function (record) {
    		         	  record.query = new RegExp(record.query, 'i');
    		         	  record.forceAll = true;
    	                },
    	                change: function(field,newVal) {	            	
        	            	var m_grid = field.up('window').down('grid');
        	                m_grid.store.proxy.extraParams.adv_form_values = field.up('form').getValues();
        	            	m_grid.store.load();
        	            }
    	        }							 
    		}, 
    		
    		
    		{
				xtype: 'grid',
				loadMask: true,
				flex: 1,
				autoScroll : true,
				selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},				
				store: {
					xtype: 'store',
					autoLoad:true,	
					proxy: {
						url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_localita',
						method: 'POST',
						type: 'ajax',
			
						//Add these two properties
						actionMethods: {
							read: 'POST'
						},
						
						extraParams: <?php echo acs_je($m_params) ?>,
        				
    				    doRequest: personalizza_extraParams_to_jsonData,	
			
						reader: {
							type: 'json',
							method: 'POST',
							root: 'root'
						}
					},
						
					fields: ['TDPROD', 'TDDLOC']
								
								
				}, //store
				multiSelect: true,
				
			
				columns: [
				  {header: 'Provincia', dataIndex: 'TDPROD', width: 80},
				  {header: 'Localit&agrave;', dataIndex: 'TDDLOC', flex: 1}
				]
				 
			},
			
			
			, {								   	
	            xtype: 'checkbox'				          
		      , name: 'f_subtotale_prov_d' 
	          , boxLabel: 'Subtotale per provincia'
	          , inputValue: 'Y'		                          
	        }
    		
        ], 
        
        buttons: [{
             text: 'Conferma',
	         iconCls: 'icon-folder_search-32',
	         scale: 'large',
	         handler: function(b) {
	         	var m_win = b.up('window'),
	         	    m_form = m_win.down('form'),
	         	    m_grid = m_win.down('grid'),
	         	    form_values = m_form.getValues();
	         	    prov_selected = m_grid.getSelectionModel().getSelection();
	         	form_values.f_loc_d = [];    
	            
	         	for (var i=0; i<prov_selected.length; i++) 
            		form_values.f_loc_d.push(prov_selected[i].get('TDDLOC'));
            	m_win.fireEvent('afterOkSave', m_win, form_values);	
	         }  
	    }]
	    
	    , listeners: {
	      afterRender: function(comp){
	      	comp.getForm().setValues(<?php echo acs_je($m_params->form_values) ?>);
	      }
	    }
      }
  ]
}
<?php } exit;



//In base ai parametri gia' impostati in Info (main_search), recupero l'elenco delle province
function _get_province_filtrate($m_params){
    global $s;
    
    $filtro = $s->get_elenco_ordini_create_filtro(acs_object_to_ar($m_params->form_values));
    $stmt = $s->get_elenco_ordini($filtro, 'N', $tipo = '', 'TDDTEP', false, 'N', 'TDPROD', 'TDPROD', '1');
    
    $ret = array();
    
    while ($row = db2_fetch_assoc($stmt)) {
        $ret[] = array('id' => rtrim($row['TDPROD']), 'text' => rtrim($row['TDPROD']));
    }
    
    return $ret;
}

