<?php

require_once "../../config.inc.php";

set_time_limit(240);

$s = new Spedizioni();
$main_module = new Spedizioni();

//recupero settimana spedizione (da testate)
//$week = strftime("%V", strtotime($m_data));
//$year = strftime("%G", strtotime($m_data));

$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];

$ar_email_to = array();

if (strlen(trim($_REQUEST['sel_trasportatore'])) > 0) {
	$trasportatore = new Trasportatori();
	$trasportatore->load_rec_data_by_k(array("TAKEY1"=>$_REQUEST['sel_trasportatore']));
	$ar_email_to[] = array(trim($trasportatore->rec_data['TAMAIL']), "TRASPORTATORE " . j(trim($trasportatore->rec_data['TADESC'])) . " (" .  trim($trasportatore->rec_data['TAMAIL']) . ")");
}	

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>


<html>
 <head>
 
  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  

  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  
 
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   table.int1 th.int_data{font-weight: bold; font-size: 13px;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
   tr.ag_liv2 td{background-color: #DDDDDD;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white;}   
   tr.ag_liv_area th{background-color: #b0b0b0; color: black;}
   tr.ag_liv_el_cliente{background-color: #f3f3f3;}   
   span.sceltadata{font-size: 0.6em; font-weight: normal;}
   span.denominazione_cliente{font-weight: bold;}   
   
   div#my_content h2{font-size: 1.6em; padding: 7px;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>


  
  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {

	//invio in formato excel
    if (Ext.get("p_send_excel") != null){		
	  	Ext.get("p_send_excel").on('click', function(){
	  		Ext.getBody().mask('Loading... ', 'loading').show();		  	
	  		 html_text = document.getElementById('my_content').outerHTML;
	  		 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore
	  		 style_text = document.getElementsByTagName('style')[0].outerHTML;	 
	  		
	        Ext.Ajax.request({
	            url : 'send_excel.php' ,
	            method: 'POST',
	            jsonData: {html_text: html_text, style_text: style_text},
	            success: function ( result, request) {
					Ext.getBody().unmask();	                
	            	window.open('data:application/vnd.ms-excel,' + escape(result.responseText));                
	            },
	            failure: function ( result, request) {
	            }
	        });  			  		
	  	});
    }




	//invio contenuto pagina come e-mail
  	Ext.get("p_send_email").on('click', function(){
  		
	//html_text = document.getElementsByTagName('body')[0].innerHTML;
	 html_text = document.getElementById('my_content').outerHTML;
	 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore
//per test	 	 
//	 html_text = html_text.replace(/&amp;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&gt;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&lt;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&�/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&@/g, ""); //replace altrimenti va in errore
	 html_text = html_text.replace(/\<br\>/g, "<br/>"); //replace altrimenti va in errore

	 style_text = document.getElementsByTagName('style')[0].outerHTML;	 
	 html_text += style_text;
	
	var n_doc = document.implementation.createDocument ('http://www.w3.org/1999/xhtml', 'html',  null);
    n_doc.documentElement.innerHTML = html_text;
    
	html_text = n_doc.getElementsByTagName('html')[0].innerHTML;  	
  		
 		var f = Ext.create('Ext.form.Panel', {
        frame: true,
        title: '',        
        bodyPadding: 5,
        url: 'acs_op_exe.php',
        fieldDefaults: {
//            labelAlign: 'top',
            msgTarget: 'side',
            labelWidth: 55,
            anchor: '100%'            
        },
		layout: {
            type: 'vbox',
            align: 'stretch'  // Child items are stretched to full width
        },        

        items: [{
        	xtype: 'hidden',
        	name: 'fn',
        	value: 'exe_send_email'
        	}, {
            xtype: 'combo',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: [ 'email', 'descr' ],
                data: <?php echo $ar_email_json ?>
            }),
            displayField: 'descr',
            valueField: 'email',
            fieldLabel: 'A',
            queryMode: 'local',
            selectOnTab: false,
            name: 'to'
        }, {
            xtype: 'textfield',
            name: 'f_oggetto',
            fieldLabel: 'Oggetto',
            anchor: '96%',
		    allowBlank: false,
		    value: <?php echo j($email_subject) ?>	    
        }
        , {
            xtype: 'htmleditor',
            //xtype: 'textareafield',
            name: 'f_text',
            fieldLabel: 'Testo del messaggio',
            layout: 'fit',
            anchor: '96%',
            height: '100%',
		    allowBlank: false,
		    value: html_text,
		    flex: 1, //riempie tutto lo spazio
			hideLabel: true,
            style: 'margin:0' // Remove default margin		              
        }],

        buttons: [{
            text: 'Invia',
            scale: 'large',
            iconCls: 'icon-email_send-32',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var m_win = this.up('window');	            	
	                form.submit(
								{
		                            //waitMsg:'Loading...',
		                            success: function(form,action) {		                            	
										m_win.close();		        
										// Ext.MessageBox.alert('Avviso', 'Email inviata correttamente');
										acs_show_msg_info('Invio email completato');										                    	
		                            },
		                            failure: function(form,action){
		                                //Ext.MessageBox.alert('Erro');
		                            }
		                        }	                	
	                );
		            //this.up('window').close();            	                	                
	             }            
        		}]
    });


		// create and show window
		var win = new Ext.Window({
		  width: 800
		, height: 380
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Invia e-mail'
		, layout: 'fit'
		, border: true
		, closable: true
		, items: f
		});
		win.show().maximize();  
    	
    		
    		
    	});		
		
	});    
  </script>	

 </head>
 <body>
 	
<div class="page-utility noPrint">
	<A HREF="javascript:window.print()"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>
	
	<!-- <A HREF="#" onclick="window.open('data:application/vnd.ms-excel,' + '<html><head>' + escape(document.getElementsByTagName('style')[0].outerHTML) + '</head>' + '<body>' + escape(document.getElementById('my_content').outerHTML) + '</body></html>');"><img src=<?php echo img_path("icone/48x48/save.png") ?>></A>  -->
	
	<img id="p_send_excel" src=<?php echo img_path("icone/48x48/save.png") ?>>	
	<img id="p_send_email" src=<?php echo img_path("icone/48x48/address_black.png") ?>>	
		
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>
		
</div> 	

<div id='my_content'>

<?php





if ($_REQUEST['el_dettaglio_per_ordine'] == "Y")
	$_REQUEST['el_dettaglio_per_cliente'] = 'Y';


$campo_data = $_REQUEST['sceltadata'][0];
$campo_ora = sceltacampo_ora($campo_data);

//filtro in base a tipologia ordini (multiSelect)
$tipologia_ordini = json_decode($_REQUEST['tipologia_ordini']);

//filtro in base a modello (multiSelect)
$modello = json_decode($_REQUEST['modello']);

 
$k_field = "{$campo_data} as DATA, {$campo_ora} as ORA, TDDT, TA_ITIN.TAASPE AS TDASPE, TDNBOC, TDCITI, SP.CSCVET, TDTPCA, TDAACA, TDNRCA, SP.CSHMPG, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSPORT, SP.CSDTIC, SP.CSHMIC, SP.CSNSPC  /*, TDCVN1 AS MOD, TDSTAB AS STAB*/ ";
$k_field_ord = "{$campo_data}, TA_ITIN.TAASPE, TDCITI, SP.CSCVET, TDTPCA, TDAACA, TDNRCA, TDNBOC, SP.CSHMPG";
$k_field_grp = "{$campo_data}, {$campo_ora}, TDDT, TA_ITIN.TAASPE, TDCITI, SP.CSCVET, TDTPCA, TDAACA, TDNRCA, TDNBOC, SP.CSHMPG, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSPORT, SP.CSDTIC, SP.CSHMIC, SP.CSNSPC /*, TDCVN1, TDSTAB*/";

if ($dett_carico=="Y"){
	$k_field .= " ";
	$s_field .= " ";	
}


if ($_REQUEST['tipo'] == 'agenda'){
	$add_field = " , TDDCON, TDCCON, TDONDO, SP.CSHMPG, SP.CSTISP, TDCLOR";
	$k_field 		.= $add_field;
	$k_field_ord 	.= $add_field;
	$k_field_grp 	.= $add_field;	
	$s_field 		.= $add_field;	
}



 //$s_field = $k_field . ", CSCVET ";
 $s_field = $k_field;

//costruzione sql
if ($_REQUEST['tipo'] == "colli") {
	$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} TD 
				  INNER JOIN {$cfg_mod_Spedizioni['file_righe']}
					ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
					INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
					   ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'					
				  INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL ON
								TDDT = CAL.CSDT AND {$campo_data} = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'

					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  		ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI								
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
					   ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
					   ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'		   
					   								
					";
	$sql_WHERE = " WHERE " . $s->get_where_std() . " AND TDSWSP='Y' 
					AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year}  
					AND RDTPNO='COLLI' AND RDRIGA=0 ";
	if (isset($_REQUEST['gg'])){
		$filtra_giorni = implode(",", $_REQUEST['gg']);
		$sql_WHERE .= " AND CAL.CSGIOR IN ($filtra_giorni) ";
	}

	
	//TODO: DRY	
	if (isset($_REQUEST['area_spedizione']) AND $_REQUEST['area_spedizione'] != '')
		$sql_WHERE .= " AND TA_ITIN.TAASPE = '{$_REQUEST['area_spedizione']}' ";
	if (isset($_REQUEST['divisione']) AND $_REQUEST['divisione'] != '')
		$sql_WHERE .= " AND TDCDIV = '{$_REQUEST['divisione']}' ";	

	if (count($tipologia_ordini) > 0)
		$sql_WHERE .= " AND TDCLOR IN (" . sql_t_IN($tipologia_ordini) . ") ";	

	if (isset($_REQUEST["num_carico"])  && strlen($_REQUEST["num_carico"]) > 0 )
		$sql_WHERE .= " AND TDNRCA ='{$_REQUEST["num_carico"]}'";
	if (isset($_REQUEST["num_lotto"])  && strlen($_REQUEST["num_lotto"]) > 0 )
		$sql_WHERE .= " AND TDNRLO ='{$_REQUEST["num_lotto"]}'";
	
	if ($_REQUEST['carico_assegnato'] == "Y")
		$sql_WHERE .= " AND TDNRCA > 0 ";
	if ($_REQUEST['carico_assegnato'] == "N")
		$sql_WHERE .= " AND TDNRCA = 0 ";
	if ($_REQUEST['lotto_assegnato'] == "Y")
		$sql_WHERE .= " AND TDNRLO > 0 ";
	if ($_REQUEST['anomalie_evasione'] == "Y")
		$sql_WHERE .= " AND TDOPUN = 'N' ";
	if ($_REQUEST['lotto_assegnato'] == "N")
		$sql_WHERE .= " AND TDNRLO = 0 ";
	
	if ($_REQUEST['proforma_assegnato'] == "Y")
		$sql_WHERE .= " AND TDPROF <> '' ";
	if ($_REQUEST['proforma_assegnato'] == "N")
		$sql_WHERE .= " AND TDPROF = '' ";	

	if ($_REQUEST['escludi_da_prog'] == "Y") //escludo le sped. "da programmare"
		$sql_WHERE .= " AND SP.CSSTSP <> 'DP' ";	
	if ($_REQUEST['escludi_da_prog'] == "N") //solo le sped "da programmare"
		$sql_WHERE .= " AND SP.CSSTSP = 'DP' ";	
	

	if (isset($_REQUEST["indice_rottura"])  && strlen($_REQUEST["indice_rottura"]) > 0 )
		$sql_WHERE .= " AND TDIRLO LIKE '%{$_REQUEST["indice_rottura"]}%'";
	if ($_REQUEST['indice_rottura_assegnato'] == "Y")
		$sql_WHERE .= " AND TDIRLO <> '' ";
	if ($_REQUEST['indice_rottura_assegnato'] == "N")
		$sql_WHERE .= " AND TDIRLO = '' ";
	
	

	if (isset($_REQUEST["sel_trasportatore"])  && strlen($_REQUEST["sel_trasportatore"]) > 0 )
		$sql_WHERE .= " AND TA_TRAS.TAKEY1 = " . sql_t_trim($_REQUEST["sel_trasportatore"]);
	
	
	$sql = "SELECT {$campo_data} AS DATA, RDRIFE, RDART, RDDES1, TA_ITIN.TAASPE AS TDASPE, TDCITI, SUM(RDQTA) AS COLLI, SUM(RDQTA2) AS COLLI_PROD, SUM(RDQTA3) AS COLLI_SPED "
			. $sql_FROM . $sql_WHERE
			. " GROUP BY {$campo_data}, RDRIFE, RDART, RDDES1, TA_ITIN.TAASPE, TDCITI"
			. " ORDER BY {$campo_data}, case RDRIFE
                when 'PRODUZIONE' then 1
                else 9
                end, RDRIFE, RDDES1 ";	

}
else { //ELENCO O AGENDA
	$sql = "SELECT $s_field 
				, SUM(TDTOCO) AS S_COLLI, SUM(TDVOLU) AS S_VOLUME, SUM(TDBANC) AS S_PALLET,
				   SUM(TDPLOR) AS S_PESOL, SUM(TDPNET) AS S_PESON, SUM(TDTIMP) AS S_IMPORTO 
				, COUNT(DISTINCT(CONCAT(TDIDES, CONCAT(TDDLOC, CONCAT(TDDCAP, TDNAZD))))) AS C_CLIENTI_DEST 
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
		   		ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
	   	    INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
		   		ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
			INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL 
		   		ON TDDT = CAL.CSDT AND {$campo_data} = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
		   		ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
		   		ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'		   
		   
		   
		WHERE " . $s->get_where_std() . " AND TDSWSP='Y' AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year} ";
		
		if (isset($_REQUEST['gg'])){
			$filtra_giorni = implode(",", $_REQUEST['gg']);
			$sql .= " AND CAL.CSGIOR IN ($filtra_giorni) ";
		}
		
		if ($_REQUEST['ag_solo_ordini'] == 'Y')
			$sql .= " and TDCLOR IN ('O', 'M', 'P') ";

		
		

		
	//TODO: DRY
	
	if (isset($_REQUEST['area_spedizione']) && strlen($_REQUEST['area_spedizione'])>0)
		$sql .= " AND TA_ITIN.TAASPE = '{$_REQUEST['area_spedizione']}' ";
	if (isset($_REQUEST['divisione']) AND $_REQUEST['divisione'] != '')
		$sql .= " AND TDCDIV = '{$_REQUEST['divisione']}' ";	
				 
	if (count($tipologia_ordini) > 0)
		$sql .= " AND TDCLOR IN (" . sql_t_IN($tipologia_ordini) . ") ";
	
	if (isset($_REQUEST['stabilimento']) && $_REQUEST['stabilimento'] != '')
		$sql .= " AND TDSTAB = '{$_REQUEST['stabilimento']}' ";
	
	if (count($modello) > 0)
		$sql .= " AND TDCVN1 IN (" . sql_t_IN($modello) . ") ";
		
	
	if (isset($_REQUEST["num_carico"])  && strlen($_REQUEST["num_carico"]) > 0 )
		$sql .= " AND TDNRCA ='{$_REQUEST["num_carico"]}'";
	if (isset($_REQUEST["num_lotto"])  && strlen($_REQUEST["num_lotto"]) > 0 )
		$sql .= " AND TDNRLO ='{$_REQUEST["num_lotto"]}'";
	if (isset($_REQUEST["num_proforma"])  && strlen($_REQUEST["num_proforma"]) > 0 )
		$sql .= " AND TDPROF LIKE '%{$_REQUEST["num_proforma"]}%'";	
	
	if (isset($_REQUEST["indice_rottura"])  && strlen($_REQUEST["indice_rottura"]) > 0 )
		$sql .= " AND TDIRLO LIKE '%{$_REQUEST["indice_rottura"]}%'";
	if ($_REQUEST['indice_rottura_assegnato'] == "Y")
		$sql .= " AND TDIRLO <> '' ";
	if ($_REQUEST['indice_rottura_assegnato'] == "N")
		$sql .= " AND TDIRLO = '' ";
		
	
	if ($_REQUEST['carico_assegnato'] == "Y")
		$sql .= " AND TDNRCA > 0 ";
	if ($_REQUEST['carico_assegnato'] == "N")
		$sql .= " AND TDNRCA = 0 ";
	if ($_REQUEST['lotto_assegnato'] == "Y")
		$sql .= " AND TDNRLO > 0 ";
	if ($_REQUEST['anomalie_evasione'] == "Y")
		$sql .= " AND TDOPUN = 'N' ";
	if ($_REQUEST['lotto_assegnato'] == "N")
		$sql .= " AND TDNRLO = 0 ";	
	
	if ($_REQUEST['escludi_da_prog'] == "Y") //escludo le sped. "da programmare"
		$sql .= " AND SP.CSSTSP <> 'DP' ";
	if ($_REQUEST['escludi_da_prog'] == "N") //solo le sped "da programmare"
		$sql .= " AND SP.CSSTSP = 'DP' ";	
	
	if (isset($_REQUEST["sel_trasportatore"])  && strlen($_REQUEST["sel_trasportatore"]) > 0 )
		$sql .= " AND TA_TRAS.TAKEY1 = " . sql_t_trim($_REQUEST["sel_trasportatore"]);
	
	
	
	$sql .= " GROUP BY {$k_field_grp} ORDER BY $k_field_ord";
	
}	

/*print_r($sql);
exit;*/

$stmt = db2_prepare($conn, $sql);	
echo db2_stmt_errormsg();	
$result = db2_execute($stmt);

$ar = array();
$ar_tot = array();



/***********************************************************************************************
 *  COLLI 
 ***********************************************************************************************/ 
if ($_REQUEST['tipo'] == "colli") {
echo "<h2>Riepilogo colli - Settimana $week / $year <span class=sceltadata>" . sceltadata($campo_data) . "</span> " . add_descr_report($_REQUEST['add_descr_report']) .  "</h2>";

?>

<center>
	<div class=legenda>
		CT:Colli totali, CD:Colli disponibili/prodotti, CS:Colli spediti
	</div>
</center>

<?php	
 	
 $f_liv0 = "RDRIFE"; 	$d_liv0 = "RDRIFE"; 	
 $f_liv1 = "RDRIFE"; 	$d_liv1 = "RDRIFE";
 $f_liv2 = "RDRIFE"; 	$d_liv2 = "RDRIFE";
 $f_liv3 = "RDRIFE"; 	$d_liv3 = "RDRIFE";   
 
 if ($_REQUEST['cl_dettaglio_per_tip_produttiva'] == "Y") 
   $f_liv1 = "RDART"; 	$d_liv1 = "RDDES1";
   
 if ($_REQUEST['cl_dettaglio_per_area'] == "Y")   
   $f_liv2 = "TDASPE";	
   
 if ($_REQUEST['cl_dettaglio_per_itinerario'] == "Y")   
   $f_liv3 = "TDCITI";	
 
 
 //recupero le disponibilita' produttiva (valori limite)
 
 $sql_dd = "SELECT *
 			FROM  {$cfg_mod_Spedizioni['file_calendario']} CAL 
 			WHERE CSDT='$id_ditta_default' AND CSCALE = '*CS' AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year} 
 		  ";
 $stmt_dd = db2_prepare($conn, $sql_dd);
 $result = db2_execute($stmt_dd); 
 
 $ar_dd = array();
 while ($r = db2_fetch_assoc($stmt_dd)) {
 	$ar_dd[$r['CSGIOR']] = $r['CSDTRG'];
 }
 
 
 if (isset($_REQUEST['area_spedizione']) && strlen($_REQUEST['area_spedizione'])>0)
 	$m_where_area = " AND CPAREA = '{$_REQUEST['area_spedizione']}' ";
 else $m_where_area = '';
 
 $sql_cp = "SELECT 0 as TIPO_REC, CPDATA, CPTPCO, CPRIFE, CPAREA, CPCAPA
		FROM {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP
		INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL ON CAL.CSDT=CP.CPDT AND CAL.CSDTRG = CP.CPDATA AND CSCALE = '*CS' 
		WHERE CAL.CSDT='$id_ditta_default' AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year} {$m_where_area}
 
 			UNION

		SELECT 1 as TIPO_REC, CPDATA, CPTPCO, CPRIFE, CPAREA, CPCAPA
		FROM {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP2		 
		WHERE CP2.CPDT='$id_ditta_default' AND CP2.CPDATA = {$year} {$m_where_area}			
 			
 		";
 
 
 $stmt_cp = db2_prepare($conn, $sql_cp);
 $result = db2_execute($stmt_cp);

 $ar_cp = array();
 while ($r = db2_fetch_assoc($stmt_cp)) {
	if ($r['TIPO_REC'] == 0 ){ //BY DAY
		$data = $r['CPDATA'];		
		$ar_cp[$data][$r['CPTPCO']]['TOTALE'] += $r['CPCAPA']; 	
		$ar_cp[$data][$r['CPTPCO']][$r['CPAREA']] += $r['CPCAPA'];
	} else {
		//RECORD GENERICO (PER GIORNO)
		$data = $ar_dd[$r['CPRIFE']];
		if (!isset($ar_cp[$data][$r['CPTPCO']][$r['CPAREA']])){
			$ar_cp[$data][$r['CPTPCO']]['TOTALE'] += $r['CPCAPA'];
			$ar_cp[$data][$r['CPTPCO']][$r['CPAREA']] += $r['CPCAPA'];			
		}
	}		
 }
 
 while ($r = db2_fetch_assoc($stmt)) {
 	
		$d_liv2 = $s->decod_std('ASPE', $r[$f_liv2]);
		$d_liv3 = $s->decod_std('ITIN', $r[$f_liv3]);	 			
			
		if (!isset($ar[$r['DATA']])) 				
 			$ar[$r['DATA']] = array("cod" => $r['DATA'], "descr"=>$r['DATA'], 
																  "val" => array(), "children"=>array());

	 	$d_ar = &$ar[$r['DATA']]['children'];
		 
		 $tmp_ar = &$ar[$r['DATA']];																  
		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
	 	

		//liv0
		if (!isset($d_ar[$r[$f_liv0]]))	
				$d_ar[$r[$f_liv0]] = array("cod" => $r[$f_liv0], "descr"=>$r[$d_liv0], 
																  "val" => array(), "children"=>array()); 														  
																  																  
		 $tmp_ar = &$d_ar[$r[$f_liv0]];																  
		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
		 $d_ar = &$d_ar[$r[$f_liv0]]['children'];
	 	
		//liv1
		if (!isset($d_ar[$r[$f_liv1]]))	
				$d_ar[$r[$f_liv1]] = array("cod" => $r[$f_liv1], "descr"=>$r[$d_liv1], 
																  "val" => array(), "children"=>array()); 														  
																  																  
		 $tmp_ar = &$d_ar[$r[$f_liv1]];																  
		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
					
	 
	 	//liv2
		if (!isset($d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]]))	
				$d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]] = array("cod" => $r[$f_liv2], "descr"=>$d_liv2, 
																  "val" => array(), "children"=>array());
		 $tmp_ar = &$d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]];																  
		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
		 
		//liv3 
		if (!isset($d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]]['children'][$r[$f_liv3]]))	
				$d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]]['children'][$r[$f_liv3]] = array("cod" => $r[$f_liv3], "descr"=>$d_liv3, 
																  "val" => array(), "children"=>array());
																  
		 $tmp_ar = &$d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]]['children'][$r[$f_liv3]];																  
		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
		 
	
 } //while
 
 
 $cl_liv_cont = 0;
 $liv0_row_cl = 0; 
 if ($_REQUEST['cl_dettaglio_per_itinerario'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
 if ($_REQUEST['cl_dettaglio_per_area'] == "Y") $liv2_row_cl = ++$cl_liv_cont; 
 if ($_REQUEST['cl_dettaglio_per_tip_produttiva'] == "Y") $liv1_row_cl = ++$cl_liv_cont; 
 	 
 echo "<table class=int1><tr>";
 foreach ($ar as $kgg => $gg){
  //echo "<div class='ag_giorno' style='display: inline-block; vertical-align: top;'>";
	echo "<td valign=top>";  
	 echo "<table class=int1>";
	  echo "<tr><th colspan=4>" . ucfirst(print_date($kgg, "%A %d/%m")) . "</th></tr>";
	  echo "<tr><th>&nbsp;</th><th>CT</th><th>CD</th><th>CS</th></tr>";	  
	  
	  
		foreach ($gg['children'] as $kl0 => $l0){
		  echo "<tr class=ag_liv{$liv0_row_cl}><td>" . $l0['descr'] . "</td><td class=number>" . $l0['val']['COLLI'] . "</td>
		  	  														<td class=number>" . $l0['val']['COLLI_PROD'] . "</td>
		  	  														<td class=number>" . $l0['val']['COLLI_SPED'] . "</td></tr>";

				  //confronto con capacita' produttiva
				  if (isset($ar_cp[$kgg][trim($kl0)]['TOTALE']) && $_REQUEST['cl_confronta_cap_prod'] == 'Y'){
				  	$m_cp = $ar_cp[$kgg][trim($kl0)]['TOTALE'];
				  	$m_colli = $l0['val']['COLLI']; 
				  	$m_dif = $m_colli - $m_cp;
				  	$m_dif_perc = ($m_colli - $m_cp) / $m_cp * 100;
				  	
				  	if ($m_dif_perc >= 5) 
				  		$m_style = "background-color: #F9BFC1;";
				  	elseif ($m_dif_perc >= -5)
				  		$m_style = "background-color: yellow;";
				  	else $m_style = "background-color: #BAE860;"; 		  	
				  	echo "<tr class=ag_liv{$liv0_row_cl}><td align=right>Capacit&agrave;</td><td class=number>" . $m_cp . "</td>
				  	  														<td class=number style='$m_style'>" . n($m_dif, 0, 'Y') . "</td>
				  	  														<td class=number style='$m_style'>" . n($m_dif_perc, 1, 'Y') . "%</td></tr>";
				  }
		  
				  
		  foreach ($l0['children'] as $kl1 => $l1){
	 		if ($_REQUEST['cl_dettaglio_per_tip_produttiva'] == "Y")	  	
		  	  echo "<tr class=ag_liv{$liv1_row_cl}><td>" . $l1['descr'] . "</td><td class=number>" . $l1['val']['COLLI'] . "</td>
		  	  														<td class=number>" . $l1['val']['COLLI_PROD'] . "</td>
		  	  														<td class=number>" . $l1['val']['COLLI_SPED'] . "</td></tr>";	  	
			
		  		foreach ($l1['children'] as $kl2 => $l2){
		  		  if ($_REQUEST['cl_dettaglio_per_area'] == "Y"){
		  			echo "<tr class=ag_liv{$liv2_row_cl}><td>" . $l2['descr'] . "</td><td class=number>" . $l2['val']['COLLI'] . "</td>
		  																 <td class=number>" . $l2['val']['COLLI_PROD'] . "</td>
		  																 <td class=number>" . $l2['val']['COLLI_SPED'] . "</td></tr>";
		  			
			  			//confronto con capacita' produttiva
			  			if (isset($ar_cp[$kgg][trim($kl0)][trim($kl2)]) && $_REQUEST['cl_confronta_cap_prod'] == 'Y' && $_REQUEST['cl_dettaglio_per_tip_produttiva'] != "Y"){
			  				$m_cp = $ar_cp[$kgg][trim($kl0)][trim($kl2)];
			  				$m_colli = $l2['val']['COLLI'];
			  				$m_dif = $m_colli - $m_cp;
			  				$m_dif_perc = ($m_colli - $m_cp) / $m_cp * 100;
			  				 
						  	if ($m_dif_perc >= 5) 
						  		$m_style = "background-color: #F9BFC1;";
						  	elseif ($m_dif_perc >= -5)
						  		$m_style = "background-color: yellow;";
						  	else $m_style = "background-color: #BAE860;";
			  				echo "<tr class=ag_liv{$liv2_row_cl}><td align=right>Capacit&agrave;</td><td class=number>" . $m_cp . "</td>
			  	  														<td class=number style='$m_style'>" . n($m_dif, 0, 'Y') . "</td>
			  					  	  									<td class=number style='$m_style'>" . n($m_dif_perc, 1, 'Y') . "%</td></tr>";
			  			}		  			
		  			
		  		  }	
		  		  
		  		  
						 
				  		foreach ($l2['children'] as $kl3 => $l3){
				  		  if ($_REQUEST['cl_dettaglio_per_itinerario'] == "Y")
				  			echo "<tr class=ag_liv{$liv3_row_cl}><td>" . $l3['descr'] . "</td><td class=number>" . $l3['val']['COLLI'] . "</td>
				  																 <td class=number>" . $l3['val']['COLLI_PROD'] . "</td>
				  																 <td class=number>" . $l3['val']['COLLI_SPED'] . "</td></tr>";	  			
						}					  			
				}		
				  	
		  }
		}
	  
	 echo "</table>";
  //echo "</div>";
  echo "</td>";
 }
 echo "</tr></table>";
} //colli










/***********************************************************************************************
 *  AGENDA 
 ***********************************************************************************************/
if ($_REQUEST['tipo'] == "agenda") {
 echo "<h2>Agenda spedizioni - Settimana $week / $year <span class=sceltadata>" . sceltadata($campo_data) . "</span> " . add_descr_report($_REQUEST['add_descr_report']) .  "</h2>";
 
 while ($r = db2_fetch_assoc($stmt)) {		
		if (!isset($ar[$r['DATA']])) 				
 			$ar[$r['DATA']] = array("cod" => $r['DATA'], "descr"=>$r['DATA'], 
																  "val" => array(), "children"=>array());

	 	$d_ar = &$ar[$r['DATA']]['children'];
		 
		 $tmp_ar = &$ar[$r['DATA']];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;					
		 $tmp_ar["val"]['COLLI']  	+= $r['S_COLLI'] ;		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;
		 		 
		 if (in_array($r['TDCLOR'], array('O', 'M', 'P'))){
		 	$tmp_ar["val"]['N_ORDINI'] += 1 ;		 
		 }
	 	
	 	
		
		$l_itinerario = $r['TDCITI'];
		$d_itinerario = $s->decod_std('ITIN', $l_itinerario);
		$cstisp = "";		
		if ($_REQUEST['ag_dettaglio_per_orario_spedizione'] == "Y"){
			$l_itinerario .= $r['ORA'];
			if ($r['ORA'] > 0)   $d_itinerario .= " [" . print_ora($r['ORA']) . "]"; 
		}
		if ($_REQUEST['ag_dettaglio_per_tip_spedizione'] == "Y"){
			$l_itinerario .= $r['CSTISP'];
			//$d_itinerario .= " " . $r['CSTISP'];
			$cstisp = "<br>" . $r['CSTISP']; 			
		}			
		
		if ($_REQUEST['ag_dettaglio_per_itinerario'] != "Y") $l_itinerario = '000'; 
		
		if (!isset($d_ar[$l_itinerario]))	
				$d_ar[$l_itinerario] = array("cod" => $l_itinerario, 
																 "descr" 	=> $d_itinerario,
																 "cstisp" 	=> $cstisp,
																  "val" => array(), "children"=>array()); 														  
																  																  
		 $tmp_ar = &$d_ar[$l_itinerario];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;
		 $tmp_ar["val"]['COLLI'] 	+= $r['S_COLLI'] ;		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;

		 if (in_array($r['TDCLOR'], array('O', 'M', 'P')))		 
		 	$tmp_ar["val"]['N_ORDINI'] += 1 ;
		 					
	 
		 
	 	// ------------- liv2 (vettore / mezzo) -------------------------
 		 if ($_REQUEST['ag_dettaglio_per_mezzo'] == "Y"){
		 	$liv2_cod = implode("|", array($r['CSCVET'], $r['CSCAUT']));
		 	$liv2_des = $s->decod_std('AUTR', trim($r['CSCVET'])) . " - " . $s->decod_std('AUTO', trim($r['CSCAUT'])); 
		 } else if ($_REQUEST['ag_dettaglio_per_vettore'] == "Y") { //per vettore
		 	$liv2_cod = implode("|", array($r['CSCVET']));
		 	$liv2_des = $s->decod_std('AUTR', trim($r['CSCVET']));		 	
		 } 	else { //non stacco, cosi' gli eventuali clienti rimangono ordinati
		 	$liv2_cod = '000';
		 	$liv2_des = '000';		 	
		 } 	
	 	
		if (!isset($d_ar[$l_itinerario]['children'][$liv2_cod]))	
				$d_ar[$l_itinerario]['children'][$liv2_cod] = array("cod" => $liv2_cod, "descr"=>$liv2_des, 
																  "val" => array(), "children"=>array(), "carichi"=>array());
		 $tmp_ar = &$d_ar[$l_itinerario]['children'][$liv2_cod];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;
		 $tmp_ar["val"]['COLLI'] 	+= $r['S_COLLI'] ;		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;
		 
		 if (in_array($r['TDCLOR'], array('O', 'M', 'P')))		 
		 	$tmp_ar["val"]['N_ORDINI'] += 1 ;
		 			 
		 $tmp_ar["carichi"][$s->k_carico_td_encode($r)] += 1;
	 	
		
		// ------------- liv3 (cliente) -------------------------		 
		if (!isset($d_ar[$l_itinerario]['children'][$liv2_cod]['children'][$r['TDCCON']]))	
				$d_ar[$l_itinerario]['children'][$liv2_cod]['children'][$r['TDCCON']] = array("cod" => $r['TDCCON'], "descr"=>$r['TDDCON'], 
																  "val" => array(), );
																  
		 $tmp_ar = &$d_ar[$l_itinerario]['children'][$liv2_cod]['children'][$r['TDCCON']];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;			 
		 $tmp_ar["val"]['COLLI'] 	+= $r['S_COLLI'] ;		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;
		 
		 if (in_array($r['TDCLOR'], array('O', 'M', 'P')))		 
		 	$tmp_ar["val"]['N_ORDINI'] += 1 ;		 
		 
	
 } //while

 
 

 $campo_totalizzato = strtoupper($_REQUEST['ag_f_tot']);
 switch (strtoupper($_REQUEST['ag_f_tot'])){
	case "VOLUME":
		$n_dec = 3; break;
	default:
		$n_dec = 0;	
 }
 
 

 $cl_liv_cont = 0;
 if ($_REQUEST['ag_dettaglio_per_cliente'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
 if ($_REQUEST['ag_dettaglio_per_vettore'] == "Y" || $_REQUEST['ag_dettaglio_per_mezzo'] == "Y") $liv2_row_cl = ++$cl_liv_cont; 
 if ($_REQUEST['ag_dettaglio_per_itinerario'] == "Y") $liv1_row_cl = ++$cl_liv_cont; 
 
 echo "<table class=int1><tr>";
 foreach ($ar as $kgg => $gg){
	echo "<td valign=top>";  
	 echo "<table class=int1>";
	  echo "<tr><th colspan=2 class=int_data>" . ucfirst(print_date($kgg, "%A %d/%m")) . "</th></tr>";
	  
	  foreach ($gg['children'] as $kl1 => $l1){
 		if ($_REQUEST['ag_dettaglio_per_itinerario'] == "Y")	  	
	  	  echo "<tr class=ag_liv{$liv1_row_cl}><td>" . $l1['descr'] . "</td><td class=number>" . n($l1['val'][$campo_totalizzato], $n_dec) . $l1['cstisp'] . "</td></tr>";	  	
		
	  		foreach ($l1['children'] as $kl2 => $l2){
	  		  if ($_REQUEST['ag_dettaglio_per_vettore'] == "Y" || $_REQUEST['ag_dettaglio_per_mezzo'] == "Y"){
	  		  	
		  		  	if ($_REQUEST['ag_dett_carichi'] == 'Y'){
		  		  		//stampo l'elenco dei carichi
		  		  		$ar_carichi = array();
		  		  		foreach ($l2['carichi'] as $k_carico => $carico)
		  		  			$ar_carichi[] = $k_carico;
		  		  		
		  		  		$l2['descr'] .= "<br><div style='float: right; font-size: 9px; font-weight: bold;'>[" . implode(", ", $ar_carichi) . "]</div>";	  		  		
		  		  	}		  		  	
	  		  	
	  			echo "<tr class=ag_liv{$liv2_row_cl}><td>" . $l2['descr'] . "</td><td class=number>" . n($l2['val'][$campo_totalizzato], $n_dec) . "</td></tr>";
	  		  }	

	  		  		usort($l2['children'], "cmp");	  		  
			  		foreach ($l2['children'] as $kl3 => $l3){
			  		  if ($_REQUEST['ag_dettaglio_per_cliente'] == "Y")
			  			echo "<tr class=ag_liv{$liv3_row_cl}><td>" . $l3['descr'] . "</td><td class=number>" . n($l3['val'][$campo_totalizzato], $n_dec) . "</td></tr>";	  			
					}				
					  			
			}		
			  	
	  }
	  
	 echo "</table>";
  echo "</td>";
 }
 echo "</tr></table>";
} //agenda





/***********************************************************************************************
 *  ELENCO 
 ***********************************************************************************************/
 
if ($_REQUEST['tipo'] == "elenco") {

 echo "<h2>Riepilogo spedizioni - Settimana $week / $year <span class=sceltadata>" . sceltadata($campo_data) . "</span> " . add_descr_report($_REQUEST['add_descr_report']) .  "</h2>";

 $f_liv0 = "TDASPE"; 			$d_liv1 = "TDASPE"; 
 $f_liv1 = "ITIN_VETT_SPED"; 	$d_liv1 = "D_ITIN_VETT_SPED";
 $f_liv2 = "CARICO";			$d_liv2 = "CARICO";
 $f_liv3 = "";					$d_liv3 = "";
 
 $ar_tot["TOTALI"] = array();
 
 while ($r = db2_fetch_assoc($stmt)) {
			
		$r['ITIN_VETT_SPED'] 	= implode("|",  array($r['TDCITI'], $r['CSCVET'], $r['TDNBOC'])); 	
		$r['D_ITIN_VETT_SPED'] 	= implode("|", 	array($s->decod_std('ITIN', $r['TDCITI']), $r['CSCVET'], $r['TDNBOC']));
		$r['CARICO'] 			= implode("_",  array($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA']));
		
	 
		 $tmp_ar = &$ar_tot["TOTALI"];																  
		 $tmp_ar['VOLUME'] += $r['S_VOLUME'] ;					
		 $tmp_ar['COLLI'] += $r['S_COLLI'] ;
		 $tmp_ar['PALLET'] += $r['S_PALLET'] ;
		 $tmp_ar['PESOL'] += $r['S_PESOL'] ;
		 $tmp_ar['IMPORTO'] += $r['S_IMPORTO'] ;
		 $tmp_ar['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;	 
			
		if (!isset($ar[$r['DATA']])) 				
 			$ar[$r['DATA']] = array("cod" => $r['DATA'], "descr"=>$r['DATA'], 
																  "val" => array(), "children"=>array());

	 	$d_ar = &$ar[$r['DATA']]['children'];
		 
		 $tmp_ar = &$ar[$r['DATA']];																  
		 $tmp_ar["val"]['VOLUME'] += $r['S_VOLUME'] ;					
		 $tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
		 $tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
		 $tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
		 $tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;		 		 


		 //liv0
		 if (!isset($d_ar[$r[$f_liv0]]))
		 	$d_ar[$r[$f_liv0]] = array("cod" => $r[$f_liv0], "descr"=>$s->decod_std('ASPE', $r[$f_liv0]),
		 			"val" => array(),
		 			"children"=>array());
		 
		 $tmp_ar = &$d_ar[$r[$f_liv0]];
		 $tmp_ar["val"]['VOLUME'] += $r['S_VOLUME'] ;
		 $tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
		 $tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
		 $tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
		 $tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
		 
		 $d_ar = &$tmp_ar['children'];
		 	
		 
		//liv1
		if (!isset($d_ar[$r[$f_liv1]]))	
				$d_ar[$r[$f_liv1]] = array("cod" => $r[$f_liv1], "descr"=>$r[$d_liv1], 
																  "val" => array(),
																  "itinerario" => $s->decod_std('ITIN', $r['TDCITI']), 
																  "vettore" => $s->decod_std('AUTR', trim($r['CSCVET'])),
																  "vmc"		=> $s->des_vmc_sp($r),
																  "orario" => get_orario($r),
																  "TDNBOC" => $r['TDNBOC'], "CSNSPC" => $r['CSNSPC'],
																  "CSTISP" => $r['CSTISP'], "CSTITR" => $r['CSTITR'],
																  "CSDTIC" => $r['CSDTIC'], "CSHMIC" => $r['CSHMIC'],
																  "CSKMTR" => $r['CSKMTR'], "CSPORT" => $r['CSPORT'],
																  "children"=>array()); 														  
																  																  
		 $tmp_ar = &$d_ar[$r[$f_liv1]];																  
		 $tmp_ar["val"]['VOLUME'] += $r['S_VOLUME'] ;					
		 $tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
		 $tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
		 $tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
		 $tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;

		 $d_ar = &$tmp_ar['children'];		 
					
	 
	 	//liv2
		if (!isset($d_ar[$r[$f_liv2]]))	
				$d_ar[$r[$f_liv2]] = array("cod" => $r[$f_liv2], "descr"=>$r[$d_liv2],
																	"note_carico" => note_carico($r),
																	"TDTPCA" => $r['TDTPCA'],
																	"TDAACA" => $r['TDAACA'],
																	"TDNRCA" => $r['TDNRCA'], 
																  	"val"  => array(), "children"=>array());
		 $tmp_ar = &$d_ar[$r[$f_liv2]];																  
		 $tmp_ar["val"]['VOLUME'] += $r['S_VOLUME'] ;					
		 $tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
		 $tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
		 $tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
		 $tmp_ar["val"]['STAB_MOD'] = $r['STAB'].", ".$r['MOD'] ;    //SARA
		 $tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;		 
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;

		 $tmp_ar["children"][] = $r;
		 
	
 } //while






	echo "<table class=int1>";
	
	echo "
			<tr>
			 <th>Itinerario</th>
			 <th>Vettore</th>";
	
	if ($_REQUEST['mostra_porta'] == "Y")
		echo "<th>Porta</th>";
	
	
	echo "<th>Orario</th>";
	
	if ($_REQUEST['dettaglia_km_sped'] == "Y") echo "<th>Km</th>";	
	
	if ($_REQUEST['mostra_tipologie'] == "Y")	
    echo "
			 <th>TS</th>			 
			 <th>TT</th>			 
			 ";			 
			 
			if ($_REQUEST['dettaglio_per_carico'] == "Y" || $_REQUEST['el_dettaglio_per_cliente'] == "Y") echo "<th>Carico</th>";
			
			echo "
		
					 <th>Colli</th>
				";

			if ($_REQUEST['mostra_pallet'] == "Y")
				echo "<th>Pallet</th>";
			
			echo "
					 <th>Volume</th>			 
					 <th>Peso</th>
					 <th>Scarichi</th>";
			if ($_REQUEST['mostra_importo'] == "Y")			
				echo "<th>Importo</th>";
			echo "</tr>";
	
 


 $cl_liv_cont = 0;
 if ($_REQUEST['dettaglio_per_carico'] == "Y") $liv2_row_cl = ++$cl_liv_cont;
 $liv1_row_cl = ++$cl_liv_cont; 


 $r_colspan_carico = 3;
 $col_dettaglio    = 4;
 if ($_REQUEST['dettaglio_per_carico'] == "Y" || $_REQUEST['el_dettaglio_per_cliente'] == "Y") $r_colspan = 4; else $r_colspan=3;
 if ($_REQUEST['dettaglia_km_sped'] == "Y") {$r_colspan++; $r_colspan_carico++;}
 if ($_REQUEST['mostra_porta'] == "Y") {$r_colspan++; $r_colspan_carico++;} 
 if ($_REQUEST['mostra_tipologie'] == "Y") {$r_colspan+=2; $r_colspan_carico+=2;}
 if ($_REQUEST['mostra_pallet'] == "Y") $col_dettaglio++; 
 if ($_REQUEST['mostra_importo'] == "Y") $col_dettaglio++; 

 foreach ($ar as $kgg => $gg){

	  echo "<tr class=ag_liv_data><th colspan={$r_colspan}>" . ucfirst(print_date($kgg, "%A %d/%m")) . "</th>
	  	  			<th class=number>" . $gg['val']['COLLI'] . "</th>";

	  if ($_REQUEST['mostra_pallet'] == "Y")	  
	  	echo "<th class=number>" . $gg['val']['PALLET'] . "</th>";
	  
	  echo "
	  	  			<th class=number>" . n($gg['val']['VOLUME'],3) . "</th>	  	  			
	  	  			<th class=number>" . n($gg['val']['PESOL'],1) . "</th>
	  	  			<th class=number>" . $gg['val']['CLIENTI_DEST'] . "</th>";
	  if ($_REQUEST['mostra_importo'] == "Y")	  
	  	echo "<th class=number>" . n($gg['val']['IMPORTO'], 0) . "</th>";
	  echo "</tr>";
	  
	  
	  	foreach ($gg['children'] as $kl0 => $l0){


			//AREA SPEDIZIONE
			  if ($_REQUEST['dettaglio_per_area_spedizione'] == "Y"){		
				echo "<tr class=ag_liv_area><th colspan={$r_colspan}>" . ucfirst($l0['descr']) . "</th>
					  	  			<th class=number>" . $l0['val']['COLLI'] . "</th>";
				
				if ($_REQUEST['mostra_pallet'] == "Y")
					echo "<th class=number>" . $l0['val']['PALLET'] . "</th>";
				 
				echo "
					  	  			<th class=number>" . n($l0['val']['VOLUME'],3) . "</th>
					  	  			<th class=number>" . n($l0['val']['PESOL'],1) . "</th>
					  	  			<th class=number>" . $l0['val']['CLIENTI_DEST'] . "</th>";
				if ($_REQUEST['mostra_importo'] == "Y")
					echo "<th class=number>" . n($l0['val']['IMPORTO'], 0) . "</th>";
				echo "</tr>";
			 }

	  
			 usort($l0['children'], "cmp_el_l1");			 
			 
			  foreach ($l0['children'] as $kl1 => $l1){
			  	
				//ITINERARIO / VETTORE	  	
			  	  echo "<tr class=ag_liv{$liv1_row_cl}>
			  	  			<td>" . $l1['itinerario'] . "</td>
			  	  			<td>" . $l1['vmc'] . "</td>";
			  	  
			  	  if ($_REQUEST['mostra_porta'] == "Y")
			  	  	echo "<td colspan=1>" . $l1['CSPORT'] . "</td>";	  	  	  	  
			  	  
				  echo "	<td colspan=1>" . $l1['orario'] . "</td>";
			  	  
			  	  if ($_REQUEST['dettaglia_km_sped'] == "Y") echo "<td class=number>{$l1['CSKMTR']}</td>";	  	  
		
			  	  if ($_REQUEST['mostra_tipologie'] == "Y")	  	  	  	  
			  	  echo "
							<td colspan=1>" . $l1['CSTISP'] . "</td>" . "
							<td colspan=1>" . $l1['CSTITR'] . "</td>" . "										
							";
							
				  if ($_REQUEST['dettaglio_per_carico'] == "Y" || $_REQUEST['el_dettaglio_per_cliente'] == "Y"){						
					if ($_REQUEST['dettaglio_per_carico'] == "Y" && count($l1['children']) == 1) {
						$r_carico = reset($l1['children']); //prendo il primo elemento
						$ar_txt_carico = array();
						$ar_txt_carico[] = $r_carico['descr'];
						if (strlen(trim($r_carico['note_carico'])) > 0)
							$ar_txt_carico[] = $r_carico['note_carico'];						
						
						echo "<td colspan=1>" . implode("<br>", $ar_txt_carico) . "</td>";
					} else
						echo "<td colspan=1>&nbsp;</td>";				  		
				  }	 
				  					
				  echo "		
			  	  			<td class=number>" . $l1['val']['COLLI'] . "</td>";
				  if ($_REQUEST['mostra_pallet'] == "Y")		  
				  		echo "<td class=number>" . $l1['val']['PALLET'] . "</td>";
				  echo "
			  	  			<td class=number>" . n($l1['val']['VOLUME'], 3) . "</td>	  	  			
			  	  			<td class=number>" . n($l1['val']['PESOL'], 1) . "</td>
			  	  			<td class=number>" . $l1['val']['CLIENTI_DEST'] . "</td>";
				  if ($_REQUEST['mostra_importo'] == "Y")		  
				  	echo "<td class=number>" . n($l1['val']['IMPORTO'], 0) . "</td>";	  	  			
			  	  echo "</tr>";	  	
		
					// CARICO		
			  		foreach ($l1['children'] as $kl2 => $l2){
			  		  if ($_REQUEST['dettaglio_per_carico'] == "Y" && count($l1['children']) > 1){
			  			echo "<tr class=ag_liv{$liv2_row_cl}>
			  					<td colspan={$r_colspan_carico} align=right>
			  						". $l2['note_carico'] ."
			  					</td>
			  					<td>" . $l2['descr'] . "</td>";
			  		  
			  		    if (count($l1['children']) > 1){
			  		  	  echo "
				  	  			<td class=number>" . $l2['val']['COLLI'] . "</td>";
			  		  	  if ($_REQUEST['mostra_pallet'] == "Y")	  		  	  
			  		  	  	echo "<td class=number>" . $l2['val']['PALLET'] . "</td>";
			  		  	  echo "
				  	  			<td class=number>" . n($l2['val']['VOLUME'], 3) . "</td>		  	  			
				  	  			<td class=number>" . n($l2['val']['PESOL'], 1) . "</td>
		  						<td class=number>" . $l2['val']['CLIENTI_DEST'] . "</td>";
			  		  	  if ($_REQUEST['mostra_importo'] == "Y")	  		  	  
			  		    	echo "<td class=number>" . n($l2['val']['IMPORTO'], 0) . "</td>";
			  		    }
			  		    else //un carico solo... non ripeto i dettagli
			  		      echo "<td colspan={$col_dettaglio}>&nbsp;</td>";
		
			  		    echo "</tr>";
			  		   } 
							  			
			  		   
			  		   //CLIENTI
			  		   if ($_REQUEST['el_dettaglio_per_cliente'] == "Y"){
						global $id_ditta_default;
						$el_scarichi =$s->el_scarichi_by_sped_car($l1['TDNBOC'], $kgg, $l2['TDTPCA'], $l2['TDAACA'], $l2['TDNRCA'], $id_ditta_default, $campo_data, "finale");
						foreach ($el_scarichi as $sca){
							
										echo "<tr class=ag_liv_el_cliente>
												<td colspan=1 align=right>&nbsp;</td>										
												<td colspan=" . ($r_colspan_carico -1) . ">". out_cliente($sca) ."</td>
												<td>" . implode("_",  array($sca['TDTPCA'], $sca['TDAACA'], $sca['TDNRCA'])) . "</td>";
													  

										echo "<td class=number>" . $sca['S_COLLI'] . "</td>";
										if ($_REQUEST['mostra_pallet'] == "Y")
											echo "<td class=number>" . $sca['S_PALLET'] . "</td>";
										echo "
											<td class=number>" . n($sca['TDVOLU'], 3) . "</td>
											<td class=number>" . n($sca['S_PESO'], 1) . "</td>
											<td class=number>&nbsp;</td>";
										if ($_REQUEST['mostra_importo'] == "Y")
											echo "<td class=number>" . n($sca['S_IMPORTO'], 0) . "</td>";

			  		  	  				echo "</tr>";
			  		  	  				
			  		  	  				//ORDINI
			  		  	  				if ($_REQUEST['el_dettaglio_per_ordine'] == "Y"){
			  		  	  					global $id_ditta_default;
			  		  	  					$el_ordini =$s->el_ordini_by_scarico($l1['TDNBOC'], $kgg, $l2['TDTPCA'], $l2['TDAACA'], $l2['TDNRCA'], $id_ditta_default, $campo_data, "finale", 
			  		  	  							$sca['TDCCON'], $sca['TDCDES'], $sca['TDTDES'], $_REQUEST);			  		  	  					
			  		  	  					foreach ($el_ordini as $r_ord){
			  		  	  					
			  		  	  						echo "<tr class=ag_liv_el_cliente>
												<td colspan=" . ($r_colspan_carico) . " align=right>" . trim($r_ord['TDVSRF']) . "</td>
												<td> " . $s->k_ordine_out($r_ord['TDDOCU']) . "</td>";			  		  	  							
			  		  	  				
			  		  	  						echo "<td class=number>" . $r_ord['S_COLLI'] . "</td>";
			  		  	  						if ($_REQUEST['mostra_pallet'] == "Y")
			  		  	  							echo "<td class=number>" . $r_ord['S_PALLET'] . "</td>";
			  		  	  						echo "
												<td class=number>" . n($r_ord['TDVOLU'], 3) . "</td>
												<td class=number>" . n($r_ord['S_PESO'], 1) . "</td>";
		 		
		 										if (trim($r_ord['TDCVN1'])==''){
		 											
		 											echo "<td class=number>" . $r_ord['TDSTAB']."</td>";
		 										}else{
													echo "<td class=number>" . $r_ord['TDSTAB'].", " .$r_ord['TDCVN1']. "</td>";
		 										}   //sara
												
			  		  	  						if ($_REQUEST['mostra_importo'] == "Y")
			  		  	  							echo "<td class=number>" . n($r_ord['S_IMPORTO'], 0) . "</td>";
			  		  	  				
			  		  	  						echo "</tr>";
			  		  	  				
			  		  	  				
			  		  	  					}
			  		  	  				}			  		  	  				
			  		  	  				
			  		  	  				


						} 	
					   }			  		   
			  		   
					}

					//stamp l'eventuale riga collegata
					if ($_REQUEST['dettaglio_per_carico'] == "Y" && $l1['CSNSPC'] > 0){
						$col_dettaglio_coll = $col_dettaglio +1;
						echo "<tr class=ag_liv{$liv2_row_cl}>
							<td colspan={$r_colspan_carico} align=right>
							<img src=" . img_path("icone/48x48/link_blu.png") . " width=18> Spedizione collegata:</td>
							<td colspan={$col_dettaglio_coll}>" .  $s->get_el_carichi_by_sped($l1['CSNSPC'], 'Y', 'N', 'Y') . "</td>";
			  		  	echo "</tr>";
					}
					
					
					  	
			  }
		}
 }

		
	//STAMPO TOTALE
					echo "
					<tr class=liv_totale>
					 <td colspan={$r_colspan}>TOTALE GENERALE</td>
					 <td class=number>" . $ar_tot['TOTALI']['COLLI'] . "</td>";
					if ($_REQUEST['mostra_pallet'] == "Y")					
						echo "<td class=number>" . $ar_tot['TOTALI']['PALLET'] . "</td>";
					echo "
					 <td class=number>" . n($ar_tot['TOTALI']['VOLUME'], 3) . "</td>					 
					 <td class=number>" . n($ar_tot['TOTALI']['PESOL'], 1) . "</td>
					 <td class=number>" . $ar_tot['TOTALI']['CLIENTI_DEST'] . "</td>";
					if ($_REQUEST['mostra_importo'] == "Y")					
						echo "<td class=number>" . n($ar_tot['TOTALI']['IMPORTO'], 0) . "</td>";
					echo "</tr>";		
		
		
	echo "</table>";
 } //elenco
 
 
 
 
 function cmp($a, $b)
 {
 	return strcmp($a["descr"], $b["descr"]);
 }
 
 
 
 function cmp_el_l1($a, $b)
 {
 	//elenco: ordini it/ve per data
 	
 	if ($_REQUEST['evidenzia_inizio_carico'] == 'Y'){
		return strcmp("{$a['CSDTIC']}", "{$b['CSDTIC']}");
		if ($a['CSDTIC'] == 0) $a_cmp = $a['DATA'];
		 else $a_cmp = $a['CSDTIC'];
		if ($b['CSDTIC'] == 0) $b_cmp = $b['DATA'];
		 else $b_cmp = $b['CSDTIC'];
		
		return strcmp($a_cmp . sprintf("%06s", $a['CSHMIC']), $b_cmp . sprintf("%06s", $b['CSHMIC']));
		 
	}
 	
 	return strcmp(strtoupper($a["itinerario"] . $a["vmc"] . $a["CSHMPG"]) , strtoupper($b["itinerario"] . $b["vmc"] . $b["CSHMPG"]));
 }
 
 
 
 function n_cmp($a, $b)
 {
 	if ($a == $b) {
 		return 0;
 	}
 	return ($a < $b) ? -1 : 1;
 } 
 
 
 
 function out_cliente($row){
		$ret = "<span class=denominazione_cliente>{$row['TDDCON']}</span>";
		
		if ($row['DEST_NAZ_C'] == "ITA" || $row['DEST_NAZ_C'] == "IT"){
			$ind = "{$row['DEST_LOCA']} ({$row['DEST_PROV']})";
		} else {
			$ind = "{$row['DEST_LOCA']} ({$row['DEST_NAZ_D']})";
		}
		
 		return implode(", ", array($ret, $ind));
 } 
 
 function get_orario($row){
	if ($_REQUEST['evidenzia_inizio_carico'] == 'Y'){
		$ret = print_ora($row['CSHMIC']);
		if ( $row['CSDTIC'] != 0 && $row['DATA'] != $row['CSDTIC'] )
			$ret .= " del " . print_date($row['CSDTIC'], "%d/%m");
		return $ret;
	}
	else
		return $row['CSHMPG'];
}
 
 
 function note_carico($row){
	global $s;
	$carico = $s->get_carico_td($row);
	if (strlen(trim($carico['PSDESC']) . trim($carico['PSNOTE'])) > 0)
		return trim($carico['PSDESC']) . "  " . trim($carico['PSNOTE']);
	else
		return "";
 }
 
 function sceltadata($campo_data){
	switch ($campo_data){
		case "TDDTEP":
			return "(Data programmazione)";
		case "TDDTSP":
			return "(Data spedizione)";			
		case "SP.CSDTSC":
			return "(Data scarico)";			
	}
 }
 
 function sceltacampo_ora($campo_data){
 	switch ($campo_data){
 		case "TDDTEP":
 			return "SP.CSHMPG";
 		case "TDDTSP":
 			return "SP.CSHMPG";
 		case "SP.CSDTSC":
 			return "SP.CSHMSC";
 	}
 }
 
 
 function add_descr_report($desc){
	if (strlen($desc) > 0)
		return ' - ' . $desc;
	else return '';
			
 } 
 
 		
?>
  </div>
 </body>
</html>		