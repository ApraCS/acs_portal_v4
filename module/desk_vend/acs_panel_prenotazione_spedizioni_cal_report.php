<?php
require_once "../../config.inc.php";
set_time_limit(240);
$main_module = new Spedizioni();
$s = new Spedizioni();

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");


$ar_email_json = acs_je($ar_email_to);

$m_params = acs_m_params_json_decode();


function riga_vuota(){
	return "<td class=vuota>&nbsp;</td>";
}


// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
	require_once("acs_panel_todolist_data.php");
	
	$giorni_stadio = $s->get_giorni_stadio($da_data, $n_giorni);	
	
?>
<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
div#my_content h1{font-size: 22px; padding: 5px;}
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table.acs_report{border-collapse:collapse; width: 95%; margin-left: 20px; margin-right: 20px;}
table.acs_report td, table.acs_report th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}

.title{font-size: 20;}

/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{color: white; background-color: #000000; padding: 5px; margin-top: 10px; margin-bottom: 10px;}
table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}


/*table.acs_report tr td.colli{border-right: 2px solid gray;}*/ 

table.acs_report tr td.vuota{background-color: white; border: 0px; width: 10px;}

table.acs_report tr th.ora{background-color: #cccccc; font-weight: bold;}
table.acs_report tr td.nomeporta{font-size: 16px;}

div.ev {}

<?php if ($_REQUEST['dett_ord'] == 'Y'){ ?>
table.acs_report tr.liv_2 td{font-weight: bold; background-color: #a0a0a0;}
table.acs_report tr.liv_3 td{font-weight: bold; background-color: #f0f0f0;}
<?php } else { ?>
table.acs_report tr.liv_2 td{font-weight: bold; background-color: #f0f0f0;}
<?php } ?>
   
	@media print 
	{
	    .noPrint{display:none;}
	    .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'N';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<?php

$open_params =	strtr($_REQUEST['open_params'], array('\"' => '"'));
$open_params = json_decode($open_params);

$stabilimento = $open_params->c_stabilimento;            
$data		  = $open_params->form_values->f_data;       


$el_porte = $main_module->get_elenco_porte_stabilimento($stabilimento);

$sql = "SELECT TA_TRAS.TAKEY1 AS C_TRASP, TA_TRAS.TADESC AS D_TRASP, TA_ASPE.TARIF2 AS C_STAB,
		CSPROG, CSCITI, CSDTPG, CSHMPG, CSPORT, SUM(TDVOLU) AS S_VOLU, SUM(TDTOCO) AS S_TOCO, TA_VETT.TADESC AS D_VETT, TA_AUTO.TADESC AS D_AUTO
		FROM {$cfg_mod_Spedizioni['file_calendario']} SP
			
		INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
		ON SP.CSDT = TD.TDDT AND SP.CSPROG = TD.TDNBOF
		
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
		ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
		INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
		ON TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE

		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_AUTO
			ON TA_VETT.TADT = TA_AUTO.TADT AND TA_AUTO.TAKEY1 = SP.CSCAUT AND TA_AUTO.TATAID = 'AUTO'
			
			
		WHERE " . $main_module->get_where_std() . "
		AND CSDTPG = {$data} AND CSHMPG > 0
		AND TA_ASPE.TARIF2 = " . sql_t($stabilimento) . "
		GROUP BY TA_TRAS.TAKEY1, TA_TRAS.TADESC, TA_ASPE.TARIF2, CSPROG, CSCITI, CSDTPG, CSHMPG, CSPORT, TA_VETT.TADESC, TA_AUTO.TADESC

		";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);

		while ($r = db2_fetch_assoc($stmt)) {
			
			$evs[trim($r['CSPORT'])]['TOT']['S_VOLU'] += $r['S_VOLU'];
			$evs[trim($r['CSPORT'])]['TOT']['S_TOCO'] += $r['S_TOCO'];
			
			$evs[trim($r['CSPORT'])][substr(print_ora($r['CSHMPG']), 0, 2)][] = $r;
		}
		
//		echo "<pre>"; print_r($evs); exit;

?>



<div id='my_content'>



<?php if($_REQUEST['f_salto_si']=='Y'){
 	
 		//echo "<div class=\"page-break\"></div>";
 	?>




 <!--  stampa agenda -->
 <?php
 foreach($el_porte as $kporta => $porta) { 
 	
 	echo "<table class=acs_report>";  ?>
 	
 	<div>
 	<b class = "title">Programmazione porta/orario di uscita del <?php echo print_date($data); ?></b>
 	<h3>Stabilimento <?php echo $main_module->decod_std('START', $stabilimento); ?></h3>
 	<div style="text-align: right;">Data elaborazione: <?php echo Date('d/m/Y H:i'); ?></div>
 	<br/>
 	</div>
 	
 	<?php 
 	
 	echo "<tr class=liv1>";
 	//echo riga_vuota();
 	echo "<td class=number width=30>Ora</td>";
 	echo "<td class=nomeporta width=300>Porta " . $porta['text'] . "</td>";
 	echo "<td>MC</td>";
 	echo "<td>Colli</td>";
 	echo "</tr>";
 	 
 	 for($i = 6; $i <= 21; $i++){
 	 	echo "<tr>";
 	 	echo "<th class=\"ora number\">" . $i . ":00</th>";
 	 	//echo riga_vuota(); 	 
 	 
 	 	echo "<TD>";
 	 	
 	 	if (isset($evs[trim($porta['text'])][sprintf("%02s", $i)])){
 	 		foreach($evs[trim($porta['text'])][sprintf("%02s", $i)] as $r){
 	 			
 	 			$des = print_ora($r['CSHMPG']) . " " . $main_module->decod_std('ITIN', $r['CSCITI']) . " [" . $main_module->get_el_carichi_by_TDNBOF($r['CSPROG'], 'TN', 'N', 'N') . "] " . trim($r['D_VETT']);
 	 			
 	 			$des .= " " . trim($r['D_AUTO']);
 	 			//echo "<pre>"; print_r($evs[trim($porta['text'])][sprintf("%02s", $i)]); echo "</pre>";;
 	 			echo "<div class=ev>" . $des . "</div>";
 	 			
 	 		}
 	 	}
 	 	
 	 	echo "</td>";
 	 	
 	 	
 	 	//MC e colli
 	 	echo "<td class=number width=150>"; 	 	
 	 	if (isset($evs[trim($porta['text'])][sprintf("%02s", $i)])){
 	 		foreach($evs[trim($porta['text'])][sprintf("%02s", $i)] as $r){
 	 			//echo "<pre>"; print_r($evs[trim($porta['text'])][sprintf("%02s", $i)]); echo "</pre>";;  	 	
		 	 	echo "<div>" . n($r['S_VOLU'], 2) . "</div>";
 	 		}
 	 	}
 	 	echo "</td>";
 	 	
 	 	echo "<td class=\"number colli\" width=150>";
 	 	if (isset($evs[trim($porta['text'])][sprintf("%02s", $i)])){
 	 		foreach($evs[trim($porta['text'])][sprintf("%02s", $i)] as $r){
 	 			echo "<div>" . n($r['S_TOCO'], 0) . "</div>";
 	 		}
 	 	}
 	 	echo "</td>";
 	 	
 	 	
 	 }
 	 
 	echo "</tr>";
 	
 	//totali
 	echo "<tr class=liv_totale>";
 	echo "<td class=\"number colli\">TOT </td>";
 	
 		echo "<TD>&nbsp;</td>";
 		echo "<TD class=number>" . n($evs[trim($porta['text'])]['TOT']['S_VOLU'], 2) . "</td>";
 		echo "<TD class=number>" . n($evs[trim($porta['text'])]['TOT']['S_TOCO'], 0) . "</td>";
 	
 	echo "<tr>";

 	echo riga_vuota();

 	echo "</table>";
 	echo "<div class=\"page-break\"></div>";
 	
 }
 

 
 }else{?>
 
 <div>
 	<b class = "title">Programmazione porta/orario di uscita del <?php echo print_date($data); ?></b>
 	<h3>Stabilimento <?php echo $main_module->decod_std('START', $stabilimento); ?></h3>
 	<div style="text-align: right;">Data elaborazione: <?php echo Date('d/m/Y H:i'); ?></div>
 	<br/>
 </div>


<table class=acs_report>

 <tr class=liv1>
  	<td class=number width=30>Ora</td>
  	<?php
  	foreach($el_porte as $kporta => $porta){
  		echo riga_vuota();
  		echo "<td class=nomeporta>Porta " . $porta['text'] . "</td>";
  		echo "<td>MC</td>";
  		echo "<td>Colli</td>";
  	}
  	?>
 </tr>

 <!--  stampa agenda -->
 <?php
 for($i = 6; $i <= 21; $i++) {
 	echo "<tr>";
 	 echo "<th class=\"ora number\">" . $i . ":00</th>";
 	 
 	 foreach($el_porte as $kporta => $porta){
 	 	echo riga_vuota(); 	 	
 	 	echo "<TD>";
 	 	
 	 	if (isset($evs[trim($porta['text'])][sprintf("%02s", $i)])){
 	 		foreach($evs[trim($porta['text'])][sprintf("%02s", $i)] as $r){
 	 			$des = print_ora($r['CSHMPG']) . " " . $main_module->decod_std('ITIN', $r['CSCITI']) . " [" . $main_module->get_el_carichi_by_TDNBOF($r['CSPROG'], 'TN', 'N', 'N') . "] " . trim($r['D_VETT']);
 	 			
 	 			$des .= " " . trim($r['D_AUTO']);
 	 			//echo "<pre>"; print_r($evs[trim($porta['text'])][sprintf("%02s", $i)]); echo "</pre>";;
 	 			echo "<div class=ev>" . $des . "<div>";
 	 		}
 	 	}
 	 	
 	 	echo "</td>";
 	 	
 	 	//MC e colli
 	 	echo "<td class=number>"; 	 	
 	 	if (isset($evs[trim($porta['text'])][sprintf("%02s", $i)])){
 	 		foreach($evs[trim($porta['text'])][sprintf("%02s", $i)] as $r){
 	 			//echo "<pre>"; print_r($evs[trim($porta['text'])][sprintf("%02s", $i)]); echo "</pre>";;  	 	
		 	 	echo "<div>" . n($r['S_VOLU'], 2) . "</div>";
 	 		}
 	 	}
 	 	echo "</td>";
 	 	
 	 	echo "<td class=\"number colli\">";
 	 	if (isset($evs[trim($porta['text'])][sprintf("%02s", $i)])){
 	 		foreach($evs[trim($porta['text'])][sprintf("%02s", $i)] as $r){
 	 			echo "<div>" . n($r['S_TOCO'], 0) . "</div>";
 	 		}
 	 	}
 	 	echo "</td>";
 	 	
 	 	
 	 }
 	 
 	echo "</tr>";
 	
 	
 }
 
 //totali
 echo "<tr class=liv_totale>";
 	echo "<td class=\"number colli\">TOT";
 	foreach($el_porte as $kporta => $porta){
 		echo riga_vuota();
 		echo "<TD>&nbsp;</td>";
 		echo "<TD class=number>" . n($evs[trim($porta['text'])]['TOT']['S_VOLU'], 2) . "</td>";
 		echo "<TD class=number>" . n($evs[trim($porta['text'])]['TOT']['S_TOCO'], 0) . "</td>";
 	}
 echo "<tr>";	
 
?>

</table>

<?php  }?>

</div>
</body>
</html> 
<?php
 exit;	
}


  if ($_REQUEST['fn'] == 'open_form'){
  	$m_params = acs_m_params_json_decode();
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
     
            items: [
            
             {
							flex: 1,						 
							name: 'f_salto',
							xtype: 'checkboxgroup',
							fieldLabel: 'Salta pagina per porta',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_salto_si' 
		                          , boxLabel: ''
		                          , inputValue: 'Y'
		                          , checked: true
		                        }]														
						 }           
            ],
            
			buttons: [					
				{
		            text: 'Stampa',
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',		            
		            handler: function() {
		                this.up('form').submit({
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                           open_params: JSON.stringify(<?php echo acs_je($m_params); ?>),
	                           form_values: Ext.encode(this.up('form').getValues())
	                        }
	                  });
	                  
	                   this.up('window').close(); 
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}
?>