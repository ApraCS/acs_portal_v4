<?php   

require_once "../../config.inc.php";


$s = new Spedizioni();
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'get_parametri_form'){

	//Elenco opzioni "Area spedizione"
	$ar_area_spedizione = $s->get_options_area_spedizione();
	$ar_area_spedizione_txt = '';

	$ar_ar2 = array();
	foreach ($ar_area_spedizione as $a)
		$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
		$ar_area_spedizione_txt = implode(',', $ar_ar2);
		
		
    $scelta_data_evasione = true;
    if ($m_params->filter_type == 'desk_prod')
        $scelta_data_evasione = false;
    
    $solo_ordini_con_carico = false;
    if ($m_params->filter_type == 'desk_prod')
        $solo_ordini_con_carico = true;
?>

{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: false,
	title: '',
	url: '../desk_vend/acs_calendario_settimanale_spedizioni_report.php',
	buttonAlign:'center',
	buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "REP_CAL_SPE");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	            handler: function() {
	                form = this.up('form').getForm();
	                
	                if (form.isValid()){	                	                
		                form.submit({
				                standardSubmit: true,
				                submitEmptyText: false,
		                        method: 'POST',
								target : '_blank',
								params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())}		                        
		                });
	                }	                
	            }
	        }],             
            
            items: [{
					xtype: 'fieldset',
	                title: 'Seleziona data',
	                defaultType: 'textfield',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	             	items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data da'
						   , name: 'f_data_da'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin: "5 10 5 10"
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data a'
						   , name: 'f_data_a'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin: "5 10 5 10"
						}
				<?php if ($scelta_data_evasione) { ?>		
				, {
                    xtype: 'radiogroup',
                    anchor: '100%',
                    fieldLabel: '',
                    items: [
                        {
                            xtype: 'radio'
                          , name: 'sceltadata' 
                          , boxLabel: 'Programmata'
                          , inputValue: 'TDDTEP'
                          , checked: true
                        }
                        
                        
                        , {
                            xtype: 'radio'
                          , name: 'sceltadata' 
                          , boxLabel: 'Spedizione'
                          , inputValue: 'TDDTSP'                          
                        }
                       
                   ]
                }
                <?php }?>
                
                
					]
	            }       
	            
	            
                , {
					xtype: 'fieldset',
	                title: 'Selezione area/divisione',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
	             {
							name: 'f_areaspe',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 
								    ] 
							}						 
						},   	{
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?>	
								    ] 
							}						 
						},  {
							name: 'f_itinerario',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
								    ] 
							}						 
						}
	             
	             ]
	             }, {
					xtype: 'fieldset',
					border: true,
	                title: '',
	                defaultType: 'textfield',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	                items: [
	                
	                  <?php if ($solo_ordini_con_carico) { ?>
	                    {xtype: 'hiddenfield', name: 'f_solo_ordini_con_carico', value: 'Y'}
	                  <?php } else { ?>
	                	{
							flex: 1,						 
							name: 'f_solo_bloccati',
							xtype: 'checkboxgroup',
							fieldLabel: 'Solo ordini con carico',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_solo_ordini_con_carico' 
		                          , boxLabel: ''
		                          , checked: true
		                          , inputValue: 'Y'
		                        }]														
						 }
					 <?php } ?>	 
						 
						 
						 , {
							flex: 1,						 
							name: 'f_dettaglio',
							xtype: 'checkboxgroup',
							fieldLabel: 'Dettaglio destinazioni',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_dettaglio_destinazioni' 
		                          , boxLabel: ''
		                          , inputValue: 'Y'
		                        }]														
						 }
					]
	            }                                       
            ]
        }

	
]}

<?php	
 exit;	
}