<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");

$ar_email_json = acs_je($ar_email_to);

?>


<html>
<head>
	<title>RIEPILOGO RESI CLIENTE DA RITIRARE</title>
	<!-- <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/report.css"); ?> />  -->
	
	<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

	<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
 	<script type="text/javascript" src="../../../extjs/ext-all.js"></script>
	<script src=<?php echo acs_url("js/acs_js_report.js") ?>></script>  
	
	<style type="text/css">
	
	table{border-collapse:collapse; width: 100%;}
	table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
	.number{text-align: right;}

	.acs_report{font-family: "Times New Roman",Georgia,Serif;}
	
	tr.liv1 td{background-color: #cccccc; font-weight: bold;}
	tr.liv3 td{font-size: 9px;}
	
	tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
	tr.ag_liv2 td{background-color: #DDDDDD;}
	tr.ag_liv1 td{background-color: #ffffff;}
	tr.ag_liv0 td{font-weight: bold;}
	
	tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
	tr.ag_liv_data th{background-color: #333333; color: white;}
	
	h2.acs_report_title{font-size: 20px; padding: 10px;}
	
	.grassetto{font-weight: bold;}   
	.number{text-align: right;}
	
	@media print
	{
		.noPrint{display:none;}
	}
	
	
	</style>
	</head>
<body>


	<div class="page-utility noPrint">
		<?php 
		
					$bt_fascetta_print = 'Y';
					$bt_fascetta_email = 'Y';
					$bt_fascetta_excel = 'Y';
					$bt_fascetta_close = 'Y';
					include  "../../templates/bottoni_fascetta.php";
		
		?>	
	</div>
	
	<div id='my_content'>	
		<h2 class="acs_report_title">RIEPILOGO MENSILE ANZIANIT&Agrave RESI </h2>
		
		<p align="right"> Data elaborazione:  <?php echo date('d/m/Y  H:i');?>
		
		
		<?php
		
		
		function add_array_valori_liv(&$ar, $r){
			$ar["IMPORTO"] 		+= $r['IMPORTO'];
			$ar["N_RIGHE"] += 1;
		}
		
		
		function make_tr($n_td,  $array_value, $tr_class = " "){
		
			$ar_class = array();
			$ar_class[5] = $ar_class[6] = 'number';
		
			echo "<tr class=$tr_class>";
			for ($i=0; $i<$n_td; $i++){
				echo "<td class=\"{$ar_class[$i]}\">". $array_value[$i] ."</td>";
			}
			echo "</tr>";
		}
		
		$form_ep = json_decode($_REQUEST['form_ep']);
		
		/**************** INTERROGAZIONE DB E CHECK FILTRI   *********************************************************************************/
		
		$field = "SUBSTRING(RDODER,1,4) AS ANNO_RESO, SUBSTRING(RDODER,5,2) AS MESE_RESO, TDCAG1 AS C_AG, TDDAG1 AS D_AG, 
							TDCCON AS C_CLI, TDDCON AS DEN_CLI, RDQTA2 AS IMPORTO";
		$field_group_by = "SUBSTRING(RDDES2,68,4 ), SUBSTRING(RDDES2,65,2 ), TDCAG1, TDDAG1, TDCCON ";
		
		$m_where = "TDOTID = 'VO' AND TDOINU = '' AND TDOADO = 0 AND TDSWPP = 'R'";
		$m_where .= sql_where_by_combo_value("TDASPE", $form_ep->area_spedizione);
		$m_where .= sql_where_by_combo_value("TDCCON", $form_ep->f_cliente_cod);
		$m_where .= sql_where_by_combo_value("TDCITI", $form_ep->itinerario_spedizione);
		$m_where .= sql_where_by_combo_value("TDCAG1", $form_ep->agente);
		
		$sql = "SELECT $field
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
			INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD  
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
		WHERE  $m_where
		ORDER BY SUBSTRING(RDODER,1,4) ASC, SUBSTRING(RDODER,5,2)  ASC, TDDAG1 ASC, TDDCON ASC";
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		
		
		/**************** CREAZIONE ALBERO   *********************************************************************************/
		
		$ar = array();
		while ($r = db2_fetch_assoc($stmt)){
			
			$liv0 = "TOTALE";
			$liv1 = implode("_", array(trim($r['ANNO_RESO'])));
			$liv2 = implode("_", array(trim($r['MESE_RESO'])));
			$liv3 = implode("_", array(trim($r['COD_CLIENTE']), trim($r['C_AG'])));
			
			
			// ----LIV0
			$d_ar = &$ar;
			$c_liv = $liv0;
			if (!isset($d_ar[$c_liv]))
				$d_ar[$c_liv] = array(
						"DESCRIZIONE"  => 'TOTALE GENERALE',
						"children"=>array());
			add_array_valori_liv($d_ar[$c_liv], $r);
			$d_ar = &$d_ar[$c_liv]['children'];
			
			// ----LIV1
			$c_liv = $liv1;
			if (!isset($d_ar[$c_liv]))
				$d_ar[$c_liv] = array("ANNO" => $r['ANNO_RESO'], "children"=>array());
			add_array_valori_liv($d_ar[$c_liv], $r);
			$d_ar = &$d_ar[$c_liv]['children'];
				
			// --- LIV2
			$c_liv = $liv2;
			if (!isset($d_ar[$c_liv]))
				$d_ar[$c_liv] = array("MESE" => $r['MESE_RESO'], "ANNO" => $r['ANNO_RESO'], "children"=>array());
			add_array_valori_liv($d_ar[$c_liv], $r);
			$d_ar = &$d_ar[$c_liv]['children'];
			
			// DETTAGLIO
			if ($_REQUEST['rb_anche_dettaglio_articoli'] == 'Y'){
				//  --- LIV3
				$c_liv = $liv3;
				if (!isset($d_ar[$c_liv]))
					$d_ar[$c_liv] = array('AGENTE' => $r['D_AG'] .  " [". $r['C_AG'] . "]", 'COD_CLIENTE' => $r['C_CLI'], 'DEN_CLIENTE' => $r['DEN_CLI']);
				add_array_valori_liv($d_ar[$c_liv], $r);	
			}
	
		}
		
		/*
		echo "<pre>";
		print_r($ar);
		exit;
		*/
		
		
		
		
		
		
		
		/**************** STAMPA REPORT   *********************************************************************************/
		
		echo "<table class=\"acs_report\">";
		
		echo "<tr class=\"ag_liv_data\">";
		echo "<th colspan=\"2\">Periodo reso</th> 
				<th rowspan=\"2\">Agente</th> 
				<th rowspan=\"2\">Cod cliente</th>
				<th rowspan=\"2\">Denominazione cliente</th>
	         	<th rowspan=\"2\">Importo</th> 
				<th rowspan=\"2\">Nr. righe</th>";
		echo "</tr>";
		
		echo "<tr class=\"ag_liv_data\">";
		echo "<th>Anno</th> <th>Mese</th> ";
		echo "</tr>";
		
		$nr_tr = 7;
		
		// TOTALE GENERALE
		make_tr($nr_tr,
				array("", "", "TOTALE GENERALE", "", "", n($ar['TOTALE']['IMPORTO']), $ar['TOTALE']['N_RIGHE']),
				'liv1');
		
		if(count($ar['TOTALE']['children']) != 0){
			
			//ANNO
			foreach ($ar['TOTALE']['children'] as $anno){
				make_tr($nr_tr,
					array($anno['ANNO'], "", "", "", "",
							n($anno['IMPORTO']),  $anno['N_RIGHE']),
						'ag_liv3');
									
				//MESE					
				foreach ($anno['children'] as $anno_mese){
					make_tr($nr_tr,
						array('', ucfirst(print_month($anno_mese['MESE'])) , "", "", "",
								n($anno_mese['IMPORTO']),  $anno_mese['N_RIGHE']),
							$_REQUEST['rb_anche_dettaglio_articoli'] == 'Y' ? 'ag_liv2' : '');

					if ($_REQUEST['rb_anche_dettaglio_articoli'] == 'Y'){
						
						//AGENTE_CLIENTE
						foreach ($anno_mese['children'] as $cliente){
							make_tr($nr_tr,
									array("", " ", $cliente['AGENTE'], $cliente['COD_CLIENTE'], $cliente['DEN_CLIENTE'],
											n($cliente['IMPORTO']),  $cliente['N_RIGHE']),
										'');
						}
							
					}
										
				}
			
			}
		} //fine if
		
		echo "</table>";
		?>
		
	</div>


</body>