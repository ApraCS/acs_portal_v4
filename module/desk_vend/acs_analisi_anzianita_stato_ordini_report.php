<?php

require_once("../../config.inc.php");
require_once("acs_analisi_anzianita_stato_ordini_include.php");

$s = new Spedizioni();
$main_module = new Spedizioni();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$ar=crea_ar_tree_anzianita_stato_ordini($form_values); 


echo "<div id='my_content'>";
echo "
  		<div class=header_page>
			<H2>Analisi anzianit&agrave; stato ordini corrente</H2>
 		</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";

echo "<tr class='liv_data' ><th>Stato ordine/Data assegnazione</th>
		<th><img src=" . img_path("icone/48x48/power_black.png") . " height=20></th>
  		<th><img src=" . img_path("icone/48x48/info_blue.png") . " height=20></th>
   		<th><img src=" . img_path("icone/48x48/info_black.png") . " height=20></th>
   		<th><img src=" . img_path("icone/48x48/label_blue_new.png") . " height=20></th>
		<th>Riferimento</th>
		<th>Tp</th>
		<th>Data ricezione</th>
		<th>Pr</th>
		<th> Cons. richiesta</th>
		<th>Dispon.</th>
		<th>Evas./Programm.</th>
		<th> O </th>
   		<th> P </th>
   		<th> M </th>
   		<th> A </th>
   		<th> CV </th>
   		<th class=number> Importo </th>
   		<th> GG lavorativi </th>
  		</tr>";

foreach ($ar as $kar => $r){
	
	echo "<tr class=liv_totale><td>".$r['task']. "</td>";
   		        
   				if($r['fl_bloc'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
				}else if($r['fl_bloc'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
				}else if($r['fl_bloc'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
				}elseif($r['fl_bloc'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
	
   				if($r['fl_art_manc'] == 5){
					echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
				}else if($r['fl_art_manc'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
				}else if($r['fl_art_manc'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart.png") . " height=15></td>";
				}else if($r['fl_art_manc'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
				}elseif($r['fl_art_manc'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
				
				if($r['art_da_prog'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_black.png") . " height=15></td>";
				}else {
					echo "<td>&nbsp;</td>";
				}
   				
   		    echo "<td>&nbsp;</td>
   				  <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td class=number>".$r['n_O']."</td>
    		      <td class=number>".$r['n_P']."</td>
   		 		  <td class=number>".$r['n_M']."</td>
   				  <td class=number>".$r['n_A']."</td>
   				  <td class=number>".$r['n_CV']."</td>
   			       <td class=number>".n($r['importo'],2)."</td>
   		           <td class=number>".$r['gg_lav']."</td>
    		      </tr>";
	
	foreach ($r['children'] as $kar1 => $r1){
		
		echo "<tr class=liv2><td>".$r1['task']. "</td>";
		
				if($r1['fl_bloc'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
				}else if($r1['fl_bloc'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
				}else if($r1['fl_bloc'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
				}elseif($r1['fl_bloc'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
	
   				if($r1['fl_art_manc'] == 5){
					echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
				}else if($r1['fl_art_manc'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
				}else if($r1['fl_art_manc'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart.png") . " height=15></td>";
				}else if($r1['fl_art_manc'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
				}elseif($r1['fl_art_manc'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
				
				if($r1['art_da_prog'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_black.png") . " height=15></td>";
				}else {
					echo "<td>&nbsp;</td>";
				}
				
   		    echo "<td>&nbsp;</td>
   				  <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td class=number>".$r1['n_O']."</td>
    		      <td class=number>".$r1['n_P']."</td>
   		 		  <td class=number>".$r1['n_M']."</td>
   				  <td class=number>".$r1['n_A']."</td>
   				  <td class=number>".$r1['n_CV']."</td>
   			       <td class=number>".n($r1['importo'],2)."</td>
   		           <td class=number>".$r1['gg_lav']."</td>
    		      </tr>";
		
		foreach ($r1['children'] as $kar2 => $r2){
			
			echo "<tr class=liv1><td>".$r2['task']. "</td>";
			
			if($r2['fl_bloc'] == 4){
				echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
			}else if($r2['fl_bloc'] == 3){
				echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
			}else if($r2['fl_bloc'] == 2){
				echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
			}elseif($r2['fl_bloc'] == 1){
				echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
			}else{
				echo "<td>&nbsp;</td>";
			}
			
			if($r2['fl_art_manc'] == 5){
				echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
			}else if($r2['fl_art_manc'] == 4){
				echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
			}else if($r2['fl_art_manc'] == 3){
				echo "<td><img src=" . img_path("icone/48x48/shopping_cart.png") . " height=15></td>";
			}else if($r2['fl_art_manc'] == 2){
				echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
			}elseif($r2['fl_art_manc'] == 1){
				echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
			}else{
				echo "<td>&nbsp;</td>";
			}
			
			if($r2['art_da_prog'] == 1){
				echo "<td><img src=" . img_path("icone/48x48/info_black.png") . " height=15></td>";
			}else {
				echo "<td>&nbsp;</td>";
			}
			
   			echo "<td>&nbsp;</td>
   				  <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td>
   		          <td>".print_date($r2['cons_rich'])."</td>
   				  <td>&nbsp;</td>
   		          <td>".print_date($r2['cons_prog'])."</td>
    		      <td class=number>".$r2['n_O']."</td>
    		      <td class=number>".$r2['n_P']."</td>
   		 		  <td class=number>".$r2['n_M']."</td>
   				  <td class=number>".$r2['n_A']."</td>
   				  <td class=number>".$r2['n_CV']."</td>
   			       <td class=number>".n($r2['importo'],2)."</td>
   		           <td class=number>".$r2['gg_lav']."</td>
    		      </tr>";
		
			foreach ($r2['children'] as $kar3 => $r3){
				
				echo "<tr class=liv0><td>".$r3['task']. "</td>";
				
				if($r3['fl_bloc'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
				}else if($r3['fl_bloc'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
				}else if($r3['fl_bloc'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
				}elseif($r3['fl_bloc'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
				
				
				if($r3['fl_art_manc'] == 5){
					echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
				}else if($r3['fl_art_manc'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
				}else if($r3['fl_art_manc'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart.png") . " height=15></td>";
				}else if($r3['fl_art_manc'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
				}elseif($r3['fl_art_manc'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
				
				if($r3['art_da_prog'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_black.png") . " height=15></td>";
				}else {
					echo "<td>&nbsp;</td>";
				}
				
   			echo "<td>&nbsp;</td>
   				  <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td>
   		          <td>".print_date($r3['cons_rich'])."</td>
   				  <td>&nbsp;</td>
   		          <td>".print_date($r3['cons_prog'])."</td>
    		      <td class=number>".$r3['n_O']."</td>
    		      <td class=number>".$r3['n_P']."</td>
   		 		  <td class=number>".$r3['n_M']."</td>
   				  <td class=number>".$r3['n_A']."</td>
   				  <td class=number>".$r3['n_CV']."</td>
   			       <td class=number>".n($r3['importo'],2)."</td>
   		           <td class=number>".$r3['gg_lav']."</td>
    		      </tr>";
				
				
				foreach ($r3['children'] as $kar4 => $r4){
					
				echo "<tr><td>".$r4['task']. "</td>";
				
				if($r4['fl_bloc'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
				}else if($r4['fl_bloc'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
				}else if($r4['fl_bloc'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
				}elseif($r4['fl_bloc'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
				
				if($r4['fl_art_manc'] == 5){
					echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
				}else if($r4['fl_art_manc'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
				}else if($r4['fl_art_manc'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart.png") . " height=15></td>";
				}else if($r4['fl_art_manc'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
				}elseif($r4['fl_art_manc'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}   			
				
				if($r4['art_da_prog'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_black.png") . " height=15></td>";
				}else {
					echo "<td>&nbsp;</td>";
				}
				
				if($r4['fl_new'] == 'Y'){
					echo "<td><img src=" . img_path("icone/48x48/label_blue_new.png") . " height=15></td>";
				}elseif($r4['fl_new'] == 'O'){
					echo "<td><img src=" . img_path("icone/48x48/label_new_grey.png") . " height=15></td>";
				}elseif($r4['fl_new'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/timer_blue.png") . " height=15></td>";
				}else {
					echo "<td>&nbsp;</td>";
				}
				
			echo "<td>".$r4['riferimento']."</td>
   				  <td>".$r4['tipo']."</td>
   		          <td>".print_date($r4['data_reg'])."</td>
    		      <td>".$r4['priorita']."</td>
   		          <td>".print_date($r4['cons_rich'])."</td>
   				   <td>".print_date($r4['data_disp'])."</td>
   		          <td>".print_date($r4['cons_prog'])."</td>
    		      <td class=number>".$r4['n_O']."</td>
    		      <td class=number>".$r4['n_P']."</td>
   		 		  <td class=number>".$r4['n_M']."</td>
   				  <td class=number>".$r4['n_A']."</td>
   				  <td class=number>".$r4['n_CV']."</td>
   			       <td class=number>".n($r4['importo'],2)."</td>
   		           <td class=number>".$r4['gg_lav']."</td>
    		      </tr>";
			
				}
				
			}
			
		}
	
	}
	
	
}











