<?php

require_once "../../config.inc.php";
require_once "acs_riepilogo_spedizioni.php";



$s = new Spedizioni();
$main_module = new Spedizioni();

$da_form = acs_m_params_json_decode();

// ******************************************************************************************
// REPORT
// ******************************************************************************************
$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php

$open_request = strtr($_REQUEST['open_request'], array('\"' => '"'));
$open_request = json_decode($open_request);

$form_values =	$open_request->form_values;
$list_selected_carico = $open_request->list_selected_carico;


		//controllo opzione programmato o spedizione
		if($form_values->data_riferimento == "TDDTEP"){
			$tipo_data="TDDTEP";}
		if($form_values->data_riferimento == "TDDTSP"){
			$tipo_data="TDDTSP";}
			
			if($tipo_data==0){
				$tipo_data="TDDTEP";
			}
			
		
		$sql_where.= " WHERE " . $s->get_where_std();
		
		$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);
		
		$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);
		
		$sql_where.= sql_where_by_combo_value('TD.TDCITI', $form_values ->f_itinerario);
		
		$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values ->f_cliente);
		
		//$sql_where .=sql_where_by_combo_value('CS.CSDTRG', $form_values->f_data);
	
		
		//controllo opzione ordini con carico
		if ($form_values->f_solo_ordini_con_carico == 'Y')
			$sql_where .= " AND TD.TDFN01 = 1 ";
			
		if ($form_values->f_solo_ordini_con_carico == 'Y')
			$sql_where .= " AND TD.TDFN01 = 1 ";
			

			if(count($list_selected_carico)>0){
				
				$sql_where.= " AND ( 1=2 ";
				foreach($list_selected_carico as $c){
					
					$car = explode("_", $c->num_carico);
					
					$anno_carico= $car[0];
					$num_carico= $car[2];
					
					$num_spe = $c->nr_spe;
					$area_spe = $c->area_spe;
			
					$sql_where.= " OR (TD.TDAACA = {$anno_carico} AND TD.TDNRCA= {$num_carico} AND TD.TDNBOC={$num_spe}
							AND TD.TDASPE='{$area_spe}')";
					
			}
			
			$sql_where.= ")";
			
			}
			
		
		

	$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
		         LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.{$tipo_data}
				 LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				     ON TD.TDDT=CS.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
				 LEFT OUTER JOIN (
                               SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
                               FROM {$cfg_mod_Spedizioni['file_righe']} RDO
                               WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
                               GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO) RD
                      ON TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO
				 LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe']} RD2
					ON TDDT=RD2.RDDT AND TDOTID=RD2.RDOTID AND TDOINU=RD2.RDOINU AND TDOADO=RD2.RDOADO AND TDONDO=RD2.RDONDO AND RD2.RDTPNO='RGMOB'                      
                 LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS 
                    ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA";
	$sql_WHERE= $sql_where;

	$sql = "SELECT TD.*, SP.*, RD.*, PS.*, RD2.RDQTA2 AS MOBILI, RD2.RDQTA3 AS ELETR " . $sql_FROM . $sql_WHERE	. " ORDER BY CS.CSDTRG DESC, TDSECA, TDDCON, TDDLOC";

	
	$stmt = db2_prepare($conn, $sql);	
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
 
    $liv0_in_linea = null;
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$liv0 = implode("|", array($r['TDDTEP'], $r['PSDESC'], $r['CSCVET'], $r['CSCAUT'], $r['CSCCON']));
		$liv1 = implode("|", array($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA'], $r['TDSECA']));		
		$liv2 = implode("|", array($r['TDCCON'], $r['TDCDES']));		
		$liv3 = implode("|", array($r['TDDT'], $r['TDOTID'], $r['TDOINU'], $r['TDOADO'], $r['TDONDO']));		
		
		//data|itinerario|vettore
		$t_ar = &$ar;
		$l = $liv0;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r); 					  								  
			$t_ar = &$t_ar[$l]['children'];
		
		//carico
		$l = $liv1;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>implode("_", array($r['TDAACA'], $r['TDTPCA'], $r['TDNRCA'])), "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar[$l]['carico'] = $s->get_carico_td($r);
			$t_ar[$l]['orario_carico'] = $r['CSHMPG'];			 								  
			$t_ar = &$t_ar[$l]['children'];
		 								  								  
		//cliente|dest
		$l = $liv2;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];								  								  

		//ordine
		$l = $liv3;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];			
		
	} //while

	
//STAMPO
 $cl_liv_cont = 0;
 if ($_REQUEST['stampa_dettaglio_ordini'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
 $liv2_row_cl = ++$cl_liv_cont; 
 $liv1_row_cl = ++$cl_liv_cont;
 
echo "<div id='my_content'>"; 
foreach ($ar as $kl0 => $l0){
	echo liv0_intestazione_open($l0['record']);

	  		foreach ($l0['children'] as $kl1 => $l1){
//				echo liv1_intestazione_open($l1, $liv1_row_cl);	  			
				
					global $cambia_seca, $n_ord_in_seca;
					$cambia_seca = 'Y';

					//conto le righe che stampero'
					$n_ord_in_seca = 0;
					foreach ($l1['children'] as $kl2 => $l2)
						$n_ord_in_seca += count($l2['children']);							

			  		foreach ($l1['children'] as $kl2 => $l2){
						echo liv2_intestazione_open($l2, $liv2_row_cl);
						
//						if ($_REQUEST['stampa_dettaglio_ordini']=="Y")
						$old_itin="abc";
				  		foreach ($l2['children'] as $kl3 => $l3){
							echo liv3_intestazione_open($l3, $liv3_row_cl);
				  		  }	
							
					}			
			}		
	
	echo liv0_intestazione_close($l0);	
	echo "<div class=\"page-break\"></div>";
}

echo "</div>";

 		
?>

 </body>
</html>



<?php


function somma_valori($ar, $r){
 //il volume e' aumentato in base alla percentuale passata (recuperata prima dall'itinerario)
 $ar['COUNT'] 	+= 1; 
 $ar['VOLUME'] 	+= $r['TDVOLU'] * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
 $ar['COLLI'] 	+= $r['TDTOCO'];
 $ar['COLLI_PROD'] 	+= $r['RDQTA'];
 $ar['PALLET'] 	+= $r['TDBANC'];
 $ar['IMPORTO'] += $r['TDTIMP']; 
 $ar['PESO'] 	+= $r['TDPLOR'];
 
 
 if (trim($r['TDFU03']) == 'C') {
 	$ar['S_MOBILI'] += $r['MOBILI'];
 	$ar['S_MOBILI'] += $r['ELETR'];
 }else if ($r['TDFU03'] == 'N' || trim($r['TDFU03']) == '') {
 } else {
 	$ar['S_MOBILI'] += $r['MOBILI'];
 	$ar['S_ELETR'] += $r['ELETR'];
 }
 
 
 return $ar;  
}


function liv3_intestazione_open($l, $cl_liv){
	global $cambia_cliente, $cambia_seca, $n_ord_in_seca, $n_ord_in_cliente, $old_itin;
	

	
	if ($_REQUEST['indirizzo_di'] == 'ind_destinazione'){
		$m_loca = 'TDLOCA';
		$m_prov = 'TDPROV';
		$m_cap  = 'TDCAP';
	} else {
		$m_loca = 'TDDLOC';
		$m_prov = 'TDPROD';
		$m_cap  = 'TDDCAP';
	}	
	

	$ret = "<tr class=liv{$cl_liv}>";
		
	if ($cambia_seca == 'Y')
		$ret .= "<td rowspan = {$n_ord_in_seca} valign=top>{$l['record']['TDSECA']}</td>";
	$cambia_seca = 'N';
	
	if (strlen(trim($l['record']['TDPROD']))==0){ 
		$local=trim($l['record']['TDNAZD']);
	}else{
		$local=trim($l['record']['TDPROD']);
	}
	
	if ($cambia_cliente == 'Y'){	
		$ret .= "<td valign=top rowspan={$n_ord_in_cliente} class=grassetto>{$l['record']['TDDCON']} </td>";	
		$cambia_cliente = 'N';
	} else {
		//$ret .= "<td colspan=2>&nbsp;</td>";
	}	
	
	if ("{$l['record']['TDDLOC']}[{$local}]" != $old_itin){
		$ret .= "<td valign=top>{$l['record']['TDDLOC']}[{$local}] </td>";
		$old_itin = "{$l['record']['TDDLOC']}[{$local}]";
	} else {
		$ret .= "<td colspan=1>&nbsp;</td>";
	}
	
	//$ret .= "<td>{$l['record']['TDDLOC']}[{$local}]</td>";
	$ret .= "<td>{$l['record']['TDVSRF']}</td>";	
	$ret .= "<td>{$l['record']['TDOTPD']}</td>";		
	$ret .= "<td>{$l['record']['TDOADO']}</td>";	
	$ret .= "<td>{$l['record']['TDONDO']}</td>";	
	$ret .= "<td>{$l['record']['TDDVN1']}</td>";
	
	$ret .= "<td class=number>" . n($l['record']['TDTIMP'], 2) . "</td>";
	$ret .= "<td class=number>" . n($l['val']['S_MOBILI'], 2) . "</td>"; //produzione
	$ret .= "<td class=number>" . n($l['val']['S_ELETR'], 2) . "</td>"; //rivendita
	$ret .= "<td>{$l['record']['TDDPAG']}</td>"; //pagamento
	$ret .= "<td>{$l['record']['TDFU03']}</td>"; //Preferenza
		
	$ret .= "<td class=number>" . n($l['val']['VOLUME'], 2) . "</td>";	

	$ret .= "<td class=number>" . n($l['val']['COLLI'], 0) . "</td>";
	
	if ($_REQUEST['stampa_gg_rit']=='Y') $ret .= "<td class=number>{$l['record']['TDGGRI']}</td>";	
	
	$ret .= "</tr>";
 return $ret;
}



function liv2_intestazione_open($l, $cl_liv){
	global $cambia_cliente, $n_ord_in_cliente;
	$cambia_cliente = 'Y';
	$n_ord_in_cliente = count($l['children']); 
	
}




function liv0_intestazione_open($r){
	global $s, $spedizione, $itinerario;
	global $form_values;

	if ($form_values->data_riferimento == 'TDDTSP')
		$f_data = 'TDDTSP';
	else
		$f_data = 'TDDTEP';
	
	
	if ($_REQUEST['f_stampa_importi']=='Y'){
	
	$ret = "<table>
        <table><tr class=su>
		<td colspan=17 style=\"font-weight:bold; font-size:15px; padding-top:3px;\"> LISTA SEQUENZE DI CARICO <br>" .$s->decod_std('ITIN', $r['PSDESC'])."</td>
		<td style=\"text-align: right; vertical-align:top; font-size:15px; font-weight:bold; padding-top:3px;\">Carico: " .$r['TDAACA']."_".$r['TDTPCA']."_".$r['TDNRCA']. "</td>
        </tr>
         <tr class=giu>
		<td colspan=17 style=\"font-weight:bold; padding-bottom:3px;\"> Spedizione del " . print_date($r[$f_data])." ".$s->decod_std('AUTR', $r['CSCVET']) . " - " . $s->decod_std('AUTO', $r['CSCAUT']) . " " .	$s->decod_std('COSC', $r['CSCCON']) ."</td>
		<td style=\"text-align: right; vertical-align:bottom; font-size:13px; padding-bottom:3px;\">Data elaborazione: " .  Date('d/m/Y H:i') . "</td>
        </tr></table>
        </table>		
			
			
			
			<table class=int1>
			 <tr class=liv_totale>"; 
	
	}else{
		
		$ret = "<table>
        <table><tr class=su>
		<td colspan=16 style=\"font-weight:bold; font-size:15px; padding-top:3px;\"> LISTA SEQUENZE DI CARICO <br>" .$s->decod_std('ITIN', $r['PSDESC'])."</td>
		<td style=\"text-align: right; vertical-align:top; font-size:15px; font-weight:bold; padding-top:3px;\">Carico: " .$r['TDAACA']."_".$r['TDTPCA']."_".$r['TDNRCA']. "</td>
        </tr>
         <tr class=giu>
		<td colspan=16 style=\"font-weight:bold; padding-bottom:3px;\"> Spedizione del " . print_date($r[$f_data])." ".$s->decod_std('AUTR', $r['CSCVET']) . " - " . $s->decod_std('AUTO', $r['CSCAUT']) . " " .	$s->decod_std('COSC', $r['CSCCON']) ."</td>
		<td style=\"text-align: right; vertical-align:bottom; font-size:13px; padding-bottom:3px;\">Data elaborazione: " .  Date('d/m/Y H:i') . "</td>
        </tr></table>
        </table>
		
		
		
			<table class=int1>
			 <tr class=liv_totale>";
		
		
	}
	
	
	  		  $ret .= "<td class=grassetto>Seq.</td>";	  		  	  		  
	  		  $ret .= "<td class=grassetto>Cliente</td>";
	  		  $ret .= "<td class=grassetto>Localit&agrave;</td>";
	  		  $ret .= "<td class=grassetto>Riferimento</td>";
	  		  $ret .= "<td class=grassetto colspan=3>Ordine</td>";
	  		  $ret .= "<td class=grassetto>Modello</td>";
	  		  $ret .= "<td class=grassetto>Importo</td>";
	  		  $ret .= "<td class=grassetto>Produzione</td>";
	  		  $ret .= "<td class=grassetto>Rivendita</td>";
	  		  $ret .= "<td class=grassetto>Pagamento</td>";
	  		  $ret .= "<td class=grassetto>P</td>";
	  		  	  		  
	  		  $ret .= "<td class=grassetto>Volume</td>";	  	
			  $ret .= "<td class=grassetto>Colli</td>";
		
			//  if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td class=grassetto>Imp.</td>";			  
			  if ($_REQUEST['stampa_gg_rit']=='Y') $ret .= "<td class=grassetto>GG Rit</td>";	
	     $ret .= "</tr>";
	     	
 return $ret;	
}


function liv0_intestazione_close($l){

	$ret  = "<tr class=liv_totale>";
	$ret .= "<td colspan=4>Totale carico</td>";
	$ret .= "<td colspan=3 class=\"grassetto number\" style= text-align:center;>[Nr ordini " . n($l['val']['COUNT'],  0) . "]</td>";
	$ret .= "<td colspan=1>&nbsp;</td>";	
	$ret .= "<td class=number>" . n($l['val']['IMPORTO'],  2) . "</td>";
	$ret .= "<td class=number>" . n($l['val']['S_MOBILI'], 2) . "</td>";
	$ret .= "<td class=number>" . n($l['val']['S_ELETR'],  2) . "</td>";
	$ret .= "<td colspan=2>&nbsp;</td>";
	$ret .= "<td class=number>" . n($l['val']['VOLUME'], 2) . "</td>";	
	$ret .= "<td class=number>" . n($l['val']['COLLI'],  0) . "</td>";

	//if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td class=number>" . n($l['val']['IMPORTO'],  0) . "</td>";
	if ($_REQUEST['stampa_gg_rit']=='Y') $ret .= "<td>&nbsp;</td>";	
	
	$ret .= "</tr>";

	$ret .= "</table>";
	
	
	
	
 return $ret;
}

function stampa_dati_vettore($cod_vettore){
	$vt = new Vettori();
	$vt->load_rec_data_by_k(array('TAKEY1' => $cod_vettore));
	return $vt->rec_data['TATELE'];
}

function stampa_dati_targa($targa){
  if (strlen(trim($targa)) > 0){
	return "<span style='margin-left: 90px;'>Targa: " . trim($targa) . "</span>";
  }
  else return "";	

}

 
?>


