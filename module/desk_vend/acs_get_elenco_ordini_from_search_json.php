<?php

require_once "../../config.inc.php";

set_time_limit(240);

$s = new Spedizioni();

if(isset($mod_provenienza)){
    $mod_js_parameters = $mod_provenienza->get_mod_parameters();
}else
    $mod_js_parameters = $s->get_mod_parameters();


		$filtro = array();
		$filtro["cliente"] = $_REQUEST['f_cliente_cod'];

		if (strlen($_REQUEST['f_destinazione_cod']) > 0)		
			$filtro["destinazione"] = $_REQUEST['f_destinazione_cod'];			
		
		$filtro["riferimento"] = $_REQUEST['f_riferimento'];
		$filtro["agente"] = $_REQUEST['f_agente'];
		
		//data programmata
		if (strlen($_REQUEST['f_data_dal']) > 0)
			$filtro["data_dal"] = date('Ymd', strtotime($_REQUEST['f_data_dal']));		
		if (strlen($_REQUEST['f_data_al']) > 0)
			$filtro["data_al"] = date('Ymd', strtotime($_REQUEST['f_data_al']));		

		//data spedizione
		if (strlen($_REQUEST['f_data_sped_dal']) > 0)
			$filtro["data_sped_dal"] = date('Ymd', strtotime($_REQUEST['f_data_sped_dal']));
		if (strlen($_REQUEST['f_data_sped_al']) > 0)
			$filtro["data_sped_al"] = date('Ymd', strtotime($_REQUEST['f_data_sped_al']));
		
		
		//data rilascio
		if (strlen($_REQUEST['f_data_ril_dal']) > 0)
			$filtro["data_ril_dal"] = date('Ymd', strtotime($_REQUEST['f_data_ril_dal']));
		if (strlen($_REQUEST['f_data_ril_al']) > 0)
			$filtro["data_ril_al"] = date('Ymd', strtotime($_REQUEST['f_data_ril_al']));		
		

		//data ricezione (dal, al)
		if (strlen($_REQUEST['f_data_ricezione_dal']) > 0)
			$filtro["data_ricezione_dal"] = date('Ymd', strtotime($_REQUEST['f_data_ricezione_dal']));
		if (strlen($_REQUEST['f_data_ricezione_al']) > 0)
			$filtro["data_ricezione_al"] = date('Ymd', strtotime($_REQUEST['f_data_ricezione_al']));
		
		//data conferma (dal, al)
		if (strlen($_REQUEST['f_data_conferma_dal']) > 0)
			$filtro["data_conferma_dal"] = date('Ymd', strtotime($_REQUEST['f_data_conferma_dal']));
		if (strlen($_REQUEST['f_data_conferma_al']) > 0)
			$filtro["data_conferma_al"] = date('Ymd', strtotime($_REQUEST['f_data_conferma_al']));		
		
		//data scarico (dal, al)
		if (strlen($_REQUEST['f_data_scarico_dal']) > 0)
			$filtro["data_scarico_dal"] = date('Ymd', strtotime($_REQUEST['f_data_scarico_dal']));
		if (strlen($_REQUEST['f_data_scarico_al']) > 0)
			$filtro["data_scarico_al"] = date('Ymd', strtotime($_REQUEST['f_data_scarico_al']));
		
		//data disponibilita (dal, al)
		if (strlen($_REQUEST['f_data_disp_dal']) > 0)
			$filtro["data_disp_dal"] = date('Ymd', strtotime($_REQUEST['f_data_disp_dal']));
		if (strlen($_REQUEST['f_data_disp_al']) > 0)
			$filtro["data_disp_al"] = date('Ymd', strtotime($_REQUEST['f_data_disp_al']));		
		
		//data evasione richiesta (dal, al)
		if (strlen($_REQUEST['f_data_ev_rich_dal']) > 0)
			$filtro["data_ev_rich_dal"] = date('Ymd', strtotime($_REQUEST['f_data_ev_rich_dal']));
		if (strlen($_REQUEST['f_data_ev_rich_al']) > 0)
			$filtro["data_ev_rich_al"]  = date('Ymd', strtotime($_REQUEST['f_data_ev_rich_al']));
		
		if (strlen($_REQUEST['f_data_ricezione']) > 0)
			$filtro["data_ricezione"] = date('Ymd', strtotime($_REQUEST['f_data_ricezione']));		
		if (strlen($_REQUEST['f_data_conferma']) > 0)
			$filtro["data_conferma"] = date('Ymd', strtotime($_REQUEST['f_data_conferma']));		
		
		$filtro['carico_assegnato'] = $_REQUEST['f_carico_assegnato'];
		$filtro['lotto_assegnato'] 	= $_REQUEST['f_lotto_assegnato'];
		$filtro['proforma_assegnato'] 	= $_REQUEST['f_proforma_assegnato'];
		
		$filtro['collo_disp'] 		= $_REQUEST['f_collo_disp'];		
		$filtro['collo_sped'] 		= $_REQUEST['f_collo_sped'];		
		
		$filtro['anomalie_evasione']= $_REQUEST['f_anomalie_evasione'];				
		$filtro['ordini_evasi'] 	= $_REQUEST['f_ordini_evasi'];
		$filtro['area']				= $_REQUEST['f_area'];
		$filtro['stabilimento']		= $_REQUEST['f_stabilimento'];
		$filtro['divisione'] 		= $_REQUEST['f_divisione'];
		$filtro['modello'] 			= $_REQUEST['f_modello'];		
		$filtro['referente_cliente']= $_REQUEST['f_referente_cliente'];
		$filtro['referente_ordine']	= $_REQUEST['f_referente_ordine'];
		$filtro['num_ordine'] 		= $_REQUEST['f_num_ordine'];
		$filtro['num_carico'] 		= $_REQUEST['f_num_carico'];
		$filtro['num_lotto'] 		= $_REQUEST['f_num_lotto'];				
		$filtro['num_proforma'] 	= $_REQUEST['f_num_proforma'];
		$filtro['indice_rottura'] 	= $_REQUEST['f_indice_rottura'];
		$filtro['indice_rottura_assegnato'] 	= $_REQUEST['f_indice_rottura_assegnato'];
		$filtro['num_sped'] 		= $_REQUEST['f_num_sped'];
		$filtro['conf'] 		    = $_REQUEST['f_conf'];
		$filtro['itinerario'] 		= $_REQUEST['f_itinerario'];		
		$filtro['tipologia_ordine'] = $_REQUEST['f_tipologia_ordine'];
		$filtro['stato_ordine'] 	= $_REQUEST['f_stato_ordine'];		
		$filtro['priorita'] 		= $_REQUEST['f_priorita'];		
		$filtro['solo_bloccati']	= $_REQUEST['f_solo_bloccati'];		
		$filtro['confermati']		= $_REQUEST['f_confermati'];		
		$filtro['hold']				= $_REQUEST['f_hold'];
		$filtro['tipo_ordine']		= $_REQUEST['f_tipo_ordine'];
		$filtro['vettore']		    = $_REQUEST['f_vettore'];
		$filtro['evasi']     		= $_REQUEST['f_evasi'];
		$filtro['piva']     		= $_REQUEST['f_piva'];
		
		$filtro['num_contract'] 	= $_REQUEST['f_num_contract'];
		$filtro['mercato'] 			= $_REQUEST['f_mercato'];
		$filtro['forniture_MTO'] 			= $_REQUEST['f_forniture_MTO'];
		$filtro['articolo'] 	    = $_REQUEST['f_articolo'];
		$filtro['stadio'] 	        = $_REQUEST['f_stadio'];
		$filtro['ava_paga']		    = $_REQUEST['f_ava_paga'];
		
		$filtro['prov_d']           = $_REQUEST['f_prov_d'];
		$filtro['loc_d']            = $_REQUEST['f_loc_d'];
				
		
		if ($_REQUEST["node"] <> "root" && $_REQUEST["node"] <> ''){
			////$m_liv1 = implode("___", array($row['TDASPE'], $row['TDCITI']));
			////$m_liv2 = implode("___", array($row['TDCCON'], $row['TDCDES']));
		
			//trovo liv1 e liv2 selezionati
			$sottostringhe = explode("|", $_REQUEST["node"]);
			$liv1_exp = explode("___", $sottostringhe[2]);
			$liv2_exp = explode("___", $sottostringhe[4]);
		
			$filtro['area'] 		= trim($liv1_exp[0]);
			$filtro['itinerario'] 	= trim($liv1_exp[1]);
			
			if (count($liv1_exp) >= 3) {
			    $filtro['prov_d'] 	= trim($liv1_exp[2]);
			    $stacco_per_provincia = true;
			}
		
			$filtro['cliente'] 		= trim($liv2_exp[0]);
			$filtro['destinazione']	= trim($liv2_exp[1]);
		}		
		
		
		if ($_REQUEST['f_subtotale_prov_d'] == 'Y') $stacco_per_provincia = true;
		
		if ($stacco_per_provincia)
		    $forza_campi_order_by = " TA_ITIN.TAASPE, TA_ITIN.TASITI, TDPROD, TDDCON, TDDLOC, TDCCON, TDCDES, TDSCOR, TDONDO ";
		
		$tipo_elenco = 'INFO';
		$nr_ord_sel = 'N';
		
		 if($mod_js_parameters->info_search_abbinati == "Y"){
	        if(isset($_REQUEST['f_num_ordine']) && strlen($_REQUEST['f_num_ordine']) > 0)
	            $nr_ord_sel = 'Y';
	    }
		
		$titolo_panel = $s->get_titolo_panel_by_tipo($tipo_elenco);
		$tbar_title = ''; //e' fissa nel json
		
		
		//Per il bottone Programmazione/Monitor devo vedere solo gli ordini plan (TDSWPP = 'Y')
		// altrimenti uso filtro standard (search)
		if ($_REQUEST['menu_type'] == 'PRO_MON')		    
		  $search_type = null;		
		else
		  $search_type = 'search';
		
		$items  = $s->get_elenco_ordini($filtro, 'N', $search_type, 'TDDTEP', false, 'N', null, null, $forza_campi_order_by);
	
		$ars_ar = $s->crea_array_valori_el_ordini_search($items, "", $nr_ord_sel, $_REQUEST['f_subtotale_prov_d']);
		
		//se ho selezionato un ordine aggiungere dove lui � proforma
		$ars 	= $s->crea_alberatura_el_ordini_json_search($ars_ar, $_REQUEST["node"], $filtro["itinerario"], $titolo_panel, $tbar_title, $tipo_elenco);
	
		
		echo $ars;

		$appLog->save_db();		
