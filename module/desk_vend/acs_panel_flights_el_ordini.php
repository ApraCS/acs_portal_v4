<?php

require_once "../../config.inc.php";

set_time_limit(240);

$s = new Spedizioni();

	$field_data = 'TDDTSP';

	//filtro a monte (in base a dove ho fatto click)
	$sottostringhe = explode("|", $_REQUEST["m_id"]);
	
	//TODO: filtrare anch in base a $_REQUEST["node"]???? (in base al sotto-albero aperto)
	
	$liv1 = $sottostringhe[2];
	
	if ($_REQUEST['get_iti_by_par'] == 'Y') //da info (su un ordine Hold)
		$liv2 = $_REQUEST['cod_iti'];	
	else
		$liv2 = $sottostringhe[4];
	//come terza stringa ho il num. spedizione
	$liv3 = $sottostringhe[8];	 

		$filtro = array();
		
		//$filtro["stato_ordine"] = array('RI', 'PR');

		switch ($_REQUEST["tipo_cal"])
		{
			case "per_data":				
				$filtro["data_spedizione"] = substr($liv2, 0, 8);
				$filtro["itinerario"] 	= trim($liv3_explode[0]);
				$tipo_elenco = "GATE";				
				$m_data_per_panel = date('d/m', strtotime($filtro["data_spedizione"] . " +0 days"));				
				$m_data_per_tbar = date('d/m/y', strtotime($filtro["data_spedizione"] . " +0 days")); 
				break;
			case "per_sped":				
				$sped = $s->get_spedizione($_REQUEST["sped_id"]);
				$filtro["data_spedizione"]  = $sped['CSDTSP'];
				$filtro["itinerario"] 		= $sped['CSCITI'];
				$tipo_elenco = "GATE";
				$m_data_per_panel = date('d/m', strtotime($filtro["data_spedizione"] . " +0 days"));				
				$m_data_per_tbar = date('d/m/y', strtotime($filtro["data_spedizione"] . " +0 days"));				
				break;
			case "iti_data":				
				$filtro["data_spedizione"] 	= $_REQUEST['data'];
				$filtro["itinerario"] 		= $_REQUEST['cod_iti'];
				$tipo_elenco = "GATE";
				$m_data_per_panel = date('d/m', strtotime($filtro["data_spedizione"] . " +0 days"));				
				$m_data_per_tbar = date('d/m/y', strtotime($filtro["data_spedizione"] . " +0 days"));				
				break;
			default:
				$filtro["itinerario"] = $liv2;
				$filtro["data_spedizione"] = trim($liv3_explode[0]);	
				
				$titolo_panel = "Hangar ";
				$tipo_elenco = "HANGAR";				
				
				if (strlen($_REQUEST["da_data"])==8){
						
					if (substr($_REQUEST["col_name"], 0, 2)=="d_")
					{ //ho selezionato un giorno
						$add_day = (int)substr($_REQUEST["col_name"], 2, 2) - 1;
						$m_data = date('Ymd', strtotime($_REQUEST["da_data"] . " +{$add_day} days"));
						$m_data_per_panel = date('d/m', strtotime($_REQUEST["da_data"] . " +{$add_day} days"));
						$m_data_per_tbar = date('d/m/y', strtotime($_REQUEST["da_data"] . " +{$add_day} days"));
						$m_data_to = $m_data; //solo un giorno
						$tipo_elenco = "GATE";
					}
					elseif (trim($_REQUEST["col_name"])=='fl_da_prog')
					{ //ordini non programmabili (TDSWSP=='N') ... non filtro per data
						$filtro["TDSWSP"] = 'N';
					}
					elseif (trim($_REQUEST["col_name"])=='DP_val')
					{ //ordini con spedizione 'Da Programmare'
						if (strlen(trim($_REQUEST['cod_iti'])) > 0){
							$filtro["itinerario"] = trim($_REQUEST['cod_iti']);
							$liv2 = trim($_REQUEST['cod_iti']);							
						} 
						$field_data = 'TDDTEP';
						$filtro["solo_sped_DP"] = 'Y';
					}
					else
					{ //ho selezionato un'icona... non un giorno in particolare
						$m_data = date('Ymd', strtotime($_REQUEST["da_data"] . " +0 days"));
						$m_data_to = date('Ymd', strtotime($_REQUEST["da_data"] . " +14 days"));
					}
						
					if (!isSet($filtro["TDSWSP"]))
						$filtro["TDSWSP"] = "Y";
					$filtro["data_spedizione"] = $m_data;
					$filtro["a_data_spedizione"] = $m_data_to;
				}
				
		}		



	
		
		if (strlen($filtro["cliente"]) > 0)
			$tipo_elenco = "HANGAR";		
		
		if (trim($_REQUEST["col_name"])=='fl_da_prog')
		 $tipo_elenco = "HOLD";

/*		 
		if (strlen($filtro["cliente"]) > 0 )
			$tipo_elenco = "LIST";
*/			
		
		if (strlen($_REQUEST['sped_ar']) > 0)
		    $filtro['ar_spedizioni'] = explode(",", $_REQUEST['sped_ar']);
		
		$titolo_panel = $s->get_titolo_panel_by_tipo($tipo_elenco);
		
		if ($tipo_elenco == 'GATE')
			$titolo_panel .= " " . $m_data_per_panel;
						
		$titolo_panel .= " " . implode(' ', array($liv2, $liv3));

		
		//se ho aperto il livello degli ordini
		//if (($row['TDSECA'] . "___" . $row['TDCCON'] . "___" . $row['TDCDES'])  != $liv2_v){
		if ($_REQUEST['ordina_per_tipo_ord'] != 'Y')
		if (strlen($_REQUEST["node"]) > 0 && $_REQUEST["node"] != "root" && $_REQUEST["node"] != ''){
			$sottostringhe_root = explode("|", $_REQUEST["node"]);
			$liv2_root_explode = explode("___", $sottostringhe_root[4]);
			$filtro['seca'] = $liv2_root_explode[0];
			$filtro['cliente'] = $liv2_root_explode[1];
			$filtro['destinazione'] = $liv2_root_explode[2];
		}
		
		
		$tbar_title = 'Elenco ordini per data di spedizione';
		switch($tipo_elenco){
			case 'HANGAR': $tbar_title = ' Elenco per itinerario degli ordini associati a spedizioni in stato "Da programmare"'; break;
			case 'GATE': $tbar_title = "Elenco ordini programmati per itinerario e singola data di spedizione ({$m_data_per_tbar})"; break;
		}		
		
		$items 	= $s->get_elenco_ordini($filtro, $_REQUEST['ordina_per_tipo_ord'], '', $field_data);
		$ars_ar = $s->crea_array_valori_el_ordini($items, $tipo_elenco, $_REQUEST['ordina_per_tipo_ord'], $field_data);	
		$ars 	= $s->crea_alberatura_el_ordini_json($ars_ar, $_REQUEST["node"], $filtro["itinerario"], $titolo_panel, $tbar_title, $tipo_elenco, $_REQUEST['ordina_per_tipo_ord'], $m_data);
		echo $ars;

		$appLog->save_db();
		

function date_from_as($num)
{
 return substr($num, 6, 2) . "/" . substr($num, 4,2) . "/" . substr($num, 2, 2);	
}

?>
