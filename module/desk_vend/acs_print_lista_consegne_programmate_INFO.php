<?php

require_once "../../config.inc.php";

	$s = new Spedizioni();
	$main_module = new Spedizioni();
	
	set_time_limit(240);	


	$ar_email_to = array();
	$ar_email_to[] = array(trim($itinerario->rec_data['TAMAIL']), "ITINERARIO " . j(trim($itinerario->rec_data['TADESC'])) . " (" .  trim($itinerario->rec_data['TAMAIL']) . ")");
	$ar_email_to[] = array(trim($vettore->rec_data['TAMAIL']), "VETTORE " . j(trim($vettore->rec_data['TADESC'])) . " (" .  trim($vettore->rec_data['TAMAIL']) . ")");	
	
	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}

	$ar_email_json = acs_je($ar_email_to);

	
	$filter = (array)json_decode($_REQUEST['filter']);
	
	
	
		$filtro = array();
		$filtro["cliente"] = $filter['f_cliente_cod'];

		if (strlen($filter['f_destinazione_cod']) > 0)		
			$filtro["destinazione"] = $filter['f_destinazione_cod'];			
		
		$filtro["riferimento"] = $filter['f_riferimento'];
		
		//data programmata
		if (strlen($filter['f_data_dal']) > 0)
			$filtro["data_dal"] = date('Ymd', strtotime($filter['f_data_dal']));		
		if (strlen($filter['f_data_al']) > 0)
			$filtro["data_al"] = date('Ymd', strtotime($filter['f_data_al']));		

		//data spedizione
		if (strlen($filter['f_data_sped_dal']) > 0)
			$filtro["data_sped_dal"] = date('Ymd', strtotime($filter['f_data_sped_dal']));
		if (strlen($filter['f_data_sped_al']) > 0)
			$filtro["data_sped_al"] = date('Ymd', strtotime($filter['f_data_sped_al']));
		
		
		//data rilascio
		if (strlen($filter['f_data_ril_dal']) > 0)
			$filtro["data_ril_dal"] = date('Ymd', strtotime($filter['f_data_ril_dal']));
		if (strlen($filter['f_data_ril_al']) > 0)
			$filtro["data_ril_al"] = date('Ymd', strtotime($filter['f_data_ril_al']));		
		

		//data ricezione (dal, al)
		if (strlen($filter['f_data_ricezione_dal']) > 0)
			$filtro["data_ricezione_dal"] = date('Ymd', strtotime($filter['f_data_ricezione_dal']));
		if (strlen($filter['f_data_ricezione_al']) > 0)
			$filtro["data_ricezione_al"] = date('Ymd', strtotime($filter['f_data_ricezione_al']));
		
		//data conferma (dal, al)
		if (strlen($filter['f_data_conferma_dal']) > 0)
			$filtro["data_conferma_dal"] = date('Ymd', strtotime($filter['f_data_conferma_dal']));
		if (strlen($filter['f_data_conferma_al']) > 0)
			$filtro["data_conferma_al"] = date('Ymd', strtotime($filter['f_data_conferma_al']));		
		
		
		if (strlen($filter['f_data_ricezione']) > 0)
			$filtro["data_ricezione"] = date('Ymd', strtotime($filter['f_data_ricezione']));		
		if (strlen($filter['f_data_conferma']) > 0)
			$filtro["data_conferma"] = date('Ymd', strtotime($filter['f_data_conferma']));		
		
		$filtro['carico_assegnato'] = $filter['f_carico_assegnato'];
		$filtro['lotto_assegnato'] 	= $filter['f_lotto_assegnato'];
		$filtro['proforma_assegnato'] 	= $filter['f_proforma_assegnato'];
		
		$filtro['collo_disp'] = $filter['f_collo_disp'];		
		$filtro['collo_sped'] = $filter['f_collo_sped'];		
		
		$filtro['anomalie_evasione']= $filter['f_anomalie_evasione'];				
		$filtro['ordini_evasi'] 	= $filter['f_ordini_evasi'];		
		$filtro['divisione'] 		= $filter['f_divisione'];
		$filtro['modello'] 			= $filter['f_modello'];		
		$filtro['num_ordine'] 		= $filter['f_num_ordine'];
		$filtro['num_carico'] 		= $filter['f_num_carico'];
		$filtro['num_lotto'] 		= $filter['f_num_lotto'];				
		$filtro['num_proforma'] 	= $filter['f_num_proforma'];
		$filtro['indice_rottura'] 	= $filter['f_indice_rottura'];
		$filtro['indice_rottura_assegnato'] 	= $filter['f_indice_rottura_assegnato'];
		$filtro['itinerario'] 		= $filter['f_itinerario'];		
		$filtro['tipologia_ordine'] = $filter['f_tipologia_ordine'];
		$filtro['stato_ordine'] 	= $filter['f_stato_ordine'];		
		$filtro['priorita'] 		= $filter['f_priorita'];		
		$filtro['solo_bloccati']	= $filter['f_solo_bloccati'];
		$filtro['confermati']		= $filter['f_confermati'];
		$filtro['hold']				= $filter['f_hold'];
		$filtro['agente'] 			= $filter['f_agente'];
		
		
		//Per il bottone Programmazione/Monitor devo vedere solo gli ordini plan (TDSWPP = 'Y')
		// altrimenti uso filtro standard (search)
		if ($_REQUEST['menu_type'] == 'PRO_MON')
		    $search_type = null;
		else
		    $search_type = 'search';
		
		$stmt 	= $s->get_elenco_ordini($filtro, 'N', $search_type);
	
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   h1.page_title{font-size: 18px; padding-top: 10px; padding-bottom: 10px;}
   
   table.tab_itinerario{margin: 0px 0px 50px 0px;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php
 
    $liv0_in_linea = null;
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {

		//$liv0 = implode("|", array($r['TDDTEP'], $r['CSCITI'], $r['CSCVET'], $r['CSCAUT'], $r['CSCCON']));
		$liv0 = implode("|", array($r['TDCITI']));
		//$liv1 = implode("|", array($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA']));
		$liv1 = 'ITIN';		
		$liv2 = implode("|", array($r['TDCCON'], $r['TDCDES']));		
		$liv3 = implode("|", array($r['TDDT'], $r['TDOTID'], $r['TDOINU'], $r['TDOADO'], $r['TDONDO']));		
		
		//itinerario
		$t_ar = &$ar;
		$l = $liv0;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r); 					  								  
			$t_ar = &$t_ar[$l]['children'];
		
		//carico
		//non statto piu' per carico... ma lo uso come totale per itinerario
		$l = $liv1;
		if (!isset($t_ar[$l])){
 			$itinerario = new Itinerari();
			$itinerario->load_rec_data_by_k(array("TAKEY1"=>$r['TDCITI']));

 			$t_ar[$l] = array("cod" => $l, "descr"=>trim($itinerario->rec_data['TADESC']), "record" => $r,
 								  "val" => array(), "children"=>array());
		}
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar[$l]['carico'] = $s->get_carico_td($r);
			$t_ar[$l]['orario_carico'] = $r['CSHMPG'];			 								  
			$t_ar = &$t_ar[$l]['children'];
		 								  								  
		//cliente|dest
		$l = $liv2;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];								  								  

		//ordine
		$l = $liv3;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];			
		
	} //while

	

//	echo "<pre>"; print_r($ar); echo "</pre>";exit();
	
//STAMPO
 $cl_liv_cont = 0;
 if ($_REQUEST['stampa_dettaglio_ordini'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
 $liv2_row_cl = ++$cl_liv_cont; 
 $liv1_row_cl = ++$cl_liv_cont;
 
echo "<div id='my_content'>"; 

echo "<H1 class='page_title'>Lista interrogazione ordini per itinerario</H1>";

echo liv0_intestazione_open($l0['record']);

foreach ($ar as $kl0 => $l0){


	  		foreach ($l0['children'] as $kl1 => $l1){
				echo liv1_intestazione_open($l1, $liv1_row_cl);	  			
			
			  		foreach ($l1['children'] as $kl2 => $l2){
						echo liv2_intestazione_open($l2, $liv2_row_cl);
						
						if ($_REQUEST['stampa_dettaglio_ordini']=="Y")
				  		foreach ($l2['children'] as $kl3 => $l3)
							echo liv3_intestazione_open($l3, $liv3_row_cl);			
							
					}			
			}		
	
	
}
echo liv0_intestazione_close($l0['record']);
echo "</div>";

 		
?>

 </body>
</html>







<?php




function somma_valori($ar, $r){
	
	global $info_importo_field;
	if (isset($info_importo_field)==false || strlen($info_importo_field) == 0)
		$info_importo_field = 'TDTIMP';
	
	
 //il volume e' aumentato in base alla percentuale passata (recuperata prima dall'itinerario)
 $ar['VOLUME'] 	+= $r['TDVOLU'] * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
 $ar['COLLI'] 	+= $r['TDTOCO'];
 $ar['PALLET'] 	+= $r['TDBANC'];
 $ar['IMPORTO'] += $r[$info_importo_field]; 
 $ar['PESO'] 	+= $r['TDPLOR']; 
 return $ar;  
}

function liv3_intestazione_open($l, $cl_liv){


	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 3;
	else
		$n_col_span = 2;
	$sr = new SpedAutorizzazioniDeroghe();
	if ($sr->ha_RIPRO_by_k_ordine($l['record']['TDDOCU'])){
		$ar_tooltip_ripro = $sr->tooltip_RIPRO_by_k_ordine($l['record']['TDDOCU']);
		$txt_riprogrammazione = "<BR>" .  implode("<br>", $ar_tooltip_ripro);		
	}

	
	
	$ret = "
		<tr class=liv{$cl_liv}>
		 <td colspan=1>" . implode("_", array($l['record']['TDOADO'],  $l['record']['TDONDO'], $l['record']['TDOTPD'])) . " [{$l['record']['TDSTAT']}] - " . $l['record']['TDDVN1'] . "</TD>
		 <td colspan=1>Rif: " . $l['record']['TDVSRF'] . "{$txt_riprogrammazione}</TD>
		 <td colspan={$n_col_span}>Del: " . print_date($l['record']['TDODRE']) . "
								 , Disp.: " . print_date($l['record']['TDDTDS']) . "
								 , Ev.P.: " . print_date($l['record']['TDDTEP']) . "
								 , Sped.: " . print_date($l['record']['TDDTSP']) . "
					</TD>			
		  	
		 <td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>		 
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";
	$ret .="
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD> 	 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'], 2) . "</TD>";		 
	
	$ret .= "</tr>";
	

	
 return $ret;
}

function liv2_intestazione_open($l, $cl_liv){

	if ($_REQUEST['indirizzo_di'] == 'ind_destinazione'){
		$m_loca = 'TDLOCA';
		$m_prov = 'TDPROV';
		$m_cap  = 'TDCAP';
	} else {
		$m_loca = 'TDDLOC';
		$m_prov = 'TDPROD';
		$m_cap  = 'TDDCAP';
	}
		

	$ret = "
		<tr class=liv{$cl_liv}>
		 <td colspan=1>" . $l['record']['TDDCON'] . "</TD>
		 <td colspan=1>" . $l['record'][$m_loca] . "</TD>		 
		 <td colspan=1>" . $l['record'][$m_prov] . "</TD>		 
		 <td colspan=1>" . $l['record'][$m_cap] . "</TD>";
	
	if ($_REQUEST['stampa_tel']=='Y')  $ret .= "<td colspan=1>" . $l['record']['TDTEL'] . "</TD> ";	
	$ret .= "
		 <td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>		 
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";		 
	$ret .= "
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD>	 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";
		 		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'], 2) . "</TD>";		 
	
	$ret .= "</tr>";
 return $ret;
}



function liv1_intestazione_open($l, $cl_liv){

	if ($l['orario_carico'] > 0)
		$orario_carico = ' [Orario: ' . print_ora($l['orario_carico']) . "]";
	else $orario_carico = '';

	if (strlen(trim($l['carico']['PSNOTE'])) > 0)
		$note_carico = "&nbsp;&nbsp;&nbsp;<b> [" . trim($l['carico']['PSNOTE']) . "]</b>";
	else $note_carico = '';

	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 5;
	else
		$n_col_span = 4;
	
	
	//riga vuota
	$ret ="<tr><td colspan={$n_col_span}>&nbsp;</td></tr>";
	
	$ret .= "
		<tr class=liv{$cl_liv}>
		 <td colspan={$n_col_span}>Itinerario " . $l['descr'] . "</TD>
		 <td colspan=1 class=number>" . $l['val']['COLLI'] . "</TD>
		 ";
	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>" . $l['val']['PALLET'] . "</TD> ";		 
	$ret .= "
		 <td colspan=1 class=number>" . n($l['val']['VOLUME'], 2) . "</TD>		 		 
		 <td colspan=1 class=number>" . n($l['val']['PESO'], 2) . "</TD>		 
		 ";		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'],2) . "</TD>";		 

	$ret .="</tr>";
 return $ret;
}



function liv0_intestazione_open($r){
	global $s, $spedizione, $itinerario;

	if ($_REQUEST['data_riferimento'] == 'data_spedizione')
		$f_data = 'TDDTSP';
	else
		$f_data = 'TDDTEP';
	
	$itinerario = new Itinerari();
	$itinerario->load_rec_data_by_k(array("TAKEY1"=>$r['CSCITI']));	
	
	$ret = "
  		
			<table class='int1 tab_itinerario'>
			 <tr>
			  <th>Cliente</th>
			  <th>Localit&agrave;</th>
			  <th>Pr</th>			  			  
			  <th>Cap</th>";
	if ($_REQUEST['stampa_tel']=='Y')  $ret .= "<th>Telefono</T> ";	
	
	$ret .= "
			  <th>Colli</th>
			  ";
			  if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<th>Pallet</T> ";
	$ret .= "
			  <th>Volume</th>		  			  
			  <th>Peso</th>			  
			  ";			  
			  
	if ($_REQUEST['stampa_importi']=='Y') $ret .="<th>Importo</th>";			  
	
	$ret .= "</tr>";	
 return $ret;	
}

function liv0_intestazione_close($r){
	return "</table>";
}

function stampa_dati_vettore($cod_vettore){
	$vt = new Vettori();
	$vt->load_rec_data_by_k(array('TAKEY1' => $cod_vettore));
	return $vt->rec_data['TATELE'];
}

function stampa_dati_targa($targa){
  if (strlen(trim($targa)) > 0){
	return "<span style='margin-left: 90px;'>Targa: " . trim($targa) . "</span>";
  }
  else return "";	

}

?>		