<?php

function sum_columns_value(&$ar_r, $r){	
	
	global $s;
	
	$ar_r['importo'] += $r['TDTIMP'];
	$ar_r['gg_lav'] = max($ar_r['gg_lav'],$r['TDGGAN']);  //prendi il massimo valore tra me stesso e il nuovo valore
	
	$ar_r['fl_art_manc'] =max($ar_r['fl_art_manc'], $s->get_fl_art_manc($r)); 
	$ar_r['art_da_prog'] = max($ar_r['art_da_prog'], $s->get_art_da_prog($r));
	$ar_r['fl_bloc'] = $s->get_fl_bloc($ar_r['fl_bloc'], $s->calc_fl_bloc($r));
	
	if($ar_r['liv']=='liv_2' || $ar_r['liv']=='liv_3'){
	
		if(!isset($ar_r['cons_rich'])){
			$ar_r['cons_rich'] = $r['TDODER'];
			$ar_r['cons_prog'] = $r['TDDTEP'];
		}else{
			$ar_r['cons_rich'] = min($ar_r['cons_rich'] ,$r['TDODER']);
			$ar_r['cons_prog'] = min($ar_r['cons_prog'] ,$r['TDDTEP']);
		}
	
	}
	switch (trim($r['TDCLOR'])){
		case 'O':
			$ar_r['n_O']++;
			break;
		case 'P':
			$ar_r['n_P']++;
			break;
		case 'M':
			$ar_r['n_M']++;
			break;
		case 'A':
			$ar_r['n_A']++;
			break;
		default:
	
			if (trim($r['TDCLOR']) == 'C' || trim($r['TDCLOR']) == 'V')
				$ar_r['n_CV']++;
	
	}
}

function crea_ar_tree_anzianita_stato_ordini($form_values){
	//parametro da mettere nella funzione $form_values

	$s = new Spedizioni();
	$main_module = new Spedizioni(); 
	
	global $cfg_mod_Spedizioni, $conn;
	
	$giorni_stadio = $s->get_giorni_stadio(0, 0);

	$ar = array();
	$ret = array();
	
	
	
	$sql_where=parametri_sql_where($form_values);
	
	//controllo data
	if (strlen($form_values->f_data_da) > 0)
		$sql_where .= " AND TD.TDDTAN >= {$form_values->f_data_da}";
	if (strlen($form_values->f_data_a) > 0)
		$sql_where .= " AND TD.TDDTAN <= {$form_values->f_data_a}";
	
	//data programmata TDDTEP
	if (strlen($form_values->f_data_prog_da) > 0)
	    $sql_where .= " AND TD.TDDTEP >= {$form_values->f_data_prog_da}";
    if (strlen($form_values->f_data_prog_a) > 0)
        $sql_where .= " AND TD.TDDTEP <= {$form_values->f_data_prog_a}";
		
			//controllo HOLD
		$filtro_hold=$form_values->f_hold;
			
	if($filtro_hold== "Y")
		$sql_where.=" AND TDSWSP = 'Y'";
	if($filtro_hold== "N")
		$sql_where.=" AND TDSWSP = 'N'";
	
		$filtro['stato_ordine'] 	= $form_values->f_stato_ordine;
		//gestione multiselect (valori separati da ,) o array
		if (isset($filtro["stato_ordine"]) && is_array($filtro["stato_ordine"])){
			$ar_stato_ordine = $filtro["stato_ordine"];
			
			if($form_values->f_filtra_stati == 'Y'){
			    if (count($ar_stato_ordine) == 1)
			        $sql_where.= " AND TDSTAT = '{$filtro["stato_ordine"][0]}'";
		        else if (count($ar_stato_ordine) > 1)
		            $sql_where.= " AND TDSTAT IN (" . sql_t_IN($ar_stato_ordine) . ") ";
			}else{
			    if (count($ar_stato_ordine) == 1)
		            $sql_where.= " AND TDSTAT <> '{$filtro["stato_ordine"][0]}'";
		        else if (count($ar_stato_ordine) > 1)
		            $sql_where.= " AND TDSTAT NOT IN (" . sql_t_IN($ar_stato_ordine) . ") ";
			}
			
			
	    
		}
		else if (isset($filtro["stato_ordine"]) && strlen(trim($filtro["stato_ordine"])) > 0){
			$ar_stato_ordine = explode(",", $filtro["stato_ordine"]);
			if (count($ar_stato_ordine) == 1)
				$sql_where.= " AND TDSTAT <> '{$filtro["stato_ordine"]}'";
			else
				$sql_where.= " AND TDSTAT NOT IN (" . sql_t_IN($ar_stato_ordine) . ") ";
		}
	
		$sql = "SELECT TDSTAT, TDDSST, TDDTAN, TDONDO, TDVSRF, TDOTPD, TDODRE, TDSTAT, TDOPRI,
				TDODER, TDDTDS, TDDTEP, TDTIMP, TDCLOR, TDDTEP, TDGGAN, TDDOTD, TDFMTS, TDDTCF, TDFN15,
				TDFMTO, TDBLEV, TDBLOC, TDDTNEW, TDFN14, TDFN13, TDDOCU, TDCCON, TDDCON, TDOADO, TDFN04,
				TDFN11, TDFN19, TDDTCF, TDMTOO, TDFG06
				FROM {$cfg_mod_Spedizioni['file_testate']} TD 
				WHERE 1=1 {$sql_where}
				ORDER BY TDDTAN, TDGGAN, TDDCON";
	

				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt);

				echo db2_stmt_errormsg();

				while ($row = db2_fetch_assoc($stmt)) {

					$tmp_ar_id = array();
					$ar_r= &$ar;


					
					//stacco dei livelli
					$cod_liv0 = acs_u8e(trim($row['TDSTAT'])); //STATO
					$oldLocale = setlocale(LC_TIME, 'it_IT');
					$cod_liv1 = strftime("%Y %B", strtotime($row['TDDTAN'])); //ANNO E MESE
					setlocale(LC_TIME, $oldLocale);
					$cod_liv2 = acs_u8e(strftime("%d %A", strtotime($row['TDDTAN']))); //GIORNO
					$cod_liv3 = acs_u8e(trim($row['TDCCON'])); //CLIENTE
					$cod_liv4 = trim($row['TDONDO']); //ORDINE

	
					//STATO
					$liv =$cod_liv0;
					$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						$ar_new['task'] = "[{$row['TDSTAT']}] " .$row['TDDSST'];
						$ar_new['liv'] = 'liv_totale';
						$ar_new['tipo_liv'] = 'stato_ordine';
						$ar_new['stato'] = $row['TDSTAT'];  //imposto lo stato perch� mi serve recuperarlo quando faccio doppio click su di esso
						$ar_new['desc'] = acs_u8e($row['TDDSST']);
						//$ar_new['importo'] = $row['TDTIMP'];
						$ar_r["{$liv}"] = $ar_new;
					}
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $row);
			

					//ANNO E MESE
					$liv=$cod_liv1;
					$ar_r = &$ar_r['children'];
					$tmp_ar_id[] = $liv;
					if(!isset($ar_r[$liv])){
						$ar_new= array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						//$ar_new['codice']=$liv;
						$ar_new['task'] = ucwords($liv);
						$ar_new['liv'] = 'liv_1';
						//$ar_new['expanded'] =$livello1_expanded;
						$ar_r[$liv]=$ar_new;
					}

					$ar_r=&$ar_r[$liv];
					sum_columns_value($ar_r, $row);
	
					//GIORNO
					$liv=$cod_liv2;
					$ar_r=&$ar_r['children'];
					$tmp_ar_id[] = $liv;
					if(!isset($ar_r[$liv])){
						$ar_new= array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						//$ar_new['codice']=$liv;
						$ar_new['task']= ucwords($liv);
						$ar_new['liv'] = 'liv_2';
												
						//$ar_new['expanded'] =$livello2_expanded;
						$ar_r[$liv]=$ar_new;
					}

					$ar_r=&$ar_r[$liv];
					sum_columns_value($ar_r, $row);
					
					//Cliente
					$liv=$cod_liv3;
					$ar_r=&$ar_r['children'];
					$tmp_ar_id[] = $liv;
					if(!isset($ar_r[$liv])){
						$ar_new= array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						//$ar_new['codice']=$liv;
						$ar_new['task'] = acs_u8e($row['TDDCON']);
						$ar_new['cod_cli'] = acs_u8e($row['TDCCON']);
						$ar_new['liv'] = 'liv_3';
				
						//$ar_new['expanded'] =$livello2_expanded;
						$ar_r[$liv]=$ar_new;
					}
					
					$ar_r=&$ar_r[$liv];
					sum_columns_value($ar_r, $row);
	

					//ORDINE
					$liv=$cod_liv4;
					$ar_r=&$ar_r['children'];
					$tmp_ar_id[] = $liv;
					if(!isset($ar_r[$liv])){
						$ar_new= array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						//$ar_new['codice']=$row['TFTPDO'];
						$ar_new['task']= "{$row['TDOADO']}_".$liv;
						$ar_new['riferimento'] = acs_u8e(trim($row['TDVSRF']));
						$ar_new['tipo'] = trim($row['TDOTPD']);
						$ar_new['data_reg'] = $row['TDODRE'];
						$ar_new['stato'] = $row['TDSTAT'];
						$ar_new['priorita'] = acs_u8e($row['TDOPRI']);
						$ar_new['cons_rich'] = $row['TDODER'];
						$ar_new['data_disp'] = $row['TDDTDS'];
						$ar_new['cons_prog'] = $row['TDDTEP'];
						$ar_new['tdfg06'] = $row['TDFG06'];
						//$ar_new['importo'] = $row['TDTIMP'];
						$ar_new["fl_da_prog"] = $row['TDFN04'];
						$ar_new["fl_evaso"] = $row['TDFN11'];
						$ar_new['n_stadio'] = $giorni_stadio[$row['TDDTEP']];
						$ar_new['fl_new'] = $s->get_fl_new($row);
						$ar_new["classe_ordine"] =$row['TDCLOR'];
						$ar_new["k_ordine"] = trim($row['TDDOCU']);
						$ar_new['liv'] = 'liv_4';
						$ar_new['iconCls'] = $s->get_iconCls(3, $row);
						$ar_new['tipo_liv'] = 'ordine';
						$ar_new['liv_tipo_qtip'] = acs_u8e($row['TDDOTD']);
						
						$ha_commenti = $s->has_commento_ordine_by_k_ordine($ar_new['k_ordine']);
						if ($ha_commenti == FALSE)
					       $img_com_name = "icone/16x16/comment_light.png";
					    else
					       $img_com_name = "icone/16x16/comment_edit_yellow.png";
						        
						$ar_new['task'] .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine_bl(\'' . $ar_new['k_ordine'] . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
						
						//icona entry (per attivita' in corso sull'ordine)
						$sa = new SpedAssegnazioneOrdini();
						if ($sa->ha_entry_aperte_per_ordine($ar_new['k_ordine'])){
						    $img_entry_name = "icone/16x16/arrivi_gray.png";
						    $ar_new['task'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><a href="javascript:show_win_entry_ordine(\'' . $ar_new['k_ordine'] . '\', \'' . $cod_liv0 . '\', \'' . $cod_liv1 . '\')";><img class="cell-img" src=' . img_path($img_entry_name) . '></a> </span>';
						}	
						
						
						$ar_new['leaf']=true;
						$ar_r[$liv]=$ar_new;
					}

					$ar_r=&$ar_r[$liv];
					sum_columns_value($ar_r, $row);
					
					/*print_r($ar_r);
					exit();*/
				}

	return $ar;
}

function parametri_sql_where($form_values){
	
	global $s;
	
	

	$sql_where.= " AND ".$s->get_where_std();
	
	$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);
	
	$sql_where.= sql_where_by_combo_value('TD.TDCAG1', $form_values->f_agente);
	
	$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);
	
	$sql_where.= sql_where_by_combo_value('TD.TDCITI', $form_values->f_itinerario);
	
	$sql_where.= sql_where_by_combo_value('TD.TDSTAB', $form_values->f_sede);
	
	$sql_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);
	$sql_where.= sql_where_by_combo_value('TD.TDOTPD', $form_values->f_tipo_ordine);
	
	return $sql_where;
}

