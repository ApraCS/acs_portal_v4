<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
		'tab_name' =>  $cfg_mod_Spedizioni['file_tabelle'],
		
		'TATAID' => 'SEQST',
		'descrizione' => 'Sequenza per stati ordine',
		
		'fields_key' => array('TAKEY1'),
		
		'fields' => array(				
				'TAKEY1' => array('label'	=> 'Stato'),
				'TADESC' => array('label'	=> 'Descrizoine'),
				'TASITI' => array('label'	=> 'Sequenza'),
		)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
