<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

?>

{"success":true, "items": [

        {
            xtype: 'form',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            
            items: [
				{
                	xtype: 'hidden',
                	name: 'fn',
                	value: 'exe_prima_data_promettibile'
                	},            

		 {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,			
            
            store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',

            
	        listeners: {
	            change: function(field,newVal) {	            	
	            	var form = this.up('form').getForm(); 	            	
	            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
	            	form.findField('f_destinazione_cod').store.load();	            	
	            }
	        },
            
            
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '{cod}' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000
        },
        

		 {
            xtype: 'combo',
			name: 'f_destinazione_cod',
			fieldLabel: 'Destinazione',
			minChars: 2,			
            
            store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_des_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            },
                 extraParams: {
        		  	cliente_cod: ''
        		 }		            
		        },       
				fields: ['cod', 'descr'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: false, //mostro la freccetta di selezione
            anchor: '100%',

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessuna destinazione trovata',
                
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000
        }
        
        
		

			, {
				name: 'tipologia_ordine',
            	anchor: '100%',				
				xtype: 'combo',
				fieldLabel: 'Tipologia Ordine',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
					    ] 
				}						 
			}

			, {
				name: 'tipologia_art_critici',
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Tipologia Articoli Critici',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_sys('BLTO'), '') ?> 	
					    ] 
				}						 
			}, {
				name: 'priorita',
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Priorit&agrave;',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_sys('BPRI', null, null, 'VO'), '') ?> 	
					    ] 
				}						 
			}, {
				     name: 'data_evasione_richiesta'                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data evasione richiesta'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
				}
						
            ],
            
            
			buttons: [{
	            text: 'Visualizza',
	            handler: function() {
	            	var form = this.up('form').getForm();
					
					form.submit(
								{
		                            success: function(form,action) {

		                            	Ext.MessageBox.show({
									           title: 'ATP - Data spedizione promettibile',
									           msg: 'Evasione programmabile: ' + date_from_AS(action.result.data_promettibile, null, null) + '<br>Spedizione prevista: ' + date_from_AS(action.result.data_spedizione, null, null),
									           buttons: Ext.MessageBox.OK
									       });										
												                            	
		                            },
		                            failure: function(form,action){
		                                acs_show_msg_error('errore nel richiamo della funzione');
		                            }
		                        }					
					);
            	                	                
	            }
	        }],             
				
        }
]}