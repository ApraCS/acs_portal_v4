<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$sql_where = '';
$parametri = array();
$date = oggi_AS_date();

if (strlen($_REQUEST['f_trasportatore']) > 0){
	$sql_where .= " AND LTVETT = ?";
	$parametri[] = $_REQUEST['f_trasportatore'];
}
if (strlen($_REQUEST['f_itinerario']) > 0){
	$sql_where .= " AND LTITIN = ?";
	$parametri[] = $_REQUEST['f_itinerario'];
}
if (strlen($_REQUEST['f_mezzo']) > 0){
	$sql_where .= " AND LTAUTO = ?";
	$parametri[] = $_REQUEST['f_mezzo'];
}
if (($_REQUEST['f_valido']) == 'E'){
	$sql_where .= " AND (LTDTIV <= ? OR LTDTIV = 0) AND (LTDTFV >= ? OR LTDTFV = 0)";
	$parametri[] = $date;
	$parametri[] = $date;
}else if(($_REQUEST['f_valido']) == 'S'){
	$sql_where .= " AND (LTDTIV >= ? OR LTDTFV <= ?) AND (LTDTFV != 0 AND LTDTIV != 0)";
	$parametri[] = $date;
	$parametri[] = $date;
}


$sql = "SELECT *
FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
INNER JOIN {$cfg_mod_Spedizioni['file_listino_trasportatori']} LT
		ON TA.TADT = LT.LTDT
		AND	TA.TAKEY1 = LT.LTVETT
		AND TA.TATAID = 'AVET'
		WHERE 1 = 1
		
		$sql_where
		
		ORDER BY LTVETT
		";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, $parametri);
		
?>

<html>
<head>
<style type="text/css">

        table.no_tabella{border:none;}
        table.no_tabella th{background: none; text-align: left;}        
        table{border-collapse:collapse; width:100%; border: 1px solid gray;}
        td{border: 1px solid #000000; padding: 0 3px 0 3px;}
        th{background: #DCDCDC;} 
        tr.titoli th{text-align: center; white-space:nowrap; border-left: 1px solid #000000; border-right: 1px solid #000000;}
        .titoli td{white-space:nowrap;}
        span.quantita{width:120px; display: inline-block;}
        span.denominazione{width:280px; display: inline-block;}
        span.trasportatore{width:650px; display: inline-block;}
        .numeri{text-align: right;} 
        
        @media print 
		{
	    	.noPrint{display:none;}
		}    
		
</style>

<title>
Listini costi trasporto/deposito
</title>
</head>

<body>

<div class="page-utility noPrint">
	<A HREF="javascript:window.print()"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>	
	<A HREF="#" onclick="window.open('data:application/vnd.ms-excel,' + '<html><head>' + escape(document.getElementsByTagName('style')[0].outerHTML) + '</head>' + '<body>' + escape(document.getElementById('my_content').outerHTML) + '</body></html>');"><img src=<?php echo img_path("icone/48x48/save.png") ?>></A>
</div>

<div id='my_content'>

<br>

<?php	
$count = 0;
		
while ($r = db2_fetch_assoc($stmt)) {
$id1 = $r["LTVETT"];
if($count == 0)
{
	echo creazione_anagrafica($r);
	$id2 = $r["LTVETT"];
	$count++;
}

if($id1 != $id2)
{
	echo "</table> <br> <table>";	
	echo creazione_anagrafica($r);
}

$id2 = $r["LTVETT"];
		
if($id1 == $id2)
{
	echo creazione_tabella($r);		
}

} 

echo "</table> </div> </body>"; 


function creazione_anagrafica($r){
	$array_1 = array('Trasportatore [' . trim($r["LTVETT"]) . '] '. trim($r["TADESC"]));
	$array_2 = array(trim($r["TALOCA"]) . ' (' . trim($r["TAPROV"]) . ')', trim($r["TANAZI"]) ,'tel. ' . trim($r["TATELE"]),'email ' . trim($r["TAMAIL"]));
	$ana = "<table class = \"no_tabella \">
			
			<tr>
			<th width = \"50%\"> " . implode(', ', $array_1) . " </th>
			<th width = \"50%\"> " . implode(', ', $array_2) ." </th>
			</tr>
		
			</table>
			
			<br>
			
			<table>
	
			<tr class = \"titoli \">
			<th rowspan=\"2\"> Descrizione </th>
			<th colspan=\"2\"> Validit&agrave; </th>
			<th rowspan=\"2\"> Itinerario </th>
			<th> Classe </th>
			<th colspan=\"2\"> Per Spedizione </th>
			<th rowspan=\"2\"> Costo per Collo </th>				
			<th colspan=\"2\"> Per Scarico </th>				
			<th colspan=\"2\"> Per KM </th>
			<th colspan=\"2\"> Addebito </th>
			<th> Per volume </th>
			</tr>
				
			<tr class = \"titoli \">
			<th>Iniziale</th>
			<th>Finale</th>
			<th>Mezzo</th>
			<th>Costo</th>
			<th>%</th>
			<th>Costo</th>
			<th>Esclusi</th>
			<th>Costi</th>
			<th>Km minimi</th>
			<th>Min</th>
			<th>Max</th>
			<th>(costo al m3)</th>
			</tr>";
	
	return $ana;
}

function creazione_tabella($r){
	global $s;
	$tab = "<tr>
			<td><span class=\"denominazione\">" . $r["LTDESC"] ." </span></td>
			<td>" . print_date($r["LTDTIV"]) ." </td>
			<td>" . print_date($r["LTDTFV"]) ." </td>
			<td>" . $s->decod_std('ITIN', $r["LTITIN"]) ." </td>
			<td>" . $r["LTAUTO"] ." </td>
			<td class = \"numeri\">" . n($r["LTCSPE"],2,'N','Y') ." </td>
			<td class = \"numeri\">" . n($r["LTPIMP"],2,'N','Y') ." </td>
			<td class = \"numeri\">" . n($r["LTCOCO"],2,'N','Y') ." </td>
			<td class = \"numeri\">" . n($r["LTCSCA"],2,'N','Y') ." </td>
			<td class = \"numeri\">" . n($r["LTSESC"],2,'N','Y') ." </td>
			<td class = \"numeri\">" . n($r["LTCOKM"],2,'N','Y') ." </td>
			<td class = \"numeri\">" . n($r["LTKMIN"],0,'N','Y') ." </td>
			<td class = \"numeri\">" . n($r["LTAMIN"],2,'N','Y') ." </td>
			<td class = \"numeri\">" . n($r["LTAMAX"],2,'N','Y') ." </td>
			<td>" . stampa_valori($r) ." </td>
			</tr>";
	
	return $tab;
}

function stampa_valori($r){
	for($i = 1; $i <= 5; $i++)
	{
		if($r["LTCUN{$i}"] != 0 || $r["LTQTA{$i}"] != 0)
		{
			$stampa .= "<span class=\"quantita\"> da m3: ". n($r["LTQTA{$i}"]) ."</span> - costo: ".n($r["LTCUN{$i}"])."\n<br>";
	}
}
return $stampa;
}

?>

</html>