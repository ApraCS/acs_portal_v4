<?php

require_once "../../config.inc.php";
$s = new Spedizioni();

if ($cfg_mod_Spedizioni["visualizza_order_img"] == "N"){
	$ret = array('root' => array());
	echo acs_je($ret);
	exit;	
}


/**************************************************************************************/
/**    FROM_PATH (accessibile da php)                                                 */
/**************************************************************************************/
if ($cfg_mod_Spedizioni["visualizza_order_img"] == "FROM_PATH"){

  if ($_REQUEST['function'] != 'view_image'){
        
    $ord = $s->get_ordine_by_k_docu($_REQUEST['k_ordine']);
    
    /* CONFIG  (ES: "/SV2/ORDINI/") */
    $img_dir = $cfg_mod_Spedizioni['visualizza_order_img_PATH'];
    $dir_ordine = $img_dir;
    
    if (isset($cfg_mod_Spedizioni['visualizza_order_img_add_anno']) &&
        $cfg_mod_Spedizioni['visualizza_order_img_add_anno'] == 'Y')
      $dir_ordine .= '/' . $ord['TDOADO'];
        
    $dir_ordine .= '/' . trim($ord['TDONDO']);
        
    if (isset($cfg_mod_Spedizioni['visualizza_order_img_add_sub_form_prog_substr']) &&
        $cfg_mod_Spedizioni['visualizza_order_img_add_sub_form_prog_substr'] > 0)
      $dir_ordine .= '/' . substr($ord['TDPROG'], 0, $cfg_mod_Spedizioni['visualizza_order_img_add_sub_form_prog_substr']);
            
    $dir_ordine .= "/*.*";
     
    $ret = array();
    foreach (glob($dir_ordine) as $filename)
    {
        $nome_file_ar = explode('/', $filename);
        $nome_file = end($nome_file_ar);
        $r = array();
        $r['DataCreazione'] = date ("d/m/Y", filemtime($filename));
        $r['IDOggetto']		 = acs_u8e($filename);
        $r['des_oggetto']	 = basename($filename);
        
        $r['tipo_scheda'] = get_tipo_scheda(basename($filename));
        $ret[] = $r;
    }
    
    echo acs_je($ret);       
    exit;
 }


  if (isset($_REQUEST['function']) && $_REQUEST['function'] == 'view_image'){
    //mi viene passato il path del file... lo apro e ne restituisco il contenuto
    $handle = fopen(utf8_decode($_REQUEST['IDOggetto']), "rb");
    $cont_file = fread($handle, filesize($_REQUEST['IDOggetto']));
    
    $path_info = pathinfo($_REQUEST['IDOggetto']);
    $tipo_estensione = $path_info['extension'];
    
    switch(strtolower($tipo_estensione)){
        case 'pdf':
            header('Content-type: application/pdf');
            echo $cont_file;
            break;
        case 'tif':
        case 'tiff':
            header('Content-type: image/tiff');
            echo $cont_file;
            break;
        case 'png':
            header('Content-type: image/png');
            echo $cont_file;
            break;
        case 'jpg':
            header('Content-type: image/jpeg');
            echo $cont_file;
            break;
        default:
            header("Content-Description: File Transfer");
            header("Content-Type: application/octet-stream");
            header("Content-disposition: attachment; filename=\"" . preg_replace('/^.+[\\\\\\/]/', '', $path_info['basename']) . "\"");
            echo $cont_file;
            break;
    }
    
    exit;
  } //view_image
} // FROM_PATH

/**************************************************************************************/
/**                                                                  /FINE FROM_PATH **/
/**************************************************************************************/







if ($cfg_mod_Spedizioni["visualizza_order_img"] == "FROM_PC" && $_REQUEST['function'] != 'view_image'){

	$my_url_p =  http_build_query(array('ord' => $_REQUEST['k_ordine']));
	
	//interrogo il server con le immagini
	$json = file_get_contents("{$cfg_mod_Spedizioni["url_img_FROM_PC"]}?{$my_url_p}");
	
	echo $json;
	exit;
}










function get_tipo_scheda($name){
    global $n_ord;
    if (strstr( $name , "_MSGIN_" )) return "1. Messaggi ricevuti";
    if (strstr( $name , "_MSGOUT_" )) return "2. Messaggi inviati";
    
    if (substr($name, 0, 6) != $n_ord) return "3. Altro";
    return "4. Allegati grafici";
}



if ($cfg_mod_Spedizioni["visualizza_order_img"] != "FROM_PC"){
	//interrogo le immagini dell'ordine su sql-server
	
	//mssql
	$host 	= $mssql_images['host'];
	$dbname = $mssql_images['dbname'];
	$usr 	= $mssql_images['usr'];
	$psw  	= $mssql_images['psw'];
	$db = new PDO("dblib:host={$host};dbname={$dbname};charset=utf8",$usr,$psw);
	
}




function acs_gzdecode($data){
	$g=tempnam('/tmp','view_allegati_');
	@file_put_contents($g,$data, FILE_BINARY);
	
	$zip = new ZipArchive;
	if ($zip->open($g) === TRUE) {		
		$entry = $zip->getNameIndex($i);	
		$zip->extractTo('/tmp');
		$zip->close();
		unlink($g);		

		$filename = '/tmp/' . $entry;
		return $filename;		
	} else {
		unlink($g);
		echo 'failed';
		exit;		
	}
	
}


//******************************************************************************************
//visualizzo singola immagine
//******************************************************************************************
if ($_REQUEST['function'] == 'view_image'){
	
	if ($cfg_mod_Spedizioni["visualizza_order_img"] == "FROM_PC"){

		$my_url_p =  http_build_query(array('IDOggetto' => $_REQUEST['IDOggetto'], 'function' => 'view_image'));

		//interrogo il server con le immagini
		$cont_file = file_get_contents("{$cfg_mod_Spedizioni["url_img_FROM_PC"]}?{$my_url_p}");
		
		$path_info = pathinfo($_REQUEST['IDOggetto']);		
		$tipo_estensione = $path_info['extension'];

		switch(strtolower($tipo_estensione)){
			case 'pdf':
				header('Content-type: application/pdf');
				echo $cont_file;
				break;
			case 'tif':
			case 'tiff':
				header('Content-type: image/tiff');
				echo $cont_file;
				break;
			case 'png':
				header('Content-type: image/png');
				echo $cont_file;
				break;
			case 'jpg':
				header('Content-type: image/jpeg');
				echo $cont_file;
				break;				
			default:
				header("Content-Description: File Transfer");
				header("Content-Type: application/octet-stream");
				header("Content-disposition: attachment; filename=\"" . preg_replace('/^.+[\\\\\\/]/', '', $path_info['basename']) . "\"");
				echo $cont_file;
				break;
		}		
		
		
		exit;
	}
	else { //DA SQL SERVER
		$sql = "SELECT O.OGGETTO, O.TIPOOGGETTO, O.DESOGGETTO
			FROM [DB-Gestore-Stosa].dbo.[OGGETTI] O
			WHERE O.IDOGGETTO = " . sql_t($_REQUEST['IDOggetto']) ;
		
		//$stmt = mssql_query($sql);
		//$row = mssql_fetch_array($stmt);
		$stmt=$db->prepare($sql);
		$stmt->execute();
		$row=$stmt->fetch();
		
		//decomprimo l'immagine su disco
		$filename = acs_gzdecode($row['OGGETTO']);
		
		if (!$filename){
			//e' andata in errore la decompressione
		}
		
		$handle = fopen($filename, "rb");

		switch(strtolower($row['TIPOOGGETTO'])){
			case 'pdf':
				header('Content-type: application/pdf');
				echo fread($handle, filesize($filename));
				break;
			case 'tif':
			case 'tiff':
				header('Content-type: image/tiff');
				echo fread($handle, filesize($filename));
				break;
			case 'png':
				header('Content-type: image/png');
				echo fread($handle, filesize($filename));
				break;
			case 'jpg':
				header('Content-type: image/jpeg');
				echo fread($handle, filesize($filename));
				break;				
			default:
				header("Content-Description: File Transfer");
				header("Content-Type: application/octet-stream");
				header("Content-disposition: attachment; filename=\"{$row['DESOGGETTO']}\"");
				echo fread($handle, filesize($filename));
				break;
		}
		
		unlink($filename);
		exit();		
		
	}
	

}


//******************************************************************************************
// ELENCO allegati ordine
//******************************************************************************************

$data = array();

$oe = $s->k_ordine_td_decode($_REQUEST['k_ordine']);

$sql = "

SELECT
	IDOggetto = O.IDOGGETTO,
	NumeroOrdine = M.NORDINE,
	Azione = A.AZIONE,
		CASE 
		WHEN A.AZIONE = 'PDF' AND O.DESOGGETTO LIKE '%asso.PDF' THEN 'Disegno Composizione'
		WHEN A.AZIONE = 'PDF' AND O.DESOGGETTO LIKE '%eli.PDF' THEN 'Elementi Lineari'
		WHEN A.AZIONE = 'PDF' AND O.DESOGGETTO LIKE '%top.PDF' THEN 'Top e Mensole'
		WHEN A.AZIONE = 'PDF' AND O.DESOGGETTO LIKE '%fm.PDF' THEN 'Fuori Misura'
		WHEN A.AZIONE IN ('DWG','DWGX') AND O.DESOGGETTO LIKE '%.d%' THEN 'Disegni DWG'
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGIN[_]%.%' THEN 'Doc inviati (inviati dal cliente)'
		WHEN A.AZIONE = 'FAX' AND (O.DESOGGETTO LIKE '%[_]MSGOUT[_]CON%.%' OR O.DESOGGETTO LIKE '%[_]MSGOUT[_]MOD%.%') THEN 'Doc ricevuti (conferme d''ordine)'
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGOUT[_]CHI%.%' THEN 'Chiarimenti'
		WHEN A.AZIONE = 'Allegati' AND O.DESOGGETTO LIKE '%.pdf' THEN 'Doc inviati (inviati dal cliente)'
		ELSE '11. Altro'
	END AS TipoScheda,
	CASE 
		WHEN A.AZIONE = 'PDF' AND O.DESOGGETTO LIKE '%asso.PDF' THEN 3
		WHEN A.AZIONE = 'PDF' AND O.DESOGGETTO LIKE '%eli.PDF' THEN 5
		WHEN A.AZIONE = 'PDF' AND O.DESOGGETTO LIKE '%top.PDF' THEN 8
		WHEN A.AZIONE = 'PDF' AND O.DESOGGETTO LIKE '%fm.PDF' THEN 9
		WHEN A.AZIONE IN ('DWG','DWGX') AND O.DESOGGETTO LIKE '%.d%' THEN 10
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGIN[_]%.%' THEN 1
		WHEN A.AZIONE = 'FAX' AND (O.DESOGGETTO LIKE '%[_]MSGOUT[_]CON%.%' OR O.DESOGGETTO LIKE '%[_]MSGOUT[_]MOD%.%') THEN 2
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGOUT[_]CHI%.%' THEN 0
		WHEN A.AZIONE = 'Allegati' AND O.DESOGGETTO LIKE '%.pdf' THEN 1
		ELSE 11
	END AS TipoSchedaID,
	Bloccato = A.BLOCCATO,
	Stato = S.STATO,
	Nota = S.NOTA,
	Oggetto = O.OGGETTO,
	TipoOggetto = O.TIPOOGGETTO,
	DesOggetto = O.DESOGGETTO,
	DataCreazione = S.DATACREAZIONE

FROM [DB-Gestore-Stosa].dbo.[MASTER] M
	INNER JOIN [DB-Gestore-Stosa].dbo.[AZIONI] A ON M.IDMASTER = A.IDMASTER
	INNER JOIN [DB-Gestore-Stosa].dbo.[STATI] S ON A.IDAZIONE = S.IDAZIONE
	INNER JOIN [DB-Gestore-Stosa].dbo.[OGGETTI] O ON O.IDSTATO = S.IDSTATO
WHERE
	M.NORDINE= " . sql_t($oe['TDONDO']) . "
	AND
	(A.AZIONE IN ('Allegati', 'FAX')
	OR
	(A.AZIONE = 'DWG' 
			AND A.IDAZIONE= (select MAX(Z.IDAZIONE) 
			from [DB-Gestore-Stosa].[dbo].[MASTER] as A1
			inner join [DB-Gestore-Stosa].[dbo].[AZIONI] as Z
			ON (A1.IDMASTER=Z.IDMASTER )
			where A1.NORDINE=M.NORDINE AND Z.AZIONE='DWG'))
	OR
	(A.AZIONE = 'PDF' 
			AND A.IDAZIONE= (select MAX(Z.IDAZIONE) 
			from [DB-Gestore-Stosa].[dbo].[MASTER] as A1
			inner join [DB-Gestore-Stosa].[dbo].[AZIONI] as Z
			ON (A1.IDMASTER=Z.IDMASTER )
			where A1.NORDINE=M.NORDINE AND Z.AZIONE='PDF'))
	)
	order by TipoSchedaID,DataCreazione
		";


///$stmt = mssql_query($sql);
$stmt=$db->prepare($sql);
$stmt->execute();

while ($row=$stmt->fetch()){
	$data[] = array('tipo_scheda' => sprintf("%02s", $row['TipoSchedaID']) . " ". $row['TipoScheda'],
					'des_oggetto' => $row['DesOggetto'],
					'IDOggetto'	  => $row['IDOggetto'],
					'DataCreazione'	  => print_date($row['DataCreazione'])			
	);	
}

$ret = array('root' => $data);
echo acs_je($ret);

$appLog->save_db();

?>
