<?php

require_once "../../config.inc.php";

$s = new Spedizioni();


$addmenu = "
    voci_menu.push({
  		text: 'Report scarichi',
		iconCls : 'iconPrint',          		
		handler: function() {			
		  	acs_show_win_std('Parametri report scarichi', 'acs_get_elenco_stampa_form.php', 
		  		{		
		  		 rec_id: '|SPED|' + rec.get('CSPROG'),						    			  		 
		  		 rec_liv: 'liv_0',
                 tipo_elenco: 'GATE'
		  		}, 600, 400, null, 'iconPrint');
		}
	});		

";
    

?>


{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	form_values = form.getValues();
			            	
			            	//in visualizzazione non tengo conto della data finale (usata solo nel report)
			            	form_values['f_data_a'] = null;
			            	 
			            	if(form.isValid()){
								<?php $cl = new SpedManutenzioneCosti(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "spedmanutenzionecosti") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "spedmanutenzionecosti") ?>
								<?php echo $cl->out_Writer_Model("spedmanutenzionecosti") ?>
								<?php echo $cl->out_Writer_Store("spedmanutenzionecosti", 
										"form_ep: JSON.stringify(form_values) ") ?>
								<?php echo $cl->out_Writer_sotto_main("spedmanutenzionecosti", 0.4) ?>
								<?php echo $cl->out_Writer_main("Lista spedizioni", "spedmanutenzionecosti", "", 1, 'false', null, "", $addmenu) ?>		
								<?php echo $cl->out_Writer_window("Controllo/Manutenzione costo spedizioni del") ?>
								
								m_win.setTitle('Controllo/Manutenzione costo spedizioni del ' + Ext.Date.format(form.findField('f_data').getValue(),'d/m/Y'));							
			            	}
			            }
			         }, {
		                text: 'Riepilogo',
		                iconCls: 'icon-print-32',
		                scale: 'large',
		                handler: function() {
		                    form = this.up('form').getForm();
		                    if (form.isValid()){
		                        form.submit({
		                        		url: 'acs_report_riepilogo_manutenzione_costi.php',
		                        		params: {form_ep: JSON.stringify(form.getValues())},
		                                standardSubmit: true,
                                     	submitEmptyText: false,
		                                method: 'POST',
		                                target : '_blank'
		                        });
		                    }
		                }
		            }, {
		                text: 'Report<br/>incidenza',
		                iconCls: 'icon-print-32',
		                scale: 'large',
		                handler: function() {
		                    form = this.up('form').getForm();
		                    if (form.isValid()){
		                        form.submit({
		                        		url: 'acs_report_manutenzione_costi.php',
		                        		params: {form_ep: JSON.stringify(form.getValues())},
		                                standardSubmit: true,
                                     	submitEmptyText: false,
		                                method: 'POST',
		                                target : '_blank'
		                        });
		                    }
		                }
		            }, {
		                text: 'Report<br/>trasportatore',
		                iconCls: 'icon-print-32',
		                scale: 'large',
		                handler: function() {
		                    form = this.up('form').getForm();
		                    if (form.isValid()){
		                        form.submit({
		                        		url: 'acs_report_manutenzione_costi_trasp.php',
		                        		params: {form_ep: JSON.stringify(form.getValues())},
		                                standardSubmit: true,
                                     	submitEmptyText: false,
		                                method: 'POST',
		                                target : '_blank'
		                        });
		                    }
		                }
		            }],   		            
		            
		            items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data evasione'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , margin: "10 10 0 10"
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data finale<br>(solo per report)'
						   , name: 'f_data_a'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin: "10 10 0 10"
						}, {
							name: 'f_area_spedizione',
							xtype: 'combo',
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('ASPE'), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_trasporto',
							xtype: 'combo',
							fieldLabel: 'Tipologia trasporto',
							displayField: 'text',
							valueField: 'id',
						   	allowBlank: true,							
							emptyText: '- seleziona -',
							multiSelect: true,
							forceSelection:true,
						    anchor: '-15',
						    margin: "20 10 10 10",
						    value: <?php echo j(trim($sped['CSTITR'])); ?>,							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'), ""); ?>	
								    ] 
							}						 
						  }, {
							name: 'f_trasportatore',
							xtype: 'combo',
							fieldLabel: 'Trasportatore',
							displayField: 'text',
							valueField: 'id',
						   	allowBlank: true,							
							emptyText: '- seleziona -',
							multiSelect: true,
							forceSelection:true,
						    anchor: '-15',
						    margin: "20 10 10 10",							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('AVET'), ""); ?>	
								    ] 
							}						 
						  }]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
