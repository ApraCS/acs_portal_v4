<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
 
$main_module = new Spedizioni();

$da_form = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'open_elenco_clienti'){

// ******************************************************************************************
// REPORT
// ******************************************************************************************

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}

    table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}

    td.titolo{font-weight:bold;}
  
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     

</style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);

    Ext.onReady(function() {	
	});  

  </script>

</head>
<body>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content'>

<h2 style='margin-top: 10px; margin-bottom:10px; '>Riepilogo ordini immessi</h2>
<p align="right"> Data elaborazione:  <?php echo date('d/m/Y  H:i');?></p>

<table class=int1>
	<tr class=liv_totale>
		<td class=titolo>Cliente</td>
		<td class=titolo>Localit&agrave;</td>
		<td class=titolo>Riferimento</td>
		<td class=titolo>Blocco ordine</td>
		<td class=titolo>Ordine</td>
		<td class=titolo>Data ordine</td>
		<td class=titolo>Tp</td>
		<td class=titolo>Descrizione Tipo</td>
		<td class=titolo>St.</td>
		<td class=titolo>Agente</td>
		<td class=titolo>Operatore immissione</td>
		<td class=titolo>Itinerario</td>
		<td class=titolo>Importo</td>
		<td class=titolo>Modello</td>
		<td class=titolo>Variante</td>
		</tr>

<?php

	$data = array();

	$sql = "SELECT TD.*, TA_ITIN.TADESC AS ITIN	
			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI					
			WHERE " . $s->get_where_std() . " AND 1=1 ORDER BY TDDCON";
	
	
	$sql_where = "";
	$ar_sql = array();
	$m_params = acs_m_params_json_decode();
	
	if ($m_params->solo_senza_carico == 'Y')
		$sql_where .= " AND TDNRCA=0 ";
	
		
	$filtro = $s->get_elenco_ordini_create_filtro((array)json_decode($_REQUEST['filter']));  //filter
	$use_my_stmt = 'Y';
	
	//Per il bottone Programmazione/Monitor devo vedere solo gli ordini plan (TDSWPP = 'Y')
	// altrimenti uso filtro standard (search)
	if ($_REQUEST['menu_type'] == 'PRO_MON')
	    $search_type = null;
	else
	    $search_type = 'search';
	
	$stmt 	= $s->get_elenco_ordini($filtro, 'N', $search_type);

	
	
	if ($use_my_stmt != 'Y'){
		$sql = $sql . $sql_where;
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql);		
	}

	while ($row = db2_fetch_assoc($stmt)) {
			$nr = array();
				$nr['TDDCON'] 		= acs_u8e(trim($row['TDDCON']));
				$nr['localita'] 	= acs_u8e(trim($s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD'])));
				$nr['TDVSRF']		= acs_u8e(trim($row['TDVSRF']));
				$nr['k_ordine']		= trim($row['TDDOCU']);
				$nr['data_ordine']	= trim($row['TDODRE']);
			    $nr['tipoOrd']		= trim($row['TDOTPD']);
			    $nr['desc']		= trim($row['TDDOTD']);
			    $nr['fl_bloc']		= $s->get_ord_fl_bloc($row);
				$nr['colli_sped'] 	= $row['TDCOSP'];
				$nr['stato'] 		= trim($row['TDSTAT']);				
				$nr['agente'] 		= acs_u8e(trim($row['TDDAG1']));
				$nr['op_imm'] 		= acs_u8e(trim($row['TDUSIM']));
				$nr['itin'] 		= acs_u8e($s->decod_std('ITIN', trim($row['TDCITI'])));
				$nr['importo']  	= $row['TDINFI'];
				$nr['var1']			= acs_u8e(trim($row['TDDVN1']));
				$nr['var2']			= acs_u8e(trim($row['TDDVN2']));
			
				$data[] = $nr;
	}
	

foreach ($data as $k0 => $v0){

	?>

   <tr>
   <td><?php echo $v0['TDDCON']; ?></td>
   <td><?php echo $v0['localita']; ?></td>
   <td><?php echo $v0['TDVSRF']; ?></td>
 
    
   
    <td>
    
   <?php  if ($v0['fl_bloc']==0) { echo " "; }?>
   <?php  if ($v0['fl_bloc']==1) { echo " "; }?> 
   <?php  if ($v0['fl_bloc']==2) {echo "B"; }?> 
   <?php  if ($v0['fl_bloc']==3) {echo "B"; }?>
   <?php  if ($v0['fl_bloc']==4) {echo "B"; }?>
   
   </td>
   

   <td><?php echo $v0['k_ordine']; ?></td>
   <td><?php echo print_date($v0['data_ordine']); ?></td>
   <td><?php echo $v0['tipoOrd']; ?></td>
    <td><?php echo $v0['desc']; ?></td>
   <td><?php echo $v0['stato']; ?></td>
   <td><?php echo $v0['agente']; ?></td>
   <td><?php echo $v0['op_imm']; ?></td>
   <td><?php echo $v0['itin']; ?></td>
   <td class=number><?php echo n($v0['importo'],2); ?></td>
   <td><?php echo $v0['var1']; ?></td>
   <td><?php echo $v0['var2']; ?></td>
   </tr>
  

<?php 
}
?>
 </table>

</div>
</body>
</html>
<?php 
  }
  
  if ($_REQUEST['fn'] == 'open_form'){
  	$m_params = acs_m_params_json_decode();
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            //url: 'acs_print_lista_consegne.php',
            
            items: [
				{
                	xtype: 'hidden',
                	name: 'filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                }            
            ],
            
			buttons: [					
				{
		            text: 'Visualizza',
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',		            
		            handler: function() {
		                this.up('form').submit({
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_elenco_clienti',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues()),
	                        	menu_type: menu_type
	                        }
	                  });
	                  
	                  this.up('window').close();
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}
?>