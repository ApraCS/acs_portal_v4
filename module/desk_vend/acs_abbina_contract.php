<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// EDIT EVENT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_abbina_contract'){

    $k_ordine_contract = $m_params->form_values->f_k_ordine_contract;
    
    // $oe_contract = $s->k_ordine_td_decode_xx($k_ordine_contract);    
    
	foreach ($m_params->list_selected_id as $ordine){
		
		$sh = new SpedHistory();
		$sh->crea(
		    'pers',
		    array(
		        "messaggio"	=> 'ASS_CONTRACT',
		        "k_ordine"	=> $ordine->k_ordine,
		        "vals" => array(
		            "RIDART" =>$k_ordine_contract
		        )		        
		    )
		);
		
	} // per ogni ordine selezionato
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// FORM APERTURA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){    
    //recupero l'elenco degli ordini interessati
    $list_selected_id = $m_params->list_selected_id;
    
    //Dal primo ordine recupero il cliente
    $ord = $s->get_ordine_by_k_docu($list_selected_id[0]->k_ordine);
    $cod_cli = $ord['TDCCON'];
?>
{"success":true, "items": [    
    {
    	xtype: 'form',
    	bodyStyle: 'padding: 10px',
    	bodyPadding: '5 5 0',
    	frame: true,
    	title: '',    
    	defaults:{ anchor: '-10' , labelWidth: 130 },    
    	
    	items: [
    		{
    			name: 'f_k_ordine_contract',						
    			xtype: 'textfield',
    			fieldLabel: 'Ordine contract',
    		    maxLength: 100,
    		    allowBlank: false,
    		    readOnly: true					
    		}    				
    	],
    	
    	buttons: [
    				{
		         		text: 'Ricerca ordine', 
		         		itemId: 'ricerca_ordine',
		        		iconCls: 'icon-folder_search-32', scale: 'large',	   	 	
		        		handler: function () {
		        		
    		        		var m_form = this.up('form');
    		        		
    		        		var my_listeners = {
    	    		  			afterOkSave: function(from_win, k_ordine){
            						from_win.close();
            						m_form.getForm().findField('f_k_ordine_contract').setValue(k_ordine);
            						var b_conferma = m_form.down('#b_conferma');
    	 			                b_conferma.fireHandler();
    				        		}
    		    				};	
    		        		
    				         acs_show_win_std('Ricerca su archivio ordini', 
    				         	'../base/acs_seleziona_ordini_gest.php?fn=open', {
    				         		cod_cli: <?php echo j($cod_cli) ?>,
    				         		doc_gest_search: 'CONTRACT_abbina',
    				         		show_parameters: false,
    				         		auto_load: true
    				         	}, 800, 550,  my_listeners, 'icon-clessidra-16');	
			           }
		    		}, '->',
    			  {
    	            text: 'Conferma',
    	            iconCls: 'icon-save-32', scale: 'large',
    	            itemId: 'b_conferma',	
    	            handler: function() {
    	            	var form = this.up('form').getForm();
    	            	var loc_win = this.up('window')
    
    					if(form.isValid()){
    					    Ext.getBody().mask('Loading... ', 'loading').show();
    						Ext.Ajax.request({
    						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_abbina_contract',
    						        jsonData: {
    						        	form_values: form.getValues(),
    						        	list_selected_id: <?php echo acs_je($list_selected_id); ?>    						        	
    						        },
    						        method     : 'POST',
    						        waitMsg    : 'Data loading',
    						        success : function(result, request){	
    						            Ext.getBody().unmask();            	  													        
    						            var jsonData = Ext.decode(result.responseText);
    						            if (jsonData.success == true){
    						            	loc_win.fireEvent('onCloseSuccess', loc_win);
    						            } else {
    						            	Ext.Msg.alert('Message', 'Error');
    						            }	
    						        },
    						        failure    : function(result, request){
    						            Ext.getBody().unmask();
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
    						    });	    
    				    }            	                	                
    	            }
    	        }],  
    	        
    	 	listeners: {
    	 		afterrender: function(comp){
    	 			//in automatico apro la form di ricerca ordine contract
    	 			var btn_ricerca_ordine = comp.down('#ricerca_ordine');
    	 			btn_ricerca_ordine.fireHandler();
    	 		}
    	 	}                  
    				
            } //form
    ]}
<?php exit; } ?>