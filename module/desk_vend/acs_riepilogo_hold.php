<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

?>


<html class=acs_report>
<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table{border-collapse:collapse; width: 100%; margin-top: 15px;}
table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
h2.acs_report{background-color: black; color: white;}

html.acs_report body{padding: 10px;}
html.acs_report h2{font-size: 20px; padding: 5px;}
   

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}
 
@media print
{
	.noPrint{display:none;}
	.page-break  { display: block; page-break-before: always; }
 	.onlyPrint{display: block;}	
}

</style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
  
  </script>

</head>
<body>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 	

<div id='my_content'>

<h2>Riepilogo Hold - Ordini non programmati</h2>
<p style='text-align: right; font-size:13px;'> Data elaborazione: <?php echo Date('d/m/Y H:i'); ?> </p>

<?php

//filtro per area/intinerario
$from_liv_id = $_REQUEST['liv_id'];
$from_liv_ar = explode("|", $from_liv_id);
$sql_filtro_where = '';

if (count($from_liv_ar) >= 3) { //filtro su area di spedizione
 $sql_filtro_where .= " AND TDASPE = " . sql_t($from_liv_ar[2]);
}
if (count($from_liv_ar) >= 5) { //filtro su itinerario
	$sql_filtro_where .= " AND TDCITI = " . sql_t($from_liv_ar[4]);
}

$ar = array();
$ar_aspe = array();


$sql = "SELECT TDASPE, TDCITI, TDOTPD, TDDOTD, TA_ITIN.TADESC AS ITIN, TDSTAT, TDDSST, SUM (TDTOCO) COLLI, SUM (TDVOLU) AS VOLUME , COUNT(*) AS NR_ORD, SUM (TDTIMP) AS IMPORTO 
FROM {$cfg_mod_Spedizioni['file_testate']} TD
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
		  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI   
WHERE". $s->get_where_std(). " AND TDFN04=1 " . $sql_filtro_where . " 
GROUP BY TDASPE, TDCITI, TDOTPD, TDDOTD, TA_ITIN.TADESC, TDSTAT, TDDSST ORDER BY TA_ITIN.TADESC";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$sql2 = "SELECT TDASPE, TDCITI, TDOTPD, TA_ITIN.TADESC AS ITIN, TDSTAT, TDDSST, SUM (TDTOCO) COLLI, SUM (TDVOLU) AS VOLUME, COUNT(*) AS NR_ORD, SUM (TDTIMP) AS IMPORTO
FROM {$cfg_mod_Spedizioni['file_testate']} TD
LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
WHERE". $s->get_where_std(). " AND TDFN04=1 AND (TDBLOC='Y' OR TDBLEV='Y') " . $sql_filtro_where . "
GROUP BY TDASPE, TDCITI, TDOTPD, TA_ITIN.TADESC, TDSTAT, TDDSST";


$stmt2 = db2_prepare($conn, $sql2);
echo db2_stmt_errormsg();
$result = db2_execute($stmt2);

$bl = array();
while($row2 = db2_fetch_assoc($stmt2)){	
	// itinerario/stato/tipo
	$bl[$row2['TDCITI']][$row2['TDSTAT']][$row2['TDOTPD']]['NR_ORDINI']  += $row2['NR_ORD'];
	$bl[$row2['TDCITI']][$row2['TDSTAT']][$row2['TDOTPD']]['VOLUME']     += $row2['VOLUME'];
	$bl[$row2['TDCITI']][$row2['TDSTAT']][$row2['TDOTPD']]['COLLI'] 	 += $row2['COLLI'];
	$bl[$row2['TDCITI']][$row2['TDSTAT']][$row2['TDOTPD']]['IMPORTO'] 	 += $row2['IMPORTO'];
	
	// totale per itinerario/stato
	$bl[$row2['TDCITI']][$row2['TDSTAT']]['NR_ORDINI']  += $row2['NR_ORD'];
	$bl[$row2['TDCITI']][$row2['TDSTAT']]['VOLUME'] 	+= $row2['VOLUME'];
	$bl[$row2['TDCITI']][$row2['TDSTAT']]['COLLI'] 	    += $row2['COLLI'];	
	$bl[$row2['TDCITI']][$row2['TDSTAT']]['IMPORTO'] 	+= $row2['IMPORTO'];
	
	// totale per area
	$bl[$row2['TDASPE']]['NR_ORDINI']  += $row2['NR_ORD'];
	$bl[$row2['TDASPE']]['VOLUME'] 	   += $row2['VOLUME'];
	$bl[$row2['TDASPE']]['COLLI'] 	   += $row2['COLLI'];
	$bl[$row2['TDASPE']]['IMPORTO']    += $row2['IMPORTO'];
}


while ($row = db2_fetch_assoc($stmt)){
	$row['STATO'] = trim_utf8($row['TDDSST']);
	
	$row['NR_ORD'] = $row['NR_ORD'];
	$row['COLLI'] = $row['COLLI'];
	$row['VOLUME'] = $row['VOLUME'];
	$row['IMPORTO'] = $row['IMPORTO'];
	$row['DESC_ITIN'] = $row['ITIN'];
	$row['DESC_STATO'] = $row['TDDSST'];
	
	
	$row['NR_ORD_BLOC']	=  $bl[$row['TDCITI']][$row['TDSTAT']][$row['TDOTPD']]['NR_ORDINI'];
	$row['COLLI_BLOC'] 	=  $bl[$row['TDCITI']][$row['TDSTAT']][$row['TDOTPD']]['COLLI'];
	$row['VOL_BLOC'] 	=  $bl[$row['TDCITI']][$row['TDSTAT']][$row['TDOTPD']]['VOLUME'];
	$row['IMP_BLOC'] 	=  $bl[$row['TDCITI']][$row['TDSTAT']][$row['TDOTPD']]['IMPORTO'];
	
//AREA
	
	
	$liv0 = trim($row['TDASPE']);
	$liv1 = trim($row['TDSTAT']);
	$liv2 = trim($row['TDOTPD']);
	
	

	//LIV0 (area)

	$d_ar = &$ar_aspe;
	$c_liv = $liv0;
	if (!isset($d_ar[$c_liv]))
		$d_ar[$c_liv] = array(
				"cod" => $c_liv,
				"cod_liv" => $c_liv,
				"area_aspe" => $row['TDASPE'],
		        "children"=>array());
	
		$d_ar = &$d_ar[$c_liv];
		$d_ar['NR_ORD'] += $row['NR_ORD'];
		$d_ar['COLLI']  += $row['COLLI'];
		$d_ar['VOLUME'] += $row['VOLUME'];
		$d_ar['IMPORTO'] += $row['IMPORTO'];
		
		
		$d_ar['NR_ORD_BLOC'] 	=  $bl[$row['TDASPE']]['NR_ORDINI'];
		$d_ar['COLLI_BLOC'] 	=  $bl[$row['TDASPE']]['COLLI'];
		$d_ar['VOL_BLOC'] 	=  $bl[$row['TDASPE']]['VOLUME'];
		$d_ar['IMP_BLOC'] 	=  $bl[$row['TDASPE']]['IMPORTO'];
	
		$d_ar = &$d_ar['children'];
	
		//LIV1 (stato)
		$c_liv = $liv1;
		if (!isset($d_ar[$c_liv]))
			$d_ar[$c_liv] = array(
					"cod" => $c_liv,
					"cod_liv" => $c_liv,
					"descr" => acs_u8e($row['DESC_STATO']),
					"children"=>array());
	
			$d_ar = &$d_ar[$c_liv];
			$d_ar['NR_ORD'] += $row['NR_ORD'];
			$d_ar['COLLI']  += $row['COLLI'];
			$d_ar['VOLUME'] += $row['VOLUME'];
			$d_ar['IMPORTO'] += $row['IMPORTO'];
	
			$d_ar['NR_ORD_BLOC'] 	=  $bl[$row['TDASPE']]['NR_ORDINI'];
			$d_ar['COLLI_BLOC'] 	=  $bl[$row['TDASPE']]['COLLI'];
			$d_ar['VOL_BLOC'] 	=  $bl[$row['TDASPE']]['VOLUME'];
			$d_ar['IMP_BLOC'] 	=  $bl[$row['TDASPE']]['IMPORTO'];
	
			$d_ar = &$d_ar['children'];
				
			$d_ar[] = $row;
	
	
	
	//ITINERARIO
	

 $liv0 = trim($row['TDCITI']);
 $liv1 = trim($row['TDSTAT']);
 $liv2 = trim($row['TDOTPD']);


 
 //LIV0 (itin)
 $d_ar = &$ar; 
 $c_liv = $liv0;
 if (!isset($d_ar[$c_liv]))
 	$d_ar[$c_liv] = array(
 			"cod" => $c_liv,
 			"cod_liv" => $c_liv, 
 			"area" 	=> $row['TDCITI'],
 			"STATO" => $row['STATO'],
 			"children"=>array());
 
  $d_ar = &$d_ar[$c_liv];
  $d_ar['NR_ORD'] += $row['NR_ORD'];
  $d_ar['COLLI']  += $row['COLLI'];
  $d_ar['VOLUME'] += $row['VOLUME'];
  $d_ar['IMPORTO'] += $row['IMPORTO'];
  
  $d_ar = &$d_ar['children']; 
  
  //LIV1 (stato)
  $c_liv = $liv1;
  if (!isset($d_ar[$c_liv]))
  	$d_ar[$c_liv] = array(
  			"cod" => $c_liv,
  			"cod_liv" => $c_liv,
  			"descr" => acs_u8e($row['DESC_STATO']),
  			"children"=>array());
  
  	$d_ar = &$d_ar[$c_liv];
  	$d_ar['NR_ORD'] += $row['NR_ORD'];
  	$d_ar['COLLI']  += $row['COLLI'];
  	$d_ar['VOLUME'] += $row['VOLUME'];
  	$d_ar['IMPORTO'] += $row['IMPORTO'];

  	$d_ar['NR_ORD_BLOC'] 	=  $bl[$row['TDCITI']][$row['TDSTAT']]['NR_ORDINI'];
  	$d_ar['COLLI_BLOC'] 	=  $bl[$row['TDCITI']][$row['TDSTAT']]['COLLI'];
  	$d_ar['VOL_BLOC'] 	=  $bl[$row['TDCITI']][$row['TDSTAT']]['VOLUME'];
  	$d_ar['IMP_BLOC'] 	=  $bl[$row['TDCITI']][$row['TDSTAT']]['IMPORTO'];
  
  	$d_ar = &$d_ar['children'];
  	  
	$d_ar[] = $row;
 
}

// STAMPO REPORT *******************************

foreach ($ar_aspe as $ka0 => $la0){

	echo "<br><h2 class=acs_report> Area di spedizione " .$s->decod_std('ASPE', $la0['area_aspe'])."</h2>";
	
	echo "<table class=acs_report>";
	
	?>
			<tr class="t-l1">
			 <td rowspan="2">Codice</td>
			 <td rowspan="2">Stato / Tipo ordine</td>
			 <td rowspan="2">Nr ordini</td>
	 		 <td rowspan="2">Colli</td>
	 		 <td rowspan="2">Volume</td>
	 		 <td rowspan="2">Importo</td>
	 		 <td colspan="4" style='text-align: center';> Di cui clienti/ordini bloccati</td>
	 		 <tr class="t-l1"><td>&nbsp;</td>		
	 		 <td>Nr ordini</td>		 
	 		 <td>Colli</td>		 		 
	 		 <td>Volume</td>
	 		 <td>Importo</td>	</tr>	 
	 		
	 		</tr>
	 		
	 				<tr class=liv1>
		 <td><?php echo $la0['cod_liv'] ?></td>
		 <td><?php echo $la0['descr'] ?></td>
		 <td class=number><?php echo $la0['NR_ORD']; ?></td>
		 <td class=number><?php echo $la0['COLLI']; ?></td>
		 <td class=number><?php echo n($la0['VOLUME'],2); ?></td>
		 <td class=number><?php echo n($la0['IMPORTO'],2); ?></td>
		 <td class=number><?php echo $la0['NR_ORD_BLOC']; ?></td>
		 <td class=number><?php echo $la0['COLLI_BLOC']; ?></td>
		 <td class=number><?php echo n($la0['VOL_BLOC'],2); ?></td>
		 <td class=number><?php echo n($la0['IMP_BLOC'],2); ?></td>			 
		 	 	 
		 </tr> 
		 
	 		</table>
	
	 <?php 		
	
   foreach ($ar as $k0 => $l0){
	
	
	// $l0_tot = array();
	
	 echo "<br><h2 class=acs_report>" .$s->decod_std('ITIN', $l0['area'])."</h2>";
	
	 echo "<table class=acs_report>";
	 
	 ?>
		<tr class="t-l1">
		 <td rowspan="2">Codice</td>
		 <td rowspan="2">Stato / Tipo ordine</td>
		 <td rowspan="2">Nr ordini</td>
 		 <td rowspan="2">Colli</td>
 		 <td rowspan="2">Volume</td>
 		 <td rowspan="2">Importo</td>
 		 <td colspan="4" style='text-align: center';> Di cui bloccati</td>
 		 <tr class="t-l1">&nbsp;</td>		
 		 <td>Nr ordini</td>		 
 		 <td>Colli</td>		 		 
 		 <td>Volume</td>
 		 <td>Importo</td>	</tr>	 
 		
 		</tr>

 <?php 		
 
	
  $ultimo_cod_liv = 'aaaa';
	 
	 	foreach ($l0['children'] as $k1 => $l1){ //per stato
	 		
	 		if($ultimo_cod_liv!=trim_utf8($l1['cod_liv'])){	 			
	 			$cod_liv=trim_utf8($l1['cod_liv']);
	 		}else {
	 			$cod_liv = " ";}
	 		
	 		?>
		
		<tr class=liv1>
		 <td><?php echo $l1['cod_liv'] ?></td>
		 <td><?php echo $l1['descr'] ?></td>
		 <td class=number><?php echo $l1['NR_ORD']; ?></td>
		 <td class=number><?php echo $l1['COLLI']; ?></td>
		 <td class=number><?php echo n($l1['VOLUME'],2); ?></td>
		 <td class=number><?php echo n($l1['IMPORTO'],2); ?></td>
		 <td class=number><?php echo $l1['NR_ORD_BLOC']; ?></td>
		 <td class=number><?php echo $l1['COLLI_BLOC']; ?></td>
		 <td class=number><?php echo n($l1['VOL_BLOC'],2); ?></td>
		 <td class=number><?php echo n($l1['IMP_BLOC'],2); ?></td>			 
		 	 	 
		 </tr>
		 
		<?php
		
		//stampiao dettaglio per tipo ordine
		foreach ($l1['children'] as $k2 => $l2){ //per tipo ordine
		?>
			<tr>
			<td><?php echo $l2['TDOTPD'] ?></td>
			 <td><?php echo $l2['TDDOTD'] ?></td>
			 <td class=number><?php echo $l2['NR_ORD']; ?></td>
			 <td class=number><?php echo $l2['COLLI']; ?></td>
			 <td class=number><?php echo n($l2['VOLUME'],2); ?></td>
			 <td class=number><?php echo n($l2['IMPORTO'],2); ?></td>
			 <td class=number><?php echo $l2['NR_ORD_BLOC']; ?></td>
			 <td class=number><?php echo $l2['COLLI_BLOC']; ?></td>
			 <td class=number><?php echo n($l2['VOL_BLOC'],2); ?></td>
			 <td class=number><?php echo n($l2['IMP_BLOC'],2); ?></td>				 
			</tr>
		
		<?php			
		}		
		
		
		$ultimo_cod_liv = trim_utf8($l1['cod_liv']);

	 	
	  } //per ogni riga
	  

	 
	 ?>
	 


	 <?php

 echo "</table>";

	}	

}
?>




</div>
</body>
</html>


