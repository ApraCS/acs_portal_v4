<?php
require_once "../../config.inc.php";

$main_module = new Spedizioni();


function m_cell_class_fido_dispo($fido, $dispo){
		if ($dispo <= -($fido * 0.05))
			return 'R';
		if ($dispo < ($fido * 0.05) && $dispo > -($fido * 0.05))
			return 'G';
		if ($dispo >= ($fido * 0.05))
			return 'V';
}


/********************************************************
 * RICHESTA ESECUZIONE ANALISI ESPOSIZIONE
 ********************************************************/
if ($_REQUEST['fn'] == 'exe_richiesta_aggiornamento'){
	
	ini_set('max_execution_time', 30000);	
	
	$m_params = acs_m_params_json_decode();
	
	//gestiamo un singolo cliente
//	$row_cliente = $m_params->list_selected_id[0];
//	$row_cliente_id_exp = explode('|', $m_params->list_selected_id); 
//	$cod_cli = $row_cliente_id_exp[1];
	$cod_cli = sprintf("%09s", $m_params->list_selected_id);
	
	$dt = $id_ditta_default;
	
	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $dt);
	$cl_p .= sprintf("%09s", $cod_cli);
	$cl_p .= sprintf("%-1s", "N");
	$cl_p .= $m_params->add_parameters;
	
	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31G5C('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31G5C', $libreria_predefinita_EXE, $cl_in, null, null);
		//$call_return = $tkObj->PgmCall('UTIBR31G5', $libreria_predefinita_EXE, '', null, null);
	}	
	
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;	
} 
/********************************************************
 * get_json_data
 ********************************************************/
if ($_REQUEST['fn'] == 'get_json_data'){
	
	$m_params = acs_m_params_json_decode();

	$ret = array();
	$ret['success'] = true;
	$ret['children'] = array();
	
	//per singolo cliente
	$cod_cli = sprintf("%09s", $m_params->cod_cli);
	
	$dt = $id_ditta_default;

	if ($_REQUEST['view'] == 'analisi_esposizione')
		$dt = '99';
	
	
	if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
		
		/* RECORD DI TESTATA *******************************************************************/

			$sql = "SELECT * 
					 FROM {$cfg_mod_Spedizioni['file_analisi_esposizione']}					 
					 WHERE WTDT=? and WTCCON=?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($dt, $cod_cli));
			$r = db2_fetch_assoc($stmt);
			
			//liv testate
			$rT = array();
			$rT['id'] = 'WTFIDA';
			$rT['task'] = 'Fido assicurato';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';			
			$rT['imp']  = $r['WTFIDA'];
			$ret['children'][] = $rT;
			
			$rT = array();
			$disponibilita_tmp = $r['WTFIDO'];
			$fido = $r['WTFIDO'];			
			$rT['id'] = 'WTFIDO';
			$rT['task'] = 'Fido standard';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';			
			$rT['imp']  = $r['WTFIDO'];
			$ret['children'][] = $rT;
				
			$rT = array(); 
			$rT['id'] = 'WTSCON';
			$rT['task'] = 'Saldo contabile';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';			
			$rT['imp']  = $r['WTSCON'];			
			$disponibilita_tmp -= $rT['imp']; 
			$rT['disponibilita'] = $disponibilita_tmp;
			$rT['fido'] = $fido;
			$rT['dispo_class'] = m_cell_class_fido_dispo($rT['fido'], $rT['disponibilita']);
			$ret['children'][] = $rT;			

			$rT = array();
			$rT['id'] = 'WTTINS';
			$rT['task'] = 'Scadenze';
			$rT['leaf'] = false;
			$rT['liv'] = 'liv_5';			
			$rT['imp']  = $r['WTTPOR'];			
			$disponibilita_tmp -= $rT['imp'];
			$rT['disponibilita'] = $disponibilita_tmp;
			$rT['fido'] = $fido;
			$rT['dispo_class'] = m_cell_class_fido_dispo($rT['fido'], $rT['disponibilita']);			
			$ret['children'][] = $rT;
			
			$rT = array();
			$rT['id'] = 'WTTRIM';
			$rT['task'] = 'Rimesse dirette';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';			
			$rT['imp']  = $r['WTTRIM'];			
			$disponibilita_tmp -= $rT['imp'];
			$rT['disponibilita'] = $disponibilita_tmp;
			$rT['fido'] = $fido;
			$rT['dispo_class'] = m_cell_class_fido_dispo($rT['fido'], $rT['disponibilita']);			
			$ret['children'][] = $rT;
				
			$rT = array();
			$rT['id'] = 'WTTFAT';
			$rT['task'] = 'Fatture da contabilizzare';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';
			$rT['imp']  = $r['WTTFAT'];			
			$disponibilita_tmp -= $rT['imp'];
			$rT['disponibilita'] = $disponibilita_tmp;
			$rT['fido'] = $fido;
			$rT['dispo_class'] = m_cell_class_fido_dispo($rT['fido'], $rT['disponibilita']);			
			$ret['children'][] = $rT;			
			
			$rT = array();
			$rT['id'] = 'WTTDDT';
			$rT['task'] = 'DDT da fatturare';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';			
			$rT['imp']  = $r['WTTDDT'];			
			$disponibilita_tmp -= $rT['imp'];
			$rT['disponibilita'] = $disponibilita_tmp;
			$rT['fido'] = $fido;
			$rT['dispo_class'] = m_cell_class_fido_dispo($rT['fido'], $rT['disponibilita']);			
			$ret['children'][] = $rT;
				
			$rT = array();
			$rT['id'] = 'WTTORD';
			$rT['task'] = 'Ordini inevasi';
			$rT['imp']  = $r['WTTORD'];			
			$rT['liv'] = 'liv_5';			
			$disponibilita_tmp -= $rT['imp'];
			$rT['disponibilita'] = $disponibilita_tmp;
			$rT['fido'] = $fido;
			$rT['dispo_class'] = m_cell_class_fido_dispo($rT['fido'], $rT['disponibilita']);
			if ($_REQUEST['view'] == 'analisi_esposizione')
				$rT['leaf'] = true;										
			$ret['children'][] = $rT;
				
				
				
			$rT = array();
			$rT['id'] = 'WTEXPO';
			$rT['task'] = 'Esposizione globale';
			$rT['leaf'] = true;	
			$rT['liv'] = 'liv_5';			
			$rT['imp']  = $r['WTEXPO'];			
			$rT['disponibilita'] = $r['WTFIDO'] - $r['WTEXPO'];
			$rT['fido'] = $fido;
			$rT['dispo_class'] = m_cell_class_fido_dispo($rT['fido'], $rT['disponibilita']);			
			$ret['children'][] = $rT;
			
			
			//dati insoluti e di fatturato
			//lascio alcune righe vuote
			$rT = array();
			$rT['id'] = 'blank_01';
			$rT['task'] = '&nbsp;<br/>&nbsp;<br/>&nbsp;<br/>';
			$rT['iconCls'] = 'iconNoIcon';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';
			$ret['children'][] = $rT;
			
			$rT = array();
			$rT['id'] = 'blank_02';
			$rT['task'] = 'Dati contabili';
			$rT['iconCls'] = 'iconNoIcon';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_1';
			$ret['children'][] = $rT;
				
				
			
			$rT = array();
			$rT['id'] = 'pie_WTTINS';
			$rT['task'] = 'Insoluti aperti';
			$rT['leaf'] = true;	
			$rT['liv'] = 'liv_5';			
			$rT['imp']  = $r['WTTINS'];			
			$ret['children'][] = $rT;
						
			$rT = array();
			$rT['id'] = 'pie_WTTFA1';
			$rT['task'] = 'Fatturato anno corrente';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';
			$rT['imp']  = $r['WTTFA1'];
			$ret['children'][] = $rT;
			
			$rT = array();
			$rT['id'] = 'pie_WTTIN1';
			$rT['task'] = 'Insoluti anno corrente';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';
			$rT['imp']  = $r['WTTIN1'];
			$ret['children'][] = $rT;

			$rT = array();
			$rT['id'] = 'pie_WTTFA2';
			$rT['task'] = 'Fatturato anno precedente';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';
			$rT['imp']  = $r['WTTFA2'];
			$ret['children'][] = $rT;
				
						
			$rT = array();
			$rT['id'] = 'pie_WTTIN2';
			$rT['task'] = 'Insoluti anno precedente';
			$rT['leaf'] = true;
			$rT['liv'] = 'liv_5';
			$rT['imp']  = $r['WTTIN2'];
			$ret['children'][] = $rT;
						
			
			
			$ret['ultimo_aggiornamento'] = print_date($r['WTDTGE']) . "- " . print_ora($r['WTORGE']);
		
	} else {
		
		/* RECORD DI DETTAGLIO *******************************************************************/
		$giorni_stadio = $main_module->get_giorni_stadio($da_data, $n_giorni);
		
		/* RECORD DI TESTATA *******************************************************************/
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_analisi_esposizione']} WHERE WTDT=? and WTCCON=?";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($dt, $cod_cli));
		$rTestata = db2_fetch_assoc($stmt);				
		
			$disponibilita_tmp = (float)$rTestata['WTFIDO'] - (float)$rTestata['WTSCON'];
		
			switch($_REQUEST['node']) {
				case 'WTTINS': //scadenze
					$sql = "SELECT sum(wrimpo) as S_WRIMPO, wraa, wrmm, WRCAUT
							FROM {$cfg_mod_Spedizioni['file_analisi_esposizione_cont_dett']}
							WHERE WRDT=? and WRCCON=?
							AND WRGRES IN('Sospesi', 'Effetti')
							GROUP BY WRAA, WRMM, WRCAUT 
							ORDER BY wraa, wrmm, WRCAUT";
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt, array($dt, $cod_cli));

					while ($r = db2_fetch_assoc($stmt)){
						$rT = array();
						$rT['task'] = implode(" ", array($r['WRAA'], ucfirst(print_month($r['WRMM'])), $r['WRCAUT']));
						$rT['causale'] = $r['WRCAUT'];
						$rT['imp'] 	= $r['S_WRIMPO'];
						$rT['leaf'] = true;
						$rT['liv']  = 'liv_dettaglio';
						$rT['disponibilita_subparziale']  = true;
						$rT['on_click_somma_parziale'] 	= true;
						$rT['somma_in_parziale'] 		= true;
						
						$disponibilita_tmp -= $r['S_WRIMPO'];
						$rT['disponibilita'] = $disponibilita_tmp;
												
						$tot_parziale += (float)$r['S_WRIMPO'];
						
						$ret['children'][] = $rT;						
						$ret['ultimo_aggiornamento'] = print_date($rTestata['WRDTGE']) . "- " . print_ora($rTestata['WRORGE']);						
					}
				break;
				
				
				
				case 'WTTRIM': //rimesse dirette
					$sql = "SELECT sum(wrimpo) as S_WRIMPO, wraa, wrmm, WRCAUT
							FROM {$cfg_mod_Spedizioni['file_analisi_esposizione_cont_dett']}
							WHERE WRDT=? and WRCCON=?
							AND WRGRES = 'Rimesse'
							GROUP BY WRAA, WRMM, WRCAUT
							ORDER BY wraa, wrmm, WRCAUT";
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt, array($dt, $cod_cli));
				
					while ($r = db2_fetch_assoc($stmt)){
						$rT = array();
						$rT['task'] = implode("_", array($r['WRAA'], print_month($r['WRMM']), trim($r['WRCAUT'])));
						$rT['causale'] = $r['WRCAUT'];
						$rT['imp'] 	= $r['S_WRIMPO'];
						$rT['leaf'] = true;
						$rT['on_click_somma_parziale'] 	= true;
						$rT['somma_in_parziale'] 		= true;
						$rT['disponibilita_subparziale']  = true;
				
						$tot_parziale += (float)$r['S_WRIMPO'];
				
						$ret['children'][] = $rT;
						$ret['ultimo_aggiornamento'] = print_date($rTestata['WRDTGE']) . "- " . print_ora($rTestata['WRORGE']);
					}
					break;				
					
				default: //elenco ordini (su cui posso eseguire operazioni sblocco...)
					$sql = "SELECT * 
							FROM {$cfg_mod_Spedizioni['file_analisi_esposizione_dett']}
							LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD ON WDDOCU = TDDOCU				 
							WHERE WDDT=? and WDCCON=? ORDER BY wdsequ, wddtrg";
					$stmt = db2_prepare($conn, $sql);	
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt, array($dt, $cod_cli));
					while ($r = db2_fetch_assoc($stmt)){
			
						$rT = array();
						$rT['id_old'] = trim($r['WDDOCU']);
						$rT['id_old'] = implode("|", array($_REQUEST['node'], implode("_", array( $r['WDDIDO'],
																							  $r['WDTIDO'],
																							  $r['WDINUM'],
																							  $r['WDAADO'],
																							  $r['WDNRDO']					
						))));
						$rT['task'] = implode("_", array($r['TDOADO'], sprintf("%06s", $r['TDONDO'])));
						$rT['leaf'] = true;
						$rT['liv']	= 'liv_2';
						$rT['disponibilita_subparziale']  = true;
						$rT['disponibilita']  	= $r['WDIPRO'];
						$rT['imp']  	= $r['WDTOTD'];			
						$rT['azione'] 	= trim($r['WDAZIO']);
						$rT['fido'] 	= $fido;
						$rT['dispo_class'] = m_cell_class_fido_dispo($rT['fido'], $rT['disponibilita']);
			
						$rT['stato'] 	= $r['TDSTAT'];
						$rT['tipo'] 	= $r['TDOTPD'];
						$rT['data_reg'] = $r['TDODRE'];
						$rT['cons_prog']= $r['TDDTEP'];
						$rT['data_sblocco'] = $r['TDDTSBO'];
						
						$rT['fl_da_prog'] = $r['TDFN04'];
						$rT['n_stadio'] = $giorni_stadio[$r['TDDTEP']];
						
						//DRY: copiata da acs_panel_todolist_data
						$rT["iconCls"]	=  'icon-blog_compose-16'; //non confermata (default)
						if ($r['TDFN13'] == 1){
							$rT["iconCls"]	=  'icon-design-16'; //icona design
						}
						if ((int)$r['TDDTCF'] > 0){
							$rT["iconCls"]	=  'icon-blog_accept-16'; //confermata
						}
						if ((int)$r['TDNRCA'] > 0){
							$rT["iconCls"]	=  'icon-button_black_play-16'; //con carico
						}			
						
						
						
						//campo note
						if (trim($r['WDBLEV']) == 'Y')
							$rT['note'] = 'Bloccato';
						if (trim($r['WDBLEV']) == 'N')
							$rT['note'] = 'Sbloccato il ' . print_date($r['WDDTSBO']);
			
						//flag blocco/sblocco Prima/Dopo
							$rT['fl_P'] = trim($r['WDBLEV']);
							$rT['fl_D'] = trim($r['TDBLEV']);
						
						$ret['children'][] = $rT;
						
						$ret['ultimo_aggiornamento'] = print_date($rTestata['WTDTGE']) . "- " . print_ora($rTestata['WTORGE']);
					}

			} //switch
		
	}
		
	echo acs_je($ret);
exit; }
// ******************************************************************************************
// Tree
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_view'){	
	$m_params = acs_m_params_json_decode();
	
	//per singolo cliente
	//$row_cliente = $m_params->list_selected_id[0];
	//$row_cliente_id_exp = explode('|', $row_cliente->id);
	//$row_cliente_id_exp = explode('|', $m_params->list_selected_id);
	//$cod_cli = $row_cliente_id_exp[1];
	$cod_cli = $m_params->list_selected_id;
	$cod_cli = sprintf("%09s", $m_params->list_selected_id);	
	
	$dt = $id_ditta_default;
		
	/* RECORD DI TESTATA *******************************************************************/
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_analisi_esposizione']} WHERE WTDT=? and WTCCON=?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($dt, $cod_cli));
	$r = db2_fetch_assoc($stmt);
	
	if ($_REQUEST['view'] == 'analisi_esposizione')
		$view = 'analisi_esposizione';
	else
		$view = 'blocco_sblocco';
	
?>
{"success":true, "items":[    
	{
	 xtype: 'treepanel',
	 rootVisible: false,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'causale', 'imp', 'imp_doc', 'azione', 'note', 'disponibilita', 'liv', 'iconCls', 'disponibilita_subparziale', 
				    		 'on_click_somma_parziale', 'somma_in_parziale',
				    		 'stato', 'tipo', 'data_reg', 'cons_prog', 'data_sblocco', 'fl_P', 'fl_D', 'fl_da_prog', 'n_stadio'],
				    autoLoad: true,
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                      extraParams: {dt: <?php echo j($dt)?>, cod_cli: <?php echo j($cod_cli)?>, view: <?php echo j($view)?>}
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    },
                   
                   listeners: {
									beforeload: function() {
						            	Ext.getBody().mask('Loading... ', 'loading').show();
						            }
						            , load: function(result, request) {
						            	Ext.getBody().unmask();						                
						            }		                   
                   } 
                      
                }),
                
			columns: [	
	    		{text: 'Descrizione', xtype: 'treecolumn', flex: 1, dataIndex: 'task', sortable: false},
	    		/*{text: 'Causale', flex: 1, dataIndex: 'causale', sortable: false},*/
				{text: 'Importo',	width: 80, dataIndex: 'imp', sortable: false, align: 'right', renderer: floatRenderer2},
				{text: 'Disponibilit&agrave;',	width: 80, dataIndex: 'disponibilita', sortable: false, align: 'right',
					renderer: function(value, metaData, rec){
    					if (value != ''){ //non vuota
    						if (parseFloat(value) > 0) metaData.tdCls += ' tpSfondoVerde';
							if (parseFloat(value) < 0) metaData.tdCls += ' tpSfondoRosa';
							if (parseFloat(value) == 0) metaData.tdCls += ' tpSfondoGiallo';							    						
    					}
    					
    					if (rec.get('disponibilita_subparziale') == true) {
    						metaData.tdCls += ' liv_dettaglio_disponibilita';
    					}
    					
					    return floatRenderer2(value);						
					 }				 
				},
				
	<?php if ($view != 'analisi_esposizione'){ ?>		
				{text: 'Tp',		width: 30, 	dataIndex: 'tipo', 			sortable: false, tooltip: 'Tipo ordine'},
				{text: 'Data',	width: 70, 	dataIndex: 'data_reg', 		sortable: false, renderer: date_from_AS},
				{text: 'St',		width: 40, 	dataIndex: 'stato', 		sortable: false, tooltip: 'Stato'},	
				{text: 'Evas.Progr.',	width: 70, 	dataIndex: 'cons_prog', 	sortable: false, renderer: date_from_AS},
				{text: 'SP',			width: 40, 	dataIndex: 'icon_dtep', 	sortable: false,
					renderer: function(value, p, rec){
					  if (rec.get('liv') == 'liv_2') {								
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';						
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	      }
	        	    }				
				},
				{text: 'Sblocco',	width: 70, 	dataIndex: 'data_sblocco', 	sortable: false, renderer: date_from_AS},
				
				{text: 'P',			width: 30, 	dataIndex: 'fl_P', 	sortable: false, tooltip: 'Prima',
					renderer: function(value, p, record){
						if (record.get('fl_P')=='N') return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';					
						if (record.get('fl_P')=='Y') return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
					}				
				},				
				{text: 'D',			width: 30, 	dataIndex: 'fl_D', 	sortable: false, tooltip: 'Dopo',
					renderer: function(value, p, record){
						if (record.get('fl_D')=='S') return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';					
						if (record.get('fl_D')=='Y') return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
					}				
				},				
				
				{text: 'Azione',	width: 60, 	dataIndex: 'azione', 	sortable: false},				
				//{text: 'Note',		width: 100, dataIndex: 'note', 		sortable: false},
		<?php } ?>							    		
    		],
	 

    		listeners: {
					afterrender: function (comp) {
					<?php $d_cli = trim($main_module->decod_cliente($cod_cli)); ?>
					 var title = comp.up('window').initialConfig.title + ' ' + <?php echo j($d_cli); ?> ;
				     comp.up('window').setTitle(title);
				     this.getStore().on({
			            scope: this
			            , beforeload: function() {
			            	//Ext.getBody().mask('Loading... ', 'loading').show();
			                //this.getEl().mask("Loading", 'x-mask-loading');
			            }
			            , load: function(result, request) {
			            	//Ext.getBody().unmask();
			                //this.getEl().unmask();
			                
			                if (Ext.isEmpty(this.getStore().proxy.reader.jsonData.ultimo_aggiornamento) == false)						                
								comp.up('window').setTitle(title + ' - ' + this.getStore().proxy.reader.jsonData.ultimo_aggiornamento);						                
			            }					            
			        });
						        						
						
					},
					
					
					itemclick: function(view,rec,item,index,eventObj) {
						if (rec.get('on_click_somma_parziale') == true) {
							console.log('aaaa');
						}
					}
					
					
					
				  , itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      var record = grid.getStore().getAt(index);		  
				      var voci_menu = [];
				      
				      
				      
				      
				  } //itemcontextmenu
					
					
					
					
					
					    		
    		}, //listeners
    		
			viewConfig: {
			        getRowClass: function(record, index) {
			           v = '';
			           if (record.get('id') == 'WTFIDO' || record.get('id') == 'WTEXPO')
			           	v = v + ' grassetto';
			           	
						v = v + ' ' + record.get('liv');			           	
			           	
			           	return v;																
			         }   
			    },

	<?php if ($view != 'analisi_esposizione'){ ?>			    
			buttons: [{
	            text: 'Visualizza<br/>esposizione corrente',
	            scale: 'large',
	            iconCls: 'icon-button_grey_play-32',	 
	            handler: function() {
						Ext.getBody().mask('Loading... ', 'loading').show();	            
							Ext.Ajax.request({
											url: 'acs_json_analisi_esposizione.php?fn=exe_richiesta_aggiornamento',
											timeout: 2400000,
									        jsonData: {
									        	add_parameters: 'E',
									        	list_selected_id: '<?php echo($m_params->list_selected_id); ?>'
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
												Ext.getBody().unmask();									        
									            acs_show_win_std('Analisi ' + this.up('window').initialConfig.title, 'acs_json_analisi_esposizione.php?fn=open_view&view=analisi_esposizione', {list_selected_id: '<?php echo($m_params->list_selected_id); ?>'}, 600, 500, {}, 'icon-info_blue-16');
									        }, scope: this,
									        failure    : function(result, request){
												Ext.getBody().unmask();									        
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });            	                	                
	            }
	        }, { xtype: 'tbfill' }, {
	            text: 'Esegui analisi<br/>blocco/sblocco ordini',
	            scale: 'large',
	            iconCls: 'icon-button_black_play-32',	 
	            handler: function() {
						Ext.getBody().mask('Loading... ', 'loading').show();	            
							Ext.Ajax.request({
											url: 'acs_json_analisi_esposizione.php?fn=exe_richiesta_aggiornamento',
									        jsonData: {
									        	add_parameters: '',
									        	list_selected_id: '<?php echo($m_params->list_selected_id); ?>'
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
												Ext.getBody().unmask();									        
									            this.up('treepanel').getStore().load();
									            
									        }, scope: this,
									        failure    : function(result, request){
												Ext.getBody().unmask();									        
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });            	                	                
	            }
	        }]			    												    
		<?php } ?>    		
    		
	}
]}
<?php exit; }?>	
