<?php
require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();


if (strlen($_REQUEST['k_ordine']) > 0)
    $k_ord = $_REQUEST['k_ordine'];
else
    $k_ord = $m_params->k_ordine;

$row_ord  = $s->get_ordine_by_k_ordine($k_ord);

$bl = null;
if (strlen($_REQUEST['bl']) > 0)
	$bl = $_REQUEST['bl'];
if(isset($m_params->bl) && $m_params->bl != '')
    $bl = $m_params->bl;


if (strlen($_REQUEST['num_ann']) > 0){
	$num_ann = $_REQUEST['num_ann'];
}elseif(strlen($m_params->num_ann) > 0){ 
   $num_ann = $m_params->num_ann;
}else{
	$num_ann = 4;
}

$commento_txt = $s->get_commento_ordine_by_k_ordine($k_ord, $bl);
	
$object_from_id	= $_REQUEST['liv1']; //id (extjs) grid/tree su cui ho fatto click
$tipo_refresh 	= $_REQUEST['liv2']; //'ALERT' -> Aggiorno il record 

?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            items: [ 
                	{
                	xtype: 'hidden',
                	name: 'fn',
                	value: 'upd_commento_ordine'
                	},{
                	xtype: 'hidden',
                	name: 'k_ordine',
                	value: '<?php echo $k_ord ?>'
                	},
                
                	{
                	xtype: 'hidden',
                	name: 'bl',
                	value: '<?php echo $bl ?>'
                	},
                	
                		<?php for($i=1; $i<= $num_ann;$i++){ ?>
                		{
							 xtype: 'fieldcontainer',
							 flex: 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
    						 {
    						name: 'f_h_text_<?php echo $i ?>',
    						xtype: 'textfield',
    						hidden : true,
    					    anchor: '-15',
    					    maxLength: 100,					    
    					    value: <?php echo j(trim($commento_txt[$i]['text'])); ?>,							
    					}, {
    						name: 'f_text_<?php echo "{$i}_{$commento_txt[$i]['rrn']}" ?>',
    						xtype: 'textfield',
    						flex : 1,
    						fieldLabel: '',
    					    anchor: '-15',
    					    maxLength: 100,					    
    					    value: <?php echo j(trim($commento_txt[$i]['text'])); ?>,							
    					},	
    					
    					<?php 
    					
    					$data_user_ge = $commento_txt[$i]['user_ge']." ".print_date($commento_txt[$i]['data_ge']);
    					$data_user_um = $commento_txt[$i]['user_um']." ".print_date($commento_txt[$i]['data_um']);
    					if($data_user_ge != $data_user_um && $commento_txt[$i]['data_um'] > 0)
    					    $data_user_txt = "Immissione: ".$data_user_ge."<br>Modifica: ".$data_user_um;
					    else
					        $data_user_txt = "Immissione: ".$data_user_ge;
					       
    					?>
    					   {
    						name: 'f_data_user',
    						xtype: 'button',
    						iconCls: 'icon-comment_edit-16', 
    						anchor: '-15',	
    						disabled : true,
    						<?php if(trim($data_user_ge) != ''){?>				    
    					    	tooltip: <?php echo j($data_user_txt); ?>						
    					   <?php }?>
    					   }
						
						]},
            		
					<?php }?>	
                	
                	
				],
				
				listeners: {
		               afterrender: function (comp) {
		               <?php if(trim($bl) == 'PRO'){ ?>
		               		comp.up('window').setTitle('Annotazioni controllo margine ' + '<?php echo "{$row_ord['TDOADO']}_{$row_ord['TDONDO']}_{$row_ord['TDOTPD']}"; ?>');
		               <?php }else{?>
		               		comp.up('window').setTitle('Annotazioni ordine ' + '<?php echo "{$row_ord['TDOADO']}_{$row_ord['TDONDO']}_{$row_ord['TDOTPD']}"; ?>');
		               <?php }?>
		                
		            }
		         },
				
			buttons: [
			
<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] != 'Y' && is_null($_REQUEST['bl'])){ ?>			
				{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					if(form.isValid()){	            	
			                form.submit({
				                            //waitMsg:'Loading...',
				                            success: function(form,action) {
				                              console.log(loc_win);
				                              if (!Ext.isEmpty(loc_win.events.afteroksave))
											  	loc_win.fireEvent('afterOkSave', loc_win);
											  else
				            				    loc_win.close();		                            	
				                            },
				                            failure: function(form,action){
				                                Ext.MessageBox.alert('Erro');
				                            }
				                        });

				    }            	                	                
	            }
	        }
<?php } ?>	        
	        
	        ],             
				
        }
]}