<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($s->get_cod_mod());
$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();
	
	$where  = sql_where_params($m_params->form_values);

	$sql = "SELECT *
			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} AO 
				ON TD.TDDOCU = AO.ASDOCU AND AO.ASCAAS = 'SBLOC' AND ASFLRI <> 'Y'
                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
					ON TD.TDDT=SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL
					ON TD.TDDT = CAL.CSDT AND TD.TDDTEP = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']}
					ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
					ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_SEQST
					ON TD.TDDT = TA_SEQST.TADT AND TA_SEQST.TATAID = 'SEQST' AND TA_SEQST.TAKEY1 = TD.TDSTAT
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_analisi_esposizione']} CLI_ESPO
					ON TD.TDDT = CLI_ESPO.WTDT AND TD.TDCCON = DIGITS(CLI_ESPO.WTCCON)
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ATTAV
					ON TD.TDDT = TA_ATTAV.TADT AND TA_ATTAV.TATAID = 'ATTAV' AND TA_ITIN.TAKEY1 = 'SBLOC'
			WHERE " . $s->get_where_std() . " AND (TD.TDFN02=1 OR TD.TDFN03=1) 
            {$where}";
	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);		
	

	while ($row = db2_fetch_assoc($stmt)) {
			$nr = array();
				$nr['TDDCON'] 		= acs_u8e(trim($row['TDDCON']));
				$nr['TDVSRF']		= acs_u8e(trim($row['TDVSRF']));
				$nr['k_ordine']		= trim($row['TDDOCU']);
				$nr['ordine_out']	= $s->k_ordine_out($row['TDDOCU']);
				$nr['tipoOrd']		= trim($row['TDOTPD']);
				$nr['fl_cli_bloc']	= trim($row['TDFN02']);
				$nr['fl_evaso']		= trim($row['TDFN11']);
				$nr['fl_bloc']		= $s->get_ord_fl_bloc($row);
				$nr['cons_prog']	= $row['TDDTEP'];
				$nr['data_atp']	    = $row['TDDATP'];
				$nr['cons_rich']	= $row['TDODER'];
				$nr['priorita']     = acs_u8e($row['TDOPRI']);
			    $nr['stato'] 		= trim($row['TDSTAT']);				
				$nr['raggr'] 		= trim($row['TDCLOR']);
				$nr['var1']			= acs_u8e(trim($row['TDDVN1']));
				$nr['k_carico']		= $s->k_carico_td($row);
				$nr['liv']			= 'liv_5'; //serve per assegna carico
											
				$nr['fl_art_manc'] = $s->get_fl_art_manc($row);
				$nr['art_da_prog'] = $s->get_art_da_prog($row);
				$nr['TDVOLU'] 	   = $row['TDVOLU'];
				
				$nr['TDODRE'] 	   = $row['TDODRE'];	
				$nr['TDTIMP'] 	   = $row['TDTIMP'];	
				$nr["dt_orig"]	   = $row['TDDTOR']; 
				$nr["prog"]   	   = $row['ASIDPR'];
				$nr['f_ril'] =  trim($row['ASFLNR']);
				
				$data[] = $nr;
	}
	
	echo acs_je($data);
	
exit;
}


$m_params = acs_m_params_json_decode();
?>
{"success": true, "items":
	{
		xtype: 'gridpanel',
		multiSelect: true,
		selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
        stateful: true,
        stateId: 'seleziona-ordini',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
        
        features: [
	
		{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 	type: 'boolean',
			dataIndex: 'visible'
		     }
		      ]
		}],
		
  	    store: Ext.create('Ext.data.Store', {
													
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							type: 'ajax',
							actionMethods: {
								read: 'POST'
							},							
							reader: {
					            type: 'json',
					            root: 'root'
					        }
					        
					        , extraParams: <?php echo acs_raw_post_data(); ?>
					        
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
							, doRequest: personalizza_extraParams_to_jsonData					        
					        
						},
	        			fields: [
	            			'TDCCON', 'TDDCON', 'TDVOLU', 'TDSELO', 'fl_bloc', 'fl_cli_bloc', 'localita', 'TDVSRF', 'k_ordine', 'ordine_out', 
	            			'tipoOrd', 'cons_prog', 'fl_evaso', 'colli_sped', 'proforma', 'stato', 'raggr', 'var1', 'cod_articolo','des_articolo', 'um', 'qta',
	            			'k_carico', 'fl_bloc', 'iconCls', 'liv', 'fl_cons_conf', 'carico', 'lotto', 'rif_scarico', 'indice_rottura', 'fl_art_manc', 'art_da_prog',
	            			'data_atp', 'cons_rich', 'priorita', 'TDODRE', 'TDTIMP', 'dt_orig', 'prog', 'f_ril'
	        			]
	    			}),
	    			
		        columns: [{
						    text: '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> height=25>',
						    width: 30, tooltip: 'Clienti bloccati', dataIndex: 'fl_cli_bloc', 
					    	tdCls: 'tdAction',         			
			            	menuDisabled: true, sortable: false,            		        
							renderer: function(value, p, record){if (record.get('fl_cli_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
							
					  }, 
		             {
		                header   : 'Cliente',
		                dataIndex: 'TDDCON', 
		                flex: 1
		                , filter: {type: 'string'}, filterable: true
		             },
		              {
		                header   : 'Riferimento',
		                dataIndex: 'TDVSRF', 
		                width     : 110
		                , filter: {type: 'string'}, filterable: true
		             }, {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Ordini bloccati', dataIndex: 'fl_bloc', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';			    				    	
    			    	}},	
    			    	
    			    	
			    	    {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', 
			    	    		dataIndex: 'art_mancanti', menuDisabled: true, sortable: false,
			    	    	    tooltip: 'Ordini MTO',    	    		    
			    			    renderer: function(value, p, record){
			    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
			    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
			    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=18>';
			    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
			    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';			    	
			    			    	}},	
			    	
					 {text: '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Consegna confermata', dataIndex: 'fl_cons_conf', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    	if (record.get('fl_cons_conf')==1) return '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=18>';
    			    	}},  
    			    	 {
		                header   : 'Data',
		                dataIndex: 'TDODRE', 
		                width     : 70,
		                renderer: date_from_AS
		             },	
    			 	  {
		                header   : 'Ordine',
		                dataIndex: 'ordine_out', 
		                width     : 110
		                , filter: {type: 'string'}, filterable: true
		             },			        
		             {
		                header   : 'Tp', tooltip: 'Tipo',
		                dataIndex: 'tipoOrd',
		                tdCls: 'tipoOrd', 
		                width     : 30,
		                renderer: function (value, metaData, record, row, col, store, gridView){						
							metaData.tdCls += ' ' + record.get('raggr');										
							return value;			    
						}
						, filter: {type: 'string'}, filterable: true
		             }, 
		         
		             {text: 'Prod./<br>Disp.sped.',	width: 65, dataIndex: 'cons_prog', renderer: date_from_AS},
		             {
		                header   : 'Importo',
		                dataIndex: 'TDTIMP', 
		                width     : 70,
		                renderer: floatRenderer2,
		                align: 'right'
		             },	
    			     {text: 'St.',	width: 30, dataIndex: 'stato', tooltip: 'Stato ordine'},
		             {text: 'Variante',	width: 100, dataIndex: 'var1' 		<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo ", hidden: true" ?>, filter: {type: 'string'}, filterable: true},
		           
		         ]	    					
		
		, listeners: {		
	 		
		   itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
		  		var voci_menu = [];
		  		
        		id_selected = grid.getSelectionModel().getSelection();
					  list_selected_id = [];
					  for (var i=0; i<id_selected.length; i++) 
						list_selected_id.push({id: id_selected[i].get('id'), 
									prog: id_selected[i].get('prog'),
									dt_orig: id_selected[i].get('dt_orig'), 
									f_ril: id_selected[i].get('f_ril'),
									k_ordine: id_selected[i].get('k_ordine')});
									 voci_menu.push({
			      		text: 'Avanzamento/Rilascio attivit&agrave;',
			    		iconCls: 'icon-arrivi-16',
			    		handler: function() {

				       //verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('rec_stato') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
										    
										    //verifico se e' cambiato lo stato
										    grid.panel.refresh_stato_per_record(id_selected[i].get('id'));
										    
					                      } //per ogni record selezionato
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {list_selected_id: list_selected_id, grid_id: grid.id},
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
			    		}
					  });
					  
					  <?php
$causali_rilascio = $s->find_TA_std('RILAV', 'SBLOC', 'N', 'Y');					  
foreach($causali_rilascio as $ca) {
?>	
					  
	if (rec.get('k_ordine').length > 0 && rec.get('rec_stato')!='Y') //sono nel livello ordine					  
	voci_menu.push({
	      		text: <?php echo j($ca['text']); ?>,
	    		iconCls: 'iconAccept',
	    		handler: function() {					 
	
						Ext.Ajax.request({
						        url        : 'acs_op_exe.php?fn=exe_avanzamento_segnalazione_arrivi_json',
						        jsonData: {
						        	list_selected_id: list_selected_id,
						        	f_entry_prog_causale: <?php echo j($ca['id']); ?>
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
									for (var i=0; i<id_selected.length; i++){
									 id_selected[i].set('rec_stato', 'Y');
									}
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
		
			    }
			  });
  <?php } ?>
  
  				voci_menu.push({
			         		text: 'Visualizza righe',
			        		iconCls : 'icon-folder_search-16',          		
			        		handler: function() {
			        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {k_ordine: rec.get('k_ordine')}, 900, 450, null, 'icon-folder_search-16');          		
			        		}
			    		});		
      
				
			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				}).showAt(event.xy);				
			  }	
		 		
	 			 			
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {		   
		            v = rec.get('liv');
		          if (rec.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
		             	
		          return v;	
		         }   
		    }
		    
		



	}
}

<?php 

function sql_where_params($m_params){
    global $cfg_mod_Spedizioni;
    
    $where = '';
    
    
    $where .= sql_where_by_combo_value('CAL.CSFG01',     $m_params->f_stadio);
    $where .= sql_where_by_combo_value('TD.TDCITI',      $m_params->f_itinerario);
    $where .= sql_where_by_combo_value('SP.CSCVET',      $m_params->f_vettore);
    $where .= sql_where_by_combo_value('TDCPAG',         $m_params->f_pagamento);
    $where .= sql_where_by_combo_value('TDCLOR',         $m_params->f_tipologia_ordine);
    $where .= sql_where_by_combo_value('TDOTPD',         $m_params->f_tipo_ordine);
    $where .= sql_where_by_combo_value('TDCAUT',         $m_params->f_causale_trasporto);
    $where .= sql_where_by_combo_value('TD.TDCAG1',      $m_params->f_agente);
    $where .= sql_where_by_combo_value('ASUSAT',         $m_params->f_utente_assegnato);
    $where .= sql_where_by_combo_value('TD.TDCOCL',      $m_params->f_referente_cliente);
    $where .= sql_where_by_combo_value('TA_ITIN.TAASPE', $m_params->f_area);
    $where .= sql_where_by_combo_value('TD.TDCDIV', $m_params->f_divisione);
    $where .= sql_where_by_combo_value('TD.TDRISC',  $m_params->f_rischio);
    $where .= sql_where_by_combo_value('TD.TDSTAT', $m_params->f_stato_ordine);
    
    if (isset($m_params->f_cliente_cod) && strlen($m_params->f_cliente_cod) > 0)
        if ($cfg_mod_Spedizioni['disabilita_sprintf_codice_cliente'] == 'Y')
            $where.= sql_where_by_combo_value('TD.TDCCON',  $m_params->f_cliente_cod);
        else
            $where.= sql_where_by_combo_value('TD.TDCCON',  sprintf("%09s", $m_params->f_cliente_cod));
            
    if (strlen($m_params->f_data_prod) > 0)
        $where .= " AND TD.TDDTEP = " . $m_params->f_data_prod;
    if (strlen($m_params->f_data_prod_da) > 0)
        $where .= " AND TD.TDDTEP >= " . $m_params->f_data_prod_da;
    if (strlen($m_params->f_data_prod_a) > 0)
        $where .= " AND TD.TDDTEP <= " . $m_params->f_data_prod_a;
                            
                            //data registrazione
    if (strlen($m_params->f_data_reg_da) > 0)
        $where .= " AND TD.TDODRE >= " . $m_params->f_data_reg_da;
    if (strlen($m_params->f_data_reg_a) > 0)
        $where .= " AND TD.TDODRE <= " . $m_params->f_data_reg_a;
            
    if (strlen($m_params->f_data_emiss_OF_da) > 0)
        $where .= " AND TD.TDDEOA >= " . $m_params->f_data_emiss_OF_da;
    if (strlen($m_params->f_data_emiss_OF_a) > 0)
        $where .= " AND TD.TDDEOA <= " . $m_params->f_data_emiss_OF_a;
                    
    //data/ora assegnazione
    if (strlen($m_params->f_assegnazione_da_data) > 0)
        $where .= " AND ASDTAS >= " . $m_params->f_assegnazione_da_data;
    if (strlen($m_params->f_assegnazione_da_ora) > 0)
        $where .= " AND ASHMAS >= " . $m_params->f_assegnazione_da_ora;
    if (strlen($m_params->f_assegnazione_a_data) > 0)
        $where .= " AND ASDTAS <= " . $m_params->f_assegnazione_a_data;
        
    //data/ora scadenza
    if (strlen($m_params->f_scadenza_da_data) > 0)
        $where .= " AND ASDTSC >= " . $m_params->f_scadenza_da_data;
    if (strlen($m_params->f_scadenza_a_data) > 0)
        $where .= " AND ASDTSC <= " . $m_params->f_scadenza_a_data;
        
    if (isset($m_params->f_num_ordine)  && strlen($m_params->f_num_ordine) > 0 )
        $where .= " AND TDONDO='{$m_params->f_num_ordine}'";
        
    if (isset($m_params->f_riferimento) && strlen($m_params->f_riferimento) > 0)
        $where .= " AND UPPER(TDVSRF) LIKE '%" . strtoupper($m_params->f_riferimento) . "%'";
        
                                                                //------------------------------------------------------------------- filtri applicati da form
                                                                        
    if (isset($filtro["TAASPE"]))
        $where .= " AND TA_ITIN.TAASPE='{$filtro["TAASPE"]}'";
    if (isset($filtro["ASUSAT"]))
        $where .= " AND ASUSAT='{$filtro["ASUSAT"]}'";
    if (isset($filtro["TDCCON"]))
        $where .= " AND TDCCON='{$filtro["TDCCON"]}'";
    if (isset($filtro["TDCDES"]))
        $where .= " AND TDCDES='{$filtro["TDCDES"]}'";
    if (isset($filtro["TDCDIV"]))
        $where .= " AND TDCDIV='{$filtro["TDCDIV"]}'";
        
        return $where;
                                                                                            
}


