<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$its = $s->get_elenco_raggruppato_itinerari_per_data($_REQUEST["node"]);
$appLog->add_bp('get_elenco_raggruppato_itinerari_per_data - END');

$ars = $s->crea_alberatura_json_per_data($its, $field_dett, $_REQUEST["node"], 'Y');
$appLog->add_bp('crea_alberatura_json_per_data - END');

echo $ars;

$appLog->save_db();

?>
