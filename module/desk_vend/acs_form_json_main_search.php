<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

//parametri del modulo (non legati al profilo)
$mod_js_parameters = $main_module->get_mod_parameters();

//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

$m_params = acs_m_params_json_decode();

?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 5px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            autoScroll: true,
            url: 'abcdeeeee.php',
            
			buttonAlign:'center',
			
			//filtri avanzati
			f_prov_d: [],
			f_loc_d: [],
			//f_subtotale_prov_d: false, //spostato in form
            
            items: [
            
            //filtri avanzati
            {								   	
	            xtype: 'checkbox'				          
		      , name: 'f_subtotale_prov_d' 
	          , boxLabel: 'Subtotale per provincia'
	          , inputValue: 'Y'
	          , hidden: true	                          
	        },
            
					 
					 
		 {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,			
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            url : <?php
		            		$cfg_mod = $s->get_cfg_mod();
		            		if ($cfg_mod['anagrafiche_cliente_da_modulo'] == 'Y')
		            			echo acs_je('acs_get_select_json.php?select=search_cli');
		            		else
		            			echo acs_je('acs_get_select_json.php?select=search_cli_anag');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            	var form = this.up('form').getForm();
	            	form.findField('f_destinazione_cod').setValue(''); 	            	
	            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
	            	form.findField('f_destinazione_cod').store.load();	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }
					 
					 
					 
					 
					 
		, {
            xtype: 'combo',
			name: 'f_destinazione_cod',
			fieldLabel: 'Destinazione',
			minChars: 2,			
            
            store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_des_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            },
                 extraParams: {
        		  	cliente_cod: ''
        		 }		            
		        },       
				fields: ['cod', 'descr'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: false, //mostro la freccetta di selezione
            anchor: '100%',

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessuna destinazione trovata',
                
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000
        }
        

					 
					 
				  ,{ 
						flex: 1,
						xtype: 'fieldcontainer',
						layout: 'hbox',						
						items: [{						
									name: 'f_riferimento',
									xtype: 'textfield',
									fieldLabel: 'Riferimento',
									value: '',
									//anchor: "-180", 
									flex: 1,					
								 }							
						
						, {
							flex: 1,							
							name: 'f_agente',
							xtype: 'combo',
							fieldLabel: 'Agente', labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json($s->get_options_agenti('Y'), '') ?> 	
							    ] 
							},
							queryMode: 'local',
                			minChars: 1, 	
							listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }							 
							}								 
						 
						]
					}

					 
					
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							flex: 1,
							name: 'f_area',
							xtype: 'combo',
							fieldLabel: 'Area spediz.',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,
							name: 'f_stabilimento',
							xtype: 'combo',
							fieldLabel: 'Stabilimento',
							labelAlign: 'right',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('START'), '') ?> 	
								    ] 
								}						 
							}
						
						]
					}					
					 
					
					
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							flex: 1,
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							multiSelect: true,
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}
							
							
						  , {
						  	xtype: 'container',
						  	layout: 'hbox',
						  	flex: 1,
						  	items: [
							
    						   {
    							flex: 1,
    							name: 'f_itinerario',
    							xtype: 'combo',
    							fieldLabel: 'Itinerario',
    							labelAlign: 'right',
    							displayField: 'text',
    							valueField: 'id',
    							emptyText: '- seleziona -',
    							forceSelection:true,
    						   	allowBlank: true,
    						   	multiSelect: true,
    							store: {
    								autoLoad: true,
    								editable: false,
    								autoDestroy: true,	 
    							    fields: [{name:'id'}, {name:'text'}],
    							    data: [								    
    								     <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
    								    ] 
    								},
    							 queryMode: 'local',
                    			 minChars: 1, 	
    							 listeners: { 
                					 	beforequery: function (record) {
                			         	record.query = new RegExp(record.query, 'i');
                			         	record.forceAll = true;
                		             }
                		          }						 
    							}
    							
    							
    							//filtro avanzato provincia/localita
    							, {									  
                        			xtype: 'displayfield',
                        			editable: false,
                        			fieldLabel: '',
                        			padding: '0 0 0 5',
                        		    value: <?php echo j("<img src=" . img_path("icone/48x48/globe.png") . " width=16>"); ?>,
                        		    tooltip: 'Selezione localit&agrave;/provincia',
                        		    listeners: {
                    		            render: function( component ) {
                    		            
                    		            	var m_form = this.up('form');
                    		            
                    						component.getEl().on('dblclick', function( event, el ) {
                    						
                    					    var my_listeners = {
                        	    		  			afterOkSave: function(from_win, form_values){
                                						m_form.f_prov_d = form_values.f_prov_d;
														m_form.f_loc_d  = form_values.f_loc_d;
														
														//spostato in form
														m_form.getForm().findField('f_subtotale_prov_d').setValue(form_values.f_subtotale_prov_d);
														
														from_win.close();
                        				        		}
                        		    				};	
                        		        		 
                        				         acs_show_win_std(null, 
                        				         	'../desk_vend/acs_info_seleziona_provincia_localita.php?fn=open', {
                        				         		form_values: m_form.getValues()
                        				           }, null, null,  my_listeners);	    
                    						});										            
                    		             }
                    				}										    
                        		    
                        		    
                        		}
    							
    							
						     ]
						   }
						]
					}, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							labelAlign: 'right',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							}
						
						]
					}
					
					
					
					/* priorita', modello */
					, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							flex: 1,						
							name: 'f_priorita',
							xtype: 'combo',
							fieldLabel: 'Priorit&agrave;',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_priorita(), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_modello',
							xtype: 'combo',
							fieldLabel: 'Modello',
							labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_modello(), '') ?> 
								    ] 
								},
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }							 
							}
						
						]
					}
					

					/* referente ordine e cliente */
					, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							flex: 1,						
							name: 'f_referente_cliente',
							xtype: 'combo',
							fieldLabel: 'Referente cliente',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_referente_cliente(), '') ?> 	
								    ] 
								},
						   	 queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }						 
							}, {
							flex: 1,							
							name: 'f_referente_ordine',
							xtype: 'combo',
							fieldLabel: 'Referente ordine',
							labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_referente_ordine(), '') ?> 
								    ] 
								},
							  queryMode: 'local',
	                		  minChars: 1, 	
							  listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }						 
							}
						
						]
					}					
					
					
					/* conf.prog, hold */
					, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [
						 {
							flex: 1,							
							name: 'f_confermati',
							xtype: 'combo',
							fieldLabel: 'Conf.Progr.',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: 'T', text: 'Tutti'},
								     {id: 'C', text: 'Confermati'},
								     {id: 'N', text: 'Da confermare'}
								    ] 
								}						 
							}
						 , {
							flex: 1,							
							name: 'f_hold',
							xtype: 'combo',
							fieldLabel: 'Hold', labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: 'T', text: 'Tutti'},
								     {id: 'H', text: 'Solo Hold'},
								     {id: 'P', text: 'Hold esclusi'}
								    ] 
								}						 
							}								
						
						]
					}					
					
					
					
					
					
					
					
					/* num.ord / solo bloccati */					
					, { 
						flex: 1,
						xtype: 'fieldcontainer',
						layout: 'hbox',						
						items: [
								{						
									name: 'f_num_ordine',
									xtype: 'textfield',
									fieldLabel: 'Nr. Ordine',
									value: '',
									width: 170					
								 }							
							
						,{						
							name: 'f_num_contract',
							xtype: 'textfield',
							labelWidth : 60,
							labelAlign: 'right',
							fieldLabel: 'Contract',
							value: '',
							width: 130					
						 }	
						, {
							flex: 1,							
							name: 'f_solo_bloccati',
							xtype: 'combo',
							fieldLabel: 'Bloccati', labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: 'T', text: 'Tutti'},
								     {id: '1', text: 'Solo bloccati'},
								     {id: 'N', text: 'Bloccati esclusi'}
								    ] 
								}						 
							}								 
						 
						]
					}

					
					/* nr contract (stock) e mercato (canale di vendita) */					
					, { 
						flex: 1,
						xtype: 'fieldcontainer',
						layout: 'hbox',						
						items: [
						  {	
						    flex: 1,						
							name: 'f_ava_paga',
							xtype: 'combo',
							fieldLabel: 'Avanzam.pag.',		
							multiSelect : true,					
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('BGAG'), ""); ?> 
								    ] 
								}						 
						   							 
							}, {
							flex: 1,							
							name: 'f_mercato',
							xtype: 'combo',
							fieldLabel: 'Mercato', labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('MERCA'), ""); ?> 
								    ] 
								}						 
						   							 
							}								 
						 
						]
					}
					
					, { 
						flex: 1,
						xtype: 'fieldcontainer',
						layout: 'hbox',						
						items: [
						  {	
						    flex: 1,						
							name: 'f_forniture_MTO',
							xtype: 'combo',
							fieldLabel: 'Forniture MTO',
							multiSelect : true,				
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id : 'OE', text : 'Ordini da emettere'},
								     {id : 'OEA', text : 'Ordini emessi/aperti'},
								     {id : 'OEE', text : 'Ordini emessi/evasi'},
								     {id : 'SOM', text : 'Senza ordini MTO'},
								    ] 
								}						 
						   							 
							},
							{xtype : 'component', flex : 1}
						]
					}
					
					<?php if($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){?>
					, {						
						name: 'f_articolo',
						xtype: 'textfield',
						fieldLabel: 'Articolo',
						value: '', 
						margin : '0 0 0 0',
						width: 200,					
					 }	
					
					<?php }?>
					
					
					
					

					 
			,{
	            xtype:'fieldset',
	            title: 'Selezione date',
	            collapsible: true,
	            collapsed: true, // fieldset initially collapsed	            
	            defaultType: 'textfield',
	            layout: 'anchor',
	            defaults: {
	                anchor: '100%'
	            },
	            items :[
					{ 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data programm.',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_dal'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_al'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}

					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data spedizione',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_sped_dal'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_sped_al'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					
					
					
					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data rilascio',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_ril_dal'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_ril_al'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
										
					
					
					

					
					
					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data ricezione',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_ricezione_dal'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_ricezione_al'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					
					

					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data conferma',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_conferma_dal'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_conferma_al'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					
						
					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data scarico',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_scarico_dal'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_scarico_al'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}						
						
						   
						
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data disponib.',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_disp_dal'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_disp_al'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					
					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data evas. rich.',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_ev_rich_dal'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_ev_rich_al'
						   , format: 'd/m/Y'
						   , altFormats: 'Ymd'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}					
												
						
						   
						            
	            ]
	        }					 
					 
							
					
					
					
					

					
			,{
	            xtype:'fieldset',
	            title: 'Selezione colli',
	            collapsible: true,
	            collapsed: true, // fieldset initially collapsed	            
	            defaultType: 'textfield',
	            layout: 'anchor',
	            defaults: {
	                anchor: '100%'
	            },
	            items :[

					
					, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px;',
						border: false,	
			            anchor: '100%',								            
			            defaults: {
			            	anchor: '100%',
			                width: 130, labelWidth: 120			            	                
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Colli disponibili',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'collo_disp',
			                checked: true,			                
			                inputValue: 'T'
		
			            }, {
			                boxLabel: 'Con',
			                checked: false,			                
			                name: 'collo_disp',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'Senza',
			                checked: false,			                
			                name: 'collo_disp',
			                inputValue: 'N'		                
			            }
			            
			            
			            ]
			        }					
					
					
					
					, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 130, labelWidth: 120
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Colli spediti',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'collo_sped',
			                checked: true,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Con',
			                checked: false,			                
			                name: 'collo_sped',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'Senza',
			                checked: false,			                
			                name: 'collo_sped',
			                inputValue: 'N'		                
			            }]
			        }					
						            
	            ]
	         }					
					
					
					
					
					
					
					
					
					
					
					
					
			,{
	            xtype:'fieldset',
	            title: 'Selezione carico/lotto/proforma/indice rottura/spedizione',
	            collapsible: true,
	            collapsed: true, // fieldset initially collapsed	            
	            defaultType: 'textfield',
	            layout: 'anchor',
	            defaults: {
	                anchor: '100%'
	            },
	            items :[
	            
					 {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Carico spedizione',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'carico_assegnato',
			                checked: true,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Con carico',
			                checked: false,			                
			                name: 'carico_assegnato',
			                inputValue: 'Y',
			                width: 95, labelWidth: 85
			            }, {
			                boxLabel: 'Senza',
			                checked: false,			                
			                name: 'carico_assegnato',
			                inputValue: 'N'		                
			            },  {
							name: 'f_num_carico',
							xtype: 'textfield',
							fieldLabel: 'Nr. Carico',
							value: '',
			                width: 130, labelWidth: 60							
						 }]
			        }, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Lotto produzione',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'lotto_assegnato',
			                checked: true,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Con lotto',
			                checked: false,			                
			                name: 'lotto_assegnato',
			                inputValue: 'Y',
			                width: 95, labelWidth: 85
			            }, {
			                boxLabel: 'Senza',
			                checked: false,			                
			                name: 'lotto_assegnato',
			                inputValue: 'N'		                
			            }, {
							name: 'f_num_lotto',
							xtype: 'textfield',
							fieldLabel: 'Nr. Lotto',
							value: '',
			                width: 130, labelWidth: 60														
						 } ]
			        }, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Proforma',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'proforma_assegnato',
			                checked: true,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Con proforma',
			                checked: false,			                
			                name: 'proforma_assegnato',
			                inputValue: 'Y',
			                width: 95, labelWidth: 85
			            }, {
			                boxLabel: 'Senza',
			                checked: false,			                
			                name: 'proforma_assegnato',
			                inputValue: 'N'		                
			            }, {
							name: 'f_num_proforma',
							xtype: 'textfield',
							fieldLabel: 'Nr. Prof.',
							value: '',
			                width: 130, labelWidth: 60														
						 } ]
			        }, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Indice di produzione',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'indice_rottura_assegnato',
			                checked: true,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Con indice',
			                checked: false,			                
			                name: 'indice_rottura_assegnato',
			                inputValue: 'Y',
			                width: 95, labelWidth: 85
			            }, {
			                boxLabel: 'Senza',
			                checked: false,			                
			                name: 'indice_rottura_assegnato',
			                inputValue: 'N'		                
			            }, {
							name: 'f_indice_rottura',
							xtype: 'textfield',
							fieldLabel: 'Indice',
							value: '',
			                width: 130, labelWidth: 60														
						 } ]
			        }
			        
			        
			        
			        
					, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Nr. Spedizione',
		                    	width: 150
		                    },	{
							name: 'f_num_sped',
							xtype: 'textfield',
							fieldLabel: '',
							value: '',
			                width: 130, labelWidth: 60							
						 },
						 	{
							name: 'f_conf',
							xtype: 'textfield',
							fieldLabel: 'Non conformit&agrave;',
	                    	labelAlign : 'right',
							value: '',
							margin : '0 0 0 25',
							maxLength : 3,
			                width: 220, labelWidth: 150							
						 }	
						]	 		        
			       } ,
			       
			       {
		                    xtype: 'checkboxgroup',
		                    layout: 'hbox',
		                    fieldLabel: 'Solo stadio',
		                    margin : '0 0 0 10',
		                    labelWidth : 140,
		                    defaults: {
//		                     width: 150,
		                     padding: '0 10 10 0'
		                    },
		                    items: [{
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Ice'
				                  , inputValue: '1'
				                  , width: 80                
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Slush'
				                  , inputValue: '2'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Water'
				                  , inputValue: '3'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Altro'
				                  , inputValue: '0'
				                  , width: 80                        
				                 }
		                   ]
		                }
			        	            
	            
	            ]
	         }					
					
					
					
					
					
					
			,{
	            xtype:'fieldset',
	            title: 'Selezione ordini evasi',
	            collapsible: true,
	            collapsed: true, // fieldset initially collapsed	            
	            defaultType: 'textfield',
	            layout: 'anchor',
	            defaults: {
	                anchor: '100%'
	            },
	            items :[
	            
					
					 {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 130, labelWidth: 120
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Ordini evasi',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'ordini_evasi',
			                inputValue: 'T'			                
<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] != 'Y') { ?>			                
			                , checked: true
<?php } ?>			                

			            }, {
			                boxLabel: 'Da evadere',
			                checked: false,			                
			                name: 'ordini_evasi',
			                inputValue: 'N'
<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') { ?>			                
			                , checked: true
<?php } ?>			                
			                
			            }, {
			                boxLabel: 'Evasi',
			                checked: false,			                
			                name: 'ordini_evasi',
			                inputValue: 'Y'		                
			            }]
			        }, {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			            defaults: {
			                anchor: '100%',
			                width: 130, labelWidth: 120
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Anomalie evasione',
		                    	width: 150
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'anomalie_evasione',
			                checked: true,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Con anomalie',
			                checked: false,			                
			                name: 'anomalie_evasione',
			                inputValue: 'Y'		                
			            }]
			        } 
						            
	            
	            ]
	          }					
					
					
										
					

					
					
				],
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "MAIN_SEARCH");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',
	            handler: function() {
	                var m_form = this.up('form');
	            	var form = this.up('form').getForm();
					var form_values = form.getValues();
					var stadio = form_values.f_stadio;
					
					console.log('form_values');
					console.log(form_values);
					
					var to_extraParams = m_form.acs_actions.to_extraParams(m_form, form);
					
				    if(Array.isArray(stadio)){
	                   stadio = form_values.f_stadio.join(',');
	                }else if(stadio != ''){
	                   stadio = form_values.f_stadio;
	                }else if(typeof(stadio) === 'undefined') stadio = '';

		             
		            //se ho ricevuto tree_id, significa che sono stati riaperti i parametri di un tree gia' aperto
		            // -> non creo un nuovo tree ma aggiorno i parametri su quello attuale
		            <?php if (isset($m_params->tree_id)){ ?>
		            	var m_tree = Ext.getCmp(<?php echo j($m_params->tree_id) ?>);
		            	
		            	//memorizzo le nuove scelte
		            	m_tree.form_values = form_values;
		            	//m_tree.f_prov_d = to_extraParams.f_prov_d.split(',');
						//m_tree.f_loc_d = to_extraParams.f_loc_d.split(',');          
            			//spostato in form: m_tree.f_subtotale_prov_d = to_extraParams.f_subtotale_prov_d;
		            	
		            	m_tree.getStore().proxy.extraParams = to_extraParams;
		            	m_tree.getStore().load();
		            	this.up('window').destroy();
		            	return false;
		            <?php } ?>                

		mp = Ext.getCmp('m-panel');
         	
<?php
	    $ss .= "<span class=acs_button style=\"\">";
	    $ss .= "<img itemId=\"print\" src=" . img_path("icone/48x48/print.png") . " height=20>";
	    $ss .= "</span>";
	    
	    $ss = '';
?>	    
		                            	
/* ELENCO ORDINI */        
	var colElencoOrdiniSearch = [	
		{text: 'Itinerario/Cliente-Destinazione &nbsp;&nbsp;<?php echo $ss; ?>', 		xtype: 'treecolumn', width: 250, dataIndex: 'task',
    		  menuDisabled: true, sortable: false,		
          	  renderer: function (value, metaData, record, row, col, store, gridView){						
  				if (record.get('liv') == 'liv_3') metaData.tdCls += ' auto-height';
  				
  	  			if (record.get('tooltip_ripro').length > 0)
	    		 	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_ripro')) + '"';
	    		 	  															
  				return value;			    
				}		
		},
		
		/* {
            xtype: 'actioncolumn',
			text: '<img src=<?php echo img_path("icone/48x48/mobile.png") ?> width=25>',
            width: 25, menuDisabled: true, sortable: false,
            stopSelection : false,
            items: [{
                icon: <?php echo img_path("icone/24x24/mobile.png") ?>,
                // Use a URL in the icon config
                tooltip: 'Menu',
               
                handler: function (grid, rowIndex, colIndex, a, event, node) {
        			var rec = grid.getStore().getAt(rowIndex);	
        			
        			
					 if (rec.get('liv') == 'liv_0')			  	
        				showMenu_liv_0(grid, rec, node, rowIndex, event);
        	         if (rec.get('liv') == 'liv_1')			  	
        				showMenu_liv_1(grid, rec, node, rowIndex, event);
        	         if (rec.get('liv') == 'liv_2')	
        	            showMenu_liv_2(grid, rec, node, rowIndex, event);
        		     if (rec.get('liv') == 'liv_3')			  	
        				showMenu_liv_3(grid, rec, node, rowIndex, event);		
					
					
                }
            }]
        },
		*/
		
		<?php
			//inclusione colonne flag ordini
			//$js_include_flag_ordini = array('menuDisabled' => false, 'sortable' => true);
			require("_include_js_column_flag_ordini.php"); 
		?>,			    	
			    		    		
		{text: 'Seq', 			width: 33, dataIndex: 'seq_carico',
    		  renderer: function(value, metaData, record){
        		  
		    	if (record.get('liv') == 'liv_2') return value;
		    	if (record.get('liv') == 'liv_3'){
		    	    	if(record.get('contract').trim() != ''){
    		    			metaData.tdCls += ' tpSfondoGiallo';
    		    			var tooltip = '<br>Contract abbinato: ' + record.get('contract');
    		    		}else{
    		    		    var tooltip = '';
    		    		}
    		    		
    		    	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_seq_carico') + tooltip) + '"';			    	
    		    		
		    		if (record.get('liv') == 'liv_3' && value== 'S')//contrassegno
	    			 	return '<img src=<?php echo img_path("icone/48x48/bandiera_grey.png") ?> width=18>';
        		    
    		    	if (record.get('liv') == 'liv_3' && value== 'I') //indice rottura
        	    		return '<img src=<?php echo img_path("icone/48x48/sticker_black.png") ?> width=18>';
    		    		    	
    		    	if (record.get('liv') == 'liv_3' && value== 'SI') //contrassegno e indice rottura
    	    			return '<img src=<?php echo img_path("icone/48x48/sticker_blue.png") ?> width=18>';
    	    			
			    	if (record.get('liv') == 'liv_3' && (value== 'Y' || value== 'C')) //contrassegno e indice rottura
    	    			return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=18>';
        		    
        		}
    		    
    		 		
		    	}		
		},
	    {text: 'Localita\'/Riferimento', 	flex: 1, dataIndex: 'riferimento'},
    	{text: 'Tp', 			width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd', 
	    	    	renderer: function (value, metaData, record, row, col, store, gridView){						
						metaData.tdCls += ' ' + record.get('raggr');
					   									
					return value;			    
			}},
	    {text: 'Data', 			width: 60, dataIndex: 'data_reg', renderer: date_from_AS},
	    {text: 'St', 			width: 30, dataIndex: 'stato'},	    	    
	    {text: 'Pr', 			width: 35, dataIndex: 'priorita', tdCls: 'tpPri',
				renderer: function (value, metaData, record, row, col, store, gridView){
			    	if (record.get('tp_pri') == 4)
						metaData.tdCls += ' tpSfondoRosa';

					if (record.get('tp_pri') == 6)
						metaData.tdCls += ' tpSfondoCelesteEl';					
					
					return value;
				    
	    			}},
		{text: 'Consegna<br>richiesta',	width: 60, dataIndex: 'cons_rich',
		renderer: function (value, metaData, record, row, col, store, gridView){
				    if (record.get('liv') == 'liv_2' && !Ext.isEmpty(record.get('tooltip_mezzi'))){		    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_mezzi')) + '"';
				       return value;
				   }else
					   return date_from_AS(value);
				    
	    			}},	    	    	    
	    {text: 'Colli',			width: 35, dataIndex: 'colli', align: 'right'},	       
	    {text: 'D',	width: 30, tdCls: 'tdAction', 
			    renderer: function(value, p, record){
			    	if (parseInt(record.get('colli_disp')) == 0 ) return '';
			    	if (parseInt(record.get('colli_disp')) <  parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_none.png") ?> width=18>';			    	
			    	if (parseInt(record.get('colli_disp')) >= parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_full_yellow.png") ?> width=18>';			    	
			    	}},	    
	    {text: 'S',	width: 30, tdCls: 'tdAction',
			    renderer: function(value, p, record){
			    	if (parseInt(record.get('colli_sped')) == 0) return '';
			    	if (parseInt(record.get('colli_sped')) <  parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_none_black.png") ?> width=18>';			    	
			    	if (parseInt(record.get('colli_sped')) >= parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_full_green.png") ?> width=18>';			    	
			    	
			    	}},
    	<?php if ($mod_js_parameters->v_pallet == 1){ ?>			    	
	    	{text: 'Pallet',			width: 30, dataIndex: 'pallet'},
	    <?php } ?>				    		    
	    {text: 'Volume',			width: 50, dataIndex: 'volume', align: 'right'},	    
		{text: 'Peso',				flex: 55, dataIndex: 'peso', align: 'right', hidden: true},

		<?php if ($js_parameters->p_nascondi_importi != 'Y'){ ?>		
			{text: 'Importo',			width: 80, dataIndex: 'importo', align: 'right',
			 renderer: function (value, metaData, record, row, col, store, gridView){ 
					if(record.get('i_paga')!= ''){	
  				    	var icona = '<img src= "/acs_portal/images/' + record.get('i_paga') + '" width=18>';
   						var i_paga = '<span style="display: inline; float: left;">' + icona +'</span>';
    				return i_paga + value;
					}else
						return value;
  						
  			}},
		<?php  } ?>
			
		{text: 'Dispon.<br>(ATP)',		width: 60, dataIndex: 'data_disp', renderer: date_from_AS},			
		{text: 'Prod./<br>Disp.sped.',	width: 60, dataIndex: 'cons_prog', renderer: date_from_AS},		
		{text: 'Carico/<br>Spediz.',	width: 60, dataIndex: 'data_sped', renderer: date_from_AS},						
		{text: 'Cons.<br>entro',		width: 60, dataIndex: 'data_cons_cli', renderer: date_from_AS}
		
    	<?php if ($mod_js_parameters->v_gg_rit == 1){ ?>
    		,{text: 'GG<br>Rit.', width: 30, dataIndex: 'gg_rit'},
    	<?php } ?>				
	]
	



		                            	
		el = Ext.create('Ext.tree.Panel', {		
	        title: 'Info',
	        cod_iti: "",
	        
        	stateful: true,
        	stateId: 'info-std',
	        
	       
	        //selType: 'cellmodel',
	        <?php echo make_tab_closable(); ?>,
	        
	        tbar: new Ext.Toolbar({
	            items:['<b>Elenco ordini cliente da evadere/recentemente evasi per Area/Itinerario/Destinazione/Data evasione programmata</b>', '->'
	            , {xtype: 'button', text: 'Parametri', handler(b){
	            		var online_form_values = b.up('panel').form_values;
	            		acs_show_win_std('Parametri interrogazione ordini da evadere', 
    				         	'acs_form_json_main_search.php', {
    				         		form_values: online_form_values,
    				         		f_prov_d: b.up('panel').f_prov_d,
    				         		f_loc_d: b.up('panel').f_loc_d,
    				         		//spostato in form f_subtotale_prov_d: b.up('panel').f_subtotale_prov_d,
    				         		tree_id: b.up('treepanel').id
    				         	}, 650, 550,  null, 'icon-search-16');	
	              }}
	            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ console.log(this); this.up('treepanel').expandAll();}}
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	        
	        
	        cls: 'elenco_ordini',
	        
	        //memorizzo attuali parametri
	        form_values: form_values,
	        f_prov_d: m_form.f_prov_d,
	        f_loc_d:  m_form.f_loc_d,
	        //f_prov_d: to_extraParams.f_prov_d,
			//f_loc_d: to_extraParams.f_loc_d,            
            //spostato in form: f_subtotale_prov_d: to_extraParams.f_subtotale_prov_d,
            
	        
	        collapsible: true,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    model: 'ModelOrdini',
				    
					listeners: {				
								beforeload: function(store, options) {
				        			Ext.getBody().mask('Loading... ', 'loading').show();
				        			},							
					            load: function () {
					            
					            	Ext.getBody().unmask();
					            }
					         },													    
				                                          
                    proxy: {
                        type: 'ajax',
                        url: 'acs_get_elenco_ordini_from_search_json.php',
                        timeout: 240000,
                        extraParams: to_extraParams,
                        reader: {
                            root: 'children'
                        },
                        
                        
                    }
                }),
	        multiSelect: true,
	        singleExpand: false,
	
			columns: colElencoOrdiniSearch,

	        listeners: {
	        
			  afterrender: function (comp) {
			  	//console.log(Ext.fly(this.columns[0].id).select('img').elements[0].getEl());
			  }, 		        
	        	
			  itemcontextmenu : function(grid, rec, node, index, event) {
		          if (rec.get('liv') == 'liv_0')			  	
					showMenu_liv_0(grid, rec, node, index, event);
		          if (rec.get('liv') == 'liv_1')			  	
					showMenu_liv_1(grid, rec, node, index, event);
		          if (rec.get('liv') == 'liv_2')			  	
					showMenu_liv_2(grid, rec, node, index, event);										
		          if (rec.get('liv') == 'liv_3')			  	
					showMenu_liv_3(grid, rec, node, index, event);					
					
					},

			  celldblclick: {								
				  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				  	col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
				  	rec = iView.getRecord(iRowEl);
			    	var grid = this;								
				    //DAY o HOLD			    									
			    	if (rec.get('liv')=='liv_3' && ( col_name =='cons_prog' ) ){
						mp = Ext.getCmp('m-panel');
						
						if (parseInt(rec.get('fl_da_prog')) == 1)
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_from_calendar_json.php?get_iti_by_par=Y&col_name=fl_da_prog&cod_iti=' + rec.get('cod_iti') + '&da_data=' + rec.get('cons_prog'), Ext.id())
				            ).show();						
						else
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_json.php?tipo_cal=iti_data&cod_iti=' + rec.get('cod_iti') + '&data=' + rec.get('cons_prog'), Ext.id())
				            ).show();
			            
						return false; //previene expand/collapse di default			    	
			    	}
			    	
			    	//GATE O HANGAR
			    	if (rec.get('liv')=='liv_3' && (col_name =='data_sped' ) ){
						mp = Ext.getCmp('m-panel');
						
						if (parseInt(rec.get('fl_da_prog')) == 1) //apro comunque HOLD
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_from_calendar_json.php?get_iti_by_par=Y&col_name=fl_da_prog&cod_iti=' + rec.get('cod_iti') + '&da_data=' + rec.get('cons_prog'), Ext.id())
				            ).show();						
						else if (rec.get('stato_sped') == 'DP') //apro HANGAR (sped da programmare)
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_panel_flights_el_ordini.php?&col_name=DP_val&cod_iti=' + rec.get('cod_iti') + '&da_data=' + rec.get('cons_prog'), Ext.id())
				            ).show();
				        else     											//apro GATE
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_panel_flights_el_ordini.php?tipo_cal=iti_data&cod_iti=' + rec.get('cod_iti') + '&data=' + rec.get('data_sped'), Ext.id())							  
				            ).show();
			            
						return false; //previene expand/collapse di default			    	
			    	}			    	
			    		
			    		
			    	if(rec.get('liv') == 'liv_3' && col_name == 'importo'){

				  	 my_listeners = {
			  			afterOkSave: function(from_win){
			 		  		grid.getStore().load();
							from_win.close();  
			        		}
						};	
				  	 acs_show_win_std('Avanzamento pagamento ordine', 'acs_gestione_avanz_pagamento.php?fn=open_tab', {k_ordine : rec.get('k_ordine')}, 400, 150,  my_listeners, 'icon-button_black_repeat_dx-16');	

		        }									
			    														
					if (rec.get('liv')=='liv_2' && col_name=='tipo' && rec.get('tipo') != ''){
						iEvent.preventDefault();
						show_win_art_reso(rec.get('k_cli_des'));
						return false;
					}
												
					if (col_name=='art_mancanti' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();						
						show_win_art_critici(rec.get('k_ordine'), 'MTO');
						return false;							
					}	
					if (col_name=='art_da_prog' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();												
						show_win_art_critici(rec.get('k_ordine'), 'MTS');
						return false;							
					}														
					if (col_name=='colli' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();						
						show_win_colli_ord(rec, rec.get('k_ordine'));
						return false;
					}					
					if (col_name=='data_disp' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();						
					  	acs_show_win_std('Elenco fabbisogno materiali', 'acs_background_mrp_art.php?fn=fabbisogno_ordine', {k_ordine: rec.get('k_ordine')},1000, 550, null, 'icon-shopping_cart_gray-16');
						return false;
					}															
					if (col_name=='riferimento' && rec.get('liv')=='liv_2'){
						iEvent.preventDefault();						
						gmapPopup("gmap.php?ind=" + rec.get('gmap_ind'));
						return false;						
					}
																					
				 }
				},										
	       
	        	
	        	
		        itemclick: function(view,rec,item,index,eventObj) {
		          if (rec.get('liv') == 'liv_3')
		          {	
		        	var w = Ext.getCmp('OrdPropertyGrid');
		        	//var wd = Ext.getCmp('OrdPropertyGridDet');
		        	var wdi = Ext.getCmp('OrdPropertyGridImg');    
		        	var wdc = Ext.getCmp('OrdPropertyCronologia');    	
	
		        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
					wdi.store.load();		        												        	
					wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
					wdc.store.load();					
		        	
					Ext.Ajax.request({
					   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
					   success: function(response, opts) {
					      var src = Ext.decode(response.responseText);
					      w.setSource(src.riferimenti);
					      Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);					      
					   }
					});
					
					
    				//files (uploaded) - ToDo: dry 
    				var wdu = Ext.getCmp('OrdPropertyGridFiles');
    				if (!Ext.isEmpty(wdu)){
    					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
    			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
    			        	if (wdu.isVisible())		        	
    							wdu.store.load();
    					}					
    				}					
					
				   }												        	
		         }					            											        	
	        },
			    
			viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			        	v_o = record.get('liv');			        	
			        	v = record.get('liv');
			        	
			        	if (v_o=="liv_1" && record.get('n_carico')==0)
			        		v = v + ' no_carico'; 
			        	
			        	v = v + ' ' + record.get('tp_pri');												        	
			        	
			        	if (v_o=="liv_3"){
			        		v = v + ' ' + record.get('raggr');												        		
			        	} //ordini
			        	
			           if (record.get('rec_stato') == 'Y') //barrato/rilasciato
				           	v = v + ' barrato';			        	
			        	
			           return v;																
			         }   
			    }												    
			    
			    
			    																		                
	      });		                            	
		                            	
		                   
		                            	
		                            	
            		mp.add(el).show();		
		            this.up('window').close();            	                	                
	            }
	        }, {
	            text: 'Elenco',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',
	            handler: function() {
	                var form = this.up('form').getForm();
	                var m_form = this.up('form');
	                
	                if (form.isValid()){
	                
	                  //per bug su nomi campi (in alcuni manca _f)
	                	form_values = form.getValues();
                        form_values['f_carico_assegnato'] = form_values['carico_assegnato'];
                        form_values['f_lotto_assegnato'] = form_values['lotto_assegnato'];                        
                        form_values['f_proforma_assegnato'] = form_values['proforma_assegnato'];
                        form_values['f_collo_disp'] = form_values['collo_disp'];
                        form_values['f_collo_sped'] = form_values['collo_sped'];
                        form_values['f_anomalie_evasione'] = form_values['anomalie_evasione'];
                        form_values['f_ordini_evasi'] = form_values['ordini_evasi'];
                        form_values['f_indice_rottura_assegnato'] = form_values['indice_rottura_assegnato'];
                        
                        //filtri avanzati (provincia/localita)
                        form_values['f_prov_d'] = m_form.f_prov_d;
                        form_values['f_loc_d'] = m_form.f_loc_d;
                        
	                
		    			acs_show_win_std('Selezione ordini', 'acs_seleziona_ordini.php?fn=open_form', {
		        				solo_senza_carico: 'N',
		        				grid_id: null,
		        				tipo_elenco: 'DA_INFO',
			        			form_values: form_values,
			        			menu_type: menu_type
		    					},		        				 
		        				1024, 600, {}, 'icon-leaf-16');	                		                
	                }
	            }
	          }, {
	            text: 'Report',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function() {
	                var form = this.up('form').getForm();
	                var m_form = this.up('form');
	                
	                  //per bug su nomi campi (in alcuni manca _f)
	                	form_values = form.getValues();
                        form_values['f_carico_assegnato'] = form_values['carico_assegnato'];
                        form_values['f_lotto_assegnato'] = form_values['lotto_assegnato'];                        
                        form_values['f_proforma_assegnato'] = form_values['proforma_assegnato'];
                        form_values['f_collo_disp'] = form_values['collo_disp'];
                        form_values['f_collo_sped'] = form_values['collo_sped'];
                        form_values['f_anomalie_evasione'] = form_values['anomalie_evasione'];
                        form_values['f_ordini_evasi'] = form_values['ordini_evasi'];
                        form_values['f_indice_rottura_assegnato'] = form_values['indice_rottura_assegnato'];
                        
                        //filtri avanzati (provincia/localita)
                        form_values['f_prov_d'] = m_form.f_prov_d;
                        form_values['f_loc_d'] = m_form.f_loc_d;
	                
	                
	                if (form.isValid()){
	                
	                
	                acs_show_win_std('Riferimenti programmati', 'acs_print_lista_elenco_INFO.php?fn=open_form', {
		        				solo_senza_carico: 'N',
		        				filter: form_values
		    					},		        				 
		        				400, 270, {}, 'icon-print-16');	
		        				
		             
	                }
	            }
	          }
	          ],      
	          
	          
	          listeners: {
	          	afterrender: function(comp){
	          		<?php if (isset($m_params->form_values)) { ?>
	          			comp.getForm().setValues(<?php echo acs_je($m_params->form_values) ?>);
	          			comp.f_prov_d = <?php echo acs_je($m_params->f_prov_d) ?>;
	          			comp.f_loc_d =  <?php echo acs_je($m_params->f_loc_d) ?>;
	          			//spostato in form comp.f_subtotale_prov_d = <?php echo acs_je($m_params->f_subtotale_prov_d) ?>;	          			     	
	          		<?php } ?>
	          	}
	          }       
	          
	          
	          
	        , acs_actions: {
	        	to_extraParams: function(m_form, form, form_values){
	        		var form_values = form.getValues();
					var stadio = form_values.f_stadio;
					
					console.log('m_form');
					console.log(m_form);
					
					if(Array.isArray(stadio)){
	                   stadio = form_values.f_stadio.join(',');
	                }else if(stadio != ''){
	                   stadio = form_values.f_stadio;
	                }else if(typeof(stadio) === 'undefined') stadio = '';
					
					var r = {
                            //spostato in form: f_subtotale_prov_d: m_form.f_subtotale_prov_d,
                            f_subtotale_prov_d: form_values['f_subtotale_prov_d'],
                        	menu_type: menu_type,
                        	f_cliente_cod: form.findField("f_cliente_cod").getValue(),
                        	f_destinazione_cod: form.findField("f_destinazione_cod").getValue(),
                        	
                        	f_riferimento: form.findField("f_riferimento").getValue(),
                        	f_agente: form.findField("f_agente").getValue(),
                        	
                        	f_data_dal: form.findField("f_data_dal").getValue(),
                        	f_data_al: form.findField("f_data_al").getValue(),
                        	
                        	f_data_sped_dal: form.findField("f_data_sped_dal").getValue(),
                        	f_data_sped_al: form.findField("f_data_sped_al").getValue(),                        	
                        	
                        	f_data_ril_dal: form.findField("f_data_ril_dal").getValue(),
                        	f_data_ril_al: form.findField("f_data_ril_al").getValue(),
                        	                        	
                        	f_data_ricezione_dal: form.findField("f_data_ricezione_dal").getValue(),
                        	f_data_ricezione_al: form.findField("f_data_ricezione_al").getValue(),
                        	
                        	f_data_conferma_dal: form.findField("f_data_conferma_dal").getValue(),
                        	f_data_conferma_al: form.findField("f_data_conferma_al").getValue(),                        	
                        	
                        	f_data_scarico_dal: form.findField("f_data_scarico_dal").getValue(),
                        	f_data_scarico_al: form.findField("f_data_scarico_al").getValue(),                        	
                        	                        	
                        	f_data_disp_dal: form.findField("f_data_disp_dal").getValue(),
                        	f_data_disp_al: form.findField("f_data_disp_al").getValue(),
                        	
                        	f_data_ev_rich_dal: form.findField("f_data_ev_rich_dal").getValue(),
                        	f_data_ev_rich_al: form.findField("f_data_ev_rich_al").getValue(),                        	                        	                        	
                        	                        	
                        	//f_data_ricezione: form.findField("f_data_ricezione").getValue(),                        	
                        	//f_data_conferma: form.findField("f_data_conferma").getValue(),                        	
                        	
                        	
                        	f_carico_assegnato: form_values['carico_assegnato'],
                        	f_lotto_assegnato: form_values['lotto_assegnato'],
                        	f_proforma_assegnato: form_values['proforma_assegnato'],
                        	f_collo_disp: form_values['collo_disp'],
                        	f_collo_sped: form_values['collo_sped'],
                        	f_anomalie_evasione: form_values['anomalie_evasione'],                        	
                        	f_ordini_evasi: form_values['ordini_evasi'],
                        	f_indice_rottura_assegnato: form_values['indice_rottura_assegnato'],                        	
                        	f_num_sped: form_values['f_num_sped'],
                        	f_conf: form_values['f_conf'],
                        	f_stadio: stadio,
                        	f_area: form_values['f_area'],                        	                        	                        	
                        	f_stabilimento: form_values['f_stabilimento'],
                        	//f_divisione: form_values['f_divisione'],
                        	f_divisione: form.findField("f_divisione").getValue().join(','),
                        	//f_itinerario: form_values['f_itinerario'],
                        	f_itinerario: form.findField("f_itinerario").getValue().join(','),
                        	//f_tipologia_ordine: form_values['f_tipologia_ordine'],
                        	f_tipologia_ordine: form.findField("f_tipologia_ordine").getValue().join(','),
                        	f_modello: form_values['f_modello'],
                        	f_referente_cliente:  form_values['f_referente_cliente'],
                        	f_referente_ordine:  form_values['f_referente_ordine'],
                        	//f_stato_ordine: form_values['f_stato_ordine'],
                        	f_stato_ordine: form.findField("f_stato_ordine").getValue().join(','),
                        	f_num_ordine: form_values['f_num_ordine'],                        	
                        	f_num_carico: form_values['f_num_carico'],
                        	f_num_lotto: form_values['f_num_lotto'],
                        	f_num_proforma: form_values['f_num_proforma'],
                        	f_indice_rottura: form_values['f_indice_rottura'],
							f_priorita: form_values['f_priorita'],
							f_solo_bloccati: form_values['f_solo_bloccati'],
							f_confermati: form_values['f_confermati'],
							f_hold: form_values['f_hold'],
							f_num_contract: form_values['f_num_contract'],
							f_mercato: form_values['f_mercato'],
							f_forniture_MTO: form.findField("f_forniture_MTO").getValue().join(','),
						    f_ava_paga: form.findField("f_ava_paga").getValue().join(','), 
							f_articolo: form_values['f_articolo'],
							f_prov_d: m_form.f_prov_d.join(','),
							f_loc_d: m_form.f_loc_d.join(',')
                        	};
                        	return r;                        
                        }
	        }
	          
				
        }
]}