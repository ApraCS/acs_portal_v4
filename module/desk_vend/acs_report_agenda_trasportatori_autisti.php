<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_parametri_form'){
	
	//Elenco opzioni "Area spedizione"
	$ar_area_spedizione = $s->get_options_area_spedizione();
	$ar_area_spedizione_txt = '';
	
	$ar_ar2 = array();
	foreach ($ar_area_spedizione as $a)
		$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
	$ar_area_spedizione_txt = implode(',', $ar_ar2);	
	
?>	


{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>',
			buttonAlign:'center',            
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "REP_AGE_TRAS");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	            handler: function() {
	                form = this.up('form').getForm();
	                
	                if (form.isValid()){	                	                
		                form.submit({
				                standardSubmit: true,
				                submitEmptyText: false,
		                        method: 'POST',
								target : '_blank'		                        
		                });
	                }	                
	            }
	        }],             
            
            items: [{
					xtype: 'fieldset',
	                title: 'Seleziona data',
	                defaultType: 'textfield',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	             	items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin: "5 10 5 10"
						}, {
                    xtype: 'radiogroup',
                    anchor: '100%',
                    fieldLabel: '',
                    items: [
                        {
                            xtype: 'radio'
                          , name: 'sceltadata[]' 
                          , boxLabel: 'Programmata'
                          , inputValue: 'TDDTEP'
                          , checked: true
                        },
                        {
                            xtype: 'radio'
                          , name: 'sceltadata[]' 
                          , boxLabel: 'Spedizione'
                          , inputValue: 'TDDTSP'                          
                        }
                   ]
                }
					]
	            }       
	            
	            
                , {
					xtype: 'fieldset',
	                title: 'Selezione tipologie/area',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
	             
					{
					    xtype: 'combo',
					    fieldLabel: 'Tipologia trasporto 1',
					    name: 'f_tip_trasp_1',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'),"", true, 'Y', 'Y'); ?>
							    ] 
							}
					}, 					{
					    xtype: 'combo',
					    fieldLabel: 'Tipologia trasporto 2',
					    name: 'f_tip_trasp_2',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'),"", true, 'Y', 'Y'); ?>
							    ] 
							}
					},					{
					    xtype: 'combo',
					    fieldLabel: 'Tipologia trasporto 3',
					    name: 'f_tip_trasp_3',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'),"", true, 'Y', 'Y'); ?>
							    ] 
							}
					},{
					    xtype: 'combo',
					    fieldLabel: 'Tipologia trasporto 4',
					    name: 'f_tip_trasp_4',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'),"", true, 'Y', 'Y'); ?>
							    ] 
							}
					}, {
					    xtype: 'combo',
					    fieldLabel: 'Area di spedizione',
					    name: 'area_spedizione',
					    autoSelect: false,
					    allowBlank: true,
					    editable: false,
					    triggerAction: 'all',
					    typeAhead: true,
					    width:120,
					    listWidth: 120,
					    enableKeyEvents: true,
					    mode: 'local',
					    store: [
					        <?php echo $ar_area_spedizione_txt ?>
					    ]
					}
	             
	             ]
	             }, {
					xtype: 'fieldset',
					border: true,
	                title: '',
	                defaultType: 'textfield',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	                items: [{
							flex: 1,						 
							name: 'f_solo_bloccati',
							xtype: 'checkboxgroup',
							fieldLabel: 'Solo ordini con carico',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_solo_ordini_con_carico' 
		                          , boxLabel: ''
		                          , inputValue: 'Y'
		                        }]														
						 }
					]
	            }                                       
            ]
        }

	
]}

<?php	
 exit;	
}


$giorni_festivi = $s->get_giorni_festivi(0,0);
$oggi_AS = oggi_AS_date();



$sql_where = '';
$parametri = array();

//array tipologie trasporto
$tipologie_trasp = array();

if (isset($_REQUEST['f_tip_trasp_1']))
	array_push($tipologie_trasp, $_REQUEST['f_tip_trasp_1']);

if (isset($_REQUEST['f_tip_trasp_2']))
	array_push($tipologie_trasp, $_REQUEST['f_tip_trasp_2']);

if (isset($_REQUEST['f_tip_trasp_3']))
	array_push($tipologie_trasp, $_REQUEST['f_tip_trasp_3']);
	
if (isset($_REQUEST['f_tip_trasp_4']))
	array_push($tipologie_trasp, $_REQUEST['f_tip_trasp_4']);


if ($tipologie_trasp != null)
	$sql_where .= "AND TA.TATITR IN (". sql_t_IN($tipologie_trasp) . ")";


if (strlen($_REQUEST['area_spedizione']) > 0)
	$sql_where .= "AND TA_ITIN.TAASPE = " . sql_t($_REQUEST['area_spedizione']);

//mi costruisco l'elenco delle tipologie trasporto selezionate per fare un ordinamento
   $locate_tiptra = implode("|", array(sprintf("%-10s", $_REQUEST['f_tip_trasp_1']), sprintf("%-10s", $_REQUEST['f_tip_trasp_2']), sprintf("%-10s", $_REQUEST['f_tip_trasp_3']), sprintf("%-10s", $_REQUEST['f_tip_trasp_4'])));

$sql = "SELECT TA.TATITR AS TIPTRA, TA.TAKEY3, TA_VETT.TAKEY1 AS KEYVET, TA_VETT.TADESC AS DESCVE, TA_TRAS.TAKEY1 AS KEYTRA, TA_TRAS.TADESC AS DESCTR
		FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			ON TA_VETT.TADT = TA.TADT AND TA_VETT.TAKEY1 = TA.TAKEY2 AND TA_VETT.TATAID = 'AUTR'
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
			ON TA.TADT = TA_ITIN.TADT AND TA.TAKEY1 = TA_ITIN.TAKEY1 AND TA_ITIN.TATAID = 'ITIN' 			
		WHERE 1 = 1
		AND TA.TATAID = 'ITVE'
		AND TA.TADT = '{$id_ditta_default}'
		$sql_where
		ORDER BY LOCATE(TIPTRA, '{$locate_tiptra}'), TIPTRA 
";

$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, $parametri);

$array_tip_tras = array();

//CREAZIONE ARRAY AD ALBERO (Trasp - Vettore - Mezzo)
while ($r = db2_fetch_assoc($stmt)) {

	$liv0_v = trim($r['TIPTRA']);	
	$liv1_v = trim($r['KEYTRA']);
	$liv2_v = trim($r['KEYVET']);
	$liv3_v = trim($r['TAKEY3']);

	$tmp_ar_id = array();
	$n_children = "children";

	// LIVELLO 0
	$s_ar = &$array_tip_tras;
	$liv_c     = $liv0_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_1");
		$s_ar[$liv_c]["task"] = $s->decod_std('TTRA', $liv_c);
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c][$n_children];

	// LIVELLO 1
	$liv_c     = $liv1_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_2");
		$s_ar[$liv_c]["task"] = $r['DESCTR'];
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c][$n_children];
	
	
	// LIVELLO 2
	$liv_c     = $liv2_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_3");
		$s_ar[$liv_c]["task"] = $r['DESCVE'];
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c][$n_children];

	// LIVELLO 3
	$liv_c     = $liv3_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_4");
		$s_ar[$liv_c]["task"] = $s->decod_std('AUTO', $liv_c);
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c];
}


$sql_where = '';
$data_richiesta = '';
if(isset($_REQUEST['sceltadata'][0]))
	$data_sped_o_prog = $_REQUEST['sceltadata'][0];
else
	$data_sped_o_prog = "TDDTEP";


if ($_REQUEST['f_solo_ordini_con_carico'] == 'Y')
	$sql_where .= " AND TD.TDFN01 = 1 ";

if (strlen(($_REQUEST['f_data'])) > 0){
	$sql_where .= "AND  " . $data_sped_o_prog . " >= ?
				   AND " . $data_sped_o_prog . " <= " . date('Ymd', strtotime($_REQUEST['f_data'] . " +15 days"));
	$parametri[] = $_REQUEST['f_data'];
	$data_richiesta = $_REQUEST['f_data'];
}else{
	$sql_where .= "AND  " . $data_sped_o_prog . " >= " . oggi_AS_date() . "
				   AND " . $data_sped_o_prog . " <= " . date('Ymd', strtotime(oggi_AS_date() . " +15 days"));
	$data_richiesta = oggi_AS_date();
}

$sql = "SELECT SP.CSCVET AS VETT, SP.CSCAUT AS MEZZ, SP.CSCITI AS ITIN, SP.CSGGTR, TD.{$data_sped_o_prog} AS DATE, SUM(TDVOLU) AS S_VOLU
		FROM {$cfg_mod_Spedizioni['file_calendario']} SP
		JOIN {$cfg_mod_Spedizioni['file_testate']} TD
		ON TD.TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TD.TDNBOC = SP.CSPROG
		 AND  " . $s->get_where_std() . " AND TDSWSP = 'Y'
	     WHERE 1 = 1
		 $sql_where
	     GROUP BY SP.CSCVET, SP.CSCITI, SP.CSGGTR, SP.CSCAUT, TD.{$data_sped_o_prog}, SP.CSTITR
	     ORDER BY VETT, MEZZ, DATE
";

$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, $parametri);

$array_date = array();

//CREAZIONE ARRAY AD ALBERO (Vettore - Mezzo - Itinerario - Data)
while ($r = db2_fetch_assoc($stmt)) {

	$liv0_v = trim($r['VETT']);
	$liv1_v = trim($r['MEZZ']);
	$liv2_v = trim($r['DATE']);
	$liv3_v = trim($r['ITIN']);
	
	$tmp_ar_id = array();
	$n_children = "children";
	
	// LIVELLO 0
	$s_ar = &$array_date;
	$liv_c     = $liv0_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_1");
		$s_ar[$liv_c]["task"] = $s->decod_std('AUTR', $liv_c);
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c][$n_children];
	
	// LIVELLO 1
	$liv_c     = $liv1_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_2");
		$s_ar[$liv_c]["task"] = $s->decod_std('AUTO', $liv_c);
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c][$n_children];
	
	// LIVELLO 2
	$liv_c     = $liv2_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_3");
		$s_ar[$liv_c]["task"] = $r['DATE'];
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c][$n_children];
	
	// LIVELLO 3
	$liv_c     = $liv3_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_4");
		$s_ar[$liv_c]["task"] = $s->decod_std('ITIN', $liv_c);
		$s_ar[$liv_c]["volu"] = $r['S_VOLU'];
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
		if ($r['CSGGTR'] == 0)
			$s_ar[$liv_c]["CSGGTR"]   = 0; //0gg significa 1gg			
		else
			$s_ar[$liv_c]["CSGGTR"]   = $r['CSGGTR'] -1 ; //tolgo uno perche' considero anche il giorno di partenza		
	}
	$s_ar = &$s_ar[$liv_c];
}


?>

<html>
<head>
<style type="text/css">
        
        table{border-collapse:collapse; width:100%; border: 2px solid #000000; font-size: 14px;}
        td{padding: 0 3px 0 3px; border: 2px solid #000000;}
        th{background: #D3D3D3; border: 2px solid #000000;} 
        tr.titoli th{text-align: center; white-space:nowrap;}
        .numeri{text-align: right;}
        .title{font-size: 36;}
        .errori{color: #FF4500;}
/*        .no_itin{background: white;} */
        .con_itin{background-color: #26547C; padding: 2px; color: white;}
        .passato.con_itin{background-color: #26547C; color: white;}
        .tipologia_trasp{font-weight: bold; font-size: 20;}
        .date{background: #000000; color: #FFFFFF; white-space: nowrap; padding: 0 5px 0 5px; border:solid #000000;}
        
        td.festivo_S{background-color: #9BBBBF;}
        td.festivo_D{background-color: #9BAABF;}
        td.festivo_L{}        
        
        td.tipologia_trasp{padding: 20px 5px 5px 5px;}
        
        /*
        .festivo_S{opacity: 0.2}
        .festivo_D{opacity: 0.4}
        */
        
        .passato{background-color: #e0e0e0;}
        th.passato{color: gray; opacity: 1}        
                
        
        @media print 
		{
	    	.noPrint{display:none;}
		}    
		
</style>

<title>
Agenda impegno trasportatori/autisti
</title>

</head>

<body>

<div class = "page-utility noPrint" >
	<A HREF="javascript:window.print()"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>	
	<A HREF="#" onclick="window.open('data:application/vnd.ms-excel,' + '<html><head>' + escape(document.getElementsByTagName('style')[0].outerHTML) + '</head>' + '<body>' + escape(document.getElementById('my_content').outerHTML) + '</body></html>');"><img src=<?php echo img_path("icone/48x48/save.png") ?>></A>
</div>

<div id ='my_content'>

<b class = "title"> Agenda impegno trasportatori/autisti </b>

<br>
<br>

<?php



//creazione tabella trasportatore-vettore-mezzo-data(itin)
echo " <table> ";
echo " <tr> <th> Tipologia Trasporto / Ditta </th> <th> Vettore </th><th> Mezzo </th>";
echo crea_date($data_richiesta) . "</tr>";

//ordino le tipologie in base alla sequenza di quelle filtrate
//TODO

foreach ($array_tip_tras as $ar_tip_trasp){
	echo "<tr> <td colspan= 18 class =\"tipologia_trasp\"> [" . $ar_tip_trasp['cod']  . "] " .scrivi_tip_trasp_null($ar_tip_trasp['task']) . " </td></tr> ";
	foreach ($ar_tip_trasp[$n_children] as $ar_trasp) {
		echo "<tr>";
		echo "<td  align = \"center\" rowspan = " . get_row_children($ar_trasp[$n_children]) . "> " . scrivi_trasp_null($ar_trasp['task']) . "</td>";
		$conta_vettore = 0;
		
		foreach ($ar_trasp[$n_children] as $ar_vett){
					
			if($conta_vettore != 0) echo "<tr>";
			echo "<td rowspan = " . count($ar_vett[$n_children]) . ">". $ar_vett['task'] . "</td>";	
			$conta_vettore++;
			$conta_mezzo = 0;
			
			foreach($ar_vett[$n_children] as $ar_mezzo){
	
				if($conta_mezzo != 0) echo "<tr>";
				echo "<td>" . $ar_mezzo['task'] . "</td>";
				
				for ($i = 0; $i < 15; $i++){
					$data = date('Ymd', strtotime("$data_richiesta +$i days"));

					if ($data < $oggi_AS)
						$passato = "passato";
					else $passato = '';
						
					
					$festivo = $giorni_festivi[$data];					
					$ret_itinerario = get_itinerari($ar_vett['cod'], $ar_mezzo['cod'], $data);					
					if ($ret_itinerario['string'] == '') echo "<td valign=top class = \"no_itin festivo_{$festivo} {$passato}\">{$ret_itinerario['ico_string']}</td>";
					else echo "<td valign=top class=\"con_itin festivo_{$festivo} {$passato}\"> " . trim($ret_itinerario['ico_string'] . " " . $ret_itinerario['string']) . "</td>";					
					
				}
				echo " </tr>";
				$conta_mezzo++;
			}
		}
	}	
}
echo "</table>";

//FUNZIONI

//funzione per creare le date
function crea_date($data_richiesta){
	global $giorni_festivi, $oggi_AS;
//	echo "<pre>"; print_r($giorni_festivi); echo "</pre>";
	$oldLocale = setlocale(LC_TIME, 'it_IT');
	$ret = '';
	for($i = 0; $i<15; $i++){
		$data = strtotime("$data_richiesta +$i days");
		$data_AS = to_AS_date($data);
		$festivo = $giorni_festivi[$data_AS];
		
		if ($data_AS < $oggi_AS)
			$passato = "passato";
		else $passato = '';
		
		$ret .= "<th class = \"date festivo_{$festivo} {$passato}\"> " . ucfirst(strftime("%a<BR>%d/%m", $data)) . "</th>";
	}
	return $ret;
}

//funzione per contare il numero di nodi foglia (per il rowspan)
function get_row_children($array){
$num_righe = 0;
	foreach ($array as $vett){
		$num_righe += count($vett['children']);
	}
	return $num_righe;
}

//funzione per trovare un trasportatore non definito
function scrivi_trasp_null($trasportatore){
$ret = '';
	if ($trasportatore == '')
		$ret = "Trasportatore n/d";
	else
		$ret = $trasportatore;
	return $ret;
}


//funzione per trovare una tipologia non definita
function scrivi_tip_trasp_null($tip_trasp){
	$ret = '';
	if ($tip_trasp == '')
		$ret = "Tipologia n/d";
	else
		$ret = $tip_trasp;
	return $ret;
}


//funzione per controllare la rispondenza vettore_mezzo
function get_itinerari($vettore, $mezzo, $data){
	$ret = array();
	$string 	= '';
	$ico_string = '';	
	global $array_date, $main_module;
	foreach ($array_date as $vet){
		if($vettore == $vet['cod']){
			foreach ($vet['children'] as $mez){
				if($mezzo == $mez['cod']){
				 	foreach ($mez['children'] as $dat){

						//verifico se e' in un giorno di trasporto (andata/ritorno da consegna)
						if($data != $dat['cod']){
							foreach ($dat['children'] as $iti){
								//se la data + il gg trasporto comprendono la data attuale mostro l'icona

								$data_inizio_consegna   = $main_module->aggiungi_gg_lavorativi(date('Ymd', strtotime($dat['cod'] . " +0 days")), 1, '', true);
								//$data_inizio_consegna 	= date('Ymd', strtotime($dat['cod'] . " +0 days")); //carico
								
								//la consegna inizia dal primo giorno lavorativo (no sab o dom) successivo alla data di carico 
								$data_termine_consegna	= date('Ymd', strtotime($data_inizio_consegna . " +{$iti['CSGGTR']} days"));
								if ($data >= $data_inizio_consegna && $data <= $data_termine_consegna)
									$ico_string = "<img src=" . img_path("icone/48x48/delivery.png") . " height=20>"; 
							}
						}						
						
						//verifico se e' nel giorno di consegna (carico) e riporto gli itinerari
						if($data == $dat['cod']){
							foreach ($dat['children'] as $iti){
								$string .= "" . $iti['task'] . " [" . n($iti['volu'], 1) . "]\n";
							}
						}
						
					}
				}
			}
		}
	}
	return array("string" => $string, "ico_string" => $ico_string);
}



?>

</div>
</body>
</html>