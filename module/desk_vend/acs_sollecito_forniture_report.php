<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$ar_email_to = array();
$ar_email_json = acs_je($ar_email_to);

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
		{ xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
			items: [

    	       	    {  name: 'f_datacons_da'
    			     , flex: 1                		
    				 , xtype: 'datefield'
    				 , startDay: 1 //lun.
    				 , fieldLabel: 'Data consegna iniziale'
    				 , labelAlign: 'left'
    				 , format: 'd/m/Y'
    				 , submitFormat: 'Ymd'
    				// , allowBlank: false
    				 , anchor: '-15'
    				 , labelWidth: 130
    				 , listeners: {
    				 	invalid: function (field, msg) {
    				 	  Ext.Msg.alert('', msg);}
    			       }
    				}
			 , {
				     name: 'f_datacons_a'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data consegna finale'
				   , labelAlign: 'left'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				  // , allowBlank: false
				   , anchor: '-15'
				   , labelWidth: 130
				   , listeners: {
				       invalid: function (field, msg) {
				         Ext.Msg.alert('', msg);}
			 }
			}
	            ],
	            
				buttons: [	
					<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "ORD_FOR_APERTI");  ?>
					{ text: 'Report',
				      iconCls: 'icon-print-32',		            
				      scale: 'large',		            
			          handler: function() {
			          	form = this.up('form').getForm();
			          	if (form.isValid()){
    			          	this.up('form').submit({
                            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
                            target: '_blank', 
                            standardSubmit: true,
                            method: 'POST',
                            params: {
     	                        	  form_values: Ext.encode(this.up('form').getValues()),
    		                          filter : '<?php echo json_encode($m_params->filter); ?>'
    		                        }
    		                  });
    		                this.up('window').close();
		                } 
		              }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	
	
if ($_REQUEST['fn'] == 'open_report'){ 

?>


<html>
 <head>

  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white;}
   
   div#my_content h1{font-size: 22px; padding: 5px;}   
   div#my_content h2{font-size: 18px; padding: 5px; margin-top: 20px;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>

 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'N';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'> 

<?php

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$form_ep = strtr($_REQUEST['filter'], array('\"' => '"'));
$form_ep = json_decode($form_ep);


$sql_where.= " WHERE " . $s->get_where_std();

$sql_where.= sql_where_by_combo_value('TDCDIV', $form_ep->f_divisione);

$sql_where.= sql_where_by_combo_value('TDASPE', $form_ep->f_areaspe);

$sql_where.= sql_where_by_combo_value('TDCLOR', $form_ep->f_tipologia_ordine);

$sql_where.= sql_where_by_combo_value('TDSTAT', $form_ep->f_stato_ordine);

$sql_where.= sql_where_by_combo_value('RDRIFE', $form_ep->f_stato_fornitore);

$sql_where.= sql_where_by_combo_value('RDFORN', $form_ep->f_fornitore);

//controllo HOLD
$filtro_hold=$form_ep->f_hold;

if($filtro_hold== "Y")
   $sql_where.=" AND TDSWSP = 'Y'";
if($filtro_hold== "N")
   $sql_where.=" AND TDSWSP = 'N'";

if (isset($form_ep->f_indice_rottura)  && strlen($form_ep->f_indice_rottura) > 0 )
   $sql_where .= " AND TDIRLO LIKE '%{$form_ep->f_indice_rottura}%'";
if ($form_ep->f_indice_rottura_assegnato == "Y")
   $sql_where .= " AND TDIRLO <> '' ";
if ($form_ep->f_indice_rottura_assegnato == "N")
   $sql_where .= " AND TDIRLO = '' ";

if(isset($form_values->f_datacons_da)  &&strlen($form_values->f_datacons_da) > 0)
    $sql_where .= " AND RDDTEP >= {$form_values->f_datacons_da} ";
if(isset($form_values->f_datacons_a)  &&strlen($form_values->f_datacons_a) > 0)
   $sql_where .= " AND RDDTEP <= {$form_values->f_datacons_a} ";


	$sql = "SELECT RDFORN, RDDFOR, RDTPNO, TDDTEP, TDDTSP, TDCCON, TDDCON, TDOADO, TDONDO, TDOTPD, TDSTAT, TDVSRF,
				   RDART, RDDES1, RDUM, RDDTDS, RDODER, RDDTEP, RDRIFE, RDDES2, SUM(RDQTA) AS RDQTA
			FROM {$cfg_mod_Spedizioni['file_righe']} 
			 INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
			  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI				 
		 {$sql_where} AND RDTPNO = 'MTO' 
		GROUP BY RDFORN, RDDFOR, RDTPNO, TDDTEP, TDDTSP, TDCCON, TDDCON, TDOADO, TDONDO, TDOTPD, TDSTAT, TDVSRF, 
				   RDART, RDDES1, RDUM, RDDTDS, RDODER, RDDTEP, RDRIFE, RDDES2
		ORDER BY RDDFOR, RDTPNO, RDRIFE, TDDTEP, TDONDO 
		";
	
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();		
$result = db2_execute($stmt);

$ar = array();
$ar_tot = array();
$d_ar= array();



/***********************************************************************************************
 *  ELENCO 
 ***********************************************************************************************/
 
if (1 == 1) {

 echo "<h1>Riepilogo segnalazioni su articoli mancanti/critici</h1>";

 $f_liv1 = "RDFORN"; 			$d_liv1 = "RDDFOR";
 
 $ar_tot["TOTALI"] = array();
 
 while ($r = db2_fetch_assoc($stmt)) {
			
	 	
		//liv1
		if (!isset($d_ar[$r[$f_liv1]]))	
				$d_ar[$r[$f_liv1]] = array("cod" => $r[$f_liv1], "descr"=>$r[$d_liv1], 
																  "val" => array(), 
																  "children"=>array()); 														  
																  																  
		 $tmp_ar = &$d_ar[$r[$f_liv1]];

		 //Per sapere su quante date, articoli.... ho problemi con un certo fornitore
		 $tmp_ar["val"]['TDDTEP'][$r['TDDTEP']] +=1; 
		 $tmp_ar["val"]['TDDTSP'][$r['TDDTSP']] +=1;
		 $tmp_ar["val"]['TDCCON'][$r['TDCCON']] +=1;					
		 $tmp_ar["val"]['TDONDO'][$r['TDONDO']] +=1;		 
		 $tmp_ar["val"]['RDART'][$r['RDART']] +=1;		 
		 $tmp_ar["val"]['RDDTDS'][$r['RDDTDS']] +=1;		 
		 $tmp_ar["val"]['RDODER'][$r['RDODER']] +=1;		 
		 $tmp_ar["val"]['RDDTEP'][$r['RDDTEP']] +=1;		 
		 
		 //AGGIUNGO IL RECORDI IN LINEA
		 $tmp_ar['children'][] = $r;		 
	
 } //while



foreach ($d_ar as $kf => $f){
	echo "<h2>Fornitore " . $f['descr'] . "</h2>";
	
	$liv_2_inline = '-----xxxxxx------';
	
	echo "<table>";
	
		echo "<th>Data<br>Prog.<BR> (# " . count($f['val']['TDDTEP']) . ")</th>";
		echo "<th>Data<br>Sped.<BR> (# " . count($f['val']['TDDTSP']) . ")</th>";		
		echo "<th>Cliente<BR> (# " . count($f['val']['TDCCON']) . ")</th>";
		echo "<th>Denominazione</th>";		
		echo "<th>Ordine<br>Cliente<BR> (# " . count($f['val']['TDONDO']) . ")</th>";
		echo "<th>Articolo<BR> (# " . count($f['val']['RDART']) . ")</th>";
		echo "<th>Descrizione<BR>&nbsp;</th>";		
		echo "<th>Stato</th>";		
		echo "<th>UM</th>";
		echo "<th>Quantita&grave;</th>";
		echo "<th>Disponib.<BR> (# " . count($f['val']['RDDTDS']) . ")</th>";
		echo "<th>Consegna<BR> (# " . count($f['val']['RDDTEP']) . ")</th>";	
		echo "<th>Documento</th>";		
	
	
	foreach ($f['children'] as $r){
	
		if ($liv_2_inline != implode("_", array($r['RDTPNO'], $r['RDRIFE']))){
			echo "<tr class=\"ag_liv2\">";
				echo "<td colspan=13>" . implode(" - ", array($r['RDTPNO'], $r['RDRIFE'])) . "</td>";
			echo "</tr>";
			$liv_2_inline = implode("_", array($r['RDTPNO'], $r['RDRIFE']));
		}
	
		echo "<tr>";
			echo "<td>" . print_date($r['TDDTEP']) . "</td>";
			echo "<td>" . print_date($r['TDDTSP']) . "</td>";
			echo "<td>" . trim($r['TDCCON']) .  "</td>";			
			echo "<td>" . trim($r['TDDCON']) .  "</td>";			
			echo "<td nowrap>" . implode('_' , array(trim($r['TDOADO']), trim($r['TDONDO']), trim($r['TDOTPD']))) . " (" . trim($r['TDSTAT']) . ")<br>".trim($r['TDVSRF'])."</td>";
			echo "<td>" . trim($r['RDART']) .  "</td>";
			echo "<td>" . trim($r['RDDES1']) .  "</td>";			
			echo "<td>" . trim($r['RDRIFE']) . "</td>";
			echo "<td>" . trim($r['RDUM']) . "</td>";
			echo "<td>" . trim($r['RDQTA']) . "</td>";		
			echo "<td>" . print_date($r['RDDTDS']) . "</td>";
			echo "<td>" . print_date($r['RDDTEP']) . "</td>";
			echo "<td>" . trim($r['RDDES2']) . "</td>";			
		echo "</tr>";
	}
	echo "</table>";
} 

 
//echo "<pre>"; print_r($d_ar); echo "</pre>";
 
 
} //if elenco

 
 		
?>
 </div>
 </body>
</html>	

<?php 

exit;
}