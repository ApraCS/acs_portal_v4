<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			cls: 'acs_toolbar',
			tbar: [{
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Base',
			            items: [
			     
			     

// ***************************************************************************************
// Vettori
// ***************************************************************************************
			            {
			                text: 'Vettori<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-user_group-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new Vettori(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "vettori") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "vettori") ?>
								<?php echo $cl->out_Writer_Model("vettori") ?>
								<?php echo $cl->out_Writer_Store("vettori") ?>
								<?php echo $cl->out_Writer_sotto_main("vettori") ?>
								<?php echo $cl->out_Writer_main("Lista vettori", "vettori") ?>								
								<?php echo $cl->out_Writer_window("Tabella vettori", "SETUP_VETTORI") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Auto  / Mezzo
// ***************************************************************************************
			            {
			                text: 'Mezzi',
			                scale: 'large',			                 
			                iconCls: 'icon-delivery-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new Auto(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "auto") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "auto") ?>
								<?php echo $cl->out_Writer_Model("auto") ?>
								<?php echo $cl->out_Writer_Store("auto") ?>
								<?php echo $cl->out_Writer_sotto_main("auto") ?>
								<?php echo $cl->out_Writer_main("Lista mezzi", "auto") ?>								
								<?php echo $cl->out_Writer_window("Tabella mezzi", "SETUP_MEZZI") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Casse
// ***************************************************************************************
			            {
			                text: 'Casse',
			                scale: 'large',			                 
			                iconCls: 'icon-box_open-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new Casse(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "casse") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "casse") ?>
								<?php echo $cl->out_Writer_Model("casse") ?>
								<?php echo $cl->out_Writer_Store("casse") ?>
								<?php echo $cl->out_Writer_sotto_main("casse") ?>
								<?php echo $cl->out_Writer_main("Lista casse", "casse") ?>								
								<?php echo $cl->out_Writer_window("Tabella casse", "SETUP_CASSE") ?>							

								} //handler function()
			            },
// ***************************************************************************************
// Aree di spedizione
// ***************************************************************************************
			            {
			                text: 'Aree<br>spedizione',
			                scale: 'large',			                 
			                iconCls: 'icon-globe-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new AreeSpedizione(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "areespedizione") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "areespedizione") ?>
								<?php echo $cl->out_Writer_Model("areespedizione") ?>
								<?php echo $cl->out_Writer_Store("areespedizione") ?>
								<?php echo $cl->out_Writer_sotto_main("areespedizione") ?>
								<?php echo $cl->out_Writer_main("Lista aree spedizione", "areespedizione") ?>								
								<?php echo $cl->out_Writer_window("Tabella aree spedizione", "SETUP_AREE_SPEDIZIONI") ?>							

								} //handler function()
			            },			             
// ***************************************************************************************
// Itinerari
// ***************************************************************************************
			            {
			                text: 'Itinerari',
			                scale: 'large',			                 
			                iconCls: 'icon-compass-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new Itinerari(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "itinerari") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "itinerari") ?>
								<?php echo $cl->out_Writer_Model("itinerari") ?>
								<?php echo $cl->out_Writer_Store("itinerari") ?>
								<?php echo $cl->out_Writer_sotto_main("itinerari") ?>
								<?php echo $cl->out_Writer_main("Lista itinerari", "itinerari") ?>								
								<?php echo $cl->out_Writer_window("Tabella itinerari", "SETUP_ITINERARI") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Trasportatori
// ***************************************************************************************
			            {
			                text: 'Trasportatori',
			                scale: 'large',			                 
			                iconCls: 'icon-address_book-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new Trasportatori(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "trasportatori") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "trasportatori") ?>
								<?php echo $cl->out_Writer_Model("trasportatori") ?>
								<?php echo $cl->out_Writer_Store("trasportatori") ?>
								<?php echo $cl->out_Writer_sotto_main("trasportatori") ?>
								<?php echo $cl->out_Writer_main("Lista trasportatori", "trasportatori") ?>								
								<?php echo $cl->out_Writer_window("Tabella trasportatori", "SETUP_TRASPORTATORI") ?>							

								} //handler function()
			            }
			            
			            			            
			    	]
			   }, {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Spedizione',
			            items: [
			      	        
// ***************************************************************************************
// Itinerari/vettore
// ***************************************************************************************
			            {
			                text: 'Itinerari<br>vettori',
			                scale: 'large',			                 
			                iconCls: 'icon-delivery_globe-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new ItVe(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "itve") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "itve") ?>
								<?php echo $cl->out_Writer_Model("itve") ?>
								<?php echo $cl->out_Writer_Store("itve") ?>
								<?php echo $cl->out_Writer_sotto_main("itve", 0.4) ?>
								<?php echo $cl->out_Writer_main("Lista Itinerari/Vettori", "itve") ?>								
								<?php echo $cl->out_Writer_window("Tabella Itinerari/Vettori", "SETUP_ITIN_VETTORI") ?>							

								} //handler function()
			            },			             
// ***************************************************************************************
// Stati carico
// ***************************************************************************************
			            {
			                text: 'Stati<br>carico',
			                scale: 'large',			                 
			                iconCls: 'icon-tag-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new StatiCarico(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "staticarico") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "staticarico") ?>
								<?php echo $cl->out_Writer_Model("staticarico") ?>
								<?php echo $cl->out_Writer_Store("staticarico") ?>
								<?php echo $cl->out_Writer_sotto_main("staticarico") ?>
								<?php echo $cl->out_Writer_main("Lista Stati carico", "staticarico") ?>								
								<?php echo $cl->out_Writer_window("Tabella stati carico", "SETUP_STATI_CARICO") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Tipologie spedizione
// ***************************************************************************************
			            {
			                text: 'Tipologie<br>spedizione',
			                scale: 'large',			                 
			                iconCls: 'icon-battery_half-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new TipologieSpedizione(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "tipologiespedizione") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "tipologiespedizione") ?>
								<?php echo $cl->out_Writer_Model("tipologiespedizione") ?>
								<?php echo $cl->out_Writer_Store("tipologiespedizione") ?>
								<?php echo $cl->out_Writer_sotto_main("tipologiespedizione") ?>
								<?php echo $cl->out_Writer_main("Lista tipologie spedizione", "tipologiespedizione") ?>								
								<?php echo $cl->out_Writer_window("Tabella tipologie spedizione", "SETUP_TIP_SPED") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Tipologie trasporto
// ***************************************************************************************
			            {
			                text: 'Tipologie<br>trasporto',
			                scale: 'large',			                 
			                iconCls: 'icon-shopping_cart-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new TipologieTrasporto(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "tipologietrasporto") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "tipologietrasporto") ?>
								<?php echo $cl->out_Writer_Model("tipologietrasporto") ?>
								<?php echo $cl->out_Writer_Store("tipologietrasporto") ?>
								<?php echo $cl->out_Writer_sotto_main("tipologietrasporto") ?>
								<?php echo $cl->out_Writer_main("Lista tipologie trasporto", "tipologietrasporto") ?>								
								<?php echo $cl->out_Writer_window("Tabella tipologie trasporto", "SETUP_TIP_TRASP") ?>							

								} //handler function()
			            },

// ***************************************************************************************
// Capacita produttiva/spedizione
// ***************************************************************************************
			            {
			                text: 'Capacit&agrave;<br>Prod./Sped.',
			                scale: 'large',			                 
			                iconCls: 'icon-blackboard-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new SpedCapacitaProduttiva(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "capacitaproduttiva") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "capacitaproduttiva") ?>
								<?php echo $cl->out_Writer_Model("capacitaproduttiva") ?>
								<?php echo $cl->out_Writer_Store("capacitaproduttiva") ?>
								<?php echo $cl->out_Writer_sotto_main("capacitaproduttiva") ?>
								<?php echo $cl->out_Writer_main("Lista capacit&agrave; produttiva/spedizione", "capacitaproduttiva") ?>								
								<?php echo $cl->out_Writer_window("Tabella capacit&agrave; produttiva/spedizione", "SETUP_CAP_PROD") ?>							

								} //handler function()
			            },
			            
			            
			            
// ***************************************************************************************
// Indici di rottura
// ***************************************************************************************
			            {
			                text: 'Indici rottura',
			                scale: 'large',			                 
			                iconCls: 'icon-sticker_black-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new SpedIndiciRottura(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "indicirottura") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "indicirottura") ?>
								<?php echo $cl->out_Writer_Model("indicirottura") ?>
								<?php echo $cl->out_Writer_Store("indicirottura") ?>
								<?php echo $cl->out_Writer_sotto_main("indicirottura") ?>
								<?php echo $cl->out_Writer_main("Lista indici rottura", "indicirottura") ?>								
								<?php echo $cl->out_Writer_window("Tabella indici rottura", "SETUP_INDICIROTTURA") ?>							

								} //handler function()
			            },
			            
			            
			            
// ***************************************************************************************
// Forzatura data ATP (TATAID = FDATP)
// ***************************************************************************************
			            {
			                text: 'Forzatura<br>data ATP',
			                scale: 'large',			                 
			                iconCls: 'icon-blog_add-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new SpedForzaturaAtp(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "forzaturaatp") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "forzaturaatp") ?>
								<?php echo $cl->out_Writer_Model("forzaturaatp") ?>
								<?php echo $cl->out_Writer_Store("forzaturaatp") ?>
								<?php echo $cl->out_Writer_sotto_main("forzaturaatp") ?>
								<?php echo $cl->out_Writer_main("Lista forzatura date ATP", "forzaturaatp") ?>								
								<?php echo $cl->out_Writer_window("Tabella forzatura date ATP") ?>							

								} //handler function()
			            },	 			             			            			             
// ***************************************************************************************
// Autorizzazioni
// ***************************************************************************************
			            {
			                text: 'Autorizzazioni<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-unlock-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new Autorizzazioni(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "autorizzazioni") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "autorizzazioni") ?>
								<?php echo $cl->out_Writer_Model("autorizzazioni") ?>
								<?php echo $cl->out_Writer_Store("autorizzazioni") ?>
								<?php echo $cl->out_Writer_sotto_main("autorizzazioni") ?>
								<?php echo $cl->out_Writer_main("Lista autorizzazioni", "autorizzazioni") ?>								
								<?php echo $cl->out_Writer_window("Tabella autorizzazioni", "SETUP_AUTORIZZAZIONI") ?>							

								} //handler function()
			            } 
			    	]
			   }, {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Percorso',
			            items: [			            
			            
// ***************************************************************************************
// Start Point
// ***************************************************************************************
			            {
			                text: 'Stabilimenti<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-home-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new SpedStabilimenti(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "tpstartpoint") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "tpstartpoint") ?>
								<?php echo $cl->out_Writer_Model("tpstartpoint") ?>
								<?php echo $cl->out_Writer_Store("tpstartpoint") ?>
								<?php echo $cl->out_Writer_sotto_main("tpstartpoint") ?>
								<?php echo $cl->out_Writer_main("Sede inizio trasporto", "tpstartpoint") ?>								
								<?php echo $cl->out_Writer_window("Tabella Sede inizio trasporto", "SETUP_STABILIMENTI") ?>							

								} //handler function()
			            }

			            ]                        
			         },{
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Avanzate',
			            items: [{
			                xtype:'splitbutton',
			                iconCls: 'icon-unlock-32',
			                text: 'Avanzate',
			                scale: 'large',
			                iconAlign: 'top',
			                arrowAlign:'bottom',
			                menu: {
			                	xtype: 'menu',
		        				items: [	
		        				  	
						{
							xtype: 'button',
				            text: 'Gestione cambio stati',
				            //iconCls: 'icon-print-32', 
				            scale: 'small',
				            handler: function() {
				            	  acs_show_panel_std('acs_gestione_stato_ordini.php?fn=open_grid', 'panel_gestione_stati', "");	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        },{
							xtype: 'button',
				            text: 'Gestione cambio stati carico',
				            //iconCls: 'icon-print-32', 
				            scale: 'small',
				            handler: function() {
				            	  acs_show_panel_std('acs_gest_STCAR.php?fn=open_tab', null, {/*sezione: ''*/});	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        },{
							xtype: 'button',
				            text: 'Gestione tabelle TA0',
				            //iconCls: 'icon-print-32', 
				            scale: 'small',
				            handler: function() {
				            	  acs_show_panel_std('acs_gestione_tabelle.php?fn=open_grid', 'panel_gestione_tabelle', "");	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        },{
							xtype: 'button',
				            text: 'Gestione sequenza stati ordini',
				            //iconCls: 'icon-print-32', 
				            scale: 'small',
				            handler: function() {
				            	  	acs_show_win_std('Sequenza stati ordine', 
										'acs_g_seq_stati_ordine.php?fn=open_tab', {}, 950, 500, null, 'icon-shopping_cart_gray-16');
	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        },
				        {
							xtype: 'button',
				            text: 'Gestione tabella STONA <br>(stati ordine non ammessi)',
				            //iconCls: 'icon-print-32', 
				            scale: 'small',
				            handler: function() {
				            	  	acs_show_win_std('Sequenza stati ordine', 
										'acs_gest_STONA.php?fn=open_tab', {}, 950, 500, null, 'icon-shopping_cart_gray-16');
	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        },
				            {
							xtype: 'button',
				            text: 'Gestione tabella STCNA <br> (stati carichi a cui non accodare ordini)',
				            //iconCls: 'icon-print-32', 
				            scale: 'small',
				            handler: function() {
				            	  	acs_show_win_std('Sequenza stati ordine', 
										'acs_gest_STCNA.php?fn=open_tab', {}, 950, 500, null, 'icon-shopping_cart_gray-16');
	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        }
		        				 ]
			                
			                }
			            }]
			        }
			]
		}
	]		
}            