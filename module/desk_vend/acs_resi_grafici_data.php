<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();
$p_open_request = $m_params->open_request->form_values;

function get_where_sql($p_open_request){
	$m_where = "1 = 1";
	$m_where .= sql_where_by_combo_value("TDASPE", $p_open_request->area_spedizione);
	$m_where .= sql_where_by_combo_value("TDCCON", $p_open_request->f_cliente_cod);
	$m_where .= sql_where_by_combo_value("TDCITI", $p_open_request->itinerario_spedizione);
	$m_where .= sql_where_by_combo_value("TDCAG1", $p_open_request->agente);
	return $m_where;
}

function get_where_sql_resi($p_open_request){
	$m_where = "TDOTID = 'VO' AND TDOINU = '' AND TDOADO = 0 AND TDSWPP = 'R'";
	$m_where .= sql_where_by_combo_value("TDASPE", $p_open_request->area_spedizione);
	$m_where .= sql_where_by_combo_value("TDCCON", $p_open_request->f_cliente_cod);
	$m_where .= sql_where_by_combo_value("TDCITI", $p_open_request->itinerario_spedizione);
	$m_where .= sql_where_by_combo_value("TDCAG1", $p_open_request->agente);
	return $m_where;
}

function get_sql_for_causa_nc($p_open_request, $file_testate, $file_righe){

	$field = "COUNT(*) AS NR_RIGHE, SUM(RDQTA2) AS IMPORTO,  SUBSTRING(RDDES2,16,25) AS CAUSA_NC";
	$field_group_by = "SUBSTRING(RDDES2,16,25)";
	$field_order_by = "IMPORTO DESC";
	
	$m_where = get_where_sql_resi($p_open_request);
	
	$sql = "SELECT $field
	FROM $file_testate TD
		INNER JOIN $file_righe RD  
			ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
	WHERE  $m_where
	GROUP BY $field_group_by
	ORDER BY $field_order_by";
	
	return $sql;
}

// ******************************************************************************************
// DATI PER TABELLA INCIDENZA RESI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_incidenza'){
	
	$m_where = get_where_sql($p_open_request);
	$m_where_resi = get_where_sql_resi($p_open_request);
	
	// TOT N. CLIENTI
	$sql = "SELECT COUNT(DISTINCT TDCCON) AS N_CLIENTI
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
		WHERE $m_where";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$r = db2_fetch_assoc($stmt);
	$n_clienti = $r['N_CLIENTI'];
	
	
	//TOT  N CLIENTI CON RESI 
	$sql = "SELECT COUNT(DISTINCT TDCCON) AS N_CLIENTI_CON_RESI
		FROM {$cfg_mod_Spedizioni['file_testate']} TD INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD  
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO 
		WHERE $m_where_resi";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$r = db2_fetch_assoc($stmt);
	$n_clienti_con_resi = $r['N_CLIENTI_CON_RESI'];
	
	
	// NR DESTINAZIONI CLIENTI
	$sql = "SELECT COUNT(*) AS N_CLI_DES
		FROM (SELECT TDCCON, TDCDES
			FROM {$cfg_mod_Spedizioni['file_testate']} 
			WHERE $m_where
			GROUP BY TDCCON, TDCDES) TD";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$r = db2_fetch_assoc($stmt);
	$n_cli_des = $r['N_CLI_DES'];
	
	
	// NR DESTINAZIONI CLIENTI CON RESI 
	$sql = "SELECT COUNT(*) AS N_CLI_DES_RESI
		FROM (	SELECT TDCCON, TDCDES
			FROM {$cfg_mod_Spedizioni['file_testate']} INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD  
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO 
			WHERE $m_where_resi
			GROUP BY TDCCON, TDCDES) TD";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$r = db2_fetch_assoc($stmt);
	$n_cli_des_resi = $r['N_CLI_DES_RESI'];
	
	
	// IMPORTO ORDINI NON EVASI E NON RESI 
	$sql = "SELECT SUM(TDINFI) AS IMPORTO
		FROM {$cfg_mod_Spedizioni['file_testate']}
		WHERE TDOINU <> '' AND TDOADO <> 0 AND TDSWPP <> 'R' AND TDFN11 <>1 AND $m_where";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$r = db2_fetch_assoc($stmt);
	$importo = $r['IMPORTO'];
	
	// IMPORTO ORDINI DI RESO 
	$sql = "  SELECT SUM(RDQTA2) AS IMPORTO_RESI
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
			INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD  
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO 
		WHERE $m_where_resi ";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$r = db2_fetch_assoc($stmt);
	$importo_resi = $r['IMPORTO_RESI'];
	
	//creazione json
	$data = array(
			array('task'=> 'Nr',
					'cliente' => $n_clienti,
					'cliente_con_resi' => $n_clienti_con_resi,
					'perc' => $n_clienti_con_resi/$n_clienti
			),
			array('task'=> 'Nr Destinazioni',
					'cliente' => $n_cli_des,
					'cliente_con_resi' => $n_cli_des_resi,
					'perc' => $n_cli_des_resi/$n_cli_des
			),
			array('task'=> 'Importo',
					'cliente' => n($importo),
					'cliente_con_resi' => n($importo_resi),
					'perc' => isset($importo) ? $importo_resi/$importo : 0
			)
	);
	
	echo  acs_je($data);
	
	exit;
}
	
	
// ******************************************************************************************
// DATI PER TABELLA ANZIANITA' RESI
// ****************************************************************************************** 
if ($_REQUEST['fn'] == 'get_json_data_anzianita'){
	
	$field = "SUBSTRING(RDODER,1,4) AS ANNO_RESO, SUBSTRING(RDODER,5,2) AS MESE_RESO,
				COUNT(*) AS NR_RIGHE, SUM(RDQTA2) AS IMPORTO";
	$field_group_by = "SUBSTRING(RDODER,1,4), SUBSTRING(RDODER,5,2) ";
	$field_order_by = "SUBSTRING(RDODER,1,4), SUBSTRING(RDODER,5,2) ";
	
	$m_where = get_where_sql_resi($p_open_request);
	
	$sql = "SELECT $field
		FROM {$cfg_mod_Spedizioni['file_testate']} TD
			INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD  ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO
				AND TDONDO=RDONDO
		WHERE  $m_where
		GROUP BY $field_group_by
		ORDER BY $field_order_by";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$data = array();
	
	for ($i=1; $i<=6; $i++){
		$d=strtotime("-$i Months");
		define("m$i", implode("_", array(date('Y',$d), date('m',$d))));
	}
	
	$d=strtotime("-1 years");
	$d=mktime(0, 0, 1, date('m',$d), 1, date('Y', $d));
	$one_years_ago =  date("Y-m-d", $d);
	
	$d=strtotime("-2 years");
	$d=mktime(0, 0, 1, date('m',$d), 1, date('Y', $d));
	$two_years_ago =  date("Y-m-d", $d);
	
	$today = strtotime("Today");
	$mese_anno_corrente = implode("_", array(date('Y',$today), date('m',$today)));
	$totale_resi = 0;
	
	while ($r = db2_fetch_assoc($stmt)){
		$mese_anno_reso = implode("_", array(trim($r['ANNO_RESO']), trim($r['MESE_RESO'])));
		$totale_resi += $r['IMPORTO'];
	
		switch ($mese_anno_reso){
			case "$mese_anno_corrente":
				if (!isset($data[0]['m-1'])){
					$data[0]['m-1'] = 0;
					$data[1]['m-1'] = 0;
				}
				$data[0]['m-1'] += $r['IMPORTO'];
				$data[1]['m-1'] += $r['NR_RIGHE'];
				break;
			case m1:
				if (!isset($data[0]['m-1'])){
					$data[0]['m-1'] = 0;
					$data[1]['m-1'] = 0;
				}
				$data[0]['m-1'] += $r['IMPORTO'];
				$data[1]['m-1'] += $r['NR_RIGHE'];
				break;
			case m2:
				$data[0]['m-2'] = $r['IMPORTO'];
				$data[1]['m-2'] = $r['NR_RIGHE'];
				break;
			case m3:
				$data[0]['m-3'] = $r['IMPORTO'];
				$data[1]['m-3'] = $r['NR_RIGHE'];
				break;
			case m4:
				$data[0]['m-4'] = $r['IMPORTO'];
				$data[1]['m-4'] = $r['NR_RIGHE'];
				break;
			case m5:
				$data[0]['m-5'] = $r['IMPORTO'];
				$data[1]['m-5'] = $r['NR_RIGHE'];
				break;
			case m6:
				$data[0]['m-6'] = $r['IMPORTO'];
				$data[1]['m-6'] = $r['NR_RIGHE'];
				break;
			default:
				$d=mktime(0, 0, 1, trim($r['MESE_RESO']), 1, trim($r['ANNO_RESO']));
				$data_reso=  date("Y-m-d", $d);
				// RESI A UN ANNO
				if ($data_reso >= $one_years_ago){
					if (!isset($data[0]['a_anno'])){
						$data[0]['a_anno'] = 0;
						$data[1]['a_anno'] = 0;
					}
					$data[0]['a_anno'] += $r['IMPORTO'];
					$data[1]['a_anno'] += $r['NR_RIGHE'];
				} else if($data_reso < $one_years_ago and $data_reso >= $two_years_ago){  // RESI A DUE ANNI
					if (!isset($data[0]['a_due_anni'])){
						$data[0]['a_due_anni'] = 0;
						$data[1]['a_due_anni'] = 0;
					}
					$data[0]['a_due_anni'] += $r['IMPORTO'];
					$data[1]['a_due_anni'] += $r['NR_RIGHE'];
				} else {  //OLTRE
					if (!isset($data[0]['oltre'])){
						$data[0]['oltre'] = 0;
						$data[1]['oltre'] = 0;
					}
					$data[0]['oltre'] += $r['IMPORTO'];
					$data[1]['oltre'] += $r['NR_RIGHE'];
				}
		}
	}
	
	//Percentuali
	$data[2]['m-1'] = calcola_percentuale($totale_resi,$data[0]['m-1']);  
	$data[2]['m-2'] = calcola_percentuale($totale_resi,$data[0]['m-2']);   
	$data[2]['m-3'] = calcola_percentuale($totale_resi,$data[0]['m-3']);  
	$data[2]['m-4'] = calcola_percentuale($totale_resi,$data[0]['m-4']);  
	$data[2]['m-5'] = calcola_percentuale($totale_resi,$data[0]['m-5']);  
	$data[2]['m-6'] = calcola_percentuale($totale_resi,$data[0]['m-6']);  
	$data[2]['a_anno'] = calcola_percentuale($totale_resi,$data[0]['a_anno']);  
	$data[2]['a_due_anni'] = calcola_percentuale($totale_resi,$data[0]['a_due_anni']);  
	$data[2]['oltre'] = calcola_percentuale($totale_resi,$data[0]['oltre']);  
	
	//task
	$data[0]['task'] = 'Importo';
	$data[1]['task'] = 'Nr. righe';
	$data[2]['task'] = '%';
	
	//render della riga
	$data[0]['row_format'] = 'floatRenderer2';
	$data[1]['row_format'] = 'floatRenderer0';
	$data[2]['row_format'] = 'perc2';
	
	echo  acs_je($data);
	exit;
}

// ******************************************************************************************
// DATI PER TABELLA CAUSE NC
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_cause_nc'){	
	
	
	$sql = get_sql_for_causa_nc($p_open_request, $cfg_mod_Spedizioni['file_testate'], $cfg_mod_Spedizioni['file_righe']);
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	
	$data = array();
	$tot_resi = 0;
	$tot_righe = 0;
	
	while ($r = db2_fetch_assoc($stmt)){
		$tot_resi +=  $r['IMPORTO'];
		$tot_righe += $r['NR_RIGHE'];
		
		array_push($data, array(
				'cause_nc'=> $r['CAUSA_NC'],
				'importo' => $r['IMPORTO'],
				'nr_righe' => $r['NR_RIGHE'],
				'perc_importo' => '',
				'perc_nr_righe' => ''
 		));
	}
	
	
	//percentuali
	for ($i=0; $i<count($data); $i++){
		$data[$i]['perc_importo'] = calcola_percentuale($tot_resi, $data[$i]['importo']);
		$data[$i]['perc_nr_righe'] = calcola_percentuale($tot_righe, $data[$i]['nr_righe']);
	}
	
	echo  acs_je($data);
	exit;
}


// ******************************************************************************************
// DATI PER GRAFICO CAUSE NC
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_chart'){

	$campo_tot =  $_REQUEST['rb_campo_tot'];
	
	$sql = get_sql_for_causa_nc($p_open_request, $cfg_mod_Spedizioni['file_testate'], $cfg_mod_Spedizioni['file_righe']);
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);

	$ar_ret = array();

	while ($r = db2_fetch_assoc($stmt)){
		array_push($ar_ret, array(
				'CAUSA_NC'=> $r['CAUSA_NC'],
				'VALORE' => $campo_tot == 'nr_righe' ?  $r['NR_RIGHE'] : $r['IMPORTO']
		));
	}

	echo acs_je($ar_ret);
	exit;
}


?>


<?php 

	function calcola_percentuale($totale, $valore){
		return $valore/$totale;
	}

?>



