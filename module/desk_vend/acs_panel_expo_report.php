<?php
require_once "../../config.inc.php";
require_once("acs_panel_todolist_data.php");

ini_set('max_execution_time', 6000);
ini_set('memory_limit', '2048M');

$main_module = new Spedizioni();
$s = new Spedizioni();
$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$where = sql_where_params(array(), $form_values);

$sql = "SELECT AO.*, TD.*, TA_ITIN.TADESC AS D_ITIN, NT_MEMO.NTMEMO AS MEMO
        FROM {$cfg_mod_Spedizioni['file_assegna_ord']} AO
        INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
        ON TD.TDDOCU = AO.ASDOCU
        " . $main_module->add_riservatezza() . "
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
           ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ATTAV
           ON TD.TDDT = TA_ATTAV.TADT AND TA_ATTAV.TATAID = 'ATTAV' AND TA_ATTAV.TAKEY1 = AO.ASCAAS
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
		   ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = AO.ASIDPR AND NT_MEMO.NTSEQU=0
       WHERE (" . $main_module->get_where_std(null, array('filtra_su' => 'AS')) . ") 
       AND ASCAAS = '{$form_values->tipo_op}' {$where}
       ORDER BY TDDCON, TDDLOC
";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
   

$ar = array();
while ($row = db2_fetch_assoc($stmt)) {

    
    //stacco dei livelli
    //$cod_liv0 = trim($row['TDCITI']); //itinerario
    $cod_liv0 = 'ITINERARIO'; //ToDo: rendere opzionale
    if($form_values->tipo_op == 'EXPO2')
        $cod_liv1 = trim($row['ASUSAT']); //utente
    else
        $cod_liv1 = 'UTENTE';
    $cod_liv2 = implode('|', array(trim($row['TDCCON']), trim($row['TDCDES']))); //cliente|destinazione
    $cod_liv3 = trim($row['ASDOCU']); //ordini
    
    
    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    //itinerario
    $liv =$cod_liv0;
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] = 'TOTALE'; // $row['D_ITIN']; 
        $ar_new['liv'] = 'liv_0';
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    sum_columns_value($ar_r, $row);
    
    //utente
    $liv =$cod_liv1;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] = $row['ASUSAT'];
        $ar_new["riferimento"]  = $main_module->scrivi_rif_destinazione($row['TDLOCA'], $row['TDNAZI'], $row['TDPROV'], $row['TDDNAZ']);
        $ar_new['liv'] = 'liv_1';
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    sum_columns_value($ar_r, $row);
    
    //cliente|destinazione
    $liv =$cod_liv2;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] = $row['TDDCON'];
        $ar_new["riferimento"]  = $main_module->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD']);
        $ar_new['liv'] = 'liv_1';
        if (strlen(trim($row['MEMO'])) > 0)
            $ar_new['memo'] = "{$row['MEMO']}";
           
        //Aggiunto le note di apertura entry
        if (strlen(trim($row['ASNOTE'])) > 0){
            $row['ASNOTE'] = trim($row['ASNOTE']);
            $ar_new["note"] 	= "{$row['ASNOTE']}";
        }
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    sum_columns_value($ar_r, $row);
    
    //ordine
    $liv=$cod_liv3;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new = $row;
        
        //mostre TDCLOR = 'M'
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['asdocu'] =  $row['ASDOCU'];
        $ar_new['task'] =  $row['TDOADO']."_".$row['TDONDO'];
        if (strlen(trim($row['MEMO'])) > 0)
            $ar_new['memo'] = "{$row['MEMO']}";
        
        if($row['TDFN06'] != 0)
            $ar_new['p_red'] = "Y";
        
            
        $ar_new['localita'] 	=  trim($row['TDVSRF']);
        
        //Aggiunto le note di apertura entry
        if (strlen(trim($row['ASNOTE'])) > 0){
            $row['ASNOTE'] = trim($row['ASNOTE']);
            $ar_new["note"] 	= "{$row['ASNOTE']}";
        }

        global $giorni_stadio;
        $ar_new['n_stadio'] = $giorni_stadio[$row['TDDTEP']];
        $ar_new['cliente']     = $row['TDDCON'];
        $ar_new['prod_disp']     = $row['TDDTEP'];
        $ar_new["data_reg"]	     = $row['TDODRE'];
        $ar_new["cons_rich"]     = $row['TDODER'];
        $ar_new["priorita"]		 = $row['TDOPRI'];
        $ar_new["qtip_priorita"] = acs_u8e($row['TDDPRI']);
        $ar_new["stato"]		 = $row['TDSTAT'];
        $ar_new["qtip_stato"]	 = acs_u8e($row['TDDSST']);
        $ar_new["tipo"] 		 = $row['TDOTPD'];
        $ar_new["qtip_tipo"] 	 = acs_u8e($row['TDDOTD']);
        $ar_new['liv'] = 'liv_3';
        $ar_new['leaf'] = true;
        $ar_r["{$liv}"] = $ar_new;
    }
    
    $ar_r=&$ar_r[$liv];
    sum_columns_value($ar_r, $row);
    
}



echo "<div id='my_content'>";
echo "<div class=header_page>";
if($form_values->tipo_op == 'EXPO1')
    echo "<H2>Verifica installazione mostre</H2>";
else 
    echo "<H2>Riepilogo programma installazione mostre</H2>";


echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";
echo "<tr class='liv_data'>";


echo "
    <th>Itinerario/Utente assegnato/Ordine</th>
    <th><img src=" . img_path("icone/48x48/info_blue.png") . " height=20></th>
    <th><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=20></th>
	<th>Localit&agrave;/Riferimento</th>";
	
if($form_values->tipo_op == 'EXPO1')
    echo "<th>Riferimento</th>";
else
    echo "<th>Installatore</th>";
echo "  <th>Memo</th>
    	<th>Tp</th>
    	<th>Data</th>
        <th>St</th>
        <th>Pr</th>
        <th>Produz./Disponib</th>
        <th>Importo</th>";
    if($form_values->tipo_op == 'EXPO1')
        echo "<th>Scadenza</th>";
    else
        echo "<th>Inizio lavori</th>";

echo "</tr>";  


foreach ($ar as $kar => $r){
    
    echo "<tr class = 'liv3'>
          <td>".$r['task']."</td>";
          get_img_flag($r);
          echo "<td>&nbsp;</td><td colspan = 8>&nbsp;</td>";
          echo "<td align=right>".n($r['importo'], 2)."</td>";
          echo "<td>&nbsp;</td>";
	      echo "</tr>";
	   
	//utente      
    foreach ($r['children'] as $kar1 => $r1){
     
        if($form_values->tipo_op == 'EXPO2')
            echo "<tr  class = 'liv2'><td colspan=13>".$r1['task']."</td></tr>";
        
	        //cliente|destinazione
	        foreach ($r1['children'] as $karCD => $rCD){
	        
	            echo "<tr  class = 'liv1'>
                    <td>".$rCD['task']."</td>
                        <td colspna=4>&nbsp;</td>
                        ";
 
    	            echo "<td>&nbsp;</td><td>".$rCD['riferimento']."</td>";
    	            if($form_values->tipo_op == 'EXPO1')
    	                echo "<td><b>Riferimento</b></td>";
	                else
	                    echo "<td><b>Installatore</b></td>";
    	            echo "<td><b>Memo</b></td>
        	            <td><b>Tp</b></td>
        	            <td><b>Data</b></td>
        	            <td><b>St</b></td>
        	            <td><b>Pr</b></td>
        	            <td><b>Produz./Disponib</b></td>
        	            <td><b>Importo</b></td>";
    	           if($form_values->tipo_op == 'EXPO1')
                        echo "<td><b>Scadenza</b></td>";
                    else
                        echo "<td></b>Inizio lavori</b></td>";
    	            echo "</tr>";
	        
                    	    //ordini
                	         foreach ($rCD['children'] as $kar2 => $r2){
                                echo "<tr>
                                      <td>".$r2['task']."</td>";
                                      get_img_flag($r2);
                                 if($r2['p_red'] == 'Y')
                                    echo "<td><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=15></td>";
                                else 
                                    echo "<td>&nbsp;</td>";
                                echo " <td valign = top>".$r2['localita']."</td>
                                       <td valign = top>".$r2['note']."</td>
                                       <td valign = top>".$r2['memo']."</td>
                                    ";
                                echo "<td valign = top>".$r2['tipo']."</td>";
                                echo "<td valign = top>".print_date($r2['data_reg'])."</td>";
                                echo "<td valign = top>".$r2['stato']."</td>";
                                echo "<td valign = top>".$r2['priorita']."</td>";
                                echo "<td valign = top>".print_date($r2['prod_disp'])."</td>";
                                echo "<td valign = top align=right>".n($r2['importo'], 2)."</td>";
                                echo "<td valign = top>".print_date($r2['scadenza'])."</td>";
                    	        echo "</tr>";
                                
                            }
	        }               
    }
            
}

function sum_columns_value(&$ar_r, $r){
    
    global $s;
    
    $ar_r['importo'] += $r['TDTIMP'];
    
    $ar_r['fl_art_manc'] = max($ar_r['fl_art_manc'], $s->get_fl_art_manc($r));
    $ar_r['art_da_prog'] = max($ar_r['art_da_prog'], $s->get_art_da_prog($r));
    $ar_r['fl_bloc'] = $s->get_fl_bloc($ar_r['fl_bloc'], $s->calc_fl_bloc($r));
    $ar_r['fl_new'] = $s->get_fl_new($r);
    $ar_r['fl_da_prog'] += $r['TDFN04'];
    
    $ar_r['scadenza'] = max($ar_r['scadenza'] ,$r['ASDTSC']);
    if(trim($r['TDCLOR']) == 'M') $ar_r['n_M']++;
   
}



function get_img_flag($r){
   
/*    
    if($r['fl_bloc'] == 4){
        echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
    }else if($r['fl_bloc'] == 3){
        echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
    }else if($r['fl_bloc'] == 2){
        echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
    }elseif($r['fl_bloc'] == 1){
        echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
*/    
    
    if($r['fl_art_manc'] == 5){
        echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
    }else if($r['fl_art_manc'] == 20){
        echo "<td><img src=" . img_path("icone/48x48/shopping_cart_red.png") . " height=15></td>";
    }else if($r['fl_art_manc'] == 4){
        echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
    }else if($r['fl_art_manc'] == 3){
        echo "<td><img src=" . img_path("icone/48x48/shopping_cart.png") . " height=15></td>";
    }elseif($r['fl_art_manc'] == 2){
        echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
    }elseif($r['fl_art_manc'] == 1){
        echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
    
/*    
    if($r['art_da_prog'] == 3){
        echo "<td><img src=" . img_path("icone/48x48/warning_blue.png") . " height=15></td>";
    }elseif($r['art_da_prog'] == 2){
        echo "<td><img src=" . img_path("icone/48x48/warning_red.png") . " height=15></td>";
    }elseif($r['art_da_prog'] == 1){
        echo "<td><img src=" . img_path("icone/48x48/info_black.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
    
    if($r['fl_new'] == 3){
        echo "<td><img src=" . img_path("icone/48x48/timer_blue.png") . " height=15></td>";
    }elseif($r['fl_new'] == 2){
        echo "<td><img src=" . img_path("icone/48x48/mano_ko.png") . " height=15></td>";
    }elseif($r['fl_new'] == 1){
        echo "<td><img src=" . img_path("icone/48x48/label_blue_new.png") . " height=15></td>";
    }elseif($r['fl_new'] == 4){
        echo "<td><img src=" . img_path("icone/48x48/design.png") . " height=15></td>";
    }elseif($r['fl_new'] == 10){
        echo "<td><img src=" . img_path("icone/48x48/bandiera_scacchi.png") . " height=15></td>";
    }elseif($r['fl_new'] == 11){
        echo "<td><img src=" . img_path("icone/48x48/bandiera_grey.png") . " height=15></td>";
    }elseif($r['fl_new'] == 12){
        echo "<td><img src=" . img_path("icone/48x48/bandiera_red.png") . " height=15></td>";
    }elseif($r['fl_new'] == 'Y'){
        echo "<td><img src=" . img_path("icone/48x48/label_blue_new.png") . " height=15></td>";
    }elseif($r['fl_new'] == 'O'){
        echo "<td><img src=" . img_path("icone/48x48/label_new_grey.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
*/    
    
    
    
}

?>
</div>
</body>
</html>	

