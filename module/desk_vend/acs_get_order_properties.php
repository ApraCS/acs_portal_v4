<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

    $m_params = acs_m_params_json_decode();
    
    if(strlen($_REQUEST["m_id"]) >0)
    	$k_ordine = $_REQUEST["m_id"];
    else
    	$k_ordine = $m_params->k_ordine;
    

	$sottostringhe = explode("|", $k_ordine);
	$n_ord = $sottostringhe[6];


	$r = $s->get_ordine_by_k_ordine($k_ordine);
	 
	if ($r['TDDTEPI'] != $r['TDDTEPF'] && $r['TDDTEPF'] != 0)
		$seconda_data_confermata = " (Ult.  " . print_date($r['TDDTEPF']) . ")";
	else
		$seconda_data_confermata = '';
	
		
		
		//compongo le mie uniche due righe
		$nr = array();
		$nr['Tipo'] = $r['TDDOTD'];
		$nr['Divisione']= $r['TDDDIV'];
		$nr['Stato']= $r['TDDSST'];
		$nr['Varianti']=j($r['TDDVN1']);
		
		if (strlen(trim($r['TDDVN2'])) > 0)
			$nr[" ''' "]= j($r['TDDVN2']);
		
		if (strlen(trim($r['TDDVN3'])) > 0)
			$nr[" '' "]= j($r['TDDVN3']);
		
		
		$nr['Stato cliente']= $r['TDDSRI'];
		$nr['Denominazione']= j($r['TDDCON']);
		$nr['E-mail']= j($r['TDMAIL']);
		$nr['Telefono']= j($r['TDTEL']);
		$nr['Destinazione']= j($r['TDINDI']);
		
		if (trim($r['TDNAZI']) == 'ITA' || trim($r['TDNAZI']) == 'IT')
			$nr['Localit&agrave;']= j(implode(', ', array_map('trim', array($r['TDLOCA'], $r['TDCAP'], "( {$r['TDPROV']} )"))));
		else
			$nr['Localit&agrave;']=j(implode(', ', array_map('trim', array($r['TDLOCA'], $r['TDDNAZ']))));
						 
		
		$nr['Agente']= $r['TDDAG1'];
		$nr['Referente cliente']= acs_u8e($r['TDDOCL']);
		$nr['Referente ordine']=  acs_u8e($r['TDDORE']);
		
		if ($r['TDDTCF'] > 0)
			$nr['Conferma ordine']= print_date($r['TDDTCF']) . ' ' . print_ora($r['TDHMCF']) . ' [E.P. ' . print_date($r['TDDTEPI']) . ']' ;
		elseif ($r['TDDTAR'] > 0)
			$nr['Fine attesa conf.']=  print_date($r['TDDTAR']) . ' ' . print_ora($r['TDHMAR']);
		
		$nr['Cod. Cliente/Destin.']= $r['TDCCON'] . ' - ' . $r['TDCDES'];
		
		//Parametrico: classificazioni cliente		
		if (isset($cfg_mod_Spedizioni["visualizza_campo_selezione_cliente"])){		
    		$row_gc_cli = $s->get_anagrafica_from_gc('CLI', $r['TDCCON']);
    		if ($row_gc_cli){
    		    
    		    if ($cfg_mod_Spedizioni["visualizza_campo_selezione_cliente"]['1'] == true){
    		      $row_decod_sel1 = get_TA_sys('CUS1', $row_gc_cli['GCSEL1']);
    		      if ($row_decod_sel1) $nr['Class. CLI 1'] = $row_gc_cli['GCSEL1'] . " - " . $row_decod_sel1['text'];
    		    }
    		    
    		    if ($cfg_mod_Spedizioni["visualizza_campo_selezione_cliente"]['2'] == true){
    		      $row_decod_sel2 = get_TA_sys('CUS2', $row_gc_cli['GCSEL2']);
    		      if ($row_decod_sel2) $nr['Class. CLI 2'] = $row_gc_cli['GCSEL2'] . " - " . $row_decod_sel2['text'];
    		    }
    		    
    		    if ($cfg_mod_Spedizioni["visualizza_campo_selezione_cliente"]['3'] == true){
    		      $row_decod_sel3 = get_TA_sys('CUS3', $row_gc_cli['GCSEL3']);
    		      if ($row_decod_sel3) $nr['Class. CLI 3'] = $row_gc_cli['GCSEL3'] . " - " . $row_decod_sel3['text'];
    		    }
    		    
    		    if ($cfg_mod_Spedizioni["visualizza_campo_selezione_cliente"]['4'] == true){
    		      $row_decod_sel4 = get_TA_sys('CUS4', $row_gc_cli['GCSEL4']);
    		      if ($row_decod_sel4) $nr['Class. CLI 4'] = $row_gc_cli['GCSEL4'] . " - " . $row_decod_sel4['text'];
    		    }
    		}
		}
		
		
		$nr['----------']= "---------------------------";
		
		if($m_params->show != 'Y'){
		
		$nr['Rif.Scarico/Ev.Max'] = $r['TDRFCA'] . '(Sped. # ' . $r['TDNBOC'] . ')';
		if ($r['TDAUX6'] > 0)
		    $nr['Rif.Scarico/Ev.Max'] .= ' - ' . print_date($r['TDAUX6']);
		
		//vettore - mezzo
		if ($r['TDNBOC'] > 0){
			$spedizione = $s->get_spedizione($r['TDNBOC']);
			$nr['Vettore/mezzo']= j($s->decod_std('AUTR', $spedizione['CSCVET']) . " " .
			 $s->decod_std('AUTO', $spedizione['CSCAUT']) . " " . $s->decod_std('COSC', $spedizione['CSCCON']));
			}
		
		if (strlen(trim($r['TDABBI'])) > 0)
			$nr[ 'Ordine abbinato'] =$r['TDABBI'];
		
		$nr[]=array('nome'=> 'Disponib. alla sped.', 'valore'=> print_date($r['TDDTDS']));
		$nr[]=array('nome'=> 'Data spedizione', 'valore'=> print_date($r['TDDTSP']));
		
		if ($r['TDTDES'] == "1" || $r['TDTDES'] == "2"){
			$nr[]=array('nome'=> 'Scarico', 'valore'=> j($r['TDDDES'] . " ( {$r['TDCDES']} )"));
				    	 
		   if (trim($r['TDNAZD']) == 'ITA')
			  $ar_dest = array($r['TDDCAP'], $r['TDDLOC'], "( {$r['TDPROD']} )" );
	       else
			  $ar_dest = array($r['TDDLOC'], $r['TDDNAD']);
		
		$nr['Indirizzo scar.'] =j(implode(',', array_map('trim', array($r['TDIDES']))));
		$nr['Localita scar.'] = j(implode(', ', array_map('trim', $ar_dest)));
		
				 }
		
				    //sosta tecnica
		if ($r['TDTDES'] == "T"){
			$nr['Scarico'] =j($r['TDDDES'] . " ( {$r['TDCDEL']} )");
		
		   if (trim($r['TDNAZD']) == 'ITA' || trim($r['TDNAZD']) == 'IT')
			  $ar_dest = array($r['TDDCAP'], $r['TDDLOC'], "( {$r['TDPROD']} )" );
		   else
			   $ar_dest = array($r['TDDLOC'], $r['TDDNAD']);
				    				
			$nr['Indirizzo scar.'] = j(implode(',', array_map('trim', array($r['TDIDES']))));
		    $nr['Localita scar.'] = j(implode(', ', array_map('trim', $ar_dest)));
		
				    }
		
		$nr[ 'Carico'] = implode("_", array($r['TDAACA'], $r['TDTPCA'], $r['TDNRCA'])) . ' Seq. ' . $r['TDSECA'];
		$nr['Data rilascio'] = print_date($r['TDDTRP']);
		$nr[ 'Lotto']= $r['TDAALO'] . '_' . $r['TDNRLO'] . '_' . $r['TDTPLO'] . ' Seq.' . $r['TDSELO'];
		
		if (strlen(trim($r['TDIRLO'])) > 0){
				    	//todo: decodificare Indice rottura (da tabella carichi) se e' un indice personalizzato
			$nr['Indice di produzione'] = j(trim($s->decod_std('INRO', $r['TDIRLO'])));
		   }
		
		
		$nr['Spunta disponib.'] = print_date($r['TDDTSPI']) . ' / ' . print_ora($r['TDHMSPI']) . ' - ' . print_date($r['TDDTSPF']) . ' / ' . print_ora($r['TDHMSPF']);
		$nr['Spunta spedizione']= print_date($r['TDDTSSI']) . ' / ' . print_ora($r['TDHMSSI']) . ' - ' . print_date($r['TDDTSSF']) . ' / ' . print_ora($r['TDHMSSF']);
		
		}
		
		$nr['Proforma']= $r['TDPROF'];
		
		
		if (strlen(trim($r['TDASOR'])) > 0){
				    	//todo: decodificare Indice rottura (da tabella carichi) se e' un indice personalizzato
			$nr['Ordine origine'] =  $r['TDASOR'];
			 }
		
		if (trim($r['TDDAOR']) > 0){
				    	//todo: decodificare Indice rottura (da tabella carichi) se e' un indice personalizzato
			$nr['Data ordine origine']= print_date($r['TDDAOR']);
			 }
		
	    if (strlen(trim($r['TDASFR'])) > 0){
				    	//todo: decodificare Indice rottura (da tabella carichi) se e' un indice personalizzato
		   $nr['Fattura ordine origine']=  $r['TDASFR'];
			 }
		
	    if (trim($r['TDDAFR']) > 0){
				    	//todo: decodificare Indice rottura (da tabella carichi) se e' un indice personalizzato
			$nr['Data fattura ordine origine'] = print_date($r['TDDAFR']);
			}
		
		 $nr['Mercato'] =  j(trim($s->decod_std('MERCA', $r['TDCAVE'])) . ' ' . trim($r['TDCOST']));
		 $nr['Doc trasporto'] = $r['TDAADE'] . '_' . $r['TDNRDE'] . '_' . $r['TDTPDE'] . ' del ' . print_date($r['TDDTRE']);
		 $nr['Immesso il'] = print_date($r['TDDTEV']) . ' , ' . print_ora($r['TDHMEV']) . ' , ' . $r['TDUSEV'];
		 $nr['Trasportatore 1']= acs_u8e($s->decod_std('AVET', $r['TDVET1']));
		 $nr['Trasportatore 2']=  acs_u8e($s->decod_std('AVET', $r['TDVET2']));
		 
		 if (trim($r['TDAUX3']) != '')
		     $nr['Destin. Vettore']=  trim($r['TDAUX3']);
		 
		 if(trim($r['TDAUX1']) != '')
		     $nr['Pagamento']=  acs_u8e("[". trim($r['TDCPAG']) .", " .trim($r['TDAUX1'])."] " . trim($r['TDDPAG']));
		 else
		     $nr['Pagamento']=  acs_u8e("[". trim($r['TDCPAG']) ."] " . trim($r['TDDPAG']));
		    
		 
		 $nr['Codice lingua'] = trim($r['TDRFLO']);
		 $nr['Codice listino']= trim($r['TDLIST']);
	
		 $ret['title'] = "Dettaglio ordine ".$r['TDOTPD'] . '_' . $r['TDOADO'] . '_' . $r['TDONDO'] . trim($r['TDMODI']) . " " . trim($r['TDISON']);
		 $ret['riferimenti'] = $nr;
		
		 $dt = array();
		 $dt['Ordine']=  $r['TDOTPD'] . '_' . $r['TDOADO'] . '_' . $r['TDONDO'] . trim($r['TDMODI']) . " " . trim($r['TDISON']);
		 $dt['Protocollo'] = print_date($r['TDDTIM']) . ' ' . $r['TDHMIM'] . ' ' . $r['TDUSIM'];
		 $dt['Assegnato'] = print_date($r['TDDTAS']) . ' ' . print_ora($r['TDHMAS']) . ' ' . $r['TDUSAS'];
		 $dt['Order entry'] = print_date($r['TDDTIO']) . ' ' . print_ora($r['TDHMIO']) . ' ' . $r['TDUSIO'];
		 $dt['Ultima modifica'] = print_date($r['TDDTOE']) . ' ' . print_ora($r['TDOROE'])  . ' ' . $r['TDUSOE'];
		 $dt['Controllo'] = print_date($r['TDDTCO']) . ' ' . print_ora($r['TDHMCO'])  . ' ' . $r['TDUSCO'];
		 $dt['Confermato'] = print_date($r['TDDTCF']) . ' ' . print_ora($r['TDHMCF']);
		 $dt['Evas. confermata'] =  print_date($r['TDDTEPI']) . $seconda_data_confermata;
		 $dt['Blocco/Sblocco Amm'] = print_date($r['TDDTBLO']) . ' / ' . print_date($r['TDDTSBO']);
		 $dt['Blocco/Sblocco Com'] = print_date($r['TDDTBLC']) . ' / ' . print_date($r['TDDTSBC']);
		 
		 $ret['dettagli'] = $dt;
		 
		 echo acs_je($ret);
		 exit;

$appLog->save_db();


?>
