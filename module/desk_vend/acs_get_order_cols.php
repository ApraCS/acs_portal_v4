<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$k_ord = $_REQUEST['k_ord'];
$trad = $_REQUEST['trad'];
$data = array();

$ord = $s->get_ordine_by_k_docu($k_ord);

if (trim($ord['TDRFLO'])== '')    //non traduciamo se non e' impostata la lingua
    $trad = 'N';

$stmt = $s->get_cols_ordine_by_k_ordine($k_ord, $trad);

while ($row = db2_fetch_assoc($stmt)) {
	$row['PLDART'] = acs_u8e($row['PLDART']);
	if($row['PLRIGA'] == 0 && trim($row['PLART']) == '10')
	    $row['gr_collo'] = 'F';
	
	if($trad == 'Y'){
	  $row['ALDAR1'] = acs_u8e($row['ALDAR1']);
	  $row['TADESC'] = acs_u8e($row['TADESC']);
	}
	$data[] = $row;
}

$ar_ord = $s->get_ordine_by_k_ordine($k_ord);
$m_ord = array(
		'TDOADO' => $ar_ord['TDOADO'], 
		'TDONDO' => $ar_ord['TDONDO'],
		'TDDTDS' => print_date($ar_ord['TDDTDS'])
		
);

$ret = array();
$ret['success'] = true;
$ret['root'] = $data;
$ret['ordine'] = $m_ord;
$ret['trad'] = $trad;
$ret['lng']  = $ord['TDRFLO'];
echo acs_je($ret);
		
exit();
