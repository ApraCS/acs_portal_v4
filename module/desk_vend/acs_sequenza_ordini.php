
<?php

require_once "../../config.inc.php";
$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

$data 		= $m_params->open_request->data;
$itin_id 		= $m_params->open_request->itin_id ;



$cod_cliente = trim($m_params->open_request->rec_data->TDCCON);
$ind_dest= trim($m_params->open_request->rec_data->TDIDES);
$cod_dest= trim($m_params->open_request->rec_data->TDCDES);
$loc= trim($m_params->open_request->rec_data->TDDLOC);
$prov= trim($m_params->open_request->rec_data->TDPROD);
$cap= trim($m_params->open_request->rec_data->TDDCAP);


$ar = explode("_", $m_params->open_request->rec_data->k_sped_carico);

$ar1 =  explode("|",$ar[0]);
$sped_id  = $ar1[0];
$num_carico= $ar[2];



if ($_REQUEST['fn'] == 'get_json_data_grid'){
	$ar=array();
	
	$sql = "SELECT * FROM  {$cfg_mod_Spedizioni['file_testate']}
 						WHERE " . $s->get_where_std() . "
 						AND TDDTEP = ?
 						AND TDCITI = ?
					  	AND TDCCON = ?
					  	AND TDCDES = ?
				 		AND TDDLOC = ?
				 		AND TDPROD =?
				 		AND TDDCAP = ? 
				 		AND TDIDES =?
					    AND TDNBOC = ?
				 		AND TDNRCA = ?
 				        ORDER BY TDSCOR, TDONDO";

	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($data, utf8_decode($itin_id), utf8_decode($cod_cliente), $cod_dest, utf8_decode($loc), utf8_decode($prov), 
			$cap, utf8_decode($ind_dest), utf8_decode($sped_id), $num_carico));
 	
	
	while ($row = db2_fetch_assoc($stmt)) {
		$nr = array();
		$nr['num_ordine']	= trim($row['TDONDO']);
		$nr['stato']	= trim($row['TDSTAT']);
		$nr['tipo']	= trim($row['TDOTPD']);
		$nr['ordine_out']   = $s->k_ordine_out($row['TDDOCU']);
		$nr['riferimento']	= trim($row['TDVSRF']);
		$nr['volume']	= trim($row['TDVOLU']);
		$nr['colli']	= trim($row['TDTOCO']);
		$nr['seq_scarico_ordine']	= trim($row['TDSCOR']);
		$nr['k_ordine']	= trim($row['TDDOCU']);
		

		$ar[] = $nr;
	}

	echo acs_je($ar);
	exit;
}

if ($_REQUEST['fn'] == 'applica_sequenza_ordini'){
	
	$seq = 0;
	

	foreach ($m_params->selected_id as $v){
		
	
			$seq++;

			$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
			SET TDSCOR = " . sql_t(sprintf("%03d", $seq)) . "
			 				WHERE " . $s->get_where_std() . "
 							AND TDDOCU = ?";
	
			
			$stmt_upd = db2_prepare($conn, $sql_upd);
			$result = db2_execute($stmt_upd, array($v->k_ordine));

	}
}


?>


{"success":true, "items": [


  {
			xtype: 'grid',
			//title: 'Ordini <?php echo $data ?>', 
			loadMask: true,		
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										open_request:<?php echo acs_je(array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['ordine_out', 'tipo', 'stato', 'riferimento', 'volume', 'colli', 'seq_scarico_ordine', 'k_ordine']							
									
			}, //store
				

			      columns: [	
				{
	                header   : 'Nr ordine',
	                dataIndex: 'ordine_out',
	                flex: 1
	            },{
	                header   : 'Tp',
	                dataIndex: 'tipo',
	                width: 40
	            },{
	                header   : 'St',
	                dataIndex: 'stato',
	                width: 40
	            },{
	                header   : 'Riferimento',
	                dataIndex: 'riferimento',
	                flex: 1
	            },{
	                header   : 'Volume',
	                dataIndex: 'volume',
	                align: 'right',
	                renderer: floatRenderer2,
	                flex: 1
	            },{
	                header   : 'Volume progr.',
	                dataIndex: 'volume',
	                align: 'right',
	                renderer: function(v, a, b, c, d, store, e, f){
						if (c==0)
							store.tmp_value = 0;							
						store.tmp_value += parseFloat(v);
						return floatRenderer2(parseFloat(store.tmp_value));									
					},
	                width: 100
	            },{
	                header   : 'Colli',
	                dataIndex: 'colli',
	                align: 'right',
	                renderer: floatRenderer0N,
	                flex: 1
	            },{
	                header   : 'Sequenza scarico ordine',
	                dataIndex: 'seq_scarico_ordine',
	                align: 'right',
	                flex: 1
	            }
	         ] ,
	         
	           viewConfig: {
	        
	            plugins: {
	                ptype: 'gridviewdragdrop',
	                dragGroup: 'sped_grid_seq_ord',
	                dropGroup: 'sped_grid_seq_ord'
	            }
	            
	            
	          
	        },
	        	dockedItems: [{
	            dock: 'top',
	            xtype: 'toolbar',
	 
	            items: [
	                    {
	                    iconCls: 'icon-save',
	                    itemId: 'applica_sequenza',
	                    text: 'Applica seq.',
	                    disabled: false,
	                    scope: this,
	                    handler: function(bt){	                    	
	         
							Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=applica_sequenza_ordini',
							        method     : 'POST',
							        waitMsg    : 'Data loading',
				        			jsonData: {
				        			
				        				selected_id: Ext.pluck(bt.up('grid').getStore().data.items, 'data'),
				        				
									},							        
							        success : function(result, request){
							        		bt.up('grid').getStore().load();
							       
							        		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });							
							
			                
			            }
	                }            
	            ]
			}]

			
			
		}				
	
     ]
        
 }