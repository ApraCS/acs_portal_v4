<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();


$sped_id = $m_params->sped_id;




// ******************************************************************************************
// Esecuzione ricalcolo pesi colli su spedizione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_weight'){
	$ret = array();
	$sped = $s->get_spedizione($sped_id);
	
	$address = '192.168.4.223';
	$port = 6001;
	
	if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
		echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
	}
	
	if (socket_connect($sock,$address,$port) === false) {
		echo "socket_connect() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
	}
	
	$line = "MP\r\n"; //COMANDO DI LETTURA
	socket_write ($sock, $line, strlen($line));
	$response = trim(socket_read($sock, 4096));
	if ($response == 'OK') {
		$bilancia_ret = true;
		$data = trim(socket_read($sock, 4096));
	} else {
		$bilancia_false = true;
	}

	$ret['weight']  = (int)substr($data, 10, 8);
	
	
	//aggiorno il peso su db
	$ar_upd = array();
	if ($m_params->type == 'prima_del_carico')
		$ar_upd['CSPPCA'] = sql_f($ret['weight']);
	
	if ($m_params->type == 'dopo_del_carico') {	
		$ar_upd['CSPDCA'] = sql_f($ret['weight']);
		$ar_upd['CSPECA'] = (float)$ar_upd['CSPDCA'] - (float)$sped['CSPPCA'];
	}
		
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE CSDT='$id_ditta_default' AND CSCALE = '*SPR' AND CSPROG = ?";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
			array($sped_id)
	));
	echo db2_stmt_errormsg($stmt);
	
	//Recupero il record
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_calendario']} WHERE CSCALE = '*SPR' AND CSPROG = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($sped_id));
	echo db2_stmt_errormsg($stmt);
	$r = db2_fetch_assoc($stmt);
	$ret['record'] = $r;

	$ret['success'] = $bilancia_ret;
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// Esecuzione ricalcolo pesi colli su spedizione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_ricalcolo_pesi_colli'){
	$m_params = acs_m_params_json_decode();
	$ret = array();
	$sped = $s->get_spedizione($m_params->form_values->sped_id);	
	
	//Prima salvo i dati (se per caso fossero stati modificati manualmente)

		$ar_upd = array();
		$ar_upd['CSPPCA'] = sql_f($m_params->form_values->f_peso_prima_del_carico);
		$ar_upd['CSPDCA'] = sql_f($m_params->form_values->f_peso_dopo_del_carico);
	
		if (strlen($m_params->form_values->f_peso_del_carico) > 0)
			$ar_upd['CSPECA'] = sql_f($m_params->form_values->f_peso_del_carico);
		else
			$ar_upd['CSPECA'] = (float)$ar_upd['CSPDCA'] - (float)$ar_upd['CSPPCA'];
	
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}
		SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CSCALE = '*SPR' AND CSPROG = ?";
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($m_params->form_values->sped_id)
		));
		echo db2_stmt_errormsg($stmt);

	//Eseguo la call	
		//costruzione del parametro
		$cl_p = str_repeat(" ", 246);
		$cl_p .= sprintf("%-2s", $sped['CSDT']);
		$cl_p .= sprintf("%09s", $sped['CSPROG']);
		$cl_p .= sprintf("%-1s", "N");

		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
				
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('UR21HB', $libreria_predefinita_EXE, $cl_in, null, null);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// Cancellazione immagine
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_manual'){
	$m_params = acs_m_params_json_decode();
	$ret = array();
	
	$ar_upd = array();	
	$ar_upd['CSPPCA'] = sql_f($m_params->form_values->f_peso_prima_del_carico);
	$ar_upd['CSPDCA'] = sql_f($m_params->form_values->f_peso_dopo_del_carico);
	
	if ((int)($m_params->form_values->f_peso_del_carico) > 0)
		$ar_upd['CSPECA'] = sql_f($m_params->form_values->f_peso_del_carico);
	else
		$ar_upd['CSPECA'] = (float)$ar_upd['CSPDCA'] - (float)$ar_upd['CSPPCA'];
	
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE CSDT='$id_ditta_default' AND CSCALE = '*SPR' AND CSPROG = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
			array($m_params->form_values->sped_id)
	));
	echo db2_stmt_errormsg($stmt);

	//Recupero il record
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_calendario']} WHERE CSDT='$id_ditta_default' AND CSCALE = '*SPR' AND CSPROG = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->form_values->sped_id));
	echo db2_stmt_errormsg($stmt);
	$r = db2_fetch_assoc($stmt);
	$ret['record'] = $r;

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// Cancellazione immagine
// ******************************************************************************************
if ($_REQUEST['fn'] == 'delete_camera_image'){
	$m_params = acs_m_params_json_decode();
	unlink(getcwd() . "/{$m_params->path}");
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
 exit;
}


// ******************************************************************************************
// Acquisizione immagine da fotocamera
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_camera_image'){
	$ret = array();
	$m_params = acs_m_params_json_decode();
	$sped_id = $m_params->sped_id;
	$dir_path = "../../../sped_images/{$sped_id}";

	$camera_config = $cfg_mod_Spedizioni["acquisisci_peso_immagini"]["telecamere"][$m_params->camera_id];	
	
	//creo cartella
	if (!file_exists(getcwd() . "/{$dir_path}")) {
		mkdir(getcwd() . "/{$dir_path}", 0777, true);
	}
	
	
	$ts = date("d-m-Y-His");
	$saveto = getcwd() . "/{$dir_path}/" . $ts . ".jpg"; 
	$ch = curl_init ($camera_config['url']);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
	
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	curl_setopt($ch, CURLOPT_USERPWD, "{$camera_config['user']}:{$camera_config['psw']}");	
	
	$raw=curl_exec($ch);
	curl_close ($ch);
	$fp = fopen($saveto,'x');
	fwrite($fp, $raw);
	fclose($fp);
	
	
	
	// *************************************************************************
	// copia in cartella Adiuto tramite ftp - TODO: parametrizzare
	// *************************************************************************
	if ($camera_config['copy_to_ftp'] == 'Y') {
	    $el_carichi_str = $s->get_el_carichi_by_TDNBOF($sped_id, 'Y');
		$el_carichi_ar = explode(',', $el_carichi_str);
		if (is_array($el_carichi_ar)) {
			
			$msg_error = "";
			$ftp_conn = @ftp_connect('192.168.1.138');
			if ($ftp_conn == false) {
				$msg_error = 'Impossibile collegarsi a server ftp per memorizzazione immagine su Adiuto';
			} else {
				
				// try to login
				if (@ftp_login($ftp_conn, 'AS400',  'cucine')) {
					$ftp_connected = true;
				} else {
					$msg_error = 'Impossibile collegarsi a server ftp per memorizzazione immagine su Adiuto';
				}
				
				if ($ftp_connected == true) {
					foreach ($el_carichi_ar as $kc => $c) {
						$carico_exp = explode("_", $c);
						$anno_numero_carico = implode("", array(	//AnnoNumero
								sprintf("%04s", $carico_exp[0]),
								sprintf("%06s", $carico_exp[2])
						));
							
							
						// upload a file
						if (@ftp_put($ftp_conn, "ALLCARICO-{$anno_numero_carico}-{$ts}.jpg", $saveto,  FTP_BINARY)) {						
						} else {
							$msg_error = "Impossibile scrivere il file ALLCARICO-{$anno_numero_carico}-{$ts}.jpg su cartella caricamento Adiuto";
						}					
					
					} //foreach
				} // if $ftp_connected
				
				// close the connection
				ftp_close($ftp_conn);			
				
				
			}
			
		}
	} // copy_to_ftp == S
	
	
	$ret = array();
	$ret['success'] = true;
	$ret['msg_error'] = $msg_error;
	$ret['name'] = "{$dir_path}/" . $ts . ".jpg";
	$ret['url']  = "{$dir_path}/" . $ts . ".jpg";
	echo acs_je($ret, JSON_UNESCAPED_SLASHES);
	exit;
}

// ******************************************************************************************
// Recupero immagini spedizione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_sped_images'){

	$ret = array();
	$m_params = acs_m_params_json_decode();
	$sped_id = $m_params->sped_id;
	$dir_path = "../../../sped_images/{$sped_id}";
	//echo getcwd() . "/{$dir_path}/*.*\n";
	
	foreach (glob(getcwd() . "/{$dir_path}/*.*") as $filename) {
		$ret[] = array(
				'name' 		=> 'P',
				'shortName'	=> 'PPP',
				'url'		=> $dir_path . "/" . basename($filename)
		);		
	}
	
	echo acs_je($ret, JSON_UNESCAPED_SLASHES);
	exit;
}


$sped = $s->get_spedizione($sped_id);

?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            layout: {
			    type: 'vbox',
			    align: 'stretch',
			    pack : 'start',
			},
			
			save_manual: function(form, on_field){
				
				Ext.Ajax.request({
				   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_manual',
				   method: 'POST',
				   jsonData: {form_values: form.getValues()}, 
				   
				   success: function(response, opts) {
				   	  Ext.getBody().unmask();
					  var jsonData = Ext.decode(response.responseText);									   	  
		              form = form.getForm();     
		              form.findField('f_peso_prima_del_carico').setValue(jsonData.record.CSPPCA);     
		              form.findField('f_peso_dopo_del_carico').setValue(jsonData.record.CSPDCA);
		              form.findField('f_peso_del_carico').setValue(jsonData.record.CSPECA);
		              
		              acs_show_msg_info('Salvataggio completato');
		              
				   }, 
				   failure: function(response, opts) {
				      Ext.getBody().unmask();
				      alert('error on save_manual');
				   }
				});				

			},
			
            items: [
            		{
                		xtype: 'hidden',
                		name: 'sped_id',
                		value: '<?php echo $sped_id ?>'
                	}
                	
                	
                	
					, {
				        xtype: 'fieldcontainer',
	 			        layout: 'hbox',                	
                		items: [
                	
		                	{
						        xtype: 'fieldset', flex: 1,
						        title: '<b>PRIMA del carico</b>',
								defaultType: 'textfield',
			 			        layout: 'hbox',
			 			        items: [
			 			        	{
			 			        			height: 40, labelWidth: 50, width: 180,
			 			        			bodyPadding: '30 30 30 30', bodyStyle: 'padding: 30px',
			 			        			fieldStyle: 'font-size: 20px; height: 40px; text-align: right;',
			 			        			hideTrigger:true,
											name: 'f_peso_prima_del_carico',
											xtype: 'numberfield',
											fieldLabel: 'Peso<br/>(Kg)',
										    value: <?php echo j(trim($sped['CSPPCA'])); ?>,
										    maxLength: 100								    							
									}, {							
					                     xtype: 'button',
										 margin: "0 0 0 20",			                     
						            	 scale: 'large',
						            	 iconCls: 'icon-button_black_play-32',
					                     text: 'Acquisisci',
								         handler: function() {
											   t_win = this.up('window');
											   form = t_win.down('form');								         
											   Ext.Msg.confirm('Richiesta conferma', 'Confermi acquisizione peso da bilancia?', function(btn, text){
											   if (btn == 'yes'){																	         	
									         	Ext.getBody().mask('Retrieving data', 'loading').show();
												Ext.Ajax.request({
												   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_weight',
												   method: 'POST',
												   jsonData: {
												   	type: 'prima_del_carico',
												   	sped_id: '<?php echo $sped_id ?>' 
												   }, 
												   
												   success: function(response, opts) {
												   	  Ext.getBody().unmask();
													  
													  var jsonData = Ext.decode(response.responseText);
										              form = form.getForm();     
										              form.findField('f_peso_prima_del_carico').setValue(jsonData.record.CSPPCA);     
										              form.findField('f_peso_dopo_del_carico').setValue(jsonData.record.CSPDCA);
										              form.findField('f_peso_del_carico').setValue(jsonData.record.CSPECA);													  
													  
												   }, 
												   failure: function(response, opts) {
												      Ext.getBody().unmask();
												      alert('error in get image');
												   }
												});						         	
									         	
												}
											   });								         
								         } //handler
									}
			 			        ]
			 			     }, {
						        xtype: 'fieldset', flex: 1,
						        title: '<B>DOPO il carico</B>',
								defaultType: 'textfield',
			 			        layout: 'hbox',
			 			        items: [
			 			        	{
			 			        			height: 40, labelWidth: 50, width: 180,
			 			        			bodyPadding: '30 30 30 30', bodyStyle: 'padding: 30px',
			 			        			fieldStyle: 'font-size: 20px; height: 40px; text-align: right;',
			 			        			hideTrigger:true,
											name: 'f_peso_dopo_del_carico',
											xtype: 'numberfield',
											fieldLabel: 'Peso<br/>(Kg)',
										    value: <?php echo j(trim($sped['CSPDCA'])); ?>,
										    maxLength: 100								    							
									}, {							
					                     xtype: 'button',
										 margin: "0 0 0 20",			                     
						            	 scale: 'large',
						            	 iconCls: 'icon-button_blue_play-32',
					                     text: 'Acquisisci',
								         handler: function() {
								         
											   t_win = this.up('window');
											   form = t_win.down('form');								         
											   Ext.Msg.confirm('Richiesta conferma', 'Confermi acquisizione peso da bilancia?', function(btn, text){
											   if (btn == 'yes'){																	         	
									         	Ext.getBody().mask('Retrieving data', 'loading').show();
												Ext.Ajax.request({
												   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_weight',
												   method: 'POST',
												   jsonData: {
												   	type: 'dopo_del_carico',
												   	sped_id: '<?php echo $sped_id ?>' 
												   }, 
												   
												   success: function(response, opts) {
												   	  Ext.getBody().unmask();
													  
													  var jsonData = Ext.decode(response.responseText);
										              form = form.getForm();     
										              form.findField('f_peso_prima_del_carico').setValue(jsonData.record.CSPPCA);     
										              form.findField('f_peso_dopo_del_carico').setValue(jsonData.record.CSPDCA);
										              form.findField('f_peso_del_carico').setValue(jsonData.record.CSPECA);													  
													  
												   }, 
												   failure: function(response, opts) {
												      Ext.getBody().unmask();
												      alert('error in get image');
												   }
												});						         	
									         	
												}
											   });								         
								         
								         
								         } //handler
									}
			 			        ]
			 			     }
			 			 ]
			 		 }  
			 			     
	 			     
					, {
				        xtype: 'fieldcontainer',
	 			        layout: 'hbox',                	
                		items: [	 			     	 			     
			 			     {
						        xtype: 'fieldset', flex: 5,
						        title: '<B>PESO DEL CARICO</B>',
								defaultType: 'textfield',
			 			        layout: 'hbox',
			 			        items: [
			 			        	{
			 			        			height: 40, labelWidth: 50, width: 180,
			 			        			bodyPadding: '30 30 30 30', bodyStyle: 'padding: 30px',
			 			        			fieldStyle: 'font-size: 20px; height: 40px; text-align: right;',
			 			        			hideTrigger:true,
											name: 'f_peso_del_carico',
											xtype: 'numberfield',
											fieldLabel: 'Peso<br/>(Kg)',
										    value: <?php echo j(trim($sped['CSPECA'])); ?>,
										    maxLength: 100								    							
									}, {							
					                     xtype: 'button', flex: 1,
										 margin: "0 0 0 20",			                     
						            	 scale: 'large',
						            	 iconCls: 'icon-button_red_play-32',
					                     text: 'ESEGUI RICALCOLO PESO COLLI',
								         handler: function() {
								         				
											form = this.up('form');								         				
								         							
											Ext.getBody().mask('Ricalcolo in corso', 'loading').show();								         									         
											Ext.Ajax.request({
											   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ricalcolo_pesi_colli',
											   method: 'POST',
											   jsonData: {form_values: form.getValues()}, 
											   
											   success: function(response, opts) {
											   	  Ext.getBody().unmask();
												  var jsonData = Ext.decode(response.responseText);									   	  
												  if (jsonData.success == true){
												  	acs_show_msg_info('Ricalcolo completato');
												  }	else {
												  	acs_show_msg_error('Errore in fase di ricalcolo colli');
												  }
											   }, 
											   failure: function(response, opts) {
											      Ext.getBody().unmask();
											      alert('error on save_manual');
											   }
											});				
															         
								         
								         
								         }
									}
			 			        ]
			 			     }, {
						        xtype: 'fieldset', flex: 2,
						        title: 'Registra modifica manuale pesi',
								defaultType: 'textfield',
			 			        layout: 'hbox',
			 			        items: [{							
					                     xtype: 'button',
										 margin: "0 0 0 20",			                     
						            	 scale: 'large',
						            	 iconCls: 'icon-save-32', flex: 1,
					                     text: 'Conferma',
								         handler: function() {
								         	form = this.up('form');
								         	form.save_manual(form, 'f_peso_prima_del_carico');
								         }
									}
								]
							}
	 			     	]
	 			     }
	 			     
	 			     
	 			     
	 			     , {
				        xtype: 'fieldset', flex: 1,
				        title: 'Acquisizione immagine',
						defaultType: 'textfield',
	 			        
	 			        items: [
	 			        
	 			        
						{
				         xtype: 'fieldcontainer',
				         title: 'Acquisizione immagine',
						 defaultType: 'textfield',						 
	 			        
	 			         items: [	
	 			         	{ xtype: 'tbfill' } 			        
	 			        
	 			        
	 			        <?php foreach ($cfg_mod_Spedizioni["acquisisci_peso_immagini"]["telecamere"] as $kt => $t) { ?>
							, {
			                     xtype: 'button',
								 margin: "0 3 7 0",			                     
				            	 scale: 'large',
				            	 iconCls: 'icon-camera-32',
			                     text: 'Acquisisci da <?php echo $t['descr']; ?>',
						         handler: function() {
						         	dv = this.up('fieldset').down('dataview');
						         	st = dv.getStore();
						         	
						         	Ext.getBody().mask('Retrieving image', 'loading').show();
									Ext.Ajax.request({
									   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_camera_image',
									   method: 'POST',
									   jsonData: {
									   	sped_id: '<?php echo $sped_id ?>',
									   	camera_id: <?php echo acs_je($kt) ?>
									   }, 
									   
									   success: function(response, opts) {
									   	  Ext.getBody().unmask();
										  var jsonData = Ext.decode(response.responseText);									   	  

					                      //se presente mostro il messaggio di errore (es: errore su ftp per Adiuto)
					                      if (!Ext.isEmpty(jsonData.msg_error)){
					                      	setTimeout(function() { acs_show_msg_error(jsonData.msg_error, 'Contattare amministratore di sistema'); }, 1500); //attendo 1,5s					                      	
										  }
									   	  
									   	  //apro l'ultimo inserito
									   	   dv.show_image(jsonData.url, jsonData.name, dv);
									   	   
									   	  //aggiorno elenco immagini 
					                       st.load();
					                       
									   }, 
									   failure: function(response, opts) {
									      Ext.getBody().unmask();
									      alert('error in get image');
									   }
									});						         	
						         	
									  						         							         	
						         }
							}, {
			                     xtype: 'button',
								 margin: "0 30 7 0",			                     
				            	 scale: 'large',
				            	 iconCls: 'icon-video-32',
			                     text: 'Video',
						         handler: function() {
						         	window.open(<?php echo acs_je($t['url_video']); ?>, 'popup_camera_<?php echo $kt ?>', 'width=800,height=800,resizable=0');	
						         }						
							}
					<?php } //foreach telecamera ?>	
							
							
				/*			
							, {							
			                     xtype: 'button',
								 margin: "0 0 7 20",			                     
				            	 scale: 'large',
				            	 iconCls: 'icon-camera-32',
			                     text: 'Acquisisci da telecamera - 2',
						         handler: function() {
						         	dv = this.up('fieldset').down('dataview');
						         	st = dv.getStore();
						         	
						         	Ext.getBody().mask('Retrieving image', 'loading').show();
									Ext.Ajax.request({
									   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_camera_image',
									   method: 'POST',
									   jsonData: {sped_id: '<?php echo $sped_id ?>'}, 
									   
									   success: function(response, opts) {
									   	  Ext.getBody().unmask();
										  var jsonData = Ext.decode(response.responseText);									   	  
									   	  
									   	  
									   	  //apro l'ultimo inserito
									   	   dv.show_image(jsonData.url, jsonData.name, dv);
									   	   
									   	  //aggiorno elenco immagini 
					                       st.load();					                      
									   }, 
									   failure: function(response, opts) {
									      Ext.getBody().unmask();
									      alert('error in get image');
									   }
									});						         	
						         	
									  						         							         	
						         }
							}
					*/		
							
							
							
						]
					}	
							
							
							
									, {
							 			     	xtype: 'dataview',
							 			     	title: 'Immagini',
							 			     	autoHeight: false,
       											flex: 1, height: '100%',
							 			     	autoScroll: 'auto',
							 			     	//style: 'border:0px solid #99BBE8; border-top-width: 0;',
							 			     	itemSelector: 'div.thumb-wrap',
							 			     	singleSelect: true,

								                //styleHtmlContent: true,
								                width: '100%',
								                layout: {
								                    type: 'fit'
								                },
								                inline: {
								                    wrap: true
								                },
								                //itemCls: 'dataview-item',							 			     	
							 			     	
							 			     	itemTpl: '<div class="thumb-wrap">' + 
							 			     	  		 '<div class="thumb"><img width=70 src="{url}" title="{name}"></div>' +
							 			     	  		 '</div>' +
							 			     	  		 '<div class="222x-clear"></div>',
							 			     	//tplWriteMode : 'overwrite',
							 			     	//itemSelector : '.content-row',
							 			     	//tpl: new Ext.XTemplate('<tpl for=".">',
							 			     	//		'<div class="content-row">',
						   						//			'<div class="thumb-wrap" id="{name}">',
						   						//			'<div class="thumb"><img height=70 src="{url}" title="{name}"></div>',
						   						//		'</div>',	
						   						//	  '</tpl>',
						   						//	  '<div class="x-clear"></div>'),
						   						 store: {
														autoLoad: true,
														//editable: false,
														//autoDestroy: true,	 
													    //fields: ['name', 'url', 'shortName'],
													    model: 'ModelDataView',
													    
														proxy: {
															url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_sped_images',
															extraParams   : {sped_id: '<?php echo $sped_id ?>'},
															method: 'POST',
															type: 'ajax',
															
															//Add these two properties
															actionMethods: {
																read: 'POST'					
															},
												
															reader: {
																type: 'json',
																method: 'POST',
																root: 'root'					
															}
															
															/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
															, doRequest: personalizza_extraParams_to_jsonData						        
															
														}														    
 
													}	
													
													
													, listeners: {
												          itemclick: {
												            fn: function(dataview, record) {
												               dataview.show_image(record.get('url'), record.get('name'), dataview);
												            }
												          }
												       }							
													
													
												, show_image: function(url, name, dv){
																var win = new Ext.Window({ 
												                     title: name,
												                     width: 500, height: 400,
												                     items : [{ xtype : 'panel', layout : 'column', 
												                                html: '<img src="' + url + '" width="500" height="400"/></img>'}],
												                     buttonAlign: 'left',           
												                     buttons: [
												                     
																		{							
														                     xtype: 'button',
																			 //margin: "0 0 0 20",			                     
															            	 scale: 'large',
															            	 iconCls: 'icon-sub_red_delete-32',
														                     text: 'Elimina',
																	         handler: function() {
																	           t_win = this.up('window');
																	           st    = dv.getStore();
																			   Ext.Msg.confirm('Richiesta conferma', 'Confermi eliminazione?', function(btn, text){
																			   if (btn == 'yes'){																	         	
																	         	
																	         	Ext.getBody().mask('Retrieving image', 'loading').show();
																				Ext.Ajax.request({
																				   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=delete_camera_image',
																				   method: 'POST',
																				   jsonData: {
																				   	path: url 
																				   }, 
																				   
																				   success: function(response, opts) {
																				   	  Ext.getBody().unmask();
																					  t_win.close();
																				   	   
																				   	  //aggiorno elenco immagini 
																                       st.load();					                      
																				   }, 
																				   failure: function(response, opts) {
																				      Ext.getBody().unmask();
																				      alert('error in get image');
																				   }
																				});						         	
																	         	
																				}
																			   });  						         							         	
																	         }
																		}, { xtype: 'tbfill' }, {
																		
																			 xtype: 'button',			                     
															            	 scale: 'large',
															            	 iconCls: 'icon-button_grey_eject-32',
														                     text: 'Chiuidi',
																	         handler: function() {
																	           t_win = this.up('window');
																	           t_win.close();
																	         }  																		
																		
																		}												                     
												                     
												                     
												                     ]
												               });
												               win.show();
												               												
												}	
														
							 			     }							
							
							
							
							
							
	 			        ]
	 			     }
	 			     
	 			     
	 			     
	 			     
	 			     
	 			     
	 			     
	 			     
	 			     
                	
	        ],             
				
        }
	]
}