<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();
$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

$data 		= $m_params->data;
$aspe 		= $m_params->aspe;
$itin_id	= $m_params->itin_id;

if (isset($_REQUEST['data'])){
	$data 		= trim($_REQUEST['data']);	
	$aspe 		= trim($_REQUEST['aspe']);
	$itin_id 	= trim($_REQUEST['itin_id']);		
}



// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
	$stmt = $s->scarichi_ridondanti($data, $aspe);
	
	$ret_data = array();
	while ($row = db2_fetch_assoc($stmt)){
		
		//lo segnalo solo se ha scarichi in questo itinerario
		if ($s->ha_scarichi_in_itinerario($data, $row['TDDT'], $row['TDCCON'], $row['TDCDES'], $itin_id)){				
			$row['elenco_sped_car'] = $s->el_sped_car($data, $row['TDDT'], $row['TDCCON'], $row['TDCDES'], $aspe);		
			$ret_data[] = $row;
		}
	}
	
	echo acs_je($ret_data);
	exit;
}



/******************************************************
 * GRID JSON
 ******************************************************/
?>


{"success": true, "items":
	{
		xtype: 'gridpanel',
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&data=<?php echo $data; ?>',
							extraParams: {aspe: <?php echo j($aspe) ?>, itin_id:<?php echo j($itin_id) ?>},
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'root'
					        }
						},
	        			fields: ['TDDT', 'TDCCON', 'TDDCON', 'TDCDES', 'TDDLOC', 'TDDCAP', 'TDPROD', 'elenco_sped_car']
	    			}),
	    			
		        columns: [{
			                header   : 'Codice',
			                dataIndex: 'TDCCON', 
			                width: 90
			             }, {
			                header   : 'Dest',
			                dataIndex: 'TDCDES', 
			                width: 40
			             }, {
			                header   : 'Denominazione',
			                dataIndex: 'TDDCON', 
			                flex: 1
			             }, {
			                header   : 'Localit&agrave;',
			                dataIndex: 'TDDLOC', 
			                flex: 1
			             }, {
			                header   : 'CAP',
			                dataIndex: 'TDDCAP', 
			                width: 50
			             }, {
			                header   : 'Prov',
			                dataIndex: 'TDPROD', 
			                width: 50
			             }, {
			                header   : 'Sped / Carichi',
			                dataIndex: 'elenco_sped_car', 
							flex: 2
						}]	    	
			             
		, listeners: {		
	 			afterrender: function (comp) {
					//comp.up('window').setTitle('');	 				
	 			}
	 	}			             									
		         
	}
}
