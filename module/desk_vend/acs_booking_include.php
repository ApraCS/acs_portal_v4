<?php

$s = new Spedizioni(array('no_verify' => 'Y'));

$conf_stato_booking = array(
		"00" => "In inserimento", 
		"01" => "Ricevuto",
		"02" => "Confermato",
		"03" => "Annullato",
		"04" => "Respinto",
		"05" => "Evaso",
        "06" => "Scaduta"
	);



function write_log_autobooking($sped, $id_richiesta, $utente, $testo){
    global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
    $ar_ins = array();
    
    $ar_ins["NTDT"] = $id_ditta_default;
    $ar_ins["NTTPNO"] = 'BOOKA';
    $ar_ins["NTKEY1"] = $sped;
    $ar_ins["NTKEY2"] = $id_richiesta;
    $ar_ins["NTUSGE"] = $utente;
    $ar_ins["NTMEMO"] = $testo;
   
    
    $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
   /* if(1==1){
        throw new Exception("Prova di tray catch");
    }*/
    
}


function get_email_cliente_PS($sped_id, $bstime, $in_cod_cli = null){
    global $conn, $cfg_mod_Spedizioni;
    
    if (!is_null($bstime)){
        $br = bk_get_row($bstime);
        $cod_cli = trim($br['BSCCON']);
    } else {
        $cod_cli = $in_cod_cli;
    }

    //email riferimetni di consegna (piano di spedizione)
    $sql = "SELECT substr(TAREST, 41, 60) AS EMAIL  
              FROM {$cfg_mod_Spedizioni['file_tab_sys']}
              where tadt = '1 ' and taid = 'BRCF'  
              AND TACOR1 = '{$cod_cli}'             
              AND TANR = '@PS'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    if ($row && strlen(trim($row['EMAIL'])) > 0)
        return $row['EMAIL'];
    
    //Se non l'ho trovata prendo quella standard del cliente    
     $sql = "SELECT substr(TAREST, 61, 60) AS EMAIL
        FROM {$cfg_mod_Spedizioni['file_tab_sys']}
        where tadt = '1 ' and taid = 'BWWW'
        AND TACOR1 = '{$cod_cli}'
        AND TANR = '*ON'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        if ($row && strlen(trim($row['EMAIL'])) > 0)
            return $row['EMAIL'];
    return '';        
}


function send_email_conferma($sped_id, $bstime){
    global $conn, $id_ditta_default, $cfg_mod_Booking, $cfg_mod_Spedizioni, $dati_azienda, $auth;
  $email_cliente = get_email_cliente_PS($sped_id, $bstime);
  $mail = prepare_new_mail($cfg_mod_Booking['email_invio_notifiche']);
  
  if (strlen($email_cliente) > 0)
    $mail->addAddress($email_cliente);
  else
    $mail->addAddress('m.arosti@apracs.it');
  
  //email trasportatore
    $sql_tras="SELECT *
    FROM {$cfg_mod_Booking['file_indirizzi']} IG
    WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$sped_id}' AND IGTAID= 'BKSPE' AND IGKEY2='TRAS'";
    
    $stmt_tras= db2_prepare($conn, $sql_tras);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_tras);    
    $row_tras = db2_fetch_assoc($stmt_tras);
    $mail->AddCC(trim($row_tras['IGMAI1']));    
  
  //$mail->AddBCC('m.arosti@apracomputersystem.it');
  
  $mail->Subject = 'Booking request notificaton';    
  $body = "

  Gentile cliente ,<br/>
  la richiesta di booking da lei effettuata &egrave; stata confermata. La ringraziamo per aver utilizzato i nostri sistemi.
  <br/>  Saluti<br/><br/>             
              
  Dear customer,<br/>
  the booking request you made on  has been confirmed.  Thank you for using our systems.
  <br/>  Greetings<br/>
  <b>" . $dati_azienda['den'] . "</b>
  <br/>------------------------------------------------------------------<br/><br/>";
  
  ob_start();
  $_REQUEST['nr_sped'] = $sped_id;
  $_REQUEST['bstime']  = $bstime;
  include "../desk_booking/acs_booking_report.php";
  $out_report = ob_get_contents();
  ob_end_clean();
  
  $mail->Body = $body . $out_report;
 // $mail->send();
  
  $ret = array();
  if(!$mail->send()){
      $ret['success'] = false;
      $ret['error_message'] = 'Errore: ' . $mail->ErrorInfo;
      
      $mail_2 = prepare_new_mail();
      $mail_2->addAddress($email_cliente);
    //  $mail_2->addAddress('s.cultrera@apracs.it');
      $mail_2->Subject = 'Errore email booking';
      $mail_2->Body = "Gentile cliente,<br/> l'invio dell'email non � andato a buon fine per il seguente problema : ".$mail->ErrorInfo;
      $mail_2->send();
      
      write_log_autobooking($sped_id, $bstime, $auth->get_user(), "Invio email con esito NEGATIVO: ".$mail->ErrorInfo);
  }else{
      $ret['success'] = true;
      write_log_autobooking($sped_id, $bstime, $auth->get_user(), "Invio email con esito positivo (al server SMTP)");
  }    
  
}





function send_email_respinta($sped_id, $bstime){
    global $conn, $id_ditta_default, $cfg_mod_Booking, $cfg_mod_Spedizioni, $dati_azienda, $auth;
    $br = bk_get_row($bstime);
    $email_cliente = get_email_cliente_PS($sped_id, $bstime);
    $mail = prepare_new_mail($cfg_mod_Booking['email_invio_notifiche']);
    
    if (strlen($email_cliente) > 0)
        $mail->addAddress($email_cliente);
    else
        $mail->addAddress('m.arosti@apracs.it');
            
    //email trasportatore
    $sql_tras="SELECT *
               FROM {$cfg_mod_Booking['file_indirizzi']} IG
               WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$sped_id}' AND IGTAID= 'BKSPE' AND IGKEY2='TRAS'";
    
    $stmt_tras= db2_prepare($conn, $sql_tras);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_tras);
    $row_tras = db2_fetch_assoc($stmt_tras);
    $mail->AddCC(trim($row_tras['IGMAI1']));
    
    //$mail->AddBCC('m.arosti@apracomputersystem.it');
    
    $mail->Subject = 'Booking request notificaton';
    $body = "
Gentile cliente,<br/>
la richiesta di booking da lei effettuata in data " . print_date($br['BSDTGE']) . "
con ritiro " . print_date($br['BSDTSR']) . " &egrave; stata respinta.
Si prega pertanto di effettuare una nuova richiesta di booking.
La ringraziamo per aver utilizzato i nostri sistemi.
<br/>
Saluti<br/>
<b>" . $dati_azienda['den'] . "</b>
<br/><br/>
    
    
Dear customer,<br/>
the booking request you made on " . print_date($br['BSDTGE']) . "
with loading " . print_date($br['BSDTSR']) . " was rejected.
Therefore, please make a new booking request.
Thank you for using our systems.
<br/>
Greetings<br/>
<b>" . $dati_azienda['den'] . "</b>
			";
            
            $mail->Body = $body;
            //$mail->send();
            
            $ret = array();
            if(!$mail->send()){
                $ret['success'] = false;
                $ret['error_message'] = 'Errore: ' . $mail->ErrorInfo;
                
                $mail_2 = prepare_new_mail();
                $mail_2->addAddress($email_cliente);
                //  $mail_2->addAddress('s.cultrera@apracs.it');
                $mail_2->Subject = 'Errore email booking';
                $mail_2->Body = "Gentile cliente,<br/> l'invio dell'email non � andato a buon fine per il seguente problema : ".$mail->ErrorInfo;
                $mail_2->send();
                
                write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Invio email con esito NEGATIVO: ".$mail->ErrorInfo);
            }else{
                $ret['success'] = true;
                write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Invio email con esito positivo (al server SMTP)");
            }   
            
}



function get_last_booking_request_by_sped($sped_id, $solo_validi = true){
    global $conn, $id_ditta_default, $cfg_mod_Booking, $conf_stato_booking;
    
    if ($solo_validi == true)
        $sql_where_solo_validi = " AND BSSTAT IN('01', '02', '05') ";
    else $sql_where_solo_validi = '';
    
    $sql = "SELECT * FROM {$cfg_mod_Booking['file_richieste']} BS
			WHERE BSDT='$id_ditta_default' AND BSNBOC = {$sped_id}
            {$sql_where_solo_validi} 
            ORDER BY BSTIME DESC LIMIT 1";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    if ($row){
        $row['BSSTAT_O'] = $conf_stato_booking[$row['BSSTAT']];
    }
    return $row;
}


function bk_out_row($row){
	global $conf_stato_booking;
	return array(
				'stato'=> $row['BSSTAT'], 
				'stato_desc' => "[" .acs_u8e(trim($row['BSSTAT']))."] ".$conf_stato_booking[$row['BSSTAT']],
				'f_log' => trim($row['BSFLAL']),
				'f_log_data' => $row['BSDTAL'],
				'f_log_data_rit' => $row['IGCDFI'],
				'f_log_ora' => $row['BSORAL'],
				'f_log_utente' => acs_u8e(trim($row['BSUSAL'])),
				'f_amm' => acs_u8e(trim($row['BSFLAA'])),
				'f_amm_data' => acs_u8e(trim($row['BSDTAA'])),
				'f_amm_ora' => acs_u8e(trim($row['BSORAA'])),
				'f_amm_utente' => acs_u8e(trim($row['BSUSAA']))
	);
}

function auto_accettazione_amministrativa(){
    global $conn, $id_ditta_default, $cfg_mod_Booking;
	//recupero le richieste ricevute non ancora accettate
	$sql = "SELECT * FROM {$cfg_mod_Booking['file_richieste']} BS
			WHERE BSDT='$id_ditta_default' AND BSSTAT = '01' AND BSFLAA = ''";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	while ($row = db2_fetch_assoc($stmt)) {
	    
		//$ar = get_ar_carichi($row['BSNBOC']);
		//$row['flag_pagamento'] = componi_flag_pagamento($ar);
		
		$s = new Spedizioni(array('no_verify' => 'Y'));
		$row['flag_pagamento'] = $s->get_stato_pagamento_sped($row['BSNBOC'], true);		

		if (
		          $row['flag_pagamento'] == 'C'   //pagamento completo
		       || $row['flag_pagamento'] == 'B'   //tutti pagamenti differiti (icona Blu)
		    ){
			$ar_ins = array();
			$ar_ins['BSFLAA'] 	= 'Y';
			$ar_ins['BSUSAA']   = 'AUTO';
			$ar_ins['BSDTAA']   = oggi_AS_date();
			$ar_ins['BSORAA']   = oggi_AS_time();
				
			$sqlU = "UPDATE {$cfg_mod_Booking['file_richieste']}
					SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
					WHERE BSTIME= '{$row['BSTIME']}' ";
			
			$stmtU = db2_prepare($conn, $sqlU);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmtU, $ar_ins);
			
			set_conferma($row['BSTIME']);
			write_log_autobooking($row['BSNBOC'], $row['BSTIME'], 'Supply Desk', ' Accettazione amministrativa automatica');
		}
	}
}



function auto_evasione_confermate_con_ordini_evasi(){
    global $conn, $id_ditta_default, $cfg_mod_Booking,$cfg_mod_Spedizioni;
	//recupero le richieste confermate che hanno almeno un ordine evaso -> Imposto la richiesta in "Evasa" (05)
	$sql = "SELECT DISTINCT(BSTIME) FROM {$cfg_mod_Booking['file_richieste']} BS
			INNER JOIN {$cfg_mod_Spedizioni['file_testate']} ON BSDT=TDDT AND BSNBOC=TDNBOF AND TDFN11=1
			WHERE BSDT='$id_ditta_default' AND BSSTAT = '02'";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	while ($row = db2_fetch_assoc($stmt)) {

			$ar_ins = array();
			$ar_ins['BSSTAT'] 	= '05'; //richiesta Evasa

			$sqlU = "UPDATE {$cfg_mod_Booking['file_richieste']}
					SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
					WHERE BSTIME= '{$row['BSTIME']}' ";
				
			$stmtU = db2_prepare($conn, $sqlU);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmtU, $ar_ins);
			
			write_log_autobooking($row['BSNBOC'], $row['BSTIME'], 'Supply Desk', 'Booking avanzato in EVASO (confermato con ordine evaso)');
	}
	
	
	
	//imposto come Evasa (05) anche le spedizioni che non hanno piu' ordini associati
	$sql = "SELECT DISTINCT(BSTIME) FROM {$cfg_mod_Booking['file_richieste']} BS
	         LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD ON BSDT=TDDT AND BSNBOC=TDNBOF
	         WHERE BSDT='$id_ditta_default' AND BSSTAT = '02'
             AND TD.TDDOCU IS NULL
            ";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	while ($row = db2_fetch_assoc($stmt)) {
	    
	    $ar_ins = array();
	    $ar_ins['BSSTAT'] 	= '05'; //richiesta Evasa
	    
	    $sqlU = "UPDATE {$cfg_mod_Booking['file_richieste']}
	             SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
	             WHERE BSTIME= '{$row['BSTIME']}' ";
	    
	    $stmtU = db2_prepare($conn, $sqlU);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmtU, $ar_ins);
	    
	    write_log_autobooking($row['BSNBOC'], $row['BSTIME'], 'Supply Desk', "Booking avanzato in EVASO (senza ordini associati)");
	}
	
	
}




function auto_chiusura_scadute_e_invio_email(){
    global $conn, $id_ditta_default, $cfg_mod_Booking,$cfg_mod_Spedizioni, $dati_azienda;
    
    //recupero le richieste ancora in stato immesso, con data richiesta scadute
    $sql = "SELECT * FROM {$cfg_mod_Booking['file_richieste']} BS  
            WHERE BSDT='$id_ditta_default' AND BSSTAT = '01' AND BSDTSR < " . oggi_AS_date();
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while ($row = db2_fetch_assoc($stmt)) {
        
        $ar_ins = array();
        $ar_ins['BSSTAT'] 	= '06'; //richiesta Scaduta
        
        $sqlU = "UPDATE {$cfg_mod_Booking['file_richieste']}
                 SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                WHERE BSTIME= '{$row['BSTIME']}' ";
        
        $stmtU = db2_prepare($conn, $sqlU);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmtU, $ar_ins);
        
        write_log_autobooking($row['BSNBOC'], $row['BSTIME'], 'Supply Desk', 'Chiusa richiesta booking scaduta');
        
        //Recupero email a cui inviare segnalazione
        $sql_doga="SELECT * FROM {$cfg_mod_Booking['file_indirizzi']} IG
                    WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$row['BSNBOC']}' AND IGTAID= 'BKSPE' AND IGKEY2='DOGA'";
        $stmt_doga= db2_prepare($conn, $sql_doga);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_doga);
        $row_doga = db2_fetch_assoc($stmt_doga);
        $ret_doga = array(
            "denom"  => acs_u8e(trim($row_doga['IGRGSO'])),
            "telefono"  => acs_u8e(trim($row_doga['IGTEL1'])),
            "email"  => acs_u8e(trim($row_doga['IGMAI1'])),
            
        );
        
        
    
            //invio email -------------
            $mail = prepare_new_mail();
            $mail->addAddress($cfg_mod_Booking['email_notifica_chiusura_scadute']);
            if ($cfg_mod_Booking['invia_cliente_notifica_chiusura_scadute'] != 'N')
                $mail->addAddress(trim($ret_doga['email']));
                $mail->Subject = 'Annullamento richiesta booking';
                
                $body = "
Gentile " . trim($ret_doga['denom']) . ",<br/>
la richiesta di booking da lei effettuata in data " . print_date($row['BSDTGE']) . "
con ritiro " . print_date($row['BSDTSR']) . " &egrave; scaduta.
Si prega pertanto di effettuare una nuova richiesta di booking.
La ringraziamo per aver utilizzato i nostri sistemi.
<br/>
Saluti<br/>
<b>" . $dati_azienda['den'] . "</b>
<br/><br/>
    
    
Dear " . trim($ret_doga['denom']) . ",<br/>
the booking request you made on " . print_date($row['BSDTGE']) . "
with loading " . print_date($row['BSDTSR']) . " has expired.
Therefore, please make a new booking request.
Thank you for using our systems.
<br/>
Greetings<br/>
<b>" . $dati_azienda['den'] . "</b>
			";
       
$mail->Body = $body;
$addresses = out_list_value_from_array($mail->getAllRecipientAddresses());
write_log_autobooking($row['BSNBOC'], $row['BSTIME'], 'Supply Desk', "Invio email richiesta booking scadute a indirizzi: {$addresses}");

// $mail->send();

$ret = array();
if(!$mail->send()){
    $ret['success'] = false;
    $ret['error_message'] = 'Errore: ' . $mail->ErrorInfo;
    
    $mail_2 = prepare_new_mail();
    $mail->addAddress($cfg_mod_Booking['email_notifica_chiusura_scadute']);
    if ($cfg_mod_Booking['invia_cliente_notifica_chiusura_scadute'] != 'N')
        $mail->addAddress(trim($ret_doga['email']));
    $mail_2->Subject = 'Errore email scadute booking';
    $mail_2->Body = "Gentile " . trim($ret_doga['denom']) . ",<br/> l'invio dell'email non � andato a buon fine per il seguente problema : ".$mail->ErrorInfo;
    $mail_2->send();
    
    write_log_autobooking($row['BSNBOC'], $row['BSTIME'], 'Supply Desk', "Invio email con esito NEGATIVO: ".$mail->ErrorInfo);
}else{
    $ret['success'] = true;
    write_log_autobooking($row['BSNBOC'], $row['BSTIME'], 'Supply Desk', "Invio email con esito positivo (al server SMTP)");
}            
             

       
        
        
        
        
    }
}





function get_ar_carichi($nboc){
	global $conn, $id_ditta_default,$cfg_mod_Spedizioni;
	$ar = array();
	$sql_1 = "SELECT TDAACA, TDNRCA, PSFG01
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
				WHERE TDNBOF='$nboc'
				GROUP BY TDNBOF, TDAACA, TDNRCA, PSFG01";
	
	$stmt_1 = db2_prepare($conn, $sql_1);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_1);
	while ($row1 = db2_fetch_assoc($stmt_1)) {
		$anno_numero=implode("_", array($row1['TDAACA'], $row1['TDNRCA']));
		$ar[$anno_numero] = $row1['PSFG01'];
	}
	return $ar;	
}


function cambia_stato_spedizione($sped_id, $stato, $a_data = 0){
    global $s, $conn, $id_ditta_default, $cfg_mod_Spedizioni, $auth;
	
	$ar_upd = array('CSSTSP' => $stato);	
	if ($a_data > 0)
		$ar_upd['CSDTPG'] = $a_data;
		
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE CSDT='$id_ditta_default' 
			AND CSCALE = '*SPR' AND CSPROG = ?";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	db2_execute($stmt, array_merge(
			$ar_upd,
			array($sped_id)
		));

	if ($stato == 'PI')
		replica_dati_su_sped($sped_id);
	
	//esco se non ho fatto modifiche di data
	if ($a_data == 0) return;
	
	
	write_log_autobooking($row['BSNBOC'], $row['BSTIME'], $auth->get_user(),  "Modifica stato spedizione/data ordine");
	
	//scrivo su RI
	$sh = new SpedHistory();
	$sh->crea(
			'modifica_spedizione',
			array(
					"sped_id"		=> $sped_id
			)
	);
	
	$sped = $s->get_spedizione($sped_id);		
	
	//Recupero l'elenco degli ordini e aggiorno i dati in base alla spedizione
	$sql = "SELECT TDDOCU FROM {$cfg_mod_Spedizioni['file_testate']} WHERE " . $s->get_where_std() . " AND TDNBOF = ?";	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($sped_id));
	
	while ($row = db2_fetch_assoc($stmt)) {	    
	    
	    //per le spedizioni flight eseguo solo il refresh dell'ordine
	    if ($sped['CSFG04'] == 'F') {
	        //HISTORY
	        $sh = new SpedHistory();
	        $sh->crea(
	            'sincronizza_ordine',
	            array(
	                "k_ordine" 	=> $row['TDDOCU'])
	            );
	    } else {
	        $s->exe_allinea_dati_spedizione_su_testata($sped, $row['TDDOCU']);
	        $ord = $s->get_ordine_by_k_docu($row['TDDOCU']);
	        
	        $sh = new SpedHistory();
	        $sh->crea(
	            'assegna_spedizione',
	            array(
	                "k_ordine" 	=> $row['TDDOCU'],
	                "sped"		=> $sped,
	                "op"		=> 'UPD',
	                "ex"		=> "(booking)",
	                "seca"		=> $ord['TDSECA'] //attualmente va passata altrimenti vienere resettata
	            )
	        );
	    }
		
	} //while ord

	
}



function replica_dati_su_sped($sped_id){
	global $s, $conn, $id_ditta_default, $cfg_mod_Spedizioni, $cfg_mod_Booking, $auth;

	$sql_doga="SELECT * FROM {$cfg_mod_Booking['file_indirizzi']} IG
				WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$sped_id}' AND IGTAID= 'BKSPE' AND IGKEY2='DOGA'";	
	$stmt_doga= db2_prepare($conn, $sql_doga);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_doga);	
	$row_doga = db2_fetch_assoc($stmt_doga);	
	$ret_doga = array(
			"denom"  => acs_u8e(trim($row_doga['IGRGSO'])),
			"telefono"  => acs_u8e(trim($row_doga['IGTEL1'])),
			"email"  => acs_u8e(trim($row_doga['IGMAI1'])),
	
	);
	

	//TRASPORTATORE
	$sql_tras="SELECT *
				FROM {$cfg_mod_Booking['file_indirizzi']} IG
				WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$sped_id}' AND IGTAID= 'BKSPE' AND IGKEY2='TRAS'";
	
	$stmt_tras= db2_prepare($conn, $sql_tras);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_tras);
	
	$row_tras = db2_fetch_assoc($stmt_tras);
	
	$ret_tras = array(
			"denom"  => acs_u8e(trim($row_tras['IGRGSO'])),
			"indirizzo"  => acs_u8e(trim($row_tras['IGINDI'])),
			"cap"  => acs_u8e(trim($row_tras['IGCAP'])),
			"loca"  => acs_u8e(trim($row_tras['IGLOCA'])),
			"prov"  => acs_u8e(trim($row_tras['IGPROV'])),
			"nazione"  => acs_u8e(trim($row_tras['IGDNAZ'])),
			"p_iva"  => acs_u8e(trim($row_tras['IGPIVA'])),
			"telefono"  => acs_u8e(trim($row_tras['IGTEL1'])),
			"email"  => acs_u8e(trim($row_tras['IGMAI1'])),
			"orario"  => acs_u8e(trim($row_tras['IGFG01']))
	);
	
	


	//aggiorno TARGA
		$targa  = $row_doga['IGLOCA'];
		$ar_upd = array();
		$ar_upd['CSTARG'] = trim($targa);
	
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}
				SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CSDT='$id_ditta_default'
				AND CSCALE = '*SPR' AND CSPROG = ?";
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		db2_execute($stmt, array_merge(
				$ar_upd,
				array($sped_id)
		));
		
		
		
	//email TRASPORTATORE	
		$sped_id = sprintf("%08s", $sped_id);
		$tataid = 'SPINT';
		//verifico se gia' esiste
		$sql = "SELECT COUNT(*) as C_ROW FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($id_ditta_default, $tataid, $sped_id));
		$row = db2_fetch_assoc($stmt);
		
		if ($row['C_ROW'] == 0) {
			//INSERT
			$ar_ins = array();
			$ar_ins['TADT'] 	= $id_ditta_default;
			$ar_ins['TATAID'] 	= $tataid;
			$ar_ins['TAKEY1'] 	= $sped_id;
			$ar_ins['TAMAIL'] 	= $ret_tras['email'];	//email trasportatore
			$ar_ins['TALOCA'] 	= $ret_doga['email'];	//email richiesta (su campo dogana)
		
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();
		
			$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
		} else {
			//UPDATE
			$ar_ins = array();
			$ar_ins['TAMAIL'] 	= $ret_tras['email'];	//email trasportatore
			$ar_ins['TALOCA'] 	= $ret_doga['email'];	//email richiesta (su campo dogana)
		
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();
		
			$sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']} SET " . create_name_field_by_ar_UPDATE($ar_ins) . " WHERE  TADT = ? AND TATAID = ? AND TAKEY1 = ?";
		
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array_merge(
					$ar_ins,
					array($id_ditta_default, $tataid, $sped_id)
					));
			echo db2_stmt_errormsg($stmt);
		}
		
		write_log_autobooking($row['BSNBOC'], $row['BSTIME'], $auth->get_user(), 'Aggiorna targa/email su spedizione');
		
	
} //replica_dati_su_sped()




function bk_get_row($bstime){
	global $conn, $id_ditta_default, $cfg_mod_Booking;

	$sql= "SELECT * FROM {$cfg_mod_Booking['file_richieste']} BS
			WHERE BSDT ='$id_ditta_default' AND BSTIME='{$bstime}'";

	
	$stmt= db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$r = db2_fetch_assoc($stmt);

	return $r;

}





function set_conferma($bstime){

    global $conn, $id_ditta_default, $cfg_mod_Booking, $auth;

	$ar_ins = array();

	$sql= "SELECT *
			FROM {$cfg_mod_Booking['file_richieste']} BS
			WHERE BSDT ='$id_ditta_default' AND BSTIME='{$bstime}'";


	$stmt= db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$r = db2_fetch_assoc($stmt);

	if($r['BSFLAL'] == 'Y' && $r['BSFLAA'] == 'Y'){

		$ar_ins['BSSTAT'] 	= '02';

		$sql="UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSDT ='$id_ditta_default' AND BSTIME='{$bstime}'";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		
		$br = bk_get_row($bstime);
		write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Assegna stato [02]: Conferma");
    	cambia_stato_spedizione($br['BSNBOC'], 'PI', $br['BSDTSR']);
		try {
		  send_email_conferma($br['BSNBOC'], $bstime);
		} catch(ErrorException $e){
            //nothing - ToDo		    
		}
	}


	return true;

}


function set_respinta($bstime){

    global $conn, $id_ditta_default, $cfg_mod_Booking, $auth;

	$ar_ins = array();

	$sql= "SELECT *
	FROM {$cfg_mod_Booking['file_richieste']} BS
	WHERE BSDT ='$id_ditta_default' AND BSTIME='{$bstime}'";

	$stmt= db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$r = db2_fetch_assoc($stmt);

	if($r['BSFLAL'] == 'N' || $r['BSFLAA'] == 'N'){
		$ar_ins['BSSTAT'] 	= '04';

		$sql="UPDATE {$cfg_mod_Booking['file_richieste']}
		SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
		WHERE  BSDT ='$id_ditta_default' AND BSTIME='{$bstime}'";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);

		$br = bk_get_row($bstime);
		write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Assegna stato [04]: Respinta");
		cambia_stato_spedizione($br['BSNBOC'], 'DP');
		try {
		    send_email_respinta($br['BSNBOC'], $bstime);
		} catch(ErrorException $e){
		    //nothing - ToDo
		}
	}
	return true;

}




function componi_flag_pagamento($ar){
    
	//verifico se tutti i carichi sono pagati
	$pagato = true;
	foreach($ar as $k => $v){
		if($v != 'C')
			$pagato=false;

	}
	if($pagato == true)		return 'C';

	//verifico se tutti i carichi sono NON pagati
	$non_pagato = true;
	foreach($ar as $k => $v){
		if($v != 'N' && trim($v) != '')
			$non_pagato= false;
				
	}
	if($non_pagato == true)		return 'N';
	
	//verifico se tutti hanno il tipo pagamento (TDAUX1) > 'C' (cioe' pagamenti differiti)
	$solo_pagamenti_differiti = true;
	foreach($ar as $k => $v){
	    if(substr($v, 0, 1) != 'T' || 
	        (substr($v, 0, 1) == 'T' && !in_array(substr($v, 1, 1), array('D', 'E', 'F'))))
	        $solo_pagamenti_differiti = false;
	        
	}
	if($solo_pagamenti_differiti == true)		return 'B';

	//se sono qui sono in un pagamento parziale
	return 'P';

}


function get_options_itin($mostra_codice = 'Y'){
	global $conn, $s;
	global $cfg_mod_Booking, $id_ditta_default;
	$ar = array();
	$sql = "SELECT BSCITI
	FROM {$cfg_mod_Booking['file_richieste']} BS
	WHERE BSDT='$id_ditta_default' AND BSCITI <> ''";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);
	while ($row = db2_fetch_assoc($stmt)){
		if ($mostra_codice == 'Y')
			$text = "[" . trim($row['BSCITI']) . "] " . $s->decod_std('ITIN', trim($row['BSCITI']));
			else $text =  $s->decod_std('ITIN', trim($row['BSCITI']));
			$ar[] = array("id"=> trim($row['BSCITI']), "text" => $text);
	}
	return $ar;
}




function set_annulla($bstime){

    global $conn, $id_ditta_default, $cfg_mod_Booking, $auth;

	$ar_ins = array();


	$ar_ins['BSSTAT'] 	= '01'; //rimetto la richiesta in ricevuto

	$sql="UPDATE {$cfg_mod_Booking['file_richieste']}
	SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
	WHERE  BSDT ='$id_ditta_default' AND BSTIME='{$bstime}'";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$br = bk_get_row($bstime);
	write_log_autobooking($br['BSNBOC'], $br['BSTIME'], $auth->get_user(), "Assegna stato in [01]: Ricevuta");
	cambia_stato_spedizione($br['BSNBOC'], 'DP');

	return true;

}



function dettagli_anagrafici_NON_USATA($operatore, $valore){
	global $s;

?>	
	
	
				{
					xtype: 'fieldset',
	                title: '<?php echo $operatore ?>',
	                layout: 'anchor',
	                //itemId: 'sosta_yes'
	                flex:1,
	                disabled: <?php echo $valore ?>,
	              	items: [
							{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ xtype: 'textfield', flex: 1},
						items: [					
							{
						name: 'f_denom',
						fieldLabel: 'Denominazione',
						//labelWidth: 130,
						margin: '0 10 0 0',
					    maxLength: 100,
					   	},{
						name: 'f_indi',
						labelWidth: 70,
						fieldLabel: 'Indirizzo',
						maxLength: 100,
											
						}
						]
					},
						{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [
																						
								{
									name: 'f_loca', 
									labelWidth: 60,
									flex: 1,
									fieldLabel: 'Localit&agrave;',
									
								}, {
									name: 'f_cap', 
									width: 100,
									fieldLabel: 'Cap',
									labelWidth: 40,
									maxLength: 5,
									labelAlign: 'right',
								    },{
									name: 'f_prov', 
									labelWidth: 70,
									fieldLabel: 'Provincia', 
									flex:1,
									labelAlign: 'right'
									   }, {
									name: 'f_nazi',
									flex:1,
									fieldLabel: 'Nazione',
									labelWidth: 60,
								    labelAlign: 'right',
								    margin: '0 10 0 0'
										}
				
								  ]
								}	,
					 	{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [ 
							{
								name: 'f_tel',
								fieldLabel: 'Telefono',
							    maxLength: 20,
							    flex: 2,
							    labelWidth: 60,
							    margin: '0 10 0 0'
							},	{
								name: 'f_iva',
								fieldLabel: 'P. IVA',
								labelAlign: 'right',
							    maxLength: 100,
							    flex: 3,
							    labelWidth: 40							
							}, 
							
							{	name: 'f_email',
								labelAlign: 'right',
								fieldLabel: 'Email',
							   	flex: 2.5,
							    labelWidth: 50							
								}
						]
					}
	             ]
	             }	

<?php } ?>