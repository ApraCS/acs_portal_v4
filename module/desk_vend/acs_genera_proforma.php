<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_genera_proforma'){
     $ritime = microtime(true);
        
     foreach($m_params->list_selected_id as $v){
            
            $ar_upd = array();
            $ar = explode("_", $v->k_ordine);
            $ar_cli = explode("_", $v->k_cli_des);
            
            $ar_upd['RATIME'] = $ritime;
            $ar_upd['RADT']   = $ar[0];
            $ar_upd['RATIDO'] = $ar[1];
            $ar_upd['RAINUM'] = $ar[2];
            $ar_upd['RAAADO'] = $ar[3];
            $ar_upd['RANRDO'] = $ar[4];
            $ar_upd['RACLIE'] = $ar_cli[1];
            $sql = "INSERT INTO {$cfg_mod_Gest['file_richieste_anag_cli']} (" . create_name_field_by_ar($ar_upd) . ") VALUES (" . create_parameters_point_by_ar($ar_upd) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd);
            echo db2_stmt_errormsg($stmt);
        }
        
        $sh = new SpedHistory();
        $num_creato = $sh->crea(
            'pers',
            array(
                "RITIME"    => $ritime,
                "messaggio"	=> 'GEN_FAT_PROFORMA',
                "vals" => array(
                    "RICITI" 	=>  $m_params->form_values->f_banca,
                    "RIVETT" 	=> $m_params->form_values->f_pagamento
                )
            )
            );
        
        
        
        $ret = array();
        $ret['success'] = true;
        $ret['num_creato'] = trim($num_creato['RINOTE']);
        echo acs_je($ret);
        exit;
}

if ($_REQUEST['fn'] == 'open_form'){ ?>
    {"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            layout: {pack: 'start', align: 'stretch'},
	            items: [
	              
	                <?php write_combo_std('f_banca', 'Banca', '', acs_ar_to_select_json(find_TA_sys('CUBA',  null, null, null, null, null, 0, "", 'Y'), '') ) ?>
	              , <?php write_combo_std('f_pagamento', 'Pagamento', '', acs_ar_to_select_json(find_TA_sys('CUCP',  null, null, null, null, null, 0, "", 'Y'), '') ) ?>
	            ],
	            buttons : [
				
					{
	            		text: 'Conferma',
	            		iconCls: 'icon-save-32',
	            		scale: 'large',
	            		handler: function() {
	            		 
	            		 var form = this.up('form').getForm();
	            		 var loc_win = this.up('window');
	            		 
	            			Ext.MessageBox.confirm('Richiesta conferma', 'Confermi la generazione della fattura proforma?', function(btn){
                			   if(btn === 'yes'){
                			     Ext.Ajax.request({
                				        url        : 'acs_genera_proforma.php?fn=exe_genera_proforma',
                				        method     : 'POST',
                	        			jsonData: {
                	        				list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>,
                						    form_values: form.getValues()
                						},							        
                				       success: function(response, opts) {
                				        	 var jsonData = Ext.decode(response.responseText);
                				        	 Ext.Msg.alert('Message', jsonData.num_creato);
                							 loc_win.close();
                	            		   
                	            		},
                				        failure    : function(result, request){
                				            Ext.Msg.alert('Message', 'No data to be loaded');
                				        }
                				    });
                			    }else{
                			   }
                			 });	         			 	            	            
	            		} //handler
	            	} //conferma
				]
	            }
	           ]}
    
<?php  
}