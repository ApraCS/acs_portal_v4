<?php

//Visualizzo le righe (da gestionale) con le relative sottorighe (tree) ed eventuale fabbisogno....

require_once "../../config.inc.php";
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();
$k_ordine = $m_params->k_ordine;
$ord =  $main_module->get_ordine_by_k_docu($k_ordine);
$oe = $main_module->k_ordine_td_decode_xx($k_ordine);

$title = "Righe ordine {$oe['TDOADO']}_{$oe['TDONDO']}_{$ord['TDOTPD']}";
if($m_params->h_col == 'Y')
    $hidden = 'true';
else 
    $hidden = 'false';


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    $data = array();
    
    global $backend_ERP;
    switch ($backend_ERP){
        
        case 'GL':
            //
            
            $sql = "SELECT                             
                            RD.MECAR0 AS RDART,
                            MADES0    AS RDDART,
                            MEUNM0    AS RDUM,
                            MERID0    AS RDRIGA,
                            MEQTA0    AS RDQTA, MEQTS0 AS RDQTE, MERID0 AS RDRIGA,
                            MEIMN0    AS RTPRZ
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_art']} ART
					ON RD.MECAR0 = ART.MACAR0
					WHERE '1 ' = ? AND RD.MECAD0 = ? AND RD.METND0 = ? AND RD.MEAND0 = ? AND RD.MENUD0 = ?";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $oe);
            
            $ar = array();
            while ($row = db2_fetch_assoc($stmt)) {
                
                //stacco dei livelli
                $cod_liv0 = $row['RDRIGA'];
                $cod_liv1 = $row['RDSRIG'];
                
                
                $tmp_ar_id = array();
                $ar_r= &$ar;
                
                //N.RIGA
                $liv =$cod_liv0;
                $tmp_ar_id[] = $liv;
                if (!isset($ar_r["{$liv}"])){
                    $ar_new = $row;
                    $ar_new['children'] = array();
                    $ar_new['id'] = implode("|", $tmp_ar_id);
                    $ar_new['task'] = $row['RDRIGA'];
                    $ar_new['liv'] = 'liv_0';
                    $ar_new['tipo_liv'] = 'riga';
                    $ar_new['RDPROG'] = acs_u8e($row['RDPROG']);
                    $ar_new['RDNRDO'] = acs_u8e($row['RDNRDO']);
                    $ar_new['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
                    $ar_new['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
                    $ar_r["{$liv}"] = $ar_new;
                }
                $ar_r = &$ar_r["{$liv}"];
                
                if ($row['RDSRIG'] == 0)
                    continue;
                    
                    //SOTTORIGA
                    $liv=$cod_liv1;
                    $ar_r = &$ar_r['children'];
                    $tmp_ar_id[] = $liv;
                    if(!isset($ar_r[$liv])){
                        $ar_new = $row;
                        
                        $ar_new['id'] = implode("|", $tmp_ar_id);
                        $ar_new['task'] = $row['RDSRIG'];
                        $ar_new['liv'] = 'liv_1';
                        $ar_new['riga'] = $row['RDRIGA'];
                        $ar_new['tipo_liv'] = 'sottoriga';
                        $ar_new['RDPROG'] = acs_u8e($row['RDPROG']);
                        $ar_new['RDNRDO'] = acs_u8e($row['RDNRDO']);
                        $ar_new['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
                        $ar_new['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
                        $ar_new['leaf'] = true;
                        $ar_r["{$liv}"] = $ar_new;
                    }                    
            }
            
            break;            
            
        default: //SV2
            
            $where_riga = sql_where_by_combo_value('RD.RDRIGA', $m_params->riga);
            
            $sql = "SELECT RD.*, RT.RTPRZ, AR.ARTPAR
                    FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                    LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
                    ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
                    LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_art']} AR
                    ON AR.ARDT = RD.RDDT AND AR.ARART = RD.RDART         
                    WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
                    AND RD.RDTISR = ''
                    {$where_riga}
                    ORDER BY RDRIGA, RDSRIG";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $oe);
            
            
            $ar = array();
            while ($row = db2_fetch_assoc($stmt)) {
                
                //stacco dei livelli
                $cod_liv0 = $row['RDRIGA'];
                $cod_liv1 = $row['RDSRIG'];
                
                
                $tmp_ar_id = array();
                $ar_r= &$ar;
                
                //N.RIGA
                $liv =$cod_liv0;
                $tmp_ar_id[] = $liv;
                if (!isset($ar_r["{$liv}"])){
                    $ar_new = $row;
                    $ar_new['children'] = array();
                    $ar_new['id'] = implode("|", $tmp_ar_id);
                    $ar_new['task'] = $row['RDRIGA'];
                    $ar_new['liv'] = 'liv_0';
                    $ar_new['tipo_liv'] = 'riga';
                    $ar_new['RDPROG'] = acs_u8e($row['RDPROG']);
                    $ar_new['RDNRDO'] = acs_u8e($row['RDNRDO']);
                    $ar_new['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
                    $ar_new['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
                    $ar_r["{$liv}"] = $ar_new;
                }
                $ar_r = &$ar_r["{$liv}"];
                
                if ($row['RDSRIG'] == 0)
                    continue;
                    
                    //SOTTORIGA
                    $liv=$cod_liv1;
                    $ar_r = &$ar_r['children'];
                    $tmp_ar_id[] = $liv;
                    if(!isset($ar_r[$liv])){
                        $ar_new = $row;
                        
                        $ar_new['id'] = implode("|", $tmp_ar_id);
                        $ar_new['task'] = $row['RDSRIG'];
                        $ar_new['liv'] = 'liv_1';
                        $ar_new['riga'] = $row['RDRIGA'];
                        $ar_new['tipo_liv'] = 'sottoriga';
                        $ar_new['RDPROG'] = acs_u8e($row['RDPROG']);
                        $ar_new['RDNRDO'] = acs_u8e($row['RDNRDO']);
                        $ar_new['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
                        $ar_new['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
                        $ar_new['leaf'] = true;
                        $ar_r["{$liv}"] = $ar_new;
                    }
                    
            }
            
            
    } //switch $backend_ERP
    
    $ord_exp = explode('_', $n_ord);
    $anno = $ord_exp[3];
    $ord = $ord_exp[4];
    $ar_ord =  $main_module->get_ordine_by_k_docu($n_ord);
    
    $m_ord = array(
        'TDONDO' => $ar_ord['TDONDO'],
        'TDOADO' => $ar_ord['TDOADO'],
        'TDDTDS' => print_date($ar_ord['TDDTDS'])
    );
    
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array(
        'success' 	=> true,
        'children' 	=> $ret
    ));
    
    $appLog->save_db();
    exit();
}


?>


{"success": true, 
  m_win: {
  	width: 1000, height: 450,
  	iconCls : 'icon-folder_search-16',
  	title : <?php echo j($title); ?>
  },
  "items":
	{
		xtype: 'treepanel',
		
		stateful: true,
        stateId: 'righe-ordine-order-entry',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
		
		rootVisible: false,
	    loadMask: true,
		
  	    store: Ext.create('Ext.data.TreeStore', {								
					autoLoad:true,
					<!--  loadMask: true,-->	
								        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							timeout: 2400000,
							type: 'ajax',
							reader: {
					            type: 'json',
					            method: 'POST',
					            root: 'children'
					        },
					        actionMethods: {read: 'POST'},
					        
					        extraParams: <?php echo acs_je($m_params); ?>,
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 					        
					        
						},
	        			fields: [
	            			'id', 'task', 'liv', {name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART'
	            			,  'RDUM', 'RDQTA', 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna'
	            			,  'data_selezionata', 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO', 'RDPROG', 'riga'
	            			, 'RTPRZ', 'RDSC1', 'RDSC2', 'RDSC3', 'RDSC4', 'RDMAGG', 'dom_risp', 'RDNRDO'
	            			, 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR', 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE', 'ARTPAR'
	        			]
	    			}),
	    		<?php $cf1 = "<img src=" . img_path("icone/48x48/warning_black.png") . " height=20>"; ?>	
		        columns: [
		        
		        <?php if($m_params->from_righe_info == 'Y'){?>
		               {text: '<?php echo $cf1; ?>',	
		                width: 30, tdCls: 'tdAction',
		                dataIndex: 'dom_risp', 
		        		tooltip: 'Visualizza configurazione',		        			    	     
		    			renderer: function(value, p, record){
		    			  if (record.get('RDPROG') > 0) return '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=18>';
		    		  }},
		        <?php }?>
		        	{
			            xtype: 'treecolumn', 
			            columnId: 'task', 
			            flex: 1,			            
			            dataIndex: 'task',
			            menuDisabled: true, 
			            sortable: false,
			            header: 'Riga/sottoriga'
			        },
					{
		                header   : '<br/>&nbsp;Cons.',
		                dataIndex: 'data_consegna', 
		                width: 60,
		                hidden: true,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('inDataSelezionata') == 0) return ''; //non mostro la data se e' quella selezionata																	
						  					return date_from_AS(value);			    
										}		                
		                 
		             },		
		                     		        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width: 150
		             },
		             {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    			    	}}			    	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		        			dataIndex: 'ARESAU',		
    	     				tooltip: 'Articoli in esaurimento',		        			    	     
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARESAU') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
		    			    	}}			    			             
		             
		             
		             , {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 1              			                
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             }, {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;S',
		                dataIndex: 'RDSTEV', 
		                width    : 20,
		                align: 'right'
		             }, {
		                header   : '<br/>&nbsp;T.R.',
		                dataIndex: 'RDT	PRI', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Pr.',
		                dataIndex: 'RDPRIO', 
		                width    : 30,
		                hidden: true
		             }, {
		                header   : '<br/>&nbsp;St.',
		                dataIndex: 'RDSTAT', 
		                width    : 30,
		                hidden: true
		             }, {
		                header   : '<br/>&nbsp;Carico',
		                dataIndex: 'RDNRCA', 
		                width    : 80,
		                hidden: true,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRCA') == 0) return ''; //non mostro																											
						  					return rec.get('RDTICA') + "_" + rec.get('RDNRCA');			    
										}		                
		             },
		             {
		                header   : '<br/>&nbsp;Prezzo',
		                dataIndex: 'RTPRZ', 
		                width    : 50, align: 'right',
		                hidden : <?php echo $hidden; ?>,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RTPRZ') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}	
		             },	
		             {
		                header   : '<br/>&nbsp;Sc1',
		                dataIndex: 'RDSC1', 
		                width    : 50,
		                hidden : <?php echo $hidden; ?>,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDSC1') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             },
		             {
		                header   : '<br/>&nbsp;Sc2',
		                dataIndex: 'RDSC2', 
		                width    : 50,
		                hidden : <?php echo $hidden; ?>,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDSC2') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             },
		              {
		                header   : '<br/>&nbsp;Sc3',
		                dataIndex: 'RDSC3', 
		                width    : 50,
		                hidden : <?php echo $hidden; ?>,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDSC3') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             }, {
		                header   : '<br/>&nbsp;Sc4',
		                dataIndex: 'RDSC4', 
		                width    : 50,
		                hidden : <?php echo $hidden; ?>,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDSC4') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             },{
		                header   : '<br/>&nbsp;Magg',
		                dataIndex: 'RDMAGG', 
		                width    : 50,
		                hidden : <?php echo $hidden; ?>,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDMAGG') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             },
		             
		              <?php if($m_params->from_righe_info != 'Y'){?>
		               {text: '<?php echo $cf1; ?>',	
		                width: 30, tdCls: 'tdAction',
		                dataIndex: 'dom_risp', 
		        		tooltip: 'Visualizza domande/risposte',		        			    	     
		    			renderer: function(value, p, record){
		    			  if (record.get('RDPROG') > 0) return '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=18>';
		    		  }},
		        <?php }?>
		             
		             
				/*<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             , {
		                header   : '<br/>&nbsp;Commessa',
		                dataIndex: 'COMMESSA', 
		                width    : 80
		             }				
		        <?php } else { ?>
		             , {
		                header   : '<br/>&nbsp;Lotto',
		                dataIndex: 'RDNRLO', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRLO') == 0) return ''; //non mostro																											
						  					return rec.get('RDTPLO') + "_" + rec.get('RDNRLO');			    
										}
		             }		        
		        <?php } ?> */
		        
		         ]	    					
		
		, listeners: {
 					     beforeload: function(store, options) {
 					     var t_win = this.up('window');
 					     console.log(t_win);
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },

                        load: function () {
                         Ext.getBody().unmask(); 
                        },  
		
				
	 			afterrender: function (comp) {
					comp.up('window').setTitle('<?php echo "Righe ordine {$oe['TDOADO']}_{$oe['TDONDO']}_{$ord['TDOTPD']}"; ?>');	 				
	 			}
	 			
	 	//	 , beforeload: function(store, options) { Ext.getBody().mask('Loading... ', 'loading').show(); }
	    //     , load: function () { Ext.getBody().unmask(); }
	         		  
	 			
	 			
	 		 ,	itemclick: {								
						  fn: function(iView,rec,item,index,eventObj){
						  //col_name = iView.getGridColumns()[index].dataIndex;
						
					  		if(rec.get('liv') == 'liv_0')
								acs_show_win_std('Configurazione ordine ' +'<?php echo "{$oe['TDOADO']}_{$oe['TDONDO']}"; ?>'  + ' riga ' +rec.get('task') , '../desk_vend/acs_get_domande_risposte_riga.php?fn=open_form', {k_ordine: '<?php echo $k_ordine; ?>', prog: rec.get('RDPROG'), from_righe_info : '<?php echo $m_params->from_righe_info; ?>'}, 900, 450, null, 'icon-folder_search-16');          		
					  		else
					  			acs_show_win_std('Configurazione ordine ' +'<?php echo "{$oe['TDOADO']}_{$oe['TDONDO']}"; ?>'  + ' riga ' +rec.get('riga') + ' sottoriga ' +rec.get('task'), '../desk_vend/acs_get_domande_risposte_riga.php?fn=open_form', {k_ordine: '<?php echo $k_ordine; ?>', prog: rec.get('RDPROG'), from_righe_info : '<?php echo $m_params->from_righe_info; ?>'}, 900, 450, null, 'icon-folder_search-16');          		
					  	
						  
						  }
					  }
	 		
				  
				  
				  
				 , itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      var record = grid.getStore().getAt(index);				      		 
				      var voci_menu = [];
				      var t_win = this.up('window');
				      
				      if(rec.get('liv') == 'liv_0' && rec.get('RDQTA') > 0){
				
    				     voci_menu.push({
    				         		text: 'Visualizza distinta componenti',
    				        		iconCls : 'icon-folder_search-16',          		
    				        		handler: function() {
    				        			
				        		 		var my_listeners = {
						            	    onCompoSelected: function(from_win, row_selected){
    					        				t_win.fireEvent('onCompoSelected', t_win, row_selected);
    					        				from_win.close();
				        					}       		
    									};

    				        			acs_show_win_std('Distinta componenti', '../desk_vend/acs_get_riga_componenti.php', {k_ordine: '<?php echo $k_ordine; ?>', nrec: rec.get('RDNREC'), d_art: rec.get('RDDART'), c_art : rec.get('RDART'), riga : rec.get('RDRIGA'), from_anag_art : '<?php echo $m_params->from_anag_art; ?>'}, 1300, 450, my_listeners, 'icon-folder_search-16');          		
    				        		}
    				    		});
				    		
				    		
				    		
				    		<?php if ($m_params->panel_from == 'order_entry'){ ?>
				    			
				    			if (rec.get('ARTPAR').trim() == 'C' 
				    			
				    			<?php if(isset($cfg_mod_Spedizioni['oe_abilita_elimina_riga_su_articolo'])){?>
				    		    <?php echo "|| ". acs_je($cfg_mod_Spedizioni['oe_abilita_elimina_riga_su_articolo']) ?>.includes(rec.get('RDART').trim())
				    		    <?php }?>
				    			
				    			 )
            				     		voci_menu.push({
            				         		text: 'Elimina riga',
            				        		iconCls : 'icon-sub_red_delete-16',          		
            				        		handler: function() {
            				        								Ext.Msg.confirm('Richiesta conferma', 'Confermi eliminazione?', function(btn, text){																							    
																		   if (btn == 'yes'){																	         	
																         	
																         	Ext.getBody().mask('Waiting', 'loading').show();
																			Ext.Ajax.request({
																			   url        : 'acs_righe_ordine_marketing.php?fn=exe_cancella_riga',
																			   method: 'POST',
																			   jsonData: {
																			   		row: {
																			   			k_ordine: <?php echo j($k_ordine); ?>,
																			   			nrec: rec.get('RDNREC'), 
																			   			d_art: rec.get('RDDART'), 
																			   			c_art : rec.get('RDART')
																			   		}
																			   }, 
																			   
																			   success: function(response, opts) {
																			   	  Ext.getBody().unmask();
																				  grid.getStore().treeStore.load();
																			   }, 
																			   
																			   failure: function(response, opts) {
																			      Ext.getBody().unmask();
																			      alert('error in get image');
																			   }
																			});						         	
																         	
																			}
																	});
            				        		 		    		
            				        		} //handler
            				    		});
				    		
				    		
				    		<?php } ?>
				    		
				    		} //if liv_0
			
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
				      
				 } //itemcontextmenu
	 			 			
			}
			
		, viewConfig: {
		
		        getRowClass: function(rec, index) {
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';	
		        	if (rec.get('liv') == 'liv_1') 	return 'rigaNonSelezionata'; //SOTTORIGA GRIGIA        	
		         }   
		    }												    
			
		         
	}
}
