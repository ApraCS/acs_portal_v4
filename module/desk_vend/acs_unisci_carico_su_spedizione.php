<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$m_params = acs_m_params_json_decode();


switch ($m_params->tipo_elenco){
	case 'GATE':
		$f_data = 'TDDTSP';
		break;
}


//***********************************************************************
// ELENCO SPEDIZIONI/CARICHI
//***********************************************************************
if ($_REQUEST['fn'] == 'exe_unisci_carico'){
	
	$sped_from 			= $m_params->from_sped_id;
	$sped_to 			= $m_params->to_sped_id;
	$carico_ar_from		= $s->k_carico_td_decode($m_params->from_k_carico);

	$sped               = $s->get_spedizione($sped_to);
	$sped_from_row      = $s->get_spedizione($sped_from);	

	//recupero l'elenco degli ordini da modificare
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} TD
							WHERE " . $s->get_where_std() . "
							AND TDNBOC = " . $sped_from . "
							AND TDTPCA = " . sql_t($carico_ar_from['TDTPCA']) . "
							AND TDAACA = " . $carico_ar_from['TDAACA'] . "
							AND TDNRCA = " . $carico_ar_from['TDNRCA'] . "
							";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while ($row_test = db2_fetch_assoc($stmt)){
		$k_ord = $row_test['TDDOCU'];
		$ord = $s->get_ordine_by_k_docu($k_ord);
		
		//Mantieni allineato TDNBOC|TDNBOF
		if ($ord['TDNBOC'] == $ord['TDNBOF'])
		  $allinea_TDNBOF = $sped_to;
		else
		  $allinea_TDNBOF = $ord['TDNBOF'];   //lascio l'attuale;

		//update
		$sqlU = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDNBOC = ?, TDNBOF=? WHERE TDDOCU = ?";
		$stmtU = db2_prepare($conn, $sqlU);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmtU, array($sped_to, $allinea_TDNBOF, $k_ord));
		
		//devo prima allineare i dati di testata per essere sicuro che venga spostato dal trigger sulla sped selezionata
		$s->exe_allinea_dati_spedizione_su_testata($sped, $k_ord);

		$sh = new SpedHistory();
		$sh->crea(
				'assegna_spedizione',
				array(
						"k_ordine" 	=> $k_ord,
						"sped"		=> $sped,
						"op"		=> "ASS",
						"ex"		=> "(UNISCI CARICO: #{$ord['TDNBOC']})",
						"seca"		=> $ord['TDSECA'] //attualmente va passata altrimenti vienere resettata
				)
		);

	}

	//accodo la descrizione della spded_from
	$s->accoda_descrizione_sped($sped, $sped_from_row);
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	//costruzione sql
	$sql = "SELECT TDNBOC, COUNT(DISTINCT TDDCON) AS C_CLIENTI, TDCCON, TDDCON, CSDESC, TDDLOC, TDNAZD, TDPROD, TDDNAD
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
					ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
				WHERE " . $s->get_where_std() . "
					  AND TDCITI = ? AND {$f_data} = ?
					  AND TDNBOC <> {$m_params->sped_id}
					  GROUP BY TDNBOC, TDCCON, TDDCON, CSDESC, TDDLOC, TDNAZD, TDPROD, TDDNAD
					  ORDER BY TDNBOC, TDDCON
					";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->cod_iti, $m_params->data));

	$ar = array();
	$ar_tot = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['carico'] = $s->get_el_carichi_by_sped($r['TDNBOC'], 'N', 'N');
		$r['loca'] = $s->scrivi_rif_destinazione($r['TDDLOC'], $r['TDNAZD'], $r['TDPROD'], $r['TDDNAD']);
		$r['CSDESC'] = acs_u8e($r['CSDESC']);
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}







// ******************************************************************************************
// GRID
// ******************************************************************************************

//verifico che tutti gli ordini della spedizione/carico siano negli richiesti
//recupero l'elenco degli ordini da modificare

$sped_id    = $m_params->sped_id;
$carico_ar	= $s->k_carico_td_decode($m_params->k_carico);

$sql = "SELECT COUNT(*) AS R_ST FROM {$cfg_mod_Spedizioni['file_testate']} TD
		WHERE " . $s->get_where_std() . "
		AND TDNBOC = " . $sped_id . "
		AND TDTPCA = " . sql_t($carico_ar['TDTPCA']) . "
		AND TDAACA = " . $carico_ar['TDAACA'] . "
		AND TDNRCA = " . $carico_ar['TDNRCA'] . "
		AND TDSTAT NOT IN ('RI', 'PR')
							";


$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);

$row = db2_fetch_assoc($stmt);
$stati_non_abilitati = $row['R_ST'];

if ($stati_non_abilitati > 0) { ?>
{"success":true, "items": [
        {
            html: 'ordini con stati non abilitati'
        }
]}	
<?php } else { ?>
{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Gate <?php echo print_date($m_params->data); ?>, Itinerario <?php echo $s->decod_std('ITIN', $m_params->cod_iti); ?>',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			            
            
            items: [					 	
					{
						xtype: 'grid',
						itemId: 'tabSpedizioni',
						loadMask: true,
						flex: 1,
						 features: [
							{
							ftype: 'filters',
							encode: false, 
							local: true,   
						    filters: [
						       {
						 	type: 'boolean',
							dataIndex: 'visible'
						     }
						      ]
						}],
						selModel: {selType: 'checkboxmodel'},
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
								, doRequest: personalizza_extraParams_to_jsonData
								, extraParams: <?php echo acs_je($m_params); ?> 
							},
								
							fields: ['TDNBOC', 'TDCCON', 'TDDCON', 'carico', 'CSDESC', 'loca', 'TDSTAT']
										
										
						}, //store
						multiSelect: false,
						
					
						columns: [
							{header: 'Cliente', 		dataIndex: 'TDCCON', width: 80,  filter: {type: 'string'}, filterable: true},						
							{header: 'Denominazione', 	dataIndex: 'TDDCON', flex: 1,  filter: {type: 'string'}, filterable: true},
							{header: 'Carico', 			dataIndex: 'carico', width: 80,  filter: {type: 'string'}, filterable: true},
							{header: 'Localit&agrave;', 		dataIndex: 'loca', flex:1,  filter: {type: 'string'}, filterable: true},
							{header: 'Sped.', 			dataIndex: 'TDNBOC', width: 60,  filter: {type: 'string'}, filterable: true},							
							{header: 'Note spedizione', 		dataIndex: 'CSDESC', flex: 1.2,  filter: {type: 'string'}, filterable: true},
						]
						 
					}	
						 
						 
				],
			buttons: [{
	            text: 'Conferma',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {

				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tabSpedizioni');
				var loc_win = this.up('window');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection();   				
	   			selected_sped_id = id_selected[0].data.TDNBOC;


				 Ext.Ajax.request({
			        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_unisci_carico',
			        timeout: 2400000,
			        method     : 'POST',
        			jsonData: {
        			    from_sped_id: <?php echo $m_params->sped_id; ?>,
        			    from_k_carico: <?php echo j($m_params->k_carico); ?>,
        			    to_sped_id: selected_sped_id
					},							        
			        success : function(result, request){
			    		//m_grid.getStore().load();
			    		loc_win.fireEvent('onSelected', loc_win); 			            			
            		},
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			    });	
				
	            } //handler
	        }
	        ],             
				
        }
]}
<?php } ?>