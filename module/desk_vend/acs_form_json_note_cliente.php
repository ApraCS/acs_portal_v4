<?php
require_once "../../config.inc.php";

$s = new Spedizioni();

$dt		= trim($_REQUEST['tddt']);
$cli 	= trim($_REQUEST['tdccon']);
$des 	= trim($_REQUEST['tdcdes']);


function m_append_to_title($txt){
	return j(" [" . $txt . "]");
}

function m_out_cli_des($dt, $cli, $des){
	global $s;
	$ret = implode("_", array($cli, $des));
	$ret .= " - " . $s->get_des_cliente($dt, $cli);
	return $ret;
}



// ******************************************************************************************
// UPDATE NOTA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'upd'){
	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());	
	$k_cli_des = trim($all_params['k_cli_des']);
	$k_cli_des_ar = explode("_", $k_cli_des);
	$dt 	= trim($k_cli_des_ar[0]);
	$cli 	= trim($k_cli_des_ar[1]);
	$des 	= trim($k_cli_des_ar[2]);
	
	$s->update_note_cliente_portal($dt, $cli, '', utf8_decode(trim($all_params['txt_memo_cli'])));

	if (strlen(trim($des)) > 0)	
		$s->update_note_cliente_portal($dt, $cli, $des, utf8_decode($all_params['txt_memo_des']));		
	
	if ((int)$all_params['sped_id'] > 0)
		$s->update_note_cliente_portal($dt, implode('_', array(trim($all_params['k_cli_des']), $all_params['sped_id'])), '', utf8_decode($all_params['txt_memo_spe']));
	

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}
?>
<?php
// ******************************************************************************************
// VISUALIZZAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'view'){
	$stmt_note_gest = $s->get_note_cliente_gest($dt, $cli);
	$txt_ar = array();
	while ($row = db2_fetch_assoc($stmt_note_gest))
		$txt_ar[] = trim($row['RDDES1']);		

	$txt_note_gest = implode("\n", $txt_ar);
	
	$txt_note_cli = $s->get_note_cliente_portal($dt, $cli, '');
	if (strlen($des) > 0)
		$txt_note_des = $s->get_note_cliente_portal($dt, $cli, $des);	

	$txt_spe = $s->get_note_cliente_portal($dt, implode('_', array(sprintf("%-2s", $dt), $cli, trim($des), $_REQUEST['tdnboc'])), '');
	
	
	$txt_acs = "";
	if (strlen(trim($txt_note_cli)) > 0)
		$txt_acs .= "[ NOTE CLIENTE ] " . "\n" . $txt_note_cli;
	
	if (strlen(trim($txt_note_des)) > 0){
		if ($txt_acs != "") $txt_acs .= "\n\n";
		$txt_acs .= "[ NOTE DESTINAZIONE ] " . "\n" . $txt_note_des;
	}	
	
?>
{"success":true, "items": [

  {
   xtype: 'container',
	layout: {
	    type: 'vbox',
	    align: 'stretch',
	    pack : 'start'
	},
   items: [
   
 
	   {
            xtype: 'textareafield',
            grow: true,
            name: 'message',
            fieldLabel: 'Segnalazioni per cliente/destinazione',
            flex: 1,
            labelAlign: 'top', labelStyle: 'font-weight:bold;',
            value: <?php echo j($txt_acs); ?>
        }
        
      <?php if ((int)$_REQUEST['tdnboc'] > 0) {?>
	   , {
            xtype: 'textareafield',
            grow: true,
            name: 'message',
            fieldLabel: 'Segnalazioni per cliente/destinazione/SPEDIZIONE',
            flex: 1,
            labelAlign: 'top', labelStyle: 'font-weight:bold;',
            value: <?php echo j($txt_spe); ?>
        }
      
      <?php } ?>  
        
        
        , {
            xtype: 'textareafield',
            grow: true,
            name: 'message',
            fieldLabel: 'Segnalazioni da anagrafica gestionale',
            flex: 1,
            labelAlign: 'top', labelStyle: 'font-weight:bold;',
            value: <?php echo j($txt_note_gest); ?>
        }
        
   
   ]
   
	, listeners: {		
 			afterrender: function (comp) {
 				comp.up('window').setTitle(comp.up('window').title + <?php echo m_append_to_title(m_out_cli_des($dt, $cli, $des)); ?>);
 			}
 	}		 	   
   
  }


]}
<?php exit; } ?>
<?php
// ******************************************************************************************
// GESTIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'gest'){

	$m_params = acs_m_params_json_decode();
	$k_cli_des = $m_params->k_cli_des;
	$k_cli_des_ar = explode("_", $k_cli_des);
	$dt 	= trim($k_cli_des_ar[0]);
	$cli 	= trim($k_cli_des_ar[1]);
	$des 	= trim($k_cli_des_ar[2]);

	$txt_note_cli 	= $s->get_note_cliente_portal($dt, $cli, '');
	$txt_note_dest 	= $s->get_note_cliente_portal($dt, $cli, $des);	
	$txt_note_spe 	= $s->get_note_cliente_portal($dt, implode('_', array(trim($m_params->k_cli_des), $m_params->sped_id)), '');
?>
{"success":true, "items": [

  {
   xtype: 'form',
   frame: false,   
   layout: {
	    type: 'vbox',
	    align: 'stretch',
	    pack : 'start'
	},
   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=upd',
   buttons: [
				{
		            text: 'Salva',
			        iconCls: 'icon-save-24',		            
			        scale: 'medium',		            
		            handler: function() {
		            	m_win = this.up('window'); 
		                this.up('form').submit({
				                            success: function(form,action) {
		                            			m_win.close();
				                            },
				                            failure: function(form,action){
				                                Ext.MessageBox.alert('Erro');
				                            }
				                        });
		                
		            }
		        }    
   ],   	
   items: [
       {xtype: 'hiddenfield', name: 'k_cli_des', value: <?php echo j($k_cli_des); ?>},
	   {xtype: 'hiddenfield', name: 'sped_id', value: <?php echo j($m_params->sped_id); ?>},       
	   {
            xtype: 'textareafield',
            grow: true,
            name: 'txt_memo_cli',
            fieldLabel: 'Segnalazioni per cliente, archiviate nel desktop vendite',
            flex: 1,
            labelAlign: 'top',
            value: <?php echo j($txt_note_cli); ?>
        }, {
            xtype: 'textareafield',
            grow: true,
            name: 'txt_memo_des',
            fieldLabel: 'Segnalazioni per cliente/destinazione, archiviate nel desktop vendite',
            flex: 1,
            labelAlign: 'top',
            value: <?php echo j($txt_note_dest); ?>
            <?php if (strlen(trim($des)) == 0) echo ", hidden: true"; ?>
        }
        
       <?php if (strlen($m_params->sped_id) > 0){ ?>        
        , {
            xtype: 'textareafield',
            grow: true,
            name: 'txt_memo_spe',
            fieldLabel: 'Segnalazioni per cliente/destinazione/SPEDIZIONE, archiviate nel desktop vendite',
            flex: 1,
            labelAlign: 'top',
            value: <?php echo j($txt_note_spe); ?>

        }
      <?php } ?>
   ]

	, listeners: {		
 			afterrender: function (comp) {
 				comp.up('window').setTitle(comp.up('window').title + <?php echo m_append_to_title(m_out_cli_des($dt, $cli, $des)); ?>);
 			}
 	}   
   
  }


]}
<?php exit; } ?>