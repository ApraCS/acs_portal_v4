<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// EDIT EVENT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_blocca_ordini'){
	foreach ($m_params->list_selected_id as $k_ordine){
		
		$sh = new SpedHistory();
		$sh->crea(
				'blocca_ordine',
				array(
			
						"k_ordine"		=> $k_ordine, //il tddocu
						"f_note"		=> $m_params->form_values->f_note
				)
		);
	}
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// EDIT EVENT
// ******************************************************************************************

//recupero l'elenco degli ordini interessati
$list_selected_id_encoded = acs_je($m_params->list_selected_id);

?>
{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: true,
	title: '',
	url: 'acs_op_exe.php',

	defaults:{ anchor: '-10' , labelWidth: 130 },

	items: [{
			name: 'f_note',						
			xtype: 'textfield',
			fieldLabel: 'Note',
		    maxLength: 100							
		}
					
					],
			buttons: [{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window')

					if(form.isValid()){
						Ext.Ajax.request({
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_blocca_ordini',
						        jsonData: {
						        	form_values: form.getValues(),
						        	list_selected_id: <?php echo $list_selected_id_encoded; ?>
						        	
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){	            	  													        
						            var jsonData = Ext.decode(result.responseText);
						            loc_win.fireEvent('onClose', loc_win);	
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						            console.log('errorrrrr');
						        }
						    });	    
				    }            	                	                
	            }
	        }],             
				
        }
]}