<?php

require_once "../../config.inc.php";

ini_set('max_execution_time', 3000);

$s = new Spedizioni();
 
//funcioni generiche
switch($_REQUEST['fn']) {
	case "crt_spedizione":
		$sped_id = $s->crt_spedizione($_REQUEST);
		$_REQUEST['sped_id'] = $sped_id;
		$result = $s->upd_spedizione($_REQUEST);		
		//echo "{success: true}";
		echo acs_je($result);
		exit();
	break;
	case "upd_spedizione":
		$result = $s->upd_spedizione($_REQUEST);
		$appLog->save_db();		
		//echo "{success: true}";
		echo acs_je($result);		
		exit();
	break;	
	case "upd_carico":
		$result = $s->upd_carico($_REQUEST);
		echo "{success: true}";
		exit();
	break;	
	case "assegna_spedizione":
		$appLog->add_bp("ASS_SPED START");
		$result = $s->exe_assegna_spedizione(acs_m_params_json_decode());
		$appLog->add_bp("ASS_SPED END");		
		$appLog->save_db();		
		echo acs_je($result);	
		exit();
	break;	
	case "toggle_data_confermata": 
		$result = $s->exe_toggle_data_conferma(acs_m_params_json_decode());		
		echo "{success: true}";
		exit();
		break;
	case "riallinea_data_confermata":
		$result = $s->exe_riallinea_data_conferma(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;			
	case "crt_promemoria": 
		$result = $s->exe_crt_promemoria($_REQUEST);		
		echo "{success: true}";
		exit();
		break;		
	case "registra_sequenza_scarico": 
		$result = $s->exe_registra_sequenza_scarico($_REQUEST);		
		echo "{success: true}";
		exit();
		break;
	case "progetta_spedizioni":
		$result = $s->exe_progetta_spedizioni(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;
	case "sincronizza_spedizione":
		$result = $s->exe_sincronizza_spedizione(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;
	case "sincronizza_ordine":
		$result = $s->exe_sincronizza_ordine(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;
	case "sincronizza_hold":
		$result = $s->exe_sincronizza_hold(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;					
	case "progetta_spedizioni_applica_sequenza":
		$result = $s->exe_progetta_spedizioni_applica_sequenza(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
	break;
	case "progetta_spedizioni_applica_sequenza_percorso":
		$result = $s->exe_progetta_spedizioni_applica_sequenza_percorso(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;	
	case "progetta_spedizioni_memorizza_coordinate":
		$result = $s->exe_progetta_spedizioni_memorizza_coordinate(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;						
	case "assegna_carico": 
		$result = $s->exe_assegna_carico($_REQUEST);		
		echo $result;
		exit();
		break;
	case "assegna_progressivo_carico":
		$result = $s->exe_assegna_progressivo_carico(acs_m_params_json_decode());
		echo $result;
		exit();
		break;		
	case "upd_commento_ordine": 
		$result = $s->exe_upd_commento_ordine($_REQUEST);		
		echo "{success: true}";
		exit();
		break;
	case "exe_ricalcolo_costo_sped": 
		$result = $s->exe_ricalcolo_costo_sped($_REQUEST);		
		echo $result;
		exit();
		break;
	case "exe_ricalcolo_costo_globale":
		$result = $s->exe_ricalcolo_costo_globale(acs_m_params_json_decode());
		echo $result;
		exit();
		break;				
	case "exe_send_email": 
		include "send_email.php";
		exit();
		break;	
	case "exe_prima_data_promettibile":
		$result = $s->exe_prima_data_promettibile($_REQUEST);		
		echo $result;
		exit();
		break;
	case "exe_modifica_dati_destinazione":
		$result = $s->exe_modifica_dati_destinazione($_REQUEST);
		echo $result;
		exit();
		break;
	case "exe_crea_dati_destinazione":
		$result = $s->exe_crea_dati_destinazione($_REQUEST);
		echo $result;
		exit();
		break;				
	case "exe_assegna_scarico_intermedio":
		$result = $s->exe_assegna_scarico_intermedio(acs_m_params_json_decode());
		echo $result;
		exit();
		break;
	case "exe_assegna_destinazione_sosta_tecnica":
		$result = $s->exe_assegna_destinazione_sosta_tecnica(acs_m_params_json_decode());
		echo $result;
		exit();
		break;						
	case "exe_assegna_destinazione_finale":
		$result = $s->exe_assegna_destinazione_finale(acs_m_params_json_decode());
		echo $result;
		exit();
		break;		
	case "crea_segnalazione_arrivi":
		$result = $s->exe_crea_segnalazione_arrivi(acs_m_params_json_decode());
		echo $result;
		exit();
		break;		
	case "exe_call_pgm":
		$result = $s->exe_call_pgm($_REQUEST);
		echo $result;
		exit();
		break;		
	case "exe_crea_segnalazione_arrivi":
	    
		$result = $s->exe_crea_segnalazione_arrivi($_REQUEST);
		echo $result;
		exit();
		break;
	case "exe_crea_segnalazione_arrivi_json":
		$result = $s->exe_crea_segnalazione_arrivi_json(acs_m_params_json_decode());
		echo $result;
		exit();
		break;				
	case "exe_avanzamento_segnalazione_arrivi":
		$result = $s->exe_avanzamento_segnalazione_arrivi($_REQUEST);
		echo $result;
		exit();
		break;
	case "exe_avanzamento_segnalazione_arrivi_json":
		$result = $s->exe_avanzamento_segnalazione_arrivi_json(acs_m_params_json_decode());
		echo $result;
		exit();
		break;		
	case "exe_create_RIPRO":
		$result = $s->exe_create_RIPRO($_REQUEST);
		echo $result;
		exit();
		break;				
	case "upd_coordinate":
		$result = $s->exe_upd_coordinate($_REQUEST);
		echo $result;
		exit();
		break;		
	case "allinea_limiti_mezzo_su_itve":
		$result = $s->exe_allinea_limiti_mezzo_su_itve(acs_m_params_json_decode());
		echo $result;
		exit();
		break;
	case "exe_protocollazione":
		$pt = new SpedProtocollazione();
		$result = $pt->exe_protocollazione(acs_m_params_json_decode());
		echo $result;
		exit();
		break;				
	case "forza_invio_conferma_ordine":
		$result = $s->exe_forza_invio_conferma_ordine(acs_m_params_json_decode());
		echo $result;
		exit();
		break;		
	case "evidenza_generica":
	    $deskPven = new DeskPVen(array('no_verify' => 'Y'));
	    $result = $deskPven->exe_evidenza_generica(acs_m_params_json_decode());
	    echo $result;
	    exit();
	    break;
}

?>