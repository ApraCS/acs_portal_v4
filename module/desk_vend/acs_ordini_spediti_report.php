<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");


	$ar_email_json = acs_je($ar_email_to);
	
	
if ($_REQUEST['fn'] == 'get_json_data_nazione'){
	
		global $cfg_mod_Spedizioni;
		$s = new Spedizioni();
	
		$ret = array();

	
		$sql = "SELECT DISTINCT TDNAZD, TDDNAD
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD " .  $s->add_riservatezza() . " 
 			WHERE  " . $s->get_where_std() . "			
 			ORDER BY TDDNAD";
	
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
	
	
		while ($row = db2_fetch_assoc($stmt)){
				
			$ret[] = array("id"=>trim($row['TDNAZD']), "text" =>trim($row['TDDNAD']));
		}
	
		echo acs_je($ret);
		exit();
	}
	
	
	
	
	
	
	
	
	

if ($_REQUEST['fn'] == 'open_parameters'){
	

		?>

{"success" : true, "items":
	
		{
	        xtype: 'form',
	        layout: 'form',
	        collapsible: false,
	        id: 'my_form',
	        url: '<?php echo $_SERVER['PHP_SELF']; ?>',
	        frame: false,
	        bodyPadding: '10 10 5 10',
	       // width: 300,
	        fieldDefaults: {
	            msgTarget: 'side',
	            labelWidth: 75,
	    		format: 'd/m/Y',
	    		submitFormat: 'Ymd'	            
	        },
	        defaultType: 'datefield',
	        items: [{
					xtype: 'fieldset',
	                title: 'Seleziona data',
	                defaultType: 'textfield',
	                layout: 'anchor',
	               /* defaults: {
	                    anchor: '100%'
	                },*/
	             	items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data DDT da'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-5'
						   , margin: "5 5 10 5"
			
						},{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data DDT a'
						   , name: 'f_data_a'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-5'
						   , margin: "5 5 5 5"
						}
						
			
					]
	            }, {
							flex: 1,							
							name: 'f_nazione',
							xtype: 'combo',
							fieldLabel: 'Nazione',
							anchor: '-15',
							margin: "20 10 20 10" ,
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,
						   	store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_nazione',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}       		
							},
							
							fields: [{name:'id'}, {name:'text'}],
							}					 
							},{
						flex: 1,
						name: 'f_agente',
						xtype: 'combo',
						fieldLabel: 'Agente',
						margin: "20 10 20 10" ,
						anchor: '-15',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection:true,
					   	allowBlank: true,													
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json($s->get_options_agenti('Y'), '') ?> 	
							    ] 
						}						  
					},{
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            anchor: '-15',
			            style: 'padding-top: 2px; padding-bottom: 2px',
						border: false,			            			            
			          
			            items: [{
			                boxLabel: 'Volume',
			                name: 'f_tipo',
			                checked: true,			                
			                inputValue: 'V',
			                width: 95
			            }, {
			                boxLabel: 'Importo',
			                checked: false,			                
			                name: 'f_tipo',
			                inputValue: 'I'
			                
			            } ]
			        }],

	        buttons: [{
		            text: 'Report',
		            iconCls: 'icon-print-32',
		            scale: 'large',	                     
		            handler: function() {
		                form = this.up('form').getForm();
		                
		                if (form.isValid()){	                	                
			                form.submit({
					                standardSubmit: true,
			                        method: 'POST',
									target : '_blank'		                        
			                });
		                }	                
		            }
		        }
	        ]
	    }
	
		
	}



<?php 
	exit;
	}
?>

<html>
<head>
	<title>RIEPILOGO ORDINI SPEDITI </title>
	<!-- <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/report.css"); ?> />  -->
	
	<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

	<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
 	<script type="text/javascript" src="../../../extjs/ext-all.js"></script>
	<script src=<?php echo acs_url("js/acs_js.js") ?>></script>  
	
	<style type="text/css">
	
	table{border-collapse:collapse; width: 100%;}
	table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
	
	.acs_report{font-family: "Times New Roman",Georgia,Serif;}
	
	tr.mese {background-color: #F4DB60; font-weight: bold;}
	tr.liv1 {background-color: #AAAAAA;}
	tr.liv2 {background-color: #cccccc;}   
	tr.liv3 {background-color: #DDDDDD;}  
	tr.imp td{font-weight: bold; text-align:center;}  


    tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
	tr.ag_liv2 td{background-color: #DDDDDD;}
	tr.ag_liv1 td{background-color: #ffffff;}
	tr.ag_liv0 td{font-weight: bold;}
	
	
	tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
	tr.ag_liv_data th{background-color: #333333; color: white;}
	
	.titoli{background-color: #333333; color: white;}
	.weekend{background-color: #EDE5AD; text-align:center;}
	.week{background-color: #474747; color: white; text-align:center;}
	
	.vuoto{background-color: white;}
	
	.grassetto{font-weight: bold;}   
	.number{text-align: right;}
	
	h2.acs_report{background-color: black; color: white; padding: 5px; font-size: 20px;}
	h2.acs_report_title{font-size: 20px; padding: 10px;}
	
	@media print
	{
		.noPrint{display:none;}
	}
	
	
	</style>
	</head>
<body>


	<div class="page-utility noPrint">
		<?php 
		
					$bt_fascetta_print = 'Y';
					$bt_fascetta_email = 'Y';
					$bt_fascetta_excel = 'Y';
					$bt_fascetta_close = 'Y';
					include  "../../templates/bottoni_fascetta.php";
		
		?>	
	</div>
	
	<div id='my_content'>
		
		<h2 class="acs_report_title">RIEPILOGO ORDINI SPEDITI PER DATA DOCUMENTO DI TRASPORTO</h2>
		<p align="right"> Data elaborazione:  <?php echo date('d/m/Y  H:i');?></p>
		
		<?php  	
		
		function sum_columns_value(&$ar, $r){
			
			$ar['VALORE'] += $r['VALORE'];
			setlocale(LC_TIME, 'it_IT');
			$gg = acs_u8e(strftime("%d", strtotime($r['TDDTRE'])));
			$ar['GG'][$gg] += $r['VALORE'];
			
				
			
			return $ar;
		}

	
		/**************** INTERROGAZIONE DB E CHECK FILTRI   *********************************************************************************/
			
		$m_where.= " WHERE " . $s->get_where_std();
		
		$m_where .= sql_where_by_combo_value("TDCAG1", $_REQUEST['f_agente']);
		
		$m_where .= sql_where_by_combo_value("TDNAZD", $_REQUEST['f_nazione']);
			
			if (strlen($_REQUEST['f_data']) > 0)
				$m_where .= " AND TDDTRE >= {$_REQUEST['f_data']}";
			if (strlen($_REQUEST['f_data_a']) > 0)
				$m_where .= " AND TDDTRE <= {$_REQUEST['f_data_a']}";
			
		   	if ($_REQUEST['f_tipo']== 'V'){
		
		   		$valore= " SUM(TDVOLU) AS VALORE";
		   		$titolo= "Volume";
			}else{
				
				$valore= " SUM(TDTIMP) AS VALORE";
				$titolo= "Importo";
			}
			
			
				
			
			$sql = "SELECT TDDTRE, TDNAZD, TDDNAD, TDCAG1, TDDAG1, 
			TDPROD, TDCCON, TDDCON, TDDLOC, $valore
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			$m_where 
			GROUP BY TDDTRE, TDNAZD, TDDNAD, TDCAG1, TDDAG1, TDPROD, TDCCON, TDDCON, TDDLOC
			ORDER BY TDDTRE DESC, TDDNAD, TDDAG1, TDPROD, TDDCON";
			
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			
			
			
		/**************** CREAZIONE ALBERO   *********************************************************************************/		
			$ar = array();
			
			while ($r = db2_fetch_assoc($stmt)){
				
				$tmp_ar_id = array();
				$ar_r= &$ar;
				
				setlocale(LC_TIME, 'it_IT');
				$cod_liv0= acs_u8e(strftime("%Y%m", strtotime($r['TDDTRE'])));   //annomese
				$cod_liv1 = trim($r['TDNAZD']);  //NAZIONE
				$cod_liv2 = trim($r['TDCAG1']);  //AGENTE
				$cod_liv3 = trim($r['TDPROD']);  //PROVINCIA
				$cod_liv4 = trim($r['TDCCON']);  //COD CLIENTE
				
				
				//MESE_ANNO
				$liv =$cod_liv0;
				$tmp_ar_id[] = $liv;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['task'] = $liv;
					$ar_new['desc'] = acs_u8e(ucfirst(strftime("%B %Y", strtotime($r['TDDTRE']))));
					$ar_new['mese'] = strtotime($r['TDDTRE']);
					$ar_new['day'] = acs_u8e(ucfirst(strftime("%a", strtotime($r['TDDTRE']))));
					$ar_r["{$liv}"] = $ar_new;
				}
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $r);
				
		
				//NAZIONE
				$liv =$cod_liv1;
				$ar_r = &$ar_r['children'];
				$tmp_ar_id[] = $liv;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['task'] = $liv;
					$ar_new['desc'] = trim($r['TDDNAD']);
					$ar_r["{$liv}"] = $ar_new;
				}
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $r);
				
				//AGENTE
				$liv=$cod_liv2;
				$ar_r = &$ar_r['children'];
				$tmp_ar_id[] = $liv;
				if(!isset($ar_r[$liv])){
					$ar_new= array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['task'] = $liv;
					$ar_new['desc'] = trim($r['TDDAG1']);
					$ar_r[$liv]=$ar_new;
				}
				
				$ar_r=&$ar_r[$liv];
				sum_columns_value($ar_r, $r);
				
				//PROVINCIA
				$liv=$cod_liv3;
				$ar_r = &$ar_r['children'];
				$tmp_ar_id[] = $liv;
				if(!isset($ar_r[$liv])){
					$ar_new= array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['task'] = $liv;
					$ar_new['desc'] = "[".trim($r['TDPROD']). "] ". trim($r['TDDLOC']);
					$ar_r[$liv]=$ar_new;
				}
				
				$ar_r=&$ar_r[$liv];
				sum_columns_value($ar_r, $r);
				
				//CLIENTE
				$liv=$cod_liv4;
				$ar_r = &$ar_r['children'];
				$tmp_ar_id[] = $liv;
				if(!isset($ar_r[$liv])){
					$ar_new= array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['task'] = $liv;
					$ar_new['desc'] = trim($r['TDDCON']);
					$ar_r[$liv]=$ar_new;
				}
				
				$ar_r=&$ar_r[$liv];
				sum_columns_value($ar_r, $r);
			    //$ar_r = &$ar_r['children'];
			    
				//$ar_r[] = $r;
				
				
			}
			

	
		/**************** STAMPA REPORT   *********************************************************************************/			
			
			
			echo "<table class=\"acs_report\">";
			
			echo "<tr class=imp> 
				  <td colspan='33'>".$titolo."</td>
				  </tr>";

			
	foreach ($ar as $k0 => $v0){
		
			
		echo  "<tr>";
		
		echo "<th class=titoli>Mese/Nazione/Agente/Provincia/Cliente</th> 
			  <th class=titoli style='text-align:right;'>Totale</th>";
	
		setlocale(LC_TIME, 'it_IT');
		$numeroDiGiorni = date("t", $v0['mese']);
		
	
		
		   for($i=1; $i<=$numeroDiGiorni; $i++){
		   	
		   	
		   	
		   	if(print_date($k0.sprintf("%02s", $i), "%a")=='sab' || print_date($k0.sprintf("%02s", $i), "%a")=='dom'){
		   		
		   		echo "<th class=weekend>";
		   		
		   	}else{
		   		
		   		echo "<th class=week>";
		   	}
		   	
		   	  echo  $i." <br>". ucfirst(print_date($k0.sprintf("%02s", $i), "%a")) ."</th>";
			
				} 	
			
				if($numeroDiGiorni<31){
				
				$diff= 31 - $numeroDiGiorni;
				
				echo  "<th colspan='".$diff."'></th>";
				}
				
				
				echo  "</tr>";
			            		
				echo "<tr class=mese> 
			          <td>". $v0['desc']." </td>
	        		  <td class=number>". n($v0['VALORE'], 2, "N", "Y")." </td>";
		
					for($i=1; $i<=$numeroDiGiorni; $i++){
						$gg = sprintf("%02s", $i);
					
						if(print_date($k0.sprintf("%02s", $i), "%a")=='sab' || print_date($k0.sprintf("%02s", $i), "%a")=='dom'){
							
							
							echo "<td class=weekend>".n($v0['GG'][$gg], 2, "N", "Y")." </td>";
							
							
						} else{
							
							echo "<td class=number>".n($v0['GG'][$gg], 2, "N", "Y")." </td>";
						}
						
					
		
							}
							
						
							if($numeroDiGiorni<31){
							
								$diff= 31 - $numeroDiGiorni;
							
								echo  "<td class=vuoto colspan='".$diff."'></td>";
							}
							
							
						
				echo  "</tr>";
				
			foreach ($v0['children'] as $k1 => $v1){
				
				echo  "<tr class=liv1><td>". $v1['desc']." </td>
						<td class=number>". n($v1['VALORE'], 2, "N", "Y")." </td>";					
											
						for($i=1; $i<=$numeroDiGiorni; $i++){
							$gg = sprintf("%02s", $i);
							if(print_date($k0.sprintf("%02s", $i), "%a")=='sab' || print_date($k0.sprintf("%02s", $i), "%a")=='dom'){
									
								echo "<td class=weekend>".n($v1['GG'][$gg], 2, "N", "Y")." </td>";
									
							} else{
									
								echo "<td class=number>".n($v1['GG'][$gg], 2, "N", "Y")." </td>";
							}
						
							}
							
							if($numeroDiGiorni<31){
									
								$diff= 31 - $numeroDiGiorni;
									
								echo  "<td class=vuoto colspan='".$diff."'></td>";
							}
				
				
				echo "</tr>";
				
				
				foreach ($v1['children'] as $k2 => $v2){
					
					echo  "<tr class=liv2><td> &nbsp; Agente: ". $v2['desc']."</td>
		                     <td class=number>". n($v2['VALORE'], 2, "N", "Y")." </td>";	
					
							for($i=1; $i<=$numeroDiGiorni; $i++){
								$gg = sprintf("%02s", $i);
								
								if(print_date($k0.sprintf("%02s", $i), "%a")=='sab' || print_date($k0.sprintf("%02s", $i), "%a")=='dom'){
										
									echo "<td class=weekend>".n($v2['GG'][$gg], 2, "N", "Y")." </td>";
										
								} else{
										
									echo "<td class=number>".n($v2['GG'][$gg], 2, "N", "Y")." </td>";
								}
							
								}
								
								if($numeroDiGiorni<31){
										
									$diff= 31 - $numeroDiGiorni;
										
									echo  "<td class=vuoto colspan='".$diff."'></td>";
								}
					
					
					echo "</tr>";
					
					
					foreach ($v2['children'] as $k3 => $v3){
						
						echo  "<tr class=liv3><td> &nbsp; &nbsp; Provincia: ". $v3['desc']."</td>
								<td class=number>". n($v3['VALORE'], 2, "N", "Y")." </td>";	
						
							for($i=1; $i<=$numeroDiGiorni; $i++){
								$gg = sprintf("%02s", $i);
								
								if(print_date($k0.sprintf("%02s", $i), "%a")=='sab' || print_date($k0.sprintf("%02s", $i), "%a")=='dom'){
										
									echo "<td class=weekend>".n($v3['GG'][$gg], 2, "N", "Y")." </td>";
										
								} else{
										
									echo "<td class=number>".n($v3['GG'][$gg], 2, "N", "Y")." </td>";
								}
									
									}
									
									if($numeroDiGiorni<31){
											
										$diff= 31 - $numeroDiGiorni;
											
										echo  "<td class=vuoto colspan='".$diff."'></td>";
									}
						
						
						echo   "</tr>";
							
						foreach ($v3['children'] as $k4 => $v4){
								
							echo  "<tr>
		                			<td> &nbsp; &nbsp; &nbsp; Cliente: ". $v4['desc']."</td>
 									<td class=number>".n($v4['VALORE'], 2, "N", "Y")." </td>
								";
							
							for($i=1; $i<=$numeroDiGiorni; $i++){
								$gg = sprintf("%02s", $i);
								
								if(print_date($k0.sprintf("%02s", $i), "%a")=='sab' || print_date($k0.sprintf("%02s", $i), "%a")=='dom'){
										
									echo "<td class=weekend>".n($v4['GG'][$gg], 2, "N", "Y")." </td>";
										
								} else{
										
									echo "<td class=number>".n($v4['GG'][$gg], 2, "N", "Y")." </td>";
								}
								
							}
							
							if($numeroDiGiorni<31){
									
								$diff= 31 - $numeroDiGiorni;
									
								echo  "<td class=vuoto colspan='".$diff."'></td>";
							}
							
							echo "</tr>";
								
								
						}
							
							
					}
					
					
					
				}
				
				
			}
			
				
			
	}
			
			echo "</table>";

		?>


	</div>

</body>