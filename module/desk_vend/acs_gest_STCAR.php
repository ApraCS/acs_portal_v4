<?php
require_once("../../config.inc.php");

$main_module =  new Spedizioni();
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    'module'      => $main_module,
    'tab_name'    => $cfg_mod['file_tabelle'],
    'descrizione' => "Stati carico",
    't_panel' =>  "STCAR - Stati carico",
    'form_title' => "Dettagli tabella stati cacrico",
    'maximize'    => 'Y',
    'conferma_elimina'    => 'Y',
    'fields_preset' => array(
        'TATAID' => 'STCAR',
        //'TARIF1' => $m_params->open_request->sezione
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
   
    'fields_key' => array('TAKEY1', 'TADESC'),
    
    'fields_grid' => array('TAKEY1', 'TADESC', 'TAB_ABB', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC'),

    
    
    'fields' => array(
        
        'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione', 'fw'=>'flex: 1'),
         //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        //TABELLA ABBINATA
        'TAB_ABB' =>  array('label' => 'rilav', 'type' => 'TA',
            'iconCls' => 'search',
            'tooltip' => 'Stati ammessi',
            'select' => "TA_AM.C_ROW AS TAB_ABB",
            'join' => "LEFT OUTER JOIN (
            SELECT COUNT(*) AS C_ROW, TA2.TAKEY1
            FROM {$cfg_mod['file_tabelle']} TA2
            WHERE TA2.TADT = '{$id_ditta_default}' AND TA2.TATAID = 'STCAM'
            GROUP BY TA2.TAKEY1) TA_AM
            ON TA.TAKEY1 = TA_AM.TAKEY1",
            'ta_config' => array(
                'tab' => 'STCAM',
                'file_acs'     => '../desk_vend/acs_gest_STCAM.php?fn=open_tab',
                
            )
            ),
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione'),
        
    )
);


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
