<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

function add_parziali(&$ar, $r){
    $ar['colli']   += $r['TDTOCO'];
    $ar['volume']  += $r['TDVOLU'];
 
}

// ******************************************************************************************
// PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
    
    ?>
	    {"success":true, 
    	    m_win: {
    		title: 'Proposta ordini anticipabili',
    		width: 400, height: 250,
    		iconCls: 'icon-button_black_repeat-16'
    	    },
	    items: 
	    {
        xtype: 'form',
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        frame: true,
        buttons: [{
	            text: 'Conferma',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var form_values = form.getValues();
					 var loc_win = this.up('window');
					 
					 acs_show_panel_std('acs_ordini_anticipabili.php?fn=open_tab', 'panel-ordini-anticipabili',{
        		            	data : <?php echo j($m_params->data); ?>, 
        		            	area : <?php echo j($m_params->area); ?>, 
        		            	itin: <?php echo j($m_params->itin); ?>,
        		            	form_values : form_values
    		            	}, null, null);
			         	loc_win.close();
			      
			         }
			     }],   		
			  items: [
			    	{
						flex: 1,						 
						name: 'f_scarichi',
						xtype: 'checkboxgroup',
						fieldLabel: 'Solo scarichi esistenti',
						labelAlign: 'left',
					   	allowBlank: true,
					   	labelWidth: 180,
					   	items: [{
	                            xtype: 'checkbox'
	                          , name: 'f_scarichi' 
	                          , boxLabel: ''
	                          , checked: true
	                          , inputValue: 'Y'
	                        }]														
					 },
			  		{
						flex: 1,						 
						name: 'f_prog_conf',
						xtype: 'checkboxgroup',
						fieldLabel: 'Programmazioni confermate',
						labelAlign: 'left',
					   	allowBlank: true,
					   	labelWidth: 180,
					   	items: [{
	                            xtype: 'checkbox'
	                          , name: 'f_prog_conf' 
	                          , boxLabel: ''
	                         // , checked: true
	                          , inputValue: 'Y'
	                        }]														
					 },{
    				name: 'f_priorita',
    				xtype: 'combo',
    				flex: 1,
                	anchor: '100%',				
    				fieldLabel: 'Priorit&agrave;',
    				displayField: 'text',
    				multiSelect : true,
    				valueField: 'id',
    				emptyText: '- seleziona -',
    				forceSelection: true,
    			   	allowBlank: true,	
    			   	queryMode: 'local',
                    minChars: 1,													
    				store: {
    					autoLoad: true,
    					editable: false,
    					autoDestroy: true,	 
    				    fields: [{name:'id'}, {name:'text'}],
    				    data: [								    
    					     <?php echo acs_ar_to_select_json($s->get_options_priorita(), '') ?> 	 	
    					    ] 
    				}	, listeners: {
                    	 beforequery: function (record) {
                            record.query = new RegExp(record.query, 'i');
                            record.forceAll = true;
                        }
                     }					 
    			},{
					name: 'f_tipologia',
					xtype: 'combo',			
				 	flex: 1,
				 	anchor: '100%',
				 	multiSelect : true,
    				fieldLabel: 'Tipologia',
    				displayField: 'text',
    				valueField: 'id',
    				emptyText: '- seleziona -',
    				forceSelection: true,
    			   	allowBlank: false,
    			   	queryMode: 'local',
                    minChars: 1,
    				store: {
    					autoLoad: true,
    					editable: false,
    					autoDestroy: true,
    				    fields: [{name:'id'}, {name:'text'}],
    				    data: [
    					    <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 
    					    ]
				    }, listeners: {
                    	 beforequery: function (record) {
                            record.query = new RegExp(record.query, 'i');
                            record.forceAll = true;
                        }
                     }
				
					}
	             	  ]
					
			    	
        
        }
	    
	    }
<?php 
exit;
}


// ******************************************************************************************
// GET JSON DATA (treepanel)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    
    $data_evasione = $m_params->params->data;
    $area_spe = $m_params->params->area;
    $itinerario = $m_params->params->itin;
    $form_values = $m_params->params->form_values;
    
    $sql_where = "";
    if($form_values->f_scarichi == 'Y')
       $sql_where  .= " AND TDDTEP = {$data_evasione}";
    else
       $sql_where .= " AND TDDTEP > {$data_evasione} AND TDDTDS <= {$data_evasione}";
    
   
    $sql_where .= " AND TDASPE = '{$area_spe}'";
    if(isset($itinerario) && strlen($itinerario) > 0)
        $sql_where .= " AND TDCITI = '{$itinerario}'";
    
    $sql_where_ord = "";
    $sql_where_ord.= sql_where_by_combo_value('TDOPRI', $form_values->f_priorita);
    $sql_where_ord.= sql_where_by_combo_value('TDCLOR', $form_values->f_tipologia);
    
    if(isset($form_values->f_prog_conf) && $form_values->f_prog_conf == 'Y')
        $sql_where_ord .= "  ";   //Tutto, anche ordini con data confermata (spunta rossa)
    else
        $sql_where_ord .= " AND TDFN06  = 0";   //Solo ordini senza data confermata (NO spunta rossa) 
    
    $sql = "SELECT TD.*, TA_ITIN.TADESC AS D_ITIN
            FROM {$cfg_mod_Spedizioni['file_testate']} TD
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
    			  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
            WHERE " . $s->get_where_std() . " AND TDDTCF > 0 AND TDNRLO = 0
            AND TDSWSP = 'Y' AND TDBLEV <> 'Y' AND TDBLOC <> 'Y' AND TDFN02 = 0
 			{$sql_where} ORDER BY TAASPE, TASITI
			";
 			
	$sql_ord = "SELECT *
        	    FROM {$cfg_mod_Spedizioni['file_testate']} TD
        	    WHERE " . $s->get_where_std() . "
                AND TDSWSP = 'Y' /*AND TDFN06 = 0*/ AND TDDTCF > 0 
                AND TDNRCA = 0 AND TDBLEV <> 'Y' AND TDBLOC <> 'Y' AND TDFN02 = 0
                AND TDDTEP > {$data_evasione} AND TDDTDS <= {$data_evasione}
                AND TDCCON = ? AND TDCDES = ? AND TDTDES = ? {$sql_where_ord}
                ORDER BY TDOADO, TDONDO
        	    ";
	
	
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $ar = array();
  
    while ($row = db2_fetch_assoc($stmt)){
        
        $stmt_ord = db2_prepare($conn, $sql_ord);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_ord, array($row['TDCCON'],$row['TDCDES'], $row['TDTDES']));
        
        $ar_ordini = array();
        while ($row_ord = db2_fetch_assoc($stmt_ord))
            $ar_ordini[] = $row_ord;
          
        
        if(count($ar_ordini)>0){
             
             $liv1 = implode("_", array($row['TDNBOC'], $row['TDTPCA'], $row['TDAACA'], $row['TDNRCA']));  //carico
             $liv2 = implode("_", array($row['TDCCON'], $row['TDCDES'], $row['TDTDES'])); //cliente/dest
             $tmp_ar_id = array();
             
             //liv1: spedizione - carico
             $d_ar = &$ar;
             $c_liv = $liv1;
             $tmp_ar_id[] = $c_liv;
             if (!isset($d_ar[$c_liv])){
                 $d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id),  "children"=>array());
                 $d_ar[$c_liv]['liv']   = 'liv_1';
                 $d_ar[$c_liv]['liv_c'] = $liv1;
                 $d_ar[$c_liv]['task']  = trim($row['D_ITIN']). " [".trim($row['TDCITI'])."], #{$row['TDNBOC']} Carico ". implode("_", array($row['TDAACA'], $row['TDTPCA'], $row['TDNRCA']));
                 $d_ar[$c_liv]['colli'] = $d_ar_liv3['colli'];
             }
             $d_ar_liv1 = &$d_ar[$c_liv];
             
            // add_parziali($d_ar[$c_liv], $row);
             
             //liv2: cliente/destinazione
             $d_ar = &$d_ar[$c_liv]['children'];
             $c_liv = $liv2;
             $tmp_ar_id[] = $c_liv;
             if (!isset($d_ar[$c_liv])){
                 $d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id),  "children"=>array());
                 $d_ar[$c_liv]['liv']   = 'liv_2';
                 $d_ar[$c_liv]['liv_c'] = $c_liv;
                 $d_ar[$c_liv]['riferimento']  = $s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD']);
                 $d_ar[$c_liv]['task']  = acs_u8e($row['TDDCON']);
                 $d_ar_liv2 = &$d_ar[$c_liv];
                 
             $id = implode("|", $tmp_ar_id);
             $d_ar = &$d_ar[$c_liv]['children'];
           
             foreach($ar_ordini as $row_ant){
                 $r = array();
                 
                 $r['id']            = implode("|", array($id, $row_ant['TDDOCU']));
                 $r['liv_c']         = $row_ant['TDDOCU'];
                 $r['liv_type']      = 'ORDINE';
                 $r['liv']           = 'liv_3';
                 $r['k_ordine']      = $row_ant['TDDOCU'];
                 $r['fl_art_manc']   = $s->get_fl_art_manc($row_ant);
                 $r['art_da_prog']   = $s->get_art_da_prog($row_ant);
                 $r['riferimento']   = $row_ant['TDVSRF'];
                 $r["data_reg"]	     = $row_ant['TDODRE'];
                 $r["stato"]         = trim($row_ant['TDSTAT']);
                 $r["data_disp"]     = $row_ant['TDDTDS'];
                 $r["gg_rit"] 	  	 = $row_ant['TDGGRI'];
                 $r['data_ep']       = $row_ant['TDDTEP'];
                 $r['cons_rich']     = $row_ant['TDODER'];
                 $r['cliente']       = $row_ant['TDCCON'] ."_". $row_ant['TDDCON'];
                 $r["tipo"] 		 = trim($row_ant['TDOTPD']);
                 $r["raggr"] 		 = trim($row_ant['TDCLOR']);
                 $r['priorita']      = $row_ant['TDOPRI'];
                 $r["qtip_pri"]	     = trim($row_ant['TDDPRI']);
                 $r['cod_iti']       = trim($row_ant['TDCITI']);
                 if($row_ant['TDDTDS'] == $data_evasione)
                     $r['s_giallo']   = 'Y';
                 $r["tp_pri"]		 = trim($s->get_tp_pri(trim($row_ant['TDOPRI']), $row_ant));
                 $r['task']          = implode('_', array($row_ant['TDOTPD'], $row_ant['TDOADO'], $row_ant['TDONDO']));
                 $r['leaf']          = true;
                 $r['iconCls']       = $s->get_iconCls(3, $row_ant);
                 
                 add_parziali($r, $row_ant);
                 add_parziali($d_ar_liv1, $row_ant);
                 add_parziali($d_ar_liv2, $row_ant);
                 $d_ar[] = $r;
             }
                 
                 
        }    
    
     }//solo con ordini
        
       
    } //while principale
       //output dati
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// TABPANEL TREE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
    
    $title = "Advance [{$m_params->area}";
    if(trim($m_params->itin) != '')
        $title .= ", {$m_params->itin}" ;
        $title .= "] ".print_date($m_params->data, "%d/%m");
    
    ?>
{"success":true, "items":
{
    xtype: 'treepanel',
    title: <?php echo j($title); 
    ?>,    
    cls: 'tree-calendario',
    collapsible: true,
    useArrows: true,
    rootVisible: false,
    <?php echo make_tab_closable(); ?>,
        
    tbar: new Ext.Toolbar({
        items:['<b>Verifica ordini anticipabili al <?php echo print_date($m_params->data); ?> [data di produzione/disponibilit&agrave; alla spedizione]</b>', '->'
       	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
   		<?php echo make_tbar_closable() ?>            
        ]
    }),  		
    	store: Ext.create('Ext.data.TreeStore', {
	        proxy: {
	            type: 'ajax', timeout: 240000, actionMethods: {read: 'POST'},
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',					            
	            extraParams: {params: <?php echo acs_je($m_params); ?>},
                doRequest: personalizza_extraParams_to_jsonData
	        },
	        
			fields: ['s_giallo', 'task', 'liv', 'liv_c', 'liv_type', 'data_ep', 'id', 'cons_rich', 'priorita', 'qtip_pri', 
			         'fl_art_manc', 'art_da_prog',  'tp_pri', 'riferimento', 'data_reg', 'stato',
			         'data_disp', 'gg_rit', 'tipo', 'raggr', 'volume', 'colli', 'cod_iti', 'k_ordine'],	        
	        
	        reader: new Ext.data.JsonReader(),
	    }),
        
        multiSelect: true,
        singleExpand: false,
		viewConfig: {getRowClass: function(record, index) {return record.get('liv');}},
        columns: [{
                xtype: 'treecolumn', dataIndex: 'task',                 
                header: '<b>Itinerario/spedizione/carico/cliente</b>', flex: 1, menuDisabled: true, sortable: false,
                }    
                , {text: '&nbsp;<br><img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini MTO',  
    	    		dataIndex: 'fl_art_manc',
					renderer: function(value, p, record){
    			    	if (record.get('fl_art_manc')==20) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_red.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=15>';			    	
    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=15>';			    	
    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=15>';			    	
    			    	}} 	 
		    	, {text: '&nbsp;<br><img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>',width: 30, tdCls: 'tdAction', tooltip: 'Articoli mancanti MTS',
	        			dataIndex: 'art_da_prog',
						renderer: function(value, p, record){
	    			    	if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=15>';
			    			if (record.get('art_da_prog')==2) return '<img src=<?php echo img_path("icone/48x48/warning_red.png") ?> width=15>';			    	
			    			if (record.get('art_da_prog')==3) return '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=15>';	    			    	
				}} 
				, {text: 'Localit&agrave;/Riferimento', 	flex: 1, dataIndex: 'riferimento',  menuDisabled: false, sortable: false}
    	        , {text: 'Tp', 			width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd',
    	    	 	menuDisabled: false, sortable: false, 
	    	    	renderer: function (value, metaData, record, row, col, store, gridView){						
						metaData.tdCls += ' ' + record.get('raggr');										
					return value;			    
					}}
    	        , {text: 'Data', width: 60, dataIndex: 'data_reg', renderer: date_from_AS,  menuDisabled: false, sortable: false}
    	        , {text: 'St', width: 30, dataIndex: 'stato',  menuDisabled: false, sortable: false} 
                , {dataIndex: 'colli', header: '<b>Colli</b>',  width:80, align: 'right', renderer: floatRenderer2}        	    
                , {dataIndex: 'volume', header: '<b>Volume</b>',  width:80, align: 'right', renderer: floatRenderer2}        	    
    	        , {text: 'Evasione<br>program.',	width: 60, dataIndex: 'data_ep',
    			    menuDisabled: false, sortable: false, renderer: date_from_AS }  
    			, {text: 'Dispon.',	width: 60, dataIndex: 'data_disp',
    			    menuDisabled: false, sortable: false, 
    			    renderer: function (value, metaData, record, row, col, store, gridView){						
						if(record.get('s_giallo') == 'Y') metaData.tdCls += ' tpSfondoGiallo';										
					return date_from_AS(value);			    
					}
    			   }	
    		    , {text: 'GG<br>Rit.', width: 30, dataIndex: 'gg_rit',  menuDisabled: false, sortable: false}
  	            , {text: 'Pr', width: 35, dataIndex: 'priorita', tdCls: 'tpPri', menuDisabled: false, sortable: false,
    				renderer: function (value, metaData, record, row, col, store, gridView){
    					if (record.get('tp_pri') == 4)
    						metaData.tdCls += ' tpSfondoRosa';
                        if (record.get('tp_pri') == 6)
    						metaData.tdCls += ' tpSfondoCelesteEl';				
    					
    					return value;
    				    
    	    			}}
    		  , {text: 'Evasione<br>richiesta', width: 60, dataIndex: 'cons_rich', renderer: date_from_AS,  menuDisabled: false, sortable: false}
  		] //columns
  		
  		
  		, listeners: {
      		 itemcontextmenu : function(grid, rec, node, index, event) {
				grid.panel.acs_actions.show_menu_dx(grid, rec, node, index, event);
    		},
    		 celldblclick: {				  							
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				  
			  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
			  	rec = iView.getRecord(iRowEl);
                var grid = this;
                iEvent.preventDefault();	
                
                if(rec.get('liv_type') == 'ORDINE'){
                    
                    if (col_name=='fl_art_manc')
    					show_win_art_critici(rec.get('k_ordine'), 'MTO');
    				
    				if (col_name=='colli')
						show_win_colli_ord(rec, rec.get('k_ordine'));
					
					if (col_name=='data_disp')
						acs_show_win_std('Elenco fabbisogno materiali', 'acs_background_mrp_art.php?fn=fabbisogno_ordine', {k_ordine: rec.get('k_ordine')}, 1000, 550, null, 'icon-shopping_cart_gray-16');
					
				}
				}
                
                },
                
                 itemclick: function(view,rec,item,index,eventObj) {
		        
	          if (rec.get('liv') == 'liv_3')
	          {	   
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');	        	        	
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				   }
				});

				//cronologia
				if (wdc.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
	        		wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
					wdc.store.load();
				}				

				//allegati
				if (wdi.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
		        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
		        	if (wdi.isVisible())		        	
						wdi.store.load();
				}

				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}				
				
			   }
	         }
                
         }
		
  		

  	//funzioni / utility  	
  	, acs_actions: {   	
      	 show_menu_dx: function(grid, rec, node, index, event) {
      	      event.stopEvent();
      	       var voci_menu = [];
      	       
      	       id_selected = grid.getSelectionModel().getSelection();
					 list_selected_id = [];
  	                 for (var i=0; i<id_selected.length; i++){
		 				record_data = id_selected[i].data;
		  				list_selected_id.push(record_data);		  
  					 }
      	       
      	       if(rec.get('liv_type') == 'ORDINE'){
      	       voci_menu.push({
		      		text: 'Conferma anticipo',
		    		iconCls: 'iconSpedizione',
		    		handler: function() {  
		    		    
          				 my_listeners = {
        					onSpedConfirmed: function(){
        						grid.getStore().load();
        					}
        					}
          				
			    			 acs_show_win_std('Assegna produzione/disponibilit&agrave; alla spedizione', 
			 				 'acs_form_json_assegna_spedizione.php',
		   				     {list_selected_id: list_selected_id, cod_iti : rec.get('cod_iti'), solo_data:'<?php echo $m_params->data; ?>'}, 1050, 400, my_listeners, 'icon-delivery-16');
		    		}
                    });
                    
                     voci_menu.push({
		      		text: 'Conferma anticipo con data confermata',
		    		iconCls: 'iconConf',
		    		handler: function() {  
		    		    
          				 my_listeners = {
        					onSpedConfirmed: function(){
        						grid.getStore().load();
        					}
        					}
          				
			    			 acs_show_win_std('Assegna produzione/disponibilit&agrave; alla spedizione', 
			 				 'acs_form_json_assegna_spedizione.php',
		   				     {list_selected_id: list_selected_id, cod_iti : rec.get('cod_iti'), solo_data:'<?php echo $m_params->data; ?>', confermaData : 'Y'}, 1050, 400, my_listeners, 'icon-delivery-16');
		    		}
                    });
                    
                    voci_menu.push({
		      		text: 'Righe ordine',
		    		iconCls: 'icon-folder_search-16',
		    		handler: function() {  
		    		    
          				acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {k_ordine: rec.get('k_ordine'), trad : 'Y'}, 900, 450, null, 'icon-folder_search-16');          		
		    		}
                    });
                    
        			}
        			
			var menu = new Ext.menu.Menu({
        	items: voci_menu
			}).showAt(event.xy);
			
			return false;
      	} //show_menu_dx
    }
  	

 } //treepanel
}
<?php } exit; ?>