<?php


$s = new Spedizioni();

function sum_columns_value(&$ar_r, $r){
	global $s;

	$ar_r['colli_tot'] += $r['COLLI'];
	$ar_r['colli_disp'] +=$r['DISP'];
	$ar_r['importo'] +=$r['IMPORTO'];
	$ar_r['volume'] +=$r['VOLUME'];
	$ar_r['colli_non_disp'] += ($r['COLLI'] - $r['DISP']);

	$ar_r['fl_bloc'] = $s->get_fl_bloc($ar_r['fl_bloc'], $s->calc_fl_bloc($r));
	$ar_r['fl_art_manc'] =max($ar_r['fl_art_manc'], $s->get_fl_art_manc($r));
	$ar_r['art_da_prog'] = max($ar_r['art_da_prog'], $s->get_art_da_prog($r));
	 
	$ar_r['fl_da_prog'] += $r['TDFN04'];
	$ar_r['fl_cli_bloc'] += $r['TDFN02'];
	
	


}


function parametri_sql_where($form_values){

	global $s;

	$sql_where.= " WHERE " . $s->get_where_std();

	$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);

	$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);

	$sql_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);

	$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);

	$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values ->f_cliente);

	//controllo HOLD
	$filtro_hold=$form_values->f_hold;

	if($filtro_hold== "Y")
		$sql_where.=" AND TDSWSP = 'Y'";
		if($filtro_hold== "N")
			$sql_where.=" AND TDSWSP = 'N'";

			$filtro_evasi=$form_values->f_evasi;

	  //solo ordini evasi
			if ($filtro_evasi == "Y"){
				$sql_where.= " AND TDFN11 = 1 ";
			}
			//solo ordini da evadere
			if ($filtro_evasi == "N"){
				$sql_where.= " AND TDFN11 = 0 ";
			}

			return $sql_where;
}