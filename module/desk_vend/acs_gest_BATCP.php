<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();
$main_module =  new Spedizioni();
$cfg_mod = $main_module->get_cfg_mod();

$menu_type = $m_params->menu_type;


$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    'descrizione' => "Gestione tabella parametri",
    'form_title' => "Dettagli tabella parametri",
    'fields_preset' => array(
        'TATAID' => 'BATCP',
        'TAKEY1' => $m_params->open_request->takey1,
        'TAKEY3' => 'DOVE',
        'TAKEY4' => $m_params->open_request->menu_type,
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
	
    'fields_key' => array('TAKEY1', 'TAKEY2', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TAKEY2', 'TADESC', 'TAMAIL', 'immissione'),
    'fields_form' => array('TAKEY1', 'TAKEY2', 'TADESC', 'TAMAIL', 'TAFG01',  'TARIF1', 'TAFG02', 'TACOGE', 'TATELE', 'TAASPE', 'TAINDI', 'TASITI', 'TACAP', 'TARIF2'),
    
	'fields' => array(				
	    'TAKEY1' => array('label'	=> 'Batch', 'maxLength' => 10, 'value' => $m_params->takey1, 'c_fw' => 'width: 60', 'fw'=>'width: 170'),
	    'TAKEY2' => array('label'	=> 'Parametro', 'maxLength' => 10, 'fw'=>'width: 170', 'c_fw' => 'width: 80'),
	    'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 100),
	    'TAFG01' => array('label'	=> 'Tipo campo', 'type' => 'from_TA', 'TAID' => 'BTYPE'),
	    'TARIF1' => array('label'	=> 'Formato'),
	    'TAFG02' => array('label'	=> 'Obbligatorio', 'type' => 'from_TA', 'TAID' => 'ARY', 'allowBlank' => true),
	    'TACOGE' => array('label'	=> 'Tabella', 'maxLength' => 5),
	    'TATELE' => array('label'	=> 'TACOR1', 'maxLength' => 20),
	    'TAASPE' => array('label'	=> 'TACOR2', 'maxLength' => 10),
	    'TAINDI' => array('label'	=> 'Default', 'maxLength' => 60),
	    'TASITI' => array('label'	=> 'Sequenza', 'maxLength' => 5),
	    'TACAP'  => array('label'	=> 'Campo RI', 'maxLength' => 10),
	    'TAMAIL' => array('label'	=> 'Note',  'maxLength' => 100),
	    'TARIF2' => array('label'	=> 'Posizione su WPI0PI0'),
	    //immissione
	    'immissione' => array(
	        'type' => 'immissione', 'fw'=>'width: 70',
	        'config' => array(
	            'data_gen'   => 'TADTGE',
	            'user_gen'   => 'TAUSGE'
	        )
	        
	    ),
	    'TADTGE' => array('label'	=> 'Data generazione'),
	    'TAUSGE' => array('label'	=> 'Utente generazione'),
	    
	)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
