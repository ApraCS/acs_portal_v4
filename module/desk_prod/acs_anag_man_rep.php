<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */


$m_params = acs_m_params_json_decode();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$main_module =  new DeskProd();
$cfg_mod = $desk_art->get_cfg_mod();


$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "REPMA - Reparti",
    'descrizione' => "Gestione reparti",
    'form_title' => "Dettagli tabella reparti",
    'fields_preset' => array(
        'TATAID' => 'REPMA'
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TACOGE', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TACOGE'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice',  'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        'TACOGE' => array('label'	=> 'Linea produzione', 'short' =>'Linea p.', 'fw'=>'width: 60', 'type' => 'from_TA', 'TAID' => 'LIPRO'),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione'),
            
            
            ),
    'buttons' => array(
        array(
            'text'  => '',
            'iconCls' => 'icon-print-16',
            'tooltip' => 'Report reparti produzione',
            'scale' => 'small',
            'style' => 'border: 1px solid gray;',
            'handler' =>  new ApiProcRaw("function(){
                acs_show_win_std(null, 'acs_anag_man_rep_report.php?fn=open_form');
             }")
        )
    )
            
    );


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
