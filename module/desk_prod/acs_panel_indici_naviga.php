<?php

require_once("../../config.inc.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_config_distinta.php';

$main_module = new DeskProd();

$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data_elenco'){
//----------------------------------------------------------------------
    $ar = get_data_elenco($main_module, $m_params);
    echo acs_je($ar);
    exit;
}


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data_seq_ind'){
//----------------------------------------------------------------------
    $ar = get_data_seq_ind($main_module, $m_params);
    echo acs_je($ar);
    exit();
}

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data_gruppo'){
    //----------------------------------------------------------------------
    $ar = get_data_gruppo($main_module, $m_params);
    echo acs_je($ar);
    exit();
}




//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_config'){
//----------------------------------------------------------------------    
    $form_values = $m_params->form_values;
    ?>	
	{
		success:true,
		m_win: {
			title: 'Indice attivit&agrave;/uscite programmi di produzione',
			width: 800, height: 400, iconCls: 'icon-windows-16'
		},
		items: [
		 {
			xtype: 'fieldcontainer',
			layout: { 	type: 'hbox', pack: 'start', align: 'stretch'},						
			items: [													
				{
						xtype: 'grid',
						title: '',
						flex: 1,
						itemId : 'g_sx',
				        loadMask: true,	
				        store: {
        						xtype: 'store',
        						autoLoad:true,
        					 data : [
        							{codice: '<?php echo $m_params->cod; ?>', desc : '<?php echo $m_params->desc; ?>', distinta : '<?php echo $m_params->cod; ?>'},
							 

							        ],
				   	fields: ['codice', 'desc', 'gruppo', 'distinta', 'radice']
						

				}, //store
				columns: [	
					<?php echo dx_mobile(); ?>,
				
			        {
	                  header   : 'Radice',
	                  dataIndex: 'radice',
	                  width : 50,
	                  <?php if($form_values->f_debug == 'Y'){?>
	                   hidden : false
	                  <?php }else{?>
	                   hidden : true
	                  <?php }?>
				
	                },
	            
	                {
	                  header   : 'Codice',
	                  dataIndex: 'codice',
	                  width : 50
	                },
	                {
	                  header   : 'Descrizione',
	                  dataIndex: 'desc',
	                  sortable : false,
	                  flex: 1
	                }
	                
	                
	         ],	 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'small',
                     iconCls : 'icon-bookmark-16',
			            handler: function() {
			            
			            acs_show_win_std(null, 'acs_panel_indici.php?fn=open_indici');
	            		this.up('window').close();
			           
			            }
			     }
			     
			     
			     ]
		   }]
	         
	         , listeners: {
	         
	             celldblclick: {
	             		
	             		//su grid con elenco voci selezionate (grid sx)
	             			             								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  col_name = iView.getGridColumns()[iColIdx].dataIndex;	
							  var grid = this;
							  var grid_dx = this.up('window').down('#g_dx');
							  var rows = grid.getStore().data.items;
							
							  var gruppo = '';
							  for(var i = 1 ; i < iRowIdx + 1; i++) {
                                 gruppo += rows[i]['data']['codice'].trim();                                 
                              }
                          
        		              grid_dx.getStore().proxy.extraParams.gruppo = gruppo;
        		              grid_dx.getStore().proxy.extraParams.radice = rec.get('radice');
        		              grid_dx.getStore().load();
        		               
        		              var idx =  iRowIdx + 1 ; 
                              var lng =  rows.length ; 
                                  
                              ar_row = [];
                              for(var i = idx ; i < lng; i++) {
                                     ar_row.push(rows[i]);
                              }
                               
                              grid.getStore().remove(ar_row);
					 }
							 							  
				}
	         
	         	,itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  		
				  		id_selected = grid.getSelectionModel().getSelection();	
				  		var all_rec = grid.getStore().getRange();
				  		var grid_dx = this.up('window').down('#g_dx');
				  	
		                var gr_config = '';
		                var title = '';        
		                 for(var i=0; i<id_selected.length; i++){
		                   gr_config +=  id_selected[i].data.gruppo;
		                  
		                  }
		                   
		                   for(var i=0; i <= index ; i++){
		                    if(i == index)
		                    	title += ' [' + all_rec[i].get('codice').trim() + '] ' +all_rec[i].get('desc').trim();
		                    else
		                        title += ' [' + all_rec[i].get('codice').trim() + '] ' +all_rec[i].get('desc').trim() + ', ';
		                   }	
		                   
		                   
						  var voci_menu = [];
						 	 voci_menu.push({
			         		text: 'Configurazione distinta	',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
			        		
			        		    if(rec.get('gruppo').trim() == ''){
			        		        acs_show_win_std('Configurazione distinta attivit&agrave;/uscite programmi di produzione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form', {distinta: rec.get('distinta'), desc : rec.get('desc')}, 500, 200, null, 'icon-bookmark-16');
			        		    }else{
			        		    	acs_show_win_std('Configurazione distinta attivit&agrave;/uscite programmi di produzione', 'acs_panel_indici_config_distinta.php?fn=open_tab', {cod: rec.get('distinta'), des : rec.get('desc'), gruppo : gr_config, title : title}, 1300, 500, null, 'icon-bookmark-16');
			        		    }
			        		
					    		  
				                }
			    		 });	 
			    		 
			    		 if(rec.get('radice') != ''){
			    		 voci_menu.push({
			         		text: 'Verifica stringhe',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
					    		  acs_show_win_std('Elenco stringhe', 'acs_anag_artd_elenco_stringhe.php?fn=open_elenco', {gruppo : gr_config, distinta: rec.get('distinta'), radice : rec.get('radice'), from_dist : 'Y'}, 1050, 500, null, 'icon-gear-16');
				                }
			    		 });	 
			    		}
			    		
			    		
			    		if(grid_dx.cugino != ''){
			    		 
						 	voci_menu.push({
			         		text: 'Configura da cugino',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
			        		     
			        		      var cugino = grid_dx.cugino;
			        		      
			        		      Ext.Ajax.request({
						        url: 'acs_anag_art_duplica_indici.php?fn=exe_rec_cd',
						        jsonData: {
						            rec : rec.data					        	
						         },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        	jsonData = Ext.decode(result.responseText);
						        	
 						        	acs_show_win_std('Codice articolo', '../desk_utility/acs_anag_artd_elenco_stringhe.php?fn=open_codice', {
            	            		row : jsonData.row, row_c : cugino, row_p : rec.data, distinta: rec.get('distinta'), from_cugino : 'Y'}, 
            	            		450, 220, null, 'icon-leaf-16');
 						        	
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						            console.log('errorrrrr');
						        }
						    });	
			        		  
		 
			        		 
			        		 }
			    		 });
				  	    }
			    		
			    		
			    		   var menu = new Ext.menu.Menu({
				            items: voci_menu
					        }).showAt(event.xy);	
						  
						  }
	       	
				   
				 }
				 
		
		
		}, //grid
				
						{
						xtype: 'grid',
						title: '',
						flex: 2,
						itemId : 'g_dx',
				        loadMask: true,	
				        stateful: true,
				        stateId: 'grid_sx_indici',
        				stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
				        cugino : '',
				        tasto_dx_title : '',
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_elenco', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
										 distinta: '<?php echo $m_params->cod?>',
										 form_values : <?php echo acs_je($form_values); ?>, 
										 gruppo: '',
										 radice : ''										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['distinta', 'voce', 'gruppo', 'desc', 'radice', 'conf_radice', 'new', 
		        			         'ant', 'art', 'prog', 'nr_id', 'ardu', 'idme', 'master', 'nota_ope', 't_nota_ope', 'nota_dis', 't_nota_dis', 'chiave',
		        			         'ul_row', 'flag', 'tipo_lista', 'red_ant', 'p_a', 'p_da', 'opzione', 'todo', 't_todo']							
									
			}, //store
			
			 <?php $icona_anteprima = "<img src=" . img_path("icone/48x48/search.png") . " height=15>"; ?>
	         <?php $icona_note_operative = "<img src=" . img_path("icone/48x48/user.png") . " height=15 >"; ?>
			 <?php $icona_note_distinta = "<img src=" . img_path("icone/48x48/info_blue.png") . " height=15 >"; ?>
			
			  columns: [	
				
    			  <?php echo dx_mobile() ?>,				 
			      {
	                header   : 'Voce',
	                dataIndex: 'voce',
	                width : 50,
	                renderer: function(value, p, record){
    		    	       if (record.get('tipo_lista') == 'L')
						     return record.get('opzione');
						   return value;
    		    		  }
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'desc',
	                flex: 1
	                }, 
	                
	                //note operative
	                {
	                  text: '<?php echo $icona_note_operative ?>', 	
    				  width: 30,
    				  align: 'center', 
    				  dataIndex: 'nota_ope',
    				  tooltip: 'Note operative',	
    				  nt_config: <?php echo acs_je($m_table_config['fields']['nt_OPERAT'])?>,	        			    	     
    		    	  renderer: function(value, p, record){
						   if(!Ext.isEmpty(record.get('t_nota_ope')))
						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_nota_ope')) + '"';    		    	
    		    	
    		    	       if(record.get('nota_ope') > 0)
    		    			 return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
    		    		   else
    		    		     return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
		    		  }
    		        }, 
    		        
    		        
    		        //Nota di distinta
    		        {
	                  text: '<?php echo $icona_note_distinta ?>', 	
    				  width: 30,
    				  align: 'center', 
    				  dataIndex: 'nota_dis',	//ToDo
    				  tooltip: 'Note configurazione',
    				  nt_config: <?php echo acs_je($m_table_config['fields']['nt_CONFIG'])?>,	        			    	     
    		    	  renderer: function(value, p, record){
						   if(!Ext.isEmpty(record.get('t_nota_dis')))
						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_nota_dis')) + '"';    		    	
    		    	
    		    	       if(record.get('nota_dis') > 0)
    		    			 return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
    		    		   else
    		    		     return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
		    		  }
    		        }, 
    		        
    		        
    		        
    		        //Anteprima
    		        {
    		          text: '<?php echo $icona_anteprima ?>', 	
    				  width: 30,
    				  align: 'center', 
    				  dataIndex: 'ant',
    				  tooltip: 'Anteprima',		        			    	     
    		    	  renderer: function(value, p, record){
    		    	       if (record.get('red_ant') == '0')
						     p.tdCls += ' sfondo_grigio';
    		    	
    		    	         if(record.get('todo').trim() != ''){
    		    			   p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_todo')) + '"';  
    		    			   return '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=15>';
    		    			 }
    		    			
    		    			if(record.get('flag') == 'Z' || record.get('flag') == 'A')
    		    			 return '<img src=<?php echo img_path("icone/48x48/search.png") ?> width=15>';
    		    	   }
    		        }
	         ]
	    
	         
	         , listeners: {
	         
	         	 /* cellclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
        		         var rec = iView.getRecord(iRowEl);
        		         var col_name = iView.getGridColumns()[iColIdx].dataIndex;
        		 
        		        
        		    }*/
	         
	      
	            celldblclick: {
	            
	            		//su elenco voci (grid dx) - Sto selezionando la risposta
	            
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  var col_name = iView.getGridColumns()[iColIdx].dataIndex;	
							  var col = iView.getGridColumns()[iColIdx];
							  var grid = this;
							  var grid_sx = this.up('window').down('#g_sx');
							  
							  if(col_name == 'art'){
							 
							  		if(rec.get('flag') == 'A'){							  		
    							  		acs_show_panel_std('acs_panel_anag_art.php?fn=open_tab', null, {
                                    	form_open: {radice :  rec.get('radice')}
                                    	}, null, null);
                                    	this.up('window').close();                                    	
							  		}							   
							  }else if(col_name == 'nota_ope' || col_name == 'nota_dis'){
							  	if (!Ext.isEmpty(col.nt_config)){
        					  	var my_listeners = {
        	    		  				afterSave: function(from_win, values){
        	    		  			    rec.set(col_name, values.icon);
        	    		  			    from_win.close();
        		     					}	
        	     					};
        	     					
        	     				
        	     					
        					  	acs_show_win_std(null, '../base/acs_gestione_nota_nt.php?fn=open_panel', {
        					  		ar_nt_config: col.nt_config,
        					  		desc : rec.get('desc'),
        					  		chiave : rec.get('chiave')
        					  		}, null, null, my_listeners);
        					  		
        					  		return;
    					  		}
							  
							  }else if(col_name == 'ant' && (rec.get('flag') == 'Z' || rec.get('flag') == 'A')){
							  		acs_show_win_std('Elenco articoli', 'acs_anag_artd_elenco_stringhe.php?fn=open_art', {
                        			c_art : rec.get('gruppo'), radice : rec.get('radice'), from_ant : 'Y', rows : rec.data,
                        			form_values : <?php echo acs_je($form_values); ?>}, 
                        			450, 450, null, 'icon-leaf-16');                              
							  }else if(col_name == 'new' && rec.get('new') == 'Y'){
							     
							     acs_show_win_std('Duplica articolo ' + rec.get('ardu'), 
				                			'acs_anag_art_duplica.php?fn=open_form', {
				                             d_art_new: rec.get('desc'),
				                             c_art: rec.get('ardu'),
				                             from_indici : 'Y',
				                             radice : rec.get('gruppo'),
				                             nr_id : rec.get('nr_id'),
				                             prog : rec.get('prog'),
				                             voce : rec.get('voce'),
				                             master : rec.get('master'),
				                             distinta : rec.get('distinta')
									       }, 500, 450, {
				        					'afterSave': function(from_win){
				        						grid.getStore().load();
	 										}
				        				}, 'icon-comment_add-16');
							  
							  
							  }else{
							
							  if(rec.get('ul_row') != 'Y'){
							
            		             grid.getStore().proxy.extraParams.distinta = rec.get('distinta');
            		             grid.getStore().proxy.extraParams.gruppo = rec.get('gruppo');
            		             grid.getStore().proxy.extraParams.radice = rec.get('radice');
            		             grid.getStore().load();
            		          
            		             if(rec.get('flag') == 'L')
						    		var voce = rec.get('opzione').trim();
						    	 else	
						    	 	var voce = rec.get('voce').trim();
            		          
            		             var values = {codice: voce, desc : rec.get('desc'), gruppo : rec.get('gruppo'), distinta : rec.get('distinta'), radice : rec.get('radice')};
                                 grid_sx.getStore().add(values); 
                                 grid.tasto_dx_title += '[' + voce + '] ' + rec.get('desc').trim() +' ';                                
                                 }
        		             }
							 
						  }
			   		  }
			   		  
			   	, itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  		
				  						  		
				  		 var voci_menu = [];
			    		  
			    		 //a primo livello (sono in START = kiosk)
			    		 if(rec.get('master') == ''){
			    		 	 console.log(rec);
    						 voci_menu.push({
    			         		text: 'Lista programmi',
    			        		iconCls : 'icon-search-16',          		
    			        		handler: function () {
    			        		       acs_show_panel_std('acs_panel_indici_todolist_progetto.php?fn=open_panel', null, {
    			        		       		indice: rec.get('distinta'),
    			        		       		gruppo: rec.get('gruppo')
    			        		       });			        			 
    			        		 }
    			    		 });			    		 	
			    		 }
			    	
			    		   var menu = new Ext.menu.Menu({
				            items: voci_menu
					        }).showAt(event.xy);	
						  
						  }
	         
				   
				 }, viewConfig: {
		       		 getRowClass: function(record, index) {	
		       		 
		       		    if(record.get('ul_row') == 'Y' && record.get('new') == 'Y')
                		   return ' colora_riga_verde';
        		        
        		        if(record.get('ul_row') == 'Y')
                		   return ' colora_riga_giallo';
                		
                															
		         }   
		    }
				 
		
		
		}, //grid
						   
						
						]}
							
					
		]}
		
		
		<?php 
		exit;
}



//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//----------------------------------------------------------------------
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            
            items: [
         		 {
						flex: 1,							
						name: 'f_seq_ind',
						xtype: 'combo',
						fieldLabel: 'Sequenza indice',
						anchor: '-5',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection:true,
						multiSelect: true,
					   	allowBlank: true,														
						store: {
						xtype: 'store',
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_seq_ind',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								read: 'POST'
							},
				
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'
							},
							   extraParams: {
							   
							    distinta : '<?php echo $m_params->distinta; ?>'
    		 				},
    		 					doRequest: personalizza_extraParams_to_jsonData        		
						},
						
						fields: [{name:'id'}, {name:'text'}],
						}					 
						},
						 {
						flex: 1,							
						name: 'f_gruppo',
						xtype: 'combo',
						fieldLabel: 'Gruppo',
						anchor: '-5',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection:true,
						multiSelect: true,
					   	allowBlank: true,														
						store: {
						xtype: 'store',
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								read: 'POST'
							},
				
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'
							},
							   extraParams: {
							   distinta : '<?php echo $m_params->distinta; ?>'
    		 				},
    		 					doRequest: personalizza_extraParams_to_jsonData        		
						},
						
						fields: [{name:'id'}, {name:'text'}],
						}					 
						},{
    						xtype : 'textfield',
    						name : 'f_d_voce',
    						fieldLabel : 'Descrizione voce',
    					    anchor: '-5',
						}
          
          
			 
            ],
            
			buttons: [	
			{
		            text: 'Visualizza',
			        iconCls: 'icon-windows-32',	            
			        scale: 'large',		            
		            handler: function() {
		            
		            var form = this.up('form').getForm();
		            			            
		            acs_show_win_std('Configurazione distinta attivit&agrave;/uscite programmi di produzione', 
		            	'acs_panel_indici_config_distinta.php?fn=open_tab', {cod: '<?php echo $m_params->distinta; ?>', des: '<?php echo $m_params->desc; ?>', form_values : form.getValues()}, 1300, 500, null, 'icon-gear-16');
		         
		                
		            }
		        }
	        
	        
	        ]            

}		
]}
<?php 
	exit;
}
