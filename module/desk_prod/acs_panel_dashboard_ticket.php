<?php
require_once("../../config.inc.php");
require_once "acs_panel_dashboard_ticket_compo.php";
require_once "acs_panel_dashboard_ticket_include.php";
require_once '_config_ticket_man.php';

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$cfg_mod = $main_module->get_cfg_mod();
$m_params = acs_m_params_json_decode();

$data = array();
//PASSO VALORI UTENTE-MACCHINA-REPARTO
//campo
if (isset($m_params->field))
    $data['field'] = $m_params->field;
else
    $data['field'] = '';
    //valore
if (isset($m_params->valueField))
    $data['valueField'] = $m_params->valueField;
else
    $data['valueField'] = '';
            
//PASSO PERIODO
if(isset($m_params->data)){
    //GIORNO DA
    if (isset($m_params->form_values->giorno_da))
        $data['giorno_da'] = $m_params->form_values->giorno_da;
    else
        $data['giorno_da'] = date('Ymd', strtotime("-1 week"));
    // GIORNO A
    if (isset($m_params->form_values->giorno_a))
        $data['giorno_a'] = $m_params->form_values->giorno_a;
    else
        $data['giorno_a'] = date('Ymd');
}


//****************************************************
if ($_REQUEST['fn'] == 'open_panel'){
//****************************************************
    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _dashboard_main_panel($m_params)
    ));
    exit;
}

//****************************************************
if ($_REQUEST['fn'] == 'get_data_grid_avanzamento'){
//****************************************************

    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_data_grid_avanzamento($m_params)
    ));
    exit;
}

//****************************************************
if ($_REQUEST['fn'] == 'get_data_grid_aperti'){
    //****************************************************
    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_data_grid_aperti($m_params)
    ));
    exit;
}


//****************************************************
if ($_REQUEST['fn'] == 'get_data_grid_operatori'){
    //****************************************************
    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_data_grid_operatori($m_params)
    ));
    exit;
}


//****************************************************
if ($_REQUEST['fn'] == 'get_chart_ticket_reparto'){
    //****************************************************
    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_data_grid_reparto($m_params)
    ));
    exit;
}



//****************************************************
if ($_REQUEST['fn'] == 'get_chart_ticket_macchina'){
    //****************************************************
    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_data_grid_macchina($m_params)
    ));
    exit;
}



