<?php

function where_filtro_colli_by_k_lista($cfg_mod, $k_lista){
    global $conn, $id_ditta_default;
    //recupero i filtri in base a k_lista
    $sql = "SELECT * FROM {$cfg_mod['file_cfg_distinte_liste']} WHERE CLDT = '$id_ditta_default' AND CLCDLI = '{$k_lista}'";
    $stmtCL = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmtCL);
    echo db2_stmt_errormsg($stmtCL);
    
    $ar_gruppo_collo = array();
    $ar_linee_produttive = array();
    $ar_PLOPE1 = array();
    $ar_PLOPE2 = array();
    
    $ar_gruppo_collo_IE = 'I';
    $ar_linee_produttive_IE = 'I';
    $ar_PLOPE1_IE = 'I';
    $ar_PLOPE2_IE = 'I';
    
    while ($rowCL = db2_fetch_assoc($stmtCL)){
        
        //filtri su codice articolo
        if (substr(trim($rowCL['CLCELE']), 0, 7)  == 'PL_ART_')
            $colli_where .= " AND PLART LIKE " . sql_char_compare_string($rowCL['CLRSTR'], 'RLIKE');
            
        //array filtro su gruppo collo
        if (substr(trim($rowCL['CLCELE']), 0, 6)  == 'PL_GC_')
            $ar_gruppo_collo[] = rtrim($rowCL['CLRSTR']);
     
        //array filtro linee produttive
        if (substr(trim($rowCL['CLCELE']), 0, 6)  == 'PL_LP_')
            $ar_linee_produttive[] = rtrim($rowCL['CLRSTR']);                
        
        //array filtro PLOPE1
        if (substr(trim($rowCL['CLCELE']), 0, 8)  == 'PL_OPE1_')
            $ar_PLOPE1[] = rtrim($rowCL['CLRSTR']);
        
        //array filtro PLOPE2
        if (substr(trim($rowCL['CLCELE']), 0, 8)  == 'PL_OPE2_')
            $ar_PLOPE2[] = rtrim($rowCL['CLRSTR']);
        
        if (trim($rowCL['CLCELE']) == 'IE_PL_GC') $ar_gruppo_collo_IE = rtrim($rowCL['CLRSTR']);
        if (trim($rowCL['CLCELE']) == 'IE_PL_LP') $ar_linee_produttive_IE = rtrim($rowCL['CLRSTR']);
        if (trim($rowCL['CLCELE']) == 'IE_PL_OPE1') $ar_PLOPE1_IE = rtrim($rowCL['CLRSTR']);
        if (trim($rowCL['CLCELE']) == 'IE_PL_OPE2') $ar_PLOPE2_IE = rtrim($rowCL['CLRSTR']);
        
        //include/escludi/solo "Colli con incasso"
        if (trim($rowCL['CLCELE']) == 'PL_CON_INC'){
            switch (rtrim($rowCL['CLRSTR'])) {
                case 'I': //Includi, non eseguo filtri
                    ; 
                    break;
                case 'E': //Escludi
                    $colli_where .= " AND PLTABB <> 'I' "; 
                    break;
                case 'S': //Solo colli con incasso
                    $colli_where .= " AND PLTABB = 'I' ";
                    break;
            }
        }
        
    }
    
    $colli_where .= sql_where_by_combo_value('PLRCOL', $ar_gruppo_collo, null, $ar_gruppo_collo_IE);
    $colli_where .= sql_where_by_combo_value('PLFAS10', $ar_linee_produttive, null, $ar_linee_produttive_IE);
    $colli_where .= sql_where_by_combo_value('PLOPE1', $ar_PLOPE1, null, $ar_PLOPE1_IE);
    $colli_where .= sql_where_by_combo_value('PLOPE2', $ar_PLOPE2, null, $ar_PLOPE2_IE); 
    
    return $colli_where;
}




function where_filtro_compo_by_k_lista($cfg_mod, $k_lista){
    global $conn, $id_ditta_default;
    //recupero i filtri in base a k_lista
    $sql = "SELECT * FROM {$cfg_mod['file_cfg_distinte_liste']} WHERE CLDT = '$id_ditta_default' AND CLCDLI = '{$k_lista}'";
    $stmtCL = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmtCL);
    echo db2_stmt_errormsg($stmtCL);
    
    $ar_gruppo_collo = array();
    $ar_linee_produttive = array();
    $ar_PLOPE1 = array();
    $ar_PLOPE2 = array();
    
    $ar_gruppo_collo_IE = 'I';
    $ar_linee_produttive_IE = 'I';
    $ar_PLOPE1_IE = 'I';
    $ar_PLOPE2_IE = 'I';
    
    while ($rowCL = db2_fetch_assoc($stmtCL)){
        
        //filtri su codice articolo
        if (substr(trim($rowCL['CLCELE']), 0, 7)  == 'RP_ART_'){
            $colli_where .= " AND REART LIKE " . sql_char_compare_string($rowCL['CLRSTR'], 'RLIKE');
        }
        
        
        //include/escludi/solo "Colli con incasso"
        if (trim($rowCL['CLCELE']) == 'RP_ART_MTO'){
            switch (rtrim($rowCL['CLRSTR'])) {
                case 'I': //Includi, non eseguo filtri
                    ;
                    break;
                case 'O': //Escludi
                    $colli_where .= " AND REMTO = 'O' ";
                    break;
                case 'A': //Solo colli con incasso
                    $colli_where .= " AND REMTO = 'A' ";
                    break;
                case 'OA': //Solo colli con incasso
                    $colli_where .= " AND REMTO IN ('A', 'O') ";
                    break;
                
            }
        }
        
        //array filtro OPE1
        if (substr(trim($rowCL['CLCELE']), 0, 8)  == 'RP_OPE1_'){
                        $ar_RPOPE1[] = rtrim($rowCL['CLRSTR']);
        }
                        
        //array filtro OPE2
        if (substr(trim($rowCL['CLCELE']), 0, 8)  == 'RP_OPE2_'){
                            $ar_RPOPE2[] = rtrim($rowCL['CLRSTR']);
        }
        
        //array tipo parte
        if (substr(trim($rowCL['CLCELE']), 0, 6)  == 'RP_TP_'){
            $ar_RETPAR[] = rtrim($rowCL['CLRSTR']);
        }
                            
        if (trim($rowCL['CLCELE']) == 'IE_RP_OPE1') {$ar_RPOPE1_IE = rtrim($rowCL['CLRSTR']);}
        if (trim($rowCL['CLCELE']) == 'IE_RP_OPE2') {$ar_RPOPE2_IE = rtrim($rowCL['CLRSTR']);}
        if (trim($rowCL['CLCELE']) == 'IE_RP_PT') {$ar_RETPAR_IE = rtrim($rowCL['CLRSTR']);}
                            
    }
    
    $colli_where .= sql_where_by_combo_value('REOPE1', $ar_RPOPE1, null, $ar_RPOPE1_IE);
    $colli_where .= sql_where_by_combo_value('REOPE2', $ar_RPOPE2, null, $ar_RPOPE2_IE); 
    $colli_where .= sql_where_by_combo_value('RETPAR', $ar_RETPAR, null, $ar_RETPAR_IE);
    return $colli_where;
}








function get_risposte_START($main_module, $row, $form_values){
    
   global $conn, $id_ditta_default;
   $cfg_mod = $main_module->get_cfg_mod();
   $desk_art = new DeskArt(array('no_verify' => 'Y'));
    
   $sql_where_ar = gest_form_values($form_values);
    
   $nr = array();
    
   $nr['gruppo'] = trim($row['CDSLAV']);
   $nr['distinta'] = trim($row['CDDICL']);
   $nr['desc'] = trim($row['CDDESC']);
   $nr['voce'] = $row['CDSLAV'];
   $nr['radice'] = trim($row['CDRSTR']);
   $nr['ul_row'] = get_cd_row($main_module, $nr['distinta'], $nr['gruppo']);
   
   $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
   $chiave = implode ("|", $ar_comp);
   $nr['chiave'] = $chiave;
  
   $nt_config_ope = array('file'     => $cfg_mod['file_note'],
                          'NTTPNO'   => 'CDOPE');
   $nr['nota_ope'] =  _ha_nota_nt_config($chiave, $nt_config_ope);
   $row_nt = _get_nota_nt_config($chiave, $nt_config_ope);
   $nr['t_nota_ope'] = utf8_decode($row_nt['NTMEMO']);
   
   $nt_config_dis = array('file'     => $cfg_mod['file_note'],
                          'NTTPNO'   => 'CDINF');
   
   $nr['nota_dis'] =  _ha_nota_nt_config($chiave, $nt_config_dis);
   $row_nt = _get_nota_nt_config($chiave, $nt_config_dis);
   $nr['t_nota_dis'] = utf8_decode($row_nt['NTMEMO']);
   
   $nr['todo'] = trim($row['CDTODO']);
   $todo = $main_module->get_TA_std('ATTAV', $nr['todo']);
   $nr['t_todo'] = "[".trim($row['CDTODO'])."] ".$todo['TADESC'];
   
   $ar[] = $nr;
   
   return $ar;
}




function get_risposte_G($main_module, $master, $row, $radice_temp = ''){
    global $conn, $id_ditta_default;
    $cfg_mod = $main_module->get_cfg_mod();
    $ar = array();
    $desk_art = new DeskArt(array('no_verify' => 'Y'));
    
    $sql_where_ar = gest_form_values($form_values);
    
    $nr = array();
    $nr['gruppo'] = $master.trim($row['CDSLAV']);
    $nr['master'] = $master;
    $nr['distinta'] = trim($row['CDDICL']);
    $nr['desc'] = trim($row['CDDESC']);
    $nr['voce'] = trim($row['CDSLAV']);
    
    //Per Duplica
    $nr['new'] = trim($row['CDNEWC']);
    $nr['prog'] = trim($row['CDPROG']);
    $nr['nr_id'] = trim($row['CDNCAR']);
    $nr['ardu'] = trim($row['CDARDU']);
    
    $nr['idme'] = trim($row['CDIDME']);
    $nr['flag'] = trim($row['CDFLGA']);
    $nr['p_da'] = trim($row['CDSTIN']);
    $nr['p_a'] = trim($row['CDSTFI']);
    $nr['ul_row'] = get_cd_row($main_module, $nr['distinta'], $nr['gruppo']);
    
    $nr['conf_radice']  = show_conf_radice($row, $row['CDRSTR']);
    $nr['radice']       = crea_nuova_radice($radice_temp, $row['CDRSTR'], $nr['p_da'], $nr['p_a']);

    $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
   
    $chiave = implode ("|", $ar_comp);
    $nr['chiave'] = $chiave;
    
    $nt_config_ope = array('file'     => $cfg_mod['file_note'],
        'NTTPNO'   => 'CDOPE');
    $nr['nota_ope'] =  _ha_nota_nt_config($chiave, $nt_config_ope);
    $row_nt = _get_nota_nt_config($chiave, $nt_config_ope);
    $nr['t_nota_ope'] = utf8_decode($row_nt['NTMEMO']);
    
    $nt_config_dis = array('file'     => $cfg_mod['file_note'],
        'NTTPNO'   => 'CDINF');
    
    $nr['nota_dis'] =  _ha_nota_nt_config($chiave, $nt_config_dis);
    $row_nt = _get_nota_nt_config($chiave, $nt_config_dis);
    $nr['t_nota_dis'] = utf8_decode($row_nt['NTMEMO']);
    
    
    $nr['todo'] = trim($row['CDTODO']);
    $todo = $desk_art->get_TA_std('ATTAV', $nr['todo']);
    $nr['t_todo'] = "[".trim($row['CDTODO'])."] ".$todo['TADESC'];
    
                
    $ar[] = $nr;
    return $ar;
}



function get_data_seq_ind($main_module, $m_params){
    global $conn, $id_ditta_default;
    $cfg_mod = $main_module->get_cfg_mod();
    
    $distinta = $m_params->distinta;
    
    $sql = "SELECT DISTINCT CDSEQI
            FROM {$cfg_mod['file_cfg_distinte']}
            WHERE CDDT = ? AND CDDICL = ? ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_ditta_default, $distinta));
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)){        
        $ret[] = array("id"=>trim($row['CDSEQI']), "text" =>trim($row['CDSEQI']));
    }
    return $ret;
}


function get_data_gruppo($main_module, $m_params){
    global $conn, $id_ditta_default;
    $cfg_mod = $main_module->get_cfg_mod();
    
    $distinta = $m_params->distinta;
    
    $sql = "SELECT DISTINCT CDCMAS
            FROM {$cfg_mod['file_cfg_distinte']}
            WHERE CDDT = ? AND CDDICL = ? ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_ditta_default, $distinta));
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)){       
        $ret[] = array("id"=>trim($row['CDCMAS']), "text" =>trim($row['CDCMAS']));
    }
    return $ret;
}