<?php
require_once("../../config.inc.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_todolist_preview_include.php';

$main_module = new DeskProd();
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$pp_colli = _pp_get_colli($main_module, $m_params, $m_params->preview);
$ar_colli = $pp_colli['ar_colli'];
$rec = $ar_colli[0];


$title = "Vista ordine {$rec['TDONDO']} {$rec['TDOTPD']}_{$rec['TDOADO']}";

//foreach ($ar_colli as $rec){
    
    $order_dir          = _pp_order_dir($main_module, $rec['TDDOCU']);
    $order_dir_assoluto = _pp_order_dir_assoluto($main_module, $rec['TDDOCU']);
    
    $fisso = '_AssoDxBN_'; //ToDo: parametrizzare
    
    $ricerca_file = $order_dir_assoluto . '/*' . "{$fisso}.JPG";
    
    $f = null;
    foreach (glob($ricerca_file) as $filename){
        $f = basename($filename);
    }

    if (is_null($f)){
        //se immagine non trovata, da formato logo, recupero immagine "No immagine"
        $row_tab_logo = $main_module->get_TA_std('LOGOP', $rec['TDAUX4']);
        $logo_path    = trim($row_tab_logo['TALOCA']);
        
        $img = "../personal/desk_prod/loghi/{$logo_path}";
    } else {
        //se immagine trovata
        $img = '../'.$order_dir . '/' . $f;
       
    }
   
//}



//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_panel'){
//----------------------------------------------------------------------

    $rowAS   = get_as_row_by_id($main_module, $m_params->open_request->id_assoluto_lista);
    $rowCD   = get_cd_row_by_id($main_module, $rowAS['ASORD4']);
 
    ?>
	{
		success:true,	
		m_win: {
			title: <?php echo j($title); ?>,
			iconCls : 'icon-design-16',
			width: 700, height: 620,
		},
		items: [
				{
					xtype: 'panel'
					, flex: 1
					, autoScroll: false
					, layout: 'fit'
			        , items: [
        				{
        					xtype: 'panel'
        					, flex: 1
        					, autoScroll: false
        					, layout: 'fit'
        			        , items: [
        			          {
        			        	  xtype: 'panel',
        			        	  autoScroll: false,
        			        	  flex: 1,
        			        	  padding: 10,
        			        	  html: '<img src=<?php echo img_path($img) ?> width="90%">',
        			        	  border: false
        			          }
        			        ]        
        		  		} //panel
			        ]
			        
			        , buttons: [
		              
		                {
			                xtype:'splitbutton',
			                iconCls: 'icon-button_grey_play-32',
			                scale: 'large',
			                iconAlign: 'top',
			                arrowAlign:'right',
			                menu: {
			                	xtype: 'menu',
		        				items: [
		        					{
                    	            xtype : 'button',
                    	            text: 'Visualizza righe',
					           	    iconCls : 'icon-folder_search-16',           
                    		        scale: 'small',		
                    		        handler: function() 
					             		 { acs_show_win_std( null
					                               , '../desk_utility/acs_get_order_rows_gest_order_entry.php'
					                               , { from_righe_info: 'Y'
					                                // , from_anag_art : 'Y'
					                                 , from_prod : 'Y'
					                                 , k_ordine: <?php echo j($rec['TDDOCU'])?>
					                                // , modificabile: 'Y'	//ToDo: solo per gli ordini creati da qui!
					                                 }); 
					                       }
                    	          		},
                    	          		{
                    	            xtype : 'button',
                    	            text: 'Visualizza packing list',
					           	    iconCls : 'icon-folder_search-16',           
                    		        scale: 'small',		
                    		        handler: function() {
                    		         acs_show_win_std(null, 'acs_win_colli_ord.php?fn=open_tab', 
	    			  				   {k_ordine : <?php echo j($rec['TDDOCU'])?>});	  
					                       }
                    	          		},
                    	          		
                    	          			{
                    	            xtype : 'button',
                    	            text: 'Gallery colli ordine',
					           	    iconCls : 'icon-print-16',           
                    		        scale: 'small',		
                    		        handler: function() {
                    		           window.open('acs_gallery_colli_report.php?k_ordine='+ <?php echo j($rec['TDDOCU'])?> + '&params=' + <?php echo j($rowCD['CDPAUS']); ?>);
					                       }
                    	          		},
                    	          		
                    	          		{
                    	            xtype : 'button',
                    	            text: 'Elenco esploso distinta componenti',
					           	    iconCls : 'icon-folder_search-16',           
                    		        scale: 'small',		
                    		        handler: function() {
                    		       			acs_show_win_std('Riepilogo articoli/componenti ', '../desk_utility/acs_elenco_riepilogo_art_comp.php?fn=open_elenco', {k_ordine: <?php echo j($rec['TDDOCU'])?>, elenco : 'Y'}, 1300, 450, {}, 'icon-leaf-16');   	     	          			  
					                       }
                    	          		}
		        				]}
		        				
		        				}
		            	   , '->',
        			   {
        	            xtype : 'button',
        	            text: 'Dettagli',
        		        iconCls: 'icon-info_black-32',	            
        		        scale: 'large',		
        		        handler: function(b) {
        		        	acs_show_win_std('Dettaglio ordini', '../desk_vend/acs_win_dettaglio_ordine.php?fn=open_win', 
	    			  	    {k_ordine : <?php echo j($rec['TDDOCU'])?>}, 400, 550, null, 'icon-info_black-16');	
        	            	
        	            }
        	          }
        		
        		]
	
        
		    

		  	} //panel
		 
		
		]
		   
	}
	
	<?php 
    
}

