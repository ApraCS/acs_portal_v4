<?php

function get_data_indici($main_module){
    global $conn, $id_ditta_default;
    $ar = array();
    
    $cfg_mod = $main_module->get_cfg_mod();
    
    $sql = "SELECT TAKEY1, TADESC, TAFG01
            FROM {$cfg_mod['file_tabelle']} TA
            WHERE TADT = ? AND TATAID = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_ditta_default, $cfg_mod['taid_indici']));
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['codice'] = trim($row['TAKEY1']);
        $nr['desc'] = acs_u8e(trim($row['TADESC']));
        $nr['att'] = trim($row['TAFG01']);        
        $ar[] = $nr;
    }    
    return $ar;
}



function get_data_elenco($main_module, $m_params){
        global $conn, $id_ditta_default;
        $cfg_mod = $main_module->get_cfg_mod();
        $ar = array();
        $distinta = trim($m_params->distinta);
        $gruppo = trim($m_params->gruppo);
        $radice_temp = trim($m_params->radice);
        $form_values = $m_params->form_values;
        
        
        if(isset($gruppo) && strlen($gruppo) >0)
            $cdcmas = $gruppo;
        else
            $cdcmas = "START";
                
        $sql = "SELECT *
                FROM {$cfg_mod['file_cfg_distinte']}
                WHERE CDDT = ? AND CDDICL = ? AND CDCMAS = ?";
                
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($id_ditta_default, $distinta, $cdcmas));
                
        while($row = db2_fetch_assoc($stmt)){
            $master = trim($row['CDCMAS']);
            
            if($master == 'START'){
                $ar = array_merge($ar, get_risposte_START($main_module, $row, $form_values));
            }else {
                switch (trim($row['CDFLLG'])){
                    case 'L':
                        $ar = array_merge($ar, get_risposte_L($main_module, $row, $radice_temp, $form_values));
                        break;
                    case 'G':
                    case 'A':
                    case '' :
                        $ar = array_merge($ar, get_risposte_G($main_module, $master, $row, $radice_temp, $form_values));
                        break;
                }
            }
            
        } //while
                
   return $ar;
}


function gest_form_values($form_values){
    
    $sql_where_ar = "";
    
    if(isset($form_values->f_sosp) && $form_values->f_sosp == 'Y')
        $sql_where_ar .= " AND ARSOSP = 'S'";
        
   return $sql_where_ar;
}



function get_cd_row($main_module, $distinta, $gruppo){    
    global $conn, $id_ditta_default;    
    $cfg_mod = $main_module->get_cfg_mod();
    
    $sql = "SELECT COUNT(*) AS C_ROW
            FROM {$cfg_mod['file_cfg_distinte']}
            WHERE CDDT = ? AND CDDICL = ? AND CDCMAS = ?";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_ditta_default, $distinta, $gruppo));
    $row = db2_fetch_assoc($stmt);
    if($row['C_ROW'] > 0)
        $ul_row = 'N';
    else
        $ul_row = 'Y';
            
    return $ul_row;            
}


function get_as_row_by_id($main_module, $asidpr){
    global $conn, $id_ditta_default;
    $cfg_mod = $main_module->get_cfg_mod();
    
    $sql_AS = "SELECT * FROM {$cfg_mod['file_assegna_ord']} WHERE ASDT = '{$id_ditta_default}' AND ASIDPR = {$asidpr}";
   
    $stmt_AS = db2_prepare($conn, $sql_AS);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_AS);
    echo db2_stmt_errormsg($stmt_AS);
    db2_free_stmt($stmt_AS);
    $rowAS =  db2_fetch_assoc($stmt_AS);
    if ($rowAS)
        $rowAS = array_map('rtrim', $rowAS);
   
    return $rowAS;
}


function get_cd_row_by_id($main_module, $cdidpr){
    global $conn, $id_ditta_default;
    $cfg_mod = $main_module->get_cfg_mod();
    
    $sql_AS = "SELECT * FROM {$cfg_mod['file_cfg_distinte']} WHERE CDDT = '$id_ditta_default' AND CDIDPR = {$cdidpr}";
    $stmt_AS = db2_prepare($conn, $sql_AS);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_AS);
    echo db2_stmt_errormsg($stmt_AS);
    db2_free_stmt($stmt_AS);
    $rowAS =  db2_fetch_assoc($stmt_AS);
    if ($rowAS)
        $rowAS = array_map('rtrim', $rowAS);
    return $rowAS;
}


function get_k_lista_from_as($main_module, $rowAS){
    global $conn, $id_ditta_default;
    $cfg_mod = $main_module->get_cfg_mod();
    $k_lista = null;
    
    //da rowAS recupero configuratore, lista in modo che posso recuperare eventuali filtri
    //ASORD1 = Indice
    //ASORD2 = Kiosk
    // attenzione: in mezzo c'e' anche la sequenza??????
    //ASORD3 = Lista
    
    //Da config. distinta recupero codice lista (per eventuali filtri colli/compo)
    $sql = "SELECT * FROM {$cfg_mod['file_cfg_distinte']}
                WHERE CDDT = '$id_ditta_default'
                  AND CDDICL = '{$rowAS['ASORD1']}'
                  AND CDCMAS = '{$rowAS['ASORD2']}'
                  AND CDSLAV = '{$rowAS['ASORD3']}' ";
    
    $stmtDC = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmtDC);
    echo db2_stmt_errormsg($stmtDC);
    $rowDC = db2_fetch_assoc($stmtDC);
    db2_free_stmt($stmtDC);
    if ($rowDC)
        $k_lista = rtrim($rowDC['CDCDLI']);
    return $k_lista;
}



function artd_ARART_add_where($radice){
    $radice = str_replace(" ", "_", $radice);
    $radice = str_replace("?", "_", $radice);
    
    if (strlen($radice) == 0)
        return "";
    $exp_radice = explode("|", $radice);
    if (count($exp_radice) == 1)
        return " AND ARART LIKE '{$radice}%' ";
        else { //ho valore divisi da | pipe
            $ar_tmp = array();
            foreach($exp_radice as $r){
                $ar_tmp[] = " ARART LIKE '{$r}%' ";
            }
            return " AND (" . implode(" OR ", $ar_tmp) . ") ";
        }
}


function show_conf_radice($row, $val){
    $da = $row['CDSTIN'];
    $a  = $row['CDSTFI'];
    if ((int)$da == 0 || (int)$a == 0) return '';
    return trim($row['CDSTIN']). " - " .trim($row['CDSTFI']).", ". trim($val);
}


function crea_nuova_radice($radice_temp, $radice, $da, $a){
    
    if ((int)$da == 0 || (int)$a == 0) return $radice_temp;
    
    $ret = '';
    $radice_orig = str_split($radice_temp);
    $radice_app = str_split(str_repeat(" ", $da - 1) . $radice);
    
    for ($i=0; $i<100; $i++){
        
        if ($i+1 >= $da && $i+1 <= $a)
            $ret .= $radice_app[$i];
            else if(isset($radice_orig[$i]))
                $ret .= $radice_orig[$i];
                else
                    $ret .= ' ';
    }
    
    return trim($ret);
}
