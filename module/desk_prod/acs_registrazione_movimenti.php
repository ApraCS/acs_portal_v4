<?php
require_once("../../config.inc.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_todolist_preview_include.php';

$main_module = new DeskProd();
$s = new Spedizioni();
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();


if ($_REQUEST['fn'] == 'exe_save_nt'){
    
    $ar_ins = array();
    $nota = $m_params->nota;
    
    $ar_ins['NTMEMO'] 	= $nota;  
    
    if(trim($nota) == '' ){
        if(trim($m_params->rec->nt_rrn) != ''){
            $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_note']} NT
                    WHERE RRN(NT) = '{$m_params->rec->nt_rrn}'";
         
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
        }
    }else{
        if(trim($m_params->rec->nt_rrn) == ''){
            
            $ar_ins['NTDT']   = $id_ditta_default;
            $ar_ins['NTKEY1'] = $m_params->rec->REART;
            $ar_ins['NTKEY2'] = $m_params->rec->programma_prod;
            $ar_ins['NTSEQU'] = 0;
            $ar_ins['NTTPNO'] = 'ASME5';
            
            $sqlMemo_f =  "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                           (" . create_name_field_by_ar($ar_ins) . ")
                           VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmtMemo_f = db2_prepare($conn, $sqlMemo_f);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtMemo_f, $ar_ins);
            echo db2_stmt_errormsg();
        }else{
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} NT
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(NT) = '{$m_params->rec->nt_rrn}'";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
        }
      
        
    }
     $ret = array();
     $ret['success'] = true;
     echo acs_je($ret);
     exit;
     
}

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'exe_movimentazione'){
//----------------------------------------------------------------------

    $ritime     = microtime(true);
    
    foreach($m_params->list_selected_id as $v){
        
        $oe = $s->get_ordine_by_k_ordine($v->k_ordine);
   
        $anno = substr($oe['TDOADO'], 2, 2);
        $numero = trim($oe['TDONDO']);
        $tipo = trim($oe['TDOTPD']);
        
        $ar_ins = array();
        $ar_ins['WSGINDR'] = $anno.$numero.$tipo;
        $ar_ins['WSGTIDO'] = 'VP';
        $ar_ins['WSGTPDO'] = 'MV';
        $ar_ins['WSGDTRG'] = oggi_AS_date();
        $ar_ins['WSGAADO'] = date('Y');
        $ar_ins['WSGART']  = $v->REART;
        $ar_ins['WSGUM']   = $v->REUM;
        $ar_ins['WSGQTA']  = sql_f($v->REQTA);  
        $ar_ins['WSGCCON'] = $m_params->intestatario;
        $ar_ins['WSGNOTE4']  = "*PR".sprintf("%015s", strtr($ritime, array("." => ""))).trim($m_params->id_prog);
        
        if(count($m_params->deposito) > 1){
            $ar_ins['WSGDEPO']  = $m_params->deposito[0];  
            $ar_ins['WSGDEPD']  = $m_params->deposito[1]; 
        }else{
            $ar_ins['WSGDEPO']  = $m_params->deposito[0];  
        }

        if(isset($m_params->form_values)){
            foreach($m_params->form_values as $k1=>$v1){
                if($v1 > 0){
                    $ar_k = explode('_', $k1);
                    $ar_ins['WSGCAMO']  = $ar_k[1];
                    $ar_ins['WSGQTA2']  = sql_f($v1);
                    
                    $sql = "INSERT INTO {$cfg_mod_DeskProd['file_gen_docu']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                    
                    $stmt = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt, $ar_ins);
                    echo db2_stmt_errormsg($stmt);
                }
            }
            
        }else{
            
            $ar_ins['WSGCAMO']  = $m_params->causale;
            $ar_ins['WSGQTA2']  = sql_f($v->qta_mov);
            
           
            $sql = "INSERT INTO {$cfg_mod_DeskProd['file_gen_docu']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            
            
        }
        
       
    }
    
    
    $sh = new SpedHistory($main_module);
    $ret_RI = $sh->crea(
        'pers',
        array(
            "RITIME"    => $ritime,
            "messaggio"	=> 'GEN_MOV_PROD',
        )
        );
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}



//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//----------------------------------------------------------------------
    $pp_colli = _pp_get_colli($main_module, $m_params->params, $m_params->params->preview);
    $ar_colli = $pp_colli['ar_colli'];
    $rec = $ar_colli[0];
    
    
    //recupero record AS e k_lista
    $rowAS   = get_as_row_by_id($main_module, $m_params->params->open_request->id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    
    
    $pp_compo = _pp_get_collo_compo($main_module, $k_lista, $rec,
        array('group_by' => 'ART', 'DIM' => 'N'));
    $ar = $pp_compo['ar_compo'];
    
    $k_distinta = $m_params->params->open_request->k_distinta;
    $ar_distinta = explode('|', $k_distinta);
    
    $sqlMemo_f =  "SELECT RRN(NT) AS RRN, NTMEMO
    FROM {$cfg_mod_Spedizioni['file_note']} NT
    WHERE NTDT = '{$id_ditta_default}' AND NTTPNO = 'ASME5' AND NTSEQU=0
    AND NTKEY1 = ? AND NTKEY2 = '{$ar_distinta[0]}'
    ";
    
    
    $stmtMemo_f = db2_prepare($conn, $sqlMemo_f);
    echo db2_stmt_errormsg();
    
    foreach($ar as $k=>$v){
        $sh = new SpedHistory($main_module);
        $ret_RI = $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'GIAC_ART',
                "vals" => array(
                    'RICDAR' => $v['REART'],
                    'RISELO' => $m_params->deposito
                )
            )
            );
        $ar[$k]['giacenza'] = $ret_RI['RIQTA'];
        $ar[$k]['programma_prod'] = $ar_distinta[0];
      
        $result = db2_execute($stmtMemo_f, array($v['REART']));
        echo db2_stmt_errormsg();
        $row = db2_fetch_assoc($stmtMemo_f);
        $ar[$k]['indicazioni'] = $row['NTMEMO'];
        $ar[$k]['nt_rrn'] = $row['RRN'];
    }
    
    echo acs_je($ar);
    exit;
}

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_grid'){
//----------------------------------------------------------------------
    //recupero record AS e k_lista
    $rowAS   = get_as_row_by_id($main_module, $m_params->open_request->id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    $rowCD   = get_cd_row_by_id($main_module, $rowAS['ASORD4']);
    
    $pp_colli = _pp_get_colli($main_module, $m_params,  $m_params->preview);
    $ar_colli = $pp_colli['ar_colli'];
    $rec = $ar_colli[0];
    
    if($m_params->report == 'eti_gru01'){
        $title = "{$rec['TDONDO']} - {$rec['PLNCOL']}";
    }else{
        $title = $ar_causale[0];
    }
    
    
    //Se e' specificato un ordinamento
    $parametri_formato = _pp_parametri_formato($rowCD['CDPAGD']);
    $parametri_int =  _pp_parametri_formato($rowCD['CDPAUS']);
    $ar_causale = explode(',', $parametri_formato['CM']);
    $row_causale = get_TA_sys('MUMV', $ar_causale[0]);
    $t_causale = "[{$title}] ".trim($row_causale['text']);
    
    $ar_deposito = explode(',', $parametri_formato['DP']);
    
?>
{"success":true, 
	m_win: {
		title: 'Registrazione movimenti di magazzino ' + <?php echo j($t_causale)?>,
		width: 700, height: 400,
		iconCls: 'icon-box_open-16'
	}, 
	items: 
	{
				xtype: 'grid',
				flex: 1,
		        useArrows: true,
		        autoScroll : true,
		        rootVisible: false,
		        loadMask: true,
		        plugins: [
        		          Ext.create('Ext.grid.plugin.CellEditing', {
        		            clicksToEdit: 1,
        		             listeners:{
                	        	afteredit: function(cellEditor, context, eOpts){
                		        	var value = context.value;
                		        	var grid = context.grid;
                		        	var record = context.record;			        	
                	        	   	Ext.Ajax.request({
                			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_nt',
                			            method: 'POST',
                	        		    jsonData: {
                			      			   nota : value,
                							   rec : record.data				      			  
                			      			   }, 					            
                			            success: function ( result, request) {
                			                var jsonData = Ext.decode(result.responseText);
                			                record.set('indicazioni', value);
                			   				record.commit();		                															
                			            },
                			            failure: function (result, request) {
                			            }
                			        });
                	        	}
                			 }
        		            })
        		      	],
		        selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
		   		store: {
					xtype: 'store',
					autoLoad:true,
					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
		                       extraParams: {
		                       
		                           params : <?php echo acs_je($m_params) ?>,
		                           parametri : <?php echo acs_je($parametri_formato); ?>,
		                           deposito : <?php echo j($ar_deposito[0]); ?>
		                           
		                       
		                       }
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['REART', 'REDART', 'REUM', 'REQTA', 'k_ordine', 'qta_mov', 'giacenza', 'indicazioni', 'programma_prod', 'nt_rrn'],
			
			}, //store
	        <?php $warn = "<img src=" . img_path("icone/48x48/warning_blue.png") . " height=20>"; ?>
	         
				columns: [ 
				         {
			                header   : 'Articolo',
			                dataIndex: 'REART', 
			                width: 80
			             },
			              {
			                header   : 'Descrizione',
			                dataIndex: 'REDART', 
			                flex: 1
			             },
			              {
			                header   : 'UM',
			                dataIndex: 'REUM', 
			                width: 40
			             },
			              {
			                header   : 'Q.t&agrave;',
			                dataIndex: 'REQTA', 
			                width: 40,
			                renderer : floatRenderer2
			             },
			             {
			                header   : 'Q.t&agrave; movim.',
			                dataIndex: 'qta_mov', 
			                width: 80,
			                editor: {
                	                xtype: 'textfield',
                	                allowBlank: true
                	            }
			             },
			             
			             { //preview
    		            xtype:'actioncolumn', width:35,
    		            text: '<img src=<?php echo img_path("icone/16x16/warning_blue.png") ?> >',
    		            tdCls: 'tdAction', tooltip: 'Movimentazioni fuori programma',
    		            menuDisabled: true, sortable: false,
    		            items: [{
    		               icon: <?php echo img_path("icone/16x16/warning_blue.png") ?>,
    		               handler: function(view, rowIndex, colIndex, item, e, record, row) {
    		                       acs_show_win_std(null,
			        				'acs_registrazione_movimenti.php?fn=open_form', {
			        				   causali : <?php echo acs_je($ar_causale); ?>,
			        				   rec : record.data,
			        				   id_prog : <?php echo j($rowAS['ASIDPR'])?>,
			        				   deposito : <?php echo j($ar_deposito)?>,
			        				   intestatario : <?php echo j($parametri_int['INT'])?>
			        				});	                	
    		                }
    		            }]
    		          },
			           
        			    {
			                header   : 'Giacenza',
			                dataIndex: 'giacenza', 
			                width: 70,
			                align: 'right',
			                renderer : floatRenderer2
			               
			             }, 
			             {
			                header   : 'Indicazioni',
			                dataIndex: 'indicazioni', 
			                width: 80,
			                editor: {
                	                xtype: 'textfield',
                	                allowBlank: true,
                	                maxLength : 20
                	            }
			             },     
			       
				
				],
				  dockedItems: [{
                    dock: 'bottom',
                    xtype: 'toolbar',
                    scale: 'large',
                    items: [
                        '->',
                        {
                         xtype: 'button',
                        text: 'Conferma movimentazione',
    		            scale: 'medium',	                     
    					iconCls: 'icon-button_blue_play-24',
    		          	handler: function() {
    		          	
    		          	     var grid = this.up('grid');
    	       			     var loc_win = this.up('window');
				                 var records =  grid.getSelectionModel().getSelection();  
				           		 list_selected_id = [];                           
                                 for (var i=0; i<records.length; i++) 
                            	 list_selected_id.push(records[i].data);
                            	 
                            	 	Ext.Ajax.request({
         						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_movimentazione',
         						        method  : 'POST',
         			        			jsonData: {
         			        			   causale : <?php echo j($ar_causale[0])?>,
         			        			   intestatario : <?php echo j($parametri_int['INT'])?>,
         			        			   deposito : <?php echo j($ar_deposito)?>,
         			        			   list_selected_id: list_selected_id,
         			        			   id_prog : <?php echo j($rowAS['ASIDPR'])?>
         								},
         								
         								success: function(response, opts) {
        						        	 var jsonData = Ext.decode(response.responseText);
        						        	 acs_show_msg_info('Operazione completata');
        			            		   
        			            		},
        						        failure    : function(result, request){
        						            Ext.Msg.alert('Message', 'No data to be loaded');
        						        }
         								
         						    });
    	       			    
         			     
    		           
    			
    			            }
    
    			     },
                    ]}],
				viewConfig: {
    				getRowClass: function(record, index) {	
    		           var v = '';
    		           //if (record.get('ERCAMP').trim() == 'ARDART') 
    		           //	v = v + ' grassetto';
    		           	
    		           	return v;																
    		         }  
				
				}
				
			
	    		
	 	}
       
}
<?php 
	exit;
}


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//----------------------------------------------------------------------
    $articolo = "[".$m_params->rec->REART ."] ".$m_params->rec->REDART;
    
?>
{"success":true, 
m_win: {
		title: 'Movimentazioni fuori programma ' + <?php echo j($articolo); ?>,
		width: 530, height: 300,
		iconCls: 'icon-warning_blue-16'
	}, 
"items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	             {
    					name: 'quantita',
    					fieldLabel : 'Quantit&agrave;',
    					xtype: 'displayfield',
    					labelWidth : 200,
    					anchor: '-15',		
    					margin : '0 5 5 300',
    					labelSeparator : ''			
				   },
	            
		        <?php 
		            
		            foreach($m_params->causali as $v){
		                $row_causale = get_TA_sys('MUMV', $v);
		           ?>     
		           
		           {
    					name: 'q_<?php echo $v?>',
    					fieldLabel : '<?php echo $row_causale['text']?>',
    					xtype: 'numberfield',
    					hideTrigger : true,	
    					decimalPrecision : 2,
    					labelWidth : 200,
    					anchor: '-15'						
				   },
		                
		             
		            <?php    
		                
		            } ?>
        	    
	            ],
	            
	            
				buttons: [	
				  {
			            text: 'Conferma',
				        scale: 'large',
                        iconCls: 'icon-button_blue_play-32',		            
			            handler: function() {
			             var form = this.up('form').getForm();
			             if (form.isValid()){
			             
			                var list_selected_id = [];
			                list_selected_id.push(<?php echo acs_je($m_params->rec); ?>);
			                
			             
                    	 	Ext.Ajax.request({
 						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_movimentazione',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   form_values : form.getValues(),
 			        			   list_selected_id: list_selected_id,
 			        			   id_prog : <?php echo j($m_params->id_prog)?>,
 			        			   deposito : <?php echo acs_je($m_params->deposito)?>,
 			        			   intestatario : <?php echo j($m_params->intestatario)?>
 								},
 								
 								success: function(response, opts) {
						        	 var jsonData = Ext.decode(response.responseText);
						        	 acs_show_msg_info('Operazione completata');
			            		     this.up('window').destroy();
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });	
			                	
        				 }
			               
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}


<?php }?>