<?php


$_stato_ticket_tab = array(
  '100' => 'Generato',
  '200' => 'Avanzato',
  '300' => 'Sospeso',
  '900' => 'Chiuso',
  '950' => 'Chiusura forzata',
  '999' => 'Annullato'
);

$_stato_decodifica = array(
    'MAN_I' => 'Generato',
    'MAN_F' => 'Terminato',
    'MAN_S' => 'Sospeso',
    'MAN_R' => 'Ripreso'
);

$_ar_stati_aperti = array('100', '200', '300');
