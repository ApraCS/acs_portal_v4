<?php
require_once "../../config.inc.php";
require_once 'acs_panel_indici_include.php';

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data_indici'){
//----------------------------------------------------------------------
    $ar = get_data_indici($main_module);
    echo acs_je($ar);
    exit;
}




//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_indici'){
//----------------------------------------------------------------------
 //parametri ammessi:
 //   seleziona_progetto: 'Y', //mostra il bottone "Seleziona"
 //   modifica: 'N' (abilita o meno le funzioni di 'Configurazione distinte' ....
?>
     {
        success:true,
        m_win: {
        	title: 'Tipologia programmi di produzione',
        	height: 400, width: 400,
        	iconCls: 'icon-bookmark-16'
        },
        items: [
        
        	{
						xtype: 'grid',
						title: '',
						flex: 1,
						itemId : 'g_indici',
				        loadMask: true,
				        
				        <?php if ($m_params->modifica != 'N') { ?>	
				          bbar: new Ext.Toolbar({
            	            items:[
            	            {
            	            
            	             iconCls: 'icon-gear-16',
            	             tooltip: 'Tipologie programmi di produzione',
            	             handler: function(b, toolEl, panel){
            	             			acs_show_panel_std('acs_panel_indici_gest.php?fn=open_tab');
            	                    	b.up('window').destroy();            			                      		           		 
            		           		 }
            		         },
            		         '->'
            		         , {
            	            text : 'Report',
            	            iconCls: 'icon-print-16',
            	             handler: function(event, toolEl, panel){
            	                    	acs_show_win_std('Parametri', 
            	                    		'acs_report_programmi_produzione.php?fn=open_form', {}, 400, 250, null, 'icon-leaf-16');            			                      		           		 
            		           		 }
            		         }
            	         ]            
	      			  }),
	      			  <?php } ?>
	      			  
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_indici', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {read: 'POST'},							        
							       extraParams: {},
			        				
			        			   doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		       fields: ['codice', 'desc', 'att']							
									
			}, //store
				columns: [	
				 <?php echo dx_mobile() ?>
			      , {header   : 'Codice', dataIndex: 'codice', width : 70}
	              , {header   : 'Descrizione', dataIndex: 'desc', flex: 1}	                
	         ]
	         
	         , listeners: {
	       			  celldblclick: {
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  var grid = this;
							  var m_form = grid.down('#init_form');
							  if (!Ext.isEmpty(m_form)){
							  	var form = grid.down('#init_form').getForm();
							  	var form_values = form.getValues(); 
							  } else {
							  	form_values = {};
							  }
							  <?php if ($m_params->modifica != 'N') {?>
        			  		  acs_show_win_std(null, 
        			  		 	'acs_panel_indici_naviga.php?fn=open_config', {
                        			cod: rec.get('codice'), desc : rec.get('desc'), 
                        			att : rec.get('att'), form_values : form_values});
                        		this.up('window').close();
                        	<?php }?>
     						 
						  }
			   		  },  
			   		  
			   		  
			   		   itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  									
						<?php if ($m_params->modifica != 'N') {?>				  													  
						  var voci_menu = [];
						 	 voci_menu.push({
			         		text: 'Configurazione distinte',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
			        			acs_show_panel_std('acs_panel_indici_config_distinta.php?fn=open_tab', null, {
                                    	cod: rec.get('codice'), desc : rec.get('desc')
                                    	}, null, null);
                             	grid.up('window').destroy();       	
				            }
			    		   });
			    		 <?php } ?>  	 
			    		
			    		   var menu = new Ext.menu.Menu({
				            items: voci_menu
					        }).showAt(event.xy);	
						  
						  }
	         
				   
				 }
				 
		
		<?php if ($m_params->seleziona == 'Y'){ ?>
			, buttons: [{
	            text: 'Conferma',
	            iconCls: 'icon-button_blue_play-32',	
	            scale: 'large',
	            handler: function() {
	            	//ritorno il codice del progetto a chi mi ha chiamato
					var loc_grid = this.up('grid'),
	                    loc_win  = this.up('window'),
                    	rec_selected = loc_grid.getSelectionModel().getSelection();
                    	
                    	if( rec_selected.length == 0){
                    		acs_show_msg_error('Selezionare un progetto');
                    	} else {
                    		loc_win.fireEvent('recordSelected', loc_win, rec_selected[0].data);
                    	}	            	
	            }
	        }] 
		<?php } ?>
		
		
		
		} //grid        
     ]
  }
<?php
}
exit;
