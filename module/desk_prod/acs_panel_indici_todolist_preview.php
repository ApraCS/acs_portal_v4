<?php

require_once("../../config.inc.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_todolist_preview_include.php';

$main_module = new DeskProd();

$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_content'){
//----------------------------------------------------------------------

    $rowAS   = get_as_row_by_id($main_module, $m_params->open_request->id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    
    $rowCD   = get_cd_row_by_id($main_module, $rowAS['ASORD4']);
    
    //dal formato di uscita recupero il report/procedura da richiamare
    $row_formato = $main_module->get_TA_std('CDFMU', $rowCD['CDFOUS']);
    
    $report_procedura = rtrim($row_formato['TALOCA']);
    $report_procedura_class_name = "FR_" . strtoupper($report_procedura);
    
    require_once("formati_report/base.php");
    require_once("formati_report/{$report_procedura}.php");
    $r = new $report_procedura_class_name($main_module,
        array(
            'rowAS' => $rowAS,
            'k_lista' => $k_lista,
            'rowCD' => $rowCD,
            'parametri_formato' => _pp_parametri_formato($rowCD['CDPAUS'])
        ));
    
    
    //ToDo: potrebbe essere per colli, o lotto, o carico...
    $pp_colli = _pp_get_colli($main_module, $m_params, $m_params->preview, $r);
    
    $ar_colli = $pp_colli['ar_colli'];
    
    foreach($ar_colli as $k=>$v){
        $ar_colli[$k]['ASDTAS'] = $rowAS['ASDTAS'];
        $ar_colli[$k]['ASHMAS'] = $rowAS['ASHMAS'];
        $ar_colli[$k]['ASORD1'] = $rowAS['ASORD1'];
        $ar_colli[$k]['ASORD2'] = $rowAS['ASORD2'];
        $ar_colli[$k]['ASORD3'] = $rowAS['ASORD3'];
        $ar_colli[$k]['d_voce'] = $m_params->open_request->d_voce;
    }
    
    //$html .= '<pre>SQL: ' . $pp_colli['sql'] . '</pre>';
    //$html .= '<pre>SQL params: ' . print_r($pp_colli['sql_params'], true) . '</pre>';    
    $html .= $r->html($ar_colli);    //ToDo: potrebbe non essere per colli
    
    echo $html;
 exit;
}







//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_tab'){
//----------------------------------------------------------------------
    //recupero record AS e k_lista
    $rowAS   = get_as_row_by_id($main_module, $m_params->id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    if (strlen($k_lista) > 0)
        $title_suff = "Filtro lista: {$k_lista}";
            
    $progetto = $m_params->k_progetto;
    $ar_gruppo = explode("|", $m_params->k_distinta);
    $d_voce = $m_params->d_voce;
    $title = "{$progetto}, {$ar_gruppo[1]}, {$ar_gruppo[2]}: {$d_voce}";
    
    $rowCD   = get_cd_row_by_id($main_module, $rowAS['ASORD4']);
    
    $parametri_formato = _pp_parametri_formato($rowCD['CDPAUS']);
    
    //dal formato di uscita recupero il report/procedura da richiamare
    $row_formato = $main_module->get_TA_std('CDFMU', $rowCD['CDFOUS']);
    
    //eventuale tipo di avanzamento
    if (strlen(trim($row_formato['TASITI'])) > 0) {
        $row_tipo_avanzamento = $main_module->get_TA_std('CDTAV', $row_formato['TASITI']);
        $etic_avanzamento = $row_tipo_avanzamento['TAINDI'];
    } else {
        $row_tipo_avanzamento = array();
        $etic_avanzamento = 'Collo';
    }
    
    
    $report_procedura = rtrim($row_formato['TALOCA']);
    $report_procedura_class_name = "FR_" . strtoupper($report_procedura);
    
    require_once("formati_report/base.php");
    require_once("formati_report/{$report_procedura}.php");
    $r = new $report_procedura_class_name($main_module, array('parametri_formato' => _pp_parametri_formato($rowCD['CDPAUS'])));
    
    
    /*per debug */
    $title .= ' [' . implode(', ', array(trim($rowCD['CDFOUS']), $report_procedura)) . ']';
    
    //forzo paginazione
    if (property_exists($r, 'forza_paginazione_preview') && $r->forza_paginazione_preview == 'S') 
        $parametri_formato['PAG'] = $r->forza_paginazione_preview;
        
    if ($parametri_formato['PAG'] == 'S'){
        //recuper conteggio "paginazione"
        $colli_where = where_filtro_colli_by_k_lista($cfg_mod, $k_lista);
        $inner_join_filtro_compo = _pp_inner_pl_on_filtro_compo($cfg_mod, $k_lista);
        
        $sql_order_by = _pp_get_order_by($parametri_formato);
        $count_pag = _pp_get_count_paginazione($main_module, $progetto, $colli_where, $inner_join_filtro_compo, $sql_order_by, $r->paginazione_preview_per);
    }
    
    
    ?>	
	{
		success:true,	
		m_win: {
			title: <?php echo j($title); ?>,
			maximize: 'Y',
			iconCls : 'icon-module-16'
		},
		items: [
				{
					xtype: 'panel'
					, page: 1
					, pages: <?php echo j($count_pag) ?>
					, flex: 1
					, autoScroll: true
			        , items: [
			        	{
			        	 xtype: 'panel',
			        	 autoScroll: true,
			        	 flex: 1,
			        	 padding: 10,
			        	 html: '<h1>Loading ...</h1>',
			        	 itemId: 'main-content',
			        	 border: false,
			        	 }
			        ]
			        
			        

        	        , listeners: {
        	        	afterRender: function(comp){
        	        		
        	        		//carico il contenuto HTML
        	        		Ext.Ajax.request({
 						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_content',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   open_request: <?php echo acs_je($m_params); ?>,
 			        			   preview: {
 			        			   	 <?php if ($parametri_formato['PAG'] == 'S'){ ?>
 			        			      page: 1
 			        			     <?php } ?> 
 			        			   }
 			        			},
 								
 								success: function(response, opts) {
						        	 //accodo HTML ricevuto
						        	 var main_content = comp.down('#main-content');
						        	 main_content.update(response.responseText);
						        	 
			            		   
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
        	        		
        	        	}
        			}//listeners
        				 
				 , acs_actions: {
				    
				    get_page: function(win){
				        var main_content = win.down('#main-content');
				        Ext.Ajax.request({
 						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_content',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   open_request: <?php echo acs_je($m_params); ?>,
 			        			   preview: {
 			        			      page: win.down('panel').page
 			        			   } 
 								},
 								
 								success: function(response, opts) {
						        	 //accodo HTML ricevuto
						        	 main_content.update(response.responseText);
						       },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
				    
				    }
				 
				 }
		
				 
        			, buttons: [
        			
        			  <?php if ($row_formato['TAFG04'] == 'Y'){ ?>
        			  {
    		            text: 'Segnalazioni',
    			        iconCls: 'icon-warning_black-32',	            
    			        scale: 'large',		
    			        handler: function(b) {
    			        	acs_show_win_std(null, 'acs_segnalazioni.php?fn=open_panel', {open_request: <?php echo acs_je($m_params); ?>});
    		            	
    		            }
    		          },
    		          <?php } ?>
    		          
    		          <?php if ($row_formato['TAFG03'] == 'Y'){ ?>
    		          {
    		            text: 'Stampa/Ristampa',
    			        iconCls: 'icon-print-32',	            
    			        scale: 'large',		
    			        handler: function(b) {
    		            	
    		            	var main_content = b.up('window').down('#main-content');
    		            	
    		            	var win = window.open('', 'Print Panel');
    		            	win.document.open();
                            win.document.write(main_content.html);
                            win.document.close();
    		            	win.print();
    		            	win.close();
    		            }
    		          },
    		          <?php } ?>
    		          
    		          <?php if ($row_formato['TAFG02'] == 'Y'){ ?>
    		          {
    		            text: 'Vista ordine',
    			        iconCls: 'icon-design-32',	            
    			        scale: 'large',		
    			        handler: function(b) {
    			        var m_win = b.up('window');
    			        var preview = {page: m_win.down('panel').page};
    			        
    			        	acs_show_win_std(null, 'acs_vista_ordine.php?fn=open_panel', {open_request: <?php echo acs_je($m_params); ?> 
 			        			   , preview : preview});
    		             }
    		          },
    		          <?php } ?>
    		          
    		          
    		          <?php if ($row_formato['TAFG01'] == 'Y'){ ?>
    		          {
    		            text: 'NON Disponibile',
    			        iconCls: 'icon-sub_black_accept-32',	            
    			        scale: 'large',		
    			        handler: function(b) {
    		            	console.log(b);
    		            	if(b.text == 'Disponibile'){
    		            		b.setText('NON Disponibile');
    		            		b.setIconCls('icon-sub_black_accept-32');
    		            	}else{
    		            		b.setText('Disponibile');
    		            		b.setIconCls('icon-check_green-32');
    		                }
    		            }
    		          },
    		          <?php }?>
    		          
    		          <?php if ($row_formato['TAFG05'] == 'Y'){ ?>
    		          {
    		            text: 'Movimenti',
    			        iconCls: 'icon-box_open-32',	            
    			        scale: 'large',		
    			        handler: function(b) {
    			            var m_win = b.up('window');
    			            var preview = {page: m_win.down('panel').page};
    		            	acs_show_win_std(null, 'acs_registrazione_movimenti.php?fn=open_grid', {open_request: <?php echo acs_je($m_params); ?>, preview: preview, report : <?php echo j($report_procedura)?> });
    		            }
    		          },
    		          <?php } ?>
    		
        			'->'
        			<?php if ($parametri_formato['PAG'] == 'S'){ ?>
            		  
            		  
            		  ,{
    		            text: 'Indietro',
    			        iconCls: 'icon-button_grey_first-32',	            
    			        scale: 'large',		
    			        handler: function(b) {
    			        	var m_win = b.up('window');
    			        	
    			        	if (m_win.down('panel').page > 1) {
        			        	m_win.down('panel').page -=1;
        			        	m_win.down('#b_page').setText('<span style = "font-size: 12px; font-weight: bold;"><?php echo $etic_avanzamento ?> ' + m_win.down('panel').page + '</span>');    	
        		                m_win.down('panel').acs_actions.get_page(m_win);
        		            }
    		            }
    		          }, {
    		          	text: <?php echo j("{$etic_avanzamento} 1")?>,
    		          	scale: 'large',
		          	    handler: function(b) {
			        	var m_win = b.up('window');
			        	
			        	if (m_win.down('panel').page > 1) {
    			        	m_win.down('panel').page = 1;
    			        	m_win.down('#b_page').setText('<span style = "font-size: 12px; font-weight: bold;"><?php echo $etic_avanzamento ?> ' + m_win.down('panel').page + '</span>');    	
    		                m_win.down('panel').acs_actions.get_page(m_win);
    		            }
		            }
    		          }, {
    		          	text: <?php echo j("<span style = 'font-size: 12px; font-weight: bold;'>{$etic_avanzamento} 1</span>")?>,
    		          	scale: 'large',
    		          	itemId: 'b_page',
    		          		handler: function(b) {
			        		  var m_win = b.up('window');
			        		  var preview = {page: m_win.down('panel').page};
			        			<?php if(trim($etic_avanzamento) == 'Collo'){  ?>
			        			    acs_show_win_std(null, 'acs_collo_immagine.php?fn=open_panel', {open_request: <?php echo acs_je($m_params); ?> 
 			        			   , preview : preview, params : <?php echo acs_je($parametri_formato); ?>});
			        			<?php }?>
    		            
		               }
    		          }, {
    		          	text: <?php echo j("{$etic_avanzamento} {$count_pag}")?>,
    		          	scale: 'large',
    		          	style:{ "font-size": '50px'},
    		          	handler: function(b) {
			        		var m_win = b.up('window');
			        	
    			        	m_win.down('panel').page = <?php echo $count_pag; ?>;
    			        	m_win.down('#b_page').setText('<span style = "font-size: 12px; font-weight: bold;"><?php echo $etic_avanzamento ?> ' + m_win.down('panel').page + '</span>');    	
    		                m_win.down('panel').acs_actions.get_page(m_win);
    		            
		               }
    		          }, {
    		            text: 'Avanti',
    			        iconCls: 'icon-button_grey_last-32',	            
    			        scale: 'large',		
    			        handler: function(b) {
    			          var m_win = b.up('window');
    			          if (m_win.down('panel').page < m_win.down('panel').pages) {
    		            	m_win.down('panel').page +=1;
    		            	m_win.down('#b_page').setText('<span style = "font-size: 12px; font-weight: bold;"><?php echo $etic_avanzamento ?> ' + m_win.down('panel').page +'</span>');    		            	
    		            	m_win.down('panel').acs_actions.get_page(m_win);
    		              }	
    		            }
    		          }		
    		          <?php } ?>		
                    ]    

		  	} //panel
		
		]
	}
		
<?php 
	exit;
}
