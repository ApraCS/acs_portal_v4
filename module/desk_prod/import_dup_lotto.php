<?php

/*
/usr/local/zendphp7/bin/php-cli /www/zendphp7/htdocs/acs_portal_dev/module/desk_prod/import_dup_lotto.php dup 1589471223.2390
  
$argv: Array                                                                                   
(                                                                                       
    [0] => /www/zendphp7/htdocs/acs_portal_dev/module/desk_prod/import_dup_lotto.php    
    [1] => dup                                                                          
    [2] => 1333.345                                                                     
)                                                                                       
*/

$disabilita_auth = 'Y';
require __DIR__ . '/../../config.inc.php';
$s = new Spedizioni(array('no_verify' => 'Y'));
$fn = $argv[1];

//esco se non sono nel caso 'FROM_PC'
if ($cfg_mod_Spedizioni["visualizza_order_img"] != 'FROM_PC')
    exit;


/**************************************************************************************/
if ($fn == 'dup'){
    //ricevo ritime (WPI5RI0, a fronte di DUP_LOTTO).
    //da RI leggo i parametri (LOTTO). Per ogni ordine duplico in locale le immagini
    $ritime = $argv[2];
    echo "\nRitime: {$ritime}";
    
    $sql = "SELECT * FROM {$cfg_mod_DeskProd['file_richieste']} WHERE RITIME = ?";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($ritime));
    $rowRI = db2_fetch_assoc($stmt);
    db2_free_stmt($stmt);
    
    //recupero elenco ordini abbinati a lotto
    $sql = "SELECT TDDOCU FROM {$cfg_mod_DeskProd['file_testate']} WHERE TDTPLO=? AND TDAALO=? AND TDNRLO=? AND TDDTOR=?";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($rowRI['RITPLO'], $rowRI['RIAALO'], $rowRI['RINRLO'], $rowRI['RIDTOR']));
    
    while ($row = db2_fetch_assoc($stmt)){
        echo "\n" . $row['TDDOCU'];
        
        $k_ordine = $row['TDDOCU'];
        $oe = $s->k_ordine_td_decode_xx($k_ordine);
        
        $ordine_dir = implode('_', array_map('trim', $oe));
        $ordine_dir_assoluto = $inc_path . '/../' . $cfg_mod_DeskProd['dir_duplica'] . '/' . $ordine_dir;
        
        //creo cartella se non esiste
        if (!is_dir($ordine_dir_assoluto)){
            mkdir($ordine_dir_assoluto, 0777, true);
            
            $my_url_p =  http_build_query(array('ord' => $k_ordine));
            $json = file_get_contents("{$cfg_mod_Spedizioni["url_img_FROM_PC"]}?{$my_url_p}");
            
            $ar_files = json_decode($json);
            foreach($ar_files as $file_def){
                _save_file($k_ordine, $file_def, $ordine_dir_assoluto);
            }
            
        } //if !is_dir
        
        
    }
    db2_free_stmt($stmt);
    
    
    echo "\nFine\n";
}



/**************************************************************************************/
if ($fn == 'del'){
    //ToDo: servira' eliminare le cartelle degli ordini dopo qualche giorno
}






/* esempio di file_def:
 * [38] => stdClass Object                                                         
    (                                                                           
        [DataCreazione] => 17/02/2020                                           
        [IDOggetto] => E:\Share\DIS3CAD/01/10/011065/011065_etic_art_16V7K.JPG  
        [des_oggettodes_oggetto] => 011065_etic_art_16V7K.JPG                              
        [tipo_scheda] => 4. Allegati grafici                                    
    )                                                                           
 */

function _save_file($k_ordine, $file_def, $to_dir){
    global $cfg_mod_Spedizioni;
    $file_path =  $file_def->IDOggetto;
    $my_url_p =  http_build_query(array('IDOggetto' => $file_path, 'function' => 'view_image'));
    
    //interrogo il server con le immagini
    $cont_file = file_get_contents("{$cfg_mod_Spedizioni["url_img_FROM_PC"]}?{$my_url_p}");
    
    $to_file = $to_dir . '/' . $file_def->des_oggetto;
    file_put_contents($to_file, $cont_file);
    echo "\nfile creato: {$to_file}";
}