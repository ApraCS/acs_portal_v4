<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();
$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// JSON GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_grid'){
?>	
	
{"success":true,

 m_win: {
   title: 'Elenco colli',
   width: 1000, height: 600
 },
 
 items:
 {
  xtype: 'grid',
  title: '',
  id: 'panel-alert',
  closable: false,
  autoScroll: true,
  features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true,
						startCollapsed: true
					}),    
 
					store: {
						xtype: 'store',

						listeners: {
				            load: function () {
				                //win.setTitle('Elenco corrente colli ordine ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO);
				            }
				         },
					
												
						autoLoad:true,				        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								timeout: 2400000, 
								jsonData: <?php echo acs_je($m_params); ?>,
								method: 'POST',								
								type: 'ajax',
								reader: {
						            type: 'json',
						            root: 'root'
						        },

								actionMethods: {
									read: 'POST'					
								},
														        
								extraParams: {open_request: <?php echo acs_je($m_params) ?>},	
								doRequest: personalizza_extraParams_to_jsonData			        
							},
		        			fields: [
		            			'gr_carico', 'TDDOCU', 'PLAADO', 'PLNRDO', 'k_ordine_out', {name: 'PLCOAB', type: 'int'}, 'PLART', 'PLDART', 'PLUM', 'PLQTA', 'PLDTEN', 'PLTMEN', 'PLDTUS', 'PLTMUS', 'PLDTRM', 'PLTMRM',
		            			'PSDTSC', 'PSTMSC', 'PSRESU', 'PSCAUS', 'PSPALM', 'PSLATI', 'PSLONG',
		        			], 
		        			groupField: 'gr_carico'
		    			},
			        columns: [			        			        
			            {
			                header   : 'Ordine',
			                dataIndex: 'k_ordine_out', 
			                width: 110,
			             }, {
			                header   : 'Nr collo', align: 'right',
			                dataIndex: 'PLCOAB', 
			                width: 50,
			             }, {
			                header   : 'Codice',
			                dataIndex: 'PLART', 
			                flex    : 50
				         }, {
			                header   : 'Descrizione',
			                dataIndex: 'PLDART', 
			                flex    : 100
			             }, {
			                header   : 'UM',
			                dataIndex: 'PLUM', 
			                flex    : 10
			             }, {
			                header   : 'Quantit&agrave;',
			                dataIndex: 'PLQTA', 
			                flex    : 30,
			                align: 'right'
			             }, {
			                header   : 'Disponibilit&agrave;',
			                dataIndex: 'PLDTEN', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTEN'), record.get('PLTMEN'))
			    			}
			             }, {
			                header   : 'Spedizione',
			                dataIndex: 'PLDTUS', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTUS'), record.get('PLTMUS'))
			    			}			                
			             }, {
			                header   : 'Aggiunte',
			                dataIndex: 'PLDTRM', 
			                flex    : 30,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('PLDTRM'), record.get('PLTMRM'))
			    			}			                			                
			             },{
				                header   : 'Scarico',
				                dataIndex: 'PSDTSC', 
				                flex    : 40,
				                renderer: function(value, metaData, record){
				                	metaData.tdAttr = 'data-qtip="Scanner: ' + Ext.String.htmlEncode(record.get('PSPALM')) + ' - Coordinate: ' + Ext.String.htmlEncode(record.get('PSLATI')) + ' - ' + Ext.String.htmlEncode(record.get('PSLONG')) + '"';
				                	return datetime_from_AS(record.get('PSDTSC'), record.get('PSTMSC'))
				    			}			                			                
				          }, {
				                header   : 'Info',
				                dataIndex: 'PSDTSC', 
				                width: 50,
				                renderer: function(value, metaData, record){

				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)
				                		metaData.tdAttr = 'data-qtip="Segnalazione allo scarico: ' + Ext.String.htmlEncode(record.get('PSCAUS')) + '"';
			                		
				                	if (Ext.isEmpty(record.get('PSRESU'))==false && record.get('PSRESU') == 'E')			    	
							    		return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)			    	
							    		return '<img src=<?php echo img_path("icone/48x48/comments.png") ?> width=18>';
				    			}			                			                
				          }
			         ],
			         
        listeners: {
	        itemclick: function(view,rec,item,index,eventObj) {
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');	        	        	

	        	wdi.store.proxy.extraParams.k_ordine = rec.get('TDDOCU');
				wdi.store.load();
	        	wdc.store.proxy.extraParams.k_ordine = rec.get('TDDOCU');
				wdc.store.load();				
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('TDDOCU'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				   }
				});
				
				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('TDDOCU')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('TDDOCU');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}				
	        }        	
        }			         
	 }
 
 
}  
  	
	
	
<?php	
	exit;
}


// ******************************************************************************************
// JSON GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
    ini_set('memory_limit', '2048M');
    $sql_WHERE = '';
    
    $filtro = (array)$m_params->open_request;
	 
	$stmt = $main_module->get_cols_ordine_adv($filtro);

	$data = array();
	while ($row = db2_fetch_assoc($stmt)) {
		$row['k_ordine_out'] = implode("_", array($row['PLAADO'], $row['PLNRDO'], $row['PLTPDO']));
		$row['gr_carico'] = implode("_", array($row['TDAACA'], $row['TDNRCA'], $row['TDTPCA']));
		$row['PLDART'] = acs_u8e($row['PLDART']);
		$data[] = $row;
	}	

	echo acs_je($data);
	
exit;
}
?>



