<?php 

require_once("../../config.inc.php");

$main_module = new DeskProd();
$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
        'module'      => $main_module,
        'tab_name' =>  $cfg_mod['file_tabelle'],
        't_panel' =>  "CDTPU - Tipo uscita",
        'descrizione' => "Tipo uscita",
        'form_title' => "Dettagli tabella tipo uscita",
        'fields_preset' => array(
            'TATAID' => 'CDTPU'
        ),
        'immissione' => array(
            'data_gen'   => 'TADTGE',
            'user_gen'   => 'TAUSGE',
            
        ),
    	
        'fields_key' => array('TAKEY1', 'TADESC'),
        'fields_grid' => array('TAKEY1', 'TADESC', 'immissione', 'TAFG01', 'TAFG02', 'TAFG03'),
        'fields_form' => array('TAKEY1', 'TADESC', 'TAFG01', 'TAFG02', 'TAFG03'),
		
		'fields' => array(				
		    'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
	            'TADESC' => array('label'	=> 'Descrizione'),
		    
		        //flag
		        'TAFG01' => array('label'	=> 'Vista colli inclusi', 'type' => 'checkbox', 'iconCls' => 'barcode'),
		        'TAFG02' => array('label'	=> 'Uscita a schermo', 'type' => 'checkbox', 'iconCls' => 'monitor'),
		        'TAFG03' => array('label'	=> 'Elaborazione batch', 'type' => 'checkbox', 'iconCls' => 'outbox'),
		    
	            //immissione
    		    'immissione' => array(
    		        'type' => 'immissione', 'fw'=>'width: 70',
    		        'config' => array(
    		            'data_gen'   => 'TADTGE',
    		            'user_gen'   => 'TAUSGE'
    		        )
    		        
    		    ),
    		    'TADTGE' => array('label'	=> 'Data generazione'),
    		    'TAUSGE' => array('label'	=> 'Utente generazione'),
		)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
