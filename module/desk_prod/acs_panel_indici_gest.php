<?php

require_once("../../config.inc.php");
$main_module = new DeskProd();

$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    
    't_panel'     => 'Tipologie programmi di produzione',
    'form_title' => 'Dettagli tipologie programmi di produzione',
    
    'fields_preset' => array(
        'TATAID' => $cfg_mod['taid_indici']
    ),

    'descrizione' => 'Distinte attivit&agrave; programmi di produzione',
    
    'fields_key' => array('TAKEY1'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice'),
        'TADESC' => array('label'	=> 'Descrizione'),
        'TAFG01' => array('label'	=> 'Attivit�', 'tooltip' => '<b>\'A\'</b>: Attivi&agrave;/Progetto<br/><br/>[TAFG01]'),
        'TACAP'  => array('label'	=> 'Logo', 'type' => 'from_TA', 'TAID' => 'LOGOP'),
    )
    
);

//require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
