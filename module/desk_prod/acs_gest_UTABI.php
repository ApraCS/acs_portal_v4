<?php 

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();
$main_module =  new DeskProd();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $desk_art->get_cfg_mod();

$m_table_config = array(
    'module'      => $desk_art,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "UTABI - Utenti abilitati",
    'descrizione' => "Utenti abilitati",
    'form_title' => "Dettagli utenti abilitati",
    'fields_preset' => array(
        'TATAID' => 'UTABI'
    ),
  
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice', 'fw'=>'width: 60', 'type' => 'user', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
      
       
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
        
        
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
