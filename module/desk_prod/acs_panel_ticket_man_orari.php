<?
require_once("../../config.inc.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// ASSEGNA LOTTO ORARI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_assegna_orari'){
    
    $form_values = $m_params->form_values;
    $list_selected = $m_params->open_request->list_selected_id;
    
  
    foreach($list_selected as $v){
        
        //$col_name = $m_params->open_request->col_name;
        //$ar_data = explode('_', $col_name);
        //$data = $ar_data[1];
        
        $data = $v->data;
        $ar_repma = explode('|', $v->rep_mac);
        
        $reparto = $ar_repma[0];
        $macchina = $ar_repma[1];
        
        
        //Rimuovo dati attuali
        $sql = "SELECT ATIDTK FROM {$cfg_mod_DeskProd['file_ticket_T']} WHERE ATTPTK = 'ATTI'
                       AND ATARTI = ? AND ATTPAB = ?";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($macchina, $data));
        $row = db2_fetch_assoc($stmt);
        
        if ($row){
            $ex_prog = $row['ATIDTK'];
            
            //Testata
            $sql = "DELETE FROM {$cfg_mod_DeskProd['file_ticket_T']} WHERE ATTPTK = 'ATTI'
                           AND ATIDTK = ?";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, array($ex_prog));
//echo $sql; print_r($ex_prog);   
            //Righe (orari)
            $sql = "DELETE FROM {$cfg_mod_DeskProd['file_ticket_R']} WHERE ARTPAV = 'ATTI'
                           AND ARIDTK = ?";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, array($ex_prog));
//echo $sql; print_r($ex_prog); 
        }
            
        
        
        $sql = "SELECT MAX(ATIDTK) AS PROG FROM {$cfg_mod_DeskProd['file_ticket_T']}";

        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt);
        $row = db2_fetch_assoc($stmt);
        
        $ar_ins_t = array();
        $ar_ins_t['ATUSGE'] 	= $auth->get_user();
        $ar_ins_t['ATDTGE'] 	= oggi_AS_date();
        $ar_ins_t['ATORGE'] 	= oggi_AS_time();
        $ar_ins_t['ATUSUM'] 	= $auth->get_user();
        $ar_ins_t['ATDTUM'] 	= oggi_AS_date();
        $ar_ins_t['ATORUM'] 	= oggi_AS_time();
        $ar_ins_t['ATTPTK']     = 'ATTI';
        $ar_ins_t['ATIDTK']     = $row['PROG'] + 1;
        $ar_ins_t['ATARTI']     = $macchina;
        $ar_ins_t['ATTPAB']     = $data;
        
        $sql_t = "INSERT INTO {$cfg_mod_DeskProd['file_ticket_T']} (" . create_name_field_by_ar($ar_ins_t) . ") VALUES (" . create_parameters_point_by_ar($ar_ins_t) . ")";
        
        $stmt_t = db2_prepare($conn, $sql_t);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_t, $ar_ins_t);
        echo db2_stmt_errormsg($stmt_t);
        
        $ar_ins_r = array();
        $ar_ins_r['ARUSGE'] 	= $auth->get_user();
        $ar_ins_r['ARDTGE'] 	= oggi_AS_date();
        $ar_ins_r['ARORGE'] 	= oggi_AS_time();
        $ar_ins_r['ARUSUM'] 	= $auth->get_user();
        $ar_ins_r['ARDTUM'] 	= oggi_AS_date();
        $ar_ins_r['ARORUM'] 	= oggi_AS_time();
        $ar_ins_r['ARTPAV']     = 'ATTI';
        $ar_ins_r['ARIDTK']     = $row['PROG'] + 1;
        $ar_ins_r['ARIDTA']     = $data;
       
        
        for($i = 0; $i <= 3; $i++){
            $da = "da_{$i}"; 
            $a = "a_{$i}";
            if(strlen($form_values->$da) > 0 && strlen($form_values->$a) > 0){
                $ar_ins_r['ARIDRG'] = $i;
                $ar_ins_r['ARSEQU'] = $form_values->$da."_".$form_values->$a;
                
                $sql_r = "INSERT INTO {$cfg_mod_DeskProd['file_ticket_R']} (" . create_name_field_by_ar($ar_ins_r) . ") VALUES (" . create_parameters_point_by_ar($ar_ins_r) . ")";
                
                $stmt_r = db2_prepare($conn, $sql_r);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_r, $ar_ins_r);
                echo db2_stmt_errormsg($stmt_r);
                
                
            }
            
        }
        
    }
    
      
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){ ?>
{"success":true, 
	m_win: {
		title: 'Inserimento orario',
		width: 500, height: 300,
		iconCls: 'icon-clock-16'
	},
	items: [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            layout: {pack: 'start', align: 'stretch'},
	            items: [
	            		{xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
                           xtype:'timefield'
                    	   , fieldLabel: 'Da'
                    	   , labelWidth : 40
						   , width : 150
                    	   , value: ''		
                    	   , name: 'da_1'
                    	   , format: 'H:i'						   
                    	   , submitFormat: 'Hi'
                    	   , increment: 1
                    	   //, minValue : '08:00'
                    	   //, maxValue : '13:00'
                       },
						{
                           xtype:'timefield'
                    	   , fieldLabel: 'A'
                    	   , labelWidth : 40
						   , width : 150
                    	   , labelAlign : 'right'
                    	   , value: ''		
                    	   , name: 'a_1'
                    	   , format: 'H:i'						   
                    	   , submitFormat: 'Hi'
                    	   , increment: 1
                    	   //, minValue : '08:00'
                    	   //, maxValue : '13:00'
                       }
						]},
						{xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
                           xtype:'timefield'
                    	   , fieldLabel: 'Da'
                    	   , labelWidth : 40
						   , width : 150
                    	   , value: ''		
                    	   , name: 'da_2'
                    	   , format: 'H:i'						   
                    	   , submitFormat: 'Hi'
                    	   , increment: 1
                    	   //, minValue : '13:00'
                    	   //, maxValue : '18:00'
                       },
						{
                           xtype:'timefield'
                    	   , fieldLabel: 'A'
                    	   , labelWidth : 40
						   , width : 150
                    	   , labelAlign : 'right'
                    	   , value: ''		
                    	   , name: 'a_2'
                    	   , format: 'H:i'						   
                    	   , submitFormat: 'Hi'
                    	   , increment: 1
                    	   //, minValue : '13:00'
                    	   //, maxValue : '18:00'
                       }
						]},
						{xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
                           xtype:'timefield'
                    	   , fieldLabel: 'Da'
                    	   , labelWidth : 40
						   , width : 150
                    	   , value: ''		
                    	   , name: 'da_3'
                    	   , format: 'H:i'						   
                    	   , submitFormat: 'Hi'
                    	   , increment: 1
                    	   //, minValue : '18:00'
                    	   //, maxValue : '23:59'
                       },
						{
                           xtype:'timefield'
                    	   , fieldLabel: 'A'
                    	   , labelWidth : 40
						   , width : 150
                    	   , labelAlign : 'right'
                    	   , value: ''		
                    	   , name: 'a_3'
                    	   , format: 'H:i'						   
                    	   , submitFormat: 'Hi'
                    	   , increment: 1
                           //, minValue : '18:00'
                    	   //, maxValue : '23:59'
                       }
						]}
    	           
						
	               ],
	              	buttons: [
	              	<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "TICKET_ORARI");  ?>	
				      '->',
				      {
			            text: 'Azzera',
				        iconCls: 'icon-sub_red_delete-32',		            
				        scale: 'large',
				        handler: function(){
				        	var form = this.up('form').getForm();
				        	form.reset();
				        }
				      },
				      '->',
				      {
			            text: 'Conferma',
				        iconCls: 'icon-print-32',		            
				        scale: 'large',		            
			            handler: function() {
			            	var form = this.up('form').getForm(),
			                loc_win = this.up('window');
			                
		                    if (form.isValid()){
    							Ext.Ajax.request({
     						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_orari',
     						        method  : 'POST',
     			        			jsonData: {
     			        			   form_values: form.getValues(),
     			        			   open_request: <?php echo acs_je($m_params); ?>
     								},
     								success: function(response, opts) {
    						        	 var jsonData = Ext.decode(response.responseText);
    						        	 loc_win.fireEvent('afterConfirm', loc_win);
    			            		},
    						        failure    : function(result, request){
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
     								
     						    });
			                }
			                
			                
			            }
			        }
		        
		        
		        ]     
	               
	               
	               }
	            
	            ]}

         


<?php 
 
}