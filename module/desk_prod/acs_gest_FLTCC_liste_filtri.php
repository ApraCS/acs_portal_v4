<?php 

require_once("../../config.inc.php");

$main_module = new DeskProd();
$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    
        'm_win' => array(
          'title' => 'Configura filtri',
          'iconCls' => 'icon-filter-16'
        ),
    
        'tab_name' =>  $cfg_mod['file_tabelle'],
        'show_dx_mobile' => 'Y',
		
		'TATAID' => 'FLTCC',
		'descrizione' => 'Impostazione filtri colli/componenti',
		
		'fields_key' => array('TAKEY1'),
		
		'fields' => array(				
		    'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
	            'TADESC' => array('label'	=> 'Descrizione'),
		),
    
        'rec_menu' => array(
            array(
                'text' => 'Filtri su colli',
                'iconCls' => 'icon-windows-16',
                'handler' => extjs_code("function() {
                    acs_show_win_std(null,
                        'acs_panel_indici_config_distinta_filtri.php?fn=form_colli', {
                         lista: rec.get('TAKEY1'),
                         d_lista : rec.get('TADESC')
                    });
                }")
            ),
            array(
                'text' => 'Filtri su componenti',
                'iconCls' => 'icon-windows-16',
                'handler' => extjs_code("function() {
                    acs_show_win_std(null,
                        'acs_panel_indici_config_distinta_filtri.php?fn=form_compo', {
                         lista: rec.get('TAKEY1'),
                         d_lista : rec.get('TADESC')
                    });
                }")
            )
        )
		
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
