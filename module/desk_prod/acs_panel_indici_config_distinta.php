<?php
require_once("../../config.inc.php");
require_once("../base/acs_gestione_nota_nt_include.php");

$main_module = new DeskProd();

$m_params = acs_m_params_json_decode();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    't_panel'     => 'Distinta_' . trim($m_params->cod),
    'module'      => $main_module,
    'tab_name'    => $cfg_mod['file_cfg_distinte'],
    'descrizione' => "Distinta indice",
    'form_title' => "Dettagli distinta attivit&agrave; di produzione",
    'maximize'    => 'Y',
    
    'f_ditta' => 'CDDT',
    'f_desc'  => 'CDDESC',
    'immissione' => array(
        'data_gen'   => 'CDDTGE',
        'user_gen'   => 'CDUSGE',
        'data_mod'   => 'CDDTUM',
        'user_mod'   => 'CDUSUM',
        
    ),
    
    'fields_key' => array('CDCMAS', 'CDSLAV'),
    'field_IDPR' => 'CDIDPR',   //progressivo globale auto incrementato (MAX + 1)
    'field_NOTE' => 'CDIDPR',  
    'fields_grid' => array('CDSEQI', 'CDCMAS', 'CDSEQU', 'CDSLAV', 'CDDESC', 'CDCDLI',
        'CDTPUS', 'CDFOUS', 'CDGEND', 'CDGRUP', 'nt_OPERAT', 'nt_CONFIG', 'cv', 'immissione', 'valid'),
    'fields_form' => array('CDSEQI', 'CDIDPR', 'CDCMAS', 'CDSEQU', 'CDSLAV', 'CDDESC',
                           'CDTODO', 'CDPARM', 'CDTPUS', 'CDFOUS', 'CDPAUS', 'CDGEND',
                           'CDPAGD', 'CDDTIV', 'CDDTFV', 'CDCDLI', 'CDFASE', 'CDLAVO', 'CDREPA', 'CDGRUP'),
    
    'tab_form' => array(
        'logistica' => array(
            'title' => 'Logistica',
            'fields' => array('CDFASE', 'CDLAVO', 'CDREPA', 'CDGRUP')
        )
    ),
    
    
    //uso come filtro, uso come auto valore in inserimento nuovo valore
    //ToDo: richiedere obbligatoriamente?
    'fields_preset' => array(
        'CDDICL' => $m_params->open_request->cod
    ),
    
    'fields' => array(
        
        'CDDICL' => array('label'	=> 'Distinta (Indice)'),
        
        'CDIDPR' => array('label'   => '#Id Rec.', 'c_fw' => 'width: 70', 'fw'=>'width: 170', 'type' => 'IDPR'),
        'CDSEQI' => array('label'	=> 'Seq.',   'c_fw' => 'width: 70', 'fw'=>'width: 170', 'tooltip'=>'Sequenza gruppo', 'f_label' => 'Seq. gruppo'),
        
        'CDCMAS' => array('label'	=> 'Gruppo', 'c_fw' => 'width: 50', 'fw'=>'width: 170'),
        'CDSEQU' => array('label'	=> 'Seq.', 'c_fw' => 'width: 50', 'fw'=>'width: 170', 'tooltip'=>'Sequenza voce', 'f_label' => 'Seq. voce'),
        'CDSLAV' => array('label'	=> 'Voce', 'c_fw' => 'width: 50', 'fw'=>'width: 170'),
        'CDDESC' => array('label'	=> 'Descriz. voce', 'f_label' => 'Descrizione'),
        
        //'CDFLGA' => array('label'	=> 'Segue',        'fw'=>'width: 50', 'tooltip'=>'(G, A) Gruppo o elenco Articoli'),
        //'CDFLLG' => array('label'	=> 'Tipo lista',   'fw'=>'width: 60', 'tooltip'=>'Lista ulteriori opzioni da selezionare (L, G, A, N) Lista opzioni, Genera in automatico, lista Articoli, Non presente'),
        
        'CDCDLI' => array('label'	=> 'Filtri',   'type' => 'from_TA', 'TAID' => 'FLTCC',
            /*'comboEditorData' => $main_module->find_TA_std('FLTCC', null, 'Y', 'N')*/),
        
        'CDTODO' => array('label'	=> 'Tipo attivit&agrave;',    'fw'=>'width: 100', 'tooltip'=>'Codice ToDo',
            'type' => 'from_TA', 'TAID' => 'ATTAV', 'gruppo' => $cfg_mod['ATTAV_TAKEY2']),
        'CDPARM' => array('label'	=> 'Parametri', 'fw'=>'width: 180', 'tooltip'=>'Parametri ToDo'),
        
        'CDTPUS' => array('label'	=> 'TU', 'tooltip'=>'Tipo uscita', 'f_label' => 'Tipo uscita', 'fw'=>'width: 30', 'type' => 'from_TA', 'TAID' => 'CDTPU', 'allowBlank' => true),
        
        //Formato uscita
        'CDFOUS' => array('label'	=> 'Formato', 'tooltip'=>'Formato uscita', 'f_label' => 'Formato uscita', 'fw'=>'width: 60', 'type' => 'from_TA', 'TAID' => 'CDFMU', 'allowBlank' => true),
        'CDPAUS' => array('label'	=> 'Parametri', 'fw'=>'width: 180', 'tooltip'=>'Parametri Formato uscita', 'type' => 'textarea'),
        
        //Generazione documenti
        'CDGEND' => array('label'	=> 'G.Doc.', 'tooltip' =>'Generazione documenti', 'fw'=>'width: 50', 'type' => 'from_TA', 'TAID' => 'CDGED', 'f_label' => 'Generazione documenti', 'allowBlank' => true),
        'CDPAGD' => array('label'	=> 'Parametri', 'tooltip'=>'Parametri Generazione documenti'),
        
        //validita
        'CDDTIV' => array('label'	=> 'Valido dal',   'fw'=>'width: 90', 'type' => 'date'),
        'CDDTFV' => array('label'	=> 'Valido al',    'fw'=>'width: 90', 'type' => 'date'),
        'valid' => array('label'	=> 'V',   'fw'=>'width: 30', 'type' => 'valid',
            'config' => array(
                'ini'   => 'CDDTIV',
                'fin'   => 'CDDTFV'
            )),
        
        //immissione
        'immissione' => array(
            
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'CDDTGE',
                'data_mod'   => 'CDDTUM',
                'user_gen'   => 'CDUSGE',
                'user_mod'   => 'CDUSUM'
            )
            
        ),
        'CDDTGE' => array('label'	=> 'Data generazione'),
        'CDUSGE' => array('label'	=> 'Utente generazione'),
        'CDDTUM' => array('label'	=> 'Data modifica'),
        'CDUSUM' => array('label'	=> 'Utente modifica'),
        
        
        'nt_OPERAT' => array('type' => 'NT', 'fw'=>'width: 40',
            'iconCls' => "user",
            'tooltip'=>'Note operative',
            'nt_config' => array(
                'title' => 'Note operative',
                'file'     => $cfg_mod['file_note'],
                'NTTPNO'   => 'CDOPE'
            )),
        'nt_CONFIG' => array('type' => 'NT', 'fw'=>'width: 40',
            'iconCls' => "info_blue",
            'tooltip'=>'Note configurazione',
            'nt_config' => array(
                'title' => 'Note configurazione',
                'file'     => $cfg_mod['file_note'],
                'NTTPNO'   => 'CDINF'
                //ToDo:
                //come default uso come chiave il valore di field_IDPR. Si potrebbe dover configurare
            )),
        
        'cv' => array(
            'type' => 'AH', 'label' => 'CV', 'tooltip' => 'Ciclo di vita', /*'iconCls' => 'button_blue_pause',*/
            'h_config' => array('causale' => 'CVOUT', 'raggruppamento' => 'CV')
        ),
        
        'CDFASE' => array('label'	=> 'Fase', 'type' => 'from_TA', 'TAID' => 'PROFS'),
        'CDLAVO' => array('label'	=> 'Lavorazione', 'type' => 'from_TA', 'TAID' => 'PROLV'),
        'CDREPA' => array('label'	=> 'Reparto', 'type' => 'from_TA', 'TAID' => 'REPMA', 'module' => $desk_art),
        'CDGRUP' => array('label'	=> 'Mansione', 'type' => 'from_TA', 'TAID' => 'PRGRO', 'short' => 'Mans.', 'fw'=>'width: 40'),
        
    ),
    
    
    
    /* posso passare parametri e impostare filtri in automatico
     'flt_adv' => array(
     'CDDICL' => array($m_params->open_request->cod)
     ),
     */
    /* 'menu_dx' => array(
     array(
     'text'  => 'Duplica',
     'iconCls' => 'icon-leaf-16',
     'handler' =>  new ApiProcRaw("function(){
     acs_show_win_std(null, 'acs_gest_FLTCC_liste_filtri.php?fn=open_tab');
     }")
     )
        
     ),*/
    
    'buttons' => array(
        array(
            'text'  => '',
            'iconCls' => 'icon-filter-16',
            'tooltip' => 'Configura filtri',
            'scale' => 'small',
            'style' => 'border: 1px solid gray;',
            'handler' =>  new ApiProcRaw("function(){
                acs_show_win_std(null, 'acs_gest_FLTCC_liste_filtri.php?fn=open_tab');
             }")
        )
    )
    
    
    
);


$_row_post_fn = function($row, $m_table_config){
    $row['nt_CONFIG']  = _ha_nota_nt_config($row[$m_table_config['field_IDPR']], $m_table_config['fields']['nt_CONFIG']['nt_config']);
    $row['nt_OPERAT']  = _ha_nota_nt_config($row[$m_table_config['field_IDPR']], $m_table_config['fields']['nt_OPERAT']['nt_config']);
    
    return $row;
};

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
