<?php
require_once("../../config.inc.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_todolist_preview_include.php';

$main_module = new DeskProd();
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$pp_colli = _pp_get_colli($main_module, $m_params, $m_params->preview);
$ar_colli = $pp_colli['ar_colli'];
$rec = $ar_colli[0];

$parametri_formato = $m_params->params;

//$title = "Vista ordine {$rec['TDONDO']} {$rec['TDOTPD']}_{$rec['TDOADO']}";
$title = "Immagine";
//foreach ($ar_colli as $rec){
    
    $order_dir          = _pp_order_dir($main_module, $rec['TDDOCU']);
    $order_dir_assoluto = _pp_order_dir_assoluto($main_module, $rec['TDDOCU']);
    
    $fisso = $parametri_formato->IMG; //es: _btop_
    $riga_grafica = trim($rec['PLCINT']);
    
    $ricerca_file = $order_dir_assoluto . '/*' . "{$fisso}{$riga_grafica}.JPG";
    
    $f = null;
    foreach (glob($ricerca_file) as $filename){
        $f = basename($filename);
    }

    if (is_null($f)){
        //se immagine non trovata, da formato logo, recupero immagine "No immagine"
        $row_tab_logo = $main_module->get_TA_std('LOGOP', $rec['TDAUX4']);
        $logo_path    = trim($row_tab_logo['TALOCA']);
        
        $img = "../personal/desk_prod/loghi/{$logo_path}";
    } else {
        //se immagine trovata
        $img = '../'.$order_dir . '/' . $f;
       
    }
   
//}



//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_panel'){
//----------------------------------------------------------------------
 
    ?>
	{
		success:true,	
		m_win: {
			title: <?php echo j($title); ?>,
			iconCls : 'icon-pencil-16',
			width: 700, height: 620,
		},
		items: [
				{
					xtype: 'panel'
					, flex: 1
					, autoScroll: false
					, layout: 'fit'
			        , items: [
        				{
        					xtype: 'panel'
        					, flex: 1
        					, autoScroll: false
        					, layout: 'fit'
        			        , items: [
        			          {
        			        	  xtype: 'panel',
        			        	  autoScroll: false,
        			        	  flex: 1,
        			        	  padding: 10,
        			        	  html: '<img src=<?php echo img_path($img) ?> width="100%" height="60%">',
        			        	  border: false
        			          }
        			        ]        
        		  		} //panel
			        ]
	
		  	} //panel
		 
		
		]
		   
	}
	
	<?php 
    
}

