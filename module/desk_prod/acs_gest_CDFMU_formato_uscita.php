<?php 

require_once("../../config.inc.php");
require_once("../base/acs_gestione_nota_nt_include.php");

$main_module = new DeskProd();
$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    
        'module'      => $main_module,
        'tab_name' =>  $cfg_mod['file_tabelle'],
        't_panel' =>  "CDFMU - Formato uscita",
        'descrizione' => "Gestione formato uscita",
        'form_title' => "Dettagli tabella formato uscita",
        'f_ditta' => 'TADT',
        'f_desc'  => 'TADESC',
        'field_NOTE' => 'TAKEY1',   //SOLO PER LA CHIAVE FILE NT
        'fields_preset' => array(
            'TATAID' => 'CDFMU'
        ),
        
        'immissione' => array(
            'data_gen'   => 'TADTGE',
            'user_gen'   => 'TAUSGE',
            
        ),
	    'fields_key' => array('TAKEY1', 'TADESC'),
        'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TALOCA', 'TASITI_d', 'nt_CONFIG', 'TAFG01', 'TAFG02', 'TAFG03', 'TAFG04', 'TAFG05', 'immissione'),
        'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TALOCA', 'TAINDI', 'TASITI', 'TARIF1', 'TAFG01', 'TAFG02', 'TAFG03', 'TAFG04', 'TAFG05'),
		
		'fields' => array(				
		    'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
	            'TADESC' => array('label'	=> 'Descrizione'),
		        'TAMAIL' => array('label'   => 'Note', 'type' => 'textarea', 'maxLength' => 100),
		        'TALOCA' => array('label'	=> 'Procedura', 'maxLength' => 60),
		    
		        'TASITI' => array('label'	=> 'Tipo avanzamento',   'type' => 'from_TA', 'TAID' => 'CDTAV', 'desc'=> 'Y', 'no_cod' => 'Y'),
		        'TASITI_d' => array('label'	=> 'Tipo avanzamento'),
		      
		        //flag
		        'TAFG01' => array('label'	=> 'Spunta disponibile', 'type' => 'checkbox', 'iconCls' => 'sub_black_accept'),
		        'TAFG02' => array('label'	=> 'Vista ordine', 'type' => 'checkbox',  'iconCls' => 'design'),
		        'TAFG03' => array('label'	=> 'Stampa/Ristampa', 'type' => 'checkbox',  'iconCls' => 'print'),
		        'TAFG04' => array('label'	=> 'Segnalazioni', 'type' => 'checkbox',  'iconCls' => 'warning_black'),
		        'TAFG05' => array('label'	=> 'Movimentazione', 'type' => 'checkbox',  'iconCls' => 'box_open'),
		    
		        'TAINDI' => array('label'	=> 'Msg. conferma', 'type' => 'textarea'),
		        'TARIF1' => array('label'	=> 'Parametri', 'maxLength' => 20),
    		    //immissione
    		    'immissione' => array(
    		        'type' => 'immissione', 'fw'=>'width: 70',
    		        'config' => array(
    		            'data_gen'   => 'TADTGE',
    		            'user_gen'   => 'TAUSGE'
    		        )
    		        
    		    ),
		    
		    'nt_CONFIG' => array('type' => 'NT', 'fw'=>'width: 40',
		        'iconCls' => "info_blue",
		        'tooltip'=>'Note formato uscita',
		        'nt_config' => array(
		            'title' => 'Note formato uscita',
		            'file'     => $cfg_mod['file_note'],
		            'NTTPNO'   => 'CDFMU'
		            //ToDo:
		            //come default uso come chiave il valore di field_IDPR. Si potrebbe dover configurare
		        )),
    		    'TADTGE' => array('label'	=> 'Data generazione'),
    		    'TAUSGE' => array('label'	=> 'Utente generazione'),
		)
		
);

$_row_post_fn = function($row, $m_table_config){
    $row['nt_CONFIG']  = _ha_nota_nt_config($row[$m_table_config['field_NOTE']], $m_table_config['fields']['nt_CONFIG']['nt_config']);
    
    return $row;
};

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
