<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

//Elenco opzioni "Area spedizione"
$ar_area_spedizione = $s->get_options_area_spedizione();
$ar_area_spedizione_txt = '';

$ar_ar2 = array();
foreach ($ar_area_spedizione as $a)
	$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
$ar_area_spedizione_txt = implode(',', $ar_ar2);

$m_params = acs_m_params_json_decode();
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
            url: 'acs_print_wrapper.php',
			buttonAlign:'center',            
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "RCOLLP");  ?>
			{
	            text: 'Report',
	            iconCls: 'icon-print-32',
	            scale: 'large',	            
	            handler: function() {
	                this.up('form').submit({
                        url: '../desk_prod/acs_report_riepilogo_colli_linea_prod.php',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',
                        params : {
                        	da_data : <?php echo j($m_params->da_data); ?>,
                        	col_name : <?php echo j($m_params->col_name); ?> 
                        },
                  });
	                
	            }
	        },
	        
			{
	            text: 'Visualizza',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',	            
	            handler: function() {
	            var form = this.up('form');
	            var form_values = form.getValues();
	               acs_show_win_std(null, 
	               	'../desk_prod/acs_riepilogo_colli_linea_prod.php?fn=get_avanzamento_lotto_grid', 
	               	 {
	               	 	da_data : <?php echo j($m_params->da_data); ?>, 
	               	 	col_name : <?php echo j($m_params->col_name); ?>, 
	               	 	form_values : form_values,
	               	 	open_request: <?php echo acs_je($m_params->open_request); ?>
	               	 });
	                
	            }
	        } 
	        
	        
	        ],             
            
            items: [     	            	            
                 {
					xtype: 'fieldset',
	                title: '',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
	             
	             

            , {
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Area di spedizione',
					defaultType: 'textfield',
		 	        layout: 'hbox',
		 	        items: [	             
	             
	             
					{
					    xtype: 'combo',
					    flex: 1,
					    name: 'area_spedizione',
					    autoSelect: false,
					    allowBlank: true,
					    editable: false,
					    triggerAction: 'all',
					    typeAhead: true,
					    width:120,
					    listWidth: 120,
					    enableKeyEvents: true,
					    mode: 'local',
					    store: [
					        <?php echo $ar_area_spedizione_txt ?>
					    ]
					}, {
					    xtype: 'combo',
					    flex: 1.5,					    
					    fieldLabel: 'Divisione', labelAlign: 'right',
					    name: 'divisione',
					    autoSelect: false,
					    allowBlank: true,
					    editable: false,
					    triggerAction: 'all',
					    typeAhead: true,
					    width:120,
					    listWidth: 120,
					    enableKeyEvents: true,
					    displayField: 'text',
						valueField: 'id',					    
					    mode: 'local',
					    store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}
					}
				]
				}	
					
					
					
					
            , {
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Stabilimento',
					defaultType: 'textfield',
		 	        layout: 'hbox',
		 	        items: [	             
					{
					    
					    xtype: 'combo',
					    flex: 1,
					    fieldLabel: '',
					    name: 'stabilimento',
					    displayField: 'text',
						valueField: 'id',
					 	margin: '5 0 0 0',
						emptyText: '- seleziona -',
					   	allowBlank: true,
					   	multiSelect: false,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json(find_TA_std('START'), ''); ?>
							    ] 
						}	
					},
					 {
					    xtype: 'combo',
					    flex: 1.5,
					    fieldLabel: 'Tipologia',  labelAlign: 'right',
					    name: 'sel_tipologia_ordini',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,
					   	multiSelect: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), ''); ?>
							    ] 
						}	
					}
					
				]}
				
				
			<?php if (!isset($m_params->filtro_semplificato) || $m_params->filtro_semplificato != 'Y'){ ?>	
				, {
					  xtype: 'fieldset',
					  layout: 'vbox',
					  margin: "10 0 0 0",					  
					  items: [
					
					{
					  xtype: 'fieldset',
					  border: false, margin: "0 0 0 0", padding: "0 0 0 0",
					  layout: 'hbox',
					  items: [
						{
		                    xtype: 'radiogroup',
		                    layout: 'hbox',
		                    fieldLabel: '',
		                    flex: 3,
		                    defaults: {
		                     width: 105
		                    },
		                    items: [{
		                    	xtype: 'label',
		                    	text: 'Carico',
		                    	width: 70
		                    },
							{
				                boxLabel: 'Tutti',
				                name: 'carico_assegnato',
				                checked: true,			                
				                inputValue: 'T'
				            }, {
				                boxLabel: 'Con carico',
				                checked: false,			                
				                name: 'carico_assegnato',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'Senza',
				                checked: false,			                
				                name: 'carico_assegnato',
				                inputValue: 'N'                
				            }
		                   ]
		                }, {
								name: 'num_carico',
								xtype: 'textfield',								
								fieldLabel: 'Nr. Carico',
								value: '',
								width: 120, labelWidth: 60,
								labelAlign: 'right'
						}					  
					  ]
					  
					}, 
					
					
					
					, {
					  xtype: 'fieldset',
					  border: false, margin: "5 0 0 0", padding: "0 0 0 0",					  
					  layout: 'hbox',
					  items: [

						{
		                    xtype: 'radiogroup',
		                    layout: 'hbox',
		                    fieldLabel: '',
		                    flex: 3,
		                    defaults: {
		                     width: 105,
		                    },		                    
		                    items: [{
			                    	xtype: 'label',
			                    	text: 'Lotto',
			                    	width: 70
			                    },
							{
				                boxLabel: 'Tutti',
				                name: 'lotto_assegnato',
				                checked: true,			                
				                inputValue: 'T'
				            }, {
				                boxLabel: 'Con lotto',
				                checked: false,			                
				                name: 'lotto_assegnato',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'Senza',
				                checked: false,			                
				                name: 'lotto_assegnato',
				                inputValue: 'N'                
				            }
		                   ]
		                }, {
								name: 'num_lotto',
								xtype: 'textfield',
								fieldLabel: 'Nr. Lotto',
								value: '',
								width: 120, labelWidth: 60,
								labelAlign: 'right'														
							 } 	
			             
					  ]
					  }					
						             
	             ]
	             }	
	             
	             <?php } ?>
	             
	                          
	             					  
					  ]
					  }
                
                
               , {
                    xtype: 'tabpanel',
					anchor: '100%',                    
                    activeTab: 0,
                    layout: 'fit',
			        defaults :{
			            bodyPadding: 10
			        },
                    items: [
						{
				            xtype: 'panel',
				            title: 'Colli',
				            items: [
								{
			                    xtype: 'checkboxgroup',
			                    layout: 'column',
			                    defaults: {width: 100, labelWidth: 90},			                    
			                    fieldLabel: 'Dettaglio per',
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'cl_dettaglio_per_linea' 
			                          , boxLabel: 'Linea'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'cl_dettaglio_per_area' 
			                          , boxLabel: 'Area sped.'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'cl_dettaglio_per_itinerario' 
			                          , boxLabel: 'Itinerario'
			                          , inputValue: 'Y'
			                        }
									]
								}, {
			                    xtype: 'checkboxgroup',
			                    fieldLabel: 'Confronta capacit&agrave;',
			                    layout: 'column',
			                    defaults: {width: 100, labelWidth: 90},			                    
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'cl_confronta_cap_prod' 
			                          , boxLabel: 'Si'
			                          , inputValue: 'Y'
			                        }
									]
								}]


				            
				            

				        },{
				            xtype: 'panel',
				            title: 'Descrizione report',
				            items: [
											{
							                    xtype: 'textfield',
							                    name: 'add_descr_report',    
							                    fieldLabel: '',
							                    allowBlank: true,			                    
							                    width: '100%', anchor: '-10'
							                }				            
				            ]
				        }                                          
                    ]
                 }
                 

                
            ]
        }

	
]}