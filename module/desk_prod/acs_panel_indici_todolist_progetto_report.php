<?php

require_once "../../config.inc.php";
require_once("../../config.inc.php");
require_once("acs_panel_prog_prod_include.php");
require_once("acs_panel_indici_include.php");
require_once("acs_panel_indici_naviga_include.php");
require_once("acs_panel_indici_todolist_preview_include.php");
require_once("../base/_rilav_g_history_include.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));
$desk_art = new DeskArt(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$_raggruppamenti_attivita = array('INI', 'SOS', 'TER');

// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
    
    $ar_email_to = array();
    
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u){
        $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
    }
    
    $ar_email_json = acs_je($ar_email_to);
    ?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff; font-weight: bold;}   
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
    
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {
        border-collapse: unset;
	     	
    }
        .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 


<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php

$progetto = $_REQUEST['progetto'];
$kiosk = $_REQUEST['kiosk'];

$as_where .= " AND ASPROG = " . sql_t($progetto);
if (isset($kiosk))
    $as_where .= " AND ASORD2 = " . sql_t($kiosk);
    
    
    $_rilav_H_for_query = _rilav_H_for_query(
        array('f_ditta' => 'ASDT', 'field_IDPR' => 'ASIDPR'),   //m_table_config
        $cfg_mod['file_assegna_ord'],
        $cfg_mod['file_tabelle'],
        'ATT_OPEN', //alias tabella
        null, //sarebbe la causale
        $_raggruppamenti_attivita);
    
    $_rilav_H_for_query_CD = _rilav_H_for_query(
        array('f_ditta' => 'ASDT', 'field_IDPR' => 'ASORD4'),   //m_table_config
        $cfg_mod['file_cfg_distinte'],
        $cfg_mod['file_tabelle'],
        'ATT_OPEN',
        'CVOUT',
        array('CV')
        );
    
    
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.TADESC AS D_AT, TA_PJ.TADTGE AS DATA_PJ,
    TA_PJ.TAMAIL AS D_CJ, TA_PJ.TADESC AS D_PJ, 
    /*NT_MEMO.NTMEMO AS MEMO, */
    NT_CD.NTKEY2 AS CODICE,
    NT_CD.NTMEMO AS DESC, TA_ALLEGATI.NR_AL AS NR_AL,
    TA_PJ.TADTGE AS DATA_PJ, TA_PJ.TAORGE AS ORA_PJ, TA_PJ.TAUSGE AS US_PJ,
    TA_IND.TADESC AS D_IND, {$_rilav_H_for_query['select']}, {$_rilav_H_for_query_CD['select']},
    TA_TU.TAFG01 as VISTA_COLLI, TA_TU.TAFG02 AS USCITA_SCHERMO, TA_TU.TAFG03 AS USCITA_BATCH
    
    FROM {$cfg_mod['file_assegna_ord']} ATT_OPEN
    
    INNER JOIN {$cfg_mod['file_tabelle']} TA_ATTAV
    ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND TA_ATTAV.TATAID = 'ATTAV' AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1
    
    /* CD */
    LEFT OUTER JOIN {$cfg_mod['file_cfg_distinte']} CD
    ON ATT_OPEN.ASDT = CD.CDDT AND ATT_OPEN.ASORD4 = CD.CDIDPR
    
    LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_TU
    ON CD.CDDT = TA_TU.TADT AND CD.CDTPUS = TA_TU.TAKEY1 AND TA_TU.TATAID = 'CDTPU'
    
    /* decod. indice */
    LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_IND
    ON ATT_OPEN.ASDT = TA_IND.TADT AND ATT_OPEN.ASORD1 = TA_IND.TAKEY1 AND TA_IND.TATAID = '{$cfg_mod['taid_indici']}'
    
    LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_PJ
    ON ATT_OPEN.ASDT = TA_PJ.TADT AND ATT_OPEN.ASPROG = TA_PJ.TAKEY1 AND TA_PJ.TATAID = '{$cfg_mod['taid_progetto']}'
    
   /* LEFT OUTER JOIN {$cfg_mod['file_note']} NT_MEMO
    ON NT_MEMO.NTTPNO = 'ASME5' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
    */
    
    LEFT OUTER JOIN {$cfg_mod['file_note']} NT_CD
    ON NT_CD.NTTPNO = 'PRJC5' AND INTEGER(NT_CD.NTKEY1) = ATT_OPEN.ASIDPR AND NT_CD.NTSEQU=0
    
    LEFT OUTER JOIN (
    SELECT COUNT(*) AS NR_AL, TADT, TATAID, TAKEY1
    FROM {$cfg_mod['file_tabelle']}
    GROUP BY TAKEY1, TADT, TATAID) TA_ALLEGATI
    ON TA_ALLEGATI.TADT = ATT_OPEN.ASDT AND TA_ALLEGATI.TATAID = 'TDPAL' AND TA_ALLEGATI.TAKEY1 = ATT_OPEN.ASPROG
    
    {$_rilav_H_for_query['joins']}
    {$_rilav_H_for_query_CD['joins']}
    WHERE ASDT = '{$id_ditta_default}' AND ASPROG <> '' {$as_where}
    ORDER BY ASIDPR";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();


$ar = array();
while ($row = db2_fetch_assoc($stmt)) {
    
    //stacco dei livelli
    $cod_liv0 = trim($row['ASPROG']);   //progetto
    $cod_liv1 = trim($row['ASORD2']);   //kiosk
    $cod_liv2 = $row['ASIDPR'];         //numeratore assoluto attivita'
    
    
    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    //PROGETTO
    $liv =$cod_liv0;
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] = "[#" . trim($row['ASPROG']) . "] " . trim($row['D_CJ']);
        $ar_new['imm'] =  print_date($row['DATA_PJ'])."-".print_ora($row['ORA_PJ']);
        
        $ar_new['allegati'] = $row['NR_AL'];
        $ar_new['ut_ass'] = "[".trim($row['US_PJ'])."]";
        $ar_new['liv'] = 'liv_1';
        $ar_new['liv_c'] = $liv;
        $ar_new['liv_type'] = 'PROGETTO';
        
        $ar_new['ASORD1'] = trim($row['ASORD1']);
        $ar_new['ASORD2'] = trim($row['ASORD2']);
        $ar_new['ASORD3'] = trim($row['ASORD3']);
        
        $ar_new['k_progetto'] = trim($row['D_CJ']);
        
        $ar_new['indice'] = trim($row['ASORD1']);
        $ar_new['desc'] .=  utf8_encode($row['D_IND']);
        $ar_new['desc'] .=   '<br>' . $row['D_PJ'];
        
        
        $ar_new['expanded'] = true;
        
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    
    //Kiosk
    $liv=$cod_liv1;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] = $row['ASORD2'];
        $ar_new['desc'] =  decod_kiosk_from_indice($cfg_mod, $row['ASORD1'], $row['ASORD2']);
        //$ar_new['causale'] =  $row['ASCAAS'];
        
        $ar_new['liv'] = 'liv_2';
        $ar_new['liv_c'] = $liv;
        $ar_new['liv_type'] = 'KIOSK';
        
        $ar_new['ASORD1'] = trim($row['ASORD1']);
        $ar_new['ASORD2'] = trim($row['ASORD2']);
        $ar_new['ASORD3'] = trim($row['ASORD3']);
        
        $ar_new['k_progetto'] = trim($row['D_CJ']);
        
        $ar_new['expanded'] = true;
        $ar_new['leaf'] = true;
        
        $ar_new['row_cls'] = 'grassetto';
        
        $t_ar_liv1 = &$ar_r[$liv];
        
        
        //recupero CV in base a indice/kiosk
        $row_kiosk = get_row_kiosk_from_indice($cfg_mod, $row['ASORD1'], $row['ASORD2']);
        if ($row_kiosk){
            $_rilav_H_for_query_CD_kiosk = _rilav_H_for_query(
                array('f_ditta' => 'CDDT', 'field_IDPR' => 'CDIDPR'),   //m_table_config
                $cfg_mod['file_cfg_distinte'],
                $cfg_mod['file_tabelle'],
                'CD',
                'CVOUT',
                array('CV')
                );
            
            
            $sql = "SELECT {$_rilav_H_for_query_CD_kiosk['select']}
            FROM {$cfg_mod['file_cfg_distinte']} CD
            {$_rilav_H_for_query_CD_kiosk['joins']}
            WHERE CDDT = '{$id_ditta_default}' AND CDIDPR = {$row_kiosk['CDIDPR']}";
            
            $stmtCDKiosk = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtCDKiosk);
            echo db2_stmt_errormsg();
            
            $rowCDKiosk = db2_fetch_assoc($stmtCDKiosk);
            
            if ($rowCDKiosk){
                $ar_new['H_CV_COD'] = $rowCDKiosk['H_CV_COD'];
                $ar_new['H_CV_ICONA'] = $rowCDKiosk['H_CV_ICONA'];
                $ar_new['H_CV_STYLE_CLASS'] = $rowCDKiosk['H_CV_STYLE_CLASS'];
            } else {
                $ar_new['H_CV_COD'] = '';
                $ar_new['H_CV_ICONA'] = '';
                $ar_new['H_CV_STYLE_CLASS'] = '';
            }
        } //if $row_kiosk
        
        
        $ar_r["{$liv}"] = $ar_new;
        
    }
    
    $ar_r = &$ar_r["{$liv}"];
    //sottolivello
    //se cod_liv2 is not null creo sotto livello
    //e imposto a false il leaf di liv1
    if(!is_null($cod_liv2)){
        
        $liv=$cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] =  $row['CODICE'];
            $ar_new['desc'] =  trim($row['DESC']);
            $ar_new['causale'] =  $row['ASCAAS'];
            $ar_new['r_note'] = utf8_decode($row['ASNORI']);
            
            $ar_new['k_distinta'] = implode("|", array(trim($row['ASORD1']), trim($row['ASORD2']), trim($row['ASORD3'])));
            
            if(trim($row['ASFLRI']) == 'Y'){
                $causali_rilascio = $s->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                $ar_new['d_rilav'] = $causali_rilascio[0]['text'];
                $ar_new['t_rilav'] = "<b>{$row['ASCARI']}</b>";
                $ar_new['t_rilav'] .= "<br>" .trim($row['ASNORI']);
            }
            $ar_new['prog'] =  $row['ASIDPR'];
            $ar_new['prog_coll'] =  $row['ASIDAC'];
            $ar_new['note'] =  $desk_art->has_nota_progetto($row['ASIDPR']);
            $row_nt =  $desk_art->get_note_progetto($row['ASIDPR']);
            $ar_new['t_note'] =  utf8_decode($row_nt['NTMEMO']);
            $ar_new['imm'] = print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
            $ar_new['ut_ass'] = trim($row['ASUSAT']);
            $ar_new['ut_ins'] = trim($row['ASUSAS']);
            $ar_new['scadenza'] = trim($row['ASDTSC']);
            $ar_new['memo'] = utf8_decode(trim($row['MEMO']));
            $ar_new['riferimento'] = trim($row['ASNOTE']);
            $ar_new['f_ril'] =  trim($row['ASFLNR']);
            $ar_new['flag'] =  trim($row['ASFLRI']);
            $ar_new['liv'] = 'liv_3';
            $ar_new['liv_c'] = $liv;
            $ar_new['liv_type'] = 'LISTA';
            
            $ar_new['ASORD1'] = trim($row['ASORD1']);
            $ar_new['ASORD2'] = trim($row['ASORD2']);
            $ar_new['ASORD3'] = trim($row['ASORD3']);
            
            $ar_new['k_progetto'] = trim($row['D_CJ']);
            $ar_new['leaf'] = true;
            
            $ar_new['c_colli'] = _pp_count_colli($main_module, $row['D_CJ'], $row['ASIDPR']);
            
            
            $t_ar_liv1['leaf'] = false;
            $ar_r["{$liv}"] = $ar_new;
        }
        
    }
    
}

//$row_indici = $s->get_TA_std('PPP', $form_values->f_prog);


echo "<div id='my_content'>"; 

echo "<div class=header_page>";
echo "<H2>Report programmi produzione</H2>";
echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";
        
echo "<table class=int1>";
        
        
        echo "<tr class='liv_data'>
          <th>ID Program/Kiosk</th>
          <th>CV</th>
          <th>Descrizione</th>
          <th><img src=". img_path('icone/48x48/barcode.png')." width=20></th>
          <th><img src=". img_path('icone/48x48/monitor.png')." width=20></th>
          <th><img src=". img_path('icone/48x48/outbox.png')." width=20></th>

          <th>Data rilascio</th>
          <th><img src=". img_path('icone/48x48/info_black.png')." width=20></th>
          <th><img src=". img_path('icone/48x48/windows.png')." width=20></th>
          <th><img src=". img_path('icone/48x48/button_blue_play.png')." width=20></th>
          <th><img src=". img_path('icone/48x48/button_blue_pause.png')." width=20></th>
          <th><img src=". img_path('icone/48x48/check_green.png')." width=20></th>
          <th><img src=". img_path('icone/48x48/warning_blue.png')." width=20></th>
          <th>Distinta uscite</th>
          <th>Filtro</th>";
     
        echo  "</tr>";
  
        
foreach($ar as $k => $v){
    
    echo "<tr class=liv1>
    <td>{$v['task']}</td>
    <td>{$v['H_CV_COD']}</td>
    <td>{$v['desc']}</td>
    <td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>{$v['imm']}</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "</tr>";
    
    foreach($v['children'] as $k1 => $v1){
        
        $filtro = get_k_lista_from_as($main_module, $v1);
       
        $icona_cv = trim($v1['H_CV_ICONA']);
        $icona_dis = trim($v1['H_DIS_ICONA']);
        $icona_ini = trim($v1['H_INI_ICONA']);
        $icona_sos = trim($v1['H_SOS_ICONA']);
        $icona_end = trim($v1['H_END_ICONA']);
    
        echo "<tr class=liv2>
        <td>{$v1['task']}</td>";
        echo "<td><img src=" . img_path("icone/48x48/{$icona_cv}.png") . " height=15></td>";
        echo "<td>{$v1['desc']}</td>";
        
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        
        echo "<td>{$v1['imm']}</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "</tr>";
        
        foreach($v1['children'] as $k2 => $v2){
            
            $filtro = get_k_lista_from_as($main_module, $v2);
            $progetto = implode("-", array($v2['ASORD1'], $v2['ASORD2'], $v2['ASORD3']));
          
            $icona_cv = trim($v2['H_CV_ICONA']);
            $icona_dis = trim($v2['H_DIS_ICONA']);
            $icona_ini = trim($v2['H_INI_ICONA']);
            $icona_sos = trim($v2['H_SOS_ICONA']);
            $icona_end = trim($v2['H_END_ICONA']);
              
            echo "<tr>
            <td>{$v2['task']}</td>";
            echo "<td><img src=" . img_path("icone/48x48/{$icona_cv}.png") . " height=15></td>";
            echo "<td>{$v2['desc']}</td>";
            
            
            if($v2['VISTA_COLLI'] == 'Y')
                echo "<td><img src=" . img_path("icone/48x48/barcode.png") . " height=15></td>";
            else
                echo "<td>&nbsp;</td>";
                    
            if($v2['liv_type'] != 'LISTA' || $v2['USCITA_SCHERMO'] != 'Y')
                echo "<td><img src=" . img_path("icone/48x48/monitor.png") . " height=15></td>";
            elseif($v2['liv_type'] == 'LISTA' && $v2['c_colli'] == 0 && $v2['USCITA_SCHERMO'] == 'Y')
                echo "<td><img src=" . img_path("icone/48x48/sub_black_remove.png") . " height=15></td>";
            else
                echo "<td><img src=" . img_path("icone/48x48/monitor.png") . " height=15></td>";
            
            if($v2['liv_type'] != 'LISTA' || $v2['USCITA_BATCH'] != 'Y')
                echo "<td><img src=" . img_path("icone/48x48/monitor.png") . " height=15></td>";
            elseif($v2['liv_type'] == 'LISTA' && $v2['c_colli'] == 0 && $v2['USCITA_BATCH'] == 'Y')
                echo "<td><img src=" . img_path("icone/48x48/sub_black_remove.png") . " height=15></td>";
            else
                echo "<td><img src=" . img_path("icone/48x48/outbox.png") . " height=15></td>";
           
            
            echo "<td>{$v2['imm']}</td>";
            echo "<td>{$v2['ha_note']}</td>";

            if($icona_dis != '')
                echo "<td><img src=" . img_path("icone/48x48/{$icona_dis}.png") . " height=15></td>";
            else
                echo "<td>&nbsp;</td>";
                    
            if($icona_ini != '')
                echo "<td><img src=" . img_path("icone/48x48/{$icona_ini}.png") . " height=15></td>";
            else
                echo "<td>&nbsp;</td>";
                            
            if($icona_sos != '')
                echo "<td><img src=" . img_path("icone/48x48/{$icona_sos}.png") . " height=15></td>";
            else
                echo "<td>&nbsp;</td>";
                    
            if($icona_end != '')
                echo "<td><img src=" . img_path("icone/48x48/{$icona_end}.png") . " height=15></td>";
            else
                echo "<td>&nbsp;</td>";
            
            echo "<td>&nbsp;</td>";
            
            echo "<td>{$progetto}</td>";
            echo "<td>{$filtro}</td>";
            echo "</tr>";
            
            
            
        }
        
    }
    
}
echo "</div>";

 		
?>

 </body>
</html>


<?php
exit;
}


