<?php

require_once("../../config.inc.php");
require_once("acs_panel_prog_prod_include.php");
require_once("acs_panel_indici_include.php");
require_once("acs_panel_indici_naviga_include.php");
require_once("acs_panel_indici_todolist_preview_include.php");
require_once("../base/_rilav_g_history_include.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));
$desk_art = new DeskArt(array('no_verify' => 'Y'));

$cfg_mod = $main_module->get_cfg_mod();

$m_params = acs_m_params_json_decode();

$_raggruppamenti_attivita = array('INI', 'SOS', 'TER');


function _icon_by_flag($field){
    global $_flag_config;
    $ret = "";
    $field_config = $_flag_config[$field];
    if (is_array($field_config))
    foreach ($field_config as $kf => $f){
      if (strlen($f['icon']) > 0)
        $ret .= "if (value.trim() == '{$kf}') return '<img src=" . img_path("icone/48x48/{$f['icon']}.png") . " width=18>';";
    }
    
    return $ret;
}




//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'exe_change_to_next_rilav_H'){
//---------------------------------------------------------------------
    $ret = _rilav_H_change_to_next_rilav($cfg_mod['file_tabelle'], $cfg_mod['file_assegna_ord_H'], 
        $m_params->rec_id, $m_params->causale, $m_params->group, $m_params->current_value);
    echo acs_je($ret);
    exit;
}



//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//---------------------------------------------------------------------
    ini_set('max_execution_time', 300);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    $as_where .= " AND ASPROG = " . sql_t($m_params->open_request->progetto);
    if (isset($m_params->open_request->kiosk))
        $as_where .= " AND ASORD2 = " . sql_t($m_params->open_request->kiosk);
 
    
    $_rilav_H_for_query = _rilav_H_for_query(
        array('f_ditta' => 'ASDT', 'field_IDPR' => 'ASIDPR'),   //m_table_config
        $cfg_mod['file_assegna_ord'],
        $cfg_mod['file_tabelle'],
        'ATT_OPEN', //alias tabella
        null, //sarebbe la causale
        $_raggruppamenti_attivita);
    
    $_rilav_H_for_query_CD = _rilav_H_for_query(
        array('f_ditta' => 'ASDT', 'field_IDPR' => 'ASORD4'),   //m_table_config
        $cfg_mod['file_cfg_distinte'],
        $cfg_mod['file_tabelle'],
        'ATT_OPEN',
        'CVOUT',
        array('CV')
        );
        
        
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.TADESC AS D_AT, TA_PJ.TADTGE AS DATA_PJ,
            TA_PJ.TAMAIL AS D_CJ, TA_PJ.TADESC AS D_PJ, 
            /*NT_MEMO.NTMEMO AS MEMO, */
            NT_CD.NTKEY2 AS CODICE,
            NT_CD.NTMEMO AS DESC, TA_ALLEGATI.NR_AL AS NR_AL,
            TA_PJ.TADTGE AS DATA_PJ, TA_PJ.TAORGE AS ORA_PJ, TA_PJ.TAUSGE AS US_PJ,
            TA_IND.TADESC AS D_IND, {$_rilav_H_for_query['select']}, {$_rilav_H_for_query_CD['select']},
            TA_TU.TAFG01 as VISTA_COLLI, TA_TU.TAFG02 AS USCITA_SCHERMO, TA_TU.TAFG03 AS USCITA_BATCH

            FROM {$cfg_mod['file_assegna_ord']} ATT_OPEN

            INNER JOIN {$cfg_mod['file_tabelle']} TA_ATTAV
                ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND TA_ATTAV.TATAID = 'ATTAV' AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1

            /* CD */
            LEFT OUTER JOIN {$cfg_mod['file_cfg_distinte']} CD
                ON ATT_OPEN.ASDT = CD.CDDT AND ATT_OPEN.ASORD4 = CD.CDIDPR

            LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_TU
                ON CD.CDDT = TA_TU.TADT AND CD.CDTPUS = TA_TU.TAKEY1 AND TA_TU.TATAID = 'CDTPU'

            /* decod. indice */
            LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_IND
                ON ATT_OPEN.ASDT = TA_IND.TADT AND ATT_OPEN.ASORD1 = TA_IND.TAKEY1 AND TA_IND.TATAID = '{$cfg_mod['taid_indici']}'

            LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_PJ
                ON ATT_OPEN.ASDT = TA_PJ.TADT AND ATT_OPEN.ASPROG = TA_PJ.TAKEY1 AND TA_PJ.TATAID = '{$cfg_mod['taid_progetto']}'

            /*LEFT OUTER JOIN {$cfg_mod['file_note']} NT_MEMO
	            ON NT_MEMO.NTTPNO = 'ASME5' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
            */
            
            LEFT OUTER JOIN {$cfg_mod['file_note']} NT_CD
	            ON NT_CD.NTTPNO = 'PRJC5' AND INTEGER(NT_CD.NTKEY1) = ATT_OPEN.ASIDPR AND NT_CD.NTSEQU=0
            
            LEFT OUTER JOIN (
                SELECT COUNT(*) AS NR_AL, TADT, TATAID, TAKEY1
                FROM {$cfg_mod['file_tabelle']}
                GROUP BY TAKEY1, TADT, TATAID) TA_ALLEGATI
            ON TA_ALLEGATI.TADT = ATT_OPEN.ASDT AND TA_ALLEGATI.TATAID = 'TDPAL' AND TA_ALLEGATI.TAKEY1 = ATT_OPEN.ASPROG
            
            {$_rilav_H_for_query['joins']}
            {$_rilav_H_for_query_CD['joins']}
            WHERE ASDT = '{$id_ditta_default}' AND ASPROG <> '' {$as_where}
            ORDER BY ASIDPR";
    
     
        
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            echo db2_stmt_errormsg();
            
            $ar = array();
            while ($row = db2_fetch_assoc($stmt)) {
                
                //stacco dei livelli
                $cod_liv0 = trim($row['ASPROG']);   //progetto                            
                $cod_liv1 = trim($row['ASORD2']);   //kiosk
                $cod_liv2 = $row['ASIDPR'];         //numeratore assoluto attivita'
                
                
                $tmp_ar_id = array();
                $ar_r= &$ar;
                
                //PROGETTO
                $liv =$cod_liv0;
                $tmp_ar_id[] = $liv;
                if (!isset($ar_r["{$liv}"])){
                    $ar_new = $row;
                    $ar_new['children'] = array();
                    $ar_new['id'] = implode("|", $tmp_ar_id);
                    $ar_new['task'] = "[#" . trim($row['ASPROG']) . "] " . trim($row['D_CJ']);
                    $ar_new['imm'] =  print_date($row['DATA_PJ'])."-".print_ora($row['ORA_PJ']);
                    
                    $ar_new['allegati'] = $row['NR_AL'];
                    $ar_new['ut_ass'] = "[".trim($row['US_PJ'])."]";
                    $ar_new['liv'] = 'liv_1';
                    $ar_new['liv_c'] = $liv;
                    $ar_new['liv_type'] = 'PROGETTO';
                    
                    $ar_new['k_progetto'] = trim($row['D_CJ']);
                    
                    $ar_new['indice'] = trim($row['ASORD1']);
                    $ar_new['desc'] .=  utf8_encode($row['D_IND']);
                    $ar_new['desc'] .=   '<br>' . $row['D_PJ'];
                    
                    
                    $ar_new['expanded'] = true;
                    
                    $ar_r["{$liv}"] = $ar_new;
                }
                $ar_r = &$ar_r["{$liv}"];
                
                //Kiosk
                $liv=$cod_liv1;
                $ar_r = &$ar_r['children'];
                $tmp_ar_id[] = $liv;
                if(!isset($ar_r[$liv])){
                    $ar_new = $row;
                    $ar_new['children'] = array();
                    $ar_new['id'] = implode("|", $tmp_ar_id);
                    $ar_new['task'] = $row['ASORD2'];
                    $ar_new['desc'] =  decod_kiosk_from_indice($cfg_mod, $row['ASORD1'], $row['ASORD2']);
                    //$ar_new['causale'] =  $row['ASCAAS'];

                    $ar_new['liv'] = 'liv_2';
                    $ar_new['liv_c'] = $liv;
                    $ar_new['liv_type'] = 'KIOSK';
                    
                    $ar_new['k_progetto'] = trim($row['D_CJ']);
                    
                    $ar_new['expanded'] = true;
                    $ar_new['leaf'] = true;
                    
                    $ar_new['row_cls'] = 'grassetto';
                    
                    $t_ar_liv1 = &$ar_r[$liv];
                    
                    
                    //recupero CV in base a indice/kiosk 
                    $row_kiosk = get_row_kiosk_from_indice($cfg_mod, $row['ASORD1'], $row['ASORD2']);
                    if ($row_kiosk){
                        $_rilav_H_for_query_CD_kiosk = _rilav_H_for_query(
                            array('f_ditta' => 'CDDT', 'field_IDPR' => 'CDIDPR'),   //m_table_config
                            $cfg_mod['file_cfg_distinte'],
                            $cfg_mod['file_tabelle'],
                            'CD',
                            'CVOUT',
                            array('CV')
                            );
                        
                        
                        $sql = "SELECT {$_rilav_H_for_query_CD_kiosk['select']}
                                FROM {$cfg_mod['file_cfg_distinte']} CD
                                {$_rilav_H_for_query_CD_kiosk['joins']}
                                WHERE CDDT = '{$id_ditta_default}' AND CDIDPR = {$row_kiosk['CDIDPR']}";
                        
                        $stmtCDKiosk = db2_prepare($conn, $sql);
                        echo db2_stmt_errormsg();
                        $result = db2_execute($stmtCDKiosk);
                        echo db2_stmt_errormsg();
                        
                        $rowCDKiosk = db2_fetch_assoc($stmtCDKiosk);
                        
                        if ($rowCDKiosk){
                            $ar_new['H_CV_COD'] = $rowCDKiosk['H_CV_COD'];
                            $ar_new['H_CV_ICONA'] = $rowCDKiosk['H_CV_ICONA'];
                            $ar_new['H_CV_STYLE_CLASS'] = $rowCDKiosk['H_CV_STYLE_CLASS'];
                        } else {
                            $ar_new['H_CV_COD'] = '';
                            $ar_new['H_CV_ICONA'] = '';
                            $ar_new['H_CV_STYLE_CLASS'] = '';
                        }                        
                    } //if $row_kiosk
                    
                    
                    $ar_r["{$liv}"] = $ar_new;
                    
                }
                
                $ar_r = &$ar_r["{$liv}"];
                //sottolivello
                //se cod_liv2 is not null creo sotto livello
                //e imposto a false il leaf di liv1
                if(!is_null($cod_liv2)){
                    
                    $liv=$cod_liv2;
                    $ar_r = &$ar_r['children'];
                    $tmp_ar_id[] = $liv;
                    if(!isset($ar_r[$liv])){
                        $ar_new = $row;
                        $ar_new['id'] = implode("|", $tmp_ar_id);
                        $ar_new['task'] =  $row['CODICE'];
                        $ar_new['desc'] =  trim($row['DESC']);
                        $ar_new['causale'] =  $row['ASCAAS'];
                        $ar_new['r_note'] = utf8_decode($row['ASNORI']);
                        
                        $ar_new['k_distinta'] = implode("|", array(trim($row['ASORD1']), trim($row['ASORD2']), trim($row['ASORD3'])));
                        
                        if(trim($row['ASFLRI']) == 'Y'){
                            $causali_rilascio = $s->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                            $ar_new['d_rilav'] = $causali_rilascio[0]['text'];
                            $ar_new['t_rilav'] = "<b>{$row['ASCARI']}</b>";
                            $ar_new['t_rilav'] .= "<br>" .trim($row['ASNORI']);
                        }
                        $ar_new['prog'] =  $row['ASIDPR'];
                        $ar_new['prog_coll'] =  $row['ASIDAC'];
                        $ar_new['note'] =  $desk_art->has_nota_progetto($row['ASIDPR']);
                        $row_nt =  $desk_art->get_note_progetto($row['ASIDPR']);
                        $ar_new['t_note'] =  utf8_decode($row_nt['NTMEMO']);
                        $ar_new['imm'] = print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
                        $ar_new['ut_ass'] = trim($row['ASUSAT']);
                        $ar_new['ut_ins'] = trim($row['ASUSAS']);
                        $ar_new['scadenza'] = trim($row['ASDTSC']);
                        $ar_new['memo'] = utf8_decode(trim($row['MEMO']));
                        $ar_new['riferimento'] = trim($row['ASNOTE']);
                        $ar_new['f_ril'] =  trim($row['ASFLNR']);
                        $ar_new['flag'] =  trim($row['ASFLRI']);
                        $ar_new['liv'] = 'liv_3';
                        $ar_new['liv_c'] = $liv;
                        $ar_new['liv_type'] = 'LISTA';
                        $ar_new['k_progetto'] = trim($row['D_CJ']);
                        $ar_new['leaf'] = true;
                        
                        $ar_new['c_colli'] = _pp_count_colli($main_module, $row['D_CJ'], $row['ASIDPR']);
                        
                        
                        $t_ar_liv1['leaf'] = false;
                        $ar_r["{$liv}"] = $ar_new;
                    }
                    
                }
                
            }
            
            
            
            foreach($ar as $kar => $r){
                $ret[] = array_values_recursive($ar[$kar]);
            }
            
            echo acs_je(array('success' => true, 'children' => $ret));
            exit;
}



//----------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_panel'){
//----------------------------------------------------------------

    //recupero progetto
    $sql = "SELECT * FROM {$cfg_mod['file_tabelle']} TA_PJ
            WHERE TADT = '{$id_ditta_default}' 
            AND TA_PJ.TATAID = '{$cfg_mod['taid_progetto']}'
            AND TA_PJ.TAKEY1 = ?";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->progetto));
    echo db2_stmt_errormsg();
    $row = db2_fetch_assoc($stmt);
    if ($row)
        $pj_cod = trim($row['TAMAIL']);
    else
        $pj_cod = 'ERROR';
    
    if (isset($m_params->kiosk)){
        
        if(strlen($m_params->mansione) > 0){
            $row_mans = $s->find_TA_std('PRGRO', $m_params->mansione);
            $descrizione = trim($row_mans[0]['text']);
            $tab_title = "Task {$pj_cod}";
            $tbar_title = "Attivit� logistico/produttive per mansione [{$m_params->mansione}] {$descrizione}";
        }else{
            $tab_title = "Kiosk {$m_params->kiosk} $pj_cod";
            $tbar_title = "Attivit&agrave; logistico/produttive per centro di rilascio";
        }
        
       
    }
    else {
        
        if(strlen($m_params->mansione) > 0){
            $row_mans = $s->find_TA_std('PRGRO', $m_params->mansione);
            $descrizione = trim($row_mans[0]['text']);
            $tab_title = "Task {$pj_cod}";
            $tbar_title = "Attivit� logistico/produttive per mansione [{$m_params->mansione}] {$descrizione}";
        }else{
            
            $tab_title = "Prg $pj_cod";
            $tbar_title = "Progetto attivit&agrave; logistico/produttive e centri di rilascio";
        }
       
    }
?>

{"success":true, "items": [

     {
        xtype: 'treepanel' ,
        multiSelect: true,
        cls: 's_giallo',
        title: <?php echo j($tab_title)?>,
	    tbar: new Ext.Toolbar({
	            items:[<?php echo j("<b>" . $tbar_title . "</b>")?>, '->',
	            
					  {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,         
		    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['r_note', 'prog_coll', 'causale', 'id', 'task', 'desc', 'segnalazioni', 'liv', 'liv_c', 'liv_type', 'flag', 'prog', 'imm', 'ut_ins', 'ut_ass', 'scadenza', 'riferimento', 
                    		 'c_colli', 'memo', 'f_ril', 'd_rilav', 't_rilav', 'note', 't_note', 'allegati', 'indice', 'indice_out', 'row_cls', 'k_progetto', 'k_distinta',
                    		 'ASFVIU', <?php echo _rilav_H_write_model_fields($_raggruppamenti_attivita) ?>,
                    		 'VISTA_COLLI', 'USCITA_SCHERMO', 'USCITA_BATCH',
                    		 <?php echo _rilav_H_write_model_fields(array('CV')) ?>],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						extraParams: {
						  open_request: <?php echo acs_je($m_params); ?>,
	                      form_values: <?php echo acs_je($m_params->form_values); ?>
	                  
                      }
                    , doRequest: personalizza_extraParams_to_jsonData  ,     
						reader: {
                            root: 'children'
                        }        				
                    }

                }),
                
                <?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
    	    			
            columns: [ {xtype: 'treecolumn', 
        	    		text: 'ID Program/Kiosk/ToDo', 	
        	    		width: 250,
        	    		dataIndex: 'task',
        	    		 renderer: function(value, p, record){
						   	if(record.get('flag') == 'Y')
    						       p.tdCls += ' barrato';
    					    return value;
    		    	    }
        	    		}
                       , {text: 'CV',					
    		          		dataIndex: 'H_CV_COD', tooltip: 'Ciclo vita', width: 35, align: 'right',
    		          		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          		  if (rec.get('liv_type') == 'LISTA' || rec.get('liv_type') == 'KIOSK'){	
    		          			<?php echo _rilav_H_icon_style('CV') ?>    		          			
							  }
        	        	}},
						{
        	    			text: 'Descrizione',
        	    			dataIndex: 'desc',
        	    			flex: 1
        	    		}
        	    		, { //apertura colli
    		            xtype:'actioncolumn', width:35,
    		            text: '<img src=<?php echo img_path("icone/24x24/barcode.png") ?>>',
    		            tdCls: 'tdAction', tooltip: 'Visualizza colli',
    		            menuDisabled: true, sortable: false,
    		            renderer: function(value, p, record){
    		             	if (record.get('liv_type') == 'LISTA' && record.get('c_colli') == 0) p.tdCls += ' sfondo_rosso';
    		            },
    		            items: [{
    		                icon: <?php echo img_path("icone/24x24/barcode.png") ?>,
    		                handler: function(view, rowIndex, colIndex, item, e, record, row) {
    		                  if (record.get('liv_type') == 'LISTA')
    		                	acs_show_win_std(null,
			        				'acs_panel_indici_todolist_colli.php?fn=open_tab', {
			        				    id_tree: record.get('id'),
			        				    d_voce: record.get('desc'),
			        					k_progetto: record.get('k_progetto'),
			        					k_distinta: record.get('k_distinta'),
			        				    id_assoluto_lista: record.get('liv_c')
			        				});	                	
    		                },
    		                isDisabled: function(view, rowIndex, colIndex, item, record) {
                              if (record.get('liv_type') != 'LISTA' || record.get('VISTA_COLLI') != 'Y')
                                return true;
                              },
            				getClass: function(value,metadata,record){
                            if (record.get('liv_type') != 'LISTA' || record.get('VISTA_COLLI') != 'Y')
                                return 'x-hide-display';    
                           
                        	}
    		            }]
    		          }
                , { //preview
    		            xtype:'actioncolumn', width:35,
    		            text: '<img src=<?php echo img_path("icone/24x24/monitor.png") ?> >',
    		            tdCls: 'tdAction', tooltip: 'Preview',
    		            menuDisabled: true, sortable: false,
    		            renderer: function(value, p, record){
    		             	if (record.get('liv_type') == 'LISTA' && record.get('c_colli') == 0 && record.get('USCITA_SCHERMO') == 'Y') 
    		           	 		return '<img src=<?php echo img_path("icone/24x24/sub_black_remove.png") ?>>';
    		            },
    		            items: [{
    		               icon: <?php echo img_path("icone/24x24/monitor.png") ?>,
    		               handler: function(view, rowIndex, colIndex, item, e, record, row) {
    		                   if (record.get('liv_type') == 'LISTA')
    		                	acs_show_win_std(null,
			        				'acs_panel_indici_todolist_preview.php?fn=open_tab', {
			        				    id_tree: record.get('id'),
			        					k_progetto: record.get('k_progetto'),
			        					k_distinta: record.get('k_distinta'),
			        					d_voce: record.get('desc'),
			        					id_assoluto_lista: record.get('liv_c')
			        				});	                	
    		                },
    		                isDisabled: function(view, rowIndex, colIndex, item, record) {
                              if (record.get('liv_type') != 'LISTA' || record.get('USCITA_SCHERMO') != 'Y')
                                return true;
                              if (record.get('liv_type') == 'LISTA' && record.get('c_colli') == 0 && record.get('USCITA_SCHERMO') == 'Y') 
                                return true;
            				},
            				getClass: function(value,metadata,record){
                				 if(record.get('liv_type') != 'LISTA' || record.get('USCITA_SCHERMO') != 'Y')
                				    return 'x-hide-display';
            				     if (record.get('liv_type') == 'LISTA' && record.get('c_colli') == 0 && record.get('USCITA_SCHERMO') == 'Y') 
                              		return 'x-hide-display';     
                                  
                        	}
    		               
    		            }]
    		          },
    		          { //esegui
    		            xtype:'actioncolumn', width:35,
    		            text: '<img src=<?php echo img_path("icone/48x48/outbox.png") ?> width=24>',
    		            tdCls: 'tdAction', tooltip: 'Esegui',
    		            menuDisabled: true, sortable: false,
    		             renderer: function(value, p, record){
    		             	if (record.get('liv_type') == 'LISTA' && record.get('c_colli') == 0 && record.get('USCITA_BATCH') == 'Y') 
    		           	 		return '<img src=<?php echo img_path("icone/24x24/sub_black_remove.png") ?>>';
    		            },
    		            items: [{
    		                icon: <?php echo img_path("icone/24x24/outbox.png") ?>,
    		                handler: function(view, rowIndex, colIndex, item, e, record, row) {
    		                   if (record.get('liv_type') == 'LISTA')
    		                	acs_show_win_std(null,
			        				'acs_panel_indici_todolist_esegui.php?fn=open_tab', {
			        				    id_tree: record.get('id'),
			        					k_progetto: record.get('k_progetto'),
			        					k_distinta: record.get('k_distinta'),
			        					d_voce: record.get('desc'),
			        					id_assoluto_lista: record.get('liv_c')
			        				});	
    		                },
    		                isDisabled: function(view, rowIndex, colIndex, item, record) {
                              if (record.get('liv_type') != 'LISTA' || record.get('USCITA_BATCH') != 'Y')
                                return true;
                              if (record.get('liv_type') == 'LISTA' && record.get('c_colli') == 0 && record.get('USCITA_BATCH') == 'Y') 
                                return true;
            				},
            				getClass: function(value,metadata,record){
                				 if(record.get('liv_type') != 'LISTA'  || record.get('USCITA_BATCH') != 'Y')
                				    return 'x-hide-display';    
                                  if (record.get('liv_type') == 'LISTA' && record.get('c_colli') == 0 && record.get('USCITA_BATCH') == 'Y') 
                              		return 'x-hide-display'; 
                        	}
    		            }]
    		          }
    		          
	 				, {text: 'Data rilascio', width: 140, dataIndex: 'imm'}
	 				
    	        	, {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=20>',					
		          		dataIndex: 'ha_note', tooltip: 'Note esplicative', width: 35, align: 'right', 
		          		renderer: function (value, metaData, rec, row, col, store, gridView){
		          		  if (rec.get('liv_type') == 'LISTA'){	
		          			
						  }
    	        	}}
        	        	
    		          
    		          

    		          
    		          
    		         , {text: '<img src=<?php echo img_path("icone/48x48/windows.png") ?> width=20>',					
    		          		dataIndex: 'H_DIS_COD', tooltip: 'Visionato', width: 35, align: 'right',
    		          		//exeChangeFlag: 'Y',
    		          		exeGestRilavH: 'DIS',
    		          		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          		  if (rec.get('liv_type') == 'LISTA'){	
    		          			<?php echo _rilav_H_icon_style('DIS') ?>
							  }
        	        	}}
        	        	
        	        	
        	        	
        	        , {text: '<img src=<?php echo img_path("icone/48x48/button_blue_play.png") ?> width=20>',					
    		          		dataIndex: 'H_INI_COD', tooltip: 'Iniziata', width: 35, align: 'right', 
    		          		exeGestRilavH: 'INI',
    		          		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          		  if (rec.get('liv_type') == 'LISTA'){	
    		          			<?php echo _rilav_H_icon_style('INI') ?>
    		          		  }
        	        		}}
        	        	
        	        , {text: '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=20>',					
    		          		dataIndex: 'H_SOS_COD', tooltip: 'Sospeso', width: 35, align: 'right', 
    		          		//exeChangeRilavH: 'SOS',
    		          		exeGestRilavH: 'SOS',
    		          		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          		  if (rec.get('liv_type') == 'LISTA'){	
    		          		  	<?php echo _rilav_H_icon_style('SOS') ?>
							  }
        	        		}}
        	        	
        	        , {text: '<img src=<?php echo img_path("icone/48x48/check_green.png") ?> width=20>',					
    		          		dataIndex: 'H_END_COD', tooltip: 'Sospeso', width: 35, align: 'right', 
    		          		exeChangeFlag: 'Y',
    		          		exeGestRilavH: 'END',
    		          		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          		  if (rec.get('liv_type') == 'LISTA'){
    		          		 	if(rec.get('c_colli') == 0) 
    		          		 		return '<img src=<?php echo img_path("icone/24x24/sub_black_remove.png") ?>>';
    		          		    else
    		          		      <?php echo _rilav_H_icon_style('END') ?>
    		          		  }
        	        		}}
        	        		
	 					
	 				, {text: '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=20>',					
    		          		dataIndex: 'ha_note', tooltip: 'Segnalazioni', width: 35, align: 'right', 
    		          		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          		  if (rec.get('liv_type') == 'LISTA'){	
    		          			
							  }
        	        	}}
        	        , {text: 'Fine attivit&agrave;', width: 140, dataIndex: ''}
        	        , <?php echo dx_mobile() ?>
	 					
        	    		
    	    ],
    	    
    	    listeners: {
    	        beforeload: function(store, options) {
                    Ext.getBody().mask('Loading... ', 'loading').show();
                    },

                load: function () {
                  Ext.getBody().unmask();
                }, 
	         
    	      itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
				var voci_menu = [];
				
			    
			    		 
			    		 
			    		 
	    		 		/*var my_listeners = {
				    		afterInsert: function(from_win, jsonData){
				    			rec.set(jsonData.item);
				        	 	rec.commit();
				    			from_win.destroy();
				    		}
				    	}
						
        	               
        	            voci_menu.push({
			         		text: 'History Sospeso (SOS)',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
			        		   acs_show_win_std(null, '../base/acs_panel_rilav_H.php?fn=open_tab', {
			        		   		file_H: <?php echo j($cfg_mod['file_assegna_ord_H'])?>,
		        		   			file_TA: <?php echo j($cfg_mod['file_tabelle'])?>,
            	            		causale: rec.get('causale'),
        	            			rec_id: rec.get('prog'),
        	            			group: 'SOS'
            	               }, null, null, my_listeners);
 						     }
			    		 });
			    		 
			    		 voci_menu.push({
			         		text: 'History Iniziato (INI)',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
			        		   acs_show_win_std(null, '../base/acs_panel_rilav_H.php?fn=open_tab', {
			        		   		file_H: <?php echo j($cfg_mod['file_assegna_ord_H'])?>,
		        		   			file_TA: <?php echo j($cfg_mod['file_tabelle'])?>,
            	            		causale: rec.get('causale'),
        	            			rec_id: rec.get('prog'),
        	            			group: 'INI'
            	               }, null, null, my_listeners);
 						     }
			    		 });
			    		 
			    		 voci_menu.push({
			         		text: 'Preview',
			        		iconCls : 'icon-search-16',          		
			        		handler: function () {
    		                  if (rec.get('liv_type') == 'LISTA')
    		                	acs_show_win_std(null,
			        				'acs_panel_indici_todolist_preview.php?fn=open_tab', {
			        				    id_tree: rec.get('id'),
			        					k_progetto: rec.get('k_progetto'),
			        					k_distinta: rec.get('k_distinta'),
			        					d_voce: rec.get('desc'),
			        					id_assoluto_lista: rec.get('liv_c')
			        				});	  
 						     }
			    		 });*/
			    
			    		 
    		    var menu = new Ext.menu.Menu({
	             items: voci_menu
		        }).showAt(event.xy);
		        
			        
			        
			        	
				  
			  } //itemcontextmenu
			  
			  
			  
			  
			  
			  , celldblclick: {								
				   fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  var rec = iView.getRecord(iRowEl);
					  var grid = this;
					  var col = iView.getGridColumns()[iColIdx];
					  var col_name = iView.getGridColumns()[iColIdx].dataIndex;			
					  
					    if (rec.get('liv_type') != 'LISTA' || rec.get('c_colli') == 0) return;
					    
					    
					    //Avanzamento a next RILAV
						if (!Ext.isEmpty(col.exeChangeRilavH))
						Ext.Ajax.request({
 						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_change_to_next_rilav_H',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   causale: rec.get('causale'),
 			        			   group: col.exeChangeRilavH,
 			        			   current_value: rec.get(col_name),
 			        			   rec_id: rec.get('prog')
 								},
 								
 								success: function(response, opts) {
						        	 var jsonData = Ext.decode(response.responseText);
						        	 
						        	 if (!jsonData.success){
						        	 	acs_show_msg_error(jsonData.message);
						        	 	return false;
						        	 }
						        	 
						        	 rec.set(jsonData.item);
						        	 rec.commit();
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
 						    
 						    //Gestione History RILAV
 						    if (!Ext.isEmpty(col.exeGestRilavH)){
 						    	var my_listeners = {
 						    		afterInsert: function(from_win, jsonData){
 						    			rec.set(jsonData.item);
						        	 	rec.commit();
 						    			from_win.destroy();
 						    		}
 						    	}
     							acs_show_win_std(null, '../base/acs_panel_rilav_H.php?fn=open_tab', {
     									file_H: <?php echo j(_rilav_H_file())?>,
    			        		   		file: <?php echo j(_rilav_H_db_file_Orig($cfg_mod['file_assegna_ord']))?>,
    			        		   		file_TA: <?php echo j($cfg_mod['file_tabelle'])?>,
    			        		   		causale: rec.get('causale'),
                	            		rec_id: rec.get('prog'),
                	            		group: col.exeGestRilavH
                	               }, null, null, my_listeners);
                	                
                	        }
 						    
 						    
					    
					}
               }		 	
    	    }, //listeners
				 
			viewConfig: {
				 toggleOnDblClick: false,
		         getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
 		           return v;																
		         }   
		    }	
    	    
    	}	//tree
	 	
	]  	
  }


<?php 
exit;
}