<?php 

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();
$main_module =  new DeskProd();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $main_module->get_cfg_mod();
$row_gr = $main_module->get_TA_std('PRGRO', $m_params->TAKEY1);
$t_dettagli = trim($row_gr['TADESC']);

$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "PRGRU - Operatori mansione [{$m_params->TAKEY1}]",
    'descrizione' => "Gestione operatori mansione",
    'form_title' => "Dettagli operatori mansione [{$m_params->TAKEY1}] {$t_dettagli}",
    'fields_preset' => array(
        'TATAID' => 'PRGRU',
        'TAKEY1' => $m_params->open_request->TAKEY1
    ),
  
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TAKEY2', 'TADESC'),
    'fields_grid' => array('TAKEY2_d', 'TADESC', 'TAMAIL', 'TARIF1_d', 'immissione'),
    'fields_form' => array('TAKEY2', 'TADESC', 'TAMAIL', 'TARIF1'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Gruppo', 'fw'=>'width: 100', 'type' => 'from_TA', 'TAID' => 'PRGRO'),
        'TAKEY2' => array('label'	=> 'Operatore', 'fw'=>'width: 200', 'type' => 'from_TA', 'TAID' => 'PROPE', 'desc' => 'Y'),
        'TAKEY2_d' => array('label'	=> 'Operatore', 'fw'=>'width: 200'),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        'TARIF1' => array('label'	=> 'Reparto', 'type' => 'from_TA', 'TAID' => 'REPMA', 'desc'=> 'Y', 'module' => $desk_art),
        'TARIF1_d' => array('label'	=> 'Reparto'),
       
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
        
        
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
