<?php

require_once("../../config.inc.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once('acs_panel_indici_config_distinta_filtri_include.php');

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'exe_save_filtri_colli'){
//----------------------------------------------------------------------

  $lista = $m_params->lista;
  if (strlen($lista) == 0){
      echo acs_je(array('success' => false, 'message' => 'Parametro lista obbligatorio'));
      exit;
  }
  
  //elimino i dati attuali
  _distinta_filtri_reset($cfg_mod['file_cfg_distinte_liste'], $lista, 'PL');
  
  //inserisco i nuovi dati impostati
  _distinta_filtri_save_params($cfg_mod['file_cfg_distinte_liste'], $lista, 'PL', $m_params->form_values);
    
  echo acs_je(array('success' => true));
exit;
}


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'exe_save_filtri_compo'){
//----------------------------------------------------------------------
    
    $lista = $m_params->lista;
    if (strlen($lista) == 0){
        echo acs_je(array('success' => false, 'message' => 'Parametro lista obbligatorio'));
        exit;
    }
    
    //elimino i dati attuali
    _distinta_filtri_reset($cfg_mod['file_cfg_distinte_liste'], $lista, 'RP');
    
    //inserisco i nuovi dati impostati
    _distinta_filtri_save_params($cfg_mod['file_cfg_distinte_liste'], $lista, 'RP', $m_params->form_values);
    
    echo acs_je(array('success' => true));
    exit;
}



//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'form_colli'){
//----------------------------------------------------------------------
  $values = _distinta_filtri_load_values($cfg_mod['file_cfg_distinte_liste'], $m_params->lista, 'PL');
  $puci_data = find_TA_sys('PUCI', null, '', '', null,null, 0, '', 'Y', 'Y', 'R', false);
  $puci_data[] = array('id' => '', 'text' => '- Vuoto -');
  $lipc_data = find_TA_sys('LIPC', null, '', '', null,null, 0, '', 'Y', 'Y', 'R', false);
  $lipc_data[] = array('id' => '', 'text' => '- Vuoto -');
  $puop_data = find_TA_sys('PUOP', null, '', '', null,null, 0, '', 'Y', 'Y', 'R', false);
  $puop_data[] = array('id' => '', 'text' => '- Vuoto -');
?>
{"success":true, 

	m_win: {
            	title: <?php echo j("Impostazione selezioni filtro colli [{$m_params->lista}] {$m_params->d_lista}"); ?>,
            	//maximize: 'Y',
            	width : 700,
            	height : 500,
            	iconCls: 'icon-windows-16'
            },

	"items": [
        {
            xtype: 'form',
            flex: 1,
            
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            layout: {border: false, pack: 'start', align: 'stretch'},
            
            items: [
            
             {
            	xtype: 'tabpanel',
            	items: [
            	
            	{
            		xtype: 'panel', flex: 1,
            		bodyPadding: '5 5 0',
            		title: 'Codice articolo',
            		defaults: {bodyPadding: '5 5 0'},
            		layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
            		items: [
            		   {
        					xtype: 'fieldset', flex: 1,
        					
        					items: <?php echo_je(array(
        					    _df_IE('IE_PL_ART', $values),
        					    extjs_cod_char_intestazione(15),
        					    _df_cod_char(15, 'Selezione 1', 'PL_ART_01', $values),
        					    _df_cod_char(15, 'Selezione 2', 'PL_ART_02', $values),
        					    _df_cod_char(15, 'Selezione 3', 'PL_ART_03', $values),
        					    _df_cod_char(15, 'Selezione 4', 'PL_ART_04', $values),
        					    _df_cod_char(15, 'Selezione 5', 'PL_ART_05', $values),
        					)) ?>
        				},
        				 {
        					xtype: 'fieldset', flex: 1,
        					title : 'Altri filtri collo',
        					items: <?php echo_je(array(
        					    _df_IE('PL_CON_INC', $values, array('add_solo' => 'Y', 'label' => 'Colli con incasso')),
        					)) ?>
        				}
        				
            		]
            	},
            	{
            		xtype: 'panel', flex: 1,
            		bodyPadding: '5 5 0',
            		defaults: {bodyPadding: '5 5 0'},
            		title : 'Gruppo collo',
            		layout: {border: false, pack: 'start', align: 'stretch'},
            		items: [
            		   
        				, 
        				{
        					xtype: 'fieldset', flex: 1,
        					layout: <?php echo layout_ar('vbox') ?>,
        					defaults: {anchor: '-10'},
        					items: <?php echo_je(array(
        					    _df_IE('IE_PL_GC', $values),
        					    _df_cf('Selezione 1', 'PL_GC_01', $values, $puci_data),
        					    _df_cf('Selezione 2', 'PL_GC_02', $values, $puci_data),
        					    _df_cf('Selezione 3', 'PL_GC_03', $values, $puci_data),
        					    _df_cf('Selezione 4', 'PL_GC_04', $values, $puci_data),
        					    _df_cf('Selezione 5', 'PL_GC_05', $values, $puci_data),
        					    _df_cf('Selezione 6', 'PL_GC_06', $values, $puci_data),
        					    _df_cf('Selezione 7', 'PL_GC_07', $values, $puci_data),
        					    _df_cf('Selezione 8', 'PL_GC_08', $values, $puci_data),
        					    _df_cf('Selezione 9', 'PL_GC_09', $values, $puci_data),
        					    _df_cf('Selezione 10', 'PL_GC_10', $values, $puci_data))) ?>
        				}
            		]
            	},
            	
            	{
            	xtype: 'panel', flex: 1,
            		bodyPadding: '5 5 0',
            		defaults: {bodyPadding: '5 5 0'},
            	    title: 'Linee logistico/produttive',
            		layout: {border: false, pack: 'start', align: 'stretch'},
            		items: [
            			{
        					xtype: 'fieldset', flex: 1,
        					layout: <?php echo layout_ar('vbox') ?>,
        					defaults: {anchor: '-10'},
        					items: <?php echo_je(array(
        					    _df_IE('IE_PL_LP', $values),
        					    _df_cf('Selezione 1', 'PL_LP_01', $values, $lipc_data),
        					    _df_cf('Selezione 2', 'PL_LP_02', $values, $lipc_data),
        					    _df_cf('Selezione 3', 'PL_LP_03', $values, $lipc_data),
        					    _df_cf('Selezione 4', 'PL_LP_04', $values, $lipc_data),
        					    _df_cf('Selezione 5', 'PL_LP_05', $values, $lipc_data),
        					    _df_cf('Selezione 6', 'PL_LP_06', $values, $lipc_data),
        					    _df_cf('Selezione 7', 'PL_LP_07', $values, $lipc_data),
        					    _df_cf('Selezione 8', 'PL_LP_08', $values, $lipc_data),
        					    _df_cf('Selezione 9', 'PL_LP_09', $values, $lipc_data),
        					    _df_cf('Selezione 10', 'PL_LP_10', $values, $lipc_data))) ?>
        				}
            		]
            	},
            	
            	{
            	xtype: 'panel', flex: 1,
            	title: 'Filtri su Operazione',
            		bodyPadding: '5 5 0',
            		defaults: {bodyPadding: '5 5 0'},
            		layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
            		items: [
            			{
        					xtype: 'container',
        					layout: 'hbox',
        					items: [
                    			{
                					xtype: 'fieldset', flex: 1,
                					title: 'Filtri su Operazione 1',
                					defaults: {anchor: '-10'},
                					items: <?php echo_je(array(
                					    _df_IE('IE_PL_OPE1', $values),
                					    _df_cf('Selezione 1', 'PL_OPE1_01', $values, $puop_data),
                					    _df_cf('Selezione 2', 'PL_OPE1_02', $values, $puop_data),
                					    _df_cf('Selezione 3', 'PL_OPE1_03', $values, $puop_data),
                					    _df_cf('Selezione 4', 'PL_OPE1_04', $values, $puop_data),
                					    _df_cf('Selezione 5', 'PL_OPE1_05', $values, $puop_data),
                					    _df_cf('Selezione 6', 'PL_OPE1_06', $values, $puop_data),
                					    _df_cf('Selezione 7', 'PL_OPE1_07', $values, $puop_data),
                					    _df_cf('Selezione 8', 'PL_OPE1_08', $values, $puop_data),
                					    _df_cf('Selezione 9', 'PL_OPE1_09', $values, $puop_data),
                					    _df_cf('Selezione 10', 'PL_OPE1_10', $values, $puop_data))) ?>
                				}, {
                					xtype: 'fieldset', flex: 1,
                					title: 'Filtri su Operazione 2',
                					defaults: {anchor: '-10'},
                					items: <?php echo_je(array(
                					    _df_IE('IE_PL_OPE2', $values),
                					    _df_cf('Selezione 1', 'PL_OPE2_01', $values, $puop_data),
                					    _df_cf('Selezione 2', 'PL_OPE2_02', $values, $puop_data),
                					    _df_cf('Selezione 3', 'PL_OPE2_03', $values, $puop_data),
                					    _df_cf('Selezione 4', 'PL_OPE2_04', $values, $puop_data),
                					    _df_cf('Selezione 5', 'PL_OPE2_05', $values, $puop_data),
                					    _df_cf('Selezione 6', 'PL_OPE2_06', $values, $puop_data),
                					    _df_cf('Selezione 7', 'PL_OPE2_07', $values, $puop_data),
                					    _df_cf('Selezione 8', 'PL_OPE2_08', $values, $puop_data),
                					    _df_cf('Selezione 9', 'PL_OPE2_09', $values, $puop_data),
                					    _df_cf('Selezione 10', 'PL_OPE2_10', $values, $puop_data))) ?>
                				}
                			]
                		}
            		]
            	}
            	]}
            
            
            	
            ],
            
			buttons: [	
			{
		            text: 'Conferma',
			        iconCls: 'icon-button_blue_play-24', scale: 'medium',		            
		            handler: function() {
		            	var m_win = this.up('window'),
		            	    form = this.up('form').getForm(),
		            		form_values = form.getValues();
		            		
    		               if (form.isValid()){
    							Ext.Ajax.request({
     						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_filtri_colli',
     						        method  : 'POST',
     			        			jsonData: {
     			        			   form_values: form.getValues(),
     			        			   lista: <?php echo acs_je($m_params->lista); ?>
     								},
     								
     								success: function(response, opts) {
     									var jsonData = Ext.decode(response.responseText);
     									if (jsonData.success){
     									    m_win.destroy();
    						        	 	alert('Salvataggio eseguito');
    						        	 }
    			            		   
    			            		},
    						        failure    : function(result, request){
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
     								
     						    });
    			            }
		            	
		            }
		        }
	        
	        
	        ]            

}		
]}
<?php 
	exit;
}






//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'form_compo'){
    //----------------------------------------------------------------------
    $values = _distinta_filtri_load_values($cfg_mod['file_cfg_distinte_liste'], $m_params->lista, 'RP');
    $puci_data = find_TA_sys('PUCI', null, '', '', null,null, 0, '', 'Y', 'Y', 'R', false);
    $puci_data[] = array('id' => '', 'text' => '- Vuoto -');
    $lipc_data = find_TA_sys('LIPC', null, '', '', null,null, 0, '', 'Y', 'Y', 'R', false);
    $lipc_data[] = array('id' => '', 'text' => '- Vuoto -');
    $puop_data = find_TA_sys('PUOP', null, '', '', null,null, 0, '', 'Y', 'Y', 'R', false);
    $puop_data[] = array('id' => '', 'text' => '- Vuoto -');
    $mtpa_data = find_TA_sys('MTPA', null, '', '', null,null, 0, '', 'Y', 'Y', 'R', false);
    $mtpa_data[] = array('id' => '', 'text' => '- Vuoto -');
    $amto_data = $main_module->find_TA_std('AMTO', null, 'N', 'N', null, null, null, 'N', 'Y');
    $amto_data[] = array('id' => '', 'text' => '- Vuoto -');
    
    ?>
{"success":true, 

	m_win: {
            	title: <?php echo j("Impostazione selezioni filtro componenti [{$m_params->lista}] {$m_params->d_lista}"); ?>,
            	//maximize: 'Y',
            	width : 700,
            	height : 500,
            	iconCls: 'icon-windows-16'
            },

	"items": [
        {
             xtype: 'form',
            flex: 1,
            
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            layout: {border: false, pack: 'start', align: 'stretch'},
            
            items: [
            
             {
        	xtype: 'tabpanel',
        	items: [
        	   {
            		xtype: 'panel', flex: 1,
            		bodyPadding: '5 5 0',
            		title: 'Codice articolo',
            		defaults: {bodyPadding: '5 5 0'},
            		layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
            		items: [
            		    {
        					xtype: 'fieldset', flex: 1,
        					title: 'Filtri su codice articolo',
        					items: <?php echo_je(array(
        					    _df_IE('IE_RP_ART', $values),
        					    extjs_cod_char_intestazione(15),
        					    _df_cod_char(15, 'Selezione 1', 'RP_ART_01', $values),
        					    _df_cod_char(15, 'Selezione 2', 'RP_ART_02', $values),
        					    _df_cod_char(15, 'Selezione 3', 'RP_ART_03', $values),
        					    _df_cod_char(15, 'Selezione 4', 'RP_ART_04', $values),
        					    _df_cod_char(15, 'Selezione 5', 'RP_ART_05', $values),
        					)) ?>
        				},
        				 {
        					xtype: 'fieldset', flex: 1,
        					title: 'Altri filtri articolo',
        					items: <?php echo_je(array(
        					    _df_cf('Articoli MTO',  'RP_ART_MTO', $values, $amto_data),
        					)) ?>
        				}
        			]
            	},
            	 {
            		xtype: 'panel', flex: 1,
            		bodyPadding: '5 5 0',
            		title: 'Tipo parte',
            		defaults: {bodyPadding: '5 5 0'},
            		layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
            		items: [
            		   {
        					xtype: 'fieldset', flex: 1,
        					defaults: {anchor: '-10'},
        					items: <?php echo_je(array(
        					    _df_IE('IE_RP_PT', $values),
        					    _df_cf('Selezione 1',  'RP_TP_01', $values, $mtpa_data),
        					    _df_cf('Selezione 2',  'RP_TP_02', $values, $mtpa_data),
        					    _df_cf('Selezione 3',  'RP_TP_03', $values, $mtpa_data),
        					    _df_cf('Selezione 4',  'RP_TP_04', $values, $mtpa_data),
        					    _df_cf('Selezione 5',  'RP_TP_05', $values, $mtpa_data),
        					)) ?>
        				}
        			]
            	},
            	{
            	xtype: 'panel', flex: 1,
            		bodyPadding: '5 5 0',
            		defaults: {bodyPadding: '5 5 0'},
            		title: 'Filtri su Operazione',
            		layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
            		items: [
            			
        				{
        					xtype: 'container',
        					layout: 'hbox',
        					items: [
                    			{
                					xtype: 'fieldset', flex: 1,
                					title: 'Filtri su Operazione 1',
                					defaults: {anchor: '-10'},
                					items: <?php echo_je(array(
                					    _df_IE('IE_RP_OPE1', $values),
                					    _df_cf('Selezione 1',  'RP_OPE1_01', $values, $puop_data),
                					    _df_cf('Selezione 2',  'RP_OPE1_02', $values, $puop_data),
                					    _df_cf('Selezione 3',  'RP_OPE1_03', $values, $puop_data),
                					    _df_cf('Selezione 4',  'RP_OPE1_04', $values, $puop_data),
                					    _df_cf('Selezione 5',  'RP_OPE1_05', $values, $puop_data),
                					    _df_cf('Selezione 6',  'RP_OPE1_06', $values, $puop_data),
                					    _df_cf('Selezione 7',  'RP_OPE1_07', $values, $puop_data),
                					    _df_cf('Selezione 8',  'RP_OPE1_08', $values, $puop_data),
                					    _df_cf('Selezione 9',  'RP_OPE1_09', $values, $puop_data),
                					    _df_cf('Selezione 10', 'RP_OPE1_10', $values, $puop_data))) ?>
                				}, {
                					xtype: 'fieldset', flex: 1,
                					title: 'Filtri su Operazione 2',
                					defaults: {anchor: '-10'},
                					items: <?php echo_je(array(
                					    _df_IE('IE_RP_OPE2', $values),
                					    _df_cf('Selezione 1',  'RP_OPE2_01', $values, $puop_data),
                					    _df_cf('Selezione 2',  'RP_OPE2_02', $values, $puop_data),
                					    _df_cf('Selezione 3',  'RP_OPE2_03', $values, $puop_data),
                					    _df_cf('Selezione 4',  'RP_OPE2_04', $values, $puop_data),
                					    _df_cf('Selezione 5',  'RP_OPE2_05', $values, $puop_data),
                					    _df_cf('Selezione 6',  'RP_OPE2_06', $values, $puop_data),
                					    _df_cf('Selezione 7',  'RP_OPE2_07', $values, $puop_data),
                					    _df_cf('Selezione 8',  'RP_OPE2_08', $values, $puop_data),
                					    _df_cf('Selezione 9',  'RP_OPE2_09', $values, $puop_data),
                					    _df_cf('Selezione 10', 'RP_OPE2_10', $values, $puop_data))) ?>
                				}
                			]
                		}
            		]
            	}
        	]}
            	
            ],
            
			buttons: [	
			{
		            text: 'Conferma',
			        iconCls: 'icon-button_blue_play-24', scale: 'medium',		            
		            handler: function() {
		            	var m_win = this.up('window'),
		            		form = this.up('form').getForm(),
		            		form_values = form.getValues();
		            		
    		               if (form.isValid()){
    							Ext.Ajax.request({
     						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_filtri_compo',
     						        method  : 'POST',
     			        			jsonData: {
     			        			   form_values: form.getValues(),
     			        			   lista: <?php echo acs_je($m_params->lista); ?>
     								},
     								
     								success: function(response, opts) {
     									var jsonData = Ext.decode(response.responseText);
     									if (jsonData.success){
     										m_win.destroy();
    						        	 	alert('Salvataggio eseguito');
    						        	 	
    			            		   }
    			            		},
    						        failure    : function(result, request){
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
     								
     						    });
    			            }
		            	
		            }
		        }
	        
	        
	        ]            

}		
]}
<?php 
	exit;
}

