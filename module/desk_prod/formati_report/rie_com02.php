<?php

require_once '_include.php';

/*
 * Lista compoenti collo
 */

class FR_RIE_COM02 extends FR_BASE {
    
    public $main_module;
    public $parametri = array();
    public $template_class = null;
    
    public $forza_paginazione_preview = 'N';    //non forzo la paginazione, la imposto eventualmente in distinta
    public $paginazione_preview_per = array('TDSELO'); 
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
        
        $this->template_class = _pp_load_template('a4_std_01');
    }
    
    
function _css(){
$str = <<<EOF

/* tabella */
div.body table td.um {width: 50px; font-size: 12px;}
div.body table td.qty {width: 50px; font-size: 12px;}
div.body table td.nr {width: 40px;}
div.body table td.art {font-size: 12px;}
div.body table td.des {font-size: 14px; margin-top: 2px}
div.body table td.cl1 {width: 150px; text-align: center;}
div.body table td.cl2 {font-size: 14px;}
div.body table th.des {text-align: right;}
div.body table td.cl1 div.bg_grey {padding: 5px; font-weight: bold;}
div.body table td.cl2 div.i1 {font-size: 25px; font-weight: bold;}

div.art2 {font-size: 12px; text-align: right;}
span.dimt {font-weight: normal; text-align: right; margin-left : 150px}

EOF;
return $str;
}
    
    //in questo caso i record sono i colli
    function html($ar_record, $parametri = array()){
        
        //css da template
        $html .= "<style>" . $this->template_class->_css() . "</style>";
        
        //inserimento css, ToDo: solo per prima richiesta
        $html .= "<style>" . $this->_css() . "</style>";
       
        //contenuto
        $html .=  $this->all($ar_record);
        
        return $html;
    }
    
    function all($ar_record){
        $ret = array();
        $ar_grou_by = array();
        
        
        //ToDo: ordinamento
        
        //separo per ordine
        if ($this->parametri['parametri_formato']['TOT'] == 'ORD'){
            foreach($ar_record as $rec){
                $ar_grou_by[$rec['TDDOCU']][] = $rec;
            }
        } else {
            //separo per lotto
            foreach($ar_record as $rec){
                $ar_grou_by[$rec['TDNRLO']][] = $rec;
            }
        }
        
        $ret[] = "<div class=page>";
        $ret[] = "<div class=header>".$this->template_class->_page_header($rec, $this)."</div>";
        foreach ($ar_grou_by as $ar_colli){
            $rec = $ar_colli[0]; //dal primo prendo i dati di testata (lotto, ...)
            $ret[] = $this->single_html($rec, $ar_colli);
        }
        $ret[] = "</div>";
        
         
        return implode("\n", $ret);
    }
    
    
    function single_html($rec, $ar_colli){
        $html = '';
        
        
        if ($this->parametri['parametri_formato']['TOT'] == 'ORD'){
            $html .= "<div class=sub_header>".$this->template_class->_page_sub_header($rec, $ar_colli)."</div>";
        }
        $html .= "<div class=body>".$this->_page_body($rec, $ar_colli)."</div>";
        return $html;
    }
    
    
    function _page_body($rec, $ar_colli){
        $html = '';
        
        $ar_rrn = array();
        foreach($ar_colli as $collo){ 
            $ar_rrn[] = $collo['RRN_PL'];
        }
   
        //da array dei colli recupero (raggruppato per art) i componenti relativi
        $pp_compo = _pp_get_colli_compo($this->main_module, 
                    $this->parametri['k_lista'],
                    array('RRN_PL' => $ar_rrn), 
                    array('group_by' => 'ART', 'DIM' => $this->parametri['parametri_formato']['DIM'], 'riga' =>'Y'));
        $ar_compo = $pp_compo['ar_compo'];
        
        $html .= "<table>";
        $html .= "<tr class=header>
                  <th>Riga</th>                    
                  <th>UM</th>
                  <th>Q.t&agrave;</th>";
        $html .= "<th>Descrizione/Segnalazioni &nbsp;&nbsp;<span class=dimt>Dimensioni</span></th>";
        $html .= "<th class = des>Descrizione collo <span class=dimt>Dimensioni</span></th>";
        $html .= "</tr>";
        
        foreach($ar_compo as $dr){
        
        	if ($dr['REUM'] != 'NR' && $dr['REUM'] != 'PZ'){
        		$row_nr = $dr['C_ROW'];
        	} else {
        		$row_nr = '&nbsp;';
        	}
        
            $html .= "<tr>
                <td class=um>{$dr['RERIGA']}</td>
                <td class=um>{$dr['REUM']}</td>
                <td class=\"n cc qty\">".n($dr['REQTA'],3)."</td>";
            $dim_compo = n($dr['REDIM1'], 0)." x ".n($dr['REDIM2'], 0)." x ".n($dr['REDIM3'], 0);
            if($this->_note($dr) != "")
                $note = "<br>".$this->_note($dr);
            $html .= "
                 <td class=des>{$dr['REDART']}<br/>
                 <div class=art2>{$dr['REART']} &nbsp;&nbsp;<b>{$dim_compo}</b> </div>
                 {$note}</td>
             ";
            
            $dim_collo = n($dr['PLDIM1'], 0)." x ".n($dr['PLDIM2'], 0)." x ".n($dr['PLDIM3'], 0);
                     
            $html .= "
            <td class=des>{$dr['PLDART']}<br/>
            <div class=art2>{$dr['PLART']} &nbsp;&nbsp;<b>{$dim_collo}</b> </div>
            </td>";
            
            $html .= "</tr>";
        }
        
        $html .= "</table>";
        
        return $html;
    }
    
    function _note($rowPL){
        
        $ar_note = _pp_get_collo_note($this->main_module, $rowPL, 
                        array('RLTPNO'  => 'RD',
                              'RLRIFE2' => $this->parametri['parametri_formato']['NOR'],
                              'solo_testo' => 'Y'));
        
        $text = implode("<br>", $ar_note['ar_note']);
        
        
        if (count($ar_note['ar_note']) > 0){
            $html .= $text;
        } else {
            $html .= "";
        }
        
        return $html;
    }
    
    function _format_ordine($rec){
        
        $numero = sprintf("%06s", $rec['TDONDO']);
        $tipo = trim($rec['TDOTPD']);
        $anno = substr($rec['TDOADO'], 2);
        
        return "<span style=\"font-size: 1.2em;\"><b>{$numero}</b></span> {$tipo}_{$anno}";
        
    }
    
    function _format_carico($value){
        
        $m_value = sprintf("%06s", $value);
        $p1 = substr($m_value, 0, 2);
        $p2 = substr($m_value, 2, 2);
        $p3 = substr($m_value, 4, 2);
        $span_p = "<span style=\"font-size: 1.2em;\"><b>{$p3}</b></span>";
        return "{$p1}.{$p2}/{$span_p}";
        
    }
    
    
}