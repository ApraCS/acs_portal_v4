<?php

class FR_ETI_COL01 {
    
    private $main_module;
    private $parametri = array();
    
    public $forza_paginazione_preview = 'S';
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
    }
    
    //in questo caso i record sono i colli
    function html($ar_record, $parametri = array()){
       
        //inserimento css, ToDo: solo per prima richiesta
        $html = "<style>" . file_get_contents(__DIR__ . '/css/eti_col01.css') . "</style>";
       
        //contenuto
        $html .=  $this->all($ar_record);
        
        return $html;
    }
    
    function all($ar_record){
        $ret = array();
        
        foreach ($ar_record as $rec){
          
            $ret[] = $this->single_html($rec);
        }
        
        return implode("\n", $ret);
    }
    
    
    function single_html($rec){
    
        $html = '';
        $html .= "<div class=etic>
                      <div class=left_bar>" . $this->left_bar($rec) . "</div>
                      <div class=main_bar>" . $this->main($rec) . "</div>
                  </div>";
        return $html;
    
    }
    
    
    function left_bar($rec){
        $html = "";
        $html .= "
            <div class=left_bar_barcode>
                 <span class=id_collo>{$rec['PLBARC']}</span>
                 <img src=" . img_path("barcode.png") . " class=c_barcode>
            </div>
            <div class=left_bar_seq_ordine>
                <span class=title>Sequenza lotto</span>
                <span class=value>{$rec['TDSELO']}</span>
            </div>
            <div class=left_bar_lotto>
                <span class=title>Lotto</span>
                <span class=value>".$this->_format_lotto_carico($rec['TDNRLO'])."</span>
            </div>
            <div class=left_bar_carico>
                <span class=title>Carico</span>
                <span class=value>".$this->_format_lotto_carico($rec['TDNRCA'])."</span>
            </div>
            <div class=left_bar_seq_carico>
                <span class=title>Sequenza carico</span>
                <span class=value>{$rec['TDSECA']}</span>
            </div>
            <div class=left_bar_ordine>
                <span class=title>Ordine</span>
                <span class=value>".$this->_format_ordine($rec)."</span>
            </div>
        ";
        return $html;
    }
    
    function main($rec){
        
        $img = $this->_get_image($rec);
        
        $html = "";
        $html .= "<div class=etic_image><img src=" . img_path($img) . " class=\"center\"></div>";
        $html .= "<div class=footer_bar2>";
        $html .= $this->_t($rec['PLDART']);
        $html .= "<div class=footer_bar>";
       
         $html .= " <div class=footer_bar_l>" . $this->footer_bar($rec) . "</div>
                    <div class=footer_bar_r>" . $this->footer_bar_r($rec) . "</div>
                  </div>";
        return $html;
    }
    
    
    function footer_bar($rec){
        $mto = $this->parametri['parametri_formato']['MTO'];
        if($mto == 'S'){
            $h_v = 30;
            $h_n = 60;
            $h_m = 30;
        }else{
            $h_v = 80;
            $h_n = 30;
        }
        
        $html = "";
        //$html .= $this->_t($rec['PLDART']);
        $html .= $this->_note($rec, $h_n);
        $html .= $this->_tv($rec, $h_v);  //tabella domanda/risposta
      
        
        if ($mto == 'S')
            $html .= $this->_mto($rec, $h_m);  //barcode MTO
    
        $html .= $this->_riferimento('Riferim.', $rec['TDVSRF']);
  
        if(!in_array(trim($rec['TDDNAZ']), array('ITA', 'IT', ''))){
            $nazione = trim($rec['TDDNAZ']);
            $html .= $this->_nazione($nazione);
        }
        
        return $html;
    }
    
    
    function footer_bar_r($rec){
        
        $html = "";
        $html .= "<div class=\"t_dimensioni\">Dimensioni</div>
                  <div class=\"div_table dimensioni\">
                  
                    <div class=\"div_tr\">
                        <div class=\"div_td c\">".n($rec['PLDIM1'], 0)."</div>
                        <div class=\"div_td c\">x ".n($rec['PLDIM2'], 0)."</div>
                        <div class=\"div_td c\">x ".n($rec['PLDIM3'], 0)."</div>
                    </div>
                  </div>

                  <div class=\"div_table\">
                    <div class=div_tr>
                        <div class=\"div_td plart\">{$rec['PLART']}</div>
                        <div class=\"div_td\"></div>
                        <div class=\"div_td r\">{$rec['PLUM']} <b>".n($rec['PLQTA'], 2)."</b></div>
                    </div>
                  </div>
                    
                  <div class=controllo_operatore><b>Controllo operatore</b><br>
                  <div class=logo><img src= '" . $this->_get_image_logo($rec) . "'  height=60px; width=60px;></div>
                  <div class=data_ora>". $this->_data_ora_progetto($rec)."</div>
                  </div>

                  <div class=left_bar_collo>
                  <span class=value> Collo <span style=\"font-size: 35px; font-weight: bold; \">{$rec['PLNCOL']} </span> di <b>{$rec['PLTCOL']}</b></span>
                  </div>
                  
                ";
        return $html;
    }
    
    
    function _t($t){
        return "<div class=pldart>" . trim($t) . "</div>";
    }
    
    
    function _tv($rowPL, $h){
        
        //recupero varianti collo
        $ar_var_da_mostrare = explode(',', $this->parametri['parametri_formato']['VAR']); //Var di riga
        
        
        $html = "";
        $html .= "<div class=tab_varianti>";
        $html .= "<table>";
        
        if (count($ar_var_da_mostrare) > 0){
            $ar = _pp_get_collo_vars($this->main_module, $rowPL, $ar_var_da_mostrare);
            
            foreach($ar as $dr){
               $html .= "<tr><td class=d width = 150>{$dr['D_VAR']}</td><td class=r>{$dr['D_VAL']}</td></tr>";
            }
        }
        
        $html .= "</table>";
        $html .= "</div>";
        return $html;
    }
    
    
    function _mto($rec, $h){
        $barcode = $rec['PLMACQ']; //'1234567890';
        $fornitore = $rec['D_FORN']; //'Artigiana Marmi snc';
        $html = "";
        $html .= "<div class=d_forn>{$fornitore}</div>";
        $html .= "<div class=mto_bar>";
        $html .= "<table style=\"height: {$h}px\">";
        //$html .= "<tr><td class=d_forn width = 100%>{$fornitore}</td></tr>";
        $html .= "<tr><td class=d_mto width = 170> Accettazione MTO<br><span class=barcode>{$barcode}</span></td>
                 <td><img src = '../base/generate_img_barcode.php?barcode={$barcode}'/></td></tr>";
        //$html .= "<tr><td class=barcode> {$barcode}</td></tr>";
        $html .= "</table>";
        $html .= "</div>";
        return $html;
    }
    
    
    function _note($rowPL, $h){
        
        $ar_note = _pp_get_collo_note($this->main_module, $rowPL, 
            array('RLTPNO'  => 'RD',
                  'RLRIFE2' => $this->parametri['parametri_formato']['NOR'],
                  'solo_testo' => 'Y'));
        
        $text = implode("<br><img src=" . img_path("sub_black_next.png") . " height=20px; width=20px;>", $ar_note['ar_note']);
       
        $html = '';
        $html.= "<div class=note style=\"height: {$h}px;\">";
        
        if (count($ar_note['ar_note']) > 0){
            $html .= "<span class=note_text><img src=" . img_path("sub_black_next.png") . " height=20px; width=20px;>{$text}</span>";
        } else {
            $html .= "";
        }
              
                   
        $html.= "</div>";
        return $html;
    }
    
    function _riferimento($title, $v){
       
        $html = "";
        $html .= "<table class=r1>";
            $html .= "<td class=d>{$title}</td><td class=r>{$v}</td>";
        $html .= "</table>";
        return $html;
    }
    
    function _nazione($v){
        
        $html = "";
        $html.= "<div class=nazione>{$v}</div>";
        return $html;
    }
    
    function _format_lotto_carico($value){
       
        $m_value = sprintf("%06s", $value);
        $p = substr($m_value, 0, 4);
        $s = substr($m_value, 4, 2);
        $span_p = "<span style=\"font-size: 0.5em;\">{$p}/</span>";
        return "{$span_p}{$s}";
        
    }
    
    function _format_ordine($rec){
        
        $numero = sprintf("%06s", $rec['TDONDO']);
        $tipo = trim($rec['TDOTPD']);
        $anno = substr($rec['TDOADO'], 2);
        
        return "<span style=\"font-size: 1.3em;\"><b>{$numero}</b></span> {$tipo}_{$anno}";
        
    }
    
    function _data_ora_progetto($rec){
        
        $data_ora = print_date($rec['ASDTAS'])." ".print_ora($rec['ASHMAS']);
        $progetto = implode("-", array($rec['ASORD1'], $rec['ASORD2'], $rec['ASORD3']));
        
        return "[Ver. {$data_ora} {$progetto}]";
        
    }
    
    function _get_image($rec){
        $order_dir          = _pp_order_dir($this->main_module, $rec['TDDOCU']);
        $order_dir_assoluto = _pp_order_dir_assoluto($this->main_module, $rec['TDDOCU']);
        
        $fisso = $this->parametri['parametri_formato']['IMG']; //es: _btop_
        $riga_grafica = trim($rec['PLCINT']);
        
        $ricerca_file = $order_dir_assoluto . '/*' . "{$fisso}{$riga_grafica}.JPG";
        
        $f = null;
        foreach (glob($ricerca_file) as $filename){
            $f = basename($filename);
        }
        
        if (is_null($f)){
            //se immagine non trovata, da formato logo, recupero immagine "No immagine"
            $row_tab_logo = $this->main_module->get_TA_std('LOGOP', $rec['TDAUX4']);
            $logo_path    = trim($row_tab_logo['TALOCA']);
            
            $img = "../personal/desk_prod/loghi/{$logo_path}";
        } else {
            //se immagine trovata
            $img = '../'.$order_dir . '/' . $f;
        }
        
        return $img;
    }
    
    
    function _get_image_logo($rec){
        //esempio: ".ROOT_PATH."personal/desk_prod/loghi/logo_turi_mini.bmp
        
        //il formato logo lo trovo in TDAUX4 (solo in WPI5TD0)
        
        $row_tab_logo = $this->main_module->get_TA_std('LOGOP', $rec['TDAUX4']);
        $logo_path    = trim($row_tab_logo['TAINDI']);
        
        $img = ROOT_PATH."personal/desk_prod/loghi/{$logo_path}";        
        return $img;
    }
    
    
}