<?php

require_once __DIR__ . '/_include.php'; //utility rendering

/*
 * Schede gruppi colli lineari
 */

class FR_SCH_GRU01 extends FR_BASE {
    
    public $main_module;
    public $parametri = array();
    public $template_class = null;
    
    public $forza_paginazione_preview = 'S';
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
        
        $this->template_class = _pp_load_template('a4_sch_01');
    }
    
    
    function _css(){
$str = <<<EOF

div.body {display: flex; flex-flow: column; flex: 1}
div.body div.collo {flex: 1; margin: 0px; display: flex}


div.collo_image {flex: 0 10cm}
div.collo_compo {flex: 1}

div.body table {width: 100%;}
div.body table th {background-color: #c0c0c0; padding: 3px;   font-size: 12px}
div.body table tr.pari{background-color: #DDDDDD;}
div.body table td {padding: 3px; padding-bottom: 3px; font-size: 10px}
div.body table td.um {width: 30px;}
div.body table td.qty {width: 50px;}
div.body table td.nr {width: 40px;}
div.body table td.dim {width: 35px; text-align: right;}

.center2 {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 70%;
  height: 450px;
}

EOF;
        return $str;
    }
    
    
    
    //in questo caso i record sono i colli
    function html($ar_record, $parametri = array()){
        
        //css da template
        $html .= "<style>" . $this->template_class->_css() . "</style>";
        
        //inserimento css, ToDo: solo per prima richiesta
        $html .= "<style>" .$this->_css() . "</style>";
        
        //contenuto
        $html .=  $this->all($ar_record);
        
        return $html;
    }
    
    
    
    function all($ar_record){
        $ret = array();
                
        //ToDo: ordinamento
        
        
        //ToDo: in una pagina vanno stampati due colli
        foreach ($ar_record as $rec){
            $ret[] = $this->single_html($rec, $ar_colli_ordine);
        }
        
        return implode("\n", $ret);
    }
    
    function single_html($rec, $ar_colli){
        $html = '';
        $html .= "<div class=page>
                      <div class=header>".$this->template_class->_page_header($rec, $this)."</div>
                      <div class=body>".$this->_page_body($rec, $ar_colli)."</div>
                  </div>";
        return $html;
    }
    
    function _page_body($rec, $ar_colli){
        $html = "<div class=voce>".$rec['d_voce']."</div>";
        $html.= $this->singolo_collo($rec, 'row1');
        return $html;
    }
    
    
    function singolo_collo($rec, $classe_row){
        $html = "";
        $html.= "<div class=collo_image>".$this->_collo_image($rec)."</div>
                 <div class=collo_compo>".$this->_collo_compo($rec)."</div>
                ";
        return $html;
    }
    
    
    function _collo_compo($rec){
        $html = '';
        $html .= "<table>";
        
        $pp_compo = _pp_get_collo_compo($this->main_module,
            $this->parametri['k_lista'],
            $rec, array('group_by' => 'ART'));
        $ar = $pp_compo['ar_compo'];
        
        $html .= "<tr class=header>        	
        		   <th>UM</th>
        		   <th>Q.t&agrave;</th>
        		   <th>NR</th>
        		   <th>Descrizione</th>
                   <th colspan = 3>D i m e n s i o n i</th>
                    <th>Articolo</th></tr>";
        
        $nr = 0;
        foreach($ar as $dr){
            $nr++;
            if($nr % 2 == 0)
               $html .= "<tr class = pari>";
            else $html .= "<tr>";
                
        
        	if ($dr['REUM'] != 'NR' && $dr['REUM'] != 'PZ'){
        		$row_nr = $dr['C_ROW'];
        	} else {
        		$row_nr = '&nbsp;';
        	}
        
            $html .= "
                <td class=um>{$dr['REUM']}</td>
                <td class=\"n cc qty\">".n($dr['REQTA'], 3)."</td>
                <td class=\"n cc nr\">{$row_nr}</td>
                <td class=\"desc cc\">{$dr['REDART']}</td>
                 <td class=dim>".n($dr['REDIM1'], 0)."</td>
                 <td class=dim>".n($dr['REDIM2'], 0)."</td>
                 <td class=dim>".n($dr['REDIM3'], 0)." &nbsp;</td>
                <td class=r>{$dr['REART']}</td>
                </tr>";
            
          
        }
        
        $html .= "</table>";
        
        return $html;
        
    }
    
    
    function _collo_image($rec){
        $img = $this->_get_image($rec, true);
        
        $html = "";
        $html .= "<img src=" . img_path($img) . " class=\"center2\">";
        return $html;
    }
    
}