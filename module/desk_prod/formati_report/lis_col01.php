<?php

require_once __DIR__ . '/_include.php'; //utility rendering

/*
 * Lista compoenti collo
 */

class FR_LIS_COL01 {
    
    private $main_module;
    private $parametri = array();
    private $template_class = null;
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
        
        $this->template_class = _pp_load_template('a4_std_01');
    }
    
    function _css(){
        $str = <<<EOF

/* tabella */
div.body table td.um {width: 50px;}
div.body table td.qty {width: 50px;}
div.body table td.art {width: 100px; font-size: 8px;}
div.body table td.riga {width: 70px; text-align: right;}

div.body table td.cl1 div.bg_grey {padding: 5px; font-weight: bold;}
div.body table td.cl2 div.i1 {font-size: 25px; font-weight: bold;}

span.art2 {font-size: 10px;}

EOF;
        return $str;
    }
    
    
    //in questo caso i record sono i colli
    function html($ar_record, $parametri = array()){
        
        //css da template
        $html .= "<style>" . $this->template_class->_css() . "</style>";
        
        //inserimento css, ToDo: solo per prima richiesta
        $html .= "<style>" .$this->_css() . "</style>";
       
        //contenuto
        $html .=  $this->all($ar_record);
        
        return $html;
    }
    
    function all($ar_record){
        $ret = array();
        
        
        //ToDo: ordinamento
        
        //separo per ordine
        //ToDo: raggruppamento per ordine opzionale
        $ar_grou_by_ord = array();
        foreach($ar_record as $rec){
            $ar_grou_by_ord[$rec['TDDOCU']][] = $rec;
        }
        
        foreach ($ar_grou_by_ord as $ar_colli_ordine){
            $rec = $ar_colli_ordine[0]; //dal primo prendo i dati di testata (lotto, ...)
            $ret[] = $this->single_html($rec, $ar_colli_ordine);
        }
        
         
        return implode("\n", $ret);
    }
    
    
    function single_html($rec, $ar_colli){
        $html = '';
        $html .= "<div class=page>
                      <div class=header>".$this->template_class->_page_header($rec)."</div>
                      <div class=sub_header>".$this->template_class->_page_sub_header($rec, $ar_colli)."</div>
                      <div class=body>".$this->_page_body($rec, $ar_colli)."</div>
                  </div>";
        return $html;
    }
    
    
    function _page_body($rec, $ar_colli){
        $html = '';
        
        
        $html .= "<table>";
        $html .= "<tr class=header>
                    <th>UM</th>
                    <th>Q.t&agrave;</th>
                    <th>Articolo</th>
                    <th>Descrizione / Segnalazioni</th>
                    <th style=\"text-align: right\">Riga</th>
                  </tr>";
        
        foreach($ar_colli as $dr){
            $html .= "<tr>
                <td class=um>{$dr['PLUM']}</td>
                <td class=\"n cc qty\">{$dr['PLQTA']}</td>
                <td class=art>{$dr['PLART']}</td>
                <td class=des>{$dr['PLDART']}<br/><span class=art2>PR-MM.150 ....</span></td>
                <td class=riga>170</td>
                </tr>";
        }
        
        $html .= "</table>";
        
        return $html;
    }
    
    
}