<?php

//load della classe template
function _pp_load_template($template_name){
    require_once("templates/{$template_name}.php");
    $report_procedura_class_name = "FR_TPL_" . strtoupper($template_name);
    $r = new $report_procedura_class_name();
    return $r;
}