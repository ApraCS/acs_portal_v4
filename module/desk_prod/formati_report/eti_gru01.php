<?php

class FR_ETI_GRU01 {
    
    private $main_module;
    private $parametri = array();
    
    public $forza_paginazione_preview = 'S';
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
    }
    
    //in questo caso i record sono i colli
    function html($ar_record, $parametri = array()){
        
        //inserimento css, ToDo: solo per prima richiesta
        $html = "<style>" . file_get_contents(__DIR__ . '/css/eti_gru01.css') . "</style>";
       
        //contenuto
        $html .=  $this->all($ar_record);
        
        return $html;
    }
    
    function all($ar_record){
        $ret = array();
        foreach ($ar_record as $rec){
            $ret[] = $this->single_html($rec);
        }
        return implode("\n", $ret);
    }
    
    
    function single_html($rec){
        $html = '';
        $html .= "<div class=etic>
                      <div class=header>
                        <div class=h_left>
                        <span class=id_collo>{$rec['PLBARC']}</span>
                       <img src=" . img_path("barcode.png") . " height=170px; width=100px;>
                        </div>

                        <div class=h_main>
                            <div class=h_collo>{$rec['PLDART']}</div>
                            <div class=h_info_1>".$this->_h_info_1($rec)."</div>
                            <div class=h_info_2>".$this->_h_info_2($rec)."</div>
                        </div>
                      </div>
                      <div class=h_info_3>".$this->_h_info_3($rec)."</div>

                      <div class=body>".$this->_body($rec)."</div>
                      <div class=footer>
                      <div class=controllo_operatore>
                       <div class=logo>Controllo operatore<BR> ". $this->_data_ora_progetto($rec)."
                       <img src= '" . $this->_get_image_logo($rec) . "'  height=60px; width=60px;></div>
                       
                      </div></div>
                      </div>
                  </div>";
        return $html;
    
    }
    
    
    
    function _h_info_1($rec){
        $html = '';
        $html .= "<div class=tlab>Ordine</div>";
        $html .= "<div class=t1>".$this->_format_ordine($rec)."</div>";
        $html .= "<div class=tlab>Riferimento</div>";
        $html .= "<div class=t1>{$rec['TDVSRF']}</div>";
        return $html;
    }
       
    function _h_info_2($rec){
        $html = '';
        $html .= "<div class=tlab>Sequenza produzione</div>";
        $html .= "<div class=t_seq_prod>{$rec['TDSELO']}</div>";
        $html .= "<div class=lotto>Lotto <span class=value>".$this->_format_lotto($rec['TDNRLO'])."</span></div>";
        $html .= "<div class=carico>
                    <div class=tlab>Carico</div>
                    <div class=carico_1><span class=value>".$this->_format_carico($rec['TDNRCA'])."</span></div>
                  
                  </div>";
        return $html;
    }
    
    
    function _h_info_3($rec){
        if(!in_array(trim($rec['TDDNAZ']), array('ITA', 'IT', '')))
            $nazione = trim($rec['TDDNAZ']);
        
        $html = '';
        $html .= "
                  <span class=nazione><br>{$nazione}</span>
                  <div style=\"flex: 1 auto;\">&nbsp;</div>
                  <div class=seq_carico>
                     <span class=title> Sequenza carico<br> </span>
                     <span class=value>{$rec['TDSECA']}</span>
                  </div>
                  <div class=collo>
                    <span class=value> Collo <span style=\"font-size: 30px; font-weight: bold; \">{$rec['PLNCOL']} </span> di <b>{$rec['PLTCOL']}</b></span>
                  </div>
                 ";
        return $html;
    }
    
    
    function _body($rec){
        $html = '';
        $html .= "<table>";
        
        
        $row_formato = $this->main_module->get_TA_std('CDFMU', $this->parametri['rowCD']['CDFOUS']);
        
        /*$ar = array(
            array('um' => 'PZ', 'qty' => 1, 'desc' => 'BARRA REGGIPENSILE'),
            array('um' => 'PZ', 'qty' => 2, 'desc' => 'BARRA REGGIPENSILE 2'),
            array('um' => 'PZ', 'qty' => 3, 'desc' => 'BARRA REGGIPENSILE 3')
        );*/
        
        //da array dei colli recupero (raggruppato per art) i componenti relativi
        $pp_compo = _pp_get_collo_compo($this->main_module,
            $this->parametri['k_lista'],
            $rec,
            array('group_by' => 'ART', 'DIM' => $this->parametri['parametri_formato']['DIM'], 'parametri_formato' => _pp_parametri_formato($row_formato['TARIF1'])));
        $ar = $pp_compo['ar_compo'];
        
        //$html .="<tr><td colspan=3>". '<pre>SQL: ' . $pp_compo['sql'] . '</pre>';
        //$html .= '<pre>SQL params: ' . print_r($pp_compo['sql_params'], true) . '</pre>';
        //$html .="</td></tr>";
        
        $html .= "<tr class=header>
                  <th>UM</th>
                  <th>Q.t&agrave;</th>";
        if ($this->parametri['parametri_formato']['DIM'] != 'N') {
            $html .= "<th>NR</th>";
        }
        $html .= "<th>Articolo</th>";
        if ($this->parametri['parametri_formato']['DIM'] != 'N'){
            $html .= "<th colspan = 3 class = dim>Dimensioni</th>";
        }

        
        foreach($ar as $dr){
            
            if ($dr['REUM'] != 'NR' && $dr['REUM'] != 'PZ'){
                $row_nr = $dr['C_ROW'];
            } else {
                $row_nr = '&nbsp;';
            }
            
            $html .= "<tr>
                <td class=um>{$dr['REUM']}</td>
                <td class=\"n cc qty\">".n($dr['REQTA'], 3)."</td>";
            if ($this->parametri['parametri_formato']['DIM'] != 'N') {
                $html .= "<td class=\"n cc nr\">{$row_nr}</td>";
            }
            $html .= "<td class=\"desc cc\">".trim($dr['REDART'])." [".trim($dr['REART'])."]</td>";
            if ($this->parametri['parametri_formato']['DIM'] != 'N'){
                $html .= "<td class = dim>".n($dr['REDIM1'], 0)."</td>
                          <td class = dim>".n($dr['REDIM2'], 0)."</td>
                          <td class = dim>".n($dr['REDIM3'], 0)."</td>";
            }
               echo "</tr>";
        }
        
        $html .= "</table>";
        
        return $html;
    }
    
    function _format_carico($value){
        
        $m_value = sprintf("%06s", $value);
        $p = substr($m_value, 0, 4);
        $s = substr($m_value, 4, 2);
        $span_p = "<span style=\"font-size: 0.5em;\">{$p}/</span>";
        return "{$span_p}{$s}";
        
    }
    
    
    function _format_lotto($value){
        
        $m_value = sprintf("%06s", $value);
        $p1 = substr($m_value, 0, 2);
        $p2 = substr($m_value, 2, 2);
        $p3 = substr($m_value, 4, 2);
        $span_p2 = "<span style=\"font-size: 1.5em;\">{$p2}</span>";
        return "{$p1}<b>.{$span_p2}.{$p3}</b>";
        
    }
    
    
    
    function _format_ordine($rec){
        
        $numero = sprintf("%06s", $rec['TDONDO']);
        $tipo = trim($rec['TDOTPD']);
        $anno = substr($rec['TDOADO'], 2);
        
        return "<span style=\"font-size: 1.3em;\"><b>{$numero}</b></span> {$tipo}_{$anno}";
        
    }
    
    function _data_ora_progetto($rec){
    
        $data_ora = print_date($rec['ASDTAS'])." ".print_ora($rec['ASHMAS']);
        $progetto = implode("-", array($rec['ASORD1'], $rec['ASORD2'], $rec['ASORD3']));
        
        return "[Ver. {$data_ora} {$progetto}]";
        
    }
    
    function _get_image_logo($rec){
        //esempio: ".ROOT_PATH."personal/desk_prod/loghi/logo_turi_mini.bmp
        
        //il formato logo lo trovo in TDAUX4 (solo in WPI5TD0)
        
        $row_tab_logo = $this->main_module->get_TA_std('LOGOP', $rec['TDAUX4']);
        $logo_path    = trim($row_tab_logo['TAINDI']);
        
        $img = ROOT_PATH."personal/desk_prod/loghi/{$logo_path}";
        return $img;
    }
    
    
}