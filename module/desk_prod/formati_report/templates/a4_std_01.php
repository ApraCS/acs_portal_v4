<?php

/*
 * Lista compoenti collo
 */

class FR_TPL_A4_STD_01 {
    
function _css(){
$str = <<<EOF

@page {
  size: A4;
  margin: 0;
}

div.page {
    /* border: 1px solid black; */
    width: 1024px; 
    padding: 5px; 
    padding-bottom: 100px; 
    margin-bottom: 50px;}

div.page div.header{
	height: 130px;
	display: flex;  
  	flex-flow: row wrap;
}

div.header div.logo {
	flex: 0 200px;
}
div.header div.header_main {
	flex: 1;
	display: flex;  
  	flex-flow: row wrap;
}
div.header div.right_bar {
	flex: 0 200px;
}

/* header main */
div.report_title {flex: 1 100%; font-size: 30px; height: 50px;}
div.lotto {flex: 1 auto; height: 50px;}
div.lotto span.numero{font-size: 26px;}
div.page_num {flex: 1 auto; height: 50px; font-weight: bold;}
div.barcode {height: 30px;}
div.emissione {text-align: center; margin-left :35px; width : 200px; background-color: #c0c0c0; margin-top: 15px; padding: 5px; font-size: 10px; }

/* logo */
div.logo img.page_logo {width: 90}


/* sub header */
div.sub_header {display: flex; flex-flow: row wrap; border-top: 1px dashed grey; padding-top: 5px;}
div.sub_header div.carico {flex: 0 60px; height: 40px;}
div.carico span.num_car{font-weight: bold; font-size: 16px;}
div.sub_header div.ordine {flex: 0 150px; height: 40px;}
div.sub_header div.sequenza {flex: 0 150px; height: 40px;}
div.sequenza span.seq{font-size: 30px; font-weight: bold;}
div.sub_header div.cliente {flex: 0 350px; height: 40px;}
div.sub_header div.riferimento {flex: 1; height: 40px;}

/* tabella */
div.body table {width: 100%;}
div.body table tr {border-bottom: 1px dashed grey;}
div.body table th {background-color: #c0c0c0; padding: 5px; text-align: left; font-weight: bold;}
div.body table td {padding: 3px; padding-bottom: 10px;}

/* generiche */
.bg_grey {background-color: #c0c0c0}

table tr.sub1 td {background-color: #d6d6d6; font-size: 0.8em}

EOF;
return $str;
}
    
    
    function _page_header($rec, $from_module){
        $html = '';
        $html.= "
            <div class=logo><img class=page_logo src='" . $from_module->_get_image_logo_by_indice($rec, $from_module) . "'/></div>
            <div class=header_main>".$this->_header_main($rec)."</div> 
           <div class=right_bar>".$this->_header_right_bar($rec)."</div>
        ";
        return $html;
    }
    
    function _header_main($rec){
        global $m_params;
        
        $html = '';
        $html.= "
            <div class=report_title>{$m_params->open_request->d_voce}</div>
            <div class=lotto>Lotto <span class=numero>".$this->_format_lotto($rec['TDNRLO'])."</span></div>
            <div class=page_num><!-- Pagina 1 di 2 --></div>
        ";
        return $html;
    }
    
    
    function _header_right_bar($rec){
        $barcode = $rec['PLBARC'];
        $html = '';
        $html.= "
             <div class=barcode><img src = '../base/generate_img_barcode.php?barcode={$barcode}' height=30 width=270></div>
            <div class=emissione>". $this->_data_ora_progetto($rec)."</div>
        ";
        return $html;
    }
    
    
    /* Intestazione lotto/ordine... */
    function _page_sub_header($rec, $ar_colli){
        
        $m_value = sprintf("%06s", $rec['TDNRCA']);
        $p3 = substr($m_value, 4, 2);
        
        $html = '';
        $html.= "
            <div class=carico>Carico<br>".$this->_format_carico($rec['TDNRCA'])."</div>
            <div class=carico><span style=\"font-size: 2.5em;\"><b>{$p3}</b></span></div>
            <div class=ordine>Ordine<br>".$this->_format_ordine($rec)."</div>
            <div class=sequenza>Seq.<span class=seq>{$rec['TDSELO']}</span></div>
            <div class=cliente>Cliente [{$rec['TDCCON']}] <br><b>{$rec['TDDCON']}</b></div>
            <div class=riferimento>Riferimento<br><b>{$rec['TDVSRF']}</b></div>
        ";
        return $html;
    }
    
    function _format_carico($value){
        
        $m_value = sprintf("%06s", $value);
        $p1 = substr($m_value, 0, 2);
        $p2 = substr($m_value, 2, 2);
        $p3 = substr($m_value, 4, 2);
        //$span_p = "<span style=\"font-size: 2.5em;\"><b>{$p3}</b></span>";
        return "<span class = num_car>{$p1}.{$p2}/{$span_p}</span>";
        
    }
    
    function _format_ordine($rec){
        
        $numero = sprintf("%06s", $rec['TDONDO']);
        $tipo = trim($rec['TDOTPD']);
        $anno = substr($rec['TDOADO'], 2);
        
        return "<span style=\"font-size: 1.4em;\"><b>{$numero}</b></span> {$tipo}_{$anno}";
        
    }
    
    function _format_lotto($value){
        
        $m_value = sprintf("%06s", $value);
        $p1 = substr($m_value, 0, 2);
        $p2 = substr($m_value, 2, 2);
        $p3 = substr($m_value, 4, 2);
        $span_p2 = "<span style=\"font-size: 1.5em;\">{$p2}</span>";
        return "{$p1}<b>.{$span_p2}.{$p3}</b>";
        
    }
    
    function _data_ora_progetto($rec){
        
        $data_ora = print_date($rec['ASDTAS'])." ".print_ora($rec['ASHMAS']);
        $progetto = implode("-", array($rec['ASORD1'], $rec['ASORD2'], $rec['ASORD3']));
        
        return "[Ver. {$data_ora} {$progetto}]";
        
    }
    
}