<?php

/*
 * A4, orizzontale
 * Header a Sx
 */

class FR_TPL_A4_SCH_01 extends FR_BASE {
    
function _css(){
$str = <<<EOF
div.page {
    border: 1px dashed grey;
    width: 277mm; height: 190mm; 
    margin-bottom: 50px;
    display: flex; flex-flow: row}

div.page div.header{
	flex: 0 6cm;
	display: flex; flex-flow: column wrap;
    border-right: 1px dashed grey;
}

div.logo {flex: 1}
div.voce {font-size: 18px; text-align: center;}
div.seq_lotto {flex: 1; /*font-size: 10px*/}
div.vars {flex: 1 auto}
div.label {font-size: 10px}
div.note span.note_text {font-size: 14px; margin-top: 0px;}
div.seq_lotto div.seq_lotto1 {font-size: 26pt; font-weight: bold; letter-spacing: 2px;}

div.barcode {flex: 0 1cm}

div.carico {flex: 1; display: flex;}
div.carico div.carico2 {flex: 1 auto; font-size: 24pt}

div.ordine {flex: 1}
div.ordine1 {font-size: 18px}

div.seq_carico_lotto {display: flex; padding: 5px}
div.seq_carico_lotto div.seq_carico {flex: 1; display: flex;}
div.seq_carico_lotto div.lotto {flex: 1}

div.carico1 span.value {text-align: center; display: block; font-weight: bold; font-size: 25px;}
div.lotto1 span.value {text-align: center; display: block; font-weight: bold; font-size: 25px;}
div.data_ora {font-size: 8px; margin-bottom:5px; text-align: center;}


div.seq_carico_lotto div.seq_carico div.label {flex: 1}
div.seq_carico_lotto div.seq_carico div.seq_carico {flex: 1; font-size: 26pt; font-weight: bold}

div.seq_carico_lotto div.lotto {display: flex; color: white}
div.seq_carico_lotto div.lotto div.lotto2 {font-size: 24pt; font-weight: bold}

div.cliente {padding-top: 5px; padding-bottom: 5px}
div.cliente div.rag_soc {font-weight: bold}

div.riferimento {padding: 5px}

table.vars td.d {font-size: 10pt; width: 2cm; padding-bottom: 3px}
table.vars td.r {font-size: 10pt; font-weight: bold; padding-bottom: 3px}

div.assonometria img {width: 6cm; margin-bottom: 10px}


/* generiche */
.bg_grey {background-color: #c0c0c0}
div.row {display: flex; flex-flow: row}

EOF;
return $str;
}
    
    
    function _page_header($rec, $from_module){
        $html = '';
        $html.= "
            <div class=row>
                <div class=logo><img src= '" . $this->_get_image_logo($rec, $from_module->main_module) . "'  height=60px; width=60px;></div>
                <div class=seq_lotto>".$this->div_seq_lotto($rec)."</div>
            </div>
            <div class=barcode><img src = '../base/generate_img_barcode.php?barcode={$rec['PLBARC']}'/></div>
            <div class=row>
                <div class=carico>".$this->div_carico($rec)."</div>
                <div class=ordine>".$this->div_ordine($rec)."</div>
            </div>
            <div class=\"row seq_carico_lotto bg_grey\">
                <div class=seq_carico>".$this->div_seq_carico($rec)."</div>
                <div class=lotto>".$this->div_lotto($rec)."</div>
            </div>
            <div class=cliente>".$this->div_cliente($rec)."</div>
            <div class=\"riferimento bg_grey\">".$this->div_riferimento($rec)."</div>
            <div class=vars>".$this->div_vars($rec, $from_module)."</div>
            <div class=assonometria>".$this->div_assonometria($rec, $from_module)."</div>
            <div class=data_ora>". $this->_data_ora_progetto($rec)."</div>
        ";
        return $html;
    }
    

    function div_seq_lotto($rec){
        $html = '';
        $html.= "<div class=label>Sequenza lotto</div>
                 <div class=seq_lotto1>{$rec['TDSELO']}</div>
                 ";
        return $html;
    }
    
    
    function div_carico($rec){
        $html = '';
        $html.= "<div class=carico1>   
                    <div class=label>Carico</div>
                    <span class=value>".$this->_format_lotto_carico($rec['TDNRCA'])."</span>
                 </div>
                 
                ";
        return $html;
    }
    
    function div_ordine($rec){
        
        $numero = sprintf("%06s", $rec['TDONDO']);
        $tipo = trim($rec['TDOTPD']);
        $anno = substr($rec['TDOADO'], 2);
        
        $html = '';
        $html.= "<div class=ordine1>
                    <div class=label>Ordine</div>
                    <div class=ordine1><b>{$numero}</b> <span style=\"font-size: 0.8em;\">{$tipo} {$anno}</span></div>
                 </div>
                ";
        return $html;
    }
    
    
    function div_seq_carico($rec){
        $html = '';
        $html.= "   <div class=label>Sequenza<br>Carico</div>
                    <div class=seq_carico>{$rec['TDSECA']}</div>
                ";
        return $html;
    }
    
    
    function div_lotto($rec){
        $html = '';
        $html.= "<div class=lotto1>
                    <div class=label>Lotto</div>
                    <span class=value>".$this->_format_lotto_carico($rec['TDNRLO'])."</span>
                 </div>
               
                ";
        return $html;
    }
    
    
    function div_cliente($rec){
        $html = '';
        $html.= "
                    <div class=label>Cliente [{$rec['TDCCON']}]</div>
                    <div class=rag_soc>{$rec['TDDCON']}</div>
                 
                ";
        return $html;
    }
    
    
    function div_riferimento($rec){
        $html = '';
        $html.= "Rif: <b>{$rec['TDVSRF']}</b>";
        return $html;
    }
    
    
    function div_vars($rec, $from_module){
        $html = '';
        //recupero varianti collo
        $ar_var_da_mostrare = explode(',', $from_module->parametri['parametri_formato']['VAR']); //Var di riga
        
        $html = "";
        $html .= "<table class=vars>";
        
        if (count($ar_var_da_mostrare) > 0){
            $ar = _pp_get_collo_vars($from_module->main_module, $rec, $ar_var_da_mostrare);
            
            /*$ar = array(
                array('D_VAR' => 'Modello', 'D_VAL' => 'KKKOORNA ABC'),
                array('D_VAR' => 'Top', 'D_VAL' => 'FANGO/VISONE CEMENTO'),
            );*/
            
            foreach($ar as $dr){
                $html .= "<tr><td class=d width = 150>{$dr['D_VAR']}</td><td class=r>{$dr['D_VAL']}</td></tr>";
            }
        }
        
        $html .= "</table>";
        return $html;
        
    }
    
    
    function div_assonometria($rec, $from_module){
        $img = $this->_get_image_assonometria($rec, $from_module);
        $html = '';
        $html.= "<img src=" . img_path($img) . " class=\"center\">";
        return $html;        
    }
    
    
    
    
    
    
    
    
    function _header_main($rec){
        $html = '';
        $html.= "
            <div class=report_title>TURI - Azzariti - Lista Top Speciali</div>
            <div class=lotto>Lotto <span class=lotto>{$rec['PLART']}
                <span class=numero>03.10.00</span></div>
            <div class=page_num>Pagina 1 di 2</div>
        ";
        return $html;
    }
    
    
    function _header_right_bar($rec){
        $barcode = "1234567890";
        $html = '';
        $html.= "
            <div class=barcode><img src = '../base/generate_img_barcode.php?barcode={$barcode}' height=30 width=190></div>
            <div class=emissione>Emissione 11/05/20 - 10:26</div>
        ";
        return $html;
    }
    
    
    /* Intestazione lotto/ordine... */
    function _page_sub_header($rec, $ar_colli){
        $html = '';
        $html.= "
            <div class=carico>Carico</div>
            <div class=ordine>{$rec['TDDOCU']}</div>
            <div class=sequenza>Seq 001</div>
            <div class=cliente>ARREDAMENTI GIGLIOLI & FIGLI</div>
            <div class=riferimento>OFF GIANNOTTA ANTONIO</div>
        ";
        return $html;
    }
    
    function _format_lotto_carico($value){
        
        $m_value = sprintf("%06s", $value);
        $p = substr($m_value, 0, 4);
        $s = substr($m_value, 4, 2);
        $span_p = "<span style=\"font-size: 0.5em;\">{$p}/</span>";
        return "{$span_p}{$s}";
        
    }
    
    function _data_ora_progetto($rec){
        
        $data_ora = print_date($rec['ASDTAS'])." ".print_ora($rec['ASHMAS']);
        $progetto = implode("-", array($rec['ASORD1'], $rec['ASORD2'], $rec['ASORD3']));
        
        return "[Ver. {$data_ora} {$progetto}]";
        
    }
    
}