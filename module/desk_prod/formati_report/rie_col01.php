<?php

/*
 * Riepilogo colli
 */

class FR_RIE_COL01 extends FR_BASE {
    
    public $main_module;
    public $parametri = array();
    public $template_class = null;
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
        
        $this->template_class = _pp_load_template('a4_std_01');
    }
    
    
    function _css(){
        $str = <<<EOF

/* tabella */
div.page{border: none}
div.body table td.um {width: 50px;}
div.body table td.qty {width: 50px;}
div.body table td.art {width: 100px; font-size: 12px;}
div.body table td.cl1 {width: 170px; text-align: center;}
div.body table td.cl2 {width: 75px; font-size: 14px; text-align: right;}

div.body table td.cl1 div.bg_grey {padding: 5px; font-weight: bold;}
div.body table td.cl2 div.i1 {font-size: 25px; font-weight: bold;}
span.art2 {font-size: 12px; font-weight: bold;}

EOF;
        return $str;
    }
    
    
    //in questo caso i record sono i colli
    function html($ar_record, $parametri = array()){
        
        //css da template
        $html .= "<style>" . $this->template_class->_css() . "</style>";
        
        //inserimento css, ToDo: solo per prima richiesta
        $html .= "<style>" . $this->_css() . "</style>";
       
        //contenuto
        $html .=  $this->all($ar_record);
        
        return $html;
    }
    
    function all($ar_record){
        $ret = array();
        
        $rec = $ar_record[0]; //dal primo prendo i dati di testata (lotto, ...)
        
        //ToDo: devo ordinare per sequenza ordine produzione / articolo
        $ret[] = $this->single_html($rec, $ar_record);
        
        return implode("\n", $ret);
    }
    
    
    function single_html($rec, $ar_colli){
        $html = '';
        $html .= "<div class=page>
                      <div class=header>".$this->template_class->_page_header($rec, $this)."</div>
                      <div class=body>".$this->_page_body($rec, $ar_colli)."</div>
                  </div>";
        return $html;
    }
    
    
    function _page_body($rec, $ar_colli){
        $variabile = $this->parametri['parametri_formato']['ARV'];
        if(strlen($variabile) > 0){
            $th_var = " <th>{$variabile}</th>";
        }else{
            $th_var = "";
        }
        $html = '';
        
        //se il primo ordinamento e' per articolo, allora aggiungo anche il subtotale (per articolo)
        $ar_tipo_ord = explode('/', $this->parametri['parametri_formato']['ORD']);
        if (count($ar_tipo_ord) > 0 && $ar_tipo_ord[0] == 'AR')
            $subtotale_articolo = true;
        else
            $subtotale_articolo = false;
        
            
        $row_inline = false;
        $sub_qta = 0;
            
        $html .= "<table>";
        $html .= "<tr class=header>
                    <th>UM</th>
                    <th>Q.t&agrave;</th>
                    <th>Articolo</th>
                    {$th_var}
                    <th>Descrizione / Segnalazioni</th>
                    <th>Ordine/Dim.</th>
                    <th>Seq./Car.</th>
                  </tr>";
        
        foreach($ar_colli as $dr){
            
            if ($subtotale_articolo){
                 if ($row_inline != false && trim($row_inline['PLART']) != trim($dr['PLART'])){
                    //cambio articolo
                    $html .= $this->_get_sub_art($row_inline, $sub_qta, false);
                    $sub_qta = 0;
                }
                $row_inline = $dr;
                $sub_qta += $dr['PLQTA'];
            }
            
            $ar_p_var = array($variabile); 
            $ar = _pp_get_collo_vars($this->main_module, $dr, $ar_p_var);
            $var = '';
            foreach($ar as $vr){
                $var .= $vr['VAL']." ";
            }
            
            if(strlen($variabile) > 0){
               $td_van = "<td class=art>{$var}</td>";
            }else{
               $td_van = "";
            }
                
            
            
            $html .= "<tr>
                <td class=um>{$dr['PLUM']}</td>
                <td class=\"n cc qty\">".n($dr['PLQTA'], 3)."</td>
                <td class=art>{$dr['PLART']}</td>
                {$td_van}
                <td class=des>{$dr['PLDART']}<br/>
                <span class=art2>".$this->_note($dr)."</span></td>
                <td class=cl1>
                    <div>".$this->_format_ordine($dr)."</div>
                    <div class=bg_grey>".n($dr['PLDIM1'], 0)." x ".n($dr['PLDIM2'], 0)." x ".n($dr['PLDIM3'], 0)."</div>
                </td>
                <td class=cl2>
                <div class=i1>{$dr['TDSELO']}</div>
                <div>".$this->_format_carico($dr['TDNRCA'])."</div>
                </td>
                </tr>";
        }
        
       // $html .= $this->_get_sub_art($dr, $sub_qta, false);
        
        $html .= "</table>";
        
        return $html;
    }
    
    

    function _get_sub_art($row, $qta, $after_space = true){
        $html = '';
        $html .= "<tr class=sub1>
                            <td class=um><b>{$row['PLUM']}</b></td>
                            <td class=\"n cc qty\"><b>".n($qta, 3)."</b></td>
                            <td class=art>{$row['PLART']}</td>
                            <td class=des>[Totale articolo]</td>
                            <td colspan=2>&nbsp;</td>
                            </tr>";
        if ($after_space)
        $html .= "<tr class=space>
                            <td colspan=6>&nbsp;</td>
                            </tr>";
        return $html;
    }
    
    function _note($rowPL){
        
        $ar_note = _pp_get_collo_note($this->main_module, $rowPL, 
            array('RLTPNO'  => 'RD',
                  'RLRIFE2' => $this->parametri['parametri_formato']['NOR'],
                  'solo_testo' => 'Y'));
        
        $text = implode("<br>", $ar_note['ar_note']);
       
        
        if (count($ar_note['ar_note']) > 0){
            $html .= $text;
        } else {
            $html .= "";
        }
        
        return $html;
    }
    
    function _format_ordine($rec){
        
        $numero = sprintf("%06s", $rec['TDONDO']);
        $tipo = trim($rec['TDOTPD']);
        $anno = substr($rec['TDOADO'], 2);
        
        return "<span style=\"font-size: 1.2em;\">{$numero}</span> {$tipo}_{$anno}";
        
    }
    
    function _format_carico($value){
        
        $m_value = sprintf("%06s", $value);
        $p1 = substr($m_value, 0, 2);
        $p2 = substr($m_value, 2, 2);
        $p3 = substr($m_value, 4, 2);
        $span_p = "<span style=\"font-size: 1.2em;\"><b>{$p3}</b></span>";
        return "{$p1}.{$p2}/{$span_p}";
        
    }

    
}