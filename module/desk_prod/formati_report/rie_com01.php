<?php

require_once '_include.php';

/*
 * Lista compoenti collo
 */

class FR_RIE_COM01 extends FR_BASE {
    
    public $main_module;
    public $parametri = array();
    public $template_class = null;
    
    public $forza_paginazione_preview = 'N';    //non forzo la paginazione, la imposto eventualmente in distinta
    public $paginazione_preview_per = array('TDSELO'); 
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
        
        $this->template_class = _pp_load_template('a4_std_01');
    }
    
    
function _css(){
$str = <<<EOF

/* tabella */
div.body table td.um {width: 50px;}
div.body table td.qty {width: 50px;}
div.body table td.nr {width: 40px;}
div.body table td.art {width: 100px; font-size: 12px;}
div.body table td.cl1 {width: 150px; text-align: center;}
div.body table td.cl2 {width: 45px; font-size: 14px; text-align: right;}
div.body table th.arfo {width: 150px; text-align: center;}
div.body table td.arfo {text-align: center;}
div.body table th.dim {text-align: center;}
div.body table td.cl1 div.bg_grey {padding: 5px; font-weight: bold;}
div.body table td.cl2 span.i1 {font-size: 25px; font-weight: bold;}

span.art2 {font-size: 10px;}

EOF;
return $str;
}
    
    //in questo caso i record sono i colli
    function html($ar_record, $parametri = array()){
        
        //css da template
        $html .= "<style>" . $this->template_class->_css() . "</style>";
        
        //inserimento css, ToDo: solo per prima richiesta
        $html .= "<style>" . $this->_css() . "</style>";
       
        //contenuto
        $html .=  $this->all($ar_record);
        
        return $html;
    }
    
    function all($ar_record){
        $ret = array();
        $ar_grou_by = array();
        
        
        //ToDo: ordinamento
        
        //separo per ordine
        if ($this->parametri['parametri_formato']['TOT'] == 'ORD'){
            foreach($ar_record as $rec){
                $ar_grou_by[$rec['TDDOCU']][] = $rec;
            }
        } elseif($this->parametri['parametri_formato']['TOT'] == 'SEQ'){
            //separo per sequenza
            foreach($ar_record as $rec){
                $ar_grou_by[$rec['TDSELO']][] = $rec;
            }
            
        }else {
            //separo per lotto
            foreach($ar_record as $rec){
                $ar_grou_by[$rec['TDNRLO']][] = $rec;
            }
        }
        
        $ret[] = "<div class=page>";
        $ret[] = "<div class=header>".$this->template_class->_page_header($rec, $this)."</div>";
        foreach ($ar_grou_by as $ar_colli){
            $rec = $ar_colli[0]; //dal primo prendo i dati di testata (lotto, ...)
            $ret[] = $this->single_html($rec, $ar_colli);
        }
        $ret[] = "</div>";
        
         
        return implode("\n", $ret);
    }
    
    
    function single_html($rec, $ar_colli){
        $html = '';
        
        
        if ($this->parametri['parametri_formato']['TOT'] == 'ORD'){
            $html .= "<div class=sub_header>".$this->template_class->_page_sub_header($rec, $ar_colli)."</div>";
        }
        $html .= "<div class=body>".$this->_page_body($rec, $ar_colli)."</div>";
        return $html;
    }
    
    
    function _page_body($rec, $ar_colli){
        $html = '';
        
        //non entra nei parametri per prendere TOT/DIM
        
        $ar_rrn = array();
        foreach($ar_colli as $collo){ 
            $ar_rrn[] = $collo['RRN_PL'];
        }
        
       
        //da array dei colli recupero (raggruppato per art) i componenti relativi
        $pp_compo = _pp_get_colli_compo($this->main_module, 
                    $this->parametri['k_lista'],
                    array('RRN_PL' => $ar_rrn), 
                    array('group_by' => 'ART', 'DIM' => $this->parametri['parametri_formato']['DIM']));
        $ar_compo = $pp_compo['ar_compo'];
        
        $html .= "<table>";
        $html .= "<tr class=header>
                    <th>UM</th>
                    <th>Q.t&agrave;</th>";
        if ($this->parametri['parametri_formato']['DIM'] != 'N') {
            $html .= "<th>NR</th>";
        }
        $html .= "<th>Articolo</th>
                  <th>Descrizione / Segnalazioni</th>";
        if ($this->parametri['parametri_formato']['DIM'] != 'N'){
            $html .= "<th colspan = 3 class = dim>Dimensioni</th>";
        }
        
       
        if($this->parametri['parametri_formato']['TOT'] == 'SEQ'){
            $html .= "<th class=arfo>Carico-Seq.</th>";
        }else{
            $html .= "<th class=arfo>Codice Riferim.</th>";
        }
        
        $html .= "</tr>";
        
        foreach($ar_compo as $dr){
        
        	if ($dr['REUM'] != 'NR' && $dr['REUM'] != 'PZ'){
        		$row_nr = $dr['C_ROW'];
        	} else {
        		$row_nr = '&nbsp;';
        	}
        
            $html .= "<tr>
                <td class=um>{$dr['REUM']}</td>
                <td class=\"n cc qty\">".n($dr['REQTA'],3)."</td>";
            if ($this->parametri['parametri_formato']['DIM'] != 'N') {
                $html .= "<td class=\"n cc nr\">{$row_nr}</td>";
            }
            $html .= "<td class=art>{$dr['REART']}</td>
                 <td class=des>{$dr['REDART']}<br/>
                 <span class=art2>".$this->_note($dr)."</span></td>
             ";
            if ($this->parametri['parametri_formato']['DIM'] != 'N'){
                $html .= "<td class = cl2>".n($dr['REDIM1'], 0)."</td>
                        <td class = cl2>".n($dr['REDIM2'], 0)."</td> 
                        <td class = cl2>".n($dr['REDIM3'], 0)."</td>";
            }
             
            if($this->parametri['parametri_formato']['TOT'] == 'SEQ'){
                $html .= "<td class=cl2>".$this->_format_carico($ar_colli[0]['TDNRCA'])."
                <span class=i1>{$ar_colli[0]['TDSELO']}</span></td>";
            }else{
                $html .= "<td class = arfo>{$dr['ARARFO']}</td>";
            }
            
            $html .= "</tr>";
        }
        
        $html .= "</table>";
        
        return $html;
    }
    
    function _note($rowPL){
        
        $ar_note = _pp_get_collo_note($this->main_module, $rowPL, 
                        array('RLTPNO'  => 'RD',
                              'RLRIFE2' => $this->parametri['parametri_formato']['NOR'],
                              'solo_testo' => 'Y'));
        
        $text = implode("<br>", $ar_note['ar_note']);
        
        
        if (count($ar_note['ar_note']) > 0){
            $html .= $text;
        } else {
            $html .= "";
        }
        
        return $html;
    }
    
    function _format_ordine($rec){
        
        $numero = sprintf("%06s", $rec['TDONDO']);
        $tipo = trim($rec['TDOTPD']);
        $anno = substr($rec['TDOADO'], 2);
        
        return "<span style=\"font-size: 1.2em;\"><b>{$numero}</b></span> {$tipo}_{$anno}";
        
    }
    
    function _format_carico($value){
        
        $m_value = sprintf("%06s", $value);
        $p1 = substr($m_value, 0, 2);
        $p2 = substr($m_value, 2, 2);
        $p3 = substr($m_value, 4, 2);
        $span_p = "<span style=\"font-size: 1.2em;\"><b>{$p3}</b></span>";
        return "{$p1}.{$p2}/{$span_p}";
        
    }
    
    
}