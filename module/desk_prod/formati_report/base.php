<?php 

require_once __DIR__ . '/_include.php'; //utility rendering

/*
 * Modulo base di stampa
 */

class FR_BASE {
    
    public function _get_image_logo_by_indice($rec, $from_module){
        
        $cfg_mod = $from_module->main_module->get_cfg_mod();
        //da rowCD recupero l'indice (CDDICL) e da questo il codice logo (in TACAP)
        $codice_indice = $from_module->parametri['rowCD']['CDDICL'];
        $row_indice    = $from_module->main_module->get_TA_std($cfg_mod['taid_indici'], $codice_indice);
        
        $row_tab_logo = $from_module->main_module->get_TA_std('LOGOP', $row_indice['TACAP']);
        
        $logo_path    = trim($row_tab_logo['TAINDI']);
        
        $img = ROOT_PATH."personal/desk_prod/loghi/{$logo_path}";
        return $img;
        
    }
    
    
    public function _get_image_logo($rec, $from_module){
        //esempio: ".ROOT_PATH."personal/desk_prod/loghi/logo_turi_mini.bmp
        
        //il formato logo lo trovo in TDAUX4 (solo in WPI5TD0)
        
        $row_tab_logo = $from_module->get_TA_std('LOGOP', $rec['TDAUX4']);
        $logo_path    = trim($row_tab_logo['TAINDI']);
        
        $img = ROOT_PATH."personal/desk_prod/loghi/{$logo_path}";
        return $img;
    }
    
    
    
    function _get_image($rec, $per_ordine = false){
        $order_dir          = _pp_order_dir($this->main_module, $rec['TDDOCU']);
        $order_dir_assoluto = _pp_order_dir_assoluto($this->main_module, $rec['TDDOCU']);
        
        $fisso = $this->parametri['parametri_formato']['IMG']; //es: _btop_
        if($per_ordine == true)
            $riga_grafica = "";
        else 
            $riga_grafica = trim($rec['PLCINT']);
        
        $ricerca_file = $order_dir_assoluto . '/*' . "{$fisso}{$riga_grafica}.JPG";
        
        $f = null;
        foreach (glob($ricerca_file) as $filename){
            $f = basename($filename);
        }
        
        if (is_null($f)){
            //se immagine non trovata, da formato logo, recupero immagine "No immagine"
            $row_tab_logo = $this->main_module->get_TA_std('LOGOP', $rec['TDAUX4']);
            $logo_path    = trim($row_tab_logo['TALOCA']);
            
            $img = "../personal/desk_prod/loghi/{$logo_path}";
        } else {
            //se immagine trovata
            $img = '../'.$order_dir . '/' . $f;
        }
        
        return $img;
    }
    
    
    
    
    public function _get_image_assonometria($rec, $from_module){
        $order_dir          = _pp_order_dir($from_module->main_module, $rec['TDDOCU']);
        $order_dir_assoluto = _pp_order_dir_assoluto($from_module->main_module, $rec['TDDOCU']);
        
        $fisso = '_AssoDxBN_'; //ToDo: parametrizzare
        
        $ricerca_file = $order_dir_assoluto . '/*' . "{$fisso}.JPG";
        
        $f = null;
        foreach (glob($ricerca_file) as $filename){
            $f = basename($filename);
        }
        
        if (is_null($f)){
            //se immagine non trovata, da formato logo, recupero immagine "No immagine"
            $row_tab_logo = $from_module->main_module->get_TA_std('LOGOP', $rec['TDAUX4']);
            $logo_path    = trim($row_tab_logo['TALOCA']);
            
            $img = "../personal/desk_prod/loghi/{$logo_path}";
        } else {
            //se immagine trovata
            $img = '../'.$order_dir . '/' . $f;
        }
        
        return $img;
    }
    
}