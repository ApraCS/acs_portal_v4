<?php

require_once __DIR__ . '/_include.php'; //utility rendering

/*
 * Schede colli lineari
 */

class FR_SCH_COL01 extends FR_BASE {
    
    public $main_module;
    public $parametri = array();
    public $template_class = null;
    
    public $forza_paginazione_preview = 'S';
    public $paginazione_preview_per = array('TDSELO'); 
    
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
        
        $this->template_class = _pp_load_template('a4_sch_01');
    }
    
    
    function _css(){
$str = <<<EOF

div.body {display: flex; flex-flow: column; flex: 1}
div.body div.collo {flex: 1; margin: 0px; display: flex;}

div.collo.row1 {border-bottom: 1px dashed grey}

div.collo div.collo_main {flex: 1}
div.collo div.collo_rbar {flex: 0 3cm}
div.collo_main div.pldart {font-weight: bold; font-size: 14px;}

div.rbar {font-size: 10px}
div.rbar1 {color: white; background-color: black; padding: 5px;}
div.rbar1 span.collo{font-weight: bold;}
div.rbar2 {padding: 5px}

.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 70%;
  height: 220px;
}

EOF;
        return $str;
    }
    
    
    
    //in questo caso i record sono i colli
    function html($ar_record, $parametri = array()){
        
        //css da template
        $html .= "<style>" . $this->template_class->_css() . "</style>";
        
        //inserimento css, ToDo: solo per prima richiesta
        $html .= "<style>" .$this->_css() . "</style>";
        
        //contenuto
        $html .=  $this->all($ar_record);
        
        return $html;
    }
    
    
    
    function all($ar_record){
        $ret = array();
                
        //ToDo: ordinamento
        
        //ToDo: in una pagina vanno stampati due colli
        /*foreach ($ar_record as $rec){
            $ret[] = $this->single_html($rec, array($rec, $rec));
        }*/
        for ($i=0; $i<count($ar_record);$i+=2){
            $rec0 = $ar_record[$i];
            if (count($ar_record) >= ($i+1))
                $rec1 = $ar_record[$i+1];
            else 
                $rec1 = null;
            
            $ret[] = $this->single_html($rec0, array($rec0, $rec1));
        }
        
        return implode("\n", $ret);
    }
    
    function single_html($rec, $ar_colli){
        $html = '';
        $html .= "<div class=page>
                      <div class=header>".$this->template_class->_page_header($rec, $this)."</div>
                      <div class=body>".$this->_page_body($rec, $ar_colli)."</div>
                  </div>";
        return $html;
    }
    
    function _page_body($rec, $ar_colli){
        $html = '';
        $html.= $this->singolo_collo($ar_colli[0], 'row1');
        if (!is_null($ar_colli[1]))
            $html.= $this->singolo_collo($ar_colli[1], 'row2');
        return $html;
    }
    
    
    function singolo_collo($rec, $classe_row){
        $html = "";
        $html.= "<div class=\"collo {$classe_row}\">
                    <div class=collo_main>
                        <div class=pldart>{$rec['PLDART']}</div>
                        ".$this->_note($rec). " 
                        <div class=collo_image>".$this->_collo_image($rec)."</div>
                    </div>
                    <div class=collo_rbar>".$this->_collo_rbar($rec)."</div>
                </div>";
        return $html;
    }
    
    
    function _collo_rbar($rec){
        $html = "";
        $html.= "<div class=rbar1><span class=collo>Collo {$rec['PLNCOL']}</span> - {$rec['PLCINT']}</div>
                 <div class=\"rbar2 bg_grey\">Art. {$rec['PLART']}</div>";
        return $html;
    }
    
    
    function _collo_image($rec){
        $img = $this->_get_image($rec);
        
        $html = "";
        $html .= "<img src=" . img_path($img) . " class=\"center\">";
        return $html;
    }
    
    function _note($rowPL){
        
        $ar_note = _pp_get_collo_note($this->main_module, $rowPL, 
                    array('RLTPNO'  => 'RD',
                          'RLRIFE2' => $this->parametri['parametri_formato']['NOR'],
                          'solo_testo' => 'Y'));
     
        $text = implode("<br><img src=" . img_path("sub_black_next.png") . " height=20px; width=20px;>", $ar_note['ar_note']);
        
        $html = '';
        $html.= "<div class=note>";
        
        if (count($ar_note['ar_note']) > 0){
            $html .= "<span class=note_text><img src=" . img_path("sub_black_next.png") . " height=20px; width=20px;>{$text}</span>";
        } else {
            $html .= "";
        }
        
        
        $html.= "</div>";
        return $html;
    }
    
}