<?php

require_once("../../config.inc.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_todolist_preview_include.php';

$main_module = new DeskProd();

$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'exe_esegui'){
//----------------------------------------------------------------------

    $rowAS   = get_as_row_by_id($main_module, $m_params->open_request->id_assoluto_lista);
    
    
    $rowCD   = get_cd_row_by_id($main_module, $rowAS['ASORD4']);
    
    //dal formato di uscita recupero il report/procedura da richiamare
    $row_formato = $main_module->get_TA_std('CDGED', $rowCD['CDGEND']);
    
    
    $report_procedura = rtrim($row_formato['TALOCA']);
    $report_procedura_class_name = "FP_" . strtoupper($report_procedura);
    
    require_once("formati_proc/base.php");
    require_once("formati_proc/{$report_procedura}.php");
    $r = new $report_procedura_class_name($main_module,
        array(
            'rowCD' => $rowCD,
            'parametri_formato' => _pp_parametri_formato($rowCD['CDPAGD'])
        ));
    
    
    //ToDo: potrebbe essere per colli, o lotto, o carico...
    $pp_colli = _pp_get_colli($main_module, $m_params, $m_params->preview, $r);
    
    $ar_colli = $pp_colli['ar_colli'];
    
    foreach($ar_colli as $k=>$v){
        $ar_colli[$k]['ASDTAS'] = $rowAS['ASDTAS'];
        $ar_colli[$k]['ASHMAS'] = $rowAS['ASHMAS'];
        $ar_colli[$k]['ASORD1'] = $rowAS['ASORD1'];
        $ar_colli[$k]['ASORD2'] = $rowAS['ASORD2'];
        $ar_colli[$k]['ASORD3'] = $rowAS['ASORD3'];
        $ar_colli[$k]['d_voce'] = $m_params->open_request->d_voce;
    }

    //$html .= '<pre>SQL: ' . $pp_colli['sql'] . '</pre>';
    //$html .= '<pre>SQL params: ' . print_r($pp_colli['sql_params'], true) . '</pre>';
    
    $ret = $r->exe($ar_colli, _pp_parametri_formato($rowCD['CDPAGD']));    //ToDo: potrebbe non essere per colli
    echo acs_je( $ret );
 exit;
}







//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_tab'){
//----------------------------------------------------------------------
    //recupero record AS e k_lista
    $rowAS   = get_as_row_by_id($main_module, $m_params->id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    if (strlen($k_lista) > 0)
        $title_suff = "Filtro lista: {$k_lista}";
            
    $progetto = $m_params->k_progetto;
    $ar_gruppo = explode("|", $m_params->k_distinta);
    $d_voce = $m_params->d_voce;
    $title = "{$progetto}, {$ar_gruppo[1]}, {$ar_gruppo[2]}: {$d_voce}";
    
    $rowCD   = get_cd_row_by_id($main_module, $rowAS['ASORD4']);
    
    //dal formato di uscita recupero il report/procedura da richiamare
    $row_formato = $main_module->get_TA_std('CDGED', $rowCD['CDGEND']);
    
    $report_procedura = rtrim($row_formato['TALOCA']);
    $report_procedura_class_name = "FR_" . strtoupper($report_procedura);
    
    /*per debug */
    $title .= ' [' . implode(', ', array(trim($rowCD['CDFOUS']), $report_procedura)) . ']';
    
    ?>	
	{
		success:true,	
		m_win: {
			title: <?php echo j($title); ?>,
			width: 600, height: 300,
			iconCls : 'icon-module-16'
		},
		items: [
				{
					xtype: 'panel'
					, flex: 1
					, items: [
			        	{
			        	 xtype: 'panel',
			        	 autoScroll: true,
			        	 flex: 1,
			        	 padding: 10,
			        	 html: '<h1>Confermi esecuzione .........</h1>',
			        	 itemId: 'main-content',
			        	 border: false,
			        	 }
			        ]
			        
				 
        			, buttons: [
        			
        			  {
    		            text: 'Esegui',
    			        iconCls: 'icon-button_grey_last-32',	            
    			        scale: 'large',		
    			        handler: function(b) {
    			          var m_win = b.up('window');
    			          Ext.Ajax.request({
 						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_esegui',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   open_request: <?php echo acs_je($m_params); ?>
 								},
 								
 								success: function(response, opts) {
						        	 var jsonData = Ext.decode(response.responseText);
						        	 m_win.destroy();
						        	 acs_show_msg_info('Operazione completata');
						        	 
								},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
    			          
    		            }
    		          }		
    		          		
                    ]    

		  	} //panel
		
		]
	}
		
<?php 
	exit;
}
