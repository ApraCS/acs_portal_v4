<?php 

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();
$main_module =  new DeskProd();
$cfg_mod = $main_module->get_cfg_mod();
$search = "<img src=" . img_path("icone/48x48/search.png") . " height=20>";

$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "PRGRO - Mansioni",
    'descrizione' => "Gestione gruppi",
    'form_title' => "Dettagli tabella mansioni",
    'fields_preset' => array(
        'TATAID' => 'PRGRO'
    ),
  
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TAB_ABB', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice',  'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        
        
        //TABELLA ABBINATA
        'TAB_ABB' =>  array( 'label' => 'ope_abbinati', 'type' => 'TA', 
                             'iconCls' => 'search',
                             'tooltip' => 'Operatori abbinati',
                             'select' => "TA_OP.C_ROW AS TAB_ABB",
                             'join' => "LEFT OUTER JOIN ( 
                                        SELECT COUNT(*) AS C_ROW, TA2.TAKEY1
                                        FROM {$cfg_mod['file_tabelle']} TA2
                                        WHERE TA2.TADT = '{$id_ditta_default}' AND TA2.TATAID = 'PRGRU'
                                        GROUP BY TA2.TAKEY1) TA_OP
                                        ON TA.TAKEY1 = TA_OP.TAKEY1",
                            'ta_config' => array(
                                'tab' => 'PRGRU',
                                'file_acs'     => 'acs_gest_PRGRU.php?fn=open_tab',
                              
                            )
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
        
        
    ),
    
    'buttons' => array(
        array(
            'text'  => '',
            'iconCls' => 'icon-print-16',
            'tooltip' => 'Riepilogo Mansioni/Operatori assegnati',
            'scale' => 'small',
            'style' => 'border: 1px solid gray;',
            'handler' =>  new ApiProcRaw("function(){
                window.open('acs_gest_PRGRO_report.php?fn=open_report');
             }")
        )
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
