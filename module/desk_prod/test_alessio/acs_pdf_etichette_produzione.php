<?php

require_once "../../config.inc.php";

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// FORM RICHIESTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_parameters'){
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
			buttons: [{
	            text: 'Visualizza',
	            iconCls: 'icon-print-32', scale: 'large',
	            handler: function() {
	                this.up('form').submit({
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST'
                  });
	                
	            }
	        }],             
            
            items: [
            	{xtype: 'hiddenfield', name: 'filtro_type', value: <?php echo j($m_params->open_request->filtro_type)?>},				               
            	{xtype: 'hiddenfield', name: 'filtro_c', value: <?php echo j($m_params->open_request->filtro_c)?>},
            ]
        }
	
]}
<?php
exit;
}

// ******************************************************************************************
// REPORT
// ******************************************************************************************

$filtro_type = $_REQUEST['filtro_type']; //es: LOTTO o CARICO ...
$filtro_c    = $_REQUEST['filtro_c']; //es: LP_2020....

$td_sped_field = 'TDNBOF';
        

switch ($filtro_type){
	case "LOTTO":
	    $ar_filtro_c = explode('_', $filtro_c);
	    $sql_WHERE_filtro_type .= " AND TDTPLO = " . sql_t($ar_filtro_c['0']);
	    $sql_WHERE_filtro_type .= " AND TDAALO = " . $ar_filtro_c['1'];
	    $sql_WHERE_filtro_type .= " AND TDNRLO = " . $ar_filtro_c['2'];
	    if (count($ar_filtro_c) == 4) $sql_WHERE_filtro_type .= " AND TDDTOR = " . sql_t($ar_filtro_c['3']);
	    
		$email_subject = "Lista sequenza carico del " . print_date($spedizione['CSDTSP']);
		break;	
	default: die("error: filtro_type non riconosciuto.");
}


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

$html = '
<html>
 <head>  
    <link rel="stylesheet" type="text/css" href="./css/etichetta_pdf.css"
 </head>
 
 <body>
'; 

	 
	$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
				   LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				     ON TD.TDDT=SP.CSDT AND {$td_sped_field} = SP.CSPROG AND SP.CSCALE = '*SPR'
                   INNER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                     ON TD.TDDT=RD.RDDT AND TD.TDOTID = RD.RDTIDO AND TD.TDOINU = RD.RDINUM AND RD.RDAADO = TD.TDOADO AND TD.TDONDO = RD.RDNRDO 
				   ";
	$sql_WHERE = " WHERE " . $s->get_where_std() . " AND TDSWSP='Y' ";
	
	$sql = "SELECT * " . $sql_FROM . $sql_WHERE	. "
            $sql_WHERE_filtro_type 
            ORDER BY TDTPCA, TDAACA, TDNRCA, TDSECA, TDCCON
            limit 10  /*ELIMAREEEEE*/
            ";	

	$stmt = db2_prepare($conn, $sql);		
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
 
    $liv0_in_linea = null;
	$ar = array();	
	$c = 0;
	
	while ($row = db2_fetch_assoc($stmt)) {
	    
	    $row['TDDLOC']  = trim($row['TDDLOC']);
	    $row['TDONDO']  = trim($row['TDONDO']);
	    $row['TDTPCA']  = trim($row['TDTPCA']);
	    $row['TDSECA']  = trim($row['TDSECA']);
	    $row['RDQTA']   = trim($row['RDQTA']);
	    $row['RDART']   = trim($row['RDART']);
	    $row['RDDART']  = trim($row['RDDART']);
	    
	    $c++;
	    
	    if ($c > 1)
	        $html .= '<div pagebreak="true">';
	    
	    $html .= "<table style=\"border:1px solid black;\">
                      <tr>
                        <th class=\"lato-sup\" rowspan=\"3\"></th>
                        <th colspan=\"2\"></th>
                        <th class=\"lato-sup\" class=\"border\"><font size=\"8\">Rif. Produzione</font><div style=\"text-align:center\"><b><font size=\"40\">" . trim($row['TDRFCA']) . "</font></b></div></th>
                      </tr>
                      <tr>
                        <th colspan=\"2\" rowspan=\"4\" style=\"text-align:center;\"><center><img src=" . logo_path() . "></center></th>
                        <th class=\"lato-sup\" class=\"border-no-top\"><font size=\"8\">Carico</font><div style=\"text-align:center\"><b><font size=\"30\">" . implode('_', array($row['TDAACA'], $row['TDNRCA'])) . "</font></b></div></th>
                      </tr>
                      <tr>
                        <th class=\"lato-sup\" class=\"border-no-top\"><font size=\"8\">Seq. Carico</font><div style=\"text-align:center\"><b><font size=\"15\">" . $row['TDSECA'] . "</font></b></div></th>
                      </tr>
                      <tr>
                        <th style=\"background-color:#E0E0E0;\" class=\"lato-sup\" class=\"border\"><font size=\"8\">Riferimento</font><div><b><font size=\"15\">" . trim($row['TDVSRF']) . "</font></b></div></th>
                        <th class=\"lato-sup\" class=\"border-no-top\"><font size=\"8\">Modello</font><div style=\"text-align:center\"><b><font size=\"15\">" . $row['TDDVN1'] . "</font></b></div></th>
                      </tr>
                      <tr>
                        <th class=\"border-no-left-top\"><font size=\"8\">Ordine</font><div style=\"text-align:right\"><b><font size=\"15\">" . $row['TDONDO'] . "</font></b></div></th>
                        <th class=\"lato-sup\" class=\"border-no-left-top\"><font size=\"8\">Frontale</font><div style=\"text-align:center\"><b><font size=\"10\">front</font></b></div></th>
                      </tr>
                      <tr>
                        <th class=\"lato-sup\" class=\"border-no-left-top\"><font size=\"8\">Quantit&agrave;</font><br><div style=\"text-align:right\"><b><font size=\"15\">" . $row['RDQTA'] . "</font></b></div></th>
                        <th class=\"border-bottom\" style=\"text-align:left\"><div></div><div></div>" . $row['RDART'] . "</th>
                        <th class=\"border-bottom\" style=\"text-align:right\"><div></div><div></div>" . implode(' x ', array($row['RDDIM1'], $row['RDDIM2'], $row['RDDIM3'])) . "</th>
                        <th class=\"lato-sup\" class=\"border-no-top\"><font size=\"8\">Etichetta</font><div style=\"text-align:center\"><b><font size=\"30\"></font></b></div></th>
                      </tr>
                      <tr>
                        <th class=\"border-left\" class=\"lato-inf\"><font size=\"8\">Controllo<br>Operatore</font><br><br><br><br><br><br><img src=" . logo_path() . "><div style=\"text-align:center\"><font size=\"8\">28</font></div></th>
                        <th class=\"border-left\" colspan=\"2\"><font size=\"8\">Descrizione Articolo</font><br><div><b><font size=\"12\">" . $row['RDDART'] . "</font></b></div></th>
                        <th class=\"border-left\"><font size=\"8\">BARCODE ACCETTAZIONE MTO</font><b><br><div style=\"text-align:center;\"><font size=\"30\">*123456789*</font></b></div></th>
                      </tr>
                    </table>
                    ";		
	} //while
	
	$html .= "</body></html>";

	
	
//Generazione PDF
	require 'TCPDF/tcpdf.php';
	
	class MYPDF extends TCPDF {
	    
	    //Page header
	    public function Header() {
	    }
	    
	    // Page footer
	    public function Footer() {
	    }
	    
	    // Rotazione
	    public function RotateBarCode() {
	        // IMPOSTAZIONE CODICE A BARRE
	        $style = array(
	        'position' => 'L',  //R = right / C = center / L = left
	        'align' => '',
	        'stretch' => false,
	        'fitwidth' => true,
	        'cellfitalign' => '',
	        'border' => false,
	        'hpadding' => 'auto',
	        'vpadding' => 'auto',
	        'fgcolor' => array(0,0,0),  //barre
	        'bgcolor' => false, //array(255,255,255),   //sfondo
	        'text' => true,
	        'font' => 'helvetica',
	        'fontsize' => 8,
	        'stretchtext' => 10
	        );
	        
	        // POSIZIONE CODICE A BARRE
	        $x = $this->GetX();
	        $y = $this->GetY();
	        
	        // CREAZIONE CODICE A BARRE
	        //write1DBarcode(stringa, codice/tipo, x, y, lunghezza, altezza, larghezza_barre, array_stile, posizione[N=sopra / T=difianco / M=angolo])
	        $this->write1DBarcode(' 8 123456789 ', 'C128B', $x-50, $y, 150, 25, 0.4, $style, '');
	        
	        //Reset X,Y so wrapping cell wraps around the barcode's cell.
	        $this->SetXY($x,$y);
	        
	        // CELLA DEL CODICE A BARRE (non necessaria)
	        //Cell($w(largezza_cella), $h=0(altezza_cella), $txt='', $border=0, $ln=0, $align='', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')
	    }
	    
	    function RotatedImage($file,$x,$y,$w,$h,$angle) {
	        //Image rotated around its upper-left corner
	        $this->Rotate($angle,$x,$y);
	        $this->Image($file,$x,$y,$w,$h);
	        $this->Rotate(0);
	    }
	}
	
	$pdf = new MYPDF('L', PDF_UNIT, 'LEGAL', true, 'UTF-8', false);
	
	//$bar_code = new Barcode39('ale', 5);
	//$bar_code->draw();
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('A.R.');
	$pdf->SetTitle('TURI');
	
	// set default header data
	$pdf->SetHeaderData(/*PDF_HEADER_LOGO*/ $pdf_logo, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
	
	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	// set margins
	$pdf->SetMargins(50, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	// add a page
	$pdf->AddPage('L', 'LEGAL');    //LETTER
	
	$pdf->writeHTML($html, true, false, true, false, '');
	
///	$pdf->RotatedImage('./images/cod_barre.png',52.5,80,52,30,90);
	
	// reset pointer to the last page
	$pdf->lastPage();
	//Close and output PDF document
	$pdf->Output('Etichette_Produzione', 'I');		