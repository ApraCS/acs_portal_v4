<?php

require_once "../../config.inc.php";


$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// FORM RICHIESTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_parameters'){
    ?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
			buttons: [{
	            text: 'Visualizza',
	            iconCls: 'icon-print-32', scale: 'large',
	            handler: function() {
	                this.up('form').submit({
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST'
                  });
	                
	            }
	        }],             
            
            items: [
            	{xtype: 'hiddenfield', name: 'filtro_type', value: <?php echo j($m_params->open_request->filtro_type)?>},				               
            	{xtype: 'hiddenfield', name: 'filtro_c', value: <?php echo j($m_params->open_request->filtro_c)?>},
            ]
        }
	
]}
<?php
exit;
}

// ******************************************************************************************
// REPORT
// ******************************************************************************************

$filtro_type = $_REQUEST['filtro_type']; //es: LOTTO o CARICO ...
$filtro_c    = $_REQUEST['filtro_c']; //es: LP_2020....


switch ($filtro_type){
    case "LOTTO":
        $ar_filtro_c = explode('_', $filtro_c);
        $sql_WHERE_filtro_type .= " AND TDTPLO = " . sql_t($ar_filtro_c['0']);
        $sql_WHERE_filtro_type .= " AND TDAALO = " . $ar_filtro_c['1'];
        $sql_WHERE_filtro_type .= " AND TDNRLO = " . $ar_filtro_c['2'];
        if (count($ar_filtro_c) == 4) $sql_WHERE_filtro_type .= " AND TDDTOR = " . sql_t($ar_filtro_c['3']);
        
        $email_subject = "Lista sequenza carico del " . print_date($spedizione['CSDTSP']);
        break;
    default: die("error: filtro_type non riconosciuto.");
}



require 'TCPDF/tcpdf.php';
//echo K_PATH_MAIN; exit;
//echo PDF_HEADER_LOGO; exit;

class MYPDF extends TCPDF {
    
    //Page header
    public function Header() {
        // Logo
        $image_file = './images/logo_apra_cs.png';
        $data = date("d/m/Y H:i");
        $this->Image($image_file, 15, 10, 15, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->writeHTML($data, $ln=true, $fill=false, $reseth=false, $cell=false, $align='R');
        $this->SetLineStyle(array('width' => 0.85 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => '000000'));
        $this->SetY((2.835 / $this->k) + max(20, $this->y));
        $this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'T', 0, 'C');
    }
    
    // Page footer
    public function FooterX() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Pag. '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}


$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, /*PDF_PAGE_FORMAT*/ 'A4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('A.R.');
$pdf->SetTitle('TURI');

// set default header data
$pdf->SetHeaderData(/*PDF_HEADER_LOGO*/ $pdf_logo, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

//PADDING
//$pdf->setCellPaddings(1, 1, 1, 1);   //non ho capito come funziona /$pdf->setCellPaddings( $left = '', $top = '', $right = '', $bottom = '');
//$pdf->setCellPadding(0);        //spazio da lasicare nella pagina in %
//$pdf->getCellHeight(10, true);  //getCellHeight($fontsize, $padding)
//$pdf->getCellPaddings(50);
//$pdf->MultiCell( $w, $h, $txt, $border = 0, $align = 'J', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false )

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// add a page
$pdf->AddPage();

$spacer = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

$html = "<html>
          <head>
            <link rel=\"stylesheet\" type=\"text/css\" href=\"./css/produzione_pdf.css\" />
          </head>
          <body>";
$html  .= "<table>
              <tr>
                <th class=\"image_header\"><div style=\"text-align:center\"><img src=" . logo_path() . "><br><b><font size=\"16\"></font></b></div></th>
                <!-- <th class=\"image_header\"><div style=\"text-align:center\"><b><font size=\"16\"></font></b><br><br><img src=" . logo_path() . "></div></th> -->
                <th class=\"titoli_header\">
                    <div style=\"text-align:left;\"><b><font size=\"16\">" . $spacer . "Riepilogo Lotti</font></b></div>
                    <div style=\"text-align:right;\"><b><font size=\"12\">Piano Di Produzione</font>" . $spacer . "<font size=\"20\">{$filtro_c}" . $spacer . "</font></b></div>
                </th>
              </tr>
              <tr>
                <th></th>
              </tr>";

//lotti
$sql_lotto = "SELECT COUNT(*) as COUNT, TD.TDTPLO, TD.TDAALO, TD.TDNRLO
              FROM {$cfg_mod_Spedizioni['file_testate']} TD
        	  WHERE TDTPLO = 'LP' $sql_WHERE_filtro_type 
              GROUP BY TD.TDTPLO, TD.TDAALO, TD.TDNRLO
              ORDER BY TD.TDAALO DESC
              LIMIT 10";

$stmt_lotto = db2_prepare($conn, $sql_lotto);
echo db2_stmt_errormsg();
$result_lotto = db2_execute($stmt_lotto);
$page_break = 0;

while ($row_lotto = db2_fetch_assoc($stmt_lotto)) {
    
    $primo_lotto = 1;
    $page_break++;
    
    //lotti
    $row_lotto['TDTPLO'] = trim($row_lotto['TDTPLO']);
    $row_lotto['TDAALO'] = trim($row_lotto['TDAALO']);
    $row_lotto['TDNRLO'] = trim($row_lotto['TDNRLO']);
    $row_lotto['TDTOCO'] = trim($row_lotto['TDTOCO']);
    $row_lotto['TDCOPR'] = trim($row_lotto['TDCOPR']);
    $row_lotto['TDCOSP'] = trim($row_lotto['TDCOSP']);

    if ($page_break == 1) {

    } else {
        if ($primo_lotto == 1){
            $html .= "<tr pagebreak=\"true\">
                        <th class=\"intestazione\">LOTTO '" . $row_lotto['TDNRLO'] . "'</th>
                      </tr>
                      <tr>
                        <th class=\"titoli_lotti\">Tipo Lotto</th>
                        <th class=\"titoli_lotti\">Anno Lotto</th>
                        <th class=\"titoli_lotti\">Numero Lotto</th>
                        <th class=\"titoli_lotti\">Totale Colli</th>
                        <th class=\"titoli_lotti\">Colli Prodotti</th>
                        <th class=\"titoli_lotti\">Colli Spediti</th>
                      </tr>";
        }
        $html .= "<tr>
                    <td class=\"valori_lotti\">" . $row_lotto['TDTPLO'] . "</td>
                    <td class=\"valori_lotti\">" . $row_lotto['TDAALO'] . "</td>
                    <td class=\"valori_lotti\">" . $row_lotto['TDNRLO'] . "</td>
                    <td class=\"valori_lotti\">" . $row_lotto['TDTOCO'] . "</td>
                    <td class=\"valori_lotti\">" . $row_lotto['TDCOPR'] . "</td>
                    <td class=\"valori_lotti\">" . $row_lotto['TDCOSP'] . "</td>
                  </tr>";
    }
    //carichi
    $sql_carichi = "SELECT COUNT(*) as COUNT, TDTPLO, TDAALO, TDNRLO, TDTPCA, TDAACA, TDNRCA, SUM(TDTOCO) AS TDTOCO, TDCOPR, TDCOSP
                	FROM {$cfg_mod_Spedizioni['file_testate']}
                    WHERE 
                             TDTPLO = '{$row_lotto['TDTPLO']}' 
                         AND TDAALO = '{$row_lotto['TDAALO']}' 
                         AND TDNRLO = '{$row_lotto['TDNRLO']}'
                         AND TDDTOR = '{$row_lotto['TDDTOR']}'
                    GROUP BY TDTPLO, TDAALO, TDNRLO, TDTPCA, TDAACA, TDNRCA, TDCOPR, TDCOSP
                    LIMIT 1";
    
    $stmt_carichi = db2_prepare($conn, $sql_carichi);
    echo db2_stmt_errormsg();
    $result_carichi = db2_execute($stmt_carichi);
    
    while ($row_carichi = db2_fetch_assoc($stmt_carichi)) {
        
        $primo_carico = 1;
        
        //carichi
        $row_carichi['TDTPCA'] = trim($row_carichi['TDTPCA']);
        $row_carichi['TDAACA'] = trim($row_carichi['TDAACA']);
        $row_carichi['TDTNRA'] = trim($row_carichi['TDNRCA']);
        $row_carichi['TDTOCO'] = trim($row_carichi['TDTOCO']);
        $row_carichi['TDCOPR'] = trim($row_carichi['TDCOPR']);
        $row_carichi['TDCOSP'] = trim($row_carichi['TDCOSP']);
        
        if ($primo_carico == 1){
            $html .= "<tr>
                        <th class=\"intestazione\">CARICO '" . $row_carichi['TDNRCA'] . "'</th>
                      </tr>
                      <tr>
                        <th class=\"titoli_carichi\">Tipo Carico</th>
                        <th class=\"titoli_carichi\">Anno Carico</th>
                        <th class=\"titoli_carichi\">Numero Carico</th>
                        <th class=\"titoli_carichi\">Totale Colli</th>
                        <th class=\"titoli_carichi\">Colli Prodotti</th>
                        <th class=\"titoli_carichi\">Colli Spediti</th>
                      </tr>";
        }
        $html .= "<tr>
                    <td class=\"valori_carichi\">" . $row_carichi['TDTPCA'] . "</td>
                    <td class=\"valori_carichi\">" . $row_carichi['TDAACA'] . "</td>
                    <td class=\"valori_carichi\">" . $row_carichi['TDNRCA'] . "</td>
                    <td class=\"valori_carichi\">" . $row_carichi['TDTOCO'] . "</td>
                    <td class=\"valori_carichi\">" . $row_carichi['TDCOPR'] . "</td>
                    <td class=\"valori_carichi\">" . $row_carichi['TDCOSP'] . "</td>
                  </tr>";
        
        //ordine/dettagli
        $sql_ordine = "SELECT TDCCON, TDDCON, TDOTID, TDOADO, TDONDO, SUM(TDTOCO) AS TDTOCO, TDCOPR, TDCOSP, TDLOCA, TDPROV, TDDOCU
                       FROM {$cfg_mod_Spedizioni['file_testate']}
                       WHERE TDTPLO = '{$row_carichi['TDTPLO']}' AND TDAALO = '{$row_carichi['TDAALO']}' AND TDNRLO = '{$row_carichi['TDNRLO']}' AND TDDTOR = '{$row_carichi['TDDTOR']}'
                         AND TDTPCA = '{$row_carichi['TDTPCA']}' AND TDAACA = '{$row_carichi['TDAACA']}' AND TDNRCA = '{$row_carichi['TDNRCA']}'
                       GROUP BY TDCCON, TDDCON, TDOTID, TDOADO, TDONDO, TDCOPR, TDCOSP, TDLOCA, TDPROV, TDDOCU
                       LIMIT 10";
        
        $stmt_ordine = db2_prepare($conn, $sql_ordine);
        echo db2_stmt_errormsg();
        $result_ordine = db2_execute($stmt_ordine);
        
        while ($row_ordine = db2_fetch_assoc($stmt_ordine)) {

            $primo_ordine = 1;
            
            //ordini
            $row_ordine['TDCCON'] = trim($row_ordine['TDCCON']);
            $row_ordine['TDDCON'] = trim($row_ordine['TDDCON']) . "<br><i><font size=\"6\">" . trim($row_ordine['TDLOCA']). " [" .trim($row_ordine['TDPROV'])."]</font></i>";
            $row_ordine['TDOTID'] = trim($row_ordine['TDOTID']);
            $row_ordine['TDOADO'] = trim($row_ordine['TDOADO']);
            $row_ordine['TDONDO'] = trim($row_ordine['TDONDO']);
            $ordine = $row_ordine['TDOTID'] . '/' . $row_ordine['TDOADO'] . '/' . $row_ordine['TDONDO'];
            $row_ordine['TDTOCO'] = trim($row_ordine['TDTOCO']);
            $row_ordine['TDCOPR'] = trim($row_ordine['TDCOPR']);
            $row_ordine['TDCOSP'] = trim($row_ordine['TDCOSP']);
            
            if ($primo_ordine == 1){
                $html .= "<tr>
                            <th class=\"intestazione\">ORDINE '" . $row_ordine['TDONDO'] . "'</th>
                          </tr>
                          <tr>
                            <th class=\"titoli_cod_cli\">Codice Cliente</th>
                            <th class=\"titoli_rag_soc\">Ragione Sociale / Localit&agrave;</th>
                            <th class=\"titoli_ordine\">Ordine</th>
                            <th class=\"titoli_colli\">Tot.Colli</th>
                            <th class=\"titoli_colli\">Colli Prod.</th>
                            <th class=\"titoli_colli\">Colli Sped.</th>
                          </tr>";
            }
            $html .= "<tr>
                        <td class=\"valori_cod_cli\">" . $row_ordine['TDCCON'] . "</td>
                        <td class=\"valori_rag_soc\">" . $row_ordine['TDDCON'] . "</td>
                        <td class=\"valori_ordine\">" . $ordine . "</td>
                        <td class=\"valori_colli\">" . $row_ordine['TDTOCO'] . "</td>
                        <td class=\"valori_colli\">" . $row_ordine['TDCOPR'] . "</td>
                        <td class=\"valori_colli\">" . $row_ordine['TDCOSP'] . "</td>
                      </tr>
                      <tr>".
                        //<th class=\"titoli_docu\">Documento</th>
                        "<th class=\"titoli_righe\">Riga</th>
                        <th class=\"titoli_cod_art\">Codice Articolo</th>
                        <th class=\"titoli_desc_art\">Descrizione Articolo</th>
                        <th class=\"titoli_quant\">Quantit&agrave;</th>
                      </tr>";
            
            //righe
            $sql_righe = "SELECT  TD.TDDOCU, RD.RDRIGA, RD.RDART, RD.RDDART, RD.RDQTA
                    	  FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                            LEFT JOIN {$cfg_mod_Spedizioni['file_testate']} TD
                                ON TD.TDDT=RD.RDDT AND TD.TDOTID = RD.RDTIDO AND TD.TDOINU = RD.RDINUM AND RD.RDAADO = TD.TDOADO AND TD.TDONDO = RD.RDNRDO
                          WHERE TD.TDDOCU = '{$row_ordine['TDDOCU']}'
                          LIMIT 5
                          ";

            
            $stmt_righe = db2_prepare($conn, $sql_righe);
            echo db2_stmt_errormsg();
            $result_righe = db2_execute($stmt_righe);                       
            
            while ($row_righe = db2_fetch_assoc($stmt_righe)) {
                
                //righe
                $row_righe['TDDOCU']  = trim($row_righe['TDDOCU']);
                $docu = substr($row_righe['TDDOCU'], 10, 12);
                $row_righe['RDRIGA']  = trim($row_righe['RDRIGA']);
                $row_righe['RDART']   = trim($row_righe['RDART']);
                $row_righe['RDDART']  = trim($row_righe['RDDART']);
                $row_righe['RDQTA']   = trim($row_righe['RDQTA']);
                
                $html .= "<tr>".
                           //<td class=\"valori_docu\">" . $docu . "</td> 
                            "<td class=\"valori_righe\">" . $row_righe['RDRIGA'] . "</td>
                            <td class=\"valori_cod_art\">" . $row_righe['RDART'] . "</td>
                            <td class=\"valori_desc_art\">" . $row_righe['RDDART'] . "</td>";
                
                if ($row_righe['RDQTA'] > 1){
                    $html .= "<td class=\"valori_quant\"><b>" . $row_righe['RDQTA'] . "</b></td>
                          </tr>";
                } else {
                    $html .= "<td class=\"valori_quant\">" . $row_righe['RDQTA'] . "</td>
                          </tr>";
                }
            }   //righe
            
            
            $html .= "<tr><td class=\"spacer\"></td></tr>";
            
        }   //ordini
    }   //carichi
}   //lotti

$html .= "</table>
        </body>
    </html>";



$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();
//Close and output PDF document
$pdf->Output('Casa Mid� - TURI', 'I');