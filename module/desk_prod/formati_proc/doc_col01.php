<?php

class FP_DOC_COL01 {
    
    private $main_module;
    private $parametri = array();
    
    function __construct($main_module, $parametri = array()) {
        $this->main_module = $main_module;
        $this->parametri = $parametri;
    }
    
    //in questo caso i record sono i colli
    function exe($ar_record, $parametri = array()){
        global $conn, $cfg_mod_DeskProd;
        $ritime     = microtime(true);
    	//scorrere ar_record (elenco colli) e scrivere file per generazione documenti
        foreach($ar_record as $v){
            $anno_lotto = substr($v['TDAALO'], 2, 2);
            $numero_lotto = sprintf("%06s", $v['TDNRLO']);
            $tipo_lotto = trim($v['TDTPLO']);
            
            $ar_ins = array();
            $ar_ins['WSGINDR'] = $anno_lotto.$numero_lotto.$tipo_lotto;
            $ar_ins['WSGTIDO'] = $parametri['TIP'];
            $ar_ins['WSGTPDO'] = $parametri['DOC'];
            $ar_ins['WSGDTEP'] = $v['ASDTAS'];
            $ar_ins['WSGDTRG'] = oggi_AS_date();
            $ar_ins['WSGAADO'] = date('Y');
            $ar_ins['WSGCCON'] = $parametri['INT'];
            $ar_ins['WSGVSRF'] = "{$tipo_lotto}-{$anno_lotto}-{$numero_lotto}-{$parametri['RIF']}";
            $ar_ins['WSGART']  = $v['PLART'];
            $ar_ins['WSGUM']   = $v['PLUM'];
            $ar_ins['WSGQTA']  = $v['PLQTA'];
            $ar_ins['WSGNOTE4']  = "*PR".sprintf("%015s", strtr($ritime, array("." => "")));
           
            
            $sql = "INSERT INTO {$cfg_mod_DeskProd['file_gen_docu']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
           
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
    	}
    	
    	$sh = new SpedHistory($this->main_module);
    	$ret_RI = $sh->crea(
    	    'pers',
    	    array(
    	        "RITIME"    => $ritime,
    	        "messaggio"	=> 'GEN_DOC_PROD',
    	    )
    	    );
    	
    	   
        return array('success' => true);
    }
    
    
    
    
}