<?php   

require_once "../../config.inc.php";
require_once "acs_calendario_settimanale_spedizioni.php";

function parametri_sql_where($form_values){

	global $s;

	$sql_where.= " AND ".$s->get_where_std();

	$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);

	$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);
	
	$sql_where.= sql_where_by_combo_value('TD.TDCITI', $form_values->f_itinerario);

	return $sql_where;
}

function sum_columns_value(&$ar_r, $r){

	global $s;

	$ar_r['volume'] += $r['TDVOLU'];
	$ar_r['colli'] += $r['TDTOCO'];
	$ar_r['colli_pro'] += $r['RDQTA'];


}

function get_data($anno, $num_set,$gg_set){
	global $id_ditta_default;	
	if(strlen($anno)==0){
		return 0;
	}
	
	$s = new Spedizioni();
	$main_module = new Spedizioni();
	
	global $cfg_mod_Spedizioni, $conn;
	
	$sql="SELECT * FROM {$cfg_mod_Spedizioni['file_calendario']} CS 
		  WHERE CSDT='$id_ditta_default' AND CSCALE ='*CS' AND CSAARG={$anno} AND CSNRSE={$num_set} AND CSGIOR={$gg_set}";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$row = db2_fetch_assoc($stmt);

	return $row['CSDTRG'];
}

function get_itin($lotto, $carico){
	
	$s = new Spedizioni();
	$main_module = new Spedizioni();
	
	global $cfg_mod_Spedizioni, $conn;
	
	$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
	$form_values = json_decode($form_values);
	$sql_where.=parametri_sql_where($form_values);
	
	$sql="SELECT TA_ITIN.TADESC AS ITIN 
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN ON TD.TDDT=TA_ITIN.TADT AND TD.TDCITI=TA_ITIN.TAKEY1 AND TA_ITIN.TATAID='ITIN'
	WHERE TDNRLO={$lotto} AND TDNRCA={$carico} {$sql_where}
	GROUP BY TA_ITIN.TADESC";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$itin=array();
	
	while($row = db2_fetch_assoc($stmt)){				
	
		$itin[] = trim($row['ITIN']);

	}
	
	$itinerari = implode(",", $itin);

	return $itinerari;

}

$s = new Spedizioni();
$main_module = new Spedizioni();

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

//filtro area spediz, divisione e itinerario
$sql_where.=parametri_sql_where($form_values);

//controllo data
if (strlen($form_values->f_data_da) > 0)
	$sql_where .= " AND CS.CSDTRG >= {$form_values->f_data_da}";
if (strlen($form_values->f_data_a) > 0)
	$sql_where .= " AND CS.CSDTRG <= {$form_values->f_data_a}";

//controllo opzione programmato o spedizione

	$tipo_data="TDDTEP";
	$titolo= "Produzione/Evasione";
	$order=" CS.CSAARG, CS.CSNRSE, CS.CSGIOR, TDNRLO, TDNRCA";
	$carico= "TDNRCA AS COL2";
	$lotto= "TDNRLO AS COL1";
	$head1="Lotto";
	$head2="Carico";
	
		
if ($form_values->f_solo_ordini_con_carico == 'Y')
	$sql_where .= " AND TD.TDFN01 = 1 ";


$ar = array();

$sql = "SELECT {$carico}, {$lotto}, CS.CSAARG, CS.CSNRSE, CS.CSGIOR, SUM(TDVOLU) AS TDVOLU, TDNRCA, TDNRLO, PSDESC, SUM(TDTOCO) AS TDTOCO, SUM(RDQTA) AS RDQTA, {$tipo_data}, MIN(TA_VETT.TADESC) AS VETTORE, MIN(TA_MEZZO.TADESC) AS MEZZO, TDASPE
        FROM {$cfg_mod_Spedizioni['file_testate']} TD
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.{$tipo_data}
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR' 
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_MEZZO ON TA_MEZZO.TADT = SP.CSDT AND TA_MEZZO.TAKEY1 = SP.CSCAUT AND TA_MEZZO.TATAID = 'AUTO'
        LEFT OUTER JOIN (
        	            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
        	            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
        	            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
        	            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
        	            ) RD
        	            ON TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO
         LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']}
                        ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA 
        WHERE TDSWSP= 'Y' {$sql_where}
        GROUP BY CS.CSAARG, CS.CSNRSE, CS.CSGIOR, TDNRCA, TDNRLO, {$tipo_data}, PSDESC, TDASPE
        ORDER BY {$order}";


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


while ($row = db2_fetch_assoc($stmt)) {

	$tmp_ar_id = array();
	$ar_r= &$ar;

	//stacco dei livelli
	$anno_set= array();
	$anno_set[0]=trim($row['CSNRSE']);
	$anno_set[1]=trim($row['CSAARG']);
	$cod_liv0 = implode("_", $anno_set); //NUMERO SETTIMANA_ANNO
	$cod_liv0_aspe = trim($row['TDASPE']); 
	$cod_liv1 = trim($row['CSGIOR']); //NUMERO GIORNO
	
	//SETTIMANA_ANNO
	$liv =$cod_liv0;
	$tmp_ar_id[] = $liv;
	if (!isset($ar_r["{$liv}"])){
		$ar_new = array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['anno'] = trim($row['CSAARG']);
		$ar_new['set'] = trim($row['CSNRSE']);
		$ar_new['task'] = $liv;
		$ar_r["{$liv}"] = $ar_new;
	}
	$ar_r = &$ar_r["{$liv}"];
	sum_columns_value($ar_r, $row);
	
	//AREA SPEDIZIONE
	$liv =$cod_liv0_aspe;
	$ar_r = &$ar_r['children'];
	$tmp_ar_id[] = $liv;
	if (!isset($ar_r["{$liv}"])){
	    $ar_new = array();
	    $ar_new['id'] = implode("|", $tmp_ar_id);
	    $d_area_spe = "[".trim($row['TDASPE'])."] ".$s->decod_std('ASPE', $row['TDASPE']);
	    $ar_new['task'] = $d_area_spe;
	    $ar_new['set'] = trim($row['CSNRSE']);
	    
	    $ar_r["{$liv}"] = $ar_new;
	}
	$ar_r = &$ar_r["{$liv}"];
	sum_columns_value($ar_r, $row);

	//GIORNO
	$liv=$cod_liv1;
	$ar_r = &$ar_r['children'];
	$tmp_ar_id[] = $liv;
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['task'] = $liv;
		$ar_new['data'] = trim($row[$tipo_data]);
		$ar_new['TDASPE'] = $row['TDASPE'];
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];
	sum_columns_value($ar_r, $row);
	$ar_r = &$ar_r['children'];
	$ar_r[] = $row;
	

}



?>

<html>
 <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.header_page h2{font-size: 16px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%; }
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{padding-top: 10px;border: 2pt solid gray; font-size: 17px;}   
   tr.liv3 td{font-weight: bold; font-size: 0.9em;}
   tr.livb th,tr.livb td{font-weight: bold; background-color: #DDDDDD;}   
   tr.liv_data th{background-color: #333333; color: white;}
   span.dw {font-size: 50px;}
   tr.sep{border-top: 2pt solid gray;}
   tr.cors td{font-style: italic; font-size: 9px;}
   table.int1 td.ff{font-size: 17px;}
   
   tr.tr-cli td{font-weight: bold;}
   .dx{
   text-align:right;
   }
  
   table.int0{margin-bottom: 5px;}
   table.int0 td{border: 0px;}
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
   
   div.with_todo_tooltip{text-align: right;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   table.int1{border: 2pt solid gray;}
  
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
  
  </script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';  
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<?php

echo "<div id='my_content'>";

echo "<br>";
echo "<br>";

echo "<table class=int1>";


foreach($ar as $kar => $r){

	echo "<tr class=\"liv2\"><td colspan=9>
		<div style=\"text-align: right; font-size:15px; font-weight: bold;\">settimana ".$r['task']."</div>
		<table class=int0><tr>
		<td colspan=8 style=\"font-weight:bold;\"> Riepilogo programmi per data ".$titolo."</td>
		<td style=\"text-align: right; vertical-align:bottom; font-size:13px; \">Data elaborazione: " .  Date('d/m/Y H:i') . "</td>
		</tr></table>
		</td></tr>";
	
	
	    
	
	    for($i=1; $i<=5; $i++){
	        
	        
	        //echo "<br>a) ".$form_values->f_data_da;
	        //echo " b) " . $date;
	        //echo " c) " . $form_values->f_data_a;
	        
	        //prendere la selezione dei giorni impostati nel form non la settimana intera
	        $date=get_data($r['anno'], $r['set'], $i);
	        
	        if (strlen($form_values->f_data_da) > 0 && (int)$form_values->f_data_da > (int)$date)
	            continue;
	            if (strlen($form_values->f_data_a) > 0 && (int)$form_values->f_data_a < (int)$date)
	                continue;
	                
	                
	                switch($i){
	                    case 1: $g="Luned�";
	                    break;
	                    case 2: $g="Marted�";
	                    break;
	                    case 3: $g="Mercoled�";
	                    break;
	                    case 4: $g="Gioved�";
	                    break;
	                    case 5: $g="Venerd�";
	                    break;
	                }
	                
	                echo    "<tr class=\"livb sep\">
						<th>Data</th>
						<th>".$head1."</th>
						<th>".$head2."</th>
						<th>Zona spedizione</th>
						<th>Vettore</th>
						<th>Mezzo</th>
						<th>Volume</th>
						<th>Colli</th>
						<th style=width:5%;>Colli prod.</th>
	  				    </tr>";
	                
	                $count = 0;
	                foreach($r['children'] as $kar1 => $r1){
	                    $count ++;
	                    
	                if(isset($r1['children'][$i])) {
	                    
	                    echo "<tr>
	                    <td colspan=6 style=font-weight:bold;>{$r1['task']}</td>
	                    
	                    <td class=number style=font-weight:bold;>".n($r1['volume'], 2)."</td>
        			    <td class=number style=font-weight:bold;>".n_auto($r1['colli'])."</td>
        		        <td class=number style=font-weight:bold;>".n_auto($r1['colli_pro'])."</td>
        		        </tr>";
	                    
	                    
	                    $ultimo_lotto="ccccc";
	                    
	                    $rowspan=1;
	                    $rowspan += count($r1['children'][$i]['children']);
	                    
	                    //conto il numero totale di righe
	                    foreach($r1['children'][$i]['children'] as $kar2 => $r2){
	                        if ($form_values->f_dettaglio_destinazioni == 'Y'){
	                            $sql= "SELECT COUNT(DISTINCT CONCAT(TDDCON, CONCAT(TDDLOC, CONCAT(TDPROD, TDDNAD)))) AS RIGHE
	                            FROM {$cfg_mod_Spedizioni['file_testate']} TD
	                            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	                            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
	                            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_MEZZO ON TA_MEZZO.TADT = SP.CSDT AND TA_MEZZO.TAKEY1 = SP.CSCAUT AND TA_MEZZO.TATAID = 'AUTO'
	                            WHERE TDNRLO ='{$r2['TDNRLO']}' AND TDNRCA= '{$r2['TDNRCA']}'
	                            AND {$tipo_data} = {$date}
	                            ";
	                            
	                            $stmt = db2_prepare($conn, $sql);
	                            echo db2_stmt_errormsg();
	                            $result = db2_execute($stmt);
	                            
	                            $row1 = db2_fetch_assoc($stmt);
	                            $righe=$row1['RIGHE'];
	                            $rowspan += $righe;
	                        }
	                    }
	                    
	                    
	                    
	                    
	                    foreach($r1['children'][$i]['children'] as $kar2 => $r2){
	                        
	                        
	                        if ($kar2 == 0)
	                            echo "<tr class=sep>";
                            else
                                echo "<tr>";
	                                
	                                setlocale(LC_TIME, $oldLocale);
	                                $g_n= acs_u8e(strftime("%d", strtotime($r1['children'][$i]['data']))); //GIORNO
	                                setlocale(LC_TIME, 'it_IT');
	                                $g_m= acs_u8e(strftime("%B", strtotime($r1['children'][$i]['data']))); //MESE
	                                
	                                
	                                if ($kar2 == 0)
	                                    echo "<td rowspan=".$rowspan.">".acs_u8e($g)." <span class=dw><br>".$g_n."</span><br>".acs_u8e(ucfirst($g_m))."</td>";
	                                    
	                                    
	                                    
	                                    
	                                    if($ultimo_lotto!=trim_utf8($r2['COL1']))
	                                        $lotto=trim_utf8($r2['COL1']);
	                                        else $lotto = "";
	                                        
	                                        if(strlen(trim($r2['PSDESC']))==0){
	                                            $desc_carico= $r2['PSDESC'];
	                                        }else{
	                                            $desc_carico= $r2['PSDESC']."<br>";
	                                        }
	                                        
	                                        echo "<td class=ff style=font-weight:bold;>".$lotto."</td>
                              <td class=ff>".$r1['COL2']."</td>
	   		          		  <td>".$desc_carico."(".get_itin($r2['TDNRLO'], $r2['TDNRCA']).")</td>
							  <td>".trim_utf8($r2['VETTORE'])."</td>
							  <td>".trim_utf8($r2['MEZZO'])."</td>
							  <td class=number>".n($r2['TDVOLU'], 2)."</td>
							  <td class=number>".n_auto($r2['TDTOCO'])."</td>
							  <td class=number>".n_auto($r2['RDQTA'])."</td>";
	                                        
	                                        echo "</tr>";
	                                        
	                                        if ($form_values->f_dettaglio_destinazioni == 'Y'){
	                                            $sql= "SELECT TDDCON, TDDLOC, TDPROD, TDNAZD, SUM(TDVOLU) AS VOLUME, SUM(TDTOCO) AS COLLI, SUM(RDQTA) AS COLLI_PROD
	                                            FROM {$cfg_mod_Spedizioni['file_testate']} TD
	                                            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
	                                            LEFT OUTER JOIN (
	                                            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
	                                            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
	                                            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
	                                            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
	                                            ) RD
	                                            ON TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO
	                                            WHERE TDNRLO ='{$r2['TDNRLO']}' AND TDNRCA= '{$r2['TDNRCA']}'
	                                            AND {$tipo_data} = {$date}
	                                            GROUP BY TDDCON, TDDLOC, TDPROD, TDNAZD";
	                                            
	                                            $stmt = db2_prepare($conn, $sql);
	                                            echo db2_stmt_errormsg();
	                                            $result = db2_execute($stmt);
	                                            
	                                            while ($row1 = db2_fetch_assoc($stmt)) {
	                                                
	                                                
	                                                if (strlen(trim($row1['TDPROD']))==0){
	                                                    $loc=trim($row1['TDNAZD']);
	                                                }else{
	                                                    $loc=trim($row1['TDPROD']);
	                                                }
	                                                
	                                                echo "<tr class=cors><td colspan=3>&nbsp;</td>
                              <td>".trim_utf8($row1['TDDCON'])."</td>
							  <td>".trim_utf8($row1['TDDLOC'])." [".$loc."]</td>
							  <td class=number>".n($row1['VOLUME'], 2)."</td>
							  <td class=number>".n_auto($row1['COLLI'])."</td>
							  <td class=number>".n_auto($row1['COLLI_PROD'])."</td></tr>";
	                                            }
	                                            
	                                        }
	                                        
	                                        $ultimo_lotto=trim_utf8($r2['COL1']);
	                                        
	                    }
	                    
	                    echo "<tr><td colspan=4>&nbsp;</td>
   								<td style=font-weight:bold;>Totale giorno</td>
								<td class=number style=font-weight:bold;>".n($r1['children'][$i]['volume'], 2)."</td>
								<td class=number style=font-weight:bold;>".n_auto($r1['children'][$i]['colli'])."</td>
		   						 <td class=number style=font-weight:bold;>".n_auto($r1['children'][$i]['colli_pro'])."</td>
	        
   								</tr>";
	                    
	                } else {
	                    
	                    if($count == 1){
    	                    $data=get_data($r['anno'], $r['set'], $i);
    	                    setlocale(LC_TIME, $oldLocale);
    	                    $g_n1= acs_u8e(strftime("%d", strtotime($data))); //GIORNO
    	                    setlocale(LC_TIME, 'it_IT');
    	                    $g_m= acs_u8e(strftime("%B", strtotime($data))); //MESE
    	                    echo "<tr  class=sep>";
    	                    echo "<td>".acs_u8e($g)." <span class=dw><br>".$g_n1."</span><br>".acs_u8e(ucfirst($g_m))."</td>";
    	                    echo "<td colspan=8>&nbsp;</td>";
    	                    echo "</tr>";
	                    }
	                    
	                }
	                
	    }
	
	
	
	
	
	
	
	
	
	
	}

			
			
			
	
			echo "<tr class=\"sep livb\">
			<td colspan=6 style=font-weight:bold;>Totale generale</td>
		    
			<td class=number style=font-weight:bold;>".n($r['volume'], 2)."</td>
			<td class=number style=font-weight:bold;>".n_auto($r['colli'])."</td>
		    <td class=number style=font-weight:bold;>".n_auto($r['colli_pro'])."</td>
		    </tr>";
			echo "<tr style=\"page-break-after: always;\"></tr> ";
			
		//echo "<div class=\"page-break\"></div>";
}	

echo "</table>";

?>

</div>
</body>
</html>
