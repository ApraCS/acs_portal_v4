<?php

require_once("../../config.inc.php");
require_once("../base/_rilav_g_history_include.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$desk_art = new DeskArt(array('no_verify' => 'Y'));





// ******************************************************************************************
// CREA PROGETTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_create_progetto'){

    $codice_progetto = $m_params->codice_progetto;
    $lotto = $m_params->lotto;
    
    $ar_memo = array();
    foreach($m_params->list_memo as $v){
        $ar_value = explode('_', $v->memo);
        $voce = $ar_value[0];
        $memo = $ar_value[1];
        $ar_memo[$voce] = $memo;
    }
   
    //ToDo: takey1 lungo 10: sufficiente?
    //$prog_progetto = trim($lotto) . trim($codice_progetto);
    $prog_progetto = $m_params->prog_progetto;    
  
    //Genero tabella TA0 - ARTPJ (Tabella Progetti)
    $ar_ins = array();
    $ar_ins['TADT'] = $id_ditta_default;
    $ar_ins['TATAID'] = $cfg_mod['taid_progetto'];
    $ar_ins['TADTGE'] = oggi_AS_date();
    $ar_ins['TAORGE'] = oggi_AS_time();
    $ar_ins['TAUSGE'] = $auth->get_user();
    $ar_ins['TAKEY1'] = $prog_progetto;
    $ar_ins['TAMAIL'] = implode("_", array(trim($m_params->lotto), trim($m_params->codice_progetto)));
    $ar_ins['TADESC'] = $m_params->form_values->f_descrizione;
    
    // TODO : DATA EVASIONE E SEQUENZA, SCEGLIERE CAMPI CORRETTI
    $ar_ins['TAVOLU'] = $m_params->form_values->f_data;
    $ar_ins['TAASPE'] = $m_params->form_values->f_sequenza;
    
    $sql = "INSERT INTO {$cfg_mod['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    //inserimento attivita' di livello 0 (START)
    foreach($m_params->list_selected_id as $v){
        
        //a livello START non ho attivita, perche rappresentano i kioski
        // quindi passo direttamente alle sottovoci        
        
        //recupero attivita' da sottolivello
        
        //per ogni sottolivello con ATTAV
        //creao una nuova ToDo collegata a quella START (f_prog_coll)
        
        $sql_ar = array();
        
        $sql_ar['select'][] = 'CD.*';
        $m_table_config = array( 'f_ditta' => 'CDDT', 'field_IDPR' => 'CDIDPR');
        
        $_rilav_H_for_query = _rilav_H_for_query(
            $m_table_config,
            $cfg_mod['file_cfg_distinte'],
            $cfg_mod['file_tabelle'],
            'CD',
            'CVOUT',
            array('CV')
            );
        $sql_ar['select'][] = $_rilav_H_for_query['select'];
        $sql_ar['joins'][] = $_rilav_H_for_query['joins'];
        
        
        //H_CV_FILE.AHCARI as H_CV_COD
        $sql_WHERE = '';
        $sql_WHERE.= sql_where_by_combo_value('H_CV_FILE.AHCARI', $m_params->form_values->f_stato);
        
        $sql_f = "SELECT " . implode(",", $sql_ar['select']) . "
                  FROM {$cfg_mod['file_cfg_distinte']} CD
                  " . implode(",", $sql_ar['joins']) . "
                  WHERE CDDT= '{$id_ditta_default}' AND CDDICL = '{$codice_progetto}' {$sql_WHERE}
                  AND CDTODO <> '' AND CDCMAS = '{$v->voce}' ORDER BY CDSEQI";
        
        $stmt_f = db2_prepare($conn, $sql_f);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_f);  
        
        while ($row = db2_fetch_assoc($stmt_f)){
            
            
            $voce = trim($row['CDSLAV']);
            
            $to_form_values = array();
            $to_form_values['f_causale'] = $row['CDTODO'];
            $to_form_values['f_note'] = $v->note;
            $to_form_values['f_utente_assegnato'] = $auth->get_user();
            $to_form_values['f_attiva_dal'] = oggi_AS_date();
            // $to_form_values['f_scadenza'] = $v->data_format;
            $to_form_values['f_progetto'] = $prog_progetto;
            
            
            $to_form_values['f_ASORD1']     = $m_params->codice_progetto;     //Indice
            $to_form_values['f_ASORD2']     = $v->voce;                       //Kiosk (START o fascicolo)
            $to_form_values['f_ASORD3']     = $voce;                          //Report (lista, etc..)
            $to_form_values['f_ASORD4']     = $row['CDIDPR'];                 //id assoluto progressivo config. distinta
            
            $na = new SpedAssegnazionePrProd();
            $as_prog_f = $na->crea('POSTM', array(
                'k_ordine' => $m_params->lotto,
                'form_values' => $to_form_values
            ));
            
            $ar_ins_nt1 = array();
            $ar_ins_nt1['NTMEMO'] = $row['CDDESC'];
            $ar_ins_nt1['NTDT']   = $id_ditta_default;
            $ar_ins_nt1['NTKEY1'] = $as_prog_f;
            $ar_ins_nt1['NTKEY2'] = $row['CDSLAV'];
            $ar_ins_nt1['NTSEQU'] = 0;
            $ar_ins_nt1['NTTPNO'] = 'PRJC5';
            
            $sql_nt1 = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                    (" . create_name_field_by_ar($ar_ins_nt1) . ")
                   VALUES (" . create_parameters_point_by_ar($ar_ins_nt1) . ")";
            
            $stmt_nt1 = db2_prepare($conn, $sql_nt1);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_nt1, $ar_ins_nt1);
            echo db2_stmt_errormsg();
            
            
            
            //creo le note standard per il progetto
            $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
            $chiave = implode ("|", $ar_comp);
            $row_nt_f = $desk_art->get_note_indici($chiave);
            if($row_nt_f != false){
                $ar_ins_nt_f = array();
                $ar_ins_nt_f['NTMEMO'] = $row_nt_f['NTMEMO'];
                $ar_ins_nt_f['NTDT']   = $id_ditta_default;
                $ar_ins_nt_f['NTKEY1'] = $as_prog_f;
                $ar_ins_nt_f['NTKEY2'] = $chiave;
                $ar_ins_nt_f['NTSEQU'] = 0;
                $ar_ins_nt_f['NTTPNO'] = 'ASPR5';
                
                $sql_nt_f = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                (" . create_name_field_by_ar($ar_ins_nt_f) . ")
                   VALUES (" . create_parameters_point_by_ar($ar_ins_nt_f) . ")";
                $stmt_nt_f = db2_prepare($conn, $sql_nt_f);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_nt_f, $ar_ins_nt_f);
                echo db2_stmt_errormsg();
            }
            
            //creo le note personalizzate per il progetto
            $ar_ins_memo_f = array();
            $ar_ins_memo_f['NTMEMO'] = $ar_memo["{$voce}"];
            $ar_ins_memo_f['NTDT']   = $id_ditta_default;
            $ar_ins_memo_f['NTKEY1'] = $as_prog_f;
            $ar_ins_memo_f['NTSEQU'] = 0;
            $ar_ins_memo_f['NTTPNO'] = 'ASME5';
            
            $sqlMemo_f =  "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                          (" . create_name_field_by_ar($ar_ins_memo_f) . ")
                          VALUES (" . create_parameters_point_by_ar($ar_ins_memo_f) . ")";
            
            $stmtMemo_f = db2_prepare($conn, $sqlMemo_f);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtMemo_f, $ar_ins_memo_f);
            echo db2_stmt_errormsg();
        
        }
    }
 
    
    
    //Duplica dati LOTTO su file replica
    $lotto_ar = explode('_', $lotto); //LP_2020_16_<ditta>
    $sh = new SpedHistory($main_module); //->WPI5RI0
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'DUP_LOTTO',
            "vals" => array(
                'RITPLO' => $lotto_ar[0],
                'RIAALO' => $lotto_ar[1],
                'RINRLO' => $lotto_ar[2],
                ////'RIDTOR' => $lotto_ar[3], //Ditta origine - Ora ho l'area di spedizione
                'RIFG01' => $m_params->form_values->f_rigenera,
            )
        )
        );
    
    
    
    echo acs_je(array('success' => true, 'prog_progetto' => $prog_progetto));
    exit;
}


//----------------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data_indici'){
//----------------------------------------------------------------------------
    
    $codice_progetto = $m_params->codice_progetto;
    
    if($m_params->dettaglio == 'Y'){
        
        $sql_s = "SELECT * FROM {$cfg_mod['file_cfg_distinte']} CD
        LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_ATTAV
        ON CD.CDDT = TA_ATTAV.TADT AND CD.CDTODO = TA_ATTAV.TAKEY1 AND TATAID = 'ATTAV'
        WHERE CDDT= ? AND CDDICL = ?
        AND CDCMAS = ? ORDER BY CDSEQI";
        
        $stmt_s = db2_prepare($conn, $sql_s);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_s, array($id_ditta_default, $codice_progetto, trim($m_params->kiosk)));
        while ($row_s = db2_fetch_assoc($stmt_s)) {
            if($row_s != false)
                $nr2 = array();
                $nr2['d_todo'] = "[".trim($row_s['CDTODO'])."] ".trim($row_s['TADESC']);
                $nr2['todo'] = trim($row_s['CDTODO']);
                $nr2['voce']  = trim($row_s['CDSLAV']);
                $nr2['desc'] = trim($row_s['CDDESC']);
               
                $ar_comp2 = array(trim($row_s['CDDT']), trim($row_s['CDDICL']), trim($row_s['CDCMAS']), trim($row_s['CDSEQU']), trim($row_s['CDSLAV']));
                $nr2['chiave'] = implode ("|", $ar_comp2);
                $nr2['nota'] =  utf8_decode($desk_art->has_nota_indici($nr2['chiave']));
                $row_nt2 = $desk_art->get_note_indici($nr2['chiave']);
                $nr2['t_nota'] = $row_nt2['NTMEMO'];
                
                $ar[] = $nr2;
        }
        
        
    }else{
        
        //recupero le attivita' (con Todo) legate all'indice
        $sql = "SELECT * FROM {$cfg_mod['file_cfg_distinte']} CD
        LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_ATTAV
        ON CD.CDDT = TA_ATTAV.TADT AND CD.CDTODO = TA_ATTAV.TAKEY1 AND TATAID = 'ATTAV'
        WHERE CDDT= ? AND CDDICL = ?
        AND CDCMAS = 'START'
        ORDER BY CDSEQI";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($id_ditta_default, $codice_progetto));
        echo db2_stmt_errormsg();
        
        $ar = array();
        while ($row = db2_fetch_assoc($stmt)) {
            $nr = array();
            $nr['d_todo'] = "[".trim($row['CDTODO'])."] ".trim($row['TADESC']);
            $nr['todo'] = trim($row['CDTODO']);
            $nr['utente'] = trim($auth->get_user());
            $nr['voce'] = trim($row['CDSLAV']);
            $nr['gruppo'] = trim($row['CDCMAS']);
            $nr['desc'] = trim($row['CDDESC']);
            $nr['check'] = 'Y';
            
            $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
            $nr['chiave'] = implode ("|", $ar_comp);
            $nr['nota'] =  $desk_art->has_nota_indici($nr['chiave']);
            $row_nt = $desk_art->get_note_indici($nr['chiave']);
            $nr['t_nota'] = utf8_decode($row_nt['NTMEMO']);
            $ar[] = $nr;
            
        }
        
        
    }
    
    
    echo acs_je($ar);
    exit();
    
}


//-----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//-----------------------------------------------------------------------
    if($m_params->dettaglio != 'Y'){
    //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    $ar_users = $users->find_all();
    
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");        
        $ar_users_json = acs_je($user_to);     
        
        $progetto = "[{$m_params->codice_progetto}] ".trim($m_params->desc_progetto);
        $prog_progetto = $s->next_num('PRPPJ');
        $codice = "[#{$prog_progetto}] ".implode("_", array($m_params->lotto, $m_params->codice_progetto));
        
        $lotto_ar = explode('_', $m_params->lotto);
        
        $sql = "SELECT COUNT(*) AS C_ROWS
        FROM {$cfg_mod['file_richieste']}
        WHERE RIESIT = 'C' AND RIRGES = 'DUP_LOTTO'
        AND RITPLO = '{$lotto_ar[0]}' AND RIAALO = '$lotto_ar[1]' AND RINRLO = '$lotto_ar[2]' AND RIDTOR = '$lotto_ar[3]'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt);
        $r = db2_fetch_assoc($stmt);
        
    }
    
    $stmt = _rilav_H_stmt_causali_avanzamento($cfg_mod['file_tabelle'], 'CVOUT', 'CV');
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $rr = array( "id" 	=> acs_u8e($row['TAKEY2']),
                     "text" =>  "[". trim($row['TAKEY2']) ."] ". acs_u8e(trim($row['TADESC'])),
          
        );
        
        $ret[] = $rr;
    }
    
        
   ?>     
{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            flex: 1,
	            layout: {type: 'vbox', pack: 'start', align: 'stretch'},
	            items: [
	            
	            <?php  if($m_params->dettaglio != 'Y'){?>
	            
	            <?php if($r['C_ROWS'] > 0){ ?>
	               {
					xtype: 'checkboxgroup',
					fieldLabel: '<b>Attenzione lotto gi&agrave; generato. Rigenerarlo?</b>',
					labelSeparator : '',
					labelWidth : 300,
				    items: [
				     {
						xtype: 'checkbox',
						name: 'f_rigenera',
						boxLabel: '',
						inputValue: 'Y',
					   							
					}
				    
				    ]},
	            <?php }?>
	            
	               {
						xtype: 'displayfield',
						fieldLabel: 'Codice',
						labelWidth : 120,
        			    value : <?php echo j($codice); ?>
					   							
					}, 
	            
	          		{
						xtype: 'displayfield',
						fieldLabel: 'Descrizione',
						labelWidth : 120,
						value : <?php echo j($progetto); ?>
					}, 
					   
					 {
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
							anchor: '-15',
				 	        layout: 'hbox',
				 	        items: [				 	       
				 	           {
        						xtype: 'textfield',
        						fieldLabel: 'Segnalazioni',
        						labelWidth : 120,
        						flex : 1,
        						name: 'f_descrizione',
        						maxLength : 50,
        						forceSelection: true,	
                			    allowBlank: true
        					   							
        					}, 
					 	      <?php echo_je(a15(n_o(extjs_datefield('f_data', 'Evasione programmata', $m_params->data_eva, array('labelWidth' => 130, 'labelAlign' => 'right', 'flex' => 1))))); ?>		            		            		     	            		            		            
							]
						},
						
						{
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
				 	        layout: 'hbox',
				 	        items: [
				 	        	{
            					xtype: 'combo',
            					name: 'f_stato',
            					labelWidth : 120,
            					flex : 1,
            					fieldLabel: 'Stato attivit&agrave;',
            					displayField: 'text',
            					valueField: 'id',
            					emptyText: '- seleziona -',
            					forceSelection: true,
            				   	allowBlank: true,		
            				    multiSelect: true,	
            				    value : 'ATT',					   													
            					store: {
            						autoLoad: true,
            						editable: false,
            						autoDestroy: true,	 
            					    fields: [{name:'id'}, {name:'text'}],
            					    data: [								    
            						     <?php echo acs_ar_to_select_json($ret, ""); ?>
            						    ]
            					}						 
            				 }	,
            				 
            				 {
            					xtype: 'combo',
            					name: 'f_sequenza',
            					labelWidth : 130,
            					fieldLabel: 'Sequenza',
            					displayField: 'text',
            					valueField: 'id',
            					flex : 1,
            					labelAlign : 'right',
            					emptyText: '- seleziona -',
            					forceSelection: true,
            				   	allowBlank: true,		
            				    multiSelect: true,	
            					store: {
            						autoLoad: true,
            						editable: false,
            						autoDestroy: true,	 
            					    fields: [{name:'id'}, {name:'text'}],
            					    data: [								    
            						     <?php echo acs_ar_to_select_json($main_module->find_TA_std('PRIOL', null,'N', 'N', null, null, null, 'N', 'Y'), ""); ?>
            						    ]
            					}						 
            				 }	
				 	        
				 	        ]}   
						,
					
					<?php }?>
	            
					 { 	xtype: 'grid',
				  		loadMask: true,	
				  		flex : 1,
				  		anchor: '-5',
				  		multiSelect: true,
				  		autoScroll : true,
				  		<?php if($m_params->dettaglio != 'Y'){?>
				  		plugins: [
        		          Ext.create('Ext.grid.plugin.CellEditing', {
        		            clicksToEdit: 1
        		            })
        		      	],
				  		selModel: {selType: 'checkboxmodel', checkOnly: true},
				  		<?php }?>
        		        store: {
							xtype: 'store',
							autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_indici', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
										codice_progetto : <?php echo j($m_params->codice_progetto); ?>,
										dettaglio : <?php echo j($m_params->dettaglio); ?>,
										kiosk : <?php echo j($m_params->kiosk); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['gruppo', 'f_giallo', 'check', 'todo', 'd_todo', 'memo', 'nota', 't_nota', 'desc', 'voce', 'chiave']							
									
					},
					
					<?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
		
				columns: [	
				         {
			                header   : 'Kiosk',
			                dataIndex: 'voce', 
			                width : 50,
			             },{
			                header   : 'Descrizione',
			                dataIndex: 'desc', 
			                flex: 1
			             },{text: '<?php echo $nota; ?>', 	
            				width: 25,
            				align: 'center', 
            				dataIndex: 'nota',
            				tooltip: 'Note',		        			    	     
            		    	renderer: function(value, p, record){
        						   if(!Ext.isEmpty(record.get('t_nota')))
        						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_nota')) + '"';    		    	
            		    	
            		    	       if(record.get('nota') > 0)
            		    			 return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
            		    		   else
            		    		     return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
        		    		  }
            		        }, {
			                header   : 'Segnalazioni',
			                dataIndex: 'memo', 
			                width : 130,
			                editor: {
            	                xtype: 'textfield',
            	                allowBlank: true
            	            }
			             },
			             {
			                header   : 'Tipo attivit&agrave;',
			                dataIndex: 'd_todo', 
			                width : 170,
			             }
		    			 
		    			 , <?php echo dx_mobile() ?>
		    		
		    		
				],
				
				listeners: {
                 
                    /*afterlayout : function(comp) {
                      var sm = comp.getSelectionModel();
        		      sm.selectAll(true);
                        
                    },*/
                  	beforeselect: function(grid, record, index, eOpts) {
						
            			if (record.get('check') =='N') {//replace this with your logic.
               		         // Ext.Msg.alert('Message', 'articolo bloccato o gi&agrave; ordinato');
               		         //alert('ToDo mancante');
               				 return false;
               				 
            			}
       				 } 
       				  <?php  if($m_params->dettaglio != 'Y'){?>
       				 ,itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  													  
						    var voci_menu = [];
						 	 voci_menu.push({
			         		text: 'Dettaglio lista attivit&agrave;',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
					    		  acs_show_win_std('Dettaglio lista attivit&agrave;',
				    				  'acs_panel_indici_nuovo_progetto.php?fn=open_form', {
				    				  	codice_progetto: <?php echo j($m_params->codice_progetto); ?>,
				    					kiosk: rec.get('voce'),
				    					dettaglio : 'Y'
			    				   	}, 600, 400, null, 'iconFabbrica');
				                }
			    		   });
			    		 	 
			    		
			    		   var menu = new Ext.menu.Menu({
				            items: voci_menu
					        }).showAt(event.xy);	
						  
						  }
						  <?php }?>
                }
				
				,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
						       if (record.get('check') =='N')
						           return ' colora_riga_rosso';
						       if (record.get('f_giallo')=='Y')
						          return ' colora_riga_giallo';			           		
						       
						       return '';																
					         },   
					    },
			
											    
				    		
                    }, //grid
				
	            ],
	            
	            <?php  if($m_params->dettaglio != 'Y'){?>
	            
				buttons: [	
				{
			            text: 'Conferma GENERAZIONE Programma',
				        iconCls: 'icon-button_blue_play-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                var win = this.up('window'),
			            		form = this.up('form').getForm(),
			            		grid = this.up('window').down('grid'),
			            		id_selected = grid.getSelectionModel().getSelection();
    			     		list_selected_id = [];
    			     		list_memo = [];
    			     		for (var i=0; i<id_selected.length; i++){
    			     		    if(id_selected[i].get('gruppo') == 'START') 
    			     				list_selected_id.push(id_selected[i].data);
    			     			if(id_selected[i].get('f_giallo') == 'Y'){	
    			     			   var voce_memo = id_selected[i].get('voce').trim() +'_' +id_selected[i].get('memo').trim()
    			     			   list_memo.push({memo : voce_memo});
    			     			 }
    						}
    						if (form.isValid()){
    						
    						var my_listeners = {
        					  afterSincro: function(from_win){
        						Ext.Ajax.request({
        							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_create_progetto',
        					        jsonData: {
        					        	codice_progetto: <?php echo j($m_params->codice_progetto); ?>,
        					        	lotto:  <?php echo j($m_params->lotto); ?>,
        					        	prog_progetto : <?php echo j($prog_progetto); ?>,
        					        	form_values: form.getValues(),
        					        	list_selected_id : list_selected_id,
        					        	list_memo :  list_memo
        					        	},
        					        method     : 'POST',
        					        waitMsg    : 'Data loading',
        					        success : function(result, request){
        					            var jsonData = Ext.decode(result.responseText);
        					             if(jsonData.success == false){
        				      	    		 acs_show_msg_error('Errore in creazione progetto');
        						        	 return;
        			        			  }else{
        			        			     win.close(); 
        			        			     from_win.down('grid').getStore().load();
        			        			    
        			        			    Ext.Msg.show({
                                               title:'Message',
                                               msg: 'Creato nuovo progetto di codifica base dati',
                                               buttons: Ext.Msg.OK,
                                               icon: Ext.MessageBox.INFO
                                            });
                                             
        			        			  }
        					        	   										        	
        					        }
        					    });	
    						
        						
        						}
    						   
    						};
    						
    						
    						 	acs_show_win_std('Sincronizzazione', '../base/acs_submit_job.php?fn=open_form', {
            	            	file_tabelle : <?php echo j($cfg_mod); ?>, 
                   				RIRGES: 'DUP_LOTTO',
                   				} , 650, 250, my_listeners, 'icon-listino'); 
    						
    						       			
	            			}		                   	                	                
	            		}
			        }
		        
		        
		        ]     
		        
		        <?php }?>     
	            
	           
	}
		
	]}
    
<?php     
exit;
}