<?php 
	require_once("../../config.inc.php");
	$_module_descr = "Desktop Programmi di produzione";
			
	$main_module = new DeskProd();
	
	$users = new Users;
	$ar_users = $users->find_all();
	
	$ar_email_json = acs_je($ar_email_to);	
		
?>
<html>
<head>
<title>ACS Portal_DPrP</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />

<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/extensible-all.css" />
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/calendar.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css?20200430"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">
</style>

<script type="text/javascript" src="../../../extjs/ext-all.js"></script>

<script type="text/javascript" src="../../js/extensible-1.5.2/lib/extensible-all-debug.js"></script>
<script type="text/javascript" src="../../js/extensible-1.5.2/src/locale/extensible-lang-it.js"></script>


<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>
<script src=<?php echo acs_url("js/acs_js.js") ?>></script>

<script type="text/javascript">


    var refresh_stato_aggiornamento = {
        run: doRefreshStatoAggiornamento,
        interval: 30 * 1000 //millisecondi
    }
    	
    function doRefreshStatoAggiornamento() {
        Ext.Ajax.request({
            url : 'get_stato_aggiornamento.php' ,
            method: 'GET',
            success: function ( result, request) {
                var jsonData = Ext.decode(result.responseText);
                var btStatoAggiornamento = document.getElementById('bt-stato-aggiornamento-esito');
                btStatoAggiornamento.innerHTML = jsonData.html;
            },
            failure: function ( result, request) {
            }
        });
    }

    function show_win_bl_riga(rife1){	
    	// create and show window
    	print_w = new Ext.Window({
    			  width: 400
    			, height: 400
    			, plain: false
    			, title: 'Blocco note riga'
    			, layout: 'fit'
    			, border: true
    			, closable: true
    			, maximizable: false										
    		});	
    	print_w.show();

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : '../desk_utility/acs_anag_art_note_riga.php?fn=open_bl&rife1=' + rife1,
    		        method     : 'POST',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            print_w.add(jsonData.items);
    		            print_w.doLayout();				            
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });		
    } //show_win_annotazioni_articolo
    
    var refresh_notifiche = {
        run: doRefreshNotifiche,
        interval: 60 * 1000 //millisecondi
    }
    
    function doRefreshNotifiche() {
        Ext.Ajax.request({
            url : 'get_stato_notifiche.php' ,
            method: 'GET',
            success: function ( result, request) {
                var jsonData = Ext.decode(result.responseText);
                var btBookingNotifica = document.getElementById('bt-booking-notifica');
    
                if (Ext.get("bt-booking") != null){
                    if (jsonData.num_booking > 0){                    
                    	btBookingNotifica.innerHTML = jsonData.num_booking;
                    	Ext.get("bt-booking-notifica").show();
                    } else {
                    	btBookingNotifica.innerHTML = '0';
                    	Ext.get("bt-booking-notifica").hide();
                    }                    
                }                
    
                	                
            },
            failure: function ( result, request) {
            }
        });
    }

	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {
            //'Ext.calendar': 'src',
	        "Extensible": "../../js/extensible-1.5.2/src", 
			"Extensible.example": "../../js/extensible-1.5.2/examples"	                   
        }	    
	});Ext.Loader.setPath('Ext.ux', 'ux');


    Ext.require(['*', 'Ext.ux.GMapPanel', 'Ext.ux.grid.FiltersFeature', 'Ext.selection.CheckboxModel', 'Ext.ux.grid.column.ProgressBar'
        , 'Extensible.calendar.data.MemoryEventStore'
        , 'Extensible.calendar.CalendarPanel'  
        , 'Extensible.example.calendar.data.Events'
		 , 'Extensible.calendar.data.CalendarMappings',
		 , 'Extensible.calendar.data.EventMappings'
		, 'Ext.ux.grid.Printer'
		, 'Ext.ux.RowExpander'    			 
        ]);

 
    Ext.onReady(function() {

        Ext.QuickTips.init();

        //COMBO: possibilita di rimuovere valore selezionato (con forceSelection lasciava sempre l'ultimo)
        Ext.form.field.ComboBox.override({
         beforeBlur: function(){
             var value = this.getRawValue();
             if(value == ''){
                 this.lastSelection = [];
             }
             this.doQueryTask.cancel();
             this.assertValue();
         }
        });


        var viewport = Ext.create('Ext.Viewport', {
            layout: 'border',
            items: [
                    
	            Ext.create('Ext.Component', {
	                region: 'north',
	                id: 'page-header',
	                height: 110, // give north and south regions a height
	                contentEl: 'header'
	            }),
            
	            Ext.create('Ext.tab.Panel', {
	            	id: 'm-panel',
	        		cls: 'supply_desk_main_panel',            	            	
	                region: 'center', // a center region is ALWAYS required for border layout
	                deferredRender: false,
	                items: [	                ]
	            })

            ]
        });
        
    
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------


        if (Ext.get("main-site-search") != null) {

          	Ext.QuickTips.register({
     			target: "main-site-search",
     			title: 'Ricerca ordini',
     			text: 'Ricerca ordini gestionale'
     		});    	 

           Ext.get("main-site-search").on('click', function(){
         	  acs_show_win_std('Ricerca su archivio ordini di vendita', '../desk_utility/acs_ricerca_ordini_gestionale.php?fn=ricerca_ordini_grid', {filter_type : 'desk_prod'}, 800, 450, null, 'icon-folder_search-16');
           });       	

        }  

        if (Ext.get("bt-page-refresh") != null){
         	Ext.QuickTips.register({
    			target: "bt-page-refresh",
    			title: 'Refresh',
    			text: 'Esegui il refresh della pagina (F5)'
    		});         
        
    	    Ext.get("bt-page-refresh").on('click', function(){
    	    	window.location.reload();
    		});
        }

        if (Ext.get("bt-toolbar") != null){
         	Ext.QuickTips.register({
    			target: "bt-toolbar",
    			title: 'Setup',
    			text: 'Manutenzione voci/parametri gestionali di base'
    		});

       	  Ext.get("bt-toolbar").on('click', function(){
         		acs_show_win_std('Gestione tabelle', 'acs_form_json_toolbar.php', {}, 900, 300, {}, 'icon-database_active-16');	
          });
        }

       if (Ext.get("bt-avanzamento_produzione") != null) {

         	Ext.QuickTips.register({
    			target: "bt-avanzamento_produzione",
    			title: 'Working',
    			text: 'Assegnazione/Rilascio programmi di produzione'
    		});    	 

          Ext.get("bt-avanzamento_produzione").on('click', function(){
        	  acs_show_win_std(null, 'acs_panel_prog_prod.php?fn=get_form_params');
          });       	

       }  


       //ToDo generici
       if (Ext.get("bt-arrivi") != null){
          	Ext.QuickTips.register({
      			target: "bt-arrivi",
      			title: 'To Do List',
      			text: 'Gestione attivit&agrave; e notifiche programmi di produzione'
      		});            
  	        Ext.get("bt-arrivi").on('click', function(){
  	        	acs_show_win_std('Parametri interrogazione To Do list', 'acs_panel_todolist.php?fn=open_filtri', null , 600, 250, {}, 'icon-arrivi-16');             
  	        	});	    
          }




       //Indici
       if (Ext.get("bt-indici") != null){
       		Ext.QuickTips.register({
	   			target: "bt-indici",
   				title: 'Configura programmi',
   				text: 'Distinta attivit&agrave; programmi di produzione'
   			});
                   
	        Ext.get("bt-indici").on('click', function(){
	        	acs_show_win_std(null, 
	    	        'acs_panel_indici.php?fn=open_indici');
            		
	        });
       } 
       if (Ext.get("bt-kiosk") != null){
      		Ext.QuickTips.register({
	   			target: "bt-kiosk",
  				title: 'Kiosk',
  				text: 'Centri rilascio attivit� logistico/produttive'
  			});
                  
	        Ext.get("bt-kiosk").on('click', function(){
	        	 acs_show_win_std(null, 'acs_panel_indici_todolist_progetto.php?fn=get_form_params');
	         });
      }       


       //Ticket manutenzione macchine
       if (Ext.get("bt-ticket-cal") != null){
     		Ext.QuickTips.register({
	   			target: "bt-ticket-cal",
 				title: 'Calendario macchine',
 				text: 'Calendario apertura/chiusura macchine'
 			});
                 
	        Ext.get("bt-ticket-cal").on('click', function(){
	        	acs_show_panel_std('acs_panel_ticket_man_cal.php?fn=open_panel');
          		
	        });
     }       



       //Ticket manutenzione (MApp / Sud2)
       if (Ext.get("bt-ticket-man") != null){
     		Ext.QuickTips.register({
	   			target: "bt-ticket-man",
 				title: 'Ticket manutenzione',
 				text: 'Ticket manutenzione'
 			});
                 
	        Ext.get("bt-ticket-man").on('click', function(){
	        	acs_show_win_std(null, 'acs_panel_ticket_man.php?fn=open_panel');          		
	        });
     }       

       

       if (Ext.get("add-promemoria") != null){
          	Ext.QuickTips.register({
      			target: "add-promemoria",
      			title: 'Memo',
      			text: 'Attiva promemoria via email'
      		});
              
   	        Ext.get("add-promemoria").on('click', function(){
   	        	show_win_crt_promemoria();
   	        });
          } 
                  
       if (Ext.get("bt-call-pgm") != null) {

         	Ext.QuickTips.register({
  			target: "bt-call-pgm",
  			title: 'Start',
  			text: 'Avvio elaborazioni non interattive'
  		});    	 

          Ext.get("bt-call-pgm").on('click', function(){
       	   acs_show_win_std('Avvio processi batch', <?php echo acs_url('module/base/acs_form_json_call_pgm.php?mod=DOVE') ?>);	
          });       	

       }  

       //tooltip stato aggiornamento
       if (Ext.get("bt-stato-aggiornamento") != null){
        	Ext.QuickTips.register({
   			target: "bt-stato-aggiornamento",
   			title: 'Synchro',
   			text: <?php echo j($main_module->get_orari_aggiornamento()); ?>
   		});
       } 

       //schedulo la richiesta dell'ora ultimo aggiornamento
      	Ext.TaskManager.start(refresh_stato_aggiornamento);	
      	Ext.TaskManager.start(refresh_notifiche);
      
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
