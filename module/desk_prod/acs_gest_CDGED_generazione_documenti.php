<?php 

require_once("../../config.inc.php");

$main_module = new DeskProd();
$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
        'module'      => $main_module,
        'tab_name' =>  $cfg_mod['file_tabelle'],
        't_panel' =>  "CDGED - Genera ordini",
        'descrizione' => "Gestione generazione ordini",
        'form_title' => "Dettagli tabella genera ordini",
        'fields_preset' => array(
            'TATAID' => 'CDGED'
        ),
        
        'immissione' => array(
            'data_gen'   => 'TADTGE',
            'user_gen'   => 'TAUSGE',
            
        ),

        'fields_key' => array('TAKEY1', 'TADESC'),
        'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TALOCA', 'immissione'),
        'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TALOCA', 'TAINDI', 'TARIF1'),
		
		'fields' => array(				
		    'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
	            'TADESC' => array('label'	=> 'Descrizione'),
	            
	            'TAMAIL' => array('label'   => 'Note', 'type' => 'textarea', 'maxLength' => 100),
		        'TALOCA' => array('label'	=> 'Procedura', 'maxLength' => 60),
		        
		        'TAINDI' => array('label'	=> 'Msg. conferma', 'type' => 'textarea'),
		        'TARIF1' => array('label'	=> 'Parametri', 'maxLength' => 20),
	            
	            //immissione
    		    'immissione' => array(
    		        'type' => 'immissione', 'fw'=>'width: 70',
    		        'config' => array(
    		            'data_gen'   => 'TADTGE',
    		            'user_gen'   => 'TAUSGE'
    		        )
    		        
    		    ),
    		    'TADTGE' => array('label'	=> 'Data generazione'),
    		    'TAUSGE' => array('label'	=> 'Utente generazione'),
		)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
