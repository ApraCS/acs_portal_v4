<?php

require_once "../../config.inc.php";

set_time_limit(240);

$s = new Spedizioni();
$main_module = new Spedizioni();

//usiamo sempre lo stacco per tipologia produttiva
$_REQUEST['cl_dettaglio_per_gruppo'] = "Y";
$_REQUEST['cl_dettaglio_per_tip_produttiva'] = "N";


//ho selezionato un cella su un giorno
$add_day = (int)substr($_REQUEST['col_name'], 2, 2) - 1;
$m_data = date('Ymd', strtotime($_REQUEST['da_data'] . " +{$add_day} days"));
//$week = strftime("%V", strtotime($m_data));
//$year = strftime("%G", strtotime($m_data));

$sql_data = "SELECT CSAARG, CSNRSE FROM {$cfg_mod_Spedizioni['file_calendario']} CS
              WHERE CSDT='{$id_ditta_default}' AND CSCALE ='*CS' AND CSDTRG = {$m_data}";

$stmt_data = db2_prepare($conn, $sql_data);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_data);
$row_data = db2_fetch_assoc($stmt_data);

$week = $row_data['CSNRSE'];
$year = $row_data['CSAARG'];

$ar_email_to = array();

if (strlen(trim($_REQUEST['sel_trasportatore'])) > 0) {
    $trasportatore = new Trasportatori();
    $trasportatore->load_rec_data_by_k(array("TAKEY1"=>$_REQUEST['sel_trasportatore']));
    $ar_email_to[] = array(trim($trasportatore->rec_data['TAMAIL']), "TRASPORTATORE " . j(trim($trasportatore->rec_data['TADESC'])) . " (" .  trim($trasportatore->rec_data['TAMAIL']) . ")");
}

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>
<html>
 <head>
 
  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  

  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  
 
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   table.int1 th.int_data{font-weight: bold; font-size: 13px;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
   tr.ag_liv2 td{background-color: #DDDDDD;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white;}   
   tr.ag_liv_area th{background-color: #b0b0b0; color: black;}
   tr.ag_liv_el_cliente{background-color: #f3f3f3;}   
   span.sceltadata{font-size: 0.6em; font-weight: normal;}
   span.denominazione_cliente{font-weight: bold;}   
   
   div#my_content h2{font-size: 1.6em; padding: 7px;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>
  </head>
    
 <body> 

<div class="page-utility noPrint">
<?php 
	$bt_fascetta_print = 'Y';
	$bt_fascetta_email = 'Y';
	$bt_fascetta_excel = 'Y';
	$bt_fascetta_close = 'Y';
	include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content'>
<?php
if ($_REQUEST['el_dettaglio_per_ordine'] == "Y")
    $_REQUEST['el_dettaglio_per_cliente'] = 'Y';
    
    
    $campo_data = "TDDTEP"; //$_REQUEST['sceltadata'][0];
    $campo_ora = sceltacampo_ora($campo_data);
    
    //filtro in base a tipologia ordini (multiSelect)
   // $tipologia_ordini = json_decode($_REQUEST['tipologia_ordini']);
    
    //filtro in base a modello (multiSelect)
    //$modello = json_decode($_REQUEST['modello']);    
    
    $k_field = "{$campo_data} as DATA, {$campo_ora} as ORA, TDDT, TA_ITIN.TAASPE AS TDASPE, TDNBOC, TDCITI, SP.CSCVET, TDTPCA, TDAACA, TDNRCA, SP.CSHMPG, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSPORT, SP.CSDTIC, SP.CSHMIC, SP.CSNSPC, TDCVN1 AS MOD, TDSTAB AS STAB ";
    $k_field_ord = "{$campo_data}, TA_ITIN.TAASPE, TDCITI, SP.CSCVET, TDTPCA, TDAACA, TDNRCA, TDNBOC, SP.CSHMPG";
    $k_field_grp = "{$campo_data}, {$campo_ora}, TDDT, TA_ITIN.TAASPE, TDCITI, SP.CSCVET, TDTPCA, TDAACA, TDNRCA, TDNBOC, SP.CSHMPG, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSPORT, SP.CSDTIC, SP.CSHMIC, SP.CSNSPC, TDCVN1, TDSTAB";
    
    if ($dett_carico=="Y"){
        $k_field .= " ";
        $s_field .= " ";
    }
    
   

        
    $s_field = $k_field;
    
    $sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} TD

                  INNER JOIN {$cfg_mod_Spedizioni['file_colli']} PL
                    ON PLDT = TD.TDDT
				      AND PLTIDO = TD.TDOTID
			          AND PLINUM = TD.TDOINU
				      AND PLAADO = TD.TDOADO
				      AND PLNRDO = TD.TDONDO
                  
                  /* per soli colli PRODUZIONE */       
                  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_PUOP
                    ON  TA_PUOP.TADT = PL.PLDT
                    AND TA_PUOP.TAID = 'PUOP'
                    AND TA_PUOP.TANR = PL.PLOPE2 
 
                  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIPC
				  	  ON PL.PLDT = TA_LIPC.TADT AND TA_LIPC.TAID = 'LIPC' AND TA_LIPC.TANR = PL.PLFAS10                     
            
				  INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
					   ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
            
				  INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL ON
				  TDDT = CAL.CSDT AND {$campo_data} = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
								
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				  		ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
					   ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
					   ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
					   
					";
    $sql_WHERE = " WHERE " . $s->get_where_std() . " AND TDSWSP='Y'
					AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year}
                    AND SUBSTR(TA_PUOP.TAREST, 141, 15) = 'PRODUZIONE'
					";
    
    if (isset($_REQUEST['gg'])){
        $filtra_giorni = implode(",", $_REQUEST['gg']);
        $sql_WHERE .= " AND CAL.CSGIOR IN ($filtra_giorni) ";
    }

    // TODO: DRY
    if (isset($_REQUEST['area_spedizione']) and $_REQUEST['area_spedizione'] != '')
        $sql_WHERE .= " AND TA_ITIN.TAASPE = '{$_REQUEST['area_spedizione']}' ";
    if (isset($_REQUEST['divisione']) and $_REQUEST['divisione'] != '')
        $sql_WHERE .= " AND TDCDIV = '{$_REQUEST['divisione']}' ";
    
  /*  if (count($tipologia_ordini) > 0)
        $sql_WHERE .= " AND TDCLOR IN (" . sql_t_IN($tipologia_ordini) . ") ";*/
    
    if (isset($_REQUEST["num_carico"]) && strlen($_REQUEST["num_carico"]) > 0)
        $sql_WHERE .= " AND TDNRCA ='{$_REQUEST["num_carico"]}'";
    if (isset($_REQUEST["num_lotto"]) && strlen($_REQUEST["num_lotto"]) > 0)
        $sql_WHERE .= " AND TDNRLO ='{$_REQUEST["num_lotto"]}'";
    
    if ($_REQUEST['carico_assegnato'] == "Y")
        $sql_WHERE .= " AND TDNRCA > 0 ";
    if ($_REQUEST['carico_assegnato'] == "N")
        $sql_WHERE .= " AND TDNRCA = 0 ";
    if ($_REQUEST['lotto_assegnato'] == "Y")
        $sql_WHERE .= " AND TDNRLO > 0 ";
    if ($_REQUEST['anomalie_evasione'] == "Y")
        $sql_WHERE .= " AND TDOPUN = 'N' ";
    if ($_REQUEST['lotto_assegnato'] == "N")
        $sql_WHERE .= " AND TDNRLO = 0 ";
    
    if ($_REQUEST['proforma_assegnato'] == "Y")
        $sql_WHERE .= " AND TDPROF <> '' ";
    if ($_REQUEST['proforma_assegnato'] == "N")
        $sql_WHERE .= " AND TDPROF = '' ";
    
    if ($_REQUEST['escludi_da_prog'] == "Y") // escludo le sped. "da programmare"
        $sql_WHERE .= " AND SP.CSSTSP <> 'DP' ";
    if ($_REQUEST['escludi_da_prog'] == "N") // solo le sped "da programmare"
        $sql_WHERE .= " AND SP.CSSTSP = 'DP' ";
    
    if (isset($_REQUEST["indice_rottura"]) && strlen($_REQUEST["indice_rottura"]) > 0)
        $sql_WHERE .= " AND TDIRLO LIKE '%{$_REQUEST["indice_rottura"]}%'";
    if ($_REQUEST['indice_rottura_assegnato'] == "Y")
        $sql_WHERE .= " AND TDIRLO <> '' ";
    if ($_REQUEST['indice_rottura_assegnato'] == "N")
        $sql_WHERE .= " AND TDIRLO = '' ";
   
    $sql = "SELECT {$campo_data} AS DATA, 
                'PRODUZIONE' AS RDRIFE,  TA_LIPC.TACINT AS GRUPPO,
                TA_ITIN.TAASPE AS TDASPE, TDCITI, 
                SUM(CASE WHEN PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI,
                PLFAS10, TA_LIPC.TADESC AS PLFAS10_D, 
                TA_ITIN.TAASPE, TDCITI, COUNT(*) AS COLLI, 
                SUM(CASE WHEN PLDTEN>0 AND PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_PROD,
                SUM(CASE WHEN PLDTUS>0 AND PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_SPED
                " . $sql_FROM . $sql_WHERE . " 
                
                GROUP BY {$campo_data}, TA_LIPC.TACINT, PLFAS10, TA_LIPC.TADESC, TA_ITIN.TAASPE, TDCITI" . "
                ORDER BY {$campo_data}, PLFAS10"; 

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ar = array();
    $ar_tot = array();
    
    
    // STAMPO REPORT
    echo "<h2>Riepilogo colli per linea produttiva - Settimana $week / $year <span class=sceltadata>" . sceltadata($campo_data) . "</span> " . add_descr_report($_REQUEST['add_descr_report']) .  "</h2>";
    ?>

    <center>
    	<div class=legenda>
    		CT:Colli totali, CD:Colli disponibili/prodotti, CS:Colli spediti
    	</div>
    </center>
    
    <?php	
     	
     $f_liv0 = "RDRIFE"; 	$d_liv0 = "RDRIFE";
     $f_liv1 = "RDRIFE"; 	$d_liv1 = "RDRIFE";
     $f_liv2 = "RDRIFE"; 	$d_liv2 = "RDRIFE";
     $f_liv3 = "RDRIFE"; 	$d_liv3 = "RDRIFE";   
     
     if ($_REQUEST['cl_dettaglio_per_gruppo'] == "Y"){
       $f_liv1_A = "GRUPPO"; 	$d_liv1_A = "GRUPPO";
       $f_liv1_B = "PLFAS10"; 	$d_liv1_B = "PLFAS10_D";
     }
       
     if ($_REQUEST['cl_dettaglio_per_area'] == "Y")   
       $f_liv2 = "TDASPE";	
       
     if ($_REQUEST['cl_dettaglio_per_itinerario'] == "Y")   
       $f_liv3 = "TDCITI";	
     
     
     //recupero le disponibilita' produttiva (valori limite)
     
     $sql_dd = "SELECT *
     			FROM  {$cfg_mod_Spedizioni['file_calendario']} CAL 
     			WHERE CSDT='$id_ditta_default' AND CSCALE = '*CS' AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year} 
     		  ";
     $stmt_dd = db2_prepare($conn, $sql_dd);
     $result = db2_execute($stmt_dd); 
     
     $ar_dd = array();
     while ($r = db2_fetch_assoc($stmt_dd)) {
     	$ar_dd[$r['CSGIOR']] = $r['CSDTRG'];
     }
     
     
     if (isset($_REQUEST['area_spedizione']) && strlen($_REQUEST['area_spedizione'])>0)
     	$m_where_area = " AND CPAREA = '{$_REQUEST['area_spedizione']}' ";
     else $m_where_area = '';
     
     $sql_cp = "SELECT 0 as TIPO_REC, CPDATA, CPTPCO, CPRIFE, CPAREA, CPCAPA
    		FROM {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP
    		INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL ON CAL.CSDT=CP.CPDT AND CAL.CSDTRG = CP.CPDATA AND CSCALE = '*CS' 
    		WHERE CAL.CSDT='$id_ditta_default' AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year} {$m_where_area}
     
     			UNION
    
    		SELECT 1 as TIPO_REC, CPDATA, CPTPCO, CPRIFE, CPAREA, CPCAPA
    		FROM {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP2		 
    		WHERE CP2.CPDT='$id_ditta_default' AND CP2.CPDATA = {$year} {$m_where_area}			
     			
     		";
     
     
     $stmt_cp = db2_prepare($conn, $sql_cp);
     $result = db2_execute($stmt_cp);
    
     $ar_cp = array();
     while ($r = db2_fetch_assoc($stmt_cp)) {
    	if ($r['TIPO_REC'] == 0 ){ //BY DAY
    		$data = $r['CPDATA'];		
    		$ar_cp[$data][$r['CPTPCO']]['TOTALE'] += $r['CPCAPA']; 	
    		$ar_cp[$data][$r['CPTPCO']][$r['CPAREA']] += $r['CPCAPA'];
    	} else {
    		//RECORD GENERICO (PER GIORNO)
    		$data = $ar_dd[$r['CPRIFE']];
    		if (!isset($ar_cp[$data][$r['CPTPCO']][$r['CPAREA']])){
    			$ar_cp[$data][$r['CPTPCO']]['TOTALE'] += $r['CPCAPA'];
    			$ar_cp[$data][$r['CPTPCO']][$r['CPAREA']] += $r['CPCAPA'];			
    		}
    	}		
     }
     
     while ($r = db2_fetch_assoc($stmt)) {
              	
    		$d_liv2 = $s->decod_std('ASPE', $r[$f_liv2]);
    		$d_liv3 = $s->decod_std('ITIN', $r[$f_liv3]);	 			
    			
    		if (!isset($ar[$r['DATA']])) 				
     			$ar[$r['DATA']] = array("cod" => $r['DATA'], "descr"=>$r['DATA'], 
    																  "val" => array(), "children"=>array());
    
    	 	$d_ar = &$ar[$r['DATA']]['children'];
    		 
    		 $tmp_ar = &$ar[$r['DATA']];																  
    		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
    		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
    		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
    	 	
    
    		//liv0
    		if (!isset($d_ar[$r[$f_liv0]]))	
    				$d_ar[$r[$f_liv0]] = array("cod" => $r[$f_liv0], "descr"=>$r[$d_liv0], 
    																  "val" => array(), "children"=>array()); 														  
    																  																  
    		 $tmp_ar = &$d_ar[$r[$f_liv0]];																  
    		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
    		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
    		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
    		 $d_ar = &$d_ar[$r[$f_liv0]]['children'];
    	 	
    		 
    		//liv1_A (gruppo = TACINT DI LICP)
    		if (!isset($d_ar[$r[$f_liv1_A]]))	
    		      $d_ar[$r[$f_liv1_A]] = array("cod" => $r[$f_liv1_A], "descr"=>$r[$f_liv1_A], 
    																  "val" => array(), "children"=>array());
    		 
    		 if (strlen(trim($r[$f_liv1_A])) == 0)
    		     $d_ar[$r[$f_liv1_A]]['descr'] = ' - NON ASSEGNATO -';    		      
    																  																  
    		 $tmp_ar = &$d_ar[$r[$f_liv1_A]];																  
    		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
    		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
    		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
			 $d_ar = &$d_ar[$r[$f_liv1_A]]['children'];

    		     		 
    		 //liv1_B (LICP, linea)
    		 if (!isset($d_ar[$r[$f_liv1_B]]))
    		     $d_ar[$r[$f_liv1_B]] = array("cod" => $r[$f_liv1_B], "descr"=>implode(' - ', array($r[$f_liv1_B], $r[$d_liv1_B])),
		                  "val" => array(), "children"=>array());
		     
		     if (strlen(trim($r[$f_liv1_B])) == 0)
		         $d_ar[$r[$f_liv1_B]]['descr'] = ' - NON ASSEGNATO -';
		         
		         
	         $tmp_ar = &$d_ar[$r[$f_liv1_B]];
	         $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;
	         $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
	         $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
		     $d_ar = &$d_ar[$r[$f_liv1_B]]['children'];    
    		 
    	
    		 
    		 
    	 	//liv2
    		if (!isset($d_ar[$r[$f_liv2]]))	
    				$d_ar[$r[$f_liv2]] = array("cod" => $r[$f_liv2], "descr"=>'2222' . $d_liv2, 
    																  "val" => array(), "children"=>array());
    		 $tmp_ar = &$d_ar[$r[$f_liv2]];																  
    		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
    		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
    		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
			 $d_ar = &$d_ar[$r[$f_liv2]]['children'];
    		 

    		//liv3 
    		if (!isset($d_ar[$r[$f_liv3]]))	
    				$d_ar[$r[$f_liv3]] = array("cod" => $r[$f_liv3], "descr"=>$d_liv3, 
    																  "val" => array(), "children"=>array());
    																  
    		 $tmp_ar = &$d_ar[$r[$f_liv3]];																  
    		 $tmp_ar["val"]['COLLI'] += $r['COLLI'] ;					
    		 $tmp_ar["val"]['COLLI_PROD'] += $r['COLLI_PROD'] ;
    		 $tmp_ar["val"]['COLLI_SPED'] += $r['COLLI_SPED'] ;
    		 
    	
     } //while
     

     
     
     
     $cl_liv_cont = 0;
     $liv0_row_cl = 0; 
     if ($_REQUEST['cl_dettaglio_per_itinerario'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
     if ($_REQUEST['cl_dettaglio_per_area'] == "Y") $liv2_row_cl = ++$cl_liv_cont; 
     if ($_REQUEST['cl_dettaglio_per_tip_produttiva'] == "Y") $liv1_row_cl = ++$cl_liv_cont; 
     	 
     echo "<table class=int1><tr>";
     foreach ($ar as $kgg => $gg){
      //echo "<div class='ag_giorno' style='display: inline-block; vertical-align: top;'>";
    	echo "<td valign=top>";  
    	 echo "<table class=int1>";
    	  echo "<tr><th colspan=4>" . ucfirst(print_date($kgg, "%A %d/%m")) . "</th></tr>";
    	  echo "<tr><th>&nbsp;</th><th>CT</th><th>CD</th><th>CS</th></tr>";	  
    	  
    	  
    		foreach ($gg['children'] as $kl0 => $l0){
    		  echo "<tr class=ag_liv{$liv0_row_cl}><td>" . $l0['descr'] . "</td><td class=number>" . $l0['val']['COLLI'] . "</td>
    		  	  														<td class=number>" . $l0['val']['COLLI_PROD'] . "</td>
    		  	  														<td class=number>" . $l0['val']['COLLI_SPED'] . "</td></tr>";
    
    				  //confronto con capacita' produttiva
    				  if (isset($ar_cp[$kgg][trim($kl0)]['TOTALE']) && $_REQUEST['cl_confronta_cap_prod'] == 'Y'){
    				  	$m_cp = $ar_cp[$kgg][trim($kl0)]['TOTALE'];
    				  	$m_colli = $l0['val']['COLLI']; 
    				  	$m_dif = $m_colli - $m_cp;
    				  	$m_dif_perc = ($m_colli - $m_cp) / $m_cp * 100;
    				  	
    				  	if ($m_dif_perc >= 5) 
    				  		$m_style = "background-color: #F9BFC1;";
    				  	elseif ($m_dif_perc >= -5)
    				  		$m_style = "background-color: yellow;";
    				  	else $m_style = "background-color: #BAE860;"; 		  	
    				  	echo "<tr class=ag_liv{$liv0_row_cl}><td align=right>Capacit&agrave;</td><td class=number>" . $m_cp . "</td>
    				  	  														<td class=number style='$m_style'>" . n($m_dif, 0, 'Y') . "</td>
    				  	  														<td class=number style='$m_style'>" . n($m_dif_perc, 1, 'Y') . "%</td></tr>";
    				  }
    		  
    				  
    		  foreach ($l0['children'] as $kl1 => $l1a){
    	 		if ($_REQUEST['cl_dettaglio_per_gruppo'] == "Y")	  	
    	 		    echo "<tr class=ag_liv2><td>" . $l1a['descr'] . "</td><td class=number>" . $l1a['val']['COLLI'] . "</td>
    		  	  														<td class=number>" . $l1a['val']['COLLI_PROD'] . "</td>
    		  	  														<td class=number>" . $l1a['val']['COLLI_SPED'] . "</td></tr>";
    		  	  
    	 		    

    	 		    //confronto con capacita' produttiva (per gruppo)
    	 		    if($_REQUEST['cl_confronta_cap_prod'] == 'Y'){
    	 		        $cp = new SpedCapacitaProduttiva;
    	 		        $m_cp =  $cp->get_tot_by_data($kgg, trim($kl1));    	 		   
    	 		    
        	 		    $m_colli = $l1a['val']['COLLI'];
    	 		        $m_dif = $m_colli - $m_cp;
    	 		        if ($m_cp != 0)
    	 		            $m_dif_perc = ($m_colli - $m_cp) / $m_cp * 100;
    	 		        else
    	 		            $m_dif_perc = 0;
    	 		        
    	 		        if ($m_dif_perc >= 5)
    	 		            $m_style = "background-color: #F9BFC1;";
    	 		        elseif ($m_dif_perc >= -5)
    	 		            $m_style = "background-color: yellow;";
    	 		        else $m_style = "background-color: #BAE860;";
    	 		        echo "<tr class=ag_liv1><td align=right>Capacit&agrave;</td><td class=number>" . $m_cp . "</td>
  														<td class=number style='$m_style'>" . n($m_dif, 0, 'Y') . "</td>
  														<td class=number style='$m_style'>" . n($m_dif_perc, 1, 'Y') . "%</td></tr>";

    	 		    }
    	 		    
    	 		      		  	  
    		  	  
    	 		    foreach ($l1a['children'] as $kl1b => $l1b){
    		  	      if ($_REQUEST['cl_dettaglio_per_linea'] == "Y")
    		  	          echo "<tr class=ag_liv{$liv3_row_cl}><td>" . $l1b['descr'] . "</td><td class=number>" . $l1b['val']['COLLI'] . "</td>
    				  																 <td class=number>" . $l1b['val']['COLLI_PROD'] . "</td>
    				  																 <td class=number>" . $l1b['val']['COLLI_SPED'] . "</td></tr>";
    		  	          
    		  	  
    		  	  
                			
                		  		foreach ($l1b['children'] as $kl2 => $l2){
                		  		  if ($_REQUEST['cl_dettaglio_per_area'] == "Y"){
                		  			echo "<tr class=ag_liv{$liv2_row_cl}><td>" . $l2['descr'] . "</td><td class=number>" . $l2['val']['COLLI'] . "</td>
                		  																 <td class=number>" . $l2['val']['COLLI_PROD'] . "</td>
                		  																 <td class=number>" . $l2['val']['COLLI_SPED'] . "</td></tr>";
                		  			
                			  			//confronto con capacita' produttiva
                			  			if (isset($ar_cp[$kgg][trim($kl0)][trim($kl2)]) && $_REQUEST['cl_confronta_cap_prod'] == 'Y' && $_REQUEST['cl_dettaglio_per_tip_produttiva'] != "Y"){
                			  				$m_cp = $ar_cp[$kgg][trim($kl0)][trim($kl2)];
                			  				$m_colli = $l2['val']['COLLI'];
                			  				$m_dif = $m_colli - $m_cp;
                			  				$m_dif_perc = ($m_colli - $m_cp) / $m_cp * 100;
                			  				 
                						  	if ($m_dif_perc >= 5) 
                						  		$m_style = "background-color: #F9BFC1;";
                						  	elseif ($m_dif_perc >= -5)
                						  		$m_style = "background-color: yellow;";
                						  	else $m_style = "background-color: #BAE860;";
                			  				echo "<tr class=ag_liv{$liv2_row_cl}><td align=right>Capacit&agrave;</td><td class=number>" . $m_cp . "</td>
                			  	  														<td class=number style='$m_style'>" . n($m_dif, 0, 'Y') . "</td>
                			  					  	  									<td class=number style='$m_style'>" . n($m_dif_perc, 1, 'Y') . "%</td></tr>";
                			  			}		  			
                		  			
                		  		  }	
                		  		  
                		  		  
            
            
                        				  		foreach ($l2['children'] as $kl4 => $l4){
                        				  		  if ($_REQUEST['cl_dettaglio_per_itinerario'] == "Y")
                        				  			echo "<tr class=ag_liv{$liv4_row_cl}><td>" . $l4['descr'] . "</td><td class=number>" . $l4['val']['COLLI'] . "</td>
                        				  																 <td class=number>" . $l4['val']['COLLI_PROD'] . "</td>
                        				  																 <td class=number>" . $l4['val']['COLLI_SPED'] . "</td></tr>";	  			
                        						}
            	  			
            
            
                						 
            					  			
                				}		
    		  	  }
    		  }
    		}
    	  
    	 echo "</table>";
      //echo "</div>";
      echo "</td>";
     }
     echo "</tr></table>";

    
    
?>


  
</div> <!-- end #my_content -->  
  
</html>

<?php

//UTIILTY
function sceltadata($campo_data){
    switch ($campo_data){
        case "TDDTEP":
            return "(Data programmazione)";
        case "TDDTSP":
            return "(Data spedizione)";
        case "SP.CSDTSC":
            return "(Data scarico)";
    }
}


function sceltacampo_ora($campo_data){
    switch ($campo_data){
        case "TDDTEP":
            return "SP.CSHMPG";
        case "TDDTSP":
            return "SP.CSHMPG";
        case "SP.CSDTSC":
            return "SP.CSHMSC";
    }
}

function add_descr_report($desc){
    if (strlen($desc) > 0)
        return ' - ' . $desc;
        else return '';
        
} 