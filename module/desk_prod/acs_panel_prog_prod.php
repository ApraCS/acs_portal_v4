<?php
require_once("../../config.inc.php");
require_once 'acs_panel_prog_prod_include.php';

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

function add_parziali(&$ar, $r){
    $ar['colli_T'] += $r['TDTOCO'];
    $ar['colli_D'] += $r['TDCOPR'];
    $ar['volume']  += $r['TDVOLU'];
    
    $ar['fl_art_manc'] = max($r['FL_ART_MANC'], $ar['fl_art_manc']);
    $ar['art_da_prog'] = max($r['ART_DA_PROG'], $ar['art_da_prog']);
    $ar['fl_bloc']     = max($r['FL_BLOC'],     $ar['fl_bloc']);
    
    if ($ar['colli_T'] > 0)
        $ar['colli_D_progress'] = $ar['colli_D'] / $ar['colli_T'] * 100;
    else $ar['colli_D_progress'] = 100;
}

// ******************************************************************************************
// GET JSON DATA (treepanel)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){

    $sql_where .= " AND TDNRCA > 0 ";  /* solo con carico */
    $sql_where .= sql_where_by_open_form($m_params->form_ep->form_values);
    
    if ($_REQUEST['node'] == 'root'){
        
        //PRIMO LIVELLO TREE
        
        
        //Lotti
        $sql = "SELECT TDTPLO, TDAALO, TDNRLO, TDASPE, TDDTEP, PSDTGE
                , SUM(TDTOCO) AS TDTOCO
                , SUM(TDCOPR) AS TDCOPR
                , SUM(TDVOLU) AS TDVOLU
                
                , MAX(
                    CASE 
                        WHEN TDFMTO = 'D' THEN 20
                        WHEN TDFMTO = 'M' THEN 5
                        WHEN TDFMTO = 'Y' AND TDMTOO = 'Y'  THEN 4
                        WHEN TDFMTO = 'P' AND TDMTOO = 'Y'  THEN 3
                        WHEN TDFMTO = 'Y' AND TDMTOO = 'O'  THEN 3
                        WHEN TDFMTO = ''  AND TDMTOO = 'Y'  THEN 2
                        WHEN TDFMTO = 'Y' AND TDMTOO <> 'Y' THEN 1
                    ELSE 0 
                    END) AS FL_ART_MANC

                , MAX(CASE 
                        WHEN TDFMTS = 'Y' THEN 1
                        WHEN TDFN15 = 1   THEN 2
                        WHEN TDFN15 = 2   THEN 3
                    ELSE 0 
                    END) AS ART_DA_PROG

                , MAX(CASE 
                        WHEN TDBLEV = 'Y' AND TDBLOC = 'Y' THEN 4
                        WHEN TDBLEV = 'Y' THEN 3
                        WHEN TDBLOC = 'Y' THEN 2
                    ELSE 0 
                    END) AS FL_BLOC

 				FROM {$cfg_mod_Spedizioni['file_testate']} TD
 				
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOF = SP.CSPROG

                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS
					ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
				  
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI

                LEFT OUTER JOIN (
    	            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
    	            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
    	            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
    	            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
    	            ) RD
    	            ON TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO
				
			 	WHERE " . $s->get_where_std() . " AND TDSWSP = 'Y' " . $sql_where . "
 				GROUP BY TDTPLO, TDAALO, TDNRLO, TDASPE, TDDTEP, PSDTGE
 				ORDER BY TDDTEP DESC, TDAALO DESC, TDNRLO DESC, TDTPLO, TDASPE
			";
 	            
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        $ar = array();
        
        $ar['LNA'] = array('id' => 'LNA', 'task' => '<b>DA PROGRAMMARE</b>', 'children' => array());
        $ar['LA']  = array('id' => 'LA',  'task' => '<b>PROGRAMMATI</b>', 'children' => array());
        
        while ($row = db2_fetch_assoc($stmt)){
            $r = array();                        
            
            if ($row['TDNRLO'] > 0){ //LOTTI ASSEGNATI
                $d_ar = &$ar['LA'];
                $r['id']      = implode('_', array($row['TDTPLO'], $row['TDAALO'], $row['TDNRLO'], $row['TDASPE']));
                $r['task']    = implode('_', array($row['TDTPLO'], $row['TDAALO'], $row['TDNRLO'], $row['TDASPE']));
                $r['liv']      = 'liv_1';
                $r['liv_type'] = 'LOTTO';
                $r['lotto']    = $r['id'];
                $r['rilascio'] = $r['PSDTGE'];
                $r['data_evasione'] = $row['TDDTEP'];
            }
            else { //LOTTI NON ASSEGNATI
                $d_ar = &$ar['LNA'];
                $r['id']      = implode('_', array('LNA', $row['TDDTEP'], $row['TDASPE']));
                $r['task']    = print_date($row['TDDTEP']) . " - {$row['TDASPE']}";
                $r['liv']      = 'liv_1';
                $r['liv_type'] = 'DATA_ASPE';
                $r['rilascio'] = $r['PSDTGE'];
            }

            $r['liv_c']    = $r['id'];
            
            
            add_parziali($r, $row);
            add_parziali($d_ar, $row);
            $d_ar['children'][$r['id']] = $r;
        } //while
    } else {
        //LIVELLI SUCCESSIVI TREE        
        
        $ar_node = explode('_', $_REQUEST['node']);
        
        if ($ar_node[0] == 'LNA') {
            //apertura liv lotto non assegnato
            $sql_where .= " AND TDNRLO = 0 AND TDDTEP = {$ar_node[1]} AND TDASPE = " . sql_t($ar_node[2]);
        } else {
            //apertura liv con lotto
            //recupero lotto/ditta origine da node            
            $sql_where .= " AND TDTPLO = " . sql_t($ar_node[0]) . " AND TDAALO = {$ar_node[1]} AND TDNRLO = {$ar_node[2]} AND TDASPE = " . sql_t($ar_node[3]);
        }
        
        $sql = "SELECT TD.*, SP.CSCVET, SP.CSCITI, PS.PSDTGE

                , CASE 
                        WHEN TDFMTO = 'D' THEN 20
                        WHEN TDFMTO = 'M' THEN 5
                        WHEN TDFMTO = 'Y' AND TDMTOO = 'Y'  THEN 4
                        WHEN TDFMTO = 'P' AND TDMTOO = 'Y'  THEN 3
                        WHEN TDFMTO = 'Y' AND TDMTOO = 'O'  THEN 3
                        WHEN TDFMTO = ''  AND TDMTOO = 'Y'  THEN 2
                        WHEN TDFMTO = 'Y' AND TDMTOO <> 'Y' THEN 1
                    ELSE 0 
                    END AS FL_ART_MANC

                , CASE 
                        WHEN TDFMTS = 'Y' THEN 1
                        WHEN TDFN15 = 1 THEN 2
                        WHEN TDFN15 = 2 THEN 3
                    ELSE 0 
                    END AS ART_DA_PROG

                , CASE 
                        WHEN TDBLEV = 'Y' AND TDBLOC = 'Y' THEN 4
                        WHEN TDBLEV = 'Y' THEN 3
                        WHEN TDBLOC = 'Y' THEN 2
                    ELSE 0 
                    END AS FL_BLOC

 				FROM {$cfg_mod_Spedizioni['file_testate']} TD
 				
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG

                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS
				  ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
				  
				  
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI

                LEFT OUTER JOIN (
    	            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
    	            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
    	            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
    	            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
    	            ) RD
    	            ON TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO
				  
			 	WHERE " . $s->get_where_std() . " AND TDSWSP = 'Y' " . $sql_where . "
                ORDER BY TDAACA, TDNRCA, TDSECA, TDDCON, TDCCON, TDCDES, TDSCOR, TDONDO
			";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        $ar = array();
        
        if ($m_params->only_tddocu == 'Y'){
            while ($row = db2_fetch_assoc($stmt)){
                $ar[] = $row['TDDOCU'];
            } //while
            echo acs_je(array('success' => true, 'items' => $ar));
            exit;
        }
        
        
        while ($row = db2_fetch_assoc($stmt)){
            $liv1 = implode("_", array($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA'],  $row['TDDTOR']));  //carico            
            $liv2 = implode("_", array($row['TDCCON'], $row['TDCDES'], $row['TDTDES'])); //cliente/dest
            $tmp_ar_id = array($_REQUEST['node']);

            //liv1: carico
            $d_ar = &$ar;
            $c_liv = $liv1;
            $tmp_ar_id[] = $c_liv; 
            if (!isset($d_ar[$c_liv])){
                $d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id),  "children"=>array());                
                $d_ar[$c_liv]['liv']   = 'liv_2';
                $d_ar[$c_liv]['liv_c'] = $liv1;
                $d_ar[$c_liv]['liv_type'] = 'CARICO';
                $d_ar[$c_liv]['itin_cli'] = '<B>' . acs_u8e($s->decod_std('ITIN', $row['CSCITI'])) . '</B>';
                $d_ar[$c_liv]['trasportatore'] = '<B>' . acs_u8e(trim($s->decod_trasportatore_by_sped_row($row))) . '</B>';
                $d_ar[$c_liv]['task']  = $liv1;
                $d_ar[$c_liv]['data_evasione']  = $row['TDDTEP'];
                $d_ar[$c_liv]['rilascio'] = $row['PSDTGE'];
            }
            add_parziali($d_ar[$c_liv], $row);
            
            //liv2: cliente/destinazione/seq.
            $d_ar = &$d_ar[$c_liv]['children'];
            $c_liv = $liv2;
            $tmp_ar_id[] = $c_liv;
            if (!isset($d_ar[$c_liv])){
                $d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id),  "children"=>array());
                $d_ar[$c_liv]['liv']   = 'liv_3';
                $d_ar[$c_liv]['liv_c'] = $c_liv;
                $d_ar[$c_liv]['liv_type'] = 'CLI_DES';
                $d_ar[$c_liv]['itin_cli'] = $s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD']);
                $d_ar[$c_liv]['task']  = acs_u8e($row['TDDCON']);
                $d_ar[$c_liv]['seq_carico']  = acs_u8e($row['TDSECA']);
            }
            add_parziali($d_ar[$c_liv], $row);
            
            //ultimo livello: accodo ordini
            $d_ar = &$d_ar[$c_liv]['children'];

            $r = array();
                $r['id']            = $row['TDDOCU'];
                $r['liv']           = 'liv_4';
                $r['liv_c']         = $row['TDDOCU'];
                $r['liv_type']      = 'ORDINE';
                $r['task']          = implode('_', array($row['TDOTPD'], $row['TDOADO'], $row['TDONDO']));
                $r['TDNBOC']        = $row['TDNBOC'];
                $r['lotto']         = implode('_', array($row['TDTPLO'], $row['TDAALO'], $row['TDNRLO'] /*, $row['TDDTOR']*/));
                $r['carico']        = implode('_', array($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA']));
                $r['cod_cli']       = $row['TDCCON'];
                $r['rag_soc']       = $row['TDDCON'];
                $r['itin_cli']      = trim(acs_u8e($row['TDVSRF']));
                $r['colli_S']       = $row['TDCOSP'];
                $r['p_lordo']       = $row['TDPLOR'];
                $r['p_netto']       = $row['TDPNET'];
                $r['c_ordine']      = $row['TDCLOR'];
                $r['itinerario']    = implode('/', array($row['TDCITI'], $row['TDSITI']));
                $r['data_scarico']  = $row['TDDTIS'];
                $r['ora_scarico']   = $row['TDHMSI'];
                $r['porta']         = $row['TDBCAR'];
                $r['vettore']       = $row['TDVETT'];
                $r['rif_p']         = trim($row['TDSELO']);
                $r['rif_s']         = trim($row['TDRFCA']);
                
                $r["tipo"] 		    = $row['TDOTPD'];
                $r["qtip_tipo"] 	= acs_u8e($row['TDDOTD']);
                $r['raggr']         = trim($row['TDCLOR']);
                
                $r['stato']	        = trim($row['TDSTAT']);
                $r['fl_bloc']       = $row['FL_BLOC'];
                $r['rilascio']      = $row['TDDTCF'];
                $r['leaf']          = true;
                
                add_parziali($r, $row);
                $d_ar[] = $r;
        }
    } //livelli successivi
    
    //output dati
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }    
    echo acs_je($ret);	
    exit;
}

// ******************************************************************************************
// FORM PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_form_params'){
?>
{"success":true, 
	m_win: {
		title: 'Assegnazione/Rilascio programmi di produzione',
		width: 600, height: 370,
		iconCls: 'iconFabbrica'
	},
	"items": [
        {
            xtype: 'panel',
            title: '',
            layout: <?php echo_je(layout_ar('vbox')); ?>,
			  items: [{
		            xtype: 'form',
		            flex: 1,
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            items: [		            
						  {
							name: 'f_area_spedizione',
							anchor: '-15',
							xtype: 'combo',
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    multiSelect: true,						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('ASPE'), ""); ?>
								    ]
							}						 
						 }, {
							name: 'f_divisione',
							xtype: 'combo',
							anchor: '-15',
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,		
						    multiSelect: true,						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}	
							
						 , {
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
				 	        layout: 'vbox',
				 	        items: [				 	       
					 	        <?php echo_je(a15(n_o(extjs_datefield('f_data_da', 'Da data', date("Ymd", strtotime( '-5 days' )))))); ?>	
					 	      , <?php echo_je(a15(n_o(extjs_datefield('f_data_a', 'A data', date("Ymd"))))); ?>		            		            		     	            		            		            
							]
						}							
							
					],
					
					buttons: [
				  
					{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32', scale: 'large',
			            handler: function() {
			            
			            	var form = this.up('form').getForm(),
			            		m_win = this.up('window'),
			            		form_values = form.getValues();
			            	 
			            	if(form.isValid()){
					        	acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', null, {
        		            			form_values: form_values});
								m_win.destroy();
			            	} //isValid
			            }
			         }
			       ]
		         }	//form		  
			  ]
		}
	]
}		
<?php exit; }



// ******************************************************************************************
// TABPANEL TREE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
?>
{"success":true, "items":
  {
    xtype: 'treepanel',
    title: 'Working',
    
    //selType: 'cellmodel',
    cls: 'tree-calendario',
    collapsible: true,
    useArrows: true,
    rootVisible: false,

    <?php echo make_tab_closable(); ?>,
        
        tbar: new Ext.Toolbar({
            items:['<b>Assegnazione/Rilascio programmi di produzione</b>', '->'
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
            ]
        }),
        
		store: Ext.create('Ext.data.TreeStore', {
	        proxy: {
	            type: 'ajax', timeout: 240000, actionMethods: {read: 'POST'},
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
	            extraParams: {form_ep: <?php echo acs_je($m_params); ?>},
                doRequest: personalizza_extraParams_to_jsonData
	        },
	        
			fields: ['task', 'liv', 'liv_c', 'liv_type', 'data', 'itinerario', 'colli_T', 'colli_S', 'colli_S_progress', 'colli_D', 'colli_D_progress', 'colli_SCAR_progress', 
					'da_spedire', 'non_disponibili', 'non_scaricati', 'data_inizio', 'ora_inizio', 'qtip_spedizione', 'taaspe',
					'carico_lotto', 'carico_lotto_est', 'ora_spedizione', 'volume', 'importo', 'c_trasportatore', 'trasportatore', 'vmc', 'targa', 'porta',
					'min_spunta_disp', 'min_spunta_scar', 'max_spunta_sped', 'min_spunta_sped', 'data_presunta', 'tipologia', 'data_riferimento', 'secondaria',
					'TDNBOC', 'TDNBOF', 'cod_cli', 'rag_soc', 'p_lordo', 'p_netto', 'c_ordine', 'data_scarico', 'ora_scarico', 'vettore', 'lotto', 'carico', 'itin_cli',
					'rif_p', 'rif_s', 'seq_carico', 'data_evasione', 'rilascio',
					'tipo', 'raggr', 'qtip_tipo', 'stato', 'fl_bloc', 'fl_art_manc', 'art_da_prog'],
	        
	        reader: new Ext.data.JsonReader()
	    }),
        
        multiSelect: true,
        singleExpand: false,
		viewConfig: {getRowClass: function(record, index) {return record.get('liv');}},
        columns: [{
                xtype: 'treecolumn', dataIndex: 'task',
                header: '<b>Lotto/Carico/Scarico</b>', flex: 1, menuDisabled: true, sortable: false,
                renderer: function (value, metaData, record, row, col, store, gridView){
                	if (record.get('qtip_spedizione').length > 0)
    	    			 	metaData.tdAttr = ' data-qtip="' + Ext.String.htmlEncode(record.get('qtip_spedizione')) + '"';
                	return value;
                }
        	}
        	
			, <?php
				//inclusione colonne flag ordini
				$js_include_flag_ordini = array('menuDisabled' => false, 'sortable' => true, 'show_new' => 'N');
				require("../desk_vend/_include_js_column_flag_ordini.php");
			?>

        	, {dataIndex: 'seq_carico', header: '<b>Seq.</b>',  width:35}
        	, {dataIndex: 'rilascio', header: '<b>Rilascio</b>',  width:70, renderer : date_from_AS}
        	, {dataIndex: 'itin_cli', header: '<b>Itinerario / Localit&agrave;</b><br><b>/ Riferimento</b>',  flex: 1}
        	
			, {text: '<b>Tp</b>', width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd',
	        	    	renderer: function (value, metaData, record, row, col, store, gridView){
								metaData.tdCls += ' toDoListTp ' + record.get('raggr');
								
								if (record.get('qtip_tipo') != "")
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '"';

							return value;
	    			}
				}
				
			, {text: '<b>St</b>', width: 30, dataIndex: 'stato',
        	    	renderer: function (value, metaData, record, row, col, store, gridView){
							//if (record.get('qtip_stato') != "")
							//	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_stato')) + '"';

						return value;
    			}
			}
        	
        	, {dataIndex: 'rif_p', header: '<b>RifP</b>',  width:50}
        	, {dataIndex: 'rif_s', header: '<b>RifS</b>',  width:50}
        	
        	, {dataIndex: 'colli_T', header: '<b>Colli</b>',  width:50, align: 'right', renderer: floatRenderer0}

			//, {dataIndex: 'colli_P', header: '<b>Colli<br>prod.</b>',  width:50, align: 'right', renderer: floatRenderer0}


        	, {dataIndex: 'colli_D_progress', header: '<b>Colli<br>Dispon.</b>',  width:70, align: 'center', xtype: 'progressbarcolumn'}
			, {header: '<b>Non dispon.</b>', dataIndex: 'COLLI_ND', width: 90, align: 'right', renderer: floatRenderer0,
				 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					            return floatRenderer0(value);
				        }
	        	}
        	        	
        	
        	
        	, <?php echo dx_mobile() ?>
        	
  		] //columns
  		
  		, listeners: {
      		 itemcontextmenu : function(tree, rec, node, index, event) {
				tree.panel.acs_actions.show_menu_dx(tree, rec, node, index, event);
    		},

    		celldblclick: { 
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
    			  	var col_name	 = iView.getGridColumns()[iColIdx].dataIndex,
    		  			rec			 = iView.getRecord(iRowEl),
    		  			liv_type	 = rec.get('liv_type'),
    	  				parentId	 = rec.get('parentId'),
    	  				ar_parentId  = parentId.split("|"),
    	  				ar_lotto	 = parentId.split("_"),
    	  				tp_lotto	 = ar_lotto[0];
    
    			  	if (col_name == 'art_mancanti' && liv_type == 'ORDINE' && rec.get('fl_art_manc') != null){
        		  	 	var	ordine	 	 = rec.get('id');		//id = liv_c

        				iEvent.preventDefault();
    	    			acs_show_win_std(null,
            				'../base/acs_win_carrello_mto.php?fn=open_win', {
        						ordine: ordine
        				 }, null, null, {}, null);
        				 
        				return false;
    			  	}
			  	
			  } //function
		    } //celldbclick
		    
		} //listeners

  	//funzioni / utility
  	, acs_actions: {

      	 show_menu_dx: function(tree, rec, node, index, event) {
      	 
    		event.stopEvent();
    		
    		var voci_menu 		   = [],
    			liv				   = rec.get('liv'),
    			id 		  		   = rec.get('id'),
    			lotto 	  		   = id.split("|"),
    			ar_lotto		   = lotto[0].split("_"),
    			tp_lotto  		   = ar_lotto[0],
    			data_evas_lotto	   = ar_lotto[1],
    			rows_selected 	   = tree.getSelectionModel().getSelection(),
    			list_rows_selected = [],
    			list_ord_selected  = [];
    
    		
    		if (rec.get('liv_type') != 'LOTTO' && rec.get('liv_type') != 'CARICO'){
    				if (id == 'LA') {
    			
    			voci_menu.push(
    						{
    				      		text: 'Agenda programmazione settimanale',
    				    		iconCls : 'iconFabbrica',
    				    		handler: function() {
    				    		
    				    		acs_show_win_std('Agenda programmi di produzione assegnati', 
			            			'acs_calendario_settimanale_spedizioni.php?fn=get_parametri_form', 
			            			{}, 400, 510, null, 'iconFabbrica')
    				    			
    				    		}
    					  }
    				);
    			
    			}
    		}
    
    
                for (var i=0; i<rows_selected.length; i++) {
                	list_rows_selected.push(rows_selected[i].get('liv_c'));
                	
                	//accodiamo gli ordini appartenenti al carico			            	
                	Ext.each(rows_selected[i].childNodes, function(rowCli) {
    	            	Ext.each(rowCli.childNodes, function(rowOrd) {
    	            		list_ord_selected.push(rowOrd.get('liv_c'));
    	            	});			            	
                	});
                	
                	if (tp_lotto == 'LNA' && liv == 'liv_2' && rows_selected[i].get('data_evasione') != data_evas_lotto) {
                		acs_show_msg_error('Selezionare carichi con la stessa data di evasione.');
                		return false;
                	}
                }
    
    			if (tp_lotto == 'LNA' && rec.get('liv_type') == 'DATA_ASPE') {
    			
    				var my_listeners = {
    					afterOk: function(from_win){
    						from_win.destroy();
    						tree.getStore().treeStore.load();
    					}
    				};
    			
    				voci_menu.push(
    					{
    			      		text: 'Assegna Lotto',
    			    		iconCls : 'iconFabbrica',
    			    		handler: function() {
    			    		
        			    		//recupero elenco ordini da passare a "Assegna lotto"
        						Ext.Ajax.request({
     						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
     						        method  : 'POST',
     						        params: {node: rec.get('id')},
     			        			jsonData: {
     			        			   form_ep: <?php echo acs_je($m_params) ?>,
     			        			   only_tddocu: 'Y'
     								},
     								
     								success: function(response, opts) {
    						        	 var jsonData = Ext.decode(response.responseText);
    						        	 var id_ordini = jsonData.items;
    						        	 
                						 acs_show_win_std(null,
                			    				'acs_form_assegna_lotto.php?fn=open_form', {
                			    					filtro_semplificato: 'Y',
                			    					open_request: {
                			    						id_ordini:      id_ordini,
                			    						data_evasione:	data_evas_lotto
                			    					}
                		    				 }, null, null, my_listeners);	        	 
    						        	      		   
    			            		},
    						        failure    : function(result, request){
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
     								
     						    });
        						
    			    		}
    				    }
    			     );
    			}
    			
    			
    			if (rec.get('liv_type') == 'LOTTO' || rec.get('liv_type') == 'CARICO'){
    				var m_open_request = {
    					no_produzione: 'Y'
    				};
    				
    				if (rec.get('liv_type') == 'CARICO'){
    					var ar_k_carico = rec.get('liv_c').split('_');
    					var ar_k_lotto = rec.parentNode.get('liv_c').split('_');
    				}
    				if (rec.get('liv_type') == 'LOTTO'){
    					var ar_k_lotto = rec.get('liv_c').split('_');
    				}
    				
    				if (!Ext.isEmpty(ar_k_carico)){
    					m_open_request.TDTPCA = ar_k_carico[0];
    					m_open_request.TDAACA = ar_k_carico[1];
    					m_open_request.TDNRCA = ar_k_carico[2];
    					m_open_request.TDDTOR = ar_k_carico[3];
    				}
    				if (!Ext.isEmpty(ar_k_lotto)){
    					m_open_request.TDTPLO = ar_k_lotto[0];
    					m_open_request.TDAALO = ar_k_lotto[1];
    					m_open_request.TDNRLO = ar_k_lotto[2];
    					m_open_request.TDASPE = ar_k_lotto[3];
    				}
    				
            		voci_menu.push(
    						{
    				      		text: 'Colli per linea',
    				    		iconCls : 'icon-folder_search-16',
    				    		handler: function() {
    				    			acs_show_win_std('Riepilogo colli per linea logistico/produttiva',
    				    				'../desk_prod/acs_report_riepilogo_colli_linea_prod_params.php?fn=open_form', {
    				    					filtro_semplificato: 'Y',
    				    					open_request: m_open_request
    			    				 }, 600, 380, {}, 'icon-folder_search-16');
    				    		}
    					  }
    				);
    				
    				if(tp_lotto != 'LNA'){
    					voci_menu.push(
    						{
    				      		text: 'Report riferimenti programma',
    				    		iconCls : 'icon-print-16',
    				    		handler: function() {
    				    			window.open('acs_report_riferimenti_produzione.php?filtro_c=' + rec.get('liv_c') + '&filtro_type='+ rec.get('liv_type'));
    							}
    					  }
    				);
    				}
    			}
    				
    			if (liv == 'liv_1') {	
    				voci_menu.push(
    						{
    				      		text: 'Genera/Rilascia programma di produzione',
    				    		iconCls : 'iconFabbrica',
    				    		handler: function() {
    				    		
    				    			var my_listeners = {
    				    				recordSelected: function(from_win, progetto){
    				    					from_win.destroy();
    				    					//apro pagina di generazione progetto
    				    					acs_show_win_std('Generazione programma di produzione',
    					    				  'acs_panel_indici_nuovo_progetto.php?fn=open_form', {
    					    				  	lotto: rec.get('lotto'),
    					    				  	data_eva : rec.get('data_evasione'),
    					    					codice_progetto: progetto.codice,
    					    					desc_progetto : progetto.desc
    				    				   	}, 800, 580, my_listeners, 'iconFabbrica');
    				    				}
    				    			};
    				    			
    				    			acs_show_win_std('Seleziona tipologia programma di produzione',
    				    				'acs_panel_indici.php?fn=open_indici', {
    				    					seleziona: 'Y',
    				    					modifica: 'N'
    			    				 	}, 600, 380, my_listeners, 'iconFabbrica');
    				    		}
    					  }
    				);
    				
    				
    				
    				/*voci_menu.push(
    						{
    				      		text: 'PDF - Etichette di Produzione',
    				    		iconCls : 'icon-print-16',
    				    		handler: function() {
    				    			acs_show_win_std('Etichette di Produzione',
    				    				'acs_pdf_etichette_produzione.php?fn=open_form_parameters', {
    				    					filtro_semplificato: 'Y',
    				    					open_request: {
    				    						filtro_c:    rec.get('liv_c'),
    				    						filtro_type: rec.get('liv_type')
    				    					}
    			    				 }, 600, 380, {}, 'icon-info_blue-16');
    				    		}
    					  }
    				);
    				voci_menu.push(
    						{
    				      		text: 'PDF - Riepilogo lotti con righe',
    				    		iconCls : 'icon-print-16',
    				    		handler: function() {
    				    			acs_show_win_std('Etichette di Produzione',
    				    				'acs_pdf_riepilogo_lotti_rd.php?fn=open_form_parameters', {
    				    					filtro_semplificato: 'Y',
    				    					open_request: {
    				    						filtro_c:    rec.get('liv_c'),
    				    						filtro_type: rec.get('liv_type')
    				    					}
    			    				 	}, 600, 380, {}, 'icon-info_blue-16');
    				    		}
    					  }
    				);*/
         
    				/********************************************************************
    				voci_menu.push({
                  	  text: 'Report - Ordine Lotto',
                	  iconCls : 'iconPrint',
                	  handler: function() {
    					acs_show_panel_std('acs_report_pdf.php?fn=print_lotto', null, {
    						qtip_spedizione: rec.get('liv_c'),
    						lotto: rec.get('lotto')
    					});
            		  }
            		});
        			voci_menu.push({
                  	  text: 'Report - Righe Ordine Lotto',
                	  iconCls : 'iconPrint',
                	  handler: function() {
                	  	acs_show_panel_std('acs_report_pdf.php?fn=print_righe', null, {
    						qtip_spedizione: rec.get('liv_c'),
    						lotto: rec.get('lotto')
    					});
            		  }
            		});
            		*********************************************************************/
    			}
    			
    
    
    		var menu = new Ext.menu.Menu({
                	items: voci_menu
    			}).showAt(event.xy);
    		return false;
      	} //show_menu_dx
    }
 } //treepanel
}
<?php } exit; ?>