<?php

require_once "../../config.inc.php";
$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();
$k_ordine = $m_params->k_ordine;

$ord =  $main_module->get_ordine_by_k_docu($k_ordine);
$oe = $main_module->k_ordine_td_decode_xx($k_ordine);
$ordine = "{$oe['TDOADO']}_{$oe['TDONDO']}";

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_tab'){

        ?>
	{
		success:true,
		m_win: {
			title: 'Elenco corrente colli ordine ' + <?php echo j($ordine); ?>,
			height: 500, width: 1200,
			iconCls : 'icon-barcode'
		},
		items: [
					{
						xtype: 'grid',
						title: '',
						flex: 2,
						loadMask: true,	
				        stateful: true,
                    	stateId: 'elenco_packing_list',
                        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
                   
        				features: [
        					{ftype: 'grouping',  startCollapsed: true, groupHeaderTpl: '{[values.rows[0].data.groupfield_header]}'},
                    		{
                    			ftype: 'filters', encode: false, local: true
                    		}
                    	],
				        
				        store: {
						xtype: 'store',
						autoLoad:true,
			            proxy: {
                            url: '../desk_vend/acs_get_order_cols.php?k_ord=' + <?php echo j($m_params->k_ordine); ?> + '&trad=Y',
                            type: 'ajax',
                            reader: {
                                type: 'json',
                                root: 'root'
                            }
                         },
		        			     fields: [
                            'PLPNET', 'PLPLOR', 'PLRIGA', 'PLVOLU', 'PLAADO', 'PLNRDO', {name: 'PLCOAB', type: 'int'},
                            'PLDART', 'PLUM', 'PLQTA', 'PLDTEN', 'PLTMEN', 'PLDTUS', 'PLTMUS', 'PLDTRM', 'PLTMRM',
                            'PSDTSC', 'PSTMSC', 'PSRESU', 'PSCAUS', 'tooltip_scarico', 'PSPALM', 'PSLATI', 'PSLONG',
                            'PLART', 'PLRCOL','ALDAR1', 'TADESC', 'TDRFLO', 'PLNCOL', 'gr_collo'
                                ]						
									
			}, //store
			
			 
			 <?php $trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=15 >"; ?>
			
				columns: [	
				
				   {
      			                header   : 'Riga', align: 'right',
      			                dataIndex: 'PLRIGA', 
      			                width    : 35
      			             },{
    			                header   : 'Nr collo', align: 'right',
    			                dataIndex: 'PLCOAB', 
    			                flex     : 20
    			             }, {
    			                header   : 'Codice',
    			                dataIndex: 'PLART', 
    			                width    : 100
    				         },{
    			                header   : 'Descrizione',
    			                dataIndex: 'PLDART', 
    			                flex    : 100
    			             },{
    			            	text: '&nbsp;<br><?php echo $trad; ?>',
    			                dataIndex: 'ALDAR1',
    			                tooltip: 'Traduzione',
    			                menuDisabled: true,
    			                width : 30,
    			                renderer: function(value, metaData, record){
    				    			  if(!Ext.isEmpty(record.get('ALDAR1')) || !Ext.isEmpty(record.get('TADESC'))){
    										if (record.get('PLART').trim() == record.get('PLRCOL').trim())	    			    	
    											metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('TADESC')) + '"';
    					    			  	else
    					    			  		metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('ALDAR1')) + '"';  			    
    					    			  
    					    			 return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=15>';
    				    			  }else{
    					    			 return '';
    				    			  }
    				    			  
    				    		  }
    			             }, {
    			                header   : 'UM',
    			                dataIndex: 'PLUM', 
    			                flex    : 10
    			             }, {
    			                header   : 'Quantit&agrave;',
    			                dataIndex: 'PLQTA', 
    			                flex    : 30,
    			                align: 'right',
    			                renderer : floatRenderer2
    			             },{
    			                header   : 'Volume',
    			                dataIndex: 'PLVOLU', 
    			                flex    : 30,
    			                align: 'right',
    			                renderer : floatRenderer3
    			             }, {
    			                header   : 'Disponibilit&agrave;',
    			                dataIndex: 'PLDTEN', 
    			                flex    : 30,
    			                renderer: function(value, p, record){
    			                	return datetime_from_AS(record.get('PLDTEN'), record.get('PLTMEN'))
    			    			}
    			             }, {
    			                header   : 'Spedizione',
    			                dataIndex: 'PLDTUS', 
    			                flex    : 30,
    			                renderer: function(value, p, record){
    			                	return datetime_from_AS(record.get('PLDTUS'), record.get('PLTMUS'))
    			    			}			                
    			             }, {
    			                header   : 'Aggiunte',
    			                dataIndex: 'PLDTRM', 
    			                flex    : 30,
    			                renderer: function(value, p, record){
    			                	return datetime_from_AS(record.get('PLDTRM'), record.get('PLTMRM'))
    			    			}			                			                
    			             }, {
    				                header   : 'Scarico',
    				                dataIndex: 'PSDTSC', 
    				                flex    : 30,
    				                renderer: function(value, metaData, record){
    				                	metaData.tdAttr = 'data-qtip="Scanner: ' + Ext.String.htmlEncode(record.get('PSPALM')) + ' - Coordinate: ' + Ext.String.htmlEncode(record.get('PSLATI')) + ' - ' + Ext.String.htmlEncode(record.get('PSLONG')) + '"';
    				                	return datetime_from_AS(record.get('PSDTSC'), record.get('PSTMSC'))
    				    			}			                			                
    				          }, {
    				                header   : 'Info',
    				                dataIndex: 'PSDTSC', 
    				                width: 50,
    				                renderer: function(value, metaData, record){

    				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)
    				                		metaData.tdAttr = 'data-qtip="Segnalazione allo scarico: ' + Ext.String.htmlEncode(record.get('PSCAUS')) + '"';
    			                		
    				                	if (Ext.isEmpty(record.get('PSRESU'))==false && record.get('PSRESU') == 'E')			    	
    							    		return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
    				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)			    	
    							    		return '<img src=<?php echo img_path("icone/48x48/comments.png") ?> width=18>';
    				    			}			                			                
    				          },
    				          {
        			                header   : 'Peso netto',
        			                dataIndex: 'PLPNET', 
        			                flex    : 30,
        			                align: 'right',
        			                renderer : floatRenderer3
        			             },
        			             {
          			                header   : 'Peso lordo',
          			                dataIndex: 'PLPLOR', 
          			                flex    : 30,
          			                align: 'right',
            			            renderer : floatRenderer3
          			             },
          			             <? echo dx_mobile() ?>
				   
	         ] //columns
	    
	         
	         ,  listeners: {
    			        	 celldblclick: {
    			        		  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
    				   		            
    					           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
    								  	rec = iView.getRecord(iRowEl);

    								  	if(Ext.isEmpty(rec.get('TDRFLO'))){
    										var lingua = 'lingua non definita';
    										var cod_lingua = null;
    									 }else{
    										var lingua = rec.get('TDRFLO').trim();
    										var cod_lingua = rec.get('TDRFLO').trim();
    									}

    									if(rec.get('PLART').trim() == rec.get('PLRCOL').trim())
    										var gest_type = 'C';
    									else
    										var gest_type = 'A';

    			    				    if(col_name == 'ALDAR1' && !Ext.isEmpty(cod_lingua))	
    			    					   acs_show_win_std('Traduzione in ' + lingua, 
    					    					'acs_gest_trad_colli_articoli.php?fn=open_tab', 
    					    					{codice: rec.get('PLART'), desc : rec.get('PLDART'), lng: lingua, type : gest_type}, 400, 270,  {
    				        					'afterSave': function(from_win){
    				        						iView.getStore().load();
    	 											from_win.close();
    				        					}
    				        				}, 'icon-globe-16');
    			    				
    				            	}

    				        	 }
   				        	  , itemcontextmenu : function(grid, rec, node, index, event) {	
									
									event.stopEvent();
									var grid = this;
				             		var rows = grid.getSelectionModel().getSelection();
					  				
									var voci_menu = [];
									
									voci_menu.push({
						         		text: 'Elenco completo articoli/componenti',
						        		iconCls : 'icon-leaf-16',          		
						        		handler: function() {
						        			acs_show_win_std(null, '../desk_utility/acs_elenco_riepilogo_art_comp.php?fn=open_elenco', 
								        				{k_ordine: <?php echo j($m_params->k_ordine); ?>, riga : rec.get('PLRIGA'), collo : rec.get('PLNCOL'), gr_collo : rec.get('gr_collo'), peso_n : rec.get('PLPNET')});   	     	
						        		}
						    		});
						    		
						    		var menu = new Ext.menu.Menu({
						            items: voci_menu
							       }).showAt(event.xy);	
										
										
										
							}

    				         }, 
				 
			
		
		  } //grid
		
		]}
		
		
		<?php 
		exit;
}