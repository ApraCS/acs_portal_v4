<?php
require_once("../../config.inc.php");
/*require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_todolist_preview_include.php';*/

$main_module = new DeskProd();
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

/*$pp_colli = _pp_get_colli($main_module, $m_params);
$ar_colli = $pp_colli['ar_colli'];
$rec = $ar_colli[0];*/

$title = "Segnalazioni";

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_panel'){
    //----------------------------------------------------------------------
    
    ?>
	{
		success:true,	
		m_win: {
			title: <?php echo j($title); ?>,
			iconCls : 'icon-warning_black-16',
			width: 500, height: 300,
		},
		items: [
		
		{
		        xtype: 'form',
		        bodyStyle: 'padding: 10px',
                bodyPadding: '5 5 0',
                frame: false,
		        items: [
		        {
					name: 'f_nconf',
					anchor: '-15',
					xtype: 'combo',
					fieldLabel: 'Non conformit&agrave;',
					displayField: 'text',
					valueField: 'id',
					emptyText: '- seleziona -',
					forceSelection: true,
				   	allowBlank: true,		
				    multiSelect: true,						   													
					store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
						     <?php echo acs_ar_to_select_json($main_module->find_TA_std('NCPRP', null, 'N', 'N', null, null, null, 'N', 'Y'), ""); ?>
						    ]
					}						 
				 },
				 
				 {
					xtype: 'textfield',
					fieldLabel: 'Note',
					anchor: '-15',
					name: 'f_note',
					maxLength : 50
				},
		        
		        
		        ],
	
		        buttons: [{
			            text: 'Conferma',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',	                     
			            handler: function() {
			            this.up('window').destroy();
			            
			                           
			            }
			        }
		        ]
		    }
				
		
		]
	}
	
	<?php 
    
}

