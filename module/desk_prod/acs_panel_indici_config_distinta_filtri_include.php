<?php

//Combo Includi/Escludi
function _df_IE($field, $values = array(), $options = array()){
    if (isset($values[$field]))
        $initial_value = $values[$field];
    else
        $initial_value = 'I';
    
    $initial_data = array(
            array('id' => 'I', 'text' => 'Includi'),
            array('id' => 'E', 'text' => 'Escludi')
        );
    
    if ($options['add_solo'] == 'Y'){
        $initial_data[] = array('id' => 'S', 'text' => 'Solo');
    }
    
    if (isset($options['label'])){
        $fieldLabel = $options['label'];
    } else {
        $fieldLabel = 'Includi/Escludi';
    }
        
    $ret = extjs_combo(array(
        'name'          => $field,
        'fieldLabel' => $fieldLabel,
        'initialData' => $initial_data,
        'initialValue' => $initial_value
        )
    );
    return $ret;
}


//textfield per form
function _df_tf($label, $field, $values = array()){    
    $ret = array(
        'xtype'         => 'textfield',
        'fieldLabel'    => $label,
        'name'          => $field,
        'value'         => $values[$field]
    );
   return $ret;  						
}


//a blocchi (es. per codici articolo)
function _df_cod_char($length, $label, $field, $values = array()){
    return extjs_cod_char($length, $label, $field, $values);
}









//combo field
function _df_cf($label, $field, $values = array(), $combo_data = array()){
    $ret = extjs_combo(array(
        'fieldLabel'    => $label,
        'name'          => $field,
        'initialData'  => $combo_data,
        'initialValue' => $values[$field],
        'queryMode'     => 'local',
        'allowBlank'    => true
    ));
    return $ret;
}


//$sezione: es: PL o RP
function _distinta_filtri_reset($nome_file, $lista, $sezione){
  global $conn, $id_ditta_default;
  
    //verifio se ci sono gia' dei dati
      $sql = "SELECT COUNT(*) AS C_ROW FROM {$nome_file} WHERE CLDT = ? AND CLCDLI = ? AND CLSEQU = ?";
      $stmt = db2_prepare($conn, $sql);
      echo db2_stmt_errormsg();
      $result = db2_execute($stmt, array($id_ditta_default, $lista, $sezione));
      echo db2_stmt_errormsg($stmt);
      $row = db2_fetch_assoc($stmt);
      $n_record = $row['C_ROW'];
      db2_free_stmt($stmt);
       
      if ($n_record > 0){
        //elimino i dati attuali
        $sql = "DELETE FROM {$nome_file} WHERE CLDT = ? AND CLCDLI = ? AND CLSEQU = ?";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($id_ditta_default, $lista, $sezione));
        echo db2_stmt_errormsg($stmt);
      }
}

function _distinta_filtri_save_params($nome_file, $lista, $sezione, $form_values){
    global $conn, $id_ditta_default;
    
    //ricompongo i campi char
    
    foreach((array)$form_values as $kf => $v){
        //i record da salvare sono quelli la cui chiave inizia per PL o IE_PL_
        if ((substr($kf, 0, 3) == "{$sezione}_"|| substr($kf, 0, 6) == "IE_{$sezione}_") && strlen(rtrim($v)) > 0){
            
            $ar_ins = array();
            $ar_ins['CLDT'] = $id_ditta_default;
            $ar_ins['CLCDLI'] = $lista;
            $ar_ins['CLCELE'] = $kf;
            $ar_ins['CLSEQU'] = $sezione;
            $ar_ins['CLRSTR'] = $v;
            $sql = "INSERT INTO {$nome_file}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            db2_free_stmt($stmt);
        }
    }
}


function _distinta_filtri_load_values($nome_file, $lista, $sezione){
    global $conn, $id_ditta_default;
    
    //verifio se ci sono gia' dei dati
    $sql = "SELECT * FROM {$nome_file} WHERE CLDT = ? AND CLCDLI = ? AND CLSEQU = ?";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_ditta_default, $lista, $sezione));
    echo db2_stmt_errormsg($stmt);
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)){
        $ar[trim($row['CLCELE'])] = rtrim($row['CLRSTR']);
    }
    return $ar;
}
