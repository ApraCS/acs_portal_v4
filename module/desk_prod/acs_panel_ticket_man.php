<?php
require_once("../../config.inc.php");
require_once 'acs_panel_prog_prod_include.php';
 
$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

//verifico se e' stato impostato il link per MApp/Sud2 (in config.inc)
if (!isset($mapp_sud2_url)){
    $mapp_sud2_url = '/Mapp';
}
?>
{"success":true, 
	m_win: {
		title: 'Ticket manutenzione',
		width: 600, height: 380,
		iconCls : 'icon-bluetooth_blue-16',
	}, 
	items: [
		{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            layout: 'hbox',
            defaults: {xtype: 'button', scale: 'large', flex: 1},
            items: [
            	{
            		text: 'Dashboard',
            		handler: function(){
            		
            		acs_show_panel_std('acs_panel_dashboard_ticket.php?fn=open_panel');
            		this.up('window').destroy();
            			     
            		}
            	}, 
            	{
            		text: 'Mobile',
            		handler: function(){
            			//apro in nuovo window browser
            			var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'GET',
								        url: <?php echo j($mapp_sud2_url) ?>,
								        params: 'auto_login=true&auto_p=ticket_man&auto_a=w_start&openAS=SimpleWinPage&mobile=Y&title=Dichiarazione%20Manutenzioni'								        
								        });	      
            		}
            	}
            ]
        }
	]
}