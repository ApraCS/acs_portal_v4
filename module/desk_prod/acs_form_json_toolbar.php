<?php
require_once "../../config.inc.php";
$main_module = new DeskProd();

$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
			cls: 'acs_toolbar',
			tbar: [
		 	 		 {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Programmi produzione',
			            width: 400, 
			            items: [
    			        	{
    			                text: '<br>Linee<br>collo',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								acs_show_panel_std('../desk_utility/acs_tab_sys_LIPC.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'LIPC'});	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: '<br>Loghi',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								    acs_show_panel_std('acs_gest_LOGOP.php?fn=open_tab', 'panel_gestione_LOGOP');          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			               {
    			                text: '<br>Priorit&agrave;',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    			                        acs_show_panel_std('acs_gest_PRIOL.php?fn=open_tab', 'panel_gestione_PRIOL');
    								    panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            
    			            {
			                xtype:'splitbutton',
			                iconCls: 'icon-database_active-32',
			                text: '<br>Uscite',
			                scale: 'large',
			                iconAlign: 'top',
			                arrowAlign:'bottom',
			                menu: {
			                	xtype: 'menu',
		        				items: [	
		        				  	
            						   {
    			                text: 'Tipo',
    			                 iconCls: 'icon-database_active-16',
    			                scale: 'small',			                 
    			                handler : function() {
    			                 	acs_show_panel_std('acs_gest_CDTPU_tipo_uscita.php?fn=open_tab', 'panel_gestione_CDTPU');	          	                	
    							        panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: 'Formato',
    			                iconCls: 'icon-database_active-16',
    			                scale: 'small',		                
    			                handler : function() {
    			                     acs_show_panel_std('acs_gest_CDFMU_formato_uscita.php?fn=open_tab', 'panel_gestione_CDFMU');
    							        panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: 'Avanzamento',
    			                iconCls: 'icon-database_active-16',
    			                scale: 'small',		                
    			                handler : function() {
    			                     acs_show_panel_std('acs_gest_CDTAV_tipo_avanzamento.php?fn=open_tab', 'panel_gestione_CDTAV');
    							        panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: 'Attivit&agrave;',
    			                iconCls: 'icon-database_active-16',
    			                scale: 'small',				                
    			                handler : function() {
    			                     acs_show_panel_std(
			           					'../desk_vend/acs_gestione_attav.php?fn=open_grid',
			           					null, {sezione: <?php echo acs_je($cfg_mod['ATTAV_TAKEY2']) ?>});	
    							        this.up('buttongroup').up('panel').up('window').destroy();
    				           		  } //handler function()
    			            },
    			            
    			            
    			            {
    			                text: 'Ciclo di vita - Desktop Produzione',
    			                iconCls: 'icon-database_active-16',
    			                scale: 'small',				                
    			                handler : function() {
    			                     acs_show_panel_std(
			           					'../desk_vend/acs_gest_attav.php?fn=open_tab',
			           					null, {sezione: ['PP', 'PPA', 'PPR']});	
    							        this.up('buttongroup').up('panel').up('window').destroy();
    				           		  } //handler function()
    			            }
    			            
		        			]
			                
			                }
			            },    
    			            {
    			                text: '<br>Genera<br>ordini',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    			                 		acs_show_panel_std('acs_gest_CDGED_generazione_documenti.php?fn=open_tab', 'panel_gestione_CDGED');   
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: '<br>Non <br> conform.',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								acs_show_panel_std('acs_gest_NCPRP.php?fn=open_tab', 'panel_gestione_NCPRP');	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			             {
    			                text: '<br>Lavorazioni',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								acs_show_panel_std('acs_gest_PROLV.php?fn=open_tab', 'panel_gestione_PROLV');	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			             {
    			                text: '<br>Fasi',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								acs_show_panel_std('acs_gest_PROFS.php?fn=open_tab', 'panel_gestione_PROFS');	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			        ]
    			     }
    			     , {
			            xtype: 'buttongroup',
			            columns: 50,
			            width: 300,
			            title: 'Risorse',
			            items: [
			            {
    			                text: '<br>Linee<br>produzione',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								acs_show_panel_std('acs_gest_LIPRO.php?fn=open_tab', 'panel_gestione_tabelle', {taid: 'LIPRO'});	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
			               {
			                text: '<br>Reparti<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_anag_man_rep.php?fn=open_tab', 'panel_gestione_REPMA');
								panel = this.up('buttongroup').up('panel').up('window');
    				            panel.close();
								} //handler
			            }	,
			              {
			                text: '<br>Macchine<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_anag_man_mac.php?fn=open_tab', 'panel_gestione_MACMA');
								panel = this.up('buttongroup').up('panel').up('window');
    				            panel.close();
								} //handler
			            }	,
			            {
    			                text: '<br>Operatori',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								acs_show_panel_std('acs_gest_PROPE.php?fn=open_tab', 'panel_gestione_PROPE');	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: '<br>Mansioni',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								acs_show_panel_std('acs_gest_PRGRO.php?fn=open_tab', 'panel_gestione_PRGRO');	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            }		            
			            ]
			        }
			        
			         , {
			            xtype: 'buttongroup',
			            columns: 50,
			            width: 150,
			            title: 'Manutenzioni',
			            items: [
			               {
    			                text: '<br>Ciclo<br>di vita',
    			                scale: 'large',			                 
    			                iconCls: 'icon-database_active-32',
    			                iconAlign: 'top',			                
    			                 handler : function() {
    								acs_show_panel_std('acs_gest_MNCIV.php?fn=open_tab', 'panel_gestione_tabelle_MNCIV', {taid: 'MNCIV'});	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			                {
			                xtype:'splitbutton',
			                iconCls: 'icon-database_active-32',
			                text: '<br>Interventi',
			                scale: 'large',
			                iconAlign: 'top',
			                arrowAlign:'bottom',
			                menu: {
			                	xtype: 'menu',
		        				items: [	
		        				  	
            						   {
    			                text: 'Tipologia',
    			                iconCls: 'icon-database_active-16',
    			                scale: 'small',			                 
    			                handler : function() {
    			                 		acs_show_panel_std('acs_gest_MNTIP.php?fn=open_tab', 'panel_gestione_tabelle_MNTIP', {taid: 'MNTIP'});	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: 'Priorit&agrave;',
    			                iconCls: 'icon-database_active-16',
    			                scale: 'small',		                
    			                handler : function() {
    			                     	acs_show_panel_std('acs_gest_MNPRI.php?fn=open_tab', 'panel_gestione_tabelle_MNPRI', {taid: 'MNPRI'});	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: 'Componenti',
    			                iconCls: 'icon-database_active-16',
    			                scale: 'small',		                
    			                handler : function() {
    			                     	acs_show_panel_std('acs_gest_MNCOM.php?fn=open_tab', 'panel_gestione_tabelle_MNCOM', {taid: 'MNCOM'});	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            },
    			            {
    			                text: 'Utenti abilitati',
    			                iconCls: 'icon-database_active-16',
    			                scale: 'small',		                
    			                handler : function() {
    			                     	acs_show_panel_std('acs_gest_UTABI.php?fn=open_tab', 'panel_gestione_tabelle_UTABI', {taid: 'UTABI'});	          	                	                
    				           			panel = this.up('buttongroup').up('panel').up('window');
    				           			panel.close();
    								} //handler function()
    			            }
		        				 ]
			                
			                }
			            }
			               	            
			            ]
			        }
			]
		}
	]		
}            