<?php
require_once("../../config.inc.php");
require_once 'acs_panel_prog_prod_include.php';
 
$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//----------------------------------------------------------------------

$id           = $m_params->open_request->id;
$data_evas    = $m_params->open_request->data_evasione;
$carico       = explode('_', $id);
$tp_carico    = $carico[0];
$anno_carico  = substr($carico[1], 0, 4);
$nr_carico    = $carico[2];

?>
{"success":true, 
	m_win: {
		title: 'Assegnazione LOTTO di Produzione ' + <?php echo acs_je("(Ordini inclusi: "  . count($m_params->open_request->id_ordini) .")"); ?>,
		width: 600, height: 380,
		iconCls: 'iconFabbrica'
	}, 
	"items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            items: [{
   					xtype: 'datefield',
					fieldLabel: 'Data Evasione',
					value: '<?php echo print_date($data_evas); ?>',
					labelWidth : 430,		//120
					labelAlign: 'right',	//left
					//flex: 1,
					anchor: '-5',
					name: 'f_data_evasione',
    			    allowBlank: false,
    			    disabled: true
            	},{
   					xtype: 'numberfield',
					fieldLabel: 'Anno',
					value: <?php echo date('Y')?>,
					labelWidth : 120,
					width : 200,
					name: 'f_anno_lotto',
					minLength : 4, maxLength : 4,
					forceSelection: true,
    			    allowBlank: false,
    			    hideTrigger: true
            	}, {
            		xtype: 'container',
            		flex: 1,
            		layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
            		title: null,
            		items:[{
            			xtype: 'label',
				        text: 'Tipo',
				        width: 105,
				        margin: '5 0 0 0',
            		}, {
        				flex: 1,
        				emptyText: '- seleziona -',
        				name: 'f_tipo_lotto',
        				xtype: 'combo',
        				labelAlign: 'left',
        				fieldLabel: null,
        				anchor: '-5',
        				margin: '5 0 5 20',
        				displayField: 'f_tipo',
        				valueField: 'f_tipo',
        				forceSelection:true,
        				multiSelect: false,
        			   	allowBlank: false,
        				store: {
            				xtype: 'store',
            				autoLoad:true,
            				proxy: {
            					url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tipo',
            					method: 'POST',
            					type: 'ajax',

            					//Add these two properties
            					actionMethods: {read: 'POST'},
            					reader: {type: 'json', method: 'POST', root: 'root'},
            					
        					    extraParams: {},
        	 					doRequest: personalizza_extraParams_to_jsonData
            				},
            				
            				fields: [{name:'f_tipo'}]
        				}
    				}]
				}, {
					xtype: 'numberfield',
					fieldLabel: 'Numero Lotto',
					labelWidth : 120,
					width : 200,
					name: 'f_num_lotto',
					maxLength : 6,
					forceSelection: true,	
    			    allowBlank: false,
    			    hideTrigger: true
				}, {
					xtype: 'textfield',
					fieldLabel: 'Descrizione Lotto',
					labelWidth : 120,
					flex : 1,
					anchor: '-5',
					name: 'f_descr_lotto',
					maxLength : 50,
					forceSelection: true,	
    			    allowBlank: false
				},
				{
				    xtype: 'combo',
    				flex: 1,
    				emptyText: '- seleziona -',
    				name: 'f_prio_lotto',
    				fieldLabel: 'Priorit&agrave; lotto',
    				anchor: '-5',
    				labelWidth : 120,
    				displayField: 'text',
    				valueField: 'id',
    				forceSelection:true,
    				allowBlank: false,
    				store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [
						    <?php echo acs_ar_to_select_json($main_module->find_TA_std('PRIOL', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
						    ]
						}
    			}
            ],
            
			buttons: [{
		           // text: '<b>Conferma</b>',
		            text: '<div style="font-size: 20px; font-weight: bold;">Conferma</div>',  
		            iconCls: 'icon-fabbrica-32',
			       	scale: 'large',		            
		            handler: function() {
		            	var form = this.up('form').getForm(),
			                loc_win = this.up('window');
			                
		               if (form.isValid()){
							Ext.Ajax.request({
 						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_lotto',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   form_values: form.getValues(),
 			        			   open_request: <?php echo acs_je($m_params->open_request); ?>
 								},
 								
 								success: function(response, opts) {
						        	 var jsonData = Ext.decode(response.responseText);
						        	 
						        	 
						        	 if (jsonData.ret_RI.RIESIT == 'E'){
									 	acs_show_msg_error('Errore in fase di elaborazione messaggio');
									 	return;
									 }						        	 
						        	 
						        	 if (jsonData.ret_RI.RIESIT == 'W'){
									 	acs_show_msg_error(jsonData.ret_RI.RINOTE);
									 	return;
									 }
									 									 
									 acs_show_msg_info('Operazione completata');
									 loc_win.fireEvent('afterOk', loc_win);
			            		   
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
			            }
		            }
		        }
	        ]
	  }
]}
<?php 
	exit;
}

// ******************************************************************************************
// GET JSON DATA TIPO (per combo)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_tipo'){
    // TIPO LOTTO
    $sql = "SELECT DISTINCT TDTPLO
			FROM {$cfg_mod_Spedizioni['file_testate']}
			ORDER BY TDTPLO
		";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ar = array();
    
    while ($row = db2_fetch_assoc($stmt)){
        $r = array();
        $r['f_tipo'] = $row['TDTPLO'];
        $ar[] = $r;
    } //while

    echo acs_je($ar);
    exit;
}

// ******************************************************************************************
// GET JSON DATA ASSEGNA LOTTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_assegna_lotto'){
    $ritime     = microtime(true);
    $carichi    = $m_params->open_request->id_carichi;
    $ordini     = $m_params->open_request->id_ordini;
    $data_evas  = $m_params->open_request->data_evasione;
    
    foreach($ordini as $v){
        
        $ar_upd = array();
        $ar_upd['RATIME'] = $ritime;
        
        $oe = $s->k_ordine_td_decode_xx($v);
        
        $ar_upd['RATIDO'] = $oe['TDOTID'];
        $ar_upd['RAINUM'] = $oe['TDOINU'];
        $ar_upd['RAAADO'] = $oe['TDOADO'];
        $ar_upd['RANRDO'] = $oe['TDONDO'];
        $ar_upd['RADT']   = $oe['TDDT'];

        $sql = "INSERT INTO {$cfg_mod_DeskProd['file_richieste_prod']} (" . create_name_field_by_ar($ar_upd) . ") VALUES (" . create_parameters_point_by_ar($ar_upd) . ")";

        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        echo db2_stmt_errormsg($stmt);
    }
    
    $sh = new SpedHistory($main_module);
    $ret_RI = $sh->crea(
        'pers',
        array(
            "RITIME"    => $ritime,
            "messaggio"	=> 'ASS_LOTTO_PROD',
            "vals" => array(
                'RIDTEP' => $data_evas,
                'RITPLO' => $m_params->form_values->f_tipo_lotto, 
                'RIAALO' => $m_params->form_values->f_anno_lotto, 
                'RINRLO' => $m_params->form_values->f_num_lotto,
                'RIDESC' => $m_params->form_values->f_descr_lotto,
                'RIPRIO' => $m_params->form_values->f_prio_lotto
            )
        )
    );
    
    $ret = array();
    $ret['success'] = true;
    $ret['ret_RI'] = $ret_RI;
    echo acs_je($ret);
    exit;
}