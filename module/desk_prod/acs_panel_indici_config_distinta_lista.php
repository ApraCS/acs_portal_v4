<?php

require_once("../../config.inc.php");

$main_module = new DeskProd();

$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    'tab_name' =>  $cfg_mod['file_cfg_distinte_liste'],
    'descrizione' => 'Gestione lista opzioni',
    'f_ditta' => 'CLDT',
    'f_desc'  => 'CLDESC',
    'f_lista' => $m_params->lista,
    'fields_key' => array('CLCDLI', 'CLCELE'),
    
    'fields' => array(
        'CLCDLI' => array('label'	=> 'Lista',     'fw'=>'width: 90'),
        'CLSEQU' => array('label'	=> 'Seq',       'fw'=>'width: 50', 'tooltip'=>'Sequenza'),
        'CLCELE' => array('label'	=> 'Opzione',   'fw'=>'width: 90'),
        'CLDESC' => array('label'	=> 'Descrizione'),
        'CLSTIN' => array('label'	=> 'Da',       'fw'=>'width: 50'),
        'CLSTFI' => array('label'	=> 'A',        'fw'=>'width: 50'),
        'CLRSTR' => array('label'	=> 'Radice',   'fw'=>'width: 50', 'tooltip'=>'Radice stringa codice'),
        'CLNEWC' => array('label'	=> 'Nuovo',    'fw'=>'width: 50', 'tooltip'=>'Y: Attiva la generazione di un nuovo codice articolo'),
        'CLPROG' => array('label'	=> 'Ultimo ID','fw'=>'width: 60', 'tooltip'=>'Ultimo suffisso automatico assegnato'),
        'CLNCAR' => array('label'	=> 'Nr ID',    'fw'=>'width: 50', 'tooltip'=>'Nr caratteri suffisso automatico'),
        'CLARDU' => array('label'	=> 'Duplica',  'fw'=>'width: 90', 'tooltip'=>'Articolo da duplicare per nuovo codice')
    ),
    
    'tasto_dx' => array('elenco_stringe' => 'Y', 'from_cl' => 'Y'),
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
