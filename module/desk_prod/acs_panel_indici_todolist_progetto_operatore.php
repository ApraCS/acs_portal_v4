<?
require_once("../../config.inc.php");
require_once("../base/_rilav_g_history_include.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $main_module->get_cfg_mod();
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'open_grid'){ ?>
{"success":true, 
	m_win: {
		title: 'Elenco operatori',
		iconCls : 'icon-user_only-16'
	}, 
"items": [
    {
			xtype: 'grid',
	        loadMask: true,
        	autoScroll : true,
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],		
            	store: {
    			editable: false,
    			autoDestroy: true,
    			fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
	     	<?php echo acs_ar_to_select_json(find_TA_std('PROPE'), ''); ?>	
    		    ]
			}, //store
					
			      columns: [	
			       {
	                header   : 'Codice',
	                dataIndex: 'id',
	                width : 70,
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'text',
	                flex : 1
	                }
	         ], listeners: {
	         
	         celldblclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
	         		var rec = iView.getRecord(iRowEl);
	         		var m_win = this.up('window');
	         		
	         		Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_mansione',
				        jsonData: {
				       		 operatore : rec.get('id')
				        },
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        success : function(result, request){	            	  													        
				            var jsonData = Ext.decode(result.responseText);
				            if(jsonData.open_win == 'Y'){
				            	acs_show_win_std(null, 'acs_panel_indici_todolist_progetto_operatore.php?fn=open_mans', {operatore : rec.get('id')}, 400, 300, my_listeners, 'icon-leaf-16');
				            }else{
				            	acs_show_panel_std('acs_panel_indici_todolist_progetto.php?fn=open_panel', null, {
       							mansione : jsonData.mansione});  
				            }
				            m_win.destroy();
				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });	  
	         	
	         	
	         	}
	         
	     
			
		}
		
		}
	    
			
     
]} 

<?php 
exit;
}

if ($_REQUEST['fn'] == 'get_mansione'){ 
   
    $ret = array();
    
    $row_mans = $s->find_TA_std('PRGRU', null, 'N', 'N', $m_params->operatore);
    if(count($row_mans) > 1){
        $ret['open_win'] == 'Y';
    }else{
        $ret['mansione'] = $row_mans[0]['id'];
    }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'open_mans'){ 
    
    ?>
{"success":true, 
	m_win: {
		title: 'Elenco mansioni operatore '  + <?php echo j($m_params->operatore); ?>,
		iconCls : 'icon-user_only-16'
	}, 
"items": [
    {
			xtype: 'grid',
	        loadMask: true,
        	autoScroll : true,
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],		
            	store: {
    			editable: false,
    			autoDestroy: true,
    			fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
	     	<?php echo acs_ar_to_select_json($s->find_TA_std('PRGRU', null, 'N', 'N', $m_params->operatore), ''); ?>	
    		    ]
			}, //store
					
			      columns: [	
			       {
	                header   : 'Codice',
	                dataIndex: 'id',
	                width : 70,
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'text',
	                flex : 1
	                }
	         ], listeners: {
	         
	         celldblclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
	         		var rec = iView.getRecord(iRowEl);
	         		var m_win = this.up('window');
         			acs_show_panel_std('acs_panel_indici_todolist_progetto.php?fn=open_panel', null, {
       				mansione : rec.get('id')});  
	         	}
	         
	     
			
		}
		
		}
]} 

<?php 
exit;
   
}

