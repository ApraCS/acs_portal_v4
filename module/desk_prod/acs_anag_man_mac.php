<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */
require_once("../base/acs_gestione_nota_nt_include.php");

$m_params = acs_m_params_json_decode();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$main_module =  new DeskProd();
$prod_module = $main_module->get_cfg_mod();
$cfg_mod = $desk_art->get_cfg_mod();

$m_table_config = array(
    'module'      => $desk_art,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "MACMA - Macchine {$m_params->TAKEY2}",
    'descrizione' => "Gestione macchine",
    'form_title' => "Dettagli tabella macchine",
    'fields_preset' => array(
        'TATAID' => 'MACMA'
    ),
    'field_NOTE' => 'TAKEY1',   //SOLO PER LA CHIAVE FILE NT
    'f_desc'  => 'TADESC',
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TARIF1_d', 'TAINDI', 'TALOCA', 'nt_CONFIG', 'TAB_ABB', 'ATTAV', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TARIF1', 'TAINDI', 'TALOCA', 'TAASPE', 'TATISP', 'TATITR', 'TACOGE', 'TACAP', 'TARIF2'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TARIF1' => array('label'	=> 'Reparto', 'type' => 'from_TA', 'TAID' => 'REPMA', 'desc'=> 'Y', 'module' => $desk_art, 'allowBlank' => true),
        'TARIF1_d' => array('label'	=> 'Reparto'),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        'TAINDI' => array('label'	=> 'Riferimento', 'maxLength' => 30),
        'TALOCA' => array('label'	=> 'Matricola', 'maxLength' => 60),
        
        
        'TAASPE' => array('label'	=> 'Mansione', 'type' => 'from_TA', 'TAID' => 'PRGRO', 'module' => $main_module, 'allowBlank' => true),
        'TATISP' => array('label'	=> 'Mansione', 'type' => 'from_TA', 'TAID' => 'PRGRO', 'module' => $main_module, 'allowBlank' => true),
        'TATITR' => array('label'	=> 'Mansione', 'type' => 'from_TA', 'TAID' => 'PRGRO', 'module' => $main_module, 'allowBlank' => true),
       
        'TACOGE' => array('label'	=> 'Fornitore', 'type' => 'fornitore'),
        'TACOGE_DE' => array('label'	=> 'Fornitore', 'type' => 'fornitore'),
        'TACAP' => array('label'	=> 'Linea', 'type' => 'from_TA', 'TAID' => 'LIPRO', 'module' => $main_module, 'allowBlank' => true),
        'TARIF2' => array('label'	=> 'Data', 'type' => 'date'),
        
        'nt_CONFIG' => array('type' => 'NT', 'fw'=>'width: 40',
            'iconCls' => "info_blue",
            'tooltip'=>'Note assistenza',
            'nt_config' => array(
                'title' => 'Note assistenza',
                'file'     => $prod_module['file_note'],
                'NTTPNO'   => 'MACMA'
                //ToDo:
                //come default uso come chiave il valore di field_IDPR. Si potrebbe dover configurare
            )),
        
        'ATTAV' => array( 'label' => 'attav', 'type' => 'TA', 'fw'=>'width: 40',
            'iconCls' => "arrivi",
            'tooltip'=>'ToDo',
            'select' => "ATTAV.C_ROW AS ATTAV",
            'join' => "LEFT OUTER JOIN (
            SELECT COUNT(*) AS C_ROW, ASDOCU
            FROM {$cfg_mod['file_assegna_ord']} 
            WHERE ASDT = '{$id_ditta_default}' AND ASFLRI <> 'Y'
            GROUP BY ASDOCU) ATTAV
            ON TA.TAKEY1 = ATTAV.ASDOCU",
            'ta_config' => array(
                'params' => array('raggruppamento' => 'MACMA', 'tab_prod' => 'Y'),
                'file_acs'     => 'acs_panel_todolist.php?fn=open_panel',
                
            )
        
        ),
        
        //TABELLA ABBINATA
        'TAB_ABB' =>  array( 'label' => 'ricambi', 'type' => 'TA',
            'iconCls' => 'search',
            'tooltip' => 'Ricambi',
            'select' => "TA_RI.C_ROW AS TAB_ABB",
            'join' => "LEFT OUTER JOIN (
            SELECT COUNT(*) AS C_ROW, TA2.TAKEY1
            FROM {$prod_module['file_tabelle']} TA2
            WHERE TA2.TADT = '{$id_ditta_default}' AND TA2.TATAID = 'MACRI'
            GROUP BY TA2.TAKEY1) TA_RI
            ON TA.TAKEY1 = TA_RI.TAKEY1",
            'ta_config' => array(
                'tab' => 'MACRI',
                'file_acs'     => 'acs_gest_MACRI.php?fn=open_tab',
                
            )
            ),
        
        
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
        
        
    ),
    
    'tasto_dx' => array(
        array(
            'text'  => 'Inserimento nuovo stato/attivit&agrave;',
            'iconCls' => 'icon-arrivi-16',
            'tooltip' => '',
            'scale' => 'small',
            'handler' =>  new ApiProcRaw("function(){
                acs_show_win_std('Nuovo stato/attivit&agrave;', '../desk_utility/acs_anag_art_create_attivita.php', 
						{	tipo_op: 'MACMA',
							rif : 'MACMA',
			  				list_selected_id : list_selected_id
			  			}, 500, 500, {
                            afterInsertRecord: function(from_win){
                                m_grid.getStore().load();		        						
        						from_win.close();
				        	}}
                        , 'icon-arrivi-16');
             }")
        )
    ),
    
    'buttons' => array(
        array(
            'text'  => '',
            'iconCls' => 'icon-gear-16',
            'tooltip' => 'Attav ToDo',
            'scale' => 'small',
            'style' => 'border: 1px solid gray;',
            'handler' =>  new ApiProcRaw("function(){
                acs_show_panel_std('../desk_utility/acs_gest_ATTAV.php?fn=open_tab', 
			           			'panel_gestione_attav', {sezione: 'MACMA'});
             }")
        )
    )
   
    
    
);

$_row_post_fn = function($row, $m_table_config){
    $row['nt_CONFIG']  = _ha_nota_nt_config($row[$m_table_config['field_NOTE']], $m_table_config['fields']['nt_CONFIG']['nt_config']);
  
    return $row;
};

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
