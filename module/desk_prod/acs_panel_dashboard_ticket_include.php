<?php 

function ultima_nota($id_ticket){
    global $conn, $cfg_mod_DeskProd;

    $sql_ur = "SELECT RLDESC
    FROM  {$cfg_mod_DeskProd['file_ticket_N']}
    WHERE RLRIF1 = {$id_ticket}
    ORDER BY RLRIF2 DESC
    LIMIT 1";
    
    $stmt_ur = db2_prepare($conn, $sql_ur);
    echo db2_stmt_errormsg(); //eventualmente stampa errore su prepare (controllo formale)
    $result_ur = db2_execute($stmt_ur);
    echo db2_stmt_errormsg($stmt_ur); //eventualmente stampa errore su prepare (controllo formale)
    $row_ur = db2_fetch_assoc($stmt_ur);
    
    if ($row_ur)
        $ultima_nota = $row_ur['RLDESC'];
    else
        //$ultima_nota = "Nessuna Nota!";
        $ultima_nota = "";
        
    return $ultima_nota;
}

function _get_data_grid_avanzamento($m_params){
    
    global $conn, $id_ditta_default, $cfg_mod_DeskProd, $_stato_decodifica, $_stato_ticket_tab;
    
    $dati = array();
    
    $sql_where = '';
    
    if(isset($m_params->field) && trim($m_params->field) != '')
        $sql_where .= " AND {$m_params->field} = '{$m_params->valueField}'";
        
    
    if(isset($m_params->form_values))
        $sql_where .= " AND AT.ATDTGE >= {$m_params->form_values->giorno_da} AND AT.ATDTGE <= {$m_params->form_values->giorno_a}";
        
        $sql = "SELECT RRN(AR) AS RRN_R, AT.ATIDTK, AT.ATDTGE, AT.ATORGE, AT.ATUSGE, AT.ATSTTK, AT.ATTEMT, AT.ATARTI,
        AR.ARIDRG, AR.ARDTGE, AR.ARORGE, AR.ARTPAV, AR.ARUSUM,
        RL.RLDESC, TRIM(TA_MAC.TADESC) AS TADESC_MAC, TRIM(TA_REP.TADESC) AS TADESC_REP, TA_REP.TAKEY1
        FROM {$cfg_mod_DeskProd['file_ticket_R']} as AR
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_ticket_T']} as AT
        ON AR.ARIDTK = AT.ATIDTK
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_ticket_N']} as RL
        ON AR.ARIDTK = RL.RLRIF1 AND AR.ARIDRG = RL.RLRIF2 AND RL.RLRIGA = 0
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_tabelle_man']} as TA_MAC
        ON TA_MAC.TATAID = 'MACMA' AND TA_MAC.TAKEY1 = AT.ATARTI
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_tabelle_man']}  as TA_REP
        ON TA_REP.TATAID = 'REPMA' AND TA_REP.TAKEY1 = TA_MAC.TARIF1
        WHERE AT.ATSTTK IN ('100' , '200' , '300' , '900' , '950' , '999')
        {$sql_where}
        ORDER BY AR.ARDTGE DESC, AR.ARORGE DESC";
  
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg(); //eventualmente stampa errore su prepare (controllo formale)
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt); //eventualmente stampa errore su prepare (controllo formale)
        while($row = db2_fetch_assoc($stmt)){
            $ultima_nota = ultima_nota($row['ATIDTK']);
            $row['ARTPAV_dec'] = $_stato_decodifica[trim($row['ARTPAV'])];
            $row['ATSTTK_dec'] = $_stato_ticket_tab[trim($row['ATSTTK'])];
            
            $dati[] = array('id' => $row['RRN_R'], 'item_id' => $row['ATIDTK'], 'ute' => $row['ARUSUM'], 'n_ticket' => $row['ATIDTK'] . ' [' . $row['ARIDRG'] . ']',
                'nota' => $row['RLDESC'], 'sta' => $row['ARTPAV_dec'],
                'sta_tes' => $row['ATSTTK_dec'],
                'data_ora_gen' => print_date($row['ARDTGE']) . " - " . print_ora($row['ARORGE']),
                'macchina' => $row['TADESC_MAC'] . ' [' . $row['ATARTI'] . ']',
                'reparto' => $row['TADESC_REP'] . ' [' . $row['TAKEY1'] . ']',
                'tempo_ticket' => $row['ATTEMT']);
        }
       
    return grid_to_json_data($dati);
    
}

function _get_data_grid_aperti($m_params){
    
    global $conn, $id_ditta_default, $cfg_mod_DeskProd;
    
    $dati = array();
    
    $sql_where = '';
    
    if(isset($m_params->form_values))
        $sql_where .= " AND ATDTGE >= {$m_params->form_values->giorno_da} AND ATDTGE <= {$m_params->form_values->giorno_a}";
        
        $sql = "SELECT AT.ATIDTK, AT.ATDTGE, AT.ATORGE, AT.ATUSGE, AT.ATTEMT, AR.ARDTGE, AR.ARORGE
        FROM {$cfg_mod_DeskProd['file_ticket_T']} as AT
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_ticket_R']} as AR
        ON AT.ATIDTK = AR.ARIDTK
        WHERE ATSTTK IN ('100' , '200' , '300')
        {$sql_where}
        ORDER BY ATIDTK DESC";
        
        /*print_r($sql);
        exit;*/
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg(); //eventualmente stampa errore su prepare (controllo formale)
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt); //eventualmente stampa errore su prepare (controllo formale)
        while($row = db2_fetch_assoc($stmt)){
            //$row['ATSTTK_dec'] = $_stato_ticket_tab[trim($row['ATSTTK'])];
            $dati[] = array('id' => $row['ATIDTK'],
                'tempo_ticket' => $row['ATTEMT'],
                'data_ora_gen' => print_date($row['ARDTGE']) . " - " . print_ora($row['ARORGE']), 'ute' => $row['ATUSGE']);
        }
        
        return grid_to_json_data($dati);
    
}

function _get_data_grid_operatori($m_params){
    global $conn, $id_ditta_default, $cfg_mod_DeskProd;
    
    $dati = array();
    
    $sql_where = '';
    
    //if(isset($m_params->form_values) && strlen(trim($m_params->form_values)) > 0)
    if(isset($m_params->form_values))
        $sql_where .= " AND ATDTGE >= {$m_params->form_values->giorno_da} AND ATDTGE <= {$m_params->form_values->giorno_a}";
        
        $sql = "SELECT DISTINCT(ATUSGE), ATDTGE
        FROM {$cfg_mod_DeskProd['file_ticket_T']} 
        WHERE ATUSGE <> ''
        {$sql_where}
        ORDER BY ATUSGE";
  
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg(); //eventualmente stampa errore su prepare (controllo formale)
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt); //eventualmente stampa errore su prepare (controllo formale)
        while($row = db2_fetch_assoc($stmt)){
            $dati[] = array('ute' => $row['ATUSGE'], 'id' => $row['ATUSGE']);
        }
        
        return grid_to_json_data($dati);
    
    
}

function _get_data_grid_reparto($m_params){
    global $conn, $id_ditta_default, $cfg_mod_DeskProd;
   
    $dati = array();
    $sql_where = '';
    
    if(isset($m_params->form_values))
        $sql_where .= " WHERE AT.ATDTGE >= {$m_params->form_values->giorno_da} AND AT.ATDTGE <= {$m_params->form_values->giorno_a}";
        
        $sql = "SELECT TRIM(TA_REP.TAKEY1) AS TAKEY1, TRIM(TA_REP.TADESC) AS TADESC, COUNT(AT.ATIDTK) AS TICKET
        FROM {$cfg_mod_DeskProd['file_ticket_T']}  AS AT
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_tabelle_man']} TA_MAC
        ON TA_MAC.TATAID = 'MACMA' AND TA_MAC.TAKEY1 = AT.ATARTI
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_tabelle_man']} TA_REP
        ON TA_REP.TATAID = 'REPMA' AND TA_REP.TAKEY1 = TA_MAC.TARIF1
        {$sql_where}
        GROUP BY TRIM(TA_REP.TADESC), TA_REP.TAKEY1
        ORDER BY TRIM(TA_REP.TADESC)";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg(); //eventualmente stampa errore su prepare (controllo formale)
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt); //eventualmente stampa errore su prepare (controllo formale)
        while($row = db2_fetch_assoc($stmt)){
            $dati[] = array('reparto' => $row['TADESC'] . ' [' . $row['TAKEY1'] . ']', 'ticket' => $row['TICKET'], 'id' => $row['TAKEY1']);
        }
        
        return grid_to_json_data($dati);
        
        
}

function _get_data_grid_macchina($m_params){
    global $conn, $id_ditta_default, $cfg_mod_DeskProd;
    
    $dati = array();
    
    $sql_where = '';
    
    if(isset($m_params->form_values))
        $sql_where .= " AND AT.ATDTGE >= {$m_params->form_values->giorno_da} AND AT.ATDTGE <= {$m_params->form_values->giorno_a}";
        
        
        //Codice Macchina Decodificato + Reparto NON Decodificato
        $sql = "SELECT TRIM(TA_MAC.TADESC) AS TADESC_MAC, TRIM(TA_REP.TAKEY1) AS TADESC_REP, AT.ATARTI, COUNT(AT.ATIDTK) AS TICKET
        FROM {$cfg_mod_DeskProd['file_ticket_T']} AS AT
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_tabelle_man']} TA_MAC
        ON TA_MAC.TATAID = 'MACMA' AND TA_MAC.TAKEY1 = AT.ATARTI
        LEFT OUTER JOIN {$cfg_mod_DeskProd['file_tabelle_man']} TA_REP
        ON TA_REP.TATAID = 'REPMA' AND TA_REP.TAKEY1 = TA_MAC.TARIF1
        WHERE AT.ATARTI != ''
        {$sql_where}
        GROUP BY AT.ATARTI, TRIM(TA_REP.TAKEY1), TRIM(TA_MAC.TADESC)
        ORDER BY TRIM(TA_REP.TAKEY1), TRIM(TA_MAC.TADESC)";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg(); //eventualmente stampa errore su prepare (controllo formale)
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt); //eventualmente stampa errore su prepare (controllo formale)
        while($row = db2_fetch_assoc($stmt)){
         
            $dati[] = array('macchina' => $row['TADESC_MAC'] . ' [' . $row['ATARTI'] . ']', 'ticket' => $row['TICKET'], 'id' => $row['ATARTI']);
        }
        
        return grid_to_json_data($dati);
        
        
}
    