<?php

require_once("../../config.inc.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

function m_get_fields($m_table_config){
    $r = array();
    foreach ($m_table_config['fields'] as $kf=>$f)
        $r[] = $kf;
        $r[] = 'descrizione';
        
        return acs_je($r);
}


function m_get_columns($m_table_config){
    
    foreach ($m_table_config['fields'] as $kf=>$f) {
        if(!isset($f['hidden']))
            $hidden = 'false';
        else
            $hidden = $f['hidden'];
                
        if(!isset($f['c_width']))
            $width = "flex : 1";
        else
            $width = "width : {$f['c_width']}";
                        
        if(isset($f['short']))
            $header = $f['short'];
        else
            $header = $f['label'];
                                
        $renderer = "";
        if($kf == 'TATP'){
            $renderer = "renderer: function(value, metaData, record){
		    			  if(record.get('TATP') == 'S'){
                               metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			       return '<img src=" .  img_path("icone/48x48/divieto.png") . " width=15>';
		    		 	   }
		    			     
					   			    
		    		  }";
                }
                                
             if($kf == 'MAC'){
                            $renderer = "renderer: function(value, metaData, record){
                 if(record.get('MAC') > 0){
    			   return '<img src=" .  img_path("icone/48x48/search.png") . " width=15>';
    		 	}
    			       
    		  }";
             }
                                
                      
                                
     
                                
                        
                                
            if($kf == 'TADESC'){
                            
                            $renderer = "renderer: function(value, metaData, record){
                   return record.get('descrizione');
    		  }";
            }
                                
                                
        $view = true;
        if($f['only_view'] == 'F')
            $view = false;
            
            if($view == true){
                                        
                                        ?>
		 {
			header: <?php echo j($header)?>,
		 	dataIndex: <?php echo j($kf)?>, 
		 	hidden: <?php echo $hidden ?>, 
		 	<?php if(isset($f['tooltip'])){?>
		 		tooltip: <?php echo j($f['tooltip']); ?>,
		 	<?php }?>	 
		    <?php echo $width ?>,
		 	filter: {type: 'string'}, filterable: true,
		 	<?php echo $renderer ?>
		 },		
		<?php
		
            }
	}
	
	?>
	
	{header: 'Immissione',
	 columns: [
	  {header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true, renderer: date_from_AS }
	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true }
	 ]}
	 
	 <?php 
}

if ($_REQUEST['fn'] == 'exe_check'){
    
    $ret = array();
    
    if(strlen(trim($m_params->takey2)) > 0)
        $sql_where = " AND TAKEY2 = '{$m_params->takey2}'";
   
    $sql = "SELECT COUNT(*) AS C_ROW
              FROM {$cfg_mod_DeskArt['file_tabelle']} TA
              WHERE TADT = '{$id_ditta_default}' AND TATAID = '{$m_table_config['TATAID']}'
              AND TAKEY1 = '{$m_params->codice}'
              {$sql_where}";
    
      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt);
      $row = db2_fetch_assoc($stmt);
                        
    if($row['C_ROW'] > 0){
        $ret['msg_error'] = "Codice esistente";
        $ret['success'] = false;
        
    }else{
        $ret['success'] = true;
    }
    
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ar_ins = array();
    
    $ar_ins['TAUSGE'] = $auth->get_user();
    $ar_ins['TADTGE'] = oggi_AS_date();
    $ar_ins['TAORGE'] = oggi_AS_time();
    $ar_ins['TADT']   = $id_ditta_default;
    $ar_ins['TATAID'] = $m_table_config['TATAID'];
    $ar_ins['TAKEY1'] = $m_params->form_values->TAKEY1;
    if(isset($m_params->takey2))
        $ar_ins['TAKEY2'] = $m_params->takey2;
    $ar_ins['TADESC'] = $m_params->form_values->TADESC;
    
    $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg();
     
    if(isset($m_params->takey2))
        $sql_where = " AND TAKEY2 = '{$m_params->takey2}'";
        
    $sql_s = "SELECT TA.*, RRN(TA) AS RRN
    FROM {$cfg_mod_DeskArt['file_tabelle']} TA
    WHERE TADT = '{$id_ditta_default}' AND TATAID = '{$m_table_config['TATAID']}'
    AND TAKEY1 = '{$m_params->form_values->TAKEY1}'
    {$sql_where}";
    
    
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_s);
    echo db2_stmt_errormsg($stmt_s);
    $row = db2_fetch_assoc($stmt_s);
    $row['RRN'] = acs_u8e(trim($row['RRN']));
    $row['TADESC'] = acs_u8e(trim($row['TADESC']));
    $row['descrizione'] = htmlentities($row['TADESC']);
    $row['TAKEY1'] = trim($row['TAKEY1']);
    $row['TADES2'] = trim($row['TADES2']);
    
    $ret['success'] = true;
    $ret['record'] = $row;
    echo acs_je($ret);
    exit;
        
}

if ($_REQUEST['fn'] == 'exe_aggiorna'){
    
    $ar_ins = array();
    $ar_ins['TAKEY1'] = $m_params->form_values->TAKEY1;
    $ar_ins['TADESC'] =$m_params->form_values->TADESC;
   
    $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle']} TA
    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
    WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg();
    
    if(isset($m_params->takey2))
        $sql_where = " AND TAKEY2 = '{$m_params->takey2}'";
        
    $sql_s = "SELECT TA.*, RRN(TA) AS RRN
    FROM {$cfg_mod_DeskArt['file_tabelle']} TA
    WHERE TADT = '{$id_ditta_default}' AND TATAID = '{$m_table_config['TATAID']}' 
    AND TAKEY1 = '{$m_params->form_values->TAKEY1}'
    {$sql_where}";
    
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_s);
    echo db2_stmt_errormsg($stmt_s);
    $row = db2_fetch_assoc($stmt_s);
    $row['RRN'] = acs_u8e(trim($row['RRN']));
    $row['TADESC'] = acs_u8e(trim($row['TADESC']));
    $row['descrizione'] = htmlentities($row['TADESC']);
    $row['TAKEY1'] = trim($row['TAKEY1']);
    $row['TADES2'] = trim($row['TADES2']);
    
    $ret['success'] = true;
    $ret['record'] = $row;
    echo acs_je($ret);
    exit;
    
}


if ($_REQUEST['fn'] == 'exe_canc'){
    
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle']} TA
            WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $sql_where = "";
    $sql_join = "";
    $sql_sel = "";
    /*$ar_lin = array();
    foreach($cfg_mod_DeskUtility['ar_lingue'] as $v){
        $ar_lin[] = $v['id'];
    }*/
    
    if($m_params->takey2 != ''){
        $sql_where .= " AND TAKEY2 = '".trim($m_params->takey2)."'";
        $sql_where_join = " AND TAKEY2 = '".trim($m_params->takey2)."'";
    }
    
    if($m_table_config['j_macchina'] == 'Y'){
        $sql_sel .= ", MC.C_ROW AS MAC";
        $sql_join .= "  LEFT OUTER JOIN (
                        SELECT COUNT(*) AS C_ROW, TAKEY2
                        FROM {$cfg_mod_DeskArt['file_tabelle']}
                        WHERE TADT = '{$id_ditta_default}' AND TATAID = 'MACMA'
                        GROUP BY TAKEY2) MC
                        ON MC.TAKEY2 = TA.TAKEY1";
    }
    
    $sql = "SELECT RRN (TA) AS RRN, TA.* {$sql_sel}
            FROM {$cfg_mod_DeskArt['file_tabelle']} TA
            {$sql_join}
             WHERE TADT='{$id_ditta_default}' AND TATAID = ?
            {$sql_where} ORDER BY TAKEY1, TADESC";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_table_config['TATAID']));
    
    while ($row = db2_fetch_assoc($stmt)) {
      
        $row['RRN'] = acs_u8e(trim($row['RRN']));
        $row['TADESC'] = acs_u8e(trim($row['TADESC']));
        $row['descrizione'] = htmlentities($row['TADESC']);
        $row['TAKEY1'] = trim($row['TAKEY1']);
        $row['TADES2'] = trim($row['TADES2']);
        $ret[] = $row;
    }
    
    echo acs_je($ret);
    exit;
}

//*************************************************************
if ($_REQUEST['fn'] == 'open_panel'){
//*************************************************************
    
 
    ?>
   {"success":true, "items": [

   {
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
    
    			{
				xtype: 'grid',
				flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		   		store: {
					xtype: 'store',
					autoLoad:true,
					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
		                        extraParams: {
									takey2 : <?php echo j($m_params->takey2); ?>
									
		        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: <?php  echo m_get_fields($m_table_config); ?>,
			
			}, //store
	
	            multiSelect: false,
		        singleExpand: false,
				columns: [ <?php m_get_columns($m_table_config) ?> ],  
				enableSort: true
				, listeners: {
					   selectionchange: function(selModel, selected) { 
	               
            	               if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().reset();
            		               rec_index = selected[0].index;
            		               form_dx.getForm().findField('rec_index').setValue(rec_index);
            	                   form_dx.getForm().setValues(selected[0].data);
	                 			}
		       		  	 	 }
		       		  	 	 
		       		  ,celldblclick: {
	           	
		           			fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
    		           		var col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
    					  	var rec = iView.getRecord(iRowEl);
    					  	var grid = this;
    					  	
    					  	if(col_name == 'MAC')
    					  		acs_show_win_std('Anagrafica Macchine [Reparto :' + rec.get('TAKEY1') + ']- Manutenzione', 'acs_anag_man_mac.php?fn=open_panel', {takey2: rec.get('TAKEY1'), d_cod : rec.get('TADESC')}, 1100, 500, {}, 'icon-gear-16');
					  		
		           
		           		}
		           
		           }
		       		  	 	 
				}
	    		
	 	},
	 	
	 	{
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: <?php echo j($text_form); ?>,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.4,
 		            frame: true,
 		            items: [ 
 		             	{xtype : 'textfield', name : 'rec_index', hidden : true},
 		             	{xtype : 'textfield', name : 'RRN', hidden : true},
 		             	{xtype : 'textfield', name : 'TAKEY1', fieldLabel : 'Codice', width : 180, maxLength : 10,
 		             	
     		            listeners: {
            				'change': function(field){
            					 field.setValue(field.getValue().toUpperCase());
          				},
          				'blur': function(field) {
          				    var form = this.up('form').getForm();
          				    var grid = this.up('window').down('grid');
          				    
          					Ext.Ajax.request({
    						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check',
    						        timeout: 2400000,
    						        method     : 'POST',
    			        			jsonData: {
    			        				codice : field.getValue(),
    			        				takey2 : <?php echo j($m_params->takey2); ?> 
    			        				
    								},				
    										        
    						        success : function(result, request){
    							        var jsonData = Ext.decode(result.responseText);
    							       
    							        if(jsonData.success == false){
    					      	    		 acs_show_msg_error(jsonData.msg_error);
    							        }
    						
    					      	    	
    							    },
    						        failure    : function(result, request){
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
    						    });
                    	 		
    
                		}
     				}
 		             	},
 		             	{xtype : 'textfield', name : 'TADESC', fieldLabel : 'Descrizione', flex : 1, maxLength : 100},
 		                  ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                  {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
        		
        		     
        		      Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
        				       Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
										
									},							        
							         success : function(result, request){
							        	var jsonData = Ext.decode(result.responseText);
        					         	if(jsonData.success == false){
					      	    		 	acs_show_msg_error(jsonData.msg_error);
							        	 	return;
				        			     }else{
        					         		form.getForm().reset();
							       	        grid.getStore().load();	
        					         	}
						
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
					
					    	}
            	   		  });		    
					        
				        }

			     },  {
                     xtype: 'button',
                    text: 'Reset',
		            iconCls: 'icon-button_black_repeat-16',
		            scale: 'small',	                     

		           handler: function() {
		              var form = this.up('form').getForm();
                	  form.reset();
			
			            }

			     }, '->',
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		           	handler: function() {
		               		
 			       			var form_values = this.up('form').getValues();
 			       			var form = this.up('form').getForm();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			var index = form_values.rec_index;
 			       			var record_grid = grid.getStore().getAt(index);
 			       	        if(form.isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				takey2 : <?php echo j(trim($m_params->takey2)); ?>,
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							           if(jsonData.success == false){
					      	    			acs_show_msg_error('errore');
					      	    		return false;
					        			}else{
					        			   //grid.getStore().load();
					        			   var new_rec = jsonData.record;
        					        	 	grid.getStore().add(new_rec);
        					        	 	var rec_index = grid.getStore().findRecord('TAKEY1', new_rec.TAKEY1);
        			  					 	grid.getView().select(rec_index);
        			  					 	grid.getView().focusRow(rec_index);
					        		   }
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               }
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               		
 			       			var form_values = this.up('form').getValues();
 			       			var form = this.up('form').getForm();
 			       			var grid = this.up('window').down('grid'); 
 			       			var index = form_values.rec_index;
 			       			var record_grid = grid.getStore().getAt(index);
 			       	        if(form.isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							           if(jsonData.success == false){
					      	    			acs_show_msg_error('errore');
					      	    		return false;
					        			}else{
					        			   //grid.getStore().load();
					        			   var new_rec = jsonData.record;
					        			   record_grid.set(new_rec);
					        		   }
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               }
			            }

			     }
			     ]
		   }]
			 	 }  
		 ]
					 
					
					
	}
    
    ]}
    
    <?php 
    
    exit;
}