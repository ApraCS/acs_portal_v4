<?php
require_once("../../config.inc.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$cfg_mod = $main_module->get_cfg_mod();
$m_params = acs_m_params_json_decode();




//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//---------------------------------------------------------------------
    
    //deafult: 6 mesi precedenti, 6 mesi successivi dfdfd
    $salta_mesi = -6;
    $numero_mesi = 12;
    
    $ret = array();
    for ($i=0; $i<$numero_mesi;$i++){
        $s = $salta_mesi + $i;
        
        $sql = "SELECT ARIDTA
                    FROM {$cfg_mod_DeskProd['file_ticket_T']}
                    LEFT OUTER JOIN {$cfg_mod_DeskProd['file_ticket_R']}
                    ON ATIDTK = ARIDTK AND ARTPAV = 'ATTI'
                    WHERE  ATTPTK = 'ATTI' AND ARIDTA > 0
                    AND ARIDTA >= " . date('Ym', strtotime("{$s} months")) . "00
                    AND ARIDTA <= " . date('Ym', strtotime("{$s} months")) . "31
                    GROUP BY ARIDTA";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $ar_date = array();
        while ($row = db2_fetch_assoc($stmt)){
            $ar_date[] = $row['ARIDTA'];
        }
        
        $ret[] = array(
            'annomese_out' => date('Y', strtotime("{$s} months")) . ' / ' . date('m', strtotime("{$s} months")) . ' - <b>' . mese_short((int)date('m', strtotime("{$s} months"))) . '</b>', 
            'annomese'   => date('Ym', strtotime("{$s} months")),
            'anno'       => date('Y', strtotime("{$s} months")),
            'mese'       => date('m', strtotime("{$s} months")),
            'ar_date_caricate' => $ar_date
        );
    }
    
    
    
    
    
    echo acs_je(array('success' => true,
        'items' => $ret));
    exit;
}

//----------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_panel'){
//----------------------------------------------------------------
?>
{"success":true, "items": [

        {
        xtype: 'grid' ,
        multiSelect: true,
        selType: 'cellmodel',
        title: 'Time table - Month',
	    tbar: new Ext.Toolbar({
	            items:['<b> Riepilogo mensile orari di apertura/chiusura macchine assegnati</b>', '->',	            
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,         
		    store: {
		    		fields: ['annomese_out', 'annomese', 'anno', 'mese', 'ar_date_caricate'],
                    autoLoad: true,
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						extraParams: {
						  open_request: <?php echo acs_je($m_params); ?>,
	                      form_values: <?php echo acs_je($m_params->form_values); ?>	                  
                      }
                      , doRequest: personalizza_extraParams_to_jsonData     
					  , reader: {root: 'items'}        				
                    }

                },
                

    	    			
            columns: [{            			
        	    		text: 'Anno / Mese', 	
        	    		width: 150,
        	    		dataIndex: 'annomese_out',
        	    		renderer: function(value, metaData, record, rowIndex, colIndex){
        	    			metaData.style += 'border: 1px solid grey; line-height: 2em;';
        	    			return value;
        	    		}
        	    	  }
        	    	  
        	    	  <?php for ($i=1; $i<=31; $i++){  ?>
        	    	  , { 
        	    		text: <?php echo j($i); ?>,
        	    		flex: 1,
        	    		dataIndex: <?php echo j("gg_{$i}") ?>,
        	    		dayOfMonth: <?php echo $i; ?>,
        	    		align: 'right',
        	    		renderer: function(value, metaData, record, rowIndex, colIndex){
        	    			var grid = this,
        	    				col = this.columns[colIndex],
        	    				dayOfMonth = col.dayOfMonth,
        	    				jsData = new Date(record.get('anno'), record.get('mese') -1, dayOfMonth),
        	    				dayOfWeek = jsData.getDay();
        	    				
                              
            	    			metaData.style += 'border: 1px solid grey; line-height: 2em;';
            	    			if (dayOfWeek == 0 || dayOfWeek == 6){ //domenica (0) o sabato (6) 
            				       metaData.tdCls += ' sfondo_giallo';        				       
            				    }
        				    
            				    var date = record.get('anno') + record.get('mese') +  <?php echo j(sprintf("%02s", $i)); ?>;
            				    
            				    var ar_date = record.get('ar_date_caricate');

            				    for (var chiave in ar_date) {
    			  					if (ar_date[chiave] == date)  
            				        	metaData.tdCls += ' sfondo_verde';        				       
            				    }
            				    
                                if (jsData.getFullYear() ==  record.get('anno') && jsData.getMonth() == record.get('mese') - 1 && jsData.getDate() ==  <?php echo j(sprintf("%02s", $i)); ?>)
                                   var valid = 'Y';
                                else var valid = 'N';
        				    
        				        if (valid == 'N')  
        				        	metaData.tdCls += ' sfondo_grigio';     
        				    
            				    if (dayOfWeek == 1){	//lunedi, mostro week of year
            				    	value = '<div style="float: right; font-size: 7px">' + Ext.Date.format(jsData, 'W') + '</div>';
            				    }
            				       
            				    return value;
        	    		}
        	    	  }
        	    	  <?php } ?>
        	    		
    	    ], //columns
    	    
			viewConfig: {
			 	toggleOnDblClick: false,
	         	getRowClass: function(record, index) {			        	
		           v = record.get('liv');    		          
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');    		           
 		           return v;																
		         }
		    },
		    
		    
		    			             
		listeners: {	

			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
 
					var record 	= iView.getRecord(iRowEl)
				    	col_name = iView.getGridColumns()[iColIdx].dataIndex,
			         	col = iView.getGridColumns()[iColIdx],
	    				dayOfMonth = col.dayOfMonth,
	    				jsData = new Date(record.get('anno'), record.get('mese') -1, dayOfMonth),
	    				dayOfWeek = jsData.getDay(),			         
			         	date_start = new Date(jsData),
			         	date_end   = new Date(jsData);
			         
			         //calcolo data inizio e fine settimana
			         date_start.setDate(date_start.getDate() - (dayOfWeek - 1));
			         date_end.setDate(date_start.getDate() + (7 - 1));
			         
			         
					 acs_show_panel_std('acs_panel_ticket_man_week.php?fn=open_panel', null, {
					 	open_request: {
					 		data_start: Ext.Date.format(date_start, 'Ymd'), 
					 		data_end:   Ext.Date.format(date_end, 'Ymd')
					 		}});			         			         
			         
					 return false;	
				}
			}
		
	 	}
		    
		    
    	}
	]
 }
<?php 
exit;
}