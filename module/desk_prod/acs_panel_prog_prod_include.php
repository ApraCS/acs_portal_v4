<?php

//la decodifica del kiosk e' la voce START
function decod_kiosk_from_indice($cfg_mod, $indice, $kiosk){
    global $conn, $id_ditta_default;
    $sql = "SELECT * FROM {$cfg_mod['file_cfg_distinte']} WHERE
                CDDT = '{$id_ditta_default}' AND CDDICL = ? AND CDCMAS = 'START' AND CDSLAV = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($indice, $kiosk));
    $row = db2_fetch_assoc($stmt);
    if ($row) return trim($row['CDDESC']);
    return '';
}

//la decodifica del kiosk e' la voce START
function get_row_kiosk_from_indice($cfg_mod, $indice, $kiosk){
    global $conn, $id_ditta_default;
    $sql = "SELECT * FROM {$cfg_mod['file_cfg_distinte']} WHERE
                CDDT = '{$id_ditta_default}' AND CDDICL = ? AND CDCMAS = 'START' AND CDSLAV = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($indice, $kiosk));
    $row = db2_fetch_assoc($stmt);
    return $row;    
}


function sql_where_by_open_form($form_values){
    $ret = "";
    
    if (isset($form_values->f_data_da)  && strlen($form_values->f_data_da) > 0 )
        $ret .= " AND TDDTEP >= {$form_values->f_data_da} ";
    if (isset($form_values->f_data_a)  && strlen($form_values->f_data_a) > 0 )
        $ret .= " AND TDDTEP <= {$form_values->f_data_a} ";
    
    $ret .= sql_where_by_combo_value('TA_ITIN.TAASPE', $form_values->f_area_spedizione);
    $ret .= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);
                
    return $ret;
}
