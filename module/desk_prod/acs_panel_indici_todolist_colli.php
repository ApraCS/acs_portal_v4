<?php

require_once("../../config.inc.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_todolist_preview_include.php';

$main_module = new DeskProd();

$m_params = acs_m_params_json_decode();

$cfg_mod = $main_module->get_cfg_mod();

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//----------------------------------------------------------------------     

    //da k_progetto recupero tipo/anno/numero lotto e ditta
    $ar_lotto = explode('_', $m_params->k_progetto);
    $tipo_lotto = $ar_lotto[0];
    $anno_lotto = $ar_lotto[1];
    $nr_lotto   = $ar_lotto[2];
    $ditta_or   = $ar_lotto[3];
    
    //recupero record AS e k_lista
    $rowAS   = get_as_row_by_id($main_module, $m_params->id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    $rowCD   = get_cd_row_by_id($main_module, $rowAS['ASORD4']);

    //Se e' specificato un ordinamento
    $parametri_formato = _pp_parametri_formato($rowCD['CDPAUS']);
    
    $colli_where = where_filtro_colli_by_k_lista($cfg_mod, $k_lista);
    $inner_join_filtro_compo = _pp_inner_pl_on_filtro_compo($cfg_mod, $k_lista);
        
    
    $sql_join_on_PL0 = _pp_sql_join_on_PL0();
    
    $sql = "SELECT TDDOCU, TDAACA, TDNRCA, TDTPCA, TDSELO,
                   PL.PLART, PL.PLDART, PL.PLAADO, PL.PLNRDO, PL.PLTPDO, PL.PLOPE1, PL.PLOPE2,
                   PL.PLPNET, PL.PLPLOR, PL.PLRIGA, PL.PLVOLU, 
    		       PL.PLUM, PL.PLQTA, PL.PLDTEN, PL.PLTMEN, PL.PLDTUS, PL.PLTMUS, PL.PLDTRM, PL.PLTMRM,
        		   PSDTSC, PSTMSC, PSRESU, PSCAUS, PSPALM, PSLATI, PSLONG, 
        		   PL.PLRCOL, PL.PLNCOL, PL.PLCOAB, PL.PLFAS10, PL.PLDT, PL.PLTIDO, PL.PLINUM, PL.PLNREC
            FROM {$cfg_mod['file_colli']} PL

            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_colli_dc']} PS
				  ON PL.PLDT = PS.PSDT
				  AND PL.PLTIDO = PS.PSTIDO
				  AND PL.PLINUM = PS.PSINUM
				  AND PL.PLAADO = PS.PSAADO
				  AND PL.PLNRDO = PS.PSNRDO
				  AND PL.PLNREC = PS.PSNREC

            {$inner_join_filtro_compo}

            INNER JOIN {$cfg_mod['file_testate']} TD
            {$sql_join_on_PL0}
                WHERE TDTPLO = ? AND TDAALO = ? AND TDNRLO = ? /* AND TDDTOR = ? */
                {$colli_where}
           ";

            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, array($tipo_lotto, $anno_lotto, $nr_lotto /*, $ditta_or */));
            echo db2_stmt_errormsg($stmt);
        
    $ar = array();   
    while ($row = db2_fetch_assoc($stmt)){
        $r = array_map('trim', $row);
        
        $r['k_carico'] = implode("_", array($r['TDAACA'], $r['TDNRCA'], $r['TDTPCA']));
        $r['k_carico_out'] = implode("_", array(substr($r['TDAACA'], 2, 2), $r['TDNRCA'], $r['TDTPCA']));
        $r['PLDART'] = acs_u8e($row['PLDART']);
        $r['k_ordine'] = acs_u8e($row['TDDOCU']);
        $r['k_ordine_out'] = implode("_", array(substr($r['PLAADO'], 2, 2), $r['PLNRDO'], $r['PLTPDO']));
       
        $r['groupfield']        = implode("_", array(trim($row['PLFAS10']), trim($row['PLOPE1']), trim($row['PLOPE2'])));
        $row_lipc = get_TA_sys('LIPC', trim($row['PLFAS10']));
        $d_linea = "Linea: [".trim($row['PLFAS10'])."] ".$row_lipc['text'];
        
        $row_ope1 = get_TA_sys('PUOP', trim($row['PLOPE1']));
        $d_ope1 = "Operazione1: [".trim($row['PLOPE1'])."] ".$row_ope1['text'];
        
        $row_ope2 = get_TA_sys('PUOP', trim($row['PLOPE2']));
        $d_ope2 = "Operazione2: [".trim($row['PLOPE2'])."] ".$row_ope2['text'];
        
        if($row['PLRIGA'] == 0 && trim($row['PLART']) == '10')
            $r['gr_collo'] = 'F';
        
        $r['groupfield_header'] = "{$d_linea}, {$d_ope1}, {$d_ope2}";
        
        
        $ar_note = _pp_get_collo_note($main_module, $row,
            array('RLTPNO'  => 'RD',
                  'RLRIFE2' => $parametri_formato['NOR'],
                  'solo_testo' => 'Y'));
        if (count($ar_note['ar_note']) > 0){
            $r['ha_note'] = 'Y';
            $r['t_note'] = implode("<br>", $ar_note['ar_note']);
        }
        
        $ar[] = $r;
    }
    
    
    
    echo acs_je(array('success' => true, 'root' => $ar, 'colli_where' => $colli_where, 'sql' => $sql));
 exit;
}


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_tab'){
//----------------------------------------------------------------------
    //recupero record AS e k_lista
    $rowAS   = get_as_row_by_id($main_module, $m_params->id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    if (strlen($k_lista) > 0)
        $f_lista = "[{$k_lista}]";
            
        $progetto = $m_params->k_progetto;
        $ar_gruppo = explode("|", $m_params->k_distinta);
        $d_voce = $m_params->d_voce;
        $title_suff = " {$progetto}, {$ar_gruppo[1]}, {$ar_gruppo[2]}: {$d_voce} {$f_lista}";
        
    ?>	
	{
		success:true,
		m_win: {
			title: 'Colli inclusi',
			title_suf: <?php echo j($title_suff)?>,
			height: 500, width: 900,
			iconCls : 'icon-barcode'
		},
		items: [
					{
						xtype: 'grid',
						title: '',
						flex: 2,
						loadMask: true,	
				        //stateful: true,
				        //stateId: 'grid_sx_indici',
        				//tateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
        				
        				features: [
        					{ftype: 'grouping',  startCollapsed: true, groupHeaderTpl: '{[values.rows[0].data.groupfield_header]}'},
                    		{
                    			ftype: 'filters', encode: false, local: true
                    		}
                    	],
				        
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: <?php echo acs_je($m_params) ?>,
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: [
    		            			'PLPNET', 'PLPLOR', 'PLRIGA', 'PLVOLU', 'PLAADO', 'PLNRDO', {name: 'PLCOAB', type: 'int'}, 
    		            			'PLDART', 'PLUM', 'PLQTA', 'PLDTEN', 'PLTMEN', 'PLDTUS', 'PLTMUS', 'PLDTRM', 'PLTMRM',
    		            			'PSDTSC', 'PSTMSC', 'PSRESU', 'PSCAUS', 'tooltip_scarico', 'PSPALM', 'PSLATI', 'PSLONG', 
    		            			'PLART', 'PLRCOL','ALDAR1', 'TADESC', 'TDRFLO', 'TDSELO', 'PLNCOL', 'gr_collo',
    		            			'k_carico', 'k_carico_out', 'TDDOCU', 'k_ordine', 'k_ordine_out',
    		            			'groupfield', 'groupfield_header', 'gr_collo', 'ha_note', 't_note'],
    		            	groupField: 'groupfield' 						
									
			}, //store
			
			 
			 <?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
			
				columns: [	
				
				   <?php  echo dx_mobile(); ?>,
				   
				   
				   		{
			                header   : 'Carico',
			                dataIndex: 'k_carico_out', 
			                width: 90,
			                filterable: true
			             },
			             {
			                header   : 'Seq.',
			                dataIndex: 'TDSELO', 
			                width: 40,
			                filterable: true
			             },
				   		{
			                header   : 'Ordine',
			                dataIndex: 'k_ordine_out', 
			                width: 90,
			                filterable: true
			             },
				
							 {
      			                header   : 'Riga', align: 'right',
      			                dataIndex: 'PLRIGA', 
      			                width    : 35,
      			                filterable: true
      			             },{
    			                header   : 'Nr collo', align: 'right',
    			                dataIndex: 'PLCOAB', 
    			                width    : 50,
    			                filterable: true
    			             }, {
    			                header   : 'Codice',
    			                dataIndex: 'PLART', 
    			                width    : 100,
    			                filterable: true
    				         },{
    			                header   : 'Descrizione',
    			                dataIndex: 'PLDART', 
    			                flex    : 1,
    			                filterable: true
    			             }, {
    			                header   : 'UM',
    			                dataIndex: 'PLUM', 
    			                width    : 30,
    			                filterable: true
    			             }, {
    			                header   : 'Q.t&agrave;',
    			                dataIndex: 'PLQTA', 
    			                width    : 50,
    			                align: 'right',
    			                renderer : floatRenderer2
    			             },{
    			                header   : 'Volume',
    			                dataIndex: 'PLVOLU', 
    			                width    : 50,
    			                align: 'right',
    			                renderer : floatRenderer3,
    			                hidden: true
    			             }, {
    			                header   : 'Disponibilit&agrave;',
    			                dataIndex: 'PLDTEN', 
    			                width    : 80,
    			                renderer: function(value, p, record){
    			                    if(record.get('PLDTEN') > 0){
    			                       return datetime_from_AS(record.get('PLDTEN'), record.get('PLTMEN'))
    			                    }else{
    			                     if(record.get('PLDTUS') > 0)
    			                      return datetime_from_AS(record.get('PLDTUS'), record.get('PLTMUS'));
    			                   }
    			              }
    			             }, {
    			                header   : 'Spedizione',
    			                dataIndex: 'PLDTUS', 
    			                width    : 80,
    			                hidden :  true,
    			                filterable: true,
    			                renderer: function(value, p, record){
    			                	return datetime_from_AS(record.get('PLDTUS'), record.get('PLTMUS'))
    			    			}			                
    			             }, {
    			                header   : 'Aggiunte',
    			                dataIndex: 'PLDTRM', 
    			                width    : 80,
    			                filterable: true,
    			                renderer: function(value, p, record){
    			                     return datetime_from_AS(record.get('PLDTRM'), record.get('PLTMRM'));
    			                }			                			                
    			             },
    				          {
        			                header   : 'Peso netto',
        			                dataIndex: 'PLPNET', 
        			                width    : 50,
        			                filterable: true,
        			                align: 'right',
        			                renderer : floatRenderer3,
        			                hidden: true
        			             },
        			             {
          			                header   : 'Peso lordo',
          			                dataIndex: 'PLPLOR', 
          			                width    : 50,
          			                filterable: true,
          			                align: 'right',
            			            renderer : floatRenderer3,
            			            hidden: true
          			             },
                  			     {
        			                header   : <?php echo j($nota); ?>,
        			                dataIndex: 'ha_note', 
        			                width: 40,
        			                filterable: true,
        			                renderer: function(value, p, record){
                						   if(!Ext.isEmpty(record.get('t_note')))
                						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_note')) + '"';    		    	
                    		    	
                    		    	       if(record.get('ha_note') == 'Y')
                    		    			 return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
                    		    		   //else
                    		    		    // return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
                		    		  }
        			             }
    				
	         ] //columns
	    
	         
	         , listeners: {
	         
	             afterrender: function(comp){
        	  
            	  		comp.store.on('load', function(store, records, options) {
            	  		    var ar_ord = [];
            	  		    store.each(function(rec){
    	    					if (!ar_ord.includes(rec.get('k_ordine_out')))
    	    					   ar_ord.push(rec.get('k_ordine_out'));
    	    					    						    				
        					});	
        					
    						t_rows = comp.store.totalCount;	
    						l_win = comp.up('window');
    						title = l_win.title;
    						ar_title = title.split('-');
    						c_title = ar_title[0] + ': ' + t_rows + ' Ordini: ' + ar_ord.length + ' - ' + ar_title[1]; 
    						l_win.setTitle(c_title);
        										
    					}, comp);
        	 		 },
	         
	        		itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  		var voci_menu = [];
				  		
				  		
				  			voci_menu.push(
					          { text: 'Visualizza componenti filtrati (procedura in lavorazione)'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function() 
					              { acs_show_win_std( null
					                               , '../desk_vend/acs_get_order_rows_gest_order_entry.php'
					                               , { from_righe_info: 'Y'
					                                 , from_anag_art : 'Y'
					                                 , k_ordine: rec.get('k_ordine')
					                                 , riga : rec.get('PLRIGA')
					                                 , modificabile: 'N'	//ToDo: solo per gli ordini creati da qui!
					                                 });            		
		        		          }
		    		          }
		    		        );
			    		  
					        voci_menu.push(
					          { text: 'Visualizza riga ordine'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function() 
					              { acs_show_win_std( null
					                               , '../desk_vend/acs_get_order_rows_gest_order_entry.php'
					                               , { from_righe_info: 'Y'
					                                 , from_anag_art : 'Y'
					                                 , k_ordine: rec.get('k_ordine')
					                                 , riga : rec.get('PLRIGA')
					                                 , h_col : 'Y'
					                                 , modificabile: 'N'	//ToDo: solo per gli ordini creati da qui!
					                                 });            		
		        		          }
		    		          }
		    		        );	
		    		        
		    		        voci_menu.push({
						         		text: 'Elenco completo articoli/componenti',
						        		iconCls : 'icon-leaf-16',          		
						        		handler: function() {
						        			acs_show_win_std(null, '../desk_utility/acs_elenco_riepilogo_art_comp.php?fn=open_elenco', 
								        				{k_ordine: rec.get('k_ordine'), riga : rec.get('PLRIGA'), collo : rec.get('PLNCOL'), gr_collo : rec.get('gr_collo'), peso_n : rec.get('PLPNET')});   	     	
						        		}
						    		});
		    		        


			    	
		    		   var menu = new Ext.menu.Menu({
			            items: voci_menu
				        }).showAt(event.xy);	
					  
					}
	         
				   
				 }, 
				 
				 
				 viewConfig: {
		       		 getRowClass: function(record, index) {	
		       		 
		       		    if(record.get('ul_row') == 'Y' && record.get('new') == 'Y')
                		   return ' colora_riga_verde';
        		        
        		        if(record.get('ul_row') == 'Y')
                		   return ' colora_riga_giallo';
                		
                															
		         }   
		    }
				 
			, buttons: [				
            ]    

		
		  } //grid
		
		]}
		
		
		<?php 
		exit;
}





//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'form_colli'){
    //----------------------------------------------------------------------
    ?>
{"success":true, 

	m_win: {
            	title: 'Impostazione filtri su colli',
            	maximize: 'Y'
            },

	"items": [
        {
            xtype: 'form',
            flex: 1,
            
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
            
            items: [
            	{
            		xtype: 'panel', flex: 1,
            		bodyPadding: '5 5 0',
            		defaults: {bodyPadding: '5 5 0'},
            		layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
            		items: [
            		   {
        					xtype: 'fieldset', flex: 0.5,
        					title: 'Filtri su codice articolo',
        					items: [
        						{xtype: 'textfield', fieldLabel: 'Selezione 1'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 2'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 3'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 4'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 5'},
        					]
        				}
        				
        				, 
        				{
        					xtype: 'fieldset', flex: 1,
        					title: 'Filtri su Gruppo collo',
        					items: [
        						{xtype: 'textfield', fieldLabel: 'Selezione 1'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 2'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 3'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 4'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 5'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 6'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 7'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 8'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 9'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 10'}
        					]
        				}
            		]
            	}, 
            	
            	{
            	xtype: 'panel', flex: 1,
            		bodyPadding: '5 5 0',
            		defaults: {bodyPadding: '5 5 0'},
            		layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
            		items: [
            			{
        					xtype: 'fieldset', flex: 0.5,
        					title: 'Parametri aggiuntivi',
        					items: [
        						
        					]
        				}, 
            			{
        					xtype: 'fieldset', flex: 1,
        					title: 'Filtri su OPE1 / OPE2',
        					items: [
        						{xtype: 'textfield', fieldLabel: 'Selezione 1'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 2'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 3'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 4'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 5'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 6'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 7'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 8'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 9'},
        						{xtype: 'textfield', fieldLabel: 'Selezione 10'}
        					]
        				}
            		]
            	}
            ],
            
            
                    
            
			buttons: [	
			{
		            text: 'Conferma (ToDo)',
			        iconCls: 'icon-button_blue_play-24', scale: 'medium',		            
		            handler: function() {
		            	var form = this.up('form').getForm();
		            }
		        }
	        
	        
	        ]            

}		
]}
<?php 
	exit;
}

