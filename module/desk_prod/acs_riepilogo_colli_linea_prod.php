<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();
$m_params = acs_m_params_json_decode();

function sum_columns_value(&$ar_r, $r){
    
    $ar_r['COLLI_T'] += $r['COLLI_T_R'];
    $ar_r['COLLI_D'] += $r['COLLI_D_R'];
    $ar_r['COLLI_S'] += $r['COLLI_S_R'];
    if($ar_r['liv'] == 'liv_1' || $ar_r['liv'] == 'liv_2'){
        $ar_r['diff_val'] = $ar_r['COLLI_T'] - $ar_r['capac'];
        if($ar_r['capac'] != 0)
            $ar_r['perc'] = $ar_r['diff_val']/$ar_r['capac'] * 100;
    }
    
    if($r['TEMPO_S'] > 0)
        $ar_r['tempo_s'] += $r['TEMPO_S']/3600;
      
}




// ******************************************************************************************
// DATI PER GRID AVANZAMENTO LOTTO (COLLI)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_avanzamento_lotto'){
    
    //$m_params = $m_params->open_request;
    $open_request = $m_params->open_request;
    $form_values  = $open_request->form_values;
    $da_data      = $open_request->da_data;
    $col_name     = $open_request->col_name;

    if (strlen($da_data) > 0){    
        $add_day = (int)substr($col_name, 2, 2) - 1;
        $m_data = date('Ymd', strtotime($da_data . " +{$add_day} days"));
        
        $sql_data = "SELECT CSAARG, CSNRSE, CSGIOR FROM {$cfg_mod_Spedizioni['file_calendario']} CS
                     WHERE CSDT='{$id_ditta_default}' AND CSCALE ='*CS' AND CSDTRG = {$m_data}";
        
        $stmt_data = db2_prepare($conn, $sql_data);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_data);
        $row_data = db2_fetch_assoc($stmt_data);
        
        $day = $row_data['CSGIOR'];
        $week = $row_data['CSNRSE'];
        $year = $row_data['CSAARG'];
        
        $sql_WHERE_week = " AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year} ";
    }
    
    if (isset($form_values->area_spedizione) && $form_values->area_spedizione != '')
        $sql_WHERE .= " AND TA_ITIN.TAASPE = '{$form_values->area_spedizione}' ";
    if (isset($form_values->divisione) || $form_values->divisione != '')
        $sql_WHERE .= " AND TDCDIV = '{$form_values->divisione}' ";
    if (isset($form_values->num_carico) && strlen($form_values->num_carico) > 0)
        $sql_WHERE .= " AND TDNRCA ='{$form_values->num_carico}'";
    if (isset($form_values->num_lotto) && strlen($form_values->num_lotto) > 0)
        $sql_WHERE .= " AND TDNRLO ='{$form_values->num_lotto}'";
                
    if ($form_values->carico_assegnato == "Y")
        $sql_WHERE .= " AND TDNRCA > 0 ";
    if ($form_values->carico_assegnato == "N")
        $sql_WHERE .= " AND TDNRCA = 0 ";
    if ($form_values->lotto_assegnato == "Y")
        $sql_WHERE .= " AND TDNRLO > 0 ";
    if ($form_values->anomalie_evasione == "Y")
        $sql_WHERE .= " AND TDOPUN = 'N' ";
    if ($form_values->lotto_assegnato == "N")
        $sql_WHERE .= " AND TDNRLO = 0 ";
    
    if (isset($open_request->filtra_gruppo))
        $sql_WHERE .= " AND  TA_LIPC.TACINT = '{$open_request->filtra_gruppo}'";
    
        
        $sql_WHERE.= sql_where_by_combo_value('TD.TDTPLO', $open_request->TDTPLO);
        $sql_WHERE.= sql_where_by_combo_value('TD.TDAALO', $open_request->TDAALO);
        $sql_WHERE.= sql_where_by_combo_value('TD.TDNRLO', $open_request->TDNRLO);
        $sql_WHERE.= sql_where_by_combo_value('TD.TDTPCA', $open_request->TDTPCA);
        $sql_WHERE.= sql_where_by_combo_value('TD.TDAACA', $open_request->TDAACA);
        $sql_WHERE.= sql_where_by_combo_value('TD.TDNRCA', $open_request->TDNRCA);
        $sql_WHERE.= sql_where_by_combo_value('TD.TDDTOR', $open_request->TDDTOR);
        $sql_WHERE.= sql_where_by_combo_value('TD.TDASPE', $open_request->TDASPE);
    
    if (isset($open_request->filtro_type)){
        $filtro_type = $open_request->filtro_type;
        $filtro_c    = $open_request->filtro_c;
        switch ($filtro_type) {
            case 'LOTTO':
                //ricevo LP_2016_92601_ (in fondo opzionalmente trovo la ditta origine)
                $ar_filtro_c = explode('_' , $filtro_c);
                $sql_WHERE .= " AND TDTPLO = " . sql_t($ar_filtro_c[0]);
                $sql_WHERE .= " AND TDAALO = " . $ar_filtro_c[1];
                $sql_WHERE .= " AND TDNRLO = " . $ar_filtro_c[2];
                //if (count($ar_filtro_c) == 4)
                //    $sql_WHERE .= " AND TDDTOR = " . sql_t($ar_filtro_c[3]);                
                break;
            case 'CARICO':
                //ricevo CA_2016_92601_ (in fondo opzionalmente trovo la ditta origine)
                $ar_filtro_c = explode('_' , $filtro_c);
                $sql_WHERE .= " AND TDTPCA = " . sql_t($ar_filtro_c[0]);
                $sql_WHERE .= " AND TDAACA = " . $ar_filtro_c[1];
                $sql_WHERE .= " AND TDNRCA = " . $ar_filtro_c[2];
                if (count($ar_filtro_c) == 4)
                    $sql_WHERE .= " AND TDDTOR = " . sql_t($ar_filtro_c[3]);
                    
                break;
        }
    }
        
         
    $sql_fs = array('TDTPLO, TDAALO, TDNRLO,
                    TA_LIPC.TACINT AS TACINT_LIPC, PLFAS10 AS LINEA, PLOPE1', 
                    'TA_LIPC.TADESC AS D_LINEA', 
                    'TA_PUOP_OPE1.TADESC AS D_OPE1',
                    'SUBSTR(TA_LIP2.TAREST, 220, 10) AS TEMPO_S'
    );
    $sql_gb = array('TDTPLO, TDAALO, TDNRLO,
                     TA_LIPC.TACINT, PLFAS10, PLOPE1, TA_LIPC.TADESC', 
                    'TA_PUOP_OPE1.TADESC', 'SUBSTR(TA_LIP2.TAREST, 220, 10)');
    
        
    $sql_ff = array(
        "SUM(CASE WHEN PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_T_R",
        "SUM(CASE WHEN (PLDTEN>0 OR PLDTUS>0) AND PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_D_R",
        "SUM(CASE WHEN PLDTUS>0 AND PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_S_R"
    );
    
    
    if ($cfg_mod_Spedizioni['disabilita_digits_on_PL0'] == 'Y')
      $sql_join_on_PL0_PLNRDO = "PLNRDO";
    else
      $sql_join_on_PL0_PLNRDO = "digits(PLNRDO)";
          
    
     
        
    if ($open_request->no_produzione == 'Y')
        $sql_where_produzione = '';
    else
        $sql_where_produzione = " AND SUBSTR(TA_PUOP.TAREST, 141, 15) = 'PRODUZIONE' ";
        
    $sql = "SELECT " . implode(",", array_merge($sql_fs, $sql_ff)) . "
            FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
            INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
                ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'

            INNER JOIN {$cfg_mod_Spedizioni['file_colli']} PL
                       ON PLDT = CASE WHEN TD.TDDTOR = '' THEN '{$id_ditta_default}' ELSE TD.TDDTOR END
				      AND PLTIDO = TD.TDOTID
			          AND PLINUM = TD.TDOINU
				      AND PLAADO = TD.TDOADO
				      AND PLNRDO = SUBSTR(TD.TDONDO, 1, 6)

                 
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_PUOP_OPE1
                ON  TA_PUOP_OPE1.TADT = PL.PLDT
                AND TA_PUOP_OPE1.TAID = 'PUOP'
                AND TA_PUOP_OPE1.TANR = PL.PLOPE1

            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIPC
	  	  	    ON PL.PLDT = TA_LIPC.TADT AND TA_LIPC.TAID = 'LIPC' AND TA_LIPC.TANR = PL.PLFAS10 
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIP2
	  	  	    ON PL.PLDT = TA_LIP2.TADT AND TA_LIP2.TAID = 'LIP2' AND TA_LIP2.TANR = PL.PLFAS10
        
            WHERE " . $s->get_where_std() . " AND TDSWSP='Y'
			{$sql_WHERE_week}
            {$sql_where_produzione}
			{$sql_WHERE}
			GROUP BY " . implode(",", $sql_gb) . "
			ORDER BY " . implode(",", $sql_gb) . "
			";			

			
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    
    //creo l'albero
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)){
      
        //controllare perch� non tornano i totali come report e gestire la capacit�
       
        //stacco dei livelli
        $cod_liv0 = implode("_", array_map('trim', array($row['TDTPLO'], $row['TDAALO'], $row['TDNRLO'])));	//lotto
        $cod_liv1 = trim($row['TACINT_LIPC']);
        $cod_liv2 = trim($row['LINEA']);    //PLFAS10
        $cod_liv3 = trim($row['PLOPE1']);
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        //LOTTO
        $liv =$cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = $liv;
            $ar_new['expanded'] = true;
            $ar_new['liv'] = 'liv_0';
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //TACINT_LIPC
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = $liv;
            //$cp = new SpedCapacitaProduttiva;
            //$ar_new['capac'] =  $cp->get_tot_by_data($row['TDDTEP'], $liv);
            $ar_new['data'] =  trim($row['TDDTEP']);
            $ar_new['expanded'] = true;
            $ar_new['liv'] = 'liv_1';
            $ar_r["{$liv}"] = $ar_new;
        }
        
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //LINEA (PLFAS10)
        $liv=$cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            if(trim($liv) == '')
                $ar_new['task'] = 'NON ASSEGNATO';
            else
                $ar_new['task'] = "[{$row['LINEA']}] " . trim($row['D_LINEA']);;
           
            //$cp = new SpedCapacitaProduttiva;
            //$ar_new['capac'] =  $cp->get_tot_by_data($row['TDDTEP'], $liv); 
            $ar_new['data'] =  trim($row['TDDTEP']);
            
            $ar_new['PLFAS10'] = rtrim($row['LINEA']);
            
            
    /*        
         $sql_cs = "SELECT CSDTRG FROM {$cfg_mod_Spedizioni['file_calendario']} CAL
                    WHERE CSDT = '{$id_ditta_default}' AND CSDTRG < {$ar_new['data']}
                    AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
                    AND CSTPGG = 'L'
                    ORDER BY CSDTRG DESC
                    LIMIT 3";
                    
            
            $stmt_cs = db2_prepare($conn, $sql_cs);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_cs);
            echo db2_stmt_errormsg($stmt_cs);
            
            $i = 1;
            while ($r = db2_fetch_assoc($stmt_cs)){
                $ar_new["data_{$i}"] = $r['CSDTRG'];
                $i++;
            }
      */      
                   
            
            $ar_new['expanded'] = false;
            $ar_new['liv'] = 'liv_2';
            $ar_r["{$liv}"] = $ar_new;
        }
        
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //PLOPE1
        $liv=$cod_liv3;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = $row['D_OPE1'];
            //$ar_new['desc'] = trim($row['D_LINEA']);
           
            $ar_new['data'] =  trim($row['TDDTEP']);
            $ar_new['liv'] = 'liv_3';
            $ar_new['leaf'] = true;
            
            $ar_new['PLFAS10'] = rtrim($row['LINEA']);
            $ar_new['PLOPE1']  = rtrim($row['PLOPE1']);
            
            $ar_r["{$liv}"] = $ar_new;
        }
        
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
    }
    
  
    
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array('success' => true, 'children' => $ret));
    exit;
}


// ******************************************************************************************
// GRID PER AVANZAMENTO COLLI LOTTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_avanzamento_lotto_grid'){
    
    
    if (strlen($m_params->da_data) > 0){
    
        $add_day = (int)substr($m_params->col_name, 2, 2) - 1;
        $m_data = date('Ymd', strtotime($m_params->da_data . " +{$add_day} days"));
        
        $sql_data = "SELECT CSAARG, CSNRSE, CSGIOR FROM {$cfg_mod_Spedizioni['file_calendario']} CS
                     WHERE CSDT='{$id_ditta_default}' AND CSCALE ='*CS' AND CSDTRG = {$m_data}";
        
        $stmt_data = db2_prepare($conn, $sql_data);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_data);
        $row_data = db2_fetch_assoc($stmt_data);
        
        $day = $row_data['CSGIOR'];
        $week = $row_data['CSNRSE'];
        $year = $row_data['CSAARG'];
    }    
?>
{"success":true, 

  m_win: {
   title: 'Riepilogo colli per linea produttiva',
   width: 1000, height: 550,
   iconCls : 'icon-folder_search-16'
  },

  items:
	{
	    xtype: 'treepanel' ,
		flex: 1,
        useArrows: true,
        rootVisible: false,
        loadMask: true,
	    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['desc', 'tempo_s', 'data', 'liv', 'task', 'capac', 'diff_val', 'perc', 
                    'LIV_GEN', 'LIV_DET', 'RIFE', 'LINEA', 'D_LINEA', 'data_1', 'data_2', 'data_3',
                    {name: 'non_scaricati', type: 'float'}, {name: 'COLLI_T', type: 'float'}, 
                    {name: 'COLLI_D', type: 'float'}, {name: 'COLLI_S', type: 'float'}, 'PLFAS10', 'PLOPE1'],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_avanzamento_lotto',
						actionMethods: {read: 'POST'},
						extraParams: <?php echo acs_je($m_params); ?>,
                       doRequest: personalizza_extraParams_to_jsonData  ,     
						//Add these two properties
					      actionMethods: {
					          read: 'POST'
					      },
						
						reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'children'						            
				        }       				
                    }

                }),
	   columns: [
			{xtype: 'treecolumn', 
    		 text: 'Centro/Linee', 	
    		 flex: 1,
    		 dataIndex: 'task'
    	    },
		   {header: 'Capacit�', dataIndex: 'capac', width: 90, renderer: floatRenderer0, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer0(value); 
	        }},	
	        
	          {header: 'Diff. in valore', dataIndex: 'diff_val', width: 100, align: 'right',
	          summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	             return floatRenderer0(value); 
	           },
	           renderer: function (value, metaData, record, row, col, store, gridView){
	             if (record.get('liv') != 'liv_0'){
			     if(record.get('perc') >= 5)
				 	metaData.tdCls += ' tpSfondoRosa';
				 else if(record.get('perc') >= -5)
				 	metaData.tdCls += ' tpSfondoGiallo';
				 else
				 	metaData.tdCls += ' tpSfondoVerde';
				 	
  			 	 return floatRenderer0(value);
				}else{
				   return floatRenderer0(value);
				}
			  
			  }
	        
	        },
	        
	          {header: '%', dataIndex: 'perc', width: 50, 
	          summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer1(value); 
	        }, renderer: function (value, metaData, record){
	             if (record.get('liv') != 'liv_0'){
			     if(record.get('perc') >= 5)
				 	metaData.tdCls += ' tpSfondoRosa';
				 else if(record.get('perc') >= -5)
				 	metaData.tdCls += ' tpSfondoGiallo';
				 else
				 	metaData.tdCls += ' tpSfondoVerde';
				 	
				 	 return floatRenderer1(value);
				}else{
				    return floatRenderer1(value);
				}
			   
			  }},				
						
			{header: 'Colli totale', dataIndex: 'COLLI_T', width: 90, align: 'right',
				summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
		               return floatRenderer0(value); 
				},
				renderer: function(value, metaData, record){
				    if(record.get('liv') == 'liv_1' || record.get('liv') == 'liv_2'){
				    	diff = record.get('COLLI_T') - record.get('capac');
				    	perc = diff/record.get('capac') * 100;
						q_tip = diff + ' , '+ floatRenderer1(perc) + '%';
		      			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
		    	 		return floatRenderer0(value);			
		    	 	}else{	
		    	 		return floatRenderer0(value);	
		    	 	}				    	 			    	
		    	}					        
					        
			},
	        {header: 'Colli disponibili', dataIndex: 'COLLI_D', width: 90, align: 'right', renderer: floatRenderer0,
 				summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer0(value); 
	        }}, 
	        {header: 'Colli spediti', dataIndex: 'COLLI_S', width: 90, align: 'right', renderer: floatRenderer0,
 				summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer0(value); 
	        }},
	        {header: 'Tempo stimato', dataIndex: 'tempo_s', width: 100, renderer: floatRenderer3, summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer3(value); 
	        }},
			
		],
		
		
		listeners: {
		
		    afterrender: function (comp) {
		    	<?php  if (isset($week)) { ?>
 					comp.up('window').setTitle(comp.up('window').title + '<?php echo " - Settimana {$week}"; ?>');
 				<?php } ?>
 			},
		    itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     var m_grid = this;
					     if(rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3'){	
					     
					            ar_params = {};
    							ar_params['filtro'] = rec.data;
    							//ar_params['filtro']['col_name'] = col_name;
    							ar_params['filtro']['da_data'] = rec.get('data');
    							ar_params['filtro']['gruppo'] = rec.get('task');
    							ar_params['filtro']['liv'] = rec.get('liv');
    							
					     
			    		 voci_menu.push({
			         		text: 'Visualizza situazione al gg-1/mm',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		   	ar_params['filtro']['c_data'] = rec.get('data_1');
			        		   acs_show_win_std('Visualizza ordini situazione al gg-1/mm ' + date_from_AS(rec.get('data_1')), 'acs_riepilogo_colli_ordini.php?fn=open_tab', Ext.encode(ar_params), 1200, 500, null, 'icon-leaf-16');
					  	
				                }
			    		});	
			    		
			    		voci_menu.push({
			         		text: 'Visualizza situazione al gg-2/mm',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		   	ar_params['filtro']['c_data'] = rec.get('data_2');
    					     acs_show_win_std('Visualizza ordini situazione al gg-2/mm '+ date_from_AS(rec.get('data_2')), 'acs_riepilogo_colli_ordini.php?fn=open_tab', Ext.encode(ar_params), 1200, 500, null, 'icon-leaf-16');
					  	
				                }
			    		});	
			    		
			    		voci_menu.push({
			         		text: 'Visualizza situazione al gg-3/mm',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		   	ar_params['filtro']['c_data'] = rec.get('data_3');
    					     acs_show_win_std('Visualizza ordini situazione al gg-3/mm '+ date_from_AS(rec.get('data_3')), 'acs_riepilogo_colli_ordini.php?fn=open_tab', Ext.encode(ar_params), 1200, 500, null, 'icon-leaf-16');
					  	
				                }
			    		});		
			    	 }
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			   },	
				celldblclick: {								

					fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	iEvent.preventDefault();
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;	
					  		
					  	if(rec.get('liv') == 'liv_1' || rec.get('liv') == 'liv_2'){							
					  		if(col_name == 'capac'){
					  	      var my_listeners = {
    			    		  			afterOkSave: function(from_win){
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					     acs_show_win_std('Modifica capacit&agrave; - ' + rec.get('task'), 'acs_riepilogo_colli_linea_prod.php?fn=open_form', {da_data : rec.get('data'), gruppo : rec.get('task'), capa : rec.get('capac'), year : '<?php echo $year; ?>', week : '<?php echo $week; ?>'}, 350, 300, my_listeners, 'icon-pencil-16');
					  	
					  		}
					  	}
					  	
					  	if(rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3'){
					  	    if(col_name != 'capac'){
					  	    
					  	    	ar_params = <?php echo acs_je($m_params->open_request) ?>;
    							ar_params['col_name'] = col_name;
    							
    							ar_params['PLFAS10'] = rec.get('PLFAS10');
    							if (rec.get('liv') == 'liv_3') ar_params['PLOPE1'] = rec.get('PLOPE1');
    							
    							acs_show_win_std(null, '../desk_prod/acs_json_elenco_colli.php?fn=get_json_grid', ar_params);
					  	    }
					  	}	
					  		
					}
				}				
		}, 	 viewConfig: {
				 toggleOnDblClick: false,
		         getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           
 		           return v;																
		         }   
		    }
		
				 
	}

}	


<?php
	exit;
}

