<?php 

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();
$main_module =  new DeskProd();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $main_module->get_cfg_mod();
$row_ma = $desk_art->get_TA_std('MACMA', $m_params->TAKEY1);
$t_dettagli = trim($row_ma['TADESC']);

$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "MACRI - Ricambi [{$m_params->TAKEY1}]",
    'descrizione' => "Gestione ricambi",
    'form_title' => "Dettagli ricambi [{$m_params->TAKEY1}] {$t_dettagli}",
    'fields_preset' => array(
        'TATAID' => 'MACRI',
        'TAKEY1' => $m_params->open_request->TAKEY1
    ),
  
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TAKEY2', 'TADESC'),
    'fields_grid' => array('TAKEY2', 'TADESC', 'TAMAIL', 'TAINDI', 'immissione'),
    'fields_form' => array('TAKEY2', 'TADESC', 'TAMAIL', 'TAINDI'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Macchina', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TAKEY2' => array('label'	=> 'Codice',  'maxLength' => 10, 'fw'=>'width: 160'),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        'TAINDI' => array('label'	=> 'Riferimento', 'maxLength' => 60),
       
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
        
        
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
