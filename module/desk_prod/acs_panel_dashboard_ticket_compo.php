<?php

function _dashboard_main_panel($m_params){
    $c = new Extjs_compo('panel', 'vbox');
    $c->set(array('itemId' => 'ticket_dashboard', 'title' => 'Dashboard', 'closable' => true, 'autoScroll' => true));
    
    $c->add_items(array(
        extjs_compo('panel', layout_ar('hbox'), null, array('width' => '100%', 'height' => 120,
            'items' => array(
                _form_periodo($m_params),
                _riepilogo_tot($m_params),
            ))),
        
        extjs_compo('panel', layout_ar('hbox'), null, array('width' => '100%', 'height' => 250,
            'items' => array(
                _ope_rep_mac($m_params),
                _ticket_aperti($m_params),
            ))),
        
        //array('xtype' => 'tbfill', 'height' => 10),
        _grid_avanzamento($m_params),
        
    ));
    $c->set(array(
        
        
    ));
     $c->set(array(
        'aggiorna_generale' => extjs_code("
            function(obj){
           
                        var me = this,
                        grid_avanzamento     = me.down('grid_av'),
                        display_field_ore    = me.down('tot_ore'),
                        display_field_ticket = me.down('tot_ticket'),
                        display_field_media  = me.down('media_ore_ticket'),
                    //SEPARO ORE E MINUTI PER CALCOLARE I SESSANTESIMI
                        h                    = Math.trunc(obj.tot_ore),
                        m                    = Math.round((obj.tot_ore - h) * 60),
                        min                  = m.toString();
                        min                  = min.padStart( 2 , '0');

                    console.log(grid_avanzamento);
                    
                    //REIMPOSTO DATI INIZIALI NELLE GRID
                    grid_avanzamento.getStore().proxy.extraParams.field      = null;
                    grid_avanzamento.getStore().proxy.extraParams.valueField = null;
                    grid_avanzamento.getStore().reload();

                    //rimuovo l'eventuale selezione su tutte le grid
                    Ext.each(this.getView().query('grid'), function(grid) {
                        grid.getSelectionModel().deselectAll();
                    }, this);

                    
                    //display_field_ore.setValue(obj.tot_ore);        //CENTESIMI
                    display_field_ore.setValue(h + \":\" + min);      //SESSANTESIMI
                    
                    display_field_ticket.setValue(obj.tot_ticket);
                    
                    display_field_media.setValue(obj.media_ore_ticket);


            }
        "),
        
    ));
    
    
    return $c->code();
}

function _form_periodo($m_params){
    
    $c = new Extjs_Form('hbox', 'Periodo');
    $c->set(array(/*'height' => 120,*/ 'itemId' => 'form_periodo'));
    
    $c->add_items(array(
        extjs_datefield('giorno_da', '', date('Ymd', strtotime("-1 week")), array('margin' => '7 0 0 0')),
        extjs_datefield('giorno_a', '', date('Ymd'), array('margin' => '7 5 0 0')),
        extjs_button("Filtra", "small", null, array('height' => 40,
            'handler' => extjs_code("
                    function(b) {
                    var me = this,
                        p_dash = this.up('#ticket_dashboard'),
                    form_values = b.up('form').getValues();

                    //REFRESH TUTTI I DATI
                   // Comm.m_api_call.call(me, me, {m_api: {p: 'ticket_man_test', a: 'get_data_totali', data: {form_values: form_values}}}, null, null, 'aggiorna_generale');

                    //REFRESH DATI GRID E PASSO PERIODO A BOTTONI
                    Ext.each(p_dash.query('grid'), function(grid) {
                        grid.getStore().proxy.extraParams.form_values = form_values;
                        grid.getStore().load();
                    }, this);


                   }
                ")
        )),
        extjs_button("Riepilogo tickets", "small", null, array( 'height' => 40,
          'handler' => extjs_code("
                    function() {



                   }
                ")
        )),
        
        extjs_button("Dettagli tickets", "small", null, array( 'height' => 40,
            'handler' => extjs_code("
                    function() {
                   }
                ")
        )),
        
    ));
    
    return $c->code();
}

function _riepilogo_tot($m_params){
    
    $c = new Extjs_Form(layout_ar('hbox'), '');
    $c->set(array('flex' => 1, 'height' => 120, 'itemId' => 'riepilogo_tot'));
    
    $c->add_items(array(
        extjs_compo('panel', layout_ar('hbox'), null, array('flex' => 1, 'height' => 120,
            'items' => array(
                
                extjs_compo('panel', layout_ar('hbox'), '<center><b>TOT. ORE</b></center>', array('height' => 120, 'flex' => 1,
                    'items' => array(
                        array(
                            'xtype'=> 'displayfield',
                            'flex'=> 1,
                            'height' => 85,
                            'reference' => 'tot_ore',
                            'fieldStyle' => 'font-size: 40px; color: black; text-align: center; text-style: bold;'
                        )
                    ))),
                
                extjs_compo('panel', layout_ar('hbox'), '<center><b>TOT. TICKET</b></center>', array('height' => 120, 'flex' => 1,
                    'items' => array(
                        array(
                            'xtype'=> 'displayfield',
                            'flex'=> 1,
                            'height' => 85,
                            'reference' => 'tot_ticket',
                            'fieldStyle' => 'font-size: 40px; color: black; text-align: center; text-style: bold;'
                        )
                    ))),
                
                extjs_compo('panel', layout_ar('hbox'), '<center><b>MEDIA</b></center>', array('height' => 120, 'flex' => 1,
                    'items' => array(
                        array(
                            'xtype'=> 'displayfield',
                            'flex'=> 1,
                            'height' => 85,
                            'reference' => 'media_ore_ticket',
                            'fieldStyle' => 'font-size: 40px; color: black; text-align: center; text-style: bold;'
                        )
                        
                    )))
                
            )))
        
    ));
    
    return $c->code();
    
}

function _ope_rep_mac($m_params){
    
    $c = new Extjs_Form(layout_ar('hbox'), '');
    $c->set(array('flex' => 1, 'height' => 250, 'itemId' => 'ope_rep_mac'));
    
    $c->add_items(array(
        //da fare
        _grid_ope($m_params),
        _grid_rep($m_params),
        _grid_mac($m_params),
        
    ));
    
    return $c->code();
    
}

function _grid_ope($m_params){
    
    $c = new Extjs_Grid(null);
    $c->set(array('itemId' => 'operatori', 'flex' => 1));
    $c->set_features(array('filters' => array()));
    $c->set(array(
        'columns' => array(
            
            grid_column_h('<center>Operatori</center>', 'ute',  'f1', 'Codice articolo', null, array('filter_as' => 'string'))
        ),
        
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('ute'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_data_grid_operatori'),array('open_parameters' => $m_params))),
        'listeners'   => array(
            'itemclick' => extjs_code(grid_itemclick("
                
                
           "))
            
        )
    ));
    
    return $c->code();
}

function _grid_rep($m_params){
    
    $c = new Extjs_Grid(null);
    $c->set(array('itemId' => 'reparto', 'flex' => 1));
    $c->set_features(array('filters' => array()));
    $c->set(array(
        'columns' => array(
            
            grid_column_h('<center>Reparto</center>', 'reparto',  'f1', 'Codice articolo', null, array('filter_as' => 'string'))
        ),
        
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('reparto'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_chart_ticket_reparto'),array('open_parameters' => $m_params))),
        'listeners'   => array(
            'itemclick' => extjs_code(grid_itemclick("
                
                
           "))
            
        )
        
    ));
    
    return $c->code();
}

function _grid_mac($m_params){
    
    $c = new Extjs_Grid(null);
    $c->set(array('itemId' => 'macchine', 'flex' => 1));
    $c->set_features(array('filters' => array()));
    $c->set(array(
        'columns' => array(
            
            grid_column_h('<center>Macchine</center>', 'macchina',  'f1', 'Codice articolo', null, array('filter_as' => 'string'))
        ),
        
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('macchina'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_chart_ticket_macchina'),array('open_parameters' => $m_params))),
        'listeners'   => array(
            'itemclick' => extjs_code(grid_itemclick("
                
                
           "))
            
        )
        
    ));
    
    return $c->code();
}

function _ticket_aperti($m_params){
    
    $c = new Extjs_Grid(null, '<center>Tickets Aperti</center>');
    $c->set(array('itemId' => 'grid_ape', 'flex' => 1));
    $c->set_features(array('filters' => array()));
    $c->set(array(
        'columns' => array(
            
            grid_column_h('Ticket ID', 'id',  'w110', 'Codice articolo', null, array('filter_as' => 'string')),
            grid_column_h('Data / Ora', 'data_ora_gen',  'w110', 'Codice articolo', null, array('filter_as' => 'string')),
            grid_column_h('Operatore', 'ute',  'w110', 'Codice articolo', null, array('filter_as' => 'string'))
        ),
        
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('id', 'data_ora_gen', 'ute', 'tempo_ticket'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_data_grid_aperti'),array('form_values' => $m_params))),
        'listeners'   => array(
            'itemclick' => extjs_code(grid_itemclick("
                
                
           "))
            
        )
        
    ));
    
    return $c->code();
    
    
}

function _grid_avanzamento($m_params){
    $c = new Extjs_Grid(null, '<center>Ultimi Avanzamenti Tickets</center>');
    $c->set(array('itemId' => 'grid_av', 'width' => '100%'));
    $c->set_features(array('filters' => array()));
    $c->set(array(
        'columns' => array(
            
            grid_column_h('Ticket ID', 'n_ticket',  'w80', 'Codice articolo', null, array('filter_as' => 'string')),
            grid_column_h('Data / Ora', 'data_ora_gen',  'f1', 'Codice articolo', null, array('filter_as' => 'string')),
            grid_column_h('Stato', 'sta_tes',  'f1', 'Codice articolo', null, array('filter_as' => 'string')),
            grid_column_h('Reparto', 'reparto',  'f1', 'Codice articolo', null, array('filter_as' => 'string')),
            grid_column_h('Macchina', 'macchina',  'f1', 'Codice articolo', null, array('filter_as' => 'string')),
            grid_column_h('Note', 'nota',  'f1', 'Codice articolo', null, array('filter_as' => 'string')),
            grid_column_h('Ora', 'tempo_ticket',  'w70', 'Codice articolo', null, array('filter_as' => 'string'))
        ),
        
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('n_ticket', 'data_ora_gen', 'ute', 'sta_tes', 'reparto', 'macchina', 'nota', 'tempo_ticket'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_data_grid_avanzamento'), array('form_values' => $m_params))
        ),
        
        
    ));
    
    return $c->code();
    
}

