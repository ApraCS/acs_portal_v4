<?php

require_once("../../config.inc.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$cfg_mod = $main_module->get_cfg_mod();

$m_params = acs_m_params_json_decode();




//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//---------------------------------------------------------------------
    
    $sql = "SELECT TA_R.TAKEY1 AS REPARTO, TA_M.TAKEY1 AS MACCHINA, TA_R.TADESC AS D_REP, TA_M.TADESC AS D_MAC
            FROM {$cfg_mod_DeskProd['file_tabelle_man']} TA_R
            LEFT OUTER JOIN {$cfg_mod_DeskProd['file_tabelle_man']} TA_M
                ON TA_R.TADT = TA_M.TADT AND TA_M.TATAID = 'MACMA' AND TA_M.TARIF1 = TA_R.TAKEY1
            WHERE TA_R.TADT = '{$id_ditta_default}' AND TA_R.TATAID = 'REPMA'
            ";
    
    #ToDO: recuperare solo gli orari della settimana in corso!!!!
    $sql_d = "SELECT ARIDTA, ARSEQU
              FROM {$cfg_mod_DeskProd['file_ticket_R']} 
              LEFT OUTER JOIN {$cfg_mod_DeskProd['file_ticket_T']}
                ON ATIDTK = ARIDTK AND ARTPAV = 'ATTI' 
              WHERE ATTPTK = 'ATTI' AND ATARTI = ?";
    
    $stmt_d = db2_prepare($conn, $sql_d);
    echo db2_stmt_errormsg();
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while ($row = db2_fetch_assoc($stmt)){
        
        $liv1 = trim($row['REPARTO']);  //REPARTO
        $liv2 = trim($row['MACCHINA']); //MACCHINA
        $tmp_ar_id = array();
        
        //liv1: REPARTO
        $d_ar = &$ar;
        $c_liv = $liv1;
        $tmp_ar_id[] = $c_liv;
        if (!isset($d_ar[$c_liv])){
            $d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id),  "children"=>array());
            $d_ar[$c_liv]['liv']   = 'liv_1';
            $d_ar[$c_liv]['liv_type']   = 'REPARTO';
            $d_ar[$c_liv]['liv_c'] = $liv1;
            $d_ar[$c_liv]['task']  = "[{$liv1}] ".acs_u8e($row['D_REP']);
            $d_ar[$c_liv]['expanded'] = true;
        }   
        $d_ar_liv0 = &$d_ar[$c_liv];    //puntamento a reparto
        
        //liv2: MACCHINA
        $d_ar = &$d_ar[$c_liv]['children'];
        $c_liv = $liv2;
        $tmp_ar_id[] = $c_liv;
        if (!isset($d_ar[$c_liv])){
            $d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id),  "children"=>array());
            $d_ar[$c_liv]['liv']   = 'liv_3';
            $d_ar[$c_liv]['liv_c'] = $liv2;
            $d_ar[$c_liv]['liv_type']   = 'MACCHINA';
            $d_ar[$c_liv]['task']  = "[{$liv2}] ".acs_u8e($row['D_MAC']);
            $d_ar[$c_liv]['leaf']  = true;

            
            $result = db2_execute($stmt_d, array($liv2));
            $count = 0;
            while ($row_d = db2_fetch_assoc($stmt_d)){
                $count++;
                $s_time = explode('_', $row_d['ARSEQU']);
                $da = print_ora($s_time[0], 4);
                $a = print_ora($s_time[1], 4);
                $d_ar[$c_liv]["gg_{$row_d['ARIDTA']}"] .= "Da {$da} a {$a} <br>";
                $d_ar[$c_liv]["s_{$row_d['ARIDTA']}"] = "Y";
                
                $d_ar_liv0['orari'][$row_d['ARIDTA']][$c_liv] = 1;

            }

            
        }
    
    } //while
    
    
    foreach($ar as $kar => $r){  
        
        //scorro gli orari presenti... se il conteggio e' uguale al numero di macchine allora Verde
        if (is_array($r['orari']))
        foreach($r['orari'] as $k_o => $o){
            if (count($o) == count($r['children']))
                $ar[$kar]["c_{$k_o}"] = 2; //verde
            else
                $ar[$kar]["c_{$k_o}"] = 1; //arancione
        }
        
        $ret[] = array_values_recursive($ar[$kar]);
    }
    echo acs_je($ret);
    exit;
}



//----------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_panel'){
//----------------------------------------------------------------
?>

{"success":true, "items": [

        {
        xtype: 'treepanel' ,
        multiSelect: 'MULTI',
        //selType: 'cellmodel',
        selType: 'cellmodel',
        simpleSelect: true,
        
        selModel : {
            mode : 'MULTI' //or MULTI
        },
       
        title: 'Time table - Week - <?php echo date('y.W', strtotime($m_params->open_request->data_start)) ?>' ,
	    tbar: new Ext.Toolbar({
	            items:['<b> Manutenzione settimanale orari di apertura/chiusura macchine</b>', '->',            
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,         
		    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['id', 'task', 'liv', 'liv_type'
                    
                    <?php for ($i=0; $i<7; $i++){ 
        	    	 $d = date('Ymd', strtotime("{$m_params->open_request->data_start} +{$i} days")); ?>
        	    			, <?php echo j("gg_{$d}"); ?>
        	    			, <?php echo j("s_{$d}"); ?>
        	    			, <?php echo j("c_{$d}"); ?>
        	    			, <?php echo j("acs_selected_{$d}"); ?>
        	    	 <?php } ?>
                    ],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						extraParams: {
						  open_request: <?php echo acs_je($m_params->open_request); ?>,
	                      form_values: <?php echo acs_je($m_params->form_values); ?>
	                  
                      }
                    , doRequest: personalizza_extraParams_to_jsonData     
					, reader: {root: 'children'}        				
                    }

                }),
                
                <?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
    	    			
            columns: [ {xtype: 'treecolumn', 
        	    		text: 'Reparto / Macchina', 	
        	    		width: 300,
        	    		dataIndex: 'task',
        	    		}
        	    		
        	    		<?php for ($i=0; $i<7; $i++){ ?>
        	    			<?php 
        	    			    $d = date('Ymd', strtotime("{$m_params->open_request->data_start} +{$i} days"));
        	    			    $d_out = print_date($d, "<b>%d</b>/%m/%y") . " - <b>" . ucfirst(print_date($d, "%a")) . "</b>";
        	    			 ?>
        	    			
        	    			, { 
		        	    		text: <?php echo j($d_out); ?>,
        			    		flex: 1,
        			    		dataIndex: <?php echo j("gg_{$d}"); ?>,
        			    		renderer: function (value, metaData, rec){	
		    		               if(rec.get(<?php echo j("s_{$d}"); ?>) == 'Y')
		    							metaData.tdCls += ' sfondo_giallo';
		    					   if(rec.get(<?php echo j("c_{$d}"); ?>) == 2)	//tutte macchine con orari
		    							metaData.tdCls += ' sfondo_verde';
	    						   else if(rec.get(<?php echo j("c_{$d}"); ?>) == 1) //alcune macchine con orari
		    							metaData.tdCls += ' sfondo_arancione';
		    							
		    					   if (rec.get('acs_selected_' + <?php echo j("{$d}"); ?>) == true)
		    					   		metaData.tdCls += ' acs_selected';
		    					   return value;
		    					}
        	    			}
        	    		<?php } ?>
        	    		    		
    	    ],
    	    
			listeners: {	
 				itemcontextmenu : function(grid, rec, node, index, event) {
					grid.panel.acs_actions.show_menu_dx(grid, rec, node, index, event);
    			}, 
    			
    			 celldblclick: {								
					fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						var rec = iView.getRecord(iRowEl), 
							col_name = iView.getGridColumns()[iColIdx].dataIndex;
						
						if (col_name == 'task'){
							//selezione di tutta la settimana, per reparto o macchina
							if (rec.get('liv_type') == 'MACCHINA'){
							
									<?php for ($i=0; $i<5; $i++){ //fino a vene
                        	    	    $d = date('Ymd', strtotime("{$m_params->open_request->data_start} +{$i} days")); 
                        	    	?>
                        	    		rec.set('<?php echo "acs_selected_{$d}"; ?>', true);
                        	    	<?php } ?>
							
							} //macchina
							
							if (rec.get('liv_type') == 'REPARTO'){
								Ext.Array.each(rec.childNodes, function(rec_mac){
									<?php for ($i=0; $i<5; $i++){ //fino a vene
                        	    	    $d = date('Ymd', strtotime("{$m_params->open_request->data_start} +{$i} days")); 
                        	    	?>
                        	    		rec_mac.set('<?php echo "acs_selected_{$d}"; ?>', true);
                        	    	<?php } ?>
								});
							} //macchina
							
							return true;
						}
						
						//click su una cella (data)
						if (rec.get('liv_type') == 'MACCHINA'){
							if (rec.get('acs_selected_' + col_name.split('_')[1]) == true)
								rec.set('acs_selected_' + col_name.split('_')[1], false);
							else
								rec.set('acs_selected_' + col_name.split('_')[1], true);
						}
    				}
    			}
			}
			
	  //funzioni / utility  	
  	  , acs_actions: {   	
      	 show_menu_dx: function(grid, rec, node, index, event) {
      	 	event.stopEvent();
      	 	
      	 	  if (rec.get('liv_type') != 'MACCHINA'){
      	 	  	return false;
      	 	  }
      	 
      	 
      	 		//recupero la cella
    				var xPos = event.getXY()[0];
    			    var cols = grid.getGridColumns();
    			    var colSelected = null;
    			
    			    for(var c in cols) {
    			
    			        var leftEdge = cols[c].getPosition()[0];
    			        var rightEdge = cols[c].getSize().width + leftEdge;
    			
    			        if(xPos>=leftEdge && xPos<=rightEdge) {
    			            colSelected = cols[c];
    			        }
    			     }	
    			var col_name =  colSelected.dataIndex;
      	      
      	       var voci_menu = [];
      	       var id_selected = grid.getSelectionModel().getSelection();
					 list_selected_id = [];
  	                 for (var i=0; i<id_selected.length; i++){		
  	                 	if (col_name != 'task') 			
		  					list_selected_id.push({rep_mac: id_selected[i].get('id'), data: col_name.split('_')[1]});		  
  					 }
  					  
  			   //accodo tutti i record selezionati con dblclick (acs_selected_gg = true)
  			   grid.store.each(function(item){
  			   		
  			   		if (item.get('liv_type') == 'MACCHINA'){
    				          <?php for ($i=0; $i<7; $i++){ 
                        	    	 $d = date('Ymd', strtotime("{$m_params->open_request->data_start} +{$i} days")); ?>
                        	    			
                        	    			if (item.get('<?php echo "acs_selected_{$d}"; ?>') == true){
                        	    				list_selected_id.push({rep_mac: item.get('id'), data: <?php echo "{$d}"; ?>});
                        	    			}
                        	    	 <?php } ?>
                     }   	    	 
              });
  			   
  			       
  			   
  			   

      	       voci_menu.push({
		      		text: 'Assegna orari',
		    		iconCls: 'icon-clock-16',
		    		handler: function() {  
		    		

    			  	 var my_listeners = {
        					afterConfirm: function(from_win){						
        						grid.getStore().treeStore.load();
        						from_win.destroy();
        					}
        					}
          				
		    			 acs_show_win_std(null, 
		 				 'acs_panel_ticket_man_orari.php?fn=open_form',
	   				     	{
	   				     		col_name : col_name, 
	   				     		list_selected_id: list_selected_id
	   				     	}, null, null, my_listeners);
		    		}
                    });
                    
        			
			var menu = new Ext.menu.Menu({
        	items: voci_menu
			}).showAt(event.xy);
			
			return false;
      	} //show_menu_dx
    },	viewConfig: {
				 toggleOnDblClick: false,
		         getRowClass: function(record, index) {			        	
		           v = record.get('liv');		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');		           
 		           return v;																
		         }   
		    }	
    	    
    	    
    	}	
	 	
	]  	
  }


<?php 
exit;
}