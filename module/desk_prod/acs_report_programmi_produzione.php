<?php

require_once "../../config.inc.php";
require_once("../base/_rilav_g_history_include.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_config_distinta.php';

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

if ($_REQUEST['fn'] == 'open_form'){
    
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	         
						 {
							name: 'f_prog',
							xtype: 'combo',
							fieldLabel: 'Programma',
							forceSelection: true,								
							displayField: 'text',
							valueField: 'id',								
							emptyText: '- seleziona -',
					   		queryMode: 'local',
                 		    minChars: 1, 	
            				labelWidth : 80,
            				anchor: '-15',
            				width : 150	,				
							store: {
								editable: false,
								autoDestroy: true,
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
							     <?php echo acs_ar_to_select_json($s->find_TA_std('PPP', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
							    ]
							}
							,listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
	            				 },
	            				 change: function(field,newVal) {	
                              		if (!Ext.isEmpty(newVal)){
                              		
                                    	c_gruppo = this.up('form').down('#c_gruppo');                      		 
                                     	c_gruppo.store.proxy.extraParams.distinta = newVal;
                                    	c_gruppo.store.load();                          
                                     }
                 

                }
	         				 }
							
					 },
					  {
							name: 'f_gruppo',
							xtype: 'combo',
							itemId: 'c_gruppo',
							fieldLabel: 'Gruppo',
							forceSelection: true,								
							displayField: 'text',
							valueField: 'id',								
							emptyText: '- seleziona -',
					   		queryMode: 'local',
                 		    minChars: 1, 	
            				labelWidth : 80,
            				anchor: '-15',
            				width : 150	,				
							store: {
            			        autoLoad: true,
            					proxy: {
            			            type: 'ajax',
            			            url: 'acs_panel_indici_naviga.php?fn=get_json_data_gruppo', 
            			            actionMethods: {
            				          read: 'POST'
            			        },
            	                	extraParams: {
            	    		    		distinta: '',
            	    				},				            
            			            doRequest: personalizza_extraParams_to_jsonData, 
            			            reader: {
            			            type: 'json',
            						method: 'POST',						            
            				            root: 'root'						            
            				        }
            			        },       
            					fields: ['id', 'text'],		             	
            	            },
							
							listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
	            				 }
	         				 }
							
					 },
					 
					 	{
						xtype: 'checkboxgroup',
						fieldLabel: 'Note operative',
						labelWidth : 180,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_nope' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					},
						{
						xtype: 'checkboxgroup',
						fieldLabel: 'Note configurazione',
						labelWidth : 180,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_ncfg' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					}
	            ],
	            
				buttons: [	
						{
		        		  text: 'Report',
			              scale: 'large',
			              iconCls: 'icon-print-32',
			              handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
		        		}
		       ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}


// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff; font-weight: bold;}   
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
    
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {
        border-collapse: unset;
	     	
    }
        .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 


<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php



$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);
$sql_where = "";
$ar = array();
if(isset($form_values->f_prog) && strlen($form_values->f_prog) > 0)
    $sql_where .= " AND CDDICL = '{$form_values->f_prog}'";

if(isset($form_values->f_gruppo) && strlen($form_values->f_gruppo) > 0)
    $sql_where .= " AND (CDCMAS = '{$form_values->f_gruppo}' OR CDSLAV =  '{$form_values->f_gruppo}')";


    
   $sql_ar['select'][] = 'TA.*';
   $m_table_config = array( 'f_ditta' => 'CDDT', 'field_IDPR' => 'CDIDPR');
   
   $_rilav_H_for_query = _rilav_H_for_query(
        $m_table_config,
        $cfg_mod['file_cfg_distinte'],
        $cfg_mod['file_tabelle'],
        'TA',
        'CVOUT',
        array('CV')
        );
    $sql_ar['select'][] = $_rilav_H_for_query['select'];
    $sql_ar['joins'][] = $_rilav_H_for_query['joins'];
    
$sql = "SELECT  " . implode(",", $sql_ar['select']) . "
        FROM {$cfg_mod['file_cfg_distinte']} TA
        " . implode(",", $sql_ar['joins']) . "
        WHERE CDDT = '{$id_ditta_default}' AND CDCMAS = 'START'
        {$sql_where}";


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


    $sql_f = "SELECT  " . implode(",", $sql_ar['select']) . "
              FROM {$cfg_mod['file_cfg_distinte']} TA
              " . implode(",", $sql_ar['joins']) . " 
              WHERE CDDT = '{$id_ditta_default}' AND CDCMAS = ?
              ORDER BY CDSLAV";
            
            $stmt_f = db2_prepare($conn, $sql_f);
            echo db2_stmt_errormsg();

$ar = array();
while ($row = db2_fetch_assoc($stmt)) {
    
   // $nr = array();
   // $nr['task'] = $row['CDCMAS'];
    
    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    $cod_liv0 = $row['CDCMAS']."_".$row['CDSLAV'];
    $cod_liv1 = $row['CDSLAV'];
   //START
    $liv =$cod_liv0;
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] = $row['CDCMAS'];
        //   $ar_new['task'] = "[#" . trim($row['ASPROG']) . "] " . trim($row['D_CJ']);
        //  $ar_new['imm'] =  print_date($row['DATA_PJ'])."-".print_ora($row['ORA_PJ']);
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    
    //GRUPPO
    $liv=$cod_liv1;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new = $row;
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] =  $row['CDSLAV'];
       
        $ar_r["{$liv}"] = $ar_new;
        
    }
    
    $ar_r=&$ar_r[$liv];
   
    
}//while

$row_indici = $s->get_TA_std('PPP', $form_values->f_prog);


echo "<div id='my_content'>"; 

echo "<div class=header_page>";
echo "<H2>Riepilogo programmi di produzione [{$form_values->f_prog}] {$row_indici['TADESC']}</H2>";
echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";
        
echo "<table class=int1>";
        
        
        echo "<tr class='liv_data'>
          <th>CV</th>
          <th>Seq.</th>
          <th>Gruppo</th>
          <th>Seq.</th>
          <th>Voce</th>
          <th>Descrizione voce</th>
          <th>Filtri</th>
          <th>TU</th>
          <th>Formato</th>
          <th>Parametri</th>
          <th>Mansione</th>
          <th>G.Doc.</th>
          <th>Attivit�</th>";
     
        echo  "</tr>";
        
        
    $nt_config_ope = array('file'     => $cfg_mod['file_note'],
        'NTTPNO'   => 'CDOPE');
    
    $nt_config_dis = array('file'     => $cfg_mod['file_note'],
        'NTTPNO'   => 'CDINF');
        
        
foreach($ar as $k => $v){
    $filtri = "";
    $row_filtri =  $main_module->get_TA_std('FLTCC', trim($v['CDCDLI']));
   if(trim($v['CDCDLI']) != '')
        $filtri = "[".trim($v['CDCDLI'])."] ".trim($row_filtri['TADESC']);
   
    $row_mansione =  $main_module->get_TA_std('PRGRO', trim($v['CDGRUP']));
    if(trim($v['CDGRUP']) != '')
        $mansione = "[".trim($v['CDGRUP'])."] ".trim($row_mansione['TADESC']);
   
    $descrizione = $v['CDDESC'];
    $chiave = $v['CDIDPR'];
  
    
    if(isset($form_values->f_nope) && strlen($form_values->f_nope) > 0){
        $nota_ope =  _ha_nota_nt_config($chiave, $nt_config_ope);
        $row_nt = _get_nota_nt_config($chiave, $nt_config_ope);
        $t_nota_ope = utf8_decode($row_nt['NTMEMO']);
        if($nota_ope == true)
         $descrizione .= "<br> Note operative: {$t_nota_ope}";
    }

    if(isset($form_values->f_ncfg) && strlen($form_values->f_ncfg) > 0){
        $nota_dis =  _ha_nota_nt_config($chiave, $nt_config_dis);
        $row_nt = _get_nota_nt_config($chiave, $nt_config_dis);
        $t_nota_dis = utf8_decode($row_nt['NTMEMO']);
        if($nota_dis == true)
            $descrizione .= "<br> Note configurazione: {$t_nota_dis}";
    }
    
    echo "<tr class=liv1>
          <td>{$v['H_CV_COD']}</td>
          <td>{$v['CDSEQI']}</td>
          <td>{$v['task']}</td>
          <td>{$v['CDSEQU']}</td>";
    echo "<td>{$v['CDSLAV']}</td>";
    echo "<td>{$descrizione}</td>";
    echo "<td>{$filtri}</td>";
    echo "<td>{$v['CDTPUS']}</td>";
    echo "<td>{$v['CDFOUS']}</td>";
    echo "<td>{$v['CDPAUS']}</td>";
    echo "<td>{$mansione}</td>";
    echo "<td>{$v['CDGEND']}</td>";
    echo "<td>{$v['CDTODO']}</td>";
    echo "</tr>";
    
    foreach($v['children'] as $k1 => $v1){
        
        
        $result = db2_execute($stmt_f, array($v1['task']));
        while($row_f = db2_fetch_assoc($stmt_f)){
            
            $filtri = "";
            $row_filtri =  $main_module->get_TA_std('FLTCC', $row_f['CDCDLI']);
            if(trim($row_f['CDCDLI']) != '')
                $filtri = "[".trim($row_f['CDCDLI'])."] ".trim($row_filtri['TADESC']);
            
            $row_mansione =  $main_module->get_TA_std('PRGRO', trim($row_f['CDGRUP']));
            if(trim($row_f['CDGRUP']) != '')
                $mansione = "[".trim($row_f['CDGRUP'])."] ".trim($row_mansione['TADESC']);
            
            $descrizione = $row_f['CDDESC'];
            $chiave = $row_f['CDIDPR'];
            
            
            if(isset($form_values->f_nope) && strlen($form_values->f_nope) > 0){
                $nota_ope =  _ha_nota_nt_config($chiave, $nt_config_ope);
                $row_nt = _get_nota_nt_config($chiave, $nt_config_ope);
                $t_nota_ope = utf8_decode($row_nt['NTMEMO']);
                if($nota_ope == true)
                     $descrizione .= "<br> Note operative: {$t_nota_ope}";
            }
            
            if(isset($form_values->f_ncfg) && strlen($form_values->f_ncfg) > 0){
                $nota_dis =  _ha_nota_nt_config($chiave, $nt_config_dis);
                $row_nt = _get_nota_nt_config($chiave, $nt_config_dis);
                $t_nota_dis = utf8_decode($row_nt['NTMEMO']);
                if($nota_dis == true)
                    $descrizione .= "<br> Note configurazione: {$t_nota_dis}";
            }
            
            echo "<tr>
            <td>{$row_f['H_CV_COD']}</td>
            <td>{$row_f['CDSEQI']}</td>
            <td>{$row_f['CDCMAS']}</td>
            <td>{$row_f['CDSEQU']}</td>";
            echo "<td>{$row_f['CDSLAV']}</td>";
            echo "<td>{$descrizione}</td>";
            echo "<td>{$filtri}</td>";
            echo "<td>{$row_f['CDTPUS']}</td>";
            echo "<td>{$row_f['CDFOUS']}</td>";
            echo "<td>{$row_f['CDPAUS']}</td>";
            echo "<td>{$mansione}</td>";
            echo "<td>{$row_f['CDGEND']}</td>";
            echo "<td>{$row_f['CDTODO']}</td>";
            echo "</tr>";
            
            
        }
        
        
        
    }
    
    
    
}
echo "</div>";

 		
?>

 </body>
</html>


<?php
exit;
}



