<?php


function _pp_order_dir($main_module, $k_ordine){
    $cfg_mod = $main_module->get_cfg_mod();
    $s = new Spedizioni(array('no_verify' => 'Y'));
    $oe = $s->k_ordine_td_decode_xx($k_ordine);
    $ordine_dir = implode('_', array_map('trim', $oe));
    $ordine_dir = '../' . $cfg_mod['dir_duplica'] . '/' . $ordine_dir;
    return $ordine_dir;
}

function _pp_order_dir_assoluto($main_module, $k_ordine){
    global $inc_path;
    $cfg_mod = $main_module->get_cfg_mod();
    $ordine_dir = _pp_order_dir($main_module, $k_ordine);
    $ordine_dir_assoluto = $inc_path . '/' . $ordine_dir;
    return $ordine_dir_assoluto;
}

function _pp_parametri_formato($string){
    $ret = array();
    
    $ar_split = explode(';', trim($string));
    foreach($ar_split as $p){
        $v_split = explode(':', $p);
        $ret[$v_split[0]] = $v_split[1];
    }
    
    return $ret;
}


function _pp_get_collo_vars($main_module, $rowPL, $ar_var_da_mostrare = null){
    global $conn;
    $cfg_mod = $main_module->get_cfg_mod();
    
    $sql_WHERE = "";
    $sql_WHERE.= sql_where_by_combo_value('VD.VDVAR01', $ar_var_da_mostrare);
    $order_by_case = "CASE VD.VDVAR01 ";
    
    foreach($ar_var_da_mostrare as $k => $v)
        $order_by_case .= " WHEN '{$v}' THEN {$k} ";
    $order_by_case .= " END";
   
    $sql = "SELECT VD.*, TA_VAR.TADESC AS D_VAR, TA_VAL.TADESC AS D_VAL 
            FROM {$cfg_mod['file_colli']} PL
            INNER JOIN {$cfg_mod['file_righe_doc_vd']} VD
                ON PLDT=VDDT AND PLTIDO=VDTIDO AND PLINUM=VDINUM AND PLAADO=VDAADO AND PLNRDO=VDNRDO AND PLREDO=VDNREC
            
            /* decodifica domanda */
            LEFT OUTER JOIN {$cfg_mod['file_tab_sys']} TA_VAR
                ON TA_VAR.TADT = PL.PLDT AND TA_VAR.TAID = 'PUVR' AND TA_VAR.TANR = VD.VDVAR01 AND TA_VAR.TALINV = ''
            /* decodifica risposta */
            LEFT OUTER JOIN {$cfg_mod['file_tab_sys']} TA_VAL
                ON TA_VAL.TADT = PL.PLDT AND TA_VAL.TAID = 'PUVN' AND TA_VAL.TACOR2 = VD.VDVAR01 AND TA_VAL.TANR = VD.VDVAL01 AND TA_VAL.TALINV = ''
            WHERE PLDT=? AND PLTIDO=? AND PLINUM=? AND PLAADO=? AND PLNRDO=? AND PLNREC=?
            {$sql_WHERE} ORDER BY {$order_by_case}";
    
       
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($rowPL['PLDT'], $rowPL['PLTIDO'], $rowPL['PLINUM'], $rowPL['PLAADO'], $rowPL['PLNRDO'], $rowPL['PLNREC']));
    echo db2_stmt_errormsg($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)){
        if (strlen(trim($row['D_VAR'])) > 0)
            $d_var = $row['D_VAR'];
        else
            $d_var = $row['VDVAR01'];
        if (strlen(trim($row['D_VAL'])) > 0)
            $d_val = $row['D_VAL'];
        else
            $d_val = $row['VDVAL01'];
            
        $ret[] = array(
                'VAR' => $row['VDVAR01'], 'VAL' => $row['VDVAL01'],
                'D_VAR' => rtrim($d_var), 'D_VAL' => rtrim($d_val)
        );
    }
    
    return $ret;
}



function _pp_get_collo_compo($main_module, $k_lista, $rowPL, $params = array()){
    global $conn, $cfg_mod_DeskArt;
    $cfg_mod = $main_module->get_cfg_mod();
        
    $compo_where = where_filtro_compo_by_k_lista($cfg_mod, $k_lista);
    
    //gruppo collo / Non gruppo collo
    $join_gruppo_no_gruppo = " AND CASE WHEN PLRCOL <> '' THEN REPROG ELSE RECOLC END = PLNCOL ";   
    
    $reart = "REART";
    if(isset($params['parametri_formato'])){
        if($params['parametri_formato']['COD'] == 'R'){
            $reart = "(CASE WHEN AR.ARARFO <> '' THEN AR.ARARFO ELSE RE.REART END)";
            $sql_join = " LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
                          ON AR.ARDT = RE.REDT AND AR.ARART = RE.REART   ";
        }
    }
    
    if ($params['group_by'] == 'ART'){
        $sql_select = "REUM, {$reart} AS REART, REDART, SUM(REQTA) AS REQTA, COUNT(*) AS C_ROW";
        $sql_group_by = "GROUP BY REUM, {$reart}, REDART";
        $sql_order_by = "ORDER BY {$reart}, REDART";
        
        //stacco anche per dimensioni, a meno che ricevo DIM:N
        if ($params['DIM'] != 'N') {
            $sql_select   .= " , REDIM1, REDIM2, REDIM3 ";
            $sql_group_by .= " , REDIM1, REDIM2, REDIM3 ";
            $sql_order_by .= " , REDIM1, REDIM2, REDIM3 ";
        }
        
    } else {
        $sql_select = "REUM, {$reart} AS REART, REDART, REQTA, REDIM1, REDIM2, REDIM3, REUM";
        $sql_group_by = '';
        $sql_order_by = '';
    }
    
    $sql = "SELECT {$sql_select}
            FROM {$cfg_mod['file_colli']} PL
            INNER JOIN {$cfg_mod['file_ricom']} RE
                ON PLDT=REDT AND PLTPCO=RETPCO AND PLAACO=REAACO AND PLNRCO=RENRCO AND PLTPSV=RETPSV
                {$join_gruppo_no_gruppo}
            {$sql_join}
            WHERE PLDT=? AND PLTIDO=? AND PLINUM=? AND PLAADO=? AND PLNRDO=? AND PLNREC=?
                {$compo_where}
                {$sql_group_by} {$sql_order_by}";   
          
         
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, array($rowPL['PLDT'], $rowPL['PLTIDO'], $rowPL['PLINUM'], $rowPL['PLAADO'], $rowPL['PLNRDO'], $rowPL['PLNREC']));
                echo db2_stmt_errormsg($stmt);
                
                $ret = array();
                while ($row = db2_fetch_assoc($stmt)) {
                    $row = array_map('rtrim', $row);    
                    $row['k_ordine'] = implode('_', array($rowPL['PLDT'], $rowPL['PLTIDO'], $rowPL['PLINUM'], $rowPL['PLAADO'], $rowPL['PLNRDO']));
                    $ret[] = $row;
                }
                
                return array('ar_compo' => $ret,
                    'compo_where' => $compo_where, 'sql' => $sql, 'sql_params' => array($rowPL['PLDT'], $rowPL['PLTIDO'], $rowPL['PLINUM'], $rowPL['PLAADO'], $rowPL['PLNRDO'], $rowPL['PLNREC']));
                
}



function _pp_get_colli_compo($main_module, $k_lista, $filter_params, $params = array()){
    global $conn, $cfg_mod_DeskArt;
    $cfg_mod = $main_module->get_cfg_mod();
    
    $compo_where = where_filtro_compo_by_k_lista($cfg_mod, $k_lista);
    
    //gruppo collo / Non gruppo collo
    $join_gruppo_no_gruppo = " AND CASE WHEN PLRCOL <> '' THEN REPROG ELSE RECOLC END = PLNCOL ";
    
    if ($params['group_by'] == 'ART'){
        
        $sql_select = "REART, REDART, SUM(REQTA) AS REQTA, COUNT(*) AS C_ROW, REUM, ARARFO, 
                       PLART, PLDART, PLDIM1, PLDIM2, PLDIM3";
        //$sql_group_by = "GROUP BY CASE WHEN REUM IN('NR', 'PZ') THEN REUM ELSE digits(RERIGA) END, REUM, REART, REDART, REDIM1, REDIM2, REDIM3, ARARFO";
        $sql_group_by = "GROUP BY REUM, REART, REDART, ARARFO, PLART, PLDART, PLDIM1, PLDIM2, PLDIM3";
        $sql_order_by = 'ORDER BY REART, REDART';
        
        if($params['riga'] == 'Y'){
            $sql_select .= " , RERIGA";
            $sql_group_by .= ", RERIGA";
        }
        
        //stacco anche per dimensioni, a meno che ricevo DIM:N
        if ($params['DIM'] != 'N') {
            $sql_select   .= " , REDIM1, REDIM2, REDIM3 ";
            $sql_group_by .= " , REDIM1, REDIM2, REDIM3 ";
            $sql_order_by .= " , REDIM1, REDIM2, REDIM3 ";
        }
        
    } else {
        $sql_select = 'REUM, REART, REDART, REQTA';
        $sql_group_by = '';
        $sql_order_by = '';
    }
    
    $sql_filtro_colli = ' AND 1 = 2 ';
    if (isset($filter_params['RRN_PL']))
        $sql_filtro_colli = " AND RRN(PL) IN (" . sql_t_IN($filter_params['RRN_PL']) . ")";
    
    $sql = "SELECT {$sql_select}
            FROM {$cfg_mod['file_colli']} PL
            INNER JOIN {$cfg_mod['file_ricom']} RE
                ON PLDT=REDT AND PLTPCO=RETPCO AND PLAACO=REAACO AND PLNRCO=RENRCO AND PLTPSV=RETPSV
                {$join_gruppo_no_gruppo}
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
            ON AR.ARDT = RE.REDT AND AR.ARART = RE.REART            
              WHERE 1= 1 {$sql_filtro_colli}
                {$compo_where}
                {$sql_group_by} {$sql_order_by}";
      
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($rowPL['PLDT'], $rowPL['PLTIDO'], $rowPL['PLINUM'], $rowPL['PLAADO'], $rowPL['PLNRDO'], $rowPL['PLNREC']));
    echo db2_stmt_errormsg($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $row = array_map('rtrim', $row);
        $ret[] = $row;
    }
    
    return array('ar_compo' => $ret,
        'compo_where' => $compo_where, 'sql' => $sql, 'sql_params' => array($rowPL['PLDT'], $rowPL['PLTIDO'], $rowPL['PLINUM'], $rowPL['PLAADO'], $rowPL['PLNRDO'], $rowPL['PLNREC']));
                
}




function _pp_get_collo_note($main_module, $rowPL, $params = array()){
    global $conn;
    $cfg_mod = $main_module->get_cfg_mod();
    
    if (trim($params['RLTPNO']) == '' || trim($params['RLRIFE2']) == '')
        return array('ar_note' => array());
        
    $sql_WHERE = "";
    $sql_WHERE.= sql_where_by_combo_value('RL.RLTPNO',  $params['RLTPNO']);
    $sql_WHERE.= sql_where_by_combo_value('RL.RLRIFE2', $params['RLRIFE2']);
        
   
    
    $sql = "SELECT RL.*
            FROM {$cfg_mod['file_colli']} PL
            INNER JOIN {$cfg_mod['file_note_gest']} RL
                ON PLDT=RLDT AND PLTIDO=RLTIDO AND PLINUM=RLINUM AND PLAADO=RLAADO AND PLNRDO=RLNRDO AND PLREDO=RLNREC
                {$join_gruppo_no_gruppo}
            WHERE PLDT=? AND PLTIDO=? AND PLINUM=? AND PLAADO=? AND PLNRDO=? AND PLNREC=?
                {$sql_WHERE}";
                
       
                
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, array($rowPL['PLDT'], $rowPL['PLTIDO'], $rowPL['PLINUM'], $rowPL['PLAADO'], $rowPL['PLNRDO'], $rowPL['PLNREC']));
            echo db2_stmt_errormsg($stmt);
            
           /* echo "<pre>";
            echo $sql;
            print_r(array($rowPL['PLDT'], $rowPL['PLTIDO'], $rowPL['PLINUM'], $rowPL['PLAADO'], $rowPL['PLNRDO'], $rowPL['PLNREC']));
            echo "</pre>"; */
            
            
            $ret = array();
            while ($row = db2_fetch_assoc($stmt)) {
                $row = array_map('rtrim', $row);
                
                $row['testo'] = $row['RLSWST'].$row['RLREST1'].$row['RLFIL1'];
                
                if ($params['solo_testo'] == 'Y')
                    $ret[] = $row['testo'];
                else 
                    $ret[] = $row;
            }
            
            return array('ar_note' => $ret);            
}



/* per filtro dei soli colli in base ai filtri componenti */
function _pp_inner_pl_on_filtro_compo($cfg_mod, $k_lista){
    $compo_where = where_filtro_compo_by_k_lista($cfg_mod, $k_lista);
    if (strlen($compo_where) > 0){
        $inner_join_filtro_compo = "
            INNER JOIN (
                  SELECT DISTINCT PL2.PLDT, PL2.PLTIDO, PL2.PLINUM, PL2.PLAADO, PL2.PLNRDO, PL2.PLNREC
                  FROM {$cfg_mod['file_colli']} PL2
                    INNER JOIN {$cfg_mod['file_ricom']} RE2
                  ON PL2.PLDT=RE2.REDT AND PL2.PLTPCO=RE2.RETPCO AND PL2.PLAACO=RE2.REAACO AND PL2.PLNRCO=RE2.RENRCO AND PL2.PLTPSV=RE2.RETPSV
                    AND CASE WHEN PL2.PLRCOL <> '' THEN RE2.REPROG ELSE RE2.RECOLC END = PL2.PLNCOL
                  WHERE 1=1 {$compo_where}
                ) PLC
              ON PL.PLDT = PLC.PLDT
             AND PL.PLTIDO = PLC.PLTIDO
             AND PL.PLINUM = PLC.PLINUM
             AND PL.PLAADO = PLC.PLAADO
             AND PL.PLNRDO = PLC.PLNRDO
             AND PL.PLNREC = PLC.PLNREC
            ";
    } else {
        $inner_join_filtro_compo = "";
    }
    return $inner_join_filtro_compo;
}



function _pp_get_colli($main_module, $m_params, $preview_params = null, $report = null){
    global $cfg_mod_Spedizioni, $cfg_mod_DeskArt, $conn, $id_ditta_default;
    
    $cfg_mod = $main_module->get_cfg_mod();
    
    //da k_progetto recupero tipo/anno/numero lotto e ditta
    $ar_lotto = explode('_', $m_params->open_request->k_progetto);
    $tipo_lotto = $ar_lotto[0];
    $anno_lotto = $ar_lotto[1];
    $nr_lotto   = $ar_lotto[2];
    $ditta_or   = $ar_lotto[3];
    
    //recupero record AS e k_lista
    $rowAS   = get_as_row_by_id($main_module, $m_params->open_request->id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    
    $rowCD   = get_cd_row_by_id($main_module, $rowAS['ASORD4']);
    
    $colli_where = where_filtro_colli_by_k_lista($cfg_mod, $k_lista);        
    $inner_join_filtro_compo = _pp_inner_pl_on_filtro_compo($cfg_mod, $k_lista);
    
    //Se e' specificato un ordinamento
    $parametri_formato = _pp_parametri_formato($rowCD['CDPAUS']);
    
    $sql_order_by = _pp_get_order_by($parametri_formato);
        
    //if (!is_null($preview_params)){
    if (!empty((array)$preview_params)){
        
        if (is_null($report) || is_null($report->paginazione_preview_per)){
            $limit  = $preview_params->limit;
            $offset = $preview_params->offset;
            
            if (isset($preview_params->page)){
                $limit = 1;
                $offset = $preview_params->page -1;
            }
        } else {
            //raggruppamento per entita'
            $where_paginazione = _pp_get_paginazione_where_colli($main_module, $m_params->open_request->k_progetto, $preview_params, $colli_where, $inner_join_filtro_compo, $sql_order_by, $report->paginazione_preview_per);
        }
        
    }
    
    if (is_null($offset)) $offset = 0;
    if (is_null($limit))  $limit = 9999999;
    
    
    $sql_join_on_PL0 = _pp_sql_join_on_PL0();
        
    $sql = "SELECT RRN(PL) AS RRN_PL,
               TDDOCU, TDCCON, TDDCON, TDAACA, TDNRCA, TDTPCA, TDVSRF, TDONDO, TDOADO, TDOTPD, TDNRLO, TDTPLO, TDAALO, TDSELO, TDDNAZ, TDSECA, TDAUX4, /* k logo */
               PL.PLART, PL.PLDART, PL.PLTPDO, PL.PLOPE1, PL.PLOPE2, PL.PLMACQ, PL.PLFORN, CF.CFRGS1 AS D_FORN,
               PL.PLDT, PL.PLTIDO, PL.PLINUM, PL.PLAADO, PL.PLNRDO, PL.PLNREC, /*chiave collo: lasciare sempre */
               PL.PLPNET, PL.PLPLOR, PL.PLRIGA, PL.PLVOLU, PL.PLBARC, PL.PLTABB, PL.PLDPAD, PL.PLCPAD, AR2.ARARFO AS C_RIF,
		       PL.PLUM, PL.PLQTA, PL.PLDTEN, PL.PLTMEN, PL.PLDTUS, PL.PLTMUS, PL.PLDTRM, PL.PLTMRM,
    		   PSDTSC, PSTMSC, PSRESU, PSCAUS, PSPALM, PSLATI, PSLONG,
    		   PL.PLRCOL, PL.PLNCOL, PL.PLTCOL, PL.PLCOAB, PL.PLDIM1, PL.PLDIM2, PL.PLDIM3, PL.PLCINT, AR.ARARFO
            FROM {$cfg_mod['file_colli']} PL
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF
              ON CF.CFDT = PL.PLDT AND CF.CFCD = PL.PLFORN
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
              ON AR.ARDT = PL.PLDT AND AR.ARART = PL.PLCPAD
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR2
              ON AR2.ARDT = PL.PLDT AND AR2.ARART = PL.PLART
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_colli_dc']} PS
			  ON PL.PLDT = PS.PSDT
			 AND PL.PLTIDO = PS.PSTIDO
			 AND PL.PLINUM = PS.PSINUM
			 AND PL.PLAADO = PS.PSAADO
			 AND PL.PLNRDO = PS.PSNRDO
			 AND PL.PLNREC = PS.PSNREC
			
            {$inner_join_filtro_compo}
  
            INNER JOIN {$cfg_mod['file_testate']} TD
            {$sql_join_on_PL0}
            WHERE TDTPLO = ? AND TDAALO = ? AND TDNRLO = ? /* AND TDDTOR = ? */
                {$colli_where} {$where_paginazione}
            {$sql_order_by}
            LIMIT {$limit} OFFSET $offset
            ";
              
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, array($tipo_lotto, $anno_lotto, $nr_lotto /*, $ditta_or*/));
            echo db2_stmt_errormsg($stmt);
            
            $ar = array();
            while ($row = db2_fetch_assoc($stmt)){
                
                $r = array_map('trim', $row);
                
                $r['k_carico'] = implode("_", array($r['TDAACA'], $r['TDNRCA'], $r['TDTPCA']));
                $r['k_carico_out'] = implode("_", array(substr($r['TDAACA'], 2, 2), $r['TDNRCA'], $r['TDTPCA']));
                $r['PLDART'] = acs_u8e($row['PLDART']);
                $r['k_ordine'] = acs_u8e($row['TDDOCU']);
                $r['k_ordine_out'] = implode("_", array(substr($r['PLAADO'], 2, 2), $r['PLNRDO'], $r['PLTPDO']));
                
                $r['groupfield']        = trim($row['PLOPE1'])."_".trim($row['PLOPE2']);
                $r['groupfield_header'] = "PLOPE1: " . trim($row['PLOPE1']).", PLOPE2: ".trim($row['PLOPE2']);
                $ar[] = $r;
            }
            return array('ar_colli' => $ar, 'colli_where' => $colli_where, 'sql' => $sql, 'sql_params' => array($tipo_lotto, $anno_lotto, $nr_lotto, $ditta_or));            
}


function _pp_count_colli($main_module, $k_progetto, $id_assoluto_lista){
    global $cfg_mod_Spedizioni, $conn, $id_ditta_default;
    
    $cfg_mod = $main_module->get_cfg_mod();
    
    //da k_progetto recupero tipo/anno/numero lotto e ditta
    $ar_lotto = explode('_', $k_progetto);
    $tipo_lotto = $ar_lotto[0];
    $anno_lotto = $ar_lotto[1];
    $nr_lotto   = $ar_lotto[2];
    
    //recupero record AS e k_lista
    $rowAS   = get_as_row_by_id($main_module, $id_assoluto_lista);
    $k_lista = get_k_lista_from_as($main_module, $rowAS);
    
    $colli_where = where_filtro_colli_by_k_lista($cfg_mod, $k_lista);
    $inner_join_filtro_compo = _pp_inner_pl_on_filtro_compo($cfg_mod, $k_lista);
    
    $sql_join_on_PL0 = _pp_sql_join_on_PL0();
        
    $sql = "SELECT count(*) AS C_ROW
        FROM {$cfg_mod['file_colli']} PL
        {$inner_join_filtro_compo}	  

        INNER JOIN {$cfg_mod['file_testate']} TD
        {$sql_join_on_PL0}
            WHERE TDTPLO = ? AND TDAALO = ? AND TDNRLO = ? /* AND TDDTOR = ? */
            {$colli_where}
        ";
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($tipo_lotto, $anno_lotto, $nr_lotto /*, $ditta_or*/));
    echo db2_stmt_errormsg($stmt);
    
    $row = db2_fetch_assoc($stmt);
    return $row['C_ROW'];
}



function _pp_get_paginazione_where_colli($main_module, $k_progetto, $preview_params, $colli_where, $inner_join_filtro_compo, $sql_order_by, $paginazione_preview_per){
    global $cfg_mod_Spedizioni, $conn, $id_ditta_default;
    
    $cfg_mod = $main_module->get_cfg_mod();
    
    //da k_progetto recupero tipo/anno/numero lotto e ditta
    $ar_lotto = explode('_', $k_progetto);
    $tipo_lotto = $ar_lotto[0];
    $anno_lotto = $ar_lotto[1];
    $nr_lotto   = $ar_lotto[2];
    
    $limit  = $preview_params->limit;
    $offset = $preview_params->offset;
    
    if (isset($preview_params->page)){
        $limit = 1;
        $offset = $preview_params->page -1;
    }
       
    $sql_join_on_PL0 = _pp_sql_join_on_PL0();
        
    $sql = "SELECT DISTINCT(" . implode(',', $paginazione_preview_per) . ")
        FROM {$cfg_mod['file_colli']} PL
        {$inner_join_filtro_compo}
        INNER JOIN {$cfg_mod['file_testate']} TD
        {$sql_join_on_PL0}
            WHERE TDTPLO = ? AND TDAALO = ? AND TDNRLO = ? /* AND TDDTOR = ? */
            {$colli_where}
        LIMIT {$limit} OFFSET $offset
        ";
                        
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($tipo_lotto, $anno_lotto, $nr_lotto /*, $ditta_or*/));
    echo db2_stmt_errormsg($stmt);
    
    $row = db2_fetch_assoc($stmt);
    
    $ret = '';
    foreach($paginazione_preview_per as $kf){
        $ret .= " AND {$kf} = '{$row[$kf]}'";
    }
    return $ret;
}





function _pp_get_count_paginazione($main_module, $k_progetto, $colli_where, $inner_join_filtro_compo, $sql_order_by, $paginazione_preview_per){
    global $cfg_mod_Spedizioni, $conn, $id_ditta_default;
    
    $cfg_mod = $main_module->get_cfg_mod();
    
    //da k_progetto recupero tipo/anno/numero lotto e ditta
    $ar_lotto = explode('_', $k_progetto);
    $tipo_lotto = $ar_lotto[0];
    $anno_lotto = $ar_lotto[1];
    $nr_lotto   = $ar_lotto[2];
    
    $sql_join_on_PL0 = _pp_sql_join_on_PL0();
        
    if (is_array($paginazione_preview_per)){
      $sql = "SELECT COUNT(DISTINCT(" . implode(',', $paginazione_preview_per) . ")) AS C_ROW
        FROM {$cfg_mod['file_colli']} PL
        
        INNER JOIN {$cfg_mod['file_testate']} TD
        {$sql_join_on_PL0}
            WHERE TDTPLO = ? AND TDAALO = ? AND TDNRLO = ? /* AND TDDTOR = ? */
            {$colli_where}   
        ";
    } else {
      $sql = "SELECT COUNT(*) AS C_ROW
        FROM {$cfg_mod['file_colli']} PL
        {$inner_join_filtro_compo}
        INNER JOIN {$cfg_mod['file_testate']} TD
        {$sql_join_on_PL0}
            WHERE TDTPLO = ? AND TDAALO = ? AND TDNRLO = ? /* AND TDDTOR = ? */
            {$colli_where}
        ";
    }     
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($tipo_lotto, $anno_lotto, $nr_lotto /*, $ditta_or*/));
    echo db2_stmt_errormsg($stmt);    
    $row = db2_fetch_assoc($stmt);
    return $row['C_ROW'];
}




function _pp_get_order_by($parametri_formato){
     
    $ar_order = explode('/', $parametri_formato['ORD']);
    
    $ar_field = array();
    foreach ($ar_order as $type_ord){
        switch ($type_ord) {
            case 'AR':  $ar_field[] = 'PLART'; break;
            case 'SP':  $ar_field[] = 'TDSELO'; break;   //Sequenza di produzione?
            case ''  : break; //TDSELO, TDOADO, TDONDO, PLNCOL
            default:
                die('ORD non conosciuto: ' . $type_ord);
            break;
        }
    }
    
    if (count($ar_field) > 0)
        return " ORDER BY " . implode(',', $ar_field);
    return "";
}


function _pp_sql_join_on_PL0(){
    global $cfg_mod_Spedizioni;
    $sql_join_on_PL0 = "ON PL.PLDT=TDDT AND PL.PLTIDO=TDOTID AND PL.PLINUM=TDOINU AND PL.PLAADO=TDOADO AND digits(PL.PLNRDO)=TDONDO";
    if ($cfg_mod_Spedizioni['disabilita_digits_on_PL0'] == 'Y'){
        $sql_join_on_PL0 = "ON PL.PLDT=TDDT AND PL.PLTIDO=TDOTID AND PL.PLINUM=TDOINU AND PL.PLAADO=TDOADO AND PL.PLNRDO=TDONDO";
    }
    return $sql_join_on_PL0;
}
