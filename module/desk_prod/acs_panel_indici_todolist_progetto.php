<?php

require_once("../../config.inc.php");
require_once("acs_panel_prog_prod_include.php");
require_once("../base/_rilav_g_history_include.php");
require_once 'acs_panel_indici_include.php';
require_once 'acs_panel_indici_naviga_include.php';
require_once 'acs_panel_indici_config_distinta.php';

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));
$desk_art = new DeskArt(array('no_verify' => 'Y'));

$cfg_mod = $main_module->get_cfg_mod();

$m_params = acs_m_params_json_decode();




//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//---------------------------------------------------------------------
    ini_set('max_execution_time', 300);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    $as_where = "";
   
    if (isset($form_values->f_rilascio_da)  && strlen($form_values->f_rilascio_da) > 0 )
        $as_where .= " AND TA_PJ.TADTGE >= {$form_values->f_rilascio_da} ";
    if (isset($form_values->f_data_a)  && strlen($form_values->f_rilascio_a) > 0 )
        $as_where .= " AND TA_PJ.TADTGE <= {$form_values->f_rilascio_a} ";
    
    if (isset($m_params->open_request->data)  && strlen($m_params->open_request->data) > 0 )
        $as_where .= " AND TA_PJ.TADTGE = {$m_params->open_request->data} ";
            
        $as_where .= sql_where_by_combo_value('TA_PJ.TAMAIL', $form_values->f_programma, 'LIKE');
 
    $_rilav_H_for_query = _rilav_H_for_query(
        array('f_ditta' => 'ASDT', 'field_IDPR' => 'ASIDPR'),   //m_table_config
        $cfg_mod['file_cfg_distinte'],
        $cfg_mod['file_tabelle'],
        'ATT_OPEN',
        'CVOUT',
        array('CV')
        );
    
    
    if (isset($m_params->open_request->mansione)  && strlen($m_params->open_request->mansione) > 0 ){
        $cd_join = " INNER JOIN {$cfg_mod['file_cfg_distinte']} CD
                      ON ATT_OPEN.ASDT = CD.CDDT AND ATT_OPEN.ASORD4  <> '' AND ATT_OPEN.ASORD4 = CD.CDIDPR 
                      ";
        
        $cd_where = " AND CD.CDGRUP = '{$m_params->open_request->mansione}'";
        
    }
        

            
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.TADESC AS D_AT, TA_PJ.TADTGE AS DATA_PJ,
            TA_PJ.TAMAIL AS D_CJ, TA_PJ.TADESC AS D_PJ, 
            /*NT_MEMO.NTMEMO AS MEMO, */
            NT_CD.NTKEY2 AS CODICE,
            NT_CD.NTMEMO AS DESC, TA_ALLEGATI.NR_AL AS NR_AL, /*PS.PSDTPG, PS.PSPRIO,*/
            TA_PJ.TADTGE AS DATA_PJ, TA_PJ.TAORGE AS ORA_PJ, TA_PJ.TAUSGE AS US_PJ,
            TA_IND.TADESC AS D_IND,  {$_rilav_H_for_query['select']}

            FROM {$cfg_mod['file_assegna_ord']} ATT_OPEN

            INNER JOIN {$cfg_mod['file_tabelle']} TA_ATTAV
                ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1

            /* decod. indice */
            LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_IND
                ON ATT_OPEN.ASDT = TA_IND.TADT AND ATT_OPEN.ASORD1 = TA_IND.TAKEY1 AND TA_IND.TATAID = '{$cfg_mod['taid_indici']}'


            LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_PJ
                ON ATT_OPEN.ASDT = TA_PJ.TADT AND ATT_OPEN.ASPROG = TA_PJ.TAKEY1 AND TA_PJ.TATAID = '{$cfg_mod['taid_progetto']}'

           /* LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
	            ON NT_MEMO.NTTPNO = 'ASME5' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0*/
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_CD
	            ON NT_CD.NTTPNO = 'PRJC5' AND INTEGER(NT_CD.NTKEY1) = ATT_OPEN.ASIDPR AND NT_CD.NTSEQU=0
            LEFT OUTER JOIN (
                SELECT COUNT(*) AS NR_AL, TADT, TATAID, TAKEY1
                FROM {$cfg_mod['file_tabelle']}
                GROUP BY TAKEY1, TADT, TATAID) TA_ALLEGATI
            ON TA_ALLEGATI.TADT = ATT_OPEN.ASDT AND TA_ALLEGATI.TATAID = 'TDPAL' AND TA_ALLEGATI.TAKEY1 = ATT_OPEN.ASPROG
           /* LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS
             ON ATT_OPEN.ASDT = PS.PSDT AND PS.PSTAID = 'LOTT' 
                AND PSTPCA = SUBSTRING(ATT_OPEN.ASDOCU, 1, 2)
                AND PSAACA = SUBSTRING(ATT_OPEN.ASDOCU, 4, 4) 
                AND PSNRCA = SUBSTRING(ATT_OPEN.ASDOCU, 9, 6) */
            {$cd_join}
            {$_rilav_H_for_query['joins']}
            WHERE ASDT = '{$id_ditta_default}' AND ASPROG <> '' {$as_where} {$cd_where}
            ORDER BY ASIDPR";
   
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            echo db2_stmt_errormsg();
             
           /* $ps_where = "";
            if (isset($form_values->f_evasione_da)  && strlen($form_values->f_evasione_da) > 0 )
              $ps_where .= " AND PSDTPG >= {$form_values->f_evasione_da} ";
            if (isset($form_values->f_evasione_a)  && strlen($form_values->f_evasione_a) > 0 )
              $ps_where .= " AND PSDTPG <= {$form_values->f_evasione_a} ";
            */ 
            
            $sql_ps = "SELECT * 
                    FROM {$cfg_mod_Spedizioni['file_carichi']} PS
					WHERE PSDT = '{$id_ditta_default}' AND PSTAID = 'LOTT' AND PSTPCA = ? AND PSAACA = ? AND PSNRCA = ?
                    {$ps_where}";
            
            $stmt_ps = db2_prepare($conn, $sql_ps);
            echo db2_stmt_errormsg();
            
            $ar = array();
            while ($row = db2_fetch_assoc($stmt)) {
                
                //stacco dei livelli
                $cod_liv0 = trim($row['ASPROG']);   //progetto                            
                $cod_liv1 = trim($row['ASORD2']);   //kiosk
                $cod_liv2 = $row['ASIDPR'];         //numeratore assoluto attivita'
                
                
                $tmp_ar_id = array();
                $ar_r= &$ar;
                
                //PROGETTO
                $liv =$cod_liv0;
                $tmp_ar_id[] = $liv;
                if (!isset($ar_r["{$liv}"])){
                    $ar_new = $row;
                    $ar_new['children'] = array();
                    $ar_new['id'] = implode("|", $tmp_ar_id);
                    $ar_lotto = explode("_", $row['ASDOCU']);
                    
                    $result = db2_execute($stmt_ps, array($ar_lotto[0], $ar_lotto[1], $ar_lotto[2]));
                    $row_ps = db2_fetch_assoc($stmt_ps);
                    $ar_new['data_eva'] = $row['PSDTPG'];
                    
                    
                    $row_priol = $s->get_TA_std('PRIOL', trim($row['PSPRIO']));
                    $ar_new['seq_prio'] = $row_priol['TASITI'];
                    
                    $ar_new['task'] = "[#" . trim($row['ASPROG'])."] " . $row['D_CJ'];
                    $ar_new['imm'] =  print_date($row['DATA_PJ'])."-".print_ora($row['ORA_PJ']);
                    
                    
                    $ar_new['allegati'] = $row['NR_AL'];
                    $ar_new['ut_ass'] = "[".trim($row['US_PJ'])."]";
                    $ar_new['liv'] = 'liv_1';
                    $ar_new['liv_c'] = $liv;
                    $ar_new['liv_type'] = 'PROGETTO';
                    
                    
                    $ar_new['indice'] = trim($row['ASORD1']);
                    
                    $ar_new['desc'] .=  utf8_encode($row['D_IND']);
                    $ar_new['desc'] .=   '<br>' . $row['D_PJ'];
                    
                    $ar_r["{$liv}"] = $ar_new;
                }
                $ar_r = &$ar_r["{$liv}"];
                
                //Kiosk
                $liv=$cod_liv1;
                $ar_r = &$ar_r['children'];
                $tmp_ar_id[] = $liv;
                if(!isset($ar_r[$liv])){
                    $ar_new = $row;
                    $ar_new['children'] = array();
                    $ar_new['id'] = implode("|", $tmp_ar_id);
                    $ar_new['task'] = $row['ASORD2'];
                    $ar_new['desc'] =  decod_kiosk_from_indice($cfg_mod, $row['ASORD1'], $row['ASORD2']);
                    //$ar_new['causale'] =  $row['ASCAAS'];

                    $ar_new['liv'] = 'liv_2';
                    $ar_new['liv_c'] = $liv;
                    $ar_new['liv_type'] = 'KIOSK';
                    
                    $ar_new['leaf'] = false;
                    $t_ar_liv1 = &$ar_r[$liv];
                    
                    
                    //recupero CV in base a indice/kiosk
                    $row_kiosk = get_row_kiosk_from_indice($cfg_mod, $row['ASORD1'], $row['ASORD2']);
                    if ($row_kiosk){
                        $_rilav_H_for_query_CD_kiosk = _rilav_H_for_query(
                            array('f_ditta' => 'CDDT', 'field_IDPR' => 'CDIDPR'),   //m_table_config
                            $cfg_mod['file_cfg_distinte'],
                            $cfg_mod['file_tabelle'],
                            'CD',
                            'CVOUT',
                            array('CV')
                            );
                        
                        
                        $sql = "SELECT {$_rilav_H_for_query_CD_kiosk['select']}
                                FROM {$cfg_mod['file_cfg_distinte']} CD
                                {$_rilav_H_for_query_CD_kiosk['joins']}
                                WHERE CDDT = '{$id_ditta_default}' AND CDIDPR = {$row_kiosk['CDIDPR']}";
                          
                                $stmtCDKiosk = db2_prepare($conn, $sql);
                                echo db2_stmt_errormsg();
                                $result = db2_execute($stmtCDKiosk);
                                echo db2_stmt_errormsg();
                                
                                $rowCDKiosk = db2_fetch_assoc($stmtCDKiosk);
                                
                                if ($rowCDKiosk){
                                    $ar_new['H_CV_COD'] = $rowCDKiosk['H_CV_COD'];
                                    $ar_new['H_CV_ICONA'] = $rowCDKiosk['H_CV_ICONA'];
                                    $ar_new['H_CV_STYLE_CLASS'] = $rowCDKiosk['H_CV_STYLE_CLASS'];
                                } else {
                                    $ar_new['H_CV_COD'] = '';
                                    $ar_new['H_CV_ICONA'] = '';
                                    $ar_new['H_CV_STYLE_CLASS'] = '';
                                }
                    } //if $row_kiosk
                    
                    
                    
                    $ar_r["{$liv}"] = $ar_new;
                }
                
                $ar_r = &$ar_r["{$liv}"];
                //sottolivello
                //se cod_liv2 is not null creo sotto livello
                //e imposto a false il leaf di liv1
                if(!is_null($cod_liv2)){
                    
                    $liv=$cod_liv2;
                    $ar_r = &$ar_r['children'];
                    $tmp_ar_id[] = $liv;
                    if(!isset($ar_r[$liv])){
                        $ar_new = $row;
                        $ar_new['id'] = implode("|", $tmp_ar_id);
                        $ar_new['task'] =  $row['CODICE'];
                        $ar_new['desc'] =  trim($row['DESC']);
                        $ar_new['causale'] =  $row['ASCAAS'];
                        $ar_new['r_note'] = utf8_decode($row['ASNORI']);
                        
                        if(trim($row['ASFLRI']) == 'Y'){
                            $causali_rilascio = $s->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                            $ar_new['d_rilav'] = $causali_rilascio[0]['text'];
                            $ar_new['t_rilav'] = "<b>{$row['ASCARI']}</b>";
                            $ar_new['t_rilav'] .= "<br>" .trim($row['ASNORI']);
                        }
                        $ar_new['prog'] =  $row['ASIDPR'];
                        $ar_new['prog_coll'] =  $row['ASIDAC'];
                        $ar_new['note'] =  $desk_art->has_nota_progetto($row['ASIDPR']);
                        $row_nt =  $desk_art->get_note_progetto($row['ASIDPR']);
                        $ar_new['t_note'] =  utf8_decode($row_nt['NTMEMO']);
                        $ar_new['imm'] = print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
                        $ar_new['ut_ass'] = trim($row['ASUSAT']);
                        $ar_new['ut_ins'] = trim($row['ASUSAS']);
                        $ar_new['scadenza'] = trim($row['ASDTSC']);
                        $ar_new['memo'] = utf8_decode(trim($row['MEMO']));
                        $ar_new['riferimento'] = trim($row['ASNOTE']);
                        $ar_new['f_ril'] =  trim($row['ASFLNR']);
                        $ar_new['flag'] =  trim($row['ASFLRI']);
                        $ar_new['liv'] = 'liv_2';
                        $ar_new['leaf'] = true;
                        
                        //AL MOMENTO MI FERMO A LIVELLO DI KIOSK
                        $t_ar_liv1['leaf'] = true;
                        
                        //$t_ar_liv1['leaf'] = false;
                        //$ar_r["{$liv}"] = $ar_new;
                    }
                    
                }
                
            }
            
            
            
            foreach($ar as $kar => $r){
                $ret[] = array_values_recursive($ar[$kar]);
            }
            
            echo acs_je(array('success' => true, 'children' => $ret));
            exit;
}



//----------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_panel'){
//----------------------------------------------------------------

    if(isset($m_params->mansione)){
       $row_mans = $s->find_TA_std('PRGRO', $m_params->mansione);
       $descrizione = trim($row_mans[0]['text']);
       $title = "Working list";
       $tbar = "Programmi attivit� logistico/produttive per mansione [{$m_params->mansione}] {$descrizione}";
    }else{
       $title = "Program list";
       $tbar = "Programmi attivit&agrave; logistico/produttive e centri di rilascio";
    }
    
    ?>

{"success":true, "items": [

     {
        xtype: 'treepanel' ,
        multiSelect: true,
        cls: 's_giallo',
        title: <?php echo j($title); ?> ,
	    tbar: new Ext.Toolbar({
	            items:['<b><?php echo j($tbar); ?></b>', '->',
	            
					{iconCls: 'icon-gear-16',
    	             text: 'CAUSALI', 
		           		handler: function(event, toolEl, panel){
			           		acs_show_panel_std(
			           			'../desk_vend/acs_gestione_attav.php?fn=open_grid',
			           			null, {sezione: <?php echo acs_je($cfg_mod['ATTAV_TAKEY2']) ?>});			          
		           		 }
		           	 }	            
	            
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,         
		    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['r_note', 'prog_coll', 'causale', 'id', 'task', 'desc', 'segnalazioni',
                             'liv', 'liv_c', 'liv_type', 'flag', 'prog', 'imm', 'ut_ins', 'ut_ass', 
                             'scadenza', 'riferimento', 'memo', 'f_ril', 'd_rilav', 't_rilav', 'note', 
                             't_note', 'allegati', 'indice', 'indice_out', 'data_eva', 'seq_prio',
                             <?php echo _rilav_H_write_model_fields(array('CV')) ?>],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						extraParams: {
						  open_request: <?php echo acs_je($m_params); ?>,
	                      form_values: <?php echo acs_je($m_params->form_values); ?>
	                  
                      }
                    , doRequest: personalizza_extraParams_to_jsonData  ,     
						reader: {
                            root: 'children'
                        }        				
                    }

                }),
                
                <?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
    	    			
            columns: [ {xtype: 'treecolumn', 
        	    		text: 'ID Program/Kiosk', 	
        	    		width: 250,
        	    		dataIndex: 'task',
        	    		 renderer: function(value, p, record){
						   	if(record.get('flag') == 'Y')
    						       p.tdCls += ' barrato';
    					    return value;
    		    	    }
        	    		},
        	    		
        	    		
        	    		{text: 'CV',					
    		          		dataIndex: 'H_CV_COD', tooltip: 'Ciclo vita', width: 35, align: 'right',
    		          		exeGestRilavH: 'CV',
    		          		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          		  if (rec.get('liv_type') == 'KIOSK'){	
    		          			<?php echo _rilav_H_icon_style('CV') ?>
    		          			}
        	        	}},
        	        	
        	        	
                        {
        	    			text: 'Descrizione',
        	    			dataIndex: 'desc',
        	    			flex: 1
        	    		},
        	    			{
        	    			text: 'Evasione programmata',
        	    			dataIndex: 'data_eva',
        	    		    width: 120,
        	    		    renderer : date_from_AS
        	    		},
        	    			{
        	    			text: 'Seq.',
        	    			dataIndex: 'seq_prio',
        	    			width: 40
        	    		}

        	    	 ,	{text: 'Data rilascio', width: 140, dataIndex: 'imm'}
        	    			
        	         , {text: '<img src=<?php echo img_path("icone/48x48/button_blue_play.png") ?> width=20>',					
    		          		dataIndex: 'H_INI_COD', tooltip: 'Iniziata', width: 35, align: 'right', 
    		          		exeGestRilavH: 'INI',
    		          		}
        	        	
        	        , {text: '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=20>',					
    		          		dataIndex: 'H_SOS_COD', tooltip: 'Sospeso', width: 35, align: 'right', 
    		          		exeChangeRilavH: 'SOS',
    		          		}
        	        	
        	        , {text: '<img src=<?php echo img_path("icone/48x48/check_green.png") ?> width=20>',					
    		          		dataIndex: 'ASFG03', tooltip: 'Sospeso', width: 35, align: 'right', 
    		          		exeChangeFlag: 'Y',
    		          		}
    		          		
          			, {text: '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=20>',					
    		          		dataIndex: 'ha_note', tooltip: 'Segnalazioni', width: 35, align: 'right', 
    		          		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          		  if (rec.get('liv_type') == 'LISTA'){	
    		          			
							  }
        	        	}}
        	        , {text: 'Fine attivit&agrave;', width: 140, dataIndex: ''}
	 				, { //tasto dx on mobile
    		            xtype:'actioncolumn', width:30,
    		            text: '<img src=<?php echo img_path("icone/24x24/button_grey_play.png") ?> width=20>',
    		            tdCls: 'tdAction', tooltip: 'Opzioni disponibili',
    		            menuDisabled: true, sortable: false,
    		            
    		            items: [{
    		                icon: <?php echo img_path("icone/24x24/button_grey_play.png") ?>,
    		                handler: function(view, rowIndex, colIndex, item, e, record, row) {
    		                	if (view.getSelectionModel().getSelection().length == 0)
    		                		view.getSelectionModel().select([record]);
    		                	view.fireEvent('itemcontextmenu', view, record, null, colIndex, e);		                	
    		                }
    		            }]
    		          }
	 					
        	    		
    	    ],
    	    
    	    listeners: {
    	     
    	        beforeload: function(store, options) {
                    Ext.getBody().mask('Loading... ', 'loading').show();
                    },

                load: function () {
                  Ext.getBody().unmask();
                }, 
    	    itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
				var voci_menu = [];
				
				
				if (rec.get('liv_type') == 'PROGETTO'){
    		 	 voci_menu.push({
	         		text: 'Apri',
	        		iconCls : 'icon-leaf-16',          		
	        		handler: function () {
	        		      acs_show_panel_std('acs_panel_indici_todolist_progetto_progetto.php?fn=open_panel', null, 
	        		      			{progetto : rec.get('liv_c'), mansione : <?php echo j($m_params->mansione)?>});              
	        		 }
	    		 });
	    		 
	    		 	 voci_menu.push({
	         		text: 'Report',
	        		iconCls: 'icon-print-16',
	        		handler: function() {
    				    			window.open('acs_panel_indici_todolist_progetto_report.php?fn=open_report&progetto=' + rec.get('liv_c'));
    							}
	        		
	    		 });
	    		 
	    		 }
	    		 
	    		if (rec.get('liv_type') == 'KIOSK'){
    		 	 voci_menu.push({
	         		text: 'Apri',
	        		iconCls : 'icon-leaf-16',          		
	        		handler: function () {
	        		      acs_show_panel_std('acs_panel_indici_todolist_progetto_progetto.php?fn=open_panel', null, 
	        		      			{progetto : rec.parentNode.get('liv_c'), kiosk: rec.get('liv_c'),  mansione : <?php echo j($m_params->mansione)?>});              
	        		 }
	    		 });
	    		 
	    		 	 voci_menu.push({
	         		text: 'Report',
	        		iconCls: 'icon-print-16',
	        		handler: function() {
    				    			window.open('acs_panel_indici_todolist_progetto_report.php?fn=open_report&progetto=' + rec.parentNode.get('liv_c') + '&kiosk='+ rec.get('liv_c'));
    							}
	        		
	    		 });
	    		 
	    		 }
	    		 
	    		 
	    	
	    		 

	    		   var menu = new Ext.menu.Menu({
		            items: voci_menu
			        }).showAt(event.xy);	
				  
			  } //itemcontextmenu
    	    }, //listeners
				 
			viewConfig: {
				 toggleOnDblClick: false,
		         getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
 		           return v;																
		         }   
		    }	
    	    
    	}	//tree
	 	
	]  	
  }


<?php 
exit;
}


// ******************************************************************************************
// FORM PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_form_params'){
    ?>
{"success":true, 
	m_win: {
		title: 'Programmi attivit� logistico/produttive',
		width: 600, height: 250,
		iconCls: 'icon-module-16'
	},
	"items": [
      {
		            xtype: 'form',
		            flex: 1,
		            anchor: '-15',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
		            frame: true,
		            items: [		            
						  {
							name: 'f_programma',
							anchor: '-15',
							xtype: 'textfield',
							fieldLabel: 'Programma',
							 
						 }	
							
						 , {
					        xtype: 'fieldcontainer',
				 	        layout: 'hbox',
				 	        items: [				 	       
					 	        <?php echo_je(a15(n_o(extjs_datefield('f_evasione_da', 'Da evasione')))); ?>	
					 	      , <?php echo_je(a15(n_o(extjs_datefield('f_evasione_a', 'A evasione', null, array('labelAlign' => 'right'))))); ?>		            		            		     	            		            		            
							]
						}	
						 , {
					        xtype: 'fieldcontainer',
				 	        layout: 'hbox',
				 	        items: [				 	       
					 	        <?php echo_je(a15(n_o(extjs_datefield('f_rilascio_da', 'Da rilascio')))); ?>	
					 	      , <?php echo_je(a15(n_o(extjs_datefield('f_rilascio_a', 'A rilascio', null, array('labelAlign' => 'right'))))); ?>		            		            		     	            		            		            
							]
						}							
							
					],
					
					buttons: [
				  {
			            text: 'Operatore',
			            iconCls: 'icon-user_only-32', scale: 'large',
			            handler: function() {
			            
			            	var form = this.up('form').getForm(),
			            		m_win = this.up('window'),
			            		form_values = form.getValues();
			            	 
			            	if(form.isValid()){
					        	acs_show_win_std(null, 'acs_panel_indici_todolist_progetto_operatore.php?fn=open_grid');
								m_win.destroy();
			            	} //isValid
			            }
			         }, '->',
	         		{
			            text: 'Visualizza per giorno',
			            iconCls: 'icon-folder_search-32', scale: 'large',
			            handler: function() {
			            
			            	var form = this.up('form').getForm(),
			            		m_win = this.up('window'),
			            		form_values = form.getValues();
			            	 
			            	if(form.isValid()){
					        	acs_show_win_std(null, '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_day', {
        		            			form_values: form_values}, 300, 300, {}, 'icon-calendar-16');
								m_win.destroy();
			            	} //isValid
			            }
			         },
					{
			            text: 'Visualizza programma',
			            iconCls: 'icon-folder_search-32', scale: 'large',
			            handler: function() {
			            
			            	var form = this.up('form').getForm(),
			            		m_win = this.up('window'),
			            		form_values = form.getValues();
			            	 
			            	if(form.isValid()){
					        	acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_panel', null, {
        		            			form_values: form_values});
								m_win.destroy();
			            	} //isValid
			            }
			         }
			       ]
		         }	//form		  
		
	]
}		
<?php exit; }



//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data_day'){
    //---------------------------------------------------------------------
    ini_set('max_execution_time', 300);
    $ar = array();
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    $as_where = "";
    
    if (isset($form_values->f_rilascio_da)  && strlen($form_values->f_rilascio_da) > 0 )
        $as_where .= " AND TA_PJ.TADTGE >= {$form_values->f_rilascio_da} ";
    if (isset($form_values->f_data_a)  && strlen($form_values->f_rilascio_a) > 0 )
        $as_where .= " AND TA_PJ.TADTGE <= {$form_values->f_rilascio_a} ";
            
    $as_where .= sql_where_by_combo_value('TA_PJ.TAMAIL', $form_values->f_programma, 'LIKE');
            
    $_rilav_H_for_query = _rilav_H_for_query(
        array('f_ditta' => 'ASDT', 'field_IDPR' => 'ASIDPR'),   //m_table_config
        $cfg_mod['file_cfg_distinte'],
        $cfg_mod['file_tabelle'],
        'ATT_OPEN',
        'CVOUT',
        array('CV')
        );
            
            
    $sql = "SELECT TA_PJ.TADTGE AS DATA_PJ
           /* , {$_rilav_H_for_query['select']}*/
            FROM {$cfg_mod['file_assegna_ord']} ATT_OPEN
            INNER JOIN {$cfg_mod['file_tabelle']} TA_ATTAV
            ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1
            LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_PJ
            ON ATT_OPEN.ASDT = TA_PJ.TADT AND ATT_OPEN.ASPROG = TA_PJ.TAKEY1 AND TA_PJ.TATAID = '{$cfg_mod['taid_progetto']}'
            {$_rilav_H_for_query['joins']}
            WHERE ASDT = '{$id_ditta_default}' AND ASPROG <> '' AND TA_PJ.TADTGE > 0 
            {$as_where} 
            GROUP BY TA_PJ.TADTGE
            /*, {$_rilav_H_for_query['group_by']} */
            ORDER BY TA_PJ.TADTGE DESC
            ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
    
     $ar = array();
     while ($row = db2_fetch_assoc($stmt)) {
         
         $nr = array();
         $nr = $row; 
         $nr['imm'] =  $row['DATA_PJ'];
         $ar[] = $nr;
         
         
     }
         
     echo acs_je($ar);
     exit;
}

if ($_REQUEST['fn'] == 'open_day'){
    ?>
{"success":true, 
 m_win : {
 	title : 'Elenco programmi'
 },
"items": [
	{
	xtype: 'grid',
	autoScroll : true,
	store: {
	xtype: 'store',
	autoLoad:true,

			proxy: {
			   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_day', 
			   method: 'POST',								
			   type: 'ajax',

		       actionMethods: {
		          read: 'POST'
		        },
		        
		        extraParams: <?php echo acs_je($m_params) ?>,
				
				doRequest: personalizza_extraParams_to_jsonData, 
	
			   reader: {
	            type: 'json',
				method: 'POST',						            
	            root: 'root'						            
	        }
		},
		
       fields: [ 'imm',
                 <?php echo _rilav_H_write_model_fields(array('CV')) ?>],						
						
}, //store
	

      columns: [
       	{text: 'Data rilascio', width: 140, dataIndex: 'imm', renderer : date_from_AS}
        	    			
     , {text: '<img src=<?php echo img_path("icone/48x48/button_blue_play.png") ?> width=20>',					
      		dataIndex: 'H_INI_COD', tooltip: 'Iniziata', width: 35, align: 'right', 
      		exeGestRilavH: 'INI',
      		}
    	
    , {text: '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=20>',					
      		dataIndex: 'H_SOS_COD', tooltip: 'Sospeso', width: 35, align: 'right', 
      		exeChangeRilavH: 'SOS',
      		}
    	
    , {text: '<img src=<?php echo img_path("icone/48x48/check_green.png") ?> width=20>',					
      		dataIndex: 'ASFG03', tooltip: 'Sospeso', width: 35, align: 'right', 
      		exeChangeFlag: 'Y',
      		}
      		
	, {text: '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=20>',					
      		dataIndex: 'ha_note', tooltip: 'Segnalazioni', width: 35, align: 'right', 
      		renderer: function (value, metaData, rec, row, col, store, gridView){
      		  if (rec.get('liv_type') == 'LISTA'){	
      			
			  }
    	}}
      ]
      
, listeners: {
 	celldblclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
 		var rec = iView.getRecord(iRowEl);
 		var m_win = this.up('window');
 		
    	acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_panel', null, {
    	form_values: <?php echo acs_je($m_params->form_values)?>, data : rec.get('imm')});
		m_win.destroy();
    	
 	}
 }  

} //
	 ]
}
<?php
    exit;
}


