<?php

require_once "../../config.inc.php";

$desk_art = new DeskArt(array('no_verify' => 'Y'));
$main_module =  new DeskProd();
$prod_module = $main_module->get_cfg_mod();
$cfg_mod = $desk_art->get_cfg_mod();

if ($_REQUEST['fn'] == 'open_form'){
    
    
    ?>
	
	{"success":true,
	
	m_win : {
	    title: 'Report reparti produzione',
		width: 400, height: 200,
		iconCls: 'icon-print-16'
	},
	 "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	         
						 {
							name: 'f_linea',
							xtype: 'combo',
							fieldLabel: 'Linea',
							multiSelect : true,
							forceSelection: true,								
							displayField: 'text',
							valueField: 'id',								
							emptyText: '- seleziona -',
					   		queryMode: 'local',
                 		    minChars: 1, 	
            				labelWidth : 80,
            				anchor: '-15',
            				width : 150	,				
							store: {
								editable: false,
								autoDestroy: true,
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
							     <?php echo acs_ar_to_select_json($main_module->find_TA_std('LIPRO', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
							    ]
							}
							,listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
	            				 }
	         				 }
							
					  },
					 
					 {xtype: 'fieldcontainer',
				     layout: { 	type: 'hbox'},						
						items: [
						
							{
						xtype: 'radiogroup',
						fieldLabel: 'Dettaglio',
						labelWidth : 80,
						width : 200,
						columns: 1,
						vertical: true,
					    items: [{
                            xtype: 'radio'
                          , name: 'filtro' 
                          , boxLabel: 'Macchine'
                          , inputValue: 'M'
                        },
                        {
                            xtype: 'radio'
                          , name: 'filtro' 
                          , boxLabel: 'Mansioni'
                          , inputValue: 'O'
                        }]							
					},
						{
						xtype: 'checkboxgroup',
						fieldLabel: 'Ricambi',
						margin : '1',
						labelWidth : 50,
						width : 150,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_ricambi' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					}
					
						
						]},
						
					
			
					
					
					
				
	            ],
	            
				buttons: [	
						{
		        		  text: 'Report',
			              scale: 'large',
			              iconCls: 'icon-print-32',
			              handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
		        		}
		       ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}


// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   .grassetto{font-weight: bold;}
   .normal{font-weight: normal;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff; font-size: 13px;}   
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
    
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {
        border-collapse: unset;
	     	
    }
        .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 


<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);
$sql_where = "";
$sql_select = "";
$sql_join = "";
$ar = array();

$sql_where.= sql_where_by_combo_value('TA.TACOGE', $form_values->f_linea);

if($form_values->filtro == 'M'){
    
    //gestiore i ricambi MACRI
    
    $sql_select .= ", TA_MACMA.TAKEY1 AS C_MAC, TA_MACMA.TADESC AS D_MAC, TA_MACMA.TAINDI AS RIFE, 
                      TA_MACMA.TALOCA AS MATRICOLA, TA_MACMA.TAMAIL AS M_NOTE, NT.NTMEMO, TA_MACMA.TACOGE AS C_FOR, CF.CFRGS1 AS D_FOR";
    $sql_join .= "
        LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_MACMA
        ON TA.TADT = TA_MACMA.TADT AND TA_MACMA.TATAID = 'MACMA' AND TA_MACMA.TARIF1 = TA.TAKEY1 
       LEFT OUTER JOIN {$prod_module['file_note']} NT
        ON NT.NTDT = TA_MACMA.TADT AND NT.NTTPNO = 'MACMA' AND NTKEY1 = TA_MACMA.TAKEY1
        LEFT OUTER JOIN {$cfg_mod['file_anag_cli']} CF
        ON CF.CFDT = TA_MACMA.TADT AND digits(CF.CFCD) = TA_MACMA.TACOGE
         ";
    
    if($form_values->f_ricambi == 'Y'){
        
        $sql_select .= " , TA_MACRI.TAKEY2 AS C_RIC, TA_MACRI.TADESC AS D_RIC, TA_MACRI.TAMAIL AS N_RIC, TA_MACRI.TAINDI AS R_RIC ";
        
        $sql_join .= "
        LEFT OUTER JOIN {$prod_module['file_tabelle']} TA_MACRI
        ON TA.TADT = TA_MACRI.TADT AND TA_MACRI.TATAID = 'MACRI' AND TA_MACMA.TAKEY1 = TA_MACRI.TAKEY1 
        ";
        
        
    }
    
}


if($form_values->filtro == 'O'){
    
    $sql_select .= ", TA_PRGRU.TAKEY1 AS MANS, TA_PRGRU.TAKEY2 AS C_OPE, TA_PROPE.TADESC AS D_OPE,  TA_PRGRO.TAKEY1 AS C_MANS, TA_PRGRO.TADESC AS D_MANS";
    $sql_join .= "
    LEFT OUTER JOIN {$prod_module['file_tabelle']} TA_PRGRU
      ON TA.TADT = TA_PRGRU.TADT AND TA_PRGRU.TATAID = 'PRGRU' AND TA_PRGRU.TARIF1 = TA.TAKEY1
    LEFT OUTER JOIN {$prod_module['file_tabelle']} TA_PRGRO
      ON TA.TADT = TA_PRGRO.TADT AND TA_PRGRO.TATAID = 'PRGRO' AND TA_PRGRO.TAKEY1 = TA_PRGRU.TAKEY1 
    LEFT OUTER JOIN {$prod_module['file_tabelle']} TA_PROPE
      ON TA.TADT = TA_PROPE.TADT AND TA_PROPE.TATAID = 'PROPE' AND TA_PROPE.TAKEY1 = TA_PRGRU.TAKEY2
    ";
}


$sql = "SELECT TA.TAKEY1,  TA.TACOGE, TA.TADESC, TA.TAMAIL, 
        TA_LIPRO.TADESC AS D_LINEA, TA_LIPRO.TAMAIL AS L_NOTE
        {$sql_select}
        FROM {$cfg_mod['file_tabelle']} TA
        LEFT OUTER JOIN {$prod_module['file_tabelle']} TA_LIPRO
        ON TA.TADT = TA_LIPRO.TADT AND TA_LIPRO.TATAID = 'LIPRO' AND TA.TACOGE = TA_LIPRO.TAKEY1 
        {$sql_join}
        WHERE TA.TADT = '{$id_ditta_default}' AND TA.TATAID = 'REPMA'
        {$sql_where}";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$ar = array();
while ($row = db2_fetch_assoc($stmt)) {

    
    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    $cod_liv0 = $row['TACOGE'];
    $cod_liv1 = $row['TAKEY1'];
    if($form_values->filtro == 'M')
        $cod_liv2 = $row['C_MAC'];
    
    if($form_values->f_filtro == 'O')
        $cod_liv2 = trim($row['MANS'])."_".trim($row['C_OPE']);
    
    if($form_values->f_ricambi == 'Y')
        $cod_liv3 = trim($row['C_MAC'])."_".trim($row['C_RIC']);
    
   //LINEA
    $liv =$cod_liv0;
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['codice'] = trim($row['TACOGE']);
        if(trim($row['TACOGE']) == ''){
            $ar_new['task'] = 'LINEA NON ASSEGNATA';
            $ar_new['codice'] = 'NA';
        }else{
            $ar_new['task'] = $row['D_LINEA'];
            $ar_new['codice'] = trim($row['TACOGE']);
        }
        
        $ar_new['note'] = $row['L_NOTE'];
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    
    //REPARTO
    $liv=$cod_liv1;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new = $row;
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['codice'] = trim($row['TAKEY1']);
        $ar_new['task'] =  $row['TADESC'];
        $ar_new['note'] = $row['TAMAIL'];
        
        $ar_new['m_codice'] = $row['C_MAC'];
        $ar_new['m_descrizione'] = $row['D_MAC'];
        $ar_new['riferimento'] = $row['RIFE'];
        $ar_new['matricola'] = $row['MATRICOLA'];
       
        $ar_new['m_note'] = "";
        if(trim($row['C_FOR']) > 0)
            $ar_new['m_note'] .= "[".trim($row['C_FOR'])."] ".trim($row['D_FOR'])."<br>";
            $ar_new['m_note'] .= trim($row['M_NOTE']);
        if($ar_new['m_note'] != ''){
            if(trim($row['NTMEMO']) != '')
                $ar_new['m_note'] .= "<br>".$row['NTMEMO'];
        }else{
            $ar_new['m_note'] .= $row['NTMEMO'];
        }
        
        
        $ar_new['liv'] = trim($row['C_MANS'])."_".trim($row['C_OPE']);
        $ar_new['mans_codice'] = $row['C_MANS'];
        $ar_new['mans_descrizione'] = $row['D_MANS'];
        $ar_new['operatore'] = "[".trim($row['C_OPE'])."] ".trim($row['D_OPE']);
     
        
        $ar_r["{$liv}"] = $ar_new;
        
    }
    
    $ar_r=&$ar_r[$liv];
    
    //MACCHINE
    if($form_values->filtro == 'M'){
        $liv=$cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['codice'] = "";//trim($row['C_MAC']);
            $ar_new['task'] =  "";//$row['D_MAC'];
            $ar_new['note'] = "";//$row['C_MAC'];
            
            $ar_new['m_codice'] = $row['C_MAC'];
            $ar_new['m_descrizione'] = $row['D_MAC'];
            $ar_new['riferimento'] = $row['RIFE'];
            $ar_new['matricola'] = $row['MATRICOLA'];
            $ar_new['m_note'] = "";
            if(trim($row['C_FOR']) > 0)
                $ar_new['m_note'] .= "[".trim($row['C_FOR'])."] ".trim($row['D_FOR'])."<br>";
            $ar_new['m_note'] .= trim($row['M_NOTE']);
            if($ar_new['m_note'] != ''){
                if(trim($row['NTMEMO']) != '')
                    $ar_new['m_note'] .= "<br>".$row['NTMEMO'];
            }else{
                $ar_new['m_note'] .= $row['NTMEMO'];
            }
          
            
            $ar_r["{$liv}"] = $ar_new;
            
        }
        
        $ar_r = &$ar_r["{$liv}"];
    }
   
    
    //MANSIONI
    if($form_values->filtro == 'O'){
        $liv=$cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['id'] = implode("|", $tmp_ar_id);
            
            $ar_new['codice'] = "";//trim($row['C_MAC']);
            $ar_new['task'] =  "";//$row['D_MAC'];
            $ar_new['note'] = "";//$row['C_MAC'];
    
            $ar_new['liv'] = $liv;
            $ar_new['mans_codice'] = $row['C_MANS'];
            $ar_new['mans_descrizione'] = $row['D_MANS'];
            $ar_new['operatore'] = "[".trim($row['C_OPE'])."] ".trim($row['D_OPE']);
            
            
            $ar_r["{$liv}"] = $ar_new;
            
        }
        
        $ar_r = &$ar_r["{$liv}"];
    }
    
    
    //RICAMBI
    if($form_values->f_ricambi == 'Y'){
        $liv=$cod_liv3;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['id'] = implode("|", $tmp_ar_id);
            
            $ar_new['ricambi'] = trim($row['C_RIC']);
            $ar_new['m_descrizione'] = $row['D_RIC'];
            $ar_new['riferimento'] = $row['R_RIC'];
            $ar_new['m_note'] = trim($row['N_RIC']);
            
            $ar_r["{$liv}"] = $ar_new;
            
        }
        
        $ar_r = &$ar_r["{$liv}"];
    }
  
    
   
    
}//while

echo "<div id='my_content'>"; 

echo "<div class=header_page>";
echo "<H2>Riepilogo reparti di produzione</H2>";
echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";
        
echo "<table class=int1>";
       echo "<tr class='liv_data'>
          <th>Codice</th>
          <th>Descrizione</th>
          <th>Note</th>";
     
       if($form_values->filtro == 'M'){
            echo "<th>Codice</th>
                 <th>Descrizione</th>
                 <th>Riferimento</th>
                 <th>Matricola</th>
                 <th>Note</th>";
            
            if($form_values->f_ricambi == 'Y')
                echo "<th>Ricambi</th>";
        }
        
        
        if($form_values->filtro == 'O'){
            echo "<th>Codice</th>
                 <th>Descrizione</th>
                 <th>Riferimento</th>";
        }
        
       
        
         echo  "</tr>";
   
 //LINEA
foreach($ar as $k => $v){

    echo "<tr class = liv1>
          <td><b>{$v['codice']}</b></td>
          <td><b>{$v['task']}</b></td>
          <td>{$v['note']}</td>";

    if($form_values->filtro == 'M'){
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        
        if($form_values->f_ricambi == 'Y')
            echo "<td>&nbsp;</td>";
    }
    
    if($form_values->filtro == 'O'){
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
        echo "<td>&nbsp;</td>";
    }
    

    echo "</tr>";
    
    //REPARTO
    foreach($v['children'] as $k1 => $v1){
        
        if($v1['codice'] != $codice){
            $classe = 'grassetto';
        }else{
            $classe = 'normal';
           
        }
        
      echo "<tr>
           <td class = {$classe} valign = top>{$v1['codice']}</td>
           <td class = {$classe} valign = top>{$v1['task']}</td>
           <td valign = top>{$v1['note']}</td>";
      
      if($form_values->filtro == 'M'){
          echo " <td valign = top>{$v1['m_codice']}</td>
                 <td valign = top>{$v1['m_descrizione']}</td>
                 <td valign = top>{$v1['riferimento']}</td>
                 <td valign = top>{$v1['matricola']}</td>
                 <td valign = top>{$v1['m_note']}</td>";
          
          if($form_values->f_ricambi == 'Y')
              echo "<td>&nbsp;</td>";
      }
      
      if($form_values->filtro == 'O'){
          echo " <td valign = top>{$v1['mans_codice']}</td>
                 <td valign = top>{$v1['mans_descrizione']}</td>
                 <td valign = top>{$v1['operatore']}</td>";
      }
     
      $codice = $v1['codice'];
    echo "</tr>";
            
                        
    if($form_values->filtro == 'M'){
        //MACCHINA
       foreach($v1['children'] as $k2 => $v2){
           if($v1['m_codice'] != $v2['m_codice']){
               echo "<tr>
               <td valign = top>{$v1['codice']}</td>
               <td valign = top>{$v1['task']}</td>
               <td>&nbsp;</td>";
               
               echo " <td valign = top>{$v2['m_codice']}</td>
               <td valign = top>{$v2['m_descrizione']}</td>
               <td valign = top>{$v2['riferimento']}</td>
               <td valign = top>{$v2['matricola']}</td>
               <td valign = top>{$v2['m_note']}</td>";
               if($form_values->f_ricambi == 'Y')
                   echo "<td>&nbsp;</td>";
              echo "<tr>";
         
           }
           
           
           //RICAMBI
           if($form_values->f_ricambi == 'Y'){
               
               foreach($v2['children'] as $k3 => $v3){
                   if($v3['ricambi'] != ''){
                   echo "<tr>
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>";
                   
                   echo " <td>&nbsp;</td>
                   <td>{$v3['m_descrizione']}</td>
                   <td>{$v3['riferimento']}</td>
                   <td>&nbsp;</td>
                   <td>{$v3['m_note']}</td>
                   <td>{$v3['ricambi']}</td>
                   <tr>";
                   }
               }
               
           }
           
           
        }
        
    }
    
    if($form_values->filtro == 'O'){
        //MANSIONE OPERATORE
        foreach($v1['children'] as $k2 => $v2){
            if($v1['liv'] != $v2['liv'] && strlen(trim($v2['mans_codice'])) > 0){
                echo "<tr>
                <td>{$v1['codice']}</td>
                <td>{$v1['task']}</td>
                <td>{$v1['note']}</td>";
                
                echo " <td>{$v2['mans_codice']}</td>
                <td>{$v2['mans_descrizione']}</td>
                <td>{$v2['operatore']}</td>
                <tr>";
                
            }
        }
        
    }   }
  
}
echo "</div>";

 		
?>

 </body>
</html>


<?php
exit;
}



