<?php

 require_once "auth.php";
 require_once("utility/function.php");
 require_once "config.inc.general.php";
 
 define("ROOT_PATH", "/acs_portal/");
 define("IMAGE_PATH", "/acs_portal/images/");
 
 //$inc_path = '/www/zendcore/htdocs/acs_portal';
 $inc_path = '/www/zendsvr6/htdocs/acs_portal';
 define("ROOT_ABS_PATH", $inc_path . "/");

 $id_ditta_default = '1 ';
 
 $backend_ERP = 'SV2';
 
 
 //XML toolkit                                                                                      
 $inc_path .= ':/usr/local/zendsvr6/share/ZendFramework/library:/usr/local/zendsvr6/share/pear';    
 $inc_path .= ':/usr/local/ZendSvr/share/ToolkitApi';                                               
 
 set_include_path(get_include_path() . PATH_SEPARATOR . $inc_path); 

 date_default_timezone_set('Europe/Rome');
 
 error_reporting(E_ALL ^ E_NOTICE);
 ini_set('display_errors','On'); 
 
 session_start();
  
 include_once 'ToolkitService.php';    
  
 $appLog = new AppLog();
 

// --------- Connession locale a As/400 ------------------
 $i5_host		= '*LOCAL';
 $i5_user		= 'QPGMR';
 $i5_psw		= 'acsacs';

 
 $namingMode = DB2_I5_NAMING_ON; // ON or OFF ??????
 $conn = db2_pconnect($i5_host, $i5_user, $i5_psw);
 ////$conn = db2_pconnect($i5_host, $i5_user, $i5_psw); 
 if(!$conn) die('Login Failed to i5 system'); 

 $useToolkit = 'Y';
 
 
 /* CONNESSIONE A TOOLKIT SERVICE */
 try {
 	/////$tkObj = ToolkitService::getInstance($db, $user, $pass);
 	$tkObj = ToolkitService::getInstance($conn);
 }
 catch (Exception $e)
 {
 	echo  $e->getMessage(), "\n";
 	exit();
 }
 
 $maxpool = 10; // 10 jobs good enough to handle my machine needs
 $tkObj->setToolkitServiceParams(array(
 		'InternalKey'=>'/tmp/packers'.rand(1,$maxpool),
 		'debug'=>true,
 		'plug' => "iPLUG32K"));
 
 
 

 
 $libreria_predefinita = "QS36F";
 $libreria_predefinita_EXE = "SV2EXE";
 
 // TEST PER ALA
 //$libreria_predefinita 		= '$FILEALA'; 
 //$libreria_predefinita_EXE 	= '$OBJALA';
 
 
 
/* CONFIGURAZIONI MODULI */
 $cfg_mod_Spedizioni = array(
 
 	"invia_dati_spedizione_a_mbm" => 'Y',	//_mbm o _sav
 		
 	"abilita_assegna_posizione" => 'Y',
 	"forza_scelta_itinerario_su_ASS_SPED_da_INFO" => 'Y',	
 	"disabilita_tutti_vincoli_su_assegna_sped" => 'Y',	
 		
        //integrazione lotto <-> indice rottura
        "scrivi_END_indice_rottura" 		=> 'N',
        "imposta_lotto_da_indice_rottura" 	=> 'N',
 
 	"force_use_session_history" => 'Y',
 	"overflow_SBLOC_per_area" => 'N',
 	"nr_max_in_el_spedizioni22" => 9,
 	"disabilita_sprintf_codice_cliente" => 'N',
 	"default_calendar_field" => "VOL",
 	"sum_su_TDFN10"		 => "Y",
 	"gestione_per_riga"	 => "N", //ad esempio in report scarichi mostro il dettaglio righe	
 	"file_testate" 		 => "{$libreria_predefinita}.WPI0TD0",
 	"file_righe" 		 => "{$libreria_predefinita}.WPI0RD0",
 	"file_righe_note_cli" => "{$libreria_predefinita}.WPI0RD1", //logico filtrato per NCSV2 	
 	"file_colli" 		 => "{$libreria_predefinita}.XX0S2PL0", 	
 	"file_calendario" 	 => "{$libreria_predefinita}.WPI0CS0",
 	"file_tabelle" 		 => "{$libreria_predefinita}.WPI0TA0", 	
 	"file_numeratori"	 => "{$libreria_predefinita}.WPI0NR0", 	
 	"file_carichi"	 	 => "{$libreria_predefinita}.WPI0PS0", 	
 	"tipo_calendario"	 => "*CS",
 	 	 	
 	"file_disponibilita" => "{$libreria_predefinita}.WMWF30", 	
 	"file_giacenze" 	 => "{$libreria_predefinita}.WMWF20",
 	"file_testate_imp" 	 => "{$libreria_predefinita}.WMWF10",
 	 	
 	"file_promemoria"	 => "{$libreria_predefinita}.WEMAIL0",
 	"bl_commenti_ordine" => "SPE", //nelle righe ordine
 	"file_listino_trasportatori" => "{$libreria_predefinita}.WPI0LT0",
 	"file_richieste"	 => "{$libreria_predefinita}.WPI0RI0",
 	//"call_odbc_after_history" => "CALL SV2EXE.UR21H1C",
 	//"call_odbc_after_history" => "CALL QSYS.QCMDEXC('CRTLIB LIB(PTF_MATTE7)', 0000000022.00000)", 	
 	"file_deroghe"	 	 => "{$libreria_predefinita}.WPI0AD0",
 	
/* 	
 	// TEST PER GL (protocollazione)
 	"file_anag_cli" 	 => 'E42E_$FILX.$BAANAF0',
 	"file_anag_cli_des"  => 'E42E_$FILX.$BFINDF0',
*/ 	
 	 	
  
 	"file_anag_cli" 	 => "{$libreria_predefinita}.XX0S2CF0",
 	"file_anag_cli_des"  => "{$libreria_predefinita}.XX0S2TA0",
 
 	 	
 	"file_assegna_ord"   => "{$libreria_predefinita}.WPI0AS0",
 	"file_capacita_produttiva"   => "{$libreria_predefinita}.WPI0CP0",
 	"file_cronologia_ordine"   => "{$libreria_predefinita}.XX0S2PQ0",
 	"file_coordinate_geografiche"   => "{$libreria_predefinita}.WPI0GL2", 	

 	"visualizza_order_img"	=> "N",
 	"url_img_FROM_PC"		=> "http://192.168.2.77:82/acs_portal_img/get_order_images.php",
 	
 	"carico_per_data"	 	=> "Y",

 	"file_testate_doc_gest"  => "{$libreria_predefinita}.XX0S2TD0",
 	"file_righe_doc_gest" 	 => "{$libreria_predefinita}.XX1S2RD1", 	
 	"file_righe_doc_gest_valuta" 	 => "{$libreria_predefinita}.XX0S2RT0",
 	"file_note"				=>  "{$libreria_predefinita}.WPI0NT0",
 	
 	"file_analisi_esposizione"				=>  "{$libreria_predefinita}.WESPT0WPI", 	
 	"file_analisi_esposizione_dett"			=>  "{$libreria_predefinita}.WESPD0WPI", //ordini
 	"file_analisi_esposizione_cont_dett"	=>  "{$libreria_predefinita}.WESPR0WPI", //dati contabili	
 																
 	
 	"report_scarichi_note_sotto_cliente" => "Y",

 	"file_riservatezza"			=>  "{$libreria_predefinita}.WPI0PG0",
 	
 	"file_parametri_operatori_OE"	=>  "{$libreria_predefinita}.WPI0OE0" 	
 	
 );
 
 
//MBM 
 $cfg_delivery_control = array(
 	"items" => array("file" => "{$libreria_predefinita}.WIT0IT0", "id" =>  "ITID", "campi_data" => array('ITDTGE', 'ITDTUM')),
 	"companies" => array("file" => "{$libreria_predefinita}.WIT0CO0", "id" =>  "COID", "campi_data" => array('CODTGE', 'CODTUM')),
 	"addresses" => array("file" => "{$libreria_predefinita}.WIT0AD0", "id" =>  "ADID", "campi_data" => array('ADDTGE', 'ADDTUM')),
 	"trips" => array("file" => "{$libreria_predefinita}.WIT0TR0", "id" =>  "TRID", "campi_data" => array('TRSCHA', 'TRDTGE', 'TRDTUM')),
 	"deliveries" => array("file" => "{$libreria_predefinita}.WIT0DE0", "id" =>  "DEID", "campi_data" => array('DEDTRG', 'DEDTCO', 'DEDTGE', 'DEDTUM')),
 	"delivery_rown" => array("file" => "{$libreria_predefinita}.WIT0DR0", "id" =>  "DRID", "campi_data" => array('DRDTPR', 'DRDTSC', 'DRDTGE', 'DRDTUM')),
 	"udc" => array("file" => "{$libreria_predefinita}.WIT0UD0", "id" =>  "UDID", "campi_data" => array('UDDTGE', 'UDDTUM')),
 	"udc_rows" => array("file" => "{$libreria_predefinita}.WIT0UR0", "id" =>  "URID", "campi_data" => array('URDTBA', 'URDTPR', 'URDTSC', 'URDTGE', 'URDTUM'))
 );
 
/* 
 //SAV
 $cfg_delivery_control = array(
 		"carico" => array("file" => "{$libreria_predefinita}.WPI8PS0", "id" =>  "ITID", "campi_data" => array('ITDTGE', 'ITDTUM')),
 		"testata" => array("file" => "{$libreria_predefinita}.WPI8TD0", "id" =>  "COID", "campi_data" => array('CODTGE', 'CODTUM')),
 		"colli" => array("file" => "{$libreria_predefinita}.WPI8PL0", "id" =>  "ADID", "campi_data" => array('ADDTGE', 'ADDTUM'))
 );
*/ 
 
 
 
 $cambia_preferenza = "{ 'vuoto' : ['N', 'F', 'C', 'P', 'R'],
 						'N' : ['N'],
 						'F' : ['F', 'N'],
 						'C' : ['C', 'N'],
 						'P' : ['P', 'N'],
						'R' : ['R', 'N'] 
 }";
 
 
 
 $cfg_mod_DeskAcq = array(
 	"file_regole_riordino" 	 => "STO2M643.WPI0CR0",
 	"file_testate" 		 => "{$libreria_predefinita}.WPI1TD0",
 	"file_righe_doc_gest" => "{$libreria_predefinita}.XX1S2RD1",
 	"file_calendario" 	 => "{$libreria_predefinita}.WPI1CS0",
 	"tipo_calendario"	 => "*CO", 	
 	"file_tabelle" 		 => "{$libreria_predefinita}.WPI1TA0",
 	"file_numeratori"	 => "{$libreria_predefinita}.WPI1NR0",
 	"file_cronologia_ordine"   => "{$libreria_predefinita}.XX0S2PQ0", 	
 	
 	"visualizza_order_img"	=> "N",
 	
 	"file_commenti_ordine" => "{$libreria_predefinita}.WPI1RD0", //nelle righe ordine
 	"bl_commenti_ordine" => "DORA", //nelle righe ordine
 	"file_anag_art" 	 => "{$libreria_predefinita}.XX0S2AR0",
 	"file_richieste"	 => "{$libreria_predefinita}.WPI1RI0",

 	//LIBRERIE WMAF
 	"file_WMAF10"	 => "{$libreria_predefinita}.WMWF10", 	
 	"file_WMAF20"	 => "{$libreria_predefinita}.WMWF20",
 	"file_WMAF30"	 => "{$libreria_predefinita}.WMWF30",

// 	"file_analisi_rivendita_promo" => "STO2M643.WBR26D40",
 	"file_analisi_rivendita_promo" => "STO2M643.WBR26D70",
 	
 	"file_orari_disponibili"	=> "{$libreria_predefinita}.WPI1OD0",
 	"file_proposte_MTO"		=> "{$libreria_predefinita}.XX0S2MT0",
 	"def_area"				=> "A01" 	
 );
 

 
 $cfg_mod_DeskStats = array(
 		"disabilita_sprintf_codice_cliente" => 'N', 		
 		"file_tab_sys" 	 		=> "{$libreria_predefinita}.XX0S2TA0", 		 	
 		"file_testate_ven" 	 	=> "{$libreria_predefinita}.WB2ST0",
 		"file_docum" 	 		=> "{$libreria_predefinita}.WB2DO0",
 		"dir_fatture"			=> "/SV2/FATTURE",
 		"anagrafiche_cliente_da_modulo" => 'N',
 		"ar_ditte"				=> array(
 					array("id" => "1", "text" => "Mia ditta")
 				),
		"ar_cartelle"			=> array(
					array("id" => "VF", "text" => "Fatture di vendita")
 		),
 );
 

 $cfg_mod_Gest = array(
 		"file_tabelle" 	 		 => "{$libreria_predefinita}.WPI0TA0",
 		"file_tab_sys" 	 		 => "{$libreria_predefinita}.XX0S2TA0",
 		"file_ABI"		 => "{$libreria_predefinita}.ABIF0",
 		"file_CAB"		 => "{$libreria_predefinita}.CABF0",
 		"abi"	=> array( 				
 					"file_conti" 	 => "ANALISICE.WPI0CM0",
 					"file_movimenti" => "ANALISICE.WPI0MM0",
 			   ),
 		"flussi_cassa" 	=> array(
 				"disabilitato" => "N",
 				//"file_conti" 	 => "ANALISICE.FLUSSTF0",
 				//"file_movimenti" => "ANALISICE.FLUSSIF0",
 				"file_conti" 	 => "{$libreria_predefinita}.WPI0FT0",
 				"file_movimenti" => "{$libreria_predefinita}.WPI0FC0"
 		),
 		"marginalita"	=> array(
 				 
 		), 		
 		"provvigioni" 	=> array(
 				"file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
 				"file_righe" 	 => "{$libreria_predefinita}.WPI0RF0"
 		),
 		"bollette_doganali" 	=> array(
 				"file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
 				"file_righe" 	 => "{$libreria_predefinita}.WPI0RF0",
 				"anagrafiche_cliente_da_modulo" => 'Y',
 				"file_assegna_ord"   => "{$libreria_predefinita}.WPI0AS0",
 		),
 		"ins_new_anag" 	=> array(
 				"file_appoggio"  	=> "{$libreria_predefinita}.WPI0GC0",
 				"file_appoggio_cc"  => "{$libreria_predefinita}.WPI0CC0",
 				"file_appoggio_crm"  => "{$libreria_predefinita}.WPI0GC0",
 				"file_cerved"	 => "{$libreria_predefinita}.WPI0IC0" 				
 		),
 		"fatture_anticipo" 	=> array(
 				"file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
 				"file_righe" 	 => "{$libreria_predefinita}.WPI0RF0"
 		),
 		"dichiarazione_intenti" 	=> array(
 		"file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
 		"file_righe" 	 => "{$libreria_predefinita}.WPI0RF0"
 		)
 );
 
 

 $cfg_mod_DeskUtility = array(
 		"file_tab_sys" 	 		 => "{$libreria_predefinita}.XX0S2TA0",
 		"file_anag_art" 	 => "{$libreria_predefinita}.XX0S2AR0"
 );
 
 
 
 
 
 $cfg_mod_Admin = array(
 	"file_utenti" 			 => "{$libreria_predefinita}.WPI0UT0",
 	"file_tabelle" 		 	 => "{$libreria_predefinita}.WPI0TA0", 	
 	"file_moduli_utente" 	 => "{$libreria_predefinita}.WPI0UP0", 	 	
 	"file_app_log" 	 		 => "{$libreria_predefinita}.WPI0LG0",
 	"file_app_log_history" 	 => "{$libreria_predefinita}.WPI0LH0",

 	
 	"file_tab_sys" 	 		 => "{$libreria_predefinita}.XX0S2TA0",
/* 	
 	//test per GL (protocollazione)
 	"file_tab_sys" 	 		 => 'E42E_$FILX.$BTTABF0', 	
 	"file_tab_sys_vars" 	 => 'E42E_$FILX.$QTTABF0',
*/ 	

 	"file_anag_cli" 	 => "{$libreria_predefinita}.XX0S2CF0",
 	"file_anag_cli_des"  => "{$libreria_predefinita}.XX0S2TA0",
 	
 	"file_anag_art" 	 => "{$libreria_predefinita}.XX0S2AR0",

 	"file_memorizzazioni"=> "{$libreria_predefinita}.WPI0DF0",
 	
 	//file appoggio (per sessione)
 	"file_WPI0WS0"	 => "{$libreria_predefinita}.WPI0WS0",

 	"file_attivita_utente"	 => "{$libreria_predefinita}.WPI0AU0", 	
 	"file_help"				=>  "{$libreria_predefinita}.WPI0HE0",
 	"user_help_modify"		=> "marosti;GIO"
 	
 ); 
 
 
 

 //ACQUISIZIONE PESO/IMMAGINI
 $cfg_mod_Spedizioni["acquisisci_peso_immagini"]["telecamere"]["01"] = array(
 		"descr" => "Telecamera 001",
 		"url" 	=> "http://192.168.0.23/axis-cgi/jpg/image.cgi?resolution=2592x1944&compression=0&colorlevel=100",
 		"url_video" => "http://192.168.0.23",
 		"user" 	=> "root",
 		"psw"	=> "cucinestosa",
 		"copy_to_ftp" => "Y"
 );
 $cfg_mod_Spedizioni["acquisisci_peso_immagini"]["telecamere"]["02"] = array(
 		"descr" => "Telecamera 002",
 		"url" 	=> "http://192.168.0.159/axis-cgi/jpg/image.cgi?resolution=2592x1944&compression=0&colorlevel=100",
 		"url_video" => "http://192.168.0.159",
 		"user" 	=> "root",
 		"psw"	=> "cucinestosa",
 		"copy_to_ftp" => "Y"
 );
 
 
 
 
 //PARAMETRI AGGIUNTIVI (ANDRANNO SPOSTATI NELLA TABELLA DF0)
 $cfg_mod_Spedizioni["protocollazione"]["divisione"]["DEF"] = "001";
 $cfg_mod_Spedizioni["protocollazione"]["stdo"]["DEF"] = "PE"; 
 $cfg_mod_Spedizioni["protocollazione"]["stdo"]["LIST"] = "PE|CO|AA|AP"; 
 $cfg_mod_Spedizioni["protocollazione"]["prio"]["DEF"] = "5";

 //TIPOLOGIE DI ESTRATTO CONTO
 $cfg_mod_Spedizioni["print_ec"]["001"] = "Stampa standard"; 
 $cfg_mod_Spedizioni["print_ec"]["002"] = "Stampa personalizzata";
 
 
 //itinerari per stato (agenda settimanale)
 $cfg_mod_Spedizioni["agenda_settimanale"]["cal_per_stato"]["GAP"] = array(
 		"id"	=> 997,
 		"title"	=> "Maripref - GAP",
 		"color" => 14
 );

 
 //default per open_params in manutenzioe parametri operatori OE
 $cfg_mod_Spedizioni["manutenzioni_parametri_operatori_OE"]["open_params"]["STD"] = array(
 		"OETPCA"		=> 'MONDO',
 		"f_area"		=> "I",
 		"f_divisione"	=> "NEW"
 );
 
 
 //PARAMETRI PER LINK NELLA HOME
 $cfg_home_button_link["desktop_lavorazioni"] = array("link" => 'module/desk_utility/', "text" => "Desktop Utility", "f-section" => 'std');

 
 //report scarichi: forza somma valori per tipo ordine/causale di trasporto
 $cfg_mod_Spedizioni["report_scarichi_forza_somma_importi"]["CU"] = array('VEN'); 
 
 
 //Italserramenti
 $cerved_user_data = array(
 		"grant_type" => "password",
 		"client_id" => 'cerved-client',
 		"username"	=> 'NET_ITA_08112016',
 		"password"	=> '8riL-olsiV'
 ); 
 
 $email_test = "m.arosti@apracs.it";
 
 //Configurazioni servizio di posta
 $smtp_address = "192.168.2.13";
 ini_set("SMTP", $smtp_address); 
 ini_set("sendmail_from", $email_test);
 
 //video youtube onClick su logo (home page)
 //$youtube_video_main_ID = '9EZyrar5fjw'; //STOSA
 
 
 //Autentificazione
 $auth = Auth::get_session_auth();
  
 if (!$auth->is_authenticate() && $_SERVER['SCRIPT_NAME'] != ROOT_PATH . "index.php" &&
 	 $_SERVER['SCRIPT_NAME'] != ROOT_PATH . "login.php")
	header("location: " . ROOT_PATH . "index.php");
 
 //memorizzo nella tabella utente l'utima operazione
 $auth->update_table_user_session();
 
 
 

 if (!function_exists('mb_sprintf')) {
 	function mb_sprintf($format) {
 		$argv = func_get_args() ;
 		array_shift($argv) ;
 		return mb_vsprintf($format, $argv) ;
 	}
 }
 if (!function_exists('mb_vsprintf')) {
 	/**
 	 * Works with all encodings in format and arguments.
 	 * Supported: Sign, padding, alignment, width and precision.
 	 * Not supported: Argument swapping.
 	 */
 	function mb_vsprintf($format, $argv, $encoding=null) {
 		if (is_null($encoding))
 			$encoding = mb_internal_encoding();
 
 		// Use UTF-8 in the format so we can use the u flag in preg_split
 		$format = mb_convert_encoding($format, 'UTF-8', $encoding);
 
 		$newformat = ""; // build a new format in UTF-8
 		$newargv = array(); // unhandled args in unchanged encoding
 
 		while ($format !== "") {
 
 			// Split the format in two parts: $pre and $post by the first %-directive
 			// We get also the matched groups
 			list ($pre, $sign, $filler, $align, $size, $precision, $type, $post) =
 			preg_split("!\%(\+?)('.|[0 ]|)(-?)([1-9][0-9]*|)(\.[1-9][0-9]*|)([%a-zA-Z])!u",
 					$format, 2, PREG_SPLIT_DELIM_CAPTURE) ;
 
 			$newformat .= mb_convert_encoding($pre, $encoding, 'UTF-8');
 
 			if ($type == '') {
 				// didn't match. do nothing. this is the last iteration.
 			}
 			elseif ($type == '%') {
 				// an escaped %
 				$newformat .= '%%';
 			}
 			elseif ($type == 's') {
 				$arg = array_shift($argv);
 				$arg = mb_convert_encoding($arg, 'UTF-8', $encoding);
 				$padding_pre = '';
 				$padding_post = '';
 
 				// truncate $arg
 				if ($precision !== '') {
 					$precision = intval(substr($precision,1));
 					if ($precision > 0 && mb_strlen($arg,$encoding) > $precision)
 						$arg = mb_substr($precision,0,$precision,$encoding);
 				}
 
 				// define padding
 				if ($size > 0) {
 					$arglen = mb_strlen($arg, $encoding);
 					if ($arglen < $size) {
 						if($filler==='')
 							$filler = ' ';
 						if ($align == '-')
 							$padding_post = str_repeat($filler, $size - $arglen);
 						else
 							$padding_pre = str_repeat($filler, $size - $arglen);
 					}
 				}
 
 				// escape % and pass it forward
 				$newformat .= $padding_pre . str_replace('%', '%%', $arg) . $padding_post;
 			}
 			else {
 				// another type, pass forward
 				$newformat .= "%$sign$filler$align$size$precision$type";
 				$newargv[] = array_shift($argv);
 			}
 			$format = strval($post);
 		}
 		// Convert new format back from UTF-8 to the original encoding
 		$newformat = mb_convert_encoding($newformat, $encoding, 'UTF-8');
 		return vsprintf($newformat, $newargv);
 	}
 }
?>