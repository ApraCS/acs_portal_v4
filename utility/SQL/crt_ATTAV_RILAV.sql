/* su ordine gia' assegnato, riassegno il referente per il caricamento dell'order entry */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAFG01, TAKEY1, TAKEY2, TADESC, TARIF2) VALUES( '1 ', 'RILAV', 'Y', 'IMMOR', 'RIA', 'Modifica operatore Order Entry', 'IMMOR')
INSERT INTO $FILNEW/WPI0TA0(TADT, TATAID, TAFG01, TAKEY1, TAKEY2, TADESC, TARIF2) VALUES( '1 ', 'RILAV', 'Y', 'IMMOR', 'RES', 'Annulla operatore Order Entry', 'REFOR')
 
 
/* su ordine critico (gestione anomalia ATP), lo riassegno al referente per la variazione dell'order entry */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAFG01, TAKEY1, TAKEY2, TADESC, TARIF2) VALUES( '1 ', 'RILAV', 'Y', 'POSTM', 'VAR', 'Variazione ordine', 'IMMOR')
 

/* - ATTIVITA TODO - BOLLETTE DOGANALI  - TEST */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TARIF1) VALUES('1 ', 'ATTAV', 'BDRDA', 'Richiesta documenti ad amministrazione', 'BD')
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TARIF1) VALUES('1 ', 'ATTAV', 'BDIDA', 'Inviata documentazione a dogana', 'BD')
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TARIF1) VALUES('1 ', 'ATTAV', 'BDALT', 'Altro', 'BD')


/* bollette doganali - TEST */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAFG01, TAKEY1, TAKEY2, TADESC, TARIF2) VALUES
('1 ', 'RILAV', 'Y', 'BDRDA', 'CLOSE', 'Operazione eseguita', ''),
('1 ', 'RILAV', 'Y', 'BDIDA', 'CLOSE', 'Operazione eseguita', ''),
('1 ', 'RILAV', 'Y', 'BDALT', 'CLOSE', 'Attivit� evasa', ''),
('1 ', 'RILAV', 'Y', 'BDALT', 'ANNUL', 'Attivit� annullata', '')

 /* - ATTIVITA TODO */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES('1 ', 'ATTAV', 'SBLOC', 'Sblocco cliente/ordine/esubero fido')




/**********************************************************
 BOLLETTE DOGANALI
***********************************************************/
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TARIF1) VALUES
 ('1 ', 'ATTAV', 'BDATTD',  'Attesa disposizioni', 'BD'),
 ('1 ', 'ATTAV', 'BDATTV',  'Attesa visto uscire', 'BD'),
 ('1 ', 'ATTAV', 'BDRICRE', 'Richiedere rettifica', 'BD'),
 ('1 ', 'ATTAV', 'BDATTR',  'Attesa rettifica', 'BD'),
 ('1 ', 'ATTAV', 'BDSOLLE', 'Sollecito bolletta', 'BD'),
 

INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAFG01, TAKEY1, TAKEY2, TADESC, TARIF2) VALUES
('1 ', 'RILAV', 'Y', 'BDATTD', 'CLOSE', 'Attivit� evasa', ''),
('1 ', 'RILAV', 'Y', 'BDATTD', 'ANNUL', 'Attivit� annullata', ''),
('1 ', 'RILAV', 'Y', 'BDATTV', 'CLOSE', 'Attivit� evasa', ''),
('1 ', 'RILAV', 'Y', 'BDATTV', 'ANNUL', 'Attivit� annullata', ''),
('1 ', 'RILAV', 'Y', 'BDRICRE', 'CLOSE', 'Attivit� evasa', ''),
('1 ', 'RILAV', 'Y', 'BDRICRE', 'ANNUL', 'Attivit� annullata', ''),
('1 ', 'RILAV', 'Y', 'BDATTR', 'CLOSE', 'Attivit� evasa', ''),
('1 ', 'RILAV', 'Y', 'BDATTR', 'ANNUL', 'Attivit� annullata', ''),
('1 ', 'RILAV', 'Y', 'BDSOLLE', 'CLOSE', 'Attivit� evasa', ''),
('1 ', 'RILAV', 'Y', 'BDSOLLE', 'ANNUL', 'Attivit� annullata', '')



/**********************************************************
 BLOCCO/SBLOCCO (OVERFLOW)
***********************************************************/
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TARIF1, TAFG02, TARIF2) VALUES
 ('1 ', 'ATTAV', 'CON',  'Condizione agente', 'SB', 'R', 'OK'),
 ('1 ', 'ATTAV', 'AUT',  'Autorizzazione', 'SB', 'R', 'OK')

INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAFG01, TAKEY1, TAKEY2, TADESC, TARIF2) VALUES
('1 ', 'RILAV', 'Y', 'CON', 'OK', 'oK', ''),
('1 ', 'RILAV', 'Y', 'AUT', 'OK', 'oK', '')


INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAFG01, TAKEY1, TAKEY2, TADESC, TARIF2) VALUES
('1 ', 'RILAV', 'Y', 'SBLOC', 'SOK', 'Sblocco commerciale/evasione', ''),


INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TARIF1, TAFG02, TARIF2) VALUES
 ('1 ', 'ATTAV', 'PV_U_STAB',  'Permessi su stabilimenti', '', '', '')

