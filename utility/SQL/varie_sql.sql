/*********************************** 
CREAZIONE TABELLA LOG 
************************************/
CREATE TABLE qs36f/WPI0LG0 (                                        
	LGID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),                                  
	LGSEID VARCHAR(32),                                                
	LGPAGE VARCHAR(200),                                               
	LGUSER VARCHAR(30),                                                
	LGTIME DECIMAL (9 , 4),                                                                   
	LGDEND TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,                   
	LGBRPT LONG VARCHAR,                                               
	PRIMARY KEY(LGID)                                                  
)       


/*********************************** 
CREAZIONE TABELLA HISTORY LOG 
************************************/
CREATE TABLE qs36f/WPI0LH0 (                                        
	LHID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),                                                                                                                                 
	LHUSER VARCHAR(30),                                                
	LHDATE DATE,                                                                   
	LHCONT INT,                                                                  
	PRIMARY KEY(LHID),
	UNIQUE (LHUSER, LHDATE)	
)       



/*********************************** 
CREAZIONE TABELLA CAPACITA' PRODUTTIVA 
************************************/
CREATE TABLE qs36f/WPI0CP0 (                                        
	CPID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	CPDT   VARCHAR(2),                                  
	CPDATA INT,
	CPRIFE VARCHAR(10),                                                
	CPAREA VARCHAR(5),                                               
	CPTPCO VARCHAR(30),                                                
	CPCAPA INT,                                              
	PRIMARY KEY(CPID)                                                  
)                                                                   




/*********************************** 
CREAZIONE TABELLA COORDINATE GEOGRAFICHE (GMAP) 
************************************/
CREATE TABLE qs36f/WPI0GL2 (                                        
	GLID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	GLLAT  VARCHAR(30),
	GLLNG  VARCHAR(30),
	GLVAL  VARCHAR(1),
	GLADR  VARCHAR(500),
	GLDCR  TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
	GLDTGE INT,
	GLHRGE INT,
	GLUSGE VARCHAR(20),
	GLMANU VARCHAR(1),
	PRIMARY KEY(GLID),
	UNIQUE (GLADR)
)                                                                   

/* creazione da vecchio file */
INSERT INTO qs36f/WPI0GL2(GLLAT,GLLNG,GLVAL,GLADR,GLDCR,GLDTGE,GLHRGE,GLUSGE,GLMANU)
SELECT GLLAT,GLLNG,GLVAL,GLADR,GLDCR,GLDTGE,GLHRGE,GLUSGE,GLMANU
 FROM qs36f/WPI0GL0 GL0 WHERE GLID IN (
	SELECT MAX(GLID) FROM QS36F/WPI0GL0 GLID_2
	GROUP BY GLADR 
 )




/*********************************** 
delete!!!!!!! CREAZIONE TABELLA COORDINATE GEOGRAFICHE (GMAP) 
************************************/
CREATE TABLE qs36f/WPI0GL0 (                                        
	GLID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	GLDT   VARCHAR(2),                                  
	GLTP   VARCHAR(3),
	GLCANA VARCHAR(20),                                                
	GLCDES VARCHAR(9),                                               
	GLLAT  VARCHAR(30),
	GLLNG  VARCHAR(30),
	GLVAL  VARCHAR(1),
	GLADR  VARCHAR(500),
	GLDCR  TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
	GLDTGE INT,
	GLHRGE INT,
	GLUSGE VARCHAR(20),
	GLMANU VARCHAR(1),
	PRIMARY KEY(GLID),
	UNIQUE (GLDT, GLTP, GLCANA, GLCDES)
)                                                                   


/*********************************** 
CREAZIONE TABELLA MEMORIZZAZIONI (F8) 
************************************/
CREATE TABLE qs36f/WPI0DF0 (                                        
	DFID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	DFDT   VARCHAR(2),                                  
	DFMODU VARCHAR(20),
	DFFUNC VARCHAR(20),                                                
	DFNAME VARCHAR(50),                                               
	DFMEMO LONG VARCHAR,
	PRIMARY KEY(DFID),
	UNIQUE (DFDT, DFMODU, DFFUNC, DFNAME)
)                                                                   


/*********************************** 
CREAZIONE TABELLA HELP 
************************************/
CREATE TABLE qs36f/WPI0HE0 (                                        
	HEID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),                                  
	HEMODU VARCHAR(20),
	HEFUNC VARCHAR(20),                                                                                               
	HEMEMO LONG VARCHAR,
	PRIMARY KEY(HEID),
	UNIQUE (HEMODU, HEFUNC)
)

/******************************************* 
CREAZIONE TABELLA CLASSE GIACENZE/CONSUMI 
*******************************************/
CREATE TABLE qs36f/WPI0WS0 (                                        
	WSID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),                                  
	WSSEID VARCHAR(32),
	WSTP VARCHAR(2),
	WSDT VARCHAR(2),
	WSRFG1 VARCHAR(15),
	WSTIPO VARCHAR(2),	
	WSCLS1 VARCHAR(5),                                                
	WSCLS2 VARCHAR(5),
	WSSGIA INT,
    WSSCON INT,
	WSPGIA INT,
	WSPCON INT,
	WSPROG INT,	
	WSMEMO LONG VARCHAR,		
	WSTSCR TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,                   
  	PRIMARY KEY(WSID)                                                  
)

CREATE INDEX QS36F/WPI0WSS ON QS36F/WPI0WS0 (WSSEID)   





/********************************************************* 
CREAZIONE TABELLA GESTIONE ORARI DISPONIBILI (DORA) 
**********************************************************/
CREATE TABLE qs36f/WPI1OD0 (                                        
	ODID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	ODTP   VARCHAR(1),
	ODDT   VARCHAR(2), 
	ODTRIS VARCHAR(10),
	ODCRIS VARCHAR(10),
	ODSTAB VARCHAR(10),
	ODAREA VARCHAR(10),
	ODPORT VARCHAR(10),
	ODGGWK INT,
	ODDTIN INT,
	ODHMIN INT,
	ODDTFI INT,
	ODHMFI INT,
	ODINDI VARCHAR(1),
	PRIMARY KEY(ODID)
)                                                                   





/*********************************** 
CREAZIONE TABELLA ATTIVITA' UTENTI 
************************************/
CREATE TABLE qs36f/WPI0AU0 (                                        
	AUID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	AUDT   VARCHAR(2),      /* ditta */                        
	AUTOPE VARCHAR(15),		/* operazione */
	AUMODU VARCHAR(20),     /* modulo */                                       
	AUKEY1 VARCHAR(50),		/* chiave per tipo operazione */                                               
	AUTSCR TIMESTAMP		/* ts creazione */
		WITH DEFAULT CURRENT TIMESTAMP,
	AUUSGE VARCHAR(20),		/* utente generazione */
	AUMEMO LONG VARCHAR,	
	PRIMARY KEY(AUID)
)


/*********************************** 
CREAZIONE TABELLA NOTE 
************************************/
CREATE TABLE qs36f/WPI0NT0 (                                        
	NTID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	NTDT   VARCHAR(2),      /* ditta */                        
	NTTPNO VARCHAR(5),		/* blocco */
	NTKEY1 VARCHAR(50),     /* chiave1 */                                       
	NTKEY2 VARCHAR(20),     /* chiave2 */                                               
	NTSEQU INT,				/* sequenza */
	NTTSCR TIMESTAMP		/* ts creazione */
		WITH DEFAULT CURRENT TIMESTAMP,
	NTUSGE VARCHAR(20),		/* utente generazione */
	NTMEMO LONG VARCHAR,	
	PRIMARY KEY(NTID)
)
CREATE INDEX QS36F/WPI0NT1 ON QS36F/WPI0NT0 (NTDT, NTTPNO, NTKEY1, NTKEY2, NTSEQU)






/*********************************** 
CREAZIONE TABELLA HISTORY AS 
************************************/
CREATE TABLE qs36f/WPI0AH0 (                                        
	AHID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	AHDT   VARCHAR(2),      /* ditta */
	AHIDPR INT,				/* ID progressivo a cui si fa riferimento - da AS0 */
	AHATES VARCHAR(5),      /* Attivita' eseguita: 
							es: VIS (Visualizzazione), RIL (Rilascio), PRC (Presa in carico) .... */ 
	AHUSGE VARCHAR(20),		/* utente esecuione */	
	AHDTGE INT,				/* data esecuzione  */
	AHHMGE INT,				/* ora esecuzione   */
	PRIMARY KEY(AHID)
)
CREATE INDEX QS36F/WPI0AH1 ON QS36F/WPI0AH0 (AHDT, AHIDPR)


CREATE TABLE qs36f/WPI2AH0 (                                        
	AHID INT NOT NULL GENERATED ALWAYS AS IDENTITY                     
	  (START WITH 1, INCREMENT BY 1),
	AHDT   VARCHAR(2),      /* ditta */
	AHIDPR INT,				/* ID progressivo a cui si fa riferimento - da AS0 */
	AHATES VARCHAR(5),      /* Attivita' eseguita: 
							es: VIS (Visualizzazione), RIL (Rilascio), PRC (Presa in carico) .... */ 
	AHUSGE VARCHAR(20),		/* utente esecuione */	
	AHDTGE INT,				/* data esecuzione  */
	AHHMGE INT,				/* ora esecuzione   */
	PRIMARY KEY(AHID)
)
CREATE INDEX QS36F/WPI2AH1 ON QS36F/WPI2AH0 (AHDT, AHIDPR)




/*************************************************
   RIORGANIZZAZIONE FILE DI LOG 
 *************************************************/

/* insert in file log history */
INSERT INTO QS36F/WPI0LH0(LHUSER, LHDATE, LHCONT)               
 SELECT lguser, date(lgdend), count(*)                          
 from qs36f/wpi0lg0                                             
 where date(lgdend) < current date - 1 DAYS                     
 AND ( date(lgdend) > (select max(lhdate) from qs36f/wpi0lh0)   
       or (select max(lhdate) from qs36f/wpi0lh0) is null)      
 group by lguser, date(lgdend)                                  
 order by date(lgdend), lguser
 
/* pulizia file log */
DELETE FROM QS36F/WPI0LG0
 where date(lgdend) < current date - 3 DAYS
 
/* RIORGANIZZAZIONE DEL FILE (aggiunta nella schedulazione del sabato) */
 RGZPFM FILE(QS36F/WPI0LG0)
 


/* MODIFICHE A CHIAVI NT0 */
alter table qs36f/wpi0nt0 alter column NTMEMO SET DATA TYPE VARCHAR(32000)                                                
alter table qs36f/wpi0nt0 alter column ntkey1 SET DATA TYPE VARCHAR(50)                                                   

UPDATE QS36F.WPI0GC0 
SET GCCDFI = UPPER(GCCDFI),
	GCNOME = UPPER(GCNOME),
	GCCOGN = UPPER(GCCOGN),
	GCINDI = UPPER(GCINDI)
WHERE HEX(GCCDFI) != HEX(UPPER(GCCDFI))
	OR HEX(GCNOME) != HEX(UPPER(GCNOME))
	OR HEX(GCCOGN) != HEX(UPPER(GCCOGN)) 
	OR HEX(GCINDI) != HEX(UPPER(GCINDI))
