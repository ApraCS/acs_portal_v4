/* PROFILI */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES('1 ', 'PRAC', 'AMM', 'Amministratore')
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES('1 ', 'PRAC', 'SEC', 'Security Administrator')


/* AMMINISTRATORE */
INSERT INTO QS36F/WPI0UT0(UTCUTE, UTDESC,UTPSW,UTMAIL,UTTPUT,UTPRAC,UTFLUT) VALUES
 ('ACS', 'Amministratore ACS', 'acsgps', 'm.arosti@apracomputersystem.it', 'AMM', 'AMM', 'T'),
 ('ACS_SE', 'Amministratore SE', 'acs_se', 'm.arosti@apracs.it', 'SECAM', 'SEC', '')
 
 


/* - MERCA - Mercato o Canale di vendita (attributo aggragazione ordine - WPI0TD0/TDCAVE) */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES('1 ', 'MERCA', 'RT', 'Retail')
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES('1 ', 'MERCA', 'CN', 'Contract')
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES('1 ', 'MERCA', 'ST', 'Stock')



/* NAZIO - Nazioni (partendo dalle solo presenti su WPI0TD0 */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC)
SELECT '1 ', 'NAZIO', TDNAZI, TDDNAZ
 FROM QS36F/WPI0TD0                 
 WHERE tdnazi <> ''                 
 GROUP BY TDNAZI, TDDNAZ      
 
 
 
 /* - Modalit� inserimento anagrafica clienti */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAFG01, TAUSGE, TADTGE) VALUES('1 ', 'MODAN', 'ITA', 'Cliente Italia ', 'I', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAFG01, TAUSGE, TADTGE) VALUES('1 ', 'MODAN', 'ECE', 'Cliente Estero CEE ', 'E', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAFG01, TAUSGE, TADTGE) VALUES('1 ', 'MODAN', 'ECX', 'Cliente Estero EXTRACEE  ', 'X', 'QPGMR', 20160101)


/* - Tipologia cliente - CUS3 */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC01', 'IND', 'Distribuzione tradizionale ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC01', 'DIR', 'Grande distribuzione', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC01', 'EST', 'Cliente Estero ', 'QPGMR', 20160101)

/* - Tipologia VARIANTI - variab. ?CC */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC02', 'P02', 'Clienti c/pla.PERS.STOSA-BATT.   ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC02', 'P01', 'Clienti c/placche PERS.STOSA ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC02', 'E01', 'Clienti c/standard elettr.EU   ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC02', 'E02', 'Clienti c/standard elettr.USA ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC02', 'I03', 'Clienti con imballo RINF.SPEC. ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANC02', 'I02', 'Clienti con imballo RINFORZATO ', 'QPGMR', 20160101)

/* - Nomenclatura condizioni commerciali BCCO */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TAKEY2, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANCCO', 'ITA', 'STD', 'Condizioni commerciali anagrafica cliente ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TAKEY2, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANCCO', 'ITA', 'CON', 'Contributo expo ( no stand )   ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TAKEY2, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANCCO', 'ITA', 'EXP', 'Pagamento expo ( con stand )  ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TAKEY2, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANCCO', 'ECE', 'STD', 'Condizioni commerciali anagrafica cliente ', 'QPGMR', 20160101)
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TAKEY2, TADESC, TAUSGE, TADTGE) VALUES('1 ', 'ANCCO', 'ECX', 'STD', 'Condizioni commerciali anagrafica cliente ', 'QPGMR', 20160101)       
 
 /* Decodifica parametri CERVED */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TAMAIL, TADESC) VALUES
   ('1 ', 'CRVDS', 'A01', 'companyName', 'Denominazione')
 , ('1 ', 'CRVDS', 'A02', 'street', 'Indirizzo')
 , ('1 ', 'CRVDS', 'A03', 'postCode', 'Cap')
 , ('1 ', 'CRVDS', 'A04', 'municipality', 'Localit�')
 , ('1 ', 'CRVDS', 'A05', 'province', 'Provincia')
 , ('1 ', 'CRVDS', 'A06', 'country', 'Nazione')
 , ('1 ', 'CRVDS', 'A07', 'companyId', 'ID Cerved')
 , ('1 ', 'CRVDS', 'A08', 'activityStatus', 'Stato')
 , ('1 ', 'CRVDS', 'A09', 'companyType', 'Tipo')
 , ('1 ', 'CRVDS', 'A10', 'companyForm', 'Forma')
 , ('1 ', 'CRVDS', 'A11', 'registrationDate', 'Registrazione')
 , ('1 ', 'CRVDS', 'A12', 'ateco07', 'Attivit�')
 , ('1 ', 'CRVDS', 'A13', 'reaCode', 'Rea')
 , ('1 ', 'CRVDS', 'A14', 'taxCode', 'Cod.Fisc.')
 , ('1 ', 'CRVDS', 'A15', 'vatRegistrationNo', 'P.Iva')
 , ('1 ', 'CRVDS', 'B01', 'telephone', 'Telefono')
 , ('1 ', 'CRVDS', 'B02', 'email', 'Email')
 , ('1 ', 'CRVDS', 'B03', 'certifiedEmail', 'Indirizzo PEC')
 , ('1 ', 'CRVDS', 'B04', 'activityStartDate', 'Inizio attivit�')
 , ('1 ', 'CRVDS', 'B05', 'paidUpEquityCapital', 'Capitale versato')
 , ('1 ', 'CRVDS', 'B06', 'sales', 'Fatturato')
 , ('1 ', 'CRVDS', 'B07', 'grossProfit', 'MOL')
 , ('1 ', 'CRVDS', 'B08', 'profitAndLoss', 'Utile/Perdita')
 , ('1 ', 'CRVDS', 'B09', 'numberOfEmployes', 'Organico')
 , ('1 ', 'CRVDS', 'C01', 'negativeEventsGrading', 'Eventi negativi')
 , ('1 ', 'CRVDS', 'C02', 'economicalFinancialGrading', 'Situazione econ./finanz.')
 , ('1 ', 'CRVDS', 'C03', 'paylineGrading', 'Tempi di pagamento')
 , ('1 ', 'CRVDS', 'C04', 'cervedGroupScore', 'Valutazione Cerved')
 
 
 
  /* Decodifica parametri PUNTI VENDITA */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC, TAFG01, TACOGE) VALUES
   ('6 ', 'MODAP', 'AZI', 'Azienda', 'I', '010000005')
 , ('6 ', 'MODAP', 'PRI', 'Privato', 'I', '010000004')

   /* Domande/riposte protocollazione */
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TAKEY2, TADESC) VALUES
   ('6 ', 'PRVAR', '01', '', 'PIANO')
 , ('6 ', 'PRVAR', '02', '', 'SCALA')
 , ('6 ', 'PRVAR', '03', '', 'CENTRO STORICO')
 , ('6 ', 'PRVAR', '04', '', 'ASCENSORE') 
 , ('6 ', 'PRVAR', '05', '', 'MISURE A CURA DEL')
 , ('6 ', 'PRVAR', '06', '', 'FATTURA') 
 , ('6 ', 'PRVAN', '01', '01', 'Primo piano') 
 , ('6 ', 'PRVAN', '01', '02', 'Secondo piano') 
 , ('6 ', 'PRVAN', '01', '03', 'Terzo piano') 
 , ('6 ', 'PRVAN', '02', 'N', 'No scala') 
 , ('6 ', 'PRVAN', '02', 'S', 'Con scala') 
 , ('6 ', 'PRVAN', '03', 'N', 'No centro storico') 
 , ('6 ', 'PRVAN', '03', 'S', 'Si centro storico')
 , ('6 ', 'PRVAN', '04', 'N', 'Senza ascensore') 
 , ('6 ', 'PRVAN', '04', 'S', 'Con ascensore')
 , ('6 ', 'PRVAN', '05', 'CL', 'Misure a cura del cliente')
 , ('6 ', 'PRVAN', '06', 'S', 'Con fattura') 

 
 
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES
   ('1 ', 'SACAR', 'C', 'Pagamento completo')
 , ('1 ', 'SACAR', 'N', 'Pagamento non effettuato')
 , ('1 ', 'SACAR', 'P', 'Pagamento parziale')

/* tipo pagamento caparra*/ 
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES
   ('6 ', 'TPCAP', 'CON', 'Contanti')
 , ('6 ', 'TPCAP', 'RIM', 'Rimessa')
 , ('6 ', 'TPCAP', 'BON', 'Bonifico')
 
/* stato di riga MTO */ 
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES
   ('6 ', 'MTOST', '1', 'Controllato')
 , ('6 ', 'MTOST', '2', 'Confermato')
 

 
INSERT INTO QS36F/WPI0TA0(TADT, TATAID, TAKEY1, TADESC) VALUES
   ('1 ', 'CCT06', '', '- Vuoto -')
 , ('1 ', 'CCT06', 'N', 'No')
 , ('1 ', 'CCT06', 'S', 'Si')
 