<?php

//require_once("function_php7.php"); //va gestito nel file di configurazione
require_once("function_extjs.php");


function ditta_default($change_to_id_ditta_default = null){
    
    if (!is_null($change_to_id_ditta_default))
        $_SESSION['id_ditta_default'] = $change_to_id_ditta_default;
    
    if (!isSet($_SESSION['id_ditta_default'])){
        global $id_ditta_default; //recupero default da config
        $_SESSION['id_ditta_default'] = $id_ditta_default;
    }
    return $_SESSION['id_ditta_default'];
}


function site_gmap_key(){
    global $gmap_api_key; //eventualmente definito su config.inc.php
    if (!isset($gmap_api_key))
        $gmap_api_key = 'AIzaSyBj5Q7l5eIx16MWJQt8eUwzAn8-eFgwSs0';
    return $gmap_api_key;
}


function get_by_rrn($rrn, $table){
	global $conn;
	$sql = "SELECT * FROM {$table} T WHERE RRN(T)=?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($rrn));
	return db2_fetch_assoc($stmt);
}



function mese_short($n){
    switch ($n) {
        case 1: return 'Gen';
        case 2: return 'Feb';
        case 3: return 'Mar';
        case 4: return 'Apr';
        case 5: return 'Mag';
        case 6: return 'Giu';
        case 7: return 'Lug';
        case 8: return 'Ago';
        case 9: return 'Set';
        case 10: return 'Ott';
        case 11: return 'Nov';
        case 12: return 'Dic';
        
        
        default: return 'Non def';
            
    }
}


function write_textfield_std($name, $fieldLabel, $value, $maxLength = 0, $p = array()){
	if (isset($p['flex_width']))
		$flex_width = $p['flex_width'];
	else
		$flex_width = "";

	?>
		{
			xtype: 'textfield'
			, name: <?php echo j($name); ?>		
			, fieldLabel: <?php echo j($fieldLabel); ?>
			, value: <?php echo j($value); ?>
			
			<?php if (isset($p['labelAlign'])) { ?>, labelAlign: <?php echo j($p['labelAlign']) ?><?php } ?>	
			<?php if (isset($p['labelWidth'])) { ?>, labelWidth: <?php echo $p['labelWidth'] ?><?php } ?>						
			<?php if ($maxLength > 0)  { ?>, maxLength: <?php echo j($maxLength) ?><?php } ?>
		}
<?php
}

function out_list_value_from_array($ar, $mostra_conteggio = 'N'){

	$ret = array();
	foreach($ar as $k => $v){
		if($mostra_conteggio == 'Y')
			$ret[] = "{$k} ({$v})";
		else
			$ret[] = $k;
	}

	return implode(',', $ret);
}


function out_checkbox($valore){
	
	if($valore===true){
		echo '[X]';
	}else{
		echo '[ ]';
	}
}

function write_textarea_std($name, $fieldLabel, $value, $maxLength = 0, $p = array()){
	if (isset($p['flex_width']))
		$flex_width = $p['flex_width'];
		else
			$flex_width = "";

			?>
		{
			xtype: 'textareafield'
			, name: <?php echo j($name); ?>		
			, fieldLabel: <?php echo j($fieldLabel); ?>
			, value: <?php echo j($value); ?>
			
			<?php if (isset($p['labelAlign'])) { ?>, labelAlign: <?php echo j($p['labelAlign']) ?><?php } ?>	
			<?php if (isset($p['labelWidth'])) { ?>, labelWidth: <?php echo j($p['labelWidth']) ?><?php } ?>						
			<?php if ($maxLength > 0)  { ?>, maxLength: <?php echo j($maxLength) ?><?php } ?>
		}
<?php
}


function write_numberfield_std($name, $fieldLabel, $value, $maxLength = 0, $p = array()){
	if (isset($p['flex_width']))
		$flex_width = "{$p['flex_width']}";
	else
	    $flex_width = "flex: 1";
	    

	?>
		{
			xtype: 'numberfield'
			, name: <?php echo j($name); ?>		
			, fieldLabel: <?php echo j($fieldLabel); ?>
			, value: <?php echo j($value); ?>
			, hideTrigger:true
			, <?php echo $flex_width ?>
			<?php if (isset($p['labelAlign'])) { ?>, labelAlign: <?php echo j($p['labelAlign']) ?><?php } ?>	
			<?php if (isset($p['labelWidth'])) { ?>, labelWidth: <?php echo j($p['labelWidth']) ?><?php } ?>						
		    , listeners: { 
		    
		        <?php echo $p['list_allowBlank']  ?>
		        
		    }
		    
		}
<?php
}



function write_checkboxgroup_std($name, $fieldLabel, $value, $data, $p = array()){
	if (isset($p['flex_width']))
		$flex_width = $p['flex_width'];
	else
		$flex_width = "flex: 1";

	?>
		{
			xtype: 'checkboxgroup',
			fieldLabel: <?php echo j($fieldLabel) ?>,
			<?php echo $flex_width ?>,
			items: [
				<?php foreach($data as $c_value => $c_label) {  ?>
						{
                            xtype: 'checkbox'
                          , name: <?php echo j($name) ?>
                          , boxLabel: <?php echo j($c_label) ?>
                          , inputValue: <?php echo j($c_value) ?>
                          
                          <?php if ($value == $c_value) { ?> , checked   : true <?php } ?>                          
                        }					
				<?php } ?>
			]						 
		}
<?php
}





function write_combo_std($name, $fieldLabel, $value, $data, $p = array()){
   if (isset($p['flex_width']))
		$flex_width = $p['flex_width'];
	else 
		$flex_width = "flex: 1";
	
	if (isset($p['allowBlank']))
		$allowBlank = $p['allowBlank'];
	else
		$allowBlank = "true";
	
	if (isset($p['margin']))
	    $margin = $p['margin'];
   
	
	if (isset($p['multiSelect']))
	    $multiSelect = "true";
    else
        $multiSelect = "false";
	
	switch ($p['transorm']){
		case "NO":
			$out_value = $value;
			break;
		case "rtrim":
			$out_value = rtrim(acs_u8e($value));
			break;
		default:
			$out_value = trim(acs_u8e($value));
	}

	
?>
{
	name: <?php echo j($name) ?>,
	value: <?php echo j($out_value); ?>,
	<?php echo $flex_width ?>,
	<?php echo $margin ?>
	xtype: 'combo',
	<?php if (isset($p['labelAlign'])) { ?>labelAlign: <?php echo j($p['labelAlign']) ?>,<?php } ?>	
	<?php if (isset($p['labelWidth'])) { ?>labelWidth: <?php echo $p['labelWidth'] ?>,<?php } ?>
	fieldLabel: <?php echo j($fieldLabel) ?>,
	displayField: 'text',
	valueField: 'id',
	anchor: '-10',
	forceSelection:true,
	allowBlank: <?php echo $allowBlank ?>,	
	multiSelect: <?php echo $multiSelect ?>,													
	store: {
		autoLoad: true,
		editable: false,
		autoDestroy: true,	 
	    fields: [{name:'id'}, {name:'text'},  {name:'color'}, {name:'sosp'}, {name:'desc'}],
	    data: [<?php echo $data; ?>] 
	},
	tpl: [
		 '<ul class="x-list-plain">',
            '<tpl for=".">',
            <?php if($p['show_det'] == 'Y'){?>
            '<li class="x-boundlist-item listItmes" style="background-color:{color}">{desc}</li>',
            <?php }else{?>
            '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
            <?php }?>
            
            '</tpl>',
            '</ul>'
		 ],
           // template for the content inside text field
        displayTpl: Ext.create('Ext.XTemplate',
               '<tpl for=".">',
                  '{text}',
               '</tpl>'
                 
                ),
    
	queryMode: 'local',
	minChars: 1,	
    listeners: { 
    	   beforequery: function (record) {
    		record.query = new RegExp(record.query, 'i');
    		record.forceAll = true;
    	},
   		   change: function(combo, newValue, oldValue, eOpts) {
    			var values = combo.store.proxy.reader.jsonData;
    		    for (var i=0; i<values.length; i++) {
    			   if(values[i].id == newValue){
    			   	  if(values[i].sosp == 1){
    			   	  	combo.setValue(combo.originalValue);
    			   	  }
    			   	  <?php if(isset($p['dest_change_list']) && $p['dest_change_list'] == 'Y'){?>
			      
			          		var form = this.up('form').getForm();
      				        Ext.Ajax.request({
        						        url        : 'acs_panel_ins_new_anag.php?fn=get_values_new_dest',
        						        timeout: 2400000,
        						        method     : 'POST',
        			        			jsonData: {
        			        				new_dest : newValue,
        			        				cliente : <?php echo j($p['cliente']) ?>
        								},				
        								success : function(result, request){
        							        var jsonData = Ext.decode(result.responseText);
        							        var rec = jsonData.rec;
        							        form.findField('ind_std').setValue(rec.IND_D);
        					        		form.findField('cap_std').setValue(rec.CAP_D);
        							        form.findField('loc_std').setValue(rec.LOC_D);
							        	    form.findField('pro_std').setValue(rec.PRO_D);
        							       	
        							    },
        						        failure    : function(result, request){
        						            Ext.Msg.alert('Message', 'No data to be loaded');
        						        }
        						    });
			   
			   <?php }?>
    			   	  
    			   	  
    			   }
    			   
			  
    			}
    	
         }
	} 					 
}
<?php
}





function write_datefield_std($name, $fieldLabel, $value, $maxLength = 0, $p = array()){
	if (isset($p['flex_width']))
		$flex_width = $p['flex_width'];
	else
		$flex_width = "";
	
	if (isset($p['allowBlank']))
		$allowBlank = $p['allowBlank'];
	else
		$allowBlank = "false";

	?>
        {
            xtype: 'datefield'
            , name: <?php echo j($name); ?>
            , startDay: 1 //lun.
            , format: 'd/m/Y', submitFormat: 'Ymd'  
            , altFormats: 'Ymd|dmY'      
            , fieldLabel: <?php echo j($fieldLabel); ?>
            , value: '<?php echo $value; ?>'
            , allowBlank: <?php echo j($allowBlank); ?>
           , listeners: {
                           invalid: function (field, msg) {
                           		Ext.Msg.alert('', msg);
                           },
                           
                           change: function(){
                           	<?php echo $p['on_change']; ?>
                           }
                        }            
                        
            <?php if (isset($p['labelAlign'])) { ?>, labelAlign: <?php echo j($p['labelAlign']) ?><?php } ?>
            <?php if (isset($p['labelWidth'])) { ?>, labelWidth: <?php echo j($p['labelWidth']) ?><?php } ?>
            <?php if (isset($p['itemId'])) { ?>, itemId: <?php echo j($p['itemId']) ?><?php } ?>
        }
<?php
}




//per sql query
//genero il nome dei campi in fase di INSERT
function create_name_field_by_ar($ar){
	$ret = array();
	foreach ($ar as $name_field => $value){
		$ret[] = $name_field;
	}
	return implode(',', $ret);
}

function create_parameters_point_by_ar($ar){
	$ret = array();
	foreach ($ar as $name_field => $value){
		$ret[] = '?';
	}
	return implode(',', $ret);
}

//genero il nome dei campi in fase di UPDATE
function create_name_field_by_ar_UPDATE($ar){
	$ret = array();
	foreach ($ar as $name_field => $value){
		$ret[] = $name_field . "=?";
	}
	return implode(',', $ret);
}


//genero il nome dei campi in fase di SELECT
function create_name_field_by_ar_SELECT($ar){
	$ret = array();
	foreach ($ar as $name_field => $value){
		$ret[] = $name_field . "=?";
	}
	return implode(' AND ', $ret);
}



function set_ar_value_if_is_set(&$ar, $fieldName, $v){
    if (isset($v))
        $ar[$fieldName] = $v;
}


function transform_codice_cliente($f_cliente_cod, $cfg_mod){
	if ($cfg_mod['disabilita_sprintf_codice_cliente'] == 'Y')
		return $f_cliente_cod;
	else
		sprintf("%09s", $f_cliente_cod);
}

function make_tab_closable(){
	return 'closable: true';
}

function make_tbar_closable(){
	return ", {iconCls: 'tbar-x-tool x-tool-close', handler: function(event, toolEl, panel){ this.up('panel').close();}}";
}



function site_protocol(){
 return stripos($_SERVER['SCRIPT_URI'],'https') === false ? 'http://' : 'https://';	
}



function prepare_new_mail($from = null){
	////require_once ('utility/emogrifier.php');
	//require_once('utility/PHPMailer-master/PHPMailerAutoload.php');
    global $auth, $smtp_isSMTP, $smtp_address, $smtp_credentials; 
	$mail = new PHPMailer;
	
	
	if (!isset($smtp_isSMTP) || $smtp_isSMTP == true)
		$mail->isSMTP(); //altrimenti usa il servizio intersno (es: sendmail)
	
	$mail->Host = $smtp_address;
	
	if (isset($smtp_credentials)){
	    $mail->SMTPAuth = true;
	    $mail->Username = $smtp_credentials['user'];                // SMTP username
	    $mail->Password = $smtp_credentials['psw'];                  // SMTP password
	} else {
	    $mail->SMTPAuth = false;
	}
	


	if (is_null($from))
	  $from = trim($auth->get_email());

	$mail->From = $from;
	$mail->FromName = trim($auth->out_name());
	$mail->isHTML(true);
	//$mail->ConfirmReadingTo = $from; //parametrizzare		
	
	return $mail;
}


function form_ep_remove_blank_parameters($ar){
	$ret = array();
	foreach($ar as $k => $v){

		if (is_array($v)){
			if (count($v) > 0)
				$ret[$k] = $v;
		} else {
			if (strlen(trim($v)) > 0)
				$ret[$k] = $v;
		}
	}
	return $ret;
}




function array_values_recursive($arr){
 	$new_arr = array();
	if (gettype($arr) == "array"){
		foreach($arr as $key => $val){
			if ($key == "children" && gettype($val) == "array"){
				if ($arr['leaf'] != true){
					$s_arr = array_values($val);
					$s_arr = array_values_recursive_2($s_arr);
					$new_arr[$key] = $s_arr; //array_values_recursive($s_arr);
				}	
			} else $new_arr[$key] = $val;		  
			  
		}
	} else $new_arr = $arr;			
        
  return $new_arr;
}


function array_values_recursive_2($arr){
	$new_arr = array();
	foreach($arr as $key => $val){
		$new_arr[$key] = array_values_recursive($val);		
	}	
 return $new_arr;	
}




function acs_execute($stmt, $ar = array()){
	
	try {
		$result = db2_execute($stmt, $ar);
	} catch (Excetpion $e) {
			echo "catchhhhhhh";
			print_r($stmt);
			echo $e->getMessage();		
	}	
	
		if (!$result){
			echo "cccccccccccc";
			print_r($stmt);
			$al = new AppLog();
			echo db2_stmt_errormsg($stmt);			
			echo db2_stmt_error($stmt);
			$er_msg = db2_stmt_errormsg($stmt);
			echo "\n" . $er_msg;
			$al->write_log("execute"  . $er_msg . "|" . print_r($ar, true));
		}		
	return $result;
}


function acs_prepare($conn, $sql){
	try {
		return db2_prepare($conn, $sql);
	} catch (Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
	}	
}





function cmp_by_TASITI($a, $b)
{
	return strcmp($a["TASITI"], $b["TASITI"]);
}



function my__autoload($classname) {

	
	if ($classname == "Spedizioni"){
		$filename 		= "class/mod.". $classname .".php";		
    	include_once($filename);
    	return;
	}
	if ($classname == "DeskArt"){
		$filename 		= "class/mod.". $classname .".php";
		include_once($filename);
		return;
	}
	if ($classname == "DeskAcq"){
	    $filename 		= "class/mod.". $classname .".php";
	    include_once($filename);
	    return;
	}
	if ($classname == "DeskUtility"){
		$filename 		= "class/mod.". $classname .".php";
		include_once($filename);
		return;
	}
	if ($classname == "DeskStats"){
		$filename 		= "class/mod.". $classname .".php";
		include_once($filename);
		return;
	}	
	if ($classname == "DeskGest"){
		$filename 		= "class/mod.". $classname .".php";
		include_once($filename);
		return;
	}
	if ($classname == "DeskPVen"){
		$filename 		= "class/mod.". $classname .".php";
		include_once($filename);
		return;
	}
	if ($classname == "DeskProd"){
	    $filename 		= "class/mod.". $classname .".php";
	    include_once($filename);
	    return;
	}
	if ($classname == "DeskTranslations"){
	    $filename 		= "module/desk_trans/mod.". $classname .".php";
	    include_once($filename);
	    return;
	}
	
	//Per Phpmyailer
	//$filenamePhpMyailer = dirname(__FILE__).DIRECTORY_SEPARATOR.'/PHPMailer-master/'.'class.'.strtolower($classname).'.php';
	$filenamePhpMyailer = dirname(__FILE__).DIRECTORY_SEPARATOR.'/PHPMailer-master/'.'class.'.strtolower($classname).'.php';
	if (is_readable($filenamePhpMyailer)) {
		require $filenamePhpMyailer;
		return;
	}	
	
	
    $filename = "class/class.". $classname .".php";
    if (file_exists(ROOT_ABS_PATH . $filename))
    	include_once($filename);
    else {
    	//$filename = "module/{$classname}/". $classname .".php";
    	//corretto problema su linux (case sensitive)
    	$filename = "module/" . strtolower($classname) . "/". $classname .".php";    	
    	include_once($filename);    	    	
    }	
}
spl_autoload_register('my__autoload');


 function next_num($cod, $table){ 	
 		global $conn;
		
		$sql = "SELECT * FROM {$table} WHERE NRDT='1' AND NRCOD='$cod'";
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		
		if ($row = db2_fetch_assoc($stmt)){
			$n = $row['NRPROG'] +1;
			$sql = "UPDATE {$table} SET NRPROG=$n WHERE NRDT='1' AND NRCOD='$cod'";
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);			
		} else {			
			$n = 1;
			$sql = "INSERT INTO {$table}(NRDT, NRCOD, NRPROG)  VALUES('1', '$cod', $n)";
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);			
		}
		
	return $n; 		
 }


 
 
 
 function find_TA_users(){
 	//elenco possibili utenti destinatari
 	$user_to = array();
 	$users = new Users;
 	$ar_users = $users->find_all();
 	
 	foreach ($ar_users as $ku=>$u)
 		$user_to[] = array(
 				"id"  => trim($u['UTCUTE']),
 				"text" => trim($u['UTDESC']) . " (" .  trim($u['UTCUTE']) . ")"
 			);


 	return $user_to;
 	//$ar_users_json = acs_je($user_to); 	

 }

 function find_TA_std($tab, $k1 = null, $livelli_k = 1, $aspe = null, $tarif1 = null, $tarif2 = null, $from_class = null, $filtri= array()){
 		global $auth, $conn, $cfg_mod_Spedizioni, $id_ditta_default;
		$ar = array(); 
		if (!is_null($from_class)){
			$m_tabella_TA = $from_class->get_table_TA();
		} else {
			global $cfg_mod_Admin;			
			$m_tabella_TA = $cfg_mod_Admin['file_tabelle'];
		}
		
		if (strlen($auth->get_cod_riservatezza()) > 0){
			//add_riservatezza
			$join_riservatezza = find_TA_std_add_riservatezza($tab);
		} else {
			$join_riservatezza = '';
		}
		
		$sql_WHERE = '';
		
		$sql_WHERE.= sql_where_by_combo_value('TD.TDCDIV', $filtri['divisione']);
		
		
		//Se sono in punto vendita, filtro per stabilimento
		global $cod_mod_provenienza;
		if ($tab=='START' && $cod_mod_provenienza == 'DESK_PVEN'){
			//eventuale filtro per stabilimento
			$dpv = new DeskPVen(array('no_verify' => 'Y'));
			$stab_assegnati = $dpv->get_stabilimenti_assegnati();
			if (count($stab_assegnati) > 0)
				$sql_WHERE .= " AND TAKEY1 IN (" . sql_t_IN($stab_assegnati) . ")";
		}
		
		//ToDo: join con TD solo se necessario
		$sql = "SELECT DISTINCT TA.* 
				FROM {$m_tabella_TA} TA {$join_riservatezza}
		        /*LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD 
		        ON TA.TADT=TD.TDDT AND TA.TAKEY1=TD.TDCLOR*/
				WHERE TA.TADT='$id_ditta_default' AND TA.TATAID = '$tab' {$sql_WHERE}	
			   ";

		if (isset($k1)){
				$k1 = trim($k1);		
				$sql .= " AND TAKEY1 = '$k1' ";
		}
		
		if (isset($aspe)){
			$aspe = trim($aspe);
			$sql .= " AND TAASPE = " . sql_t($aspe);
		}		
		
		if (isset($tarif1)){
			$tarif1 = trim($tarif1);
			$sql .= " AND TARIF1 = " . sql_t($tarif1);
		}		
 		if (isset($tarif2)){
			$tarif2 = trim($tarif2);
			$sql .= " AND TARIF2 = " . sql_t($tarif2);
		}		
				
		$sql .= " ORDER BY TADESC ";		

        /*print_r($sql);
        exit;*/
		
		$stmt = db2_prepare($conn, $sql);		
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
				
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {

		 if ($livelli_k == 1) $m_id = $row['TAKEY1'];		 
		 if ($livelli_k == 5) $m_id = implode('|', array($row['TAKEY1'], $row['TAKEY2'], $row['TAKEY3'], $row['TAKEY4'], $row['TAKEY5']));
			
		 $ret[] = array("id" 		=> rtrim($m_id),
		                "text" 		=> rtrim($row['TADESC']),
		 				"TATISP"	=> $row['TATISP'],
		 				"TATITR"	=> $row['TATITR'],		 		
		 				"TAVOLU"	=> $row['TAVOLU'],
		 				"TAPESO"	=> $row['TAPESO'],
		 				"TABANC"	=> $row['TABANC'],
		                "TASITI"	=> $row['TASITI'],
		 				"TAMAIL"	=> acs_u8e($row['TAMAIL'])			 				 				 		
		  );	
		}		
	return $ret;	
 }

 
 
 function find_TA_std_add_riservatezza($tab){
 	global $auth, $cfg_mod_Spedizioni;
	$s = new Spedizioni(); 
 	$ret = '';
 	
 	switch($tab){
 		case "ASPE":
 			 $ret = " INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
						ON TA_ITIN.TATAID='ITIN' AND TA_ITIN.TADT=TA.TADT AND TA.TAKEY1 = TA_ITIN.TAASPE
					  INNER JOIN  {$cfg_mod_Spedizioni['file_testate']} TD
					    ON TA_ITIN.TADT = TD.TDDT AND TA_ITIN.TAKEY1=TD.TDCITI					    
 			 		" . $s->add_riservatezza();	
 			 break;
 		case "START":
 		 	$ret = "INNER JOIN  {$cfg_mod_Spedizioni['file_testate']} TD
 		 				ON TA.TADT = TD.TDDT AND TA.TAKEY1=TD.TDSTAB
 		 			" . $s->add_riservatezza();
 			 	break;
 		case "ITIN":
 			$ret = "INNER JOIN  {$cfg_mod_Spedizioni['file_testate']} TD
 			 		ON TA.TADT = TD.TDDT AND TA.TAKEY1=TD.TDCITI
 		 		" . $s->add_riservatezza();
 			break;
 		case "TIPOV":
 			$ret = "INNER JOIN  {$cfg_mod_Spedizioni['file_testate']} TD
 			 		ON TA.TADT = TD.TDDT AND TA.TAKEY1=TD.TDCLOR
 		 		" . $s->add_riservatezza();
 			break; 			 	 			 
 		default:
 			$ret = ' ERRORE: FILTRO RISERVATEZZA NON DEFINITO !!!!! ';
 			$ret = '';
 			break;
 	}
 	 	
	return $ret; 	
 }
 
 
 
 function desc_TA_std($tab, $k1){
 	$r = find_TA_std($tab, $k1);
	if (!empty($r))
		return $r[0]["text"];
	else {
		return "";
	}
 }

 
 
 
 function find_TA_sys($tab, $k1 = null, $tacor1 = null, $tacor2 = null, $divisione = null, $filtra_codici = null, $da_tarest = 0, $add_where = '', $mostra_codice = 'N', $includi_sopsesi = 'N', $trim_id = false, $record_esteso = false){
 	global $conn, $id_ditta_default;
 	global $cfg_mod_Admin, $backend_ERP;
 	$ar = array();
 	$ret = array();
 	
 	if($includi_sopsesi == 'N')
 	     $add_where .= " AND TATP <> 'S' ";
 	
	 	$sql = "SELECT *
	 			FROM {$cfg_mod_Admin['file_tab_sys']}
	 			WHERE TADT = '$id_ditta_default' AND TAID = '$tab' AND TALINV=''
	 		    {$add_where}";
	 	
	 
	 	if (isset($k1)){
	 	  if (is_string($k1))	$k1 = trim($k1);
	 	  if ($k1 == '')
	 	      $sql .= " AND TANR = '$k1' ";
	 	  else
	 	     $sql .= sql_where_by_combo_value('TANR', $k1);
	 	}

	 	if (isset($tacor1)){
	 	    if (is_string($tacor1))	$tacor1 = trim($tacor1);
	 	    if ($tacor1 == '')
	 	        $sql .= " AND TACOR1 = '$tacor1' ";
	 	    else 
	 	         $sql .= sql_where_by_combo_value('TACOR1', $tacor1);
	 	} 	
	 	
	 	if (isset($tacor2)){
	 	    if (is_string($tacor2))	$tacor2 = trim($tacor2);
	 	    if ($tacor2 == '')
	 	        $sql .= " AND TACOR2 = '$tacor2' ";
	 	    else
	 	        $sql .= sql_where_by_combo_value('TACOR2', $tacor2);
	 	} 	
	 	
	 	if (isset($divisione) && $divisione != '*ALL'){
	 		$tacor2 = trim($tacor2);
	 		$sql .= " AND SUBSTR(TAREST, 117, 3) = '{$divisione}' ";
	 	} 	
	 
	 	if (isset($filtra_codici)){
	 		$sql .= " AND TANR IN (" . sql_t_IN($filtra_codici) . ")";
	 	}
	 	
	 	
	 	$sql .= " ORDER BY TATP, TADESC ";
	 	
	 	$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		
			
		while ($row = db2_fetch_assoc($stmt)) {
		    
		    $sospeso = false;
		    $color = "";
		    
		    if ($mostra_codice == 'Y')
		        $text = "[". trim($row['TANR']) ."] ". acs_u8e(trim($row['TADESC']));  
		    else
		        $text = acs_u8e($row['TADESC']);
		    
		        
		    if($includi_sopsesi == 'Y' && trim($row['TATP']) == 'S'){
		        $sospeso = true;
		        $color = "#F9BFC1";
		       
		    }
		        
		        
		        
		    
		        $rr = array( "id" 		=> acs_u8e($row['TANR']),
		                     "text" 	=> $text,
		                     "sosp"    => $sospeso,
		                     "color"   => $color,		            
		        );
		        
		        if ($record_esteso){
		            //AGGIUNGERE ANCHE GLI ALTRI CAMPI
		            $rr['TACOR2'] = trim($row['TACOR2']);
		            $rr['TADES2'] = trim($row['TADES2']);
		        }
		    
		    
		    if ($trim_id == 'Y') $rr['id'] = trim($rr['id']);
		    if ($trim_id == 'R') $rr['id'] = rtrim($rr['id']);
		    
		    $rr['tacint'] = trim($row['TACINT']);
	 					
	 		if ($da_tarest > 0)
	 		    $rr['tarest'] = acs_u8e(trim(substr($row['TAREST'], 0, $da_tarest)));
	 		
	 		if(trim($row['TAID']) == 'PUVN')
	 		    $rr['esau'] = substr($row['TAREST'], 127, 1);
	 		
 		    if(trim($row['TAID']) == 'VUDE'){
 		        $rr['desc'] = $text. ", " .
 		  		              acs_u8e(substr($row['TAREST'],  30,  60)). ", ".
 		  		              acs_u8e(substr($row['TAREST'],  90,  60)). "(" .
 		  		              acs_u8e(substr($row['TAREST'],  160,  2)). ")" ;
 		        
                $rr['out_ind_dest'] = acs_u8e(substr($row['TAREST'],  30,  60));
                $rr['out_loc_dest'] = acs_u8e(substr($row['TAREST'],  90,  60));
 		       
 		    
 		    }
	 		
	 	    
	 					
	 		$ret[] = $rr;		 						
	 	}
	
 return $ret;
 }
 
 
 
 function get_TA_sys($tab, $k1 = null, $tacor1 = null, $tacor2 = null, $divisione = null, $filtra_codici = null, $da_tarest = 0, $add_where = '', $mostra_codice = 'N', $includi_sopsesi = 'Y', $trim_id = 'Y', $record_esteso = true){
    $ar_ret = find_TA_sys($tab, $k1, $tacor1, $tacor2, $divisione, $filtra_codici, $da_tarest, $add_where, $mostra_codice, $includi_sopsesi, $trim_id, $record_esteso);
     
    if (count($ar_ret) > 0)
        return $ar_ret[0];
    else
        return false;
 }
 
 
 
 		
function explode_if_isset($del, $v){
	if (isset($v))
		return explode($del, $v);
	else
		return null;
} 		
 	

function acs_ar_to_object($ar){
    return json_decode(json_encode($ar, JSON_FORCE_OBJECT), false);
}

function acs_object_to_ar($obj){
    return (array)$obj;
}


function acs_ar_to_json($ar, $aggiungi_codice = 'N', $after_before = 'B'){

	$ret = array();

	
	foreach ($ar as $k => $v){

		if ($aggiungi_codice == 'Y'){
			if($after_before == 'B')
				$v = "[{$k}] {$v}";
			if($after_before == 'A')
				$v = "{$v} [{$k}]";
		}
 		
		$ret[] = array('id' => $k, 'text' =>$v);
 

	}

	return  acs_je($ret);
}

 

function acs_ar_to_select_json($ar, $name_tag = "options", $to_trim = true, $aggiungi_vuoto = 'N', $aggiungi_blank = 'N', $aggiungi_codice = 'N'){
	
  $ret = "";

  if($aggiungi_vuoto == 'Y' && count($ar) > 0){
  	$vuoto = array('id' => null, 'text' => ' - rimuovi selezione - ');
  	$ret .= acs_je($vuoto) . ",";
  }
  if($aggiungi_blank == 'Y' && count($ar) > 0){
  	$blank = array('id' => '', 'text' => ' - vuoto - ');
  	$ret .= acs_je($blank) . ",";
  }  
  
  if (strlen($name_tag) > 0 ) $ret .= "{\"$name_tag\":[";
	foreach ($ar as $r){
		
		if ( $to_trim === true )
			$r = array_map('trim', $r);
		
		if ( $to_trim == 'R' )
			$r = array_map('rtrim', $r);
				

		if ($aggiungi_codice == 'Y')
			$r['text'] = "[{$r['id']}] {$r['text']}";

		$ret .= acs_je($r) . ",";		
				
	}
  if (strlen($name_tag) > 0 ) $ret .= "]}";
  return $ret;	
}


function logo_path(){
	return '"' . ROOT_PATH .  'personal/logo_cliente.png"';
}

function logo_report_path(){
	return '"' . ROOT_PATH .  'personal/logo_cliente.png"';
}

function img_path($img){
	return '"' . IMAGE_PATH . $img . '"';
}

function acs_url($url = ""){
	return '"' . ROOT_PATH . $url . '"';
}

function acs_module_url($module, $page, $fn = null){
    $ret = ROOT_PATH . "module/{$module}/{$page}.php?";
    if (!is_null($fn)) $ret .= "fn={$fn}";
    return '"' . $ret . '"';
}


function print_month($month, $format = '%B'){
	$oldLocale = setlocale(LC_TIME, 'it_IT');
	$dd = strftime($format, strtotime("{$month}/01/2015"));
	setlocale(LC_TIME, $oldLocale);
	return $dd;
}

//OLD VERSION
function print_date($data, $format="%d/%m/%y"){
  if (strlen(trim($data))==0 || $data=="0") return "";
  
  if ($data == "99999999" || $data == "20991231"){
      if ($format == "%Y%m%d")
        return 20991231;
      else
        return "31/12/2099";
  }
  
	$oldLocale = setlocale(LC_TIME, 'it_IT');
	$dd = strftime($format, strtotime($data)); 
	setlocale(LC_TIME, $oldLocale);
	return $dd;
}

function print_date2($data, $format="d/m/Y"){
    if (strlen(trim($data))==0 || $data=="0") return "";
    
    if ($data == "99999999" || $data == "20991231"){
        if ($format == "Ymd")
            return 20991231;
        else
            return "31/12/2099";
    }
    
    $oldLocale = setlocale(LC_TIME, 'it_IT');
    $date = new DateTime($data);
    $dd = $date->format($format);
    setlocale(LC_TIME, $oldLocale);
    return $dd;
}

function print_date_bl($data, $format="%d/%m/%y"){
	if (strlen(trim($data))==0 || $data=="0") return "";
	
	$name_days = array(
			1 => 'Luned&igrave;/Monday',
			2 => 'Marted&igrave;/Tuesday',
			3 => 'Mercoled&igrave;/Wednesday',
			4 => 'Gioved&igrave;/Thursday',
			5 => 'Venerd&igrave;/Friday',
			6 => 'Sabato/Saturday',
			7 => 'Domenica/Sunday'
	);
	
	$day_of_week = strftime("%u", strtotime($data));

	$dd = strftime("{$name_days[$day_of_week]} - %d/%m/%y", strtotime($data));
	return $dd;
}




function print_ora($ora, $len = 6, $midnight = 'N'){
  $ora = trim($ora);
  if ($midnight != 'Y' && (strlen($ora)==0 || $ora==0)) return "";


  if ($len == 4) //formato hm
  	$ora = sprintf("%04s", $ora);
  else //di default formato hms
  	$ora = sprintf("%06s", $ora);  	
  
  if (strlen($ora) == 6)
  	return substr($ora, 0, 2) . ":" . substr($ora, 2,2);
  if (strlen($ora) == 5)
  	return substr($ora, 0, 1) . ":" . substr($ora, 1,2);  
  if (strlen($ora) == 4)
    return substr($ora, 0, 2) . ":" . substr($ora, 2,2);
  if (strlen($ora) == 3)
    return substr($ora, 0, 1) . ":" . substr($ora, 1,2);  
}


function db2_ts_to_datetime($ts, $format_data = "%d/%m/%y"){
	
	//potrei averlo nel formato 2014-06-26-09.30.24.23466 oppure 2014-06-26 09:30:20.23466
	$tse = preg_split("/[\s,:\-\.]+/", $ts);
	if (count($tse) != 7)
		return null;

	$ts2 = mktime($tse[3], $tse[4], $tse[5], $tse[1], $tse[2], $tse[0]);	
	return strftime("%d/%m/%y %H:%M:%S", $ts2);
}



function to_cod($v){
	return strtr($v, array(" " => "_"));
}


function sql_f($v){
	if (strlen(trim($v)) == 0)
	 $v = 0;
	return strtr($v, array("," => "."));
}



/* ricevo una stringa con eventuali spazi (es: 15 blocchi per filtro codice articolo)
 * e restituisco il testo da usare nel like
 */
function sql_char_compare_string($v, $like){
    $t = '';
    for ($i = 0; $i < strlen(rtrim($v)); $i++){
        if ($v[$i] == ' ')
            $t .= '_';
        else 
            $t .= $v[$i];
    }
    
    switch ($like) {
        case 'LIKE':
            return "'%" . strtr(utf8_decode($t), array("'" => "''")) . "%'";
            break;
        case 'RLIKE':
            return "'" . strtr(utf8_decode($t), array("'" => "''")) . "%'";
            break;
        case 'LLIKE':
            return "'%" . strtr(utf8_decode($t), array("'" => "''")) . "'";
            break;
        default:
            return "'" . strtr(utf8_decode($t), array("'" => "''")) . "'";
            break;
    }
}


function sql_t_compare($v, $like, $ie = 'I'){
    return sql_operator($like, $ie) . sql_t($v, $like); 
}

function sql_t($v, $like = null){
	//return "'" . $v . "'";
    switch ($like) {
	    case 'LIKE':
	        return "'%" . strtr(utf8_decode($v), array("'" => "''")) . "%'";
	        break;
	    case 'RLIKE':
	        return "'" . strtr(utf8_decode($v), array("'" => "''")) . "%'";
	        break;
	    case 'LLIKE':
	        return "'%" . strtr(utf8_decode($v), array("'" => "''")) . "'";
	        break;
	    default:
	        return "'" . strtr(utf8_decode($v), array("'" => "''")) . "'";
	    break;
	}
}

function sql_operator($operator = '=', $ie = 'I'){
  if ($ie == 'I'){  //Includi (std)
      if ($operator == 'LIKE' || $operator == 'RLIKE' || $operator == 'LLIKE') return ' LIKE ';
      if (is_null($operator)) return ' = ';
  } else {          //Escludi
      if ($operator == 'LIKE' || $operator == 'RLIKE' || $operator == 'LLIKE') return ' NOT LIKE ';
      if (is_null($operator)) return ' <> ';
  }
  return $operator;
}


function to_field_k($v){
	return "k_" . $v;
}

function sql_t_trim($v){
	return sql_t(trim($v));
}

function sql_t_IN($ar_v){
	return implode(', ', array_map('sql_t_trim', $ar_v));
}


function sql_where_by_combo_value($field, $selection, $like = null, $ie = 'I', $force = false){

    if ($ie == 'E') //escludi
        $txt_IN = 'NOT IN';
    else
        $txt_IN = 'IN';
    
    if (!isset($selection)) return '';	
    	
	if (is_array($selection)){
		if (count($selection) == 0) return '';
		if (count($selection) == 1) //ottimizzazione per sql
		    return " AND {$field} " . sql_t_compare($selection[0], $like, $ie);
		else
			return " AND {$field} {$txt_IN} (" . sql_t_IN($selection) . ") ";		
	}// is array
	
	//se non e' array
	if ($force == false && strlen(trim($selection)) == 0) return '';
	return " AND {$field} " . sql_t_compare($selection, $like, $ie);	
}



function n($v, $decimals=2, $forza_segno = 'N', $escludi_zero = 'N'){
 $pre_segno = '';
 if ($forza_segno == 'Y' && $v > 0) $pre_segno = '+';	
 
 if ($v == 0 && $escludi_zero == 'Y') return '';
 return $pre_segno . number_format($v, $decimals, ',', '.'); 
}

function n_auto($v, $decimals=2, $forza_segno = 'N', $escludi_zero = 'N'){
	$num = n($v, $decimals, $forza_segno, $escludi_zero);
	
	//rimuovo gli 0 non significativi (nei decimali)
	$pos = strpos($num, ',');
	if($pos === false) { // it is integer number
		return $num;
	}else{ // it is decimal number
		return rtrim(rtrim($num, '0'), ',');
	}	
	
}

function text_to_number($v, $n_int, $n_dec){
    
    $dec = substr($v, $n_int, $n_dec);
    if (trim($dec) == '')
        $dec = 0;
    $int = trim(substr($v, 0, $n_int)) . "." . $dec;
        
    return (float)$int;
}

function number_to_text($v, $n_int, $n_dec, $virgola = 'N'){
  
   $ar_value = explode(',', $v);
    
   if(count($ar_value) == 1)
       $dec = "";
   else
       $dec = $ar_value[1];
     
   $dec =sprintf("%0-{$n_dec}s", $dec);
   $int = sprintf("%0{$n_int}s", $ar_value[0]);
   if($virgola == 'Y')
     $text = $int.",".$dec;
   else
     $text = $int.$dec;
   

   return $text;
}


function trim_utf8($txt){
	return trim(acs_u8e($txt));
}

function j($txt){
	return acs_je(acs_u8e($txt));
}


function remove_empty($array) {
  return array_filter($array, '_remove_empty_internal');
}

function _remove_empty_internal($value) {
  return !empty($value) || $value === 0;
}

function oggi_AS_date(){
	return date('Ymd');	
}

function oggi_AS_time(){
	return date('His');
}

function now_AS_date_time(){
	return to_AS_date_time(oggi_AS_date(), oggi_AS_time());
}


function to_AS_date($data = 0){
	if ($data == 0) return 0;
	return date('Ymd', $data);
}

function to_AS_date_time($data = 0, $ora = 0){
	if ($data == 0) return 0;
	return ((int)$data * 1000000) + (int)$ora;
}



function stringToInteger($string) {
	$output = '';
	for ($i = 0; $i < strlen($string); $i++) {
		$output .= (string) ord($string[$i]);
	}
	return (int) $output;
}

function mounth_year($range){
	$d=strtotime(" $range  Months");
	return date("m/Y", $d);
}


function print_pdf_link($ar){
    $ret = '';
    foreach($ar as $k => $fp){
        $p_page = "acs_get_order_images.php?function=view_image&IDOggetto={$fp}";
        $ret .= "<a href=\"#\" onclick=\"allegatiPopup('{$p_page}');\">[ $k ]</a>";
    }
    return $ret;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
function componi_sql_in($nct, $v){
    // $nct = nome campo tabella da ricevere con alias
    $ret = array();
    if (isset($v) && strlen(trim($v[0])) > 0) {
        $sql_in = '';
        $sql_p_ar = array();
        $c = count($sql_p_ar);
        $sql_p_ar[$c] = $v[0];
        for ($i = 1; $i < count($v); $i++) {
            $sql_in .= ", ?";
            $c = count($sql_p_ar);
            $sql_p_ar[$c] = $v[$i];
        }
        
        $ret['stringa'] = "and {$nct} in( ?{$sql_in} )";
        $ret['parametri'] = $sql_p_ar;
    }
    return $ret;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------







function v_or_blank($v){
  if (!isset($v)) return ''; 
  if (is_null($v)) return '';
  return $v;
}



function acs_portal_db_debug($op, $sql, $ar, $m_params){
    try {
        global $auth;
        $log_file_data = '/tmp/acs_portal_debug.log';
        file_put_contents($log_file_data, "<start>------------------------------------\n", FILE_APPEND);
        file_put_contents($log_file_data, "Data/ora: " . date("d/m/Y H:i:s") . "\n", FILE_APPEND);
        file_put_contents($log_file_data, "Utente: " . $auth->get_user() . "\n", FILE_APPEND);
        file_put_contents($log_file_data, "Dir: " . $_SERVER['SCRIPT_NAME'] . "\n", FILE_APPEND);       
        file_put_contents($log_file_data, "Op: {$op}" . "\n", FILE_APPEND);
        file_put_contents($log_file_data, "Sql: {$sql}" . "\n", FILE_APPEND);
        file_put_contents($log_file_data, "Parametri SQL: " . acs_je($ar, true) . "\n", FILE_APPEND);
        file_put_contents($log_file_data, "m_params: " . acs_je($m_params, true) . "\n", FILE_APPEND);
        file_put_contents($log_file_data, "_REQUEST: " . acs_je($_REQUEST, true) . "\n", FILE_APPEND);
        file_put_contents($log_file_data, "------------------------------------<end>\n", FILE_APPEND);
    } catch (Excetpion $e) {
        //ToDo: 
    }
}

function SV2_db_user($user){
    
    return substr($user, 0, 8);
    
}

function regex_multiemail(){
    
    return "/^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;,.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/";
}



function sanitize_filename($name) {
    // remove illegal file system characters https://en.wikipedia.org/wiki/Filename#Reserved_characters_and_words
    $name = str_replace(array_merge(
        array_map('chr', range(0, 31)),
        array('<', '>', ':', '"', '/', '\\', '|', '?', '*')
    ), '', $name);
    $name = str_replace(array(' '), '_', $name);
    // maximise filename length to 255 bytes http://serverfault.com/a/9548/44086
    $ext = pathinfo($name, PATHINFO_EXTENSION);
    $name= mb_strcut(pathinfo($name, PATHINFO_FILENAME), 0, 255 - ($ext ? strlen($ext) + 1 : 0), mb_detect_encoding($name)) . ($ext ? '.' . $ext : '');
    return $name;
}

