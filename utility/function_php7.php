<?php

require_once("ApiProc7.php");

function acs_emogrify($html, $css){
 require_once 'emogrifier.php7.php';
 $emogrifier = new \Pelago\Emogrifier($html,  $css);
 return $emogrifier->emogrify();
}


//sostituisce:
//from:	json_decode($HTTP_RAW_POST_DATA) 	//php5
//to:	acs_m_params_json_decode() 			//php7
function acs_m_params_json_decode(){
	global $acs_m_params;
	if (!isset($acs_m_params))
		$acs_m_params = json_decode(file_get_contents("php://input")); //php7
	return $acs_m_params;
}
function acs_raw_post_data(){
	global $acs_raw_post_data;
	if (!isset($acs_raw_post_data))
		$acs_raw_post_data = file_get_contents("php://input"); //php7
		return $acs_raw_post_data;
}


/* UTF8_ENCODE */


function acs_toDb($v){
    return utf8_decode($v);
}

/* Ancora non usata */
function acs_u8e($v){
    //ToDo: verificare effettivamente se server. 
    //Attenzione: in alcuni casi possiamo avere null invece che ''
    //es: tooltip_ripro
    
    if (is_null($v))
        return '';
    else
        return $v;        
    
    //return utf8_encode($v); //su php7 non dovrebbe servire
    //return $v;
}



function acs_je($value, $pretty = false){
    if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
        if ($pretty == true)
            $encoded = json_encode($value, JSON_PRETTY_PRINT);
        else
            $encoded = json_encode($value);
    } else {
        $encoded = json_encode($value);
    }
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            return $encoded;
        case JSON_ERROR_DEPTH:
            return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_STATE_MISMATCH:
            return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_CTRL_CHAR:
            return 'Unexpected control character found';
        case JSON_ERROR_SYNTAX:
            return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_UTF8:
            $clean = utf8ize($value);
            return acs_je($clean);
        default:
            return 'Unknown error'; // or trigger_error() or throw new  Exception()
    }
}

function utf8ize($mixed) {
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } else if (is_string ($mixed)) {
        return utf8_encode($mixed);
    }
    return $mixed;
}


//sembra dare errore in php5 (almeno su midi)
function acs_decamelize($string) {
    return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $string));
}
