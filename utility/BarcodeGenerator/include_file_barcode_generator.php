<?php

require_once "BarcodeGenerator.php";
require_once "BarcodeGeneratorHTML.php";
require_once "BarcodeGeneratorJPG.php";
require_once "BarcodeGeneratorPNG.php";
require_once "BarcodeGeneratorSVG.php";

require_once "Exceptions/BarcodeException.php";
require_once "Exceptions/InvalidCharacterException.php";
require_once "Exceptions/InvalidCheckDigitException.php";
require_once "Exceptions/InvalidFormatException.php";
require_once "Exceptions/InvalidLengthException.php";
require_once "Exceptions/UnknownTypeException.php";
