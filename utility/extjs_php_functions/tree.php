<?php

function tree_to_json_data($ar){
  $ret = array();
  foreach($ar as $kar => $r){
    $ret[] = array_values_recursive($ar[$kar]);
  }
  return $ret;  
}


function create_tree_data_from_sql($sql, $ar_livs){
  global $conn;  
  $stmt = db2_prepare($conn, $sql);
  echo db2_stmt_errormsg();
  $result = db2_execute($stmt);
    
  $ret = array();
  
  while ($row = db2_fetch_assoc($stmt)) {
      $ar_r= &$ret;
      $tmp_ar_id = array();
      
      $last_liv = end($ar_livs);
      
      //per ogni livello
      foreach ($ar_livs as $k_liv => $liv_info){
          $liv = $row[$k_liv];
          $tmp_ar_id[] = $liv;
          
          if (!isset($ar_r["{$liv}"])){
              $ar_new = array();
              $ar_new['id'] = implode("|", $tmp_ar_id);
              
              $ar_new['task']       = $row[$liv_info['f_descr']]; //ToDo
              
              if ($liv_info['descr_add_code'] == 'Y')
                 $ar_new['task']    .= " [" . $row[$k_liv] . "]";
              
              $ar_new['liv_cod']    = $k_liv;
              //$ar_new['liv'] = 'liv_totale';          //ToDo

             // $ar_new['expanded'] = true;               //ToDo
              $ar_r["{$liv}"] = $ar_new;
          }
          $ar_r = &$ar_r["{$liv}"];
          
          if (end(array_keys($ar_livs)) != $k_liv){
            //entro nei figli del livello
            $ar_r = &$ar_r['children'];
          } else {
            $ar_r['leaf'] = true;    
          }
      }
      
  }
  return $ret;   
}


function tree_itemclick($function_code){
  $ret = "function(view,rec,item,index,eventObj) {";
  $ret .= $function_code;
  $ret .= "}";
  return $ret;
}


function extjs_tree_proxy($url, $extraParams = array()){
    $c = new Extjs_compo();
    $c->set(array(
        'type'            => 'ajax',
        'url'             => $url,
        'extraParams'     => $extraParams,
        'actionMethods'   => array('read' => 'POST'),
        'reader'          => array('root' => 'children'),
        'doRequest'       => new ApiProcRaw('personalizza_extraParams_to_jsonData')
    ));
    return $c->code();
}


