<?php

function extjs_datefield($name = null, $fieldLabel = null, $value = null, $v = null){
    $c = new Extjs_Datefield($name, $fieldLabel, $value, $v);
    return $c->code();
}


class Extjs_Datefield extends Extjs_Compo {
    
    function __construct($name = null, $fieldLabel = null, $value = null, $v = null) {
        parent::__construct('datefield', null, null, $v);
        
        $this->set(array(
            'startDay'       => 1,
            'labelAlign'    => 'left',
            'format'        => 'd/m/Y',
            'altFormats'    => 'Ymd',
            'submitFormat'  => 'Ymd',
            'allowBlank'    => true,
            'listeners'     => array(
                'invalid' => extjs_code("
                        function (field, msg) {
				            Ext.Msg.alert('', msg);
			 			}
                ")
            )
        ));
        
        if (!is_null($v['minValue'])) $v['minValue'] = print_date($v['minValue'], "%d/%m/%Y");
        if (!is_null($v['maxValue'])) $v['maxValue'] = print_date($v['maxValue'], "%d/%m/%Y");
        //if (!is_null($v['allowBlank'])) $this->set(array('allowBlank' => $v['allowBlank']));
        if (!is_null($name)) $this->set(array('name' => $name));
        if (!is_null($fieldLabel)) $this->set(array('fieldLabel' => $fieldLabel));        
        if (!is_null($value)) $this->set(array('value' => print_date($value, "%d/%m/%Y")));
        if (!is_null($v)) $this->set($v);
        
    } //__construct  

        
} //class


