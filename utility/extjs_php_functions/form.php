<?php

class Extjs_Form extends Extjs_Compo {
    
    function __construct($layout = null, $title = null, $v = null) {
        parent::__construct('form', $layout, $title, $v);
        $this->set(array(
            'frame'       => true,
            'bodyStyle'   => 'padding: 10px',
            'bodyPadding' => '5 5 0'
        ));
    }
        
}

