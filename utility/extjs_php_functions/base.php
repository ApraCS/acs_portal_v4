<?php

//class Extjs_compo implements \JsonSerializable {
class Extjs_compo {
    
    protected $code = array();
    
    function __construct($xtype = null, $layout = null, $title = null, $v = null) {
        if (!is_null($xtype))  $this->code['xtype']  = $xtype;
        if (!is_null($layout)) $this->code['layout'] = $layout;
        if (!is_null($title))  $this->code['title']  = $title;
        if (!is_null($v)) $this->set($v);
    }
    
    function set($v){
        $this->code = array_merge($this->code, $v);
    }
    
    function add_listener($listener_name, $f){
        $this->code['listeners'][$listener_name] = $f;
    }
    
    function add_item($v){
        $this->code['items'][] = $v;
    }
    
    function add_items($ar_v){
        foreach ($ar_v as $v){
            $this->code['items'][] = $v;
        }
    }
    
    function add_button($b, $dock_align = 'bottom'){
        if (!isset($this->code['dockedItems']))
            $this->code['dockedItems'] = array();
            
            //ToDo: in base a $dock_align verificare se gia' ho creato la dock per la posizione.
            // Eventualmente fare solo l'aggiunta del bottone
            
            if (count($this->code['dockedItems']) == 0)
                $this->code['dockedItems'][0] = array(
                    'dock'   => $dock_align
                    , 'xtype' => 'toolbar'
                    ,  'ui'   => 'footer'
                    , 'defaults' => array('scale'=>'large')
                    , 'items' => array($b)
                );
    }
    

    function add_buttons($b, $dock_align = 'bottom'){
        if (!isset($this->code['dockedItems']))
            $this->code['dockedItems'] = array();
            
            //ToDo: in base a $dock_align verificare se gia' ho creato la dock per la posizione.
            // Eventualmente fare solo l'aggiunta del bottone
            
            if (count($this->code['dockedItems']) == 0)
                $this->code['dockedItems'][0] = array(
                    'dock'   => $dock_align
                    , 'xtype' => 'toolbar'
                    ,  'ui'   => 'footer'
                    , 'defaults' => array('scale'=>'large')
                    , 'items' => $b
                );
    }
    
    
    
    function code(){
        return $this->code;
    }

/*    
    public function jsonSerialize(){
        return json_encode($this->code);
    }
*/
    
} //Extjs_compo