<?php

function extjs_timefield($name = null, $fieldLabel = null, $value = null, $v = null){
    $c = new Extjs_Timefield($name, $fieldLabel, $value, $v);
    return $c->code();
}


class Extjs_Timefield extends Extjs_Compo {
    
    function __construct($name = null, $fieldLabel = null, $value = null, $v = null) {
        parent::__construct('timefield', null, null, $v);
        
        $this->set(array(
            'labelAlign'    => 'left',
            'format'        => 'H:i',
            'submitFormat'  => 'His',
            'allowBlank'    => false,
            'listeners'     => array(
                'invalid' => extjs_code("
                        function (field, msg) {
				            Ext.Msg.alert('', msg);
			 			}
                ")
            )
        ));
        
        if (!is_null($name)) $this->set(array('name' => $name));
        if (!is_null($fieldLabel)) $this->set(array('fieldLabel' => $fieldLabel));        
        if (!is_null($value)) $this->set(array('value' => print_ora($value)));
        if (!is_null($v)) $this->set($v);
        
    } //__construct  

        
} //class


