<?php

function extjs_checkboxfield($name = null, $fieldLabel = null, $value = null, $v = null){
    $c = new Extjs_Checkboxfield($name, $fieldLabel, $value, $v);
    return $c->code();
}


class Extjs_Checkboxfield extends Extjs_Compo {
    
    function __construct($name = null, $fieldLabel = null, $value = null, $v = null) {
        parent::__construct('checkboxgroup', null, null, $v);
        
        
        $this->add_items(array(
            array('xtype' => 'checkbox', 'name' => $name, 'inputValue' => 'Y', 'uncheckedValue'   => 'N'),
            
        ));
        
       /* $this->set(array(
            'inputValue'       => 'Y',
            'uncheckedValue'   => 'N'  
        ));*/
        
        if (!is_null($fieldLabel)) $this->set(array('fieldLabel' => $fieldLabel));  
        //if (!is_null($name)) $this->set(array('name' => $name));
        if (!is_null($v)) $this->set($v);
        
    } //__construct  

        
} //class