<?php

function extjs_button($text = null, $scale = 'small', $iconCls = null, $ar_v = array()){
    $c = new Extjs_Button($text, $scale, $iconCls, $ar_v);
    return $c->code();
}


function extjs_button_gear($config_add_button_gest_tab){
    $c = new Extjs_Button();
    $c->set(array('iconCls' => 'icon-gear-16'));
    
    $url_file_gestione = $config_add_button_gest_tab['file'];
    if (!isset($config_add_button_gest_tab['fn']))
        $url_file_gestione .= '?open_tab';
    else 
        $url_file_gestione .= '?' . $config_add_button_gest_tab['fn'];
    
    $c->set(array(
        'handler' => extjs_code("
            function() {
                var combo = this.up('panel').down('#DECAF');
                var pan = this.up('panel');

                //var my_listeners = {
                //        afterInsert: function(from_win){
                //        pan.refreshCombo(combo, from_win);                
                //    }
                //}
                var my_listeners = {};
        
            acs_show_win_std('Gestione tabella', " . acs_je($url_file_gestione) . ", {}, 950, 500, my_listeners, 'icon-gear-16');
            } //handler function()
         "))
    );
    

    
    return $c->code();
}

function extjs_buttons_f8($mod, $func, $tbspacer=0){
    
    $c = new Extjs_compo('fieldcontainer', layout_ar('hbox'));
    $c->add_items(array(
        
        extjs_button('Memorizza', 'small', 'icon-F8-m-16', 
            array('tooltip' => 'Memorizza parametri correnti', 
                'handler' => extjs_code("
                    function() {
                    var form = this.up('form').getForm();
        					var form_values = form.getValues();
         
                            //gestione eventuali grid di cui salvare lo store
                            var grids = this.up('form').query('[grid_save_in_F8]');
        		        	Ext.each(grids, function(gr) {
                                var grid_values = gr.get_f8_data();
                                form_values[gr.grid_save_in_F8] = grid_values;
        		        	});
        				
        					ar_parameters = " . acs_je(array("op" => "M", "mod" => $mod, "func" => $func)) . ";				
        					ar_parameters['memo'] = form_values;
        
        					//apro la form per scegliere il nome della memorizzazione
        					acs_show_win_std('Memorizza parametri correnti', " . acs_url('module/base/f8.php?fn=get_json_m') . ", ar_parameters, 500, 460, null, 'icon-F8-m-16');				
                } //handler function()
        
               "))),
        extjs_button('Richiama', 'small', 'icon-F8-r-16',
            array('tooltip' => 'Richiama parametri preimpostati',
                  'handler' => extjs_code("
                      function() {
                       var form = this.up('form').getForm();				
            					ar_parameters = " . acs_je(array("op" => "M", "mod" => $mod, "func" => $func)) . ";
            					ar_parameters['form_id'] = this.up('form').id;											
            
            					//apro la form per la scelta della memorizzazione
            					acs_show_win_std('Richiama parametri preimpostati', " . acs_url('module/base/f8.php?fn=get_json_r') . ", ar_parameters, 500, 360, null, 'icon-F8-r-16');
                    } //handler function()
                    
               "),
                
                'listeners' =>  array(
                    'afterrender' => extjs_code("
                    function(comp){
                        var form_origine = comp.up('form');
                        ar_parameters = " . acs_je(array("op" => "M", "mod" => $mod, "func" => $func)) . ";
                       	 Ext.Ajax.request({
    				   		url:" . acs_url('module/base/f8.php?fn=get_data') . ",
    				   		method: 'POST',
    	        			jsonData: {
    	        				ar_parameters: ar_parameters
    	        				},
    				   		success: function(response, opts) {
    				      		var src = Ext.decode(response.responseText);
                                var data = src.data[0].DFMEMO;
                        
                                memorizzazione = Ext.decode(data.replace(/\\\"/g, '\"'));
                                p_ar = new Array();
                        	  	 Ext.Object.each(memorizzazione, function(key, value) {
                        	  	 	p_ar[key] = value;
                        	  	 });
                        
                        	  	//per tutti gli input della form
                        	  	form_origine.getForm().getFields().items.forEach(function(n) {
                        	  	  if (n.xtype == 'datefield'){
                        	  	    if (!Ext.isEmpty(p_ar[n.name]) && p_ar[n.name].length > 0)
                        	  	  		n.setValue(acs_date_from_AS(p_ar[n.name], 'd/m/Y'));
                        	  	  	else
                        	  	  		n.setValue(null);
                        	  	  } else if (n.xtype == 'hidden') {
                        	  		//non ricompilo i campi hidden
                        	  	  } else if (n.xtype == 'displayfield') {
                        	  		//non ricompilo i campi displayfield
                        	  	  } else {
                        	  		n.setValue(p_ar[n.name]);
                        	  	  }
                        	  	});
                        
    					   	}
    					});
                        
                }
                        
                        
            ")),
                
                
                
    )),
        
    )); 
   
    return $c->code();
}



class Extjs_Button extends Extjs_Compo {
    
    function __construct($text = null, $scale = 'small', $iconCls = null, $ar_v = array()) {
        
        //Gestione default
        $ar_v['scale'] = $scale;
        //if (!isset($ar_v['scale']))  $ar_v['scale']   = 'small';        
        
        parent::__construct('button', null, null, $ar_v);
        
        $this->set(array(
            'iconCls'       => $iconCls,
            'text'    => $text,
            'scale' => $scale
            /*'listeners'     => array(
                'invalid' => extjs_code("
                        function (field, msg) {
				            Ext.Msg.alert('', msg);
			 			}
                ")
            )*/
        ));
        
        if (!is_null($ar_v)) $this->set($ar_v);
        
    } //__construct  

        
} //class


