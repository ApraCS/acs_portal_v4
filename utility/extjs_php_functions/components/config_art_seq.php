<?php

/* es:
  $c_config_seq = new CompConfigArtSeq('seq_config', array(
    'f_seq' => 'seq_cfg',
    'f_tip' => 'tp_seq',
    'f_rig' => 'riga' ));
 */

class CompConfigArtSeq {
    
    protected $config       = array();
    protected $recordValues = array();
    
    function __construct($id_comp, $config = array(), $recordValues = array()) {
        $this->id_comp = $id_comp;
        $this->config = (object)$config;
        $this->recordValues = $recordValues;
        $this->config->component_name = get_class($this);
        
        $this->config = json_decode(json_encode($this->config, JSON_FORCE_OBJECT), false);        
    }
    
    public function out_name_field(){
        return "comp_decod_{$this->id_comp}";
    }
    
    public function prepare_row($nr){

        $dec = '';
        switch (trim($nr[$this->config->f_tip])) {
            case '?':
                $dec_dom = get_TA_sys('PUVR', $nr[$this->config->f_seq], null, null, null, null, 1);
                $dec = '?: ' . "[{$dec_dom['id']}] " . $dec_dom['text'];
            break;
            case '' :
                $dec = trim($nr[$this->config->f_seq]);
            break;
        }
        
        if (strlen(trim($nr[$this->config->f_rig])) > 0)
            $dec .= ", Riga: " . $nr[$this->config->f_rig];
        
        $nr[$this->out_name_field()] = $dec;
        return $nr;
    }
    
    
    public function get_comp(){
        return $this->prepare_comp()->code();
    }
    
    public function prepare_comp(){
        $c = new Extjs_compo('fieldcontainer', layout_ar('hbox'));
        
        $c->set(array('acs_type' => 'acs_component', 'component_params' => $this->config));
        
        $c->add_items(array(            
            array('xtype' => 'hiddenfield', 'acs_type' => 'f_seq',  'name'=> $this->config->f_seq),
            array('xtype' => 'hiddenfield', 'acs_type' => 'f_tip',  'name'=> $this->config->f_tip),
            array('xtype' => 'hiddenfield', 'acs_type' => 'f_rig',  'name'=> $this->config->f_rig),
            
            array('xtype' => 'displayfield', 'acs_type' => 'out_d',  'name'=> $this->out_name_field(),
                    'fieldLabel' => 'Sequenza', 'flex' => 1, ),
            
            array('xtype' => 'button', 'scale' => 'small', 'iconCls' => 'icon-search-16', 'width' => 25, 'margin' => '0 0 0 5',
              'handler' => extjs_code("
                function(){
                    var component_container = this.up('[acs_type=\"acs_component\"]'), 
                        component_params = component_container.component_params,
                        m_form    = this.up('form').getForm();
                    acs_show_win_std('Imposta regola'
                        , '../base/wrapper_to_component.php?fn=open_win'
                        , {component_params: component_params, formValues: m_form.getValues()}, 600, 260, {
                            'afterSave': function(from_win, formValues){
                                
                                component_container.down('[acs_type=\"f_seq\"]').setValue(formValues.f_seq); 
                                component_container.down('[acs_type=\"f_tip\"]').setValue(formValues.f_tip);
                                component_container.down('[acs_type=\"f_rig\"]').setValue(formValues.f_rig);
                							
                				component_container.down('[acs_type=\"out_d\"]').setValue(formValues.f_seq + '|' + formValues.f_tip + '|' + formValues.f_rig);    
                                from_win.destroy();

                            } //afterSave
                        }
                        , 'icon-search-16');
                } //function handler
              ")
            )
            
        ));
      return $c;
    } //prepare_comp    
    
    
    
    public function open_win(){
        $proc = new ApiProc();
        $ar_componenti = array();
        
        
        $initialData_f_tp = array(
            array('id' => '',  'text' => 'Valore'),
            array('id' => '?', 'text' => 'Da variabile')
        );
        
        $initialData_var = find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y', 'N', 'Y');
        
        $ar_componenti[] = extjs_combo(array(
            'fieldLabel' => 'Tipo',
            'name' => 'f_tip',
            'initialData'  => $initialData_f_tp,
            'initialValue' => $this->recordValues->{$this->config->f_tip},
            'queryMode'     => 'local',
            'onChangeCallFormFunction' => 'refreshForm'
        ));
        

        $ar_componenti[] =   array(
            'xtype' => 'textfield',
            'fieldLabel' => 'Sequenza',
            'name'  => 'f_seq',
            'value' => $this->recordValues->{$this->config->f_seq}
        );
        
        $ar_componenti[] =   extjs_combo(array(
            'fieldLabel' => 'Variabile',
            'name' => 'f_seq_v',
            'initialData'  => $initialData_var,
            'initialValue'=> $this->recordValues->{$this->config->f_seq},
            'queryMode'     => 'local',
            'allowBlank'    => true
        ));
        

        $ar_componenti[] =   array(
            'xtype' => 'textfield',
            'fieldLabel' => 'Riga',
            'name'  => 'f_rig',
            'value' => $this->recordValues->{$this->config->f_rig}
        );
        
        $c = new Extjs_Form( layout_ar('vbox'), null, array(
            'items' => $ar_componenti
        ));
        
        $c->add_listener('afterrender', extjs_code("
            function(comp){
                comp.refreshForm();
            }
        "));
        
        $c->set(array(
            'refreshForm' => extjs_code("
            function(){
               var me = this,
                   form       = me.getForm(),
                   f_seq      = form.findField('f_seq'),
                   f_seq_v    = form.findField('f_seq_v'),
                   formValues = form.getValues();
               
               if (formValues.f_tip == ''){
                    //valore testuale
                    f_seq.show();
                    f_seq_v.hide();
               } else {
                    //selezione variabile
                    f_seq.hide();
                    f_seq_v.show();
               }
            }
        ")
        ));
        
        
        
        $c->add_buttons(array(
            array(
                'xtype' => 'button',
                'text' => 'Elimina',
                'scale' => 'large',
                'iconCls' => 'icon-sub_red_delete-32',
                'handler' => extjs_code("
            function(){
                var me = this,
                    loc_form = me.up('form').getForm(),
                    loc_win  = me.up('window');
                    
                var outValues = [];
                outValues['f_seq'] = '';
                outValues['f_seq_v'] = '';
                outValues['f_tip'] = '';
                outValues['f_rig'] = '';
                outValues['out_d'] = '';
                //outValues.dom_d = '';
               // outValues.ris_d = '';
                    console.log(outValues);
				loc_win.fireEvent('afterSave', loc_win, outValues);
                    
            }
        ")
            ), 
            array('xtype' => 'tbfill'),
            array(
                'xtype' => 'button',
                'text' => 'Conferma',
                'iconCls' => 'icon-sub_blue_accept-32',
                'handler' => extjs_code("
            function(){
                var me = this,
                    loc_form = me.up('form').getForm(),
                    loc_win  = me.up('window');
                    
                var outValues = loc_form.getValues();
                
                if (outValues.f_tip == '?'){
                    outValues.f_seq = outValues.f_seq_v;
                }
                    
                if (loc_form.isValid()){
					loc_win.fireEvent('afterSave', loc_win, outValues);
                }
            }
        ")
            )
        ), 'bottom');
        
        $proc->out_json_response(array(
            'success'=>true
            , 'items' => $c->code()
        ));
    } //opwn_win
    
    
}
