<?php

function extjs_combo_regola_config($ar_v = array()){
    $c = new Extjs_compo('fieldcontainer', layout_ar('hbox'));
    $c->set(array(
        'acs_type' => 'regola_config',
        'combo_params' => $ar_v
    ));
    
    if(!isset($ar_v['l_width']))
        $labelWidth = 100;
    else{
        $labelWidth = $ar_v['l_width'];
    }
    
    $c->add_items(array(
        array('xtype' => 'hiddenfield',  'acs_type' => 'f_var',  'name'=> $ar_v['f_var']),
        array('xtype' => 'hiddenfield',  'acs_type' => 'f_tp',   'name'=> $ar_v['f_tp']),
        array('xtype' => 'hiddenfield',  'acs_type' => 'f_van',  'name'=> $ar_v['f_van']),
        array('xtype' => 'hiddenfield',  'acs_type' => 'f_os',   'name'=> $ar_v['f_os']),
        
        array('xtype' => 'displayfield', 'flex' => 1, 'acs_type' => 'out_d', 'fieldLabel' => $ar_v['label'], 'labelWidth' => $labelWidth),
        
        array('xtype' => 'button', 'scale' => 'small', 'iconCls' => 'icon-search-16', 'width' => 25, 'margin' => '0 0 0 5',
            'handler' => extjs_code("
                                function(){
                                    var ar_params = " . acs_je($ar_v) . ";
                                    var dom_ris_container = this.up('fieldcontainer');

                                    ar_params.f_var = dom_ris_container.down('[acs_type=\"f_var\"]').getValue();
                                    ar_params.f_tp  = dom_ris_container.down('[acs_type=\"f_tp\"]').getValue();
                                    ar_params.f_van = dom_ris_container.down('[acs_type=\"f_van\"]').getValue();
                                    ar_params.f_os  = dom_ris_container.down('[acs_type=\"f_os\"]').getValue();

                                    acs_show_win_std('Impostazione singola condizione'
                                        , '../base/decodifica_regola_config.php?fn=open_win'
                                        , ar_params, 600, 260, {
                                            'afterSave': function(from_win, formValues){

                                                dom_ris_container.down('[acs_type=\"f_os\"]').setValue(formValues.f_os);
                                                dom_ris_container.down('[acs_type=\"f_var\"]').setValue(formValues.f_var);
                                                dom_ris_container.down('[acs_type=\"f_tp\"]').setValue(formValues.f_tp);
                                                
                                                if (formValues.var_type == 'N')
                                                    dom_ris_container.down('[acs_type=\"f_van\"]').setValue(formValues.f_van_n);
                                                else
                                                    dom_ris_container.down('[acs_type=\"f_van\"]').setValue(formValues.f_van);

                                                var output = '';
                                                output += dom_ris_container.down('[acs_type=\"f_os\"]').getValue() + ', ';
                                                //output += dom_ris_container.down('[acs_type=\"f_var\"]').getValue() + ',';
                                                output += formValues.dom_d.trim() + ', ';
                                                output += dom_ris_container.down('[acs_type=\"f_tp\"]').getValue() + ', ';
                                                //output += dom_ris_container.down('[acs_type=\"f_van\"]').getValue();
                                                if (formValues.var_type == 'N')
                                                  output += formValues.f_van_n;
                                                else
                                                  output += formValues.ris_d.trim();
                                				
                                							
                                				dom_ris_container.down('[acs_type=\"out_d\"]').setValue(output);

                                                from_win.destroy();
                                            } //afterSave
                                        }
                                        , 'icon-search-16');

                                }
                             ")
        )
        
        , array('xtype' => 'button', 'scale' => 'small', 'tooltip' => 'Copia', 'iconCls' => 'icon-copy-16', 'width' => 25, 'margin' => '0 0 0 5',
            'handler' => extjs_code("
                                function(){
                                    var ar_params = " . acs_je($ar_v) . ";
                                    var dom_ris_container = this.up('fieldcontainer');

                                    var f_var = dom_ris_container.down('[acs_type=\"f_var\"]').getValue(),
                                        f_tp  = dom_ris_container.down('[acs_type=\"f_tp\"]').getValue(),
                                        f_van = dom_ris_container.down('[acs_type=\"f_van\"]').getValue(),
                                        f_os  = dom_ris_container.down('[acs_type=\"f_os\"]').getValue(),
                                        out_d = dom_ris_container.down('[acs_type=\"out_d\"]').getValue();


                                   
                                        var mem_regola_config = {f_var: f_var, f_tp: f_tp, f_van: f_van, f_os: f_os, out_d: out_d};
                


                                     Ext.Ajax.request({
                        			   url        : '../base/decodifica_regola_config.php?fn=session_copy',
                        			   method: 'POST',
                        			   jsonData: {regola_config: mem_regola_config}, 
                        			   
                        			   success: function(response, opts) {
                        			   	//window.location.reload();
                        			   }, 
                        			   failure: function(response, opts) {
                        			      Ext.getBody().unmask();
                        			      alert('error');
                        			   }
                        			});	


                                } //function
                         ")
          
              
          )
        
        
        , array('xtype' => 'button', 'scale' => 'small', 'tooltip' => 'Incolla', 'iconCls' => 'icon-paste-16', 'width' => 25, 'margin' => '0 0 0 5',
            'handler' => extjs_code("
                                function(){
                                    var ar_params = " . acs_je($ar_v) . ";
                                    var dom_ris_container = this.up('fieldcontainer');

                                    Ext.Ajax.request({
                        			   url        : '../base/decodifica_regola_config.php?fn=session_incolla',
                        			   method: 'POST',
                        			   jsonData: {}, 
                        			   
                        			   success: function(response, opts) {
                        			   	  var jsonData = Ext.decode(response.responseText);
                                            //per ogni regola memorizzata
                                            regola_config = jsonData.regola_config;
                                                    console.log(jsonData.regola_config);
                                               regola_config.forEach(function(conf) {
                                                dom_ris_container.down('[acs_type=\"f_var\"]').setValue(conf.f_var);
                                                dom_ris_container.down('[acs_type=\"f_tp\"]').setValue(conf.f_tp);
                                                dom_ris_container.down('[acs_type=\"f_van\"]').setValue(conf.f_van);
                                                dom_ris_container.down('[acs_type=\"f_os\"]').setValue(conf.f_os);
                                                dom_ris_container.down('[acs_type=\"out_d\"]').setValue(conf.out_d);
                                                dom_ris_container = dom_ris_container.next('fieldcontainer');                                      
                                    });
                        			   }, 
                        			   failure: function(response, opts) {
                        			      Ext.getBody().unmask();
                        			      alert('error');
                        			   }
                        			});	

                                }
                         ")
        )
        
        
        
        
        ));
    
    return $c->code();
}


function extjs_combo_dom_ris($ar_v = array()){
    $c = new Extjs_compo('fieldcontainer', layout_ar('hbox'));
    $c->set(array(
       'acs_type' => 'dom_ris',
       'file_TA'  => $ar_v['file_TA'],
       'combo_params' => $ar_v
    ));
    
    if(isset($ar_v['initial_value'])){
        $dom_c  = $ar_v['initial_value']['dom_c'];
        $ris_c  = $ar_v['initial_value']['ris_c'];
        $out_d  = $ar_v['initial_value']['out_d'];
        $tipo_c = "";
    }else{
        $dom_c  = "";
        $ris_c  = "";
        $out_d  = "";
        $tipo_c = "";
    }
    
    
    if(!isset($ar_v['l_width']))
        $labelWidth = 100;
    else
        $labelWidth = $ar_v['l_width'];
    
    
    
    if (isset($ar_v['tipo_opz_risposta']) && isset($ar_v['tipo_opz_risposta']['tipo_cf'])){
        $c->add_item(
            array('xtype' => 'hiddenfield',  'acs_type' => 'tipo_c', 'name'=> $ar_v['tipo_opz_risposta']['tipo_cf'], 
                'tipo_opz_risposta' => $ar_v['tipo_opz_risposta'], 'value' => $tipo_c
            )
        );
    }
    
    if (isset($ar_v['tipo_opz_risposta']) && isset($ar_v['tipo_opz_risposta']['output_domanda']) && 
         $ar_v['tipo_opz_risposta']['output_domanda'] == true){
             $c->add_items(array(
                 array('xtype' => 'hiddenfield',  'acs_type' => 'dom_c', 'name'=> $ar_v['dom_cf'], 'value' => $dom_c),
                 array('xtype' => 'hiddenfield',  'acs_type' => 'dom_d')
             ));
    } else {
        $c->add_items(array(
            array('xtype' => 'hiddenfield',  'acs_type' => 'dom_c', 'dom_c_namefield'=> $ar_v['dom_cf'], 'value' => $dom_c),
            array('xtype' => 'hiddenfield',  'acs_type' => 'dom_d')
        ));
    }
    

    
    $c->add_items(array(
            array('xtype' => 'hiddenfield',  'acs_type' => 'ris_c', 'name'=> $ar_v['ris_cf'], 'value' => $ris_c),        
            array('xtype' => 'hiddenfield',  'acs_type' => 'ris_d'),
        
        
        
        array('xtype' => 'displayfield', 'flex' => 1, 'acs_type' => 'out_d', 'fieldLabel' => $ar_v['label'], 'labelWidth' => $labelWidth, 'value' => $out_d),
            array('xtype' => 'button', 'scale' => 'small', 'iconCls' => 'icon-search-16', 'width' => 25, 'margin' => '0 0 0 5',
                'handler' => extjs_code("
                                function(){
                                    
                                    var ar_params = " . acs_je($ar_v) . ";
                                    var dom_ris_container = this.up('fieldcontainer');

                                    var g_dom_c = dom_ris_container.down('[acs_type=\"dom_c\"]');
                            		if (!Ext.isEmpty(g_dom_c.dom_c_namefield))
                            			ar_params.dom_c = dom_ris_container.up('form').getForm().findField(g_dom_c.dom_c_namefield).getValue();
                            		else
                            			ar_params.dom_c = g_dom_c.getValue();                                    

                                    ////ar_params.dom_d = dom_ris_container.down('[acs_type=\"dom_d\"]').getValue();
                                    ar_params.ris_c = dom_ris_container.down('[acs_type=\"ris_c\"]').getValue();
                                    ar_params.ris_d = dom_ris_container.down('[acs_type=\"ris_d\"]').getValue();

                                    if (!Ext.isEmpty(dom_ris_container.down('[acs_type=\"tipo_c\"]')))
                                        ar_params.tipo_c = dom_ris_container.down('[acs_type=\"tipo_c\"]').getValue();
                                    
                                    acs_show_win_std('Seleziona valori'
                                        , '../base/decodifica_dom_ris.php?fn=open_win'
                                        , ar_params, 600, 220, {
                                            'afterSave': function(from_win, formValues){
                                                 var g_dom_c = dom_ris_container.down('[acs_type=\"dom_c\"]');
                                                 console.log(formValues);
                                                if (!Ext.isEmpty(g_dom_c))
                                                    dom_ris_container.down('[acs_type=\"dom_c\"]').setValue(formValues.dom_c);
                                                
                                                if (formValues.var_type == 'N')
                                                    dom_ris_container.down('[acs_type=\"ris_c\"]').setValue(formValues.f_van_n);
                                                else
                                                    dom_ris_container.down('[acs_type=\"ris_c\"]').setValue(formValues.ris_c);

                                            if (!Ext.isEmpty(dom_ris_container.down('[acs_type=\"tipo_c\"]')))
                                                dom_ris_container.down('[acs_type=\"tipo_c\"]').setValue(formValues.f_tipo);

                                                var output = '';
                                                /*if (!Ext.isEmpty(formValues.f_tipo) && !Ext.isEmpty(formValues.f_tipo.trim()))
                                                    output = formValues.f_tipo.trim() + ': ';*/
                                				if (!Ext.isEmpty(formValues.dom_c)) output = formValues.dom_d + ', ';
                                				
                                                if (formValues.var_type == 'N')
                                                  output += formValues.f_van_n;
                                                else
                                                  output += '<br>' + formValues.ris_d;			                                							
                                				
                                                dom_ris_container.down('[acs_type=\"out_d\"]').setValue(output);

                                                from_win.destroy();
                                            }
                                        }
                                        , 'icon-search-16');
                                }
                             ")
            )
            ////array('xtype' => 'displayfield', 'acs_type' => 'ris_d', 'fieldLabel' => $ar_v['label'] . ' (r)'),
        ));
    return $c->code();
}




function extjs_combo($ar_v = array()){
    
    //Gestione default
    if (!isset($ar_v['displayField']))  $ar_v['displayField']   = 'text';
    if (!isset($ar_v['valueField']))    $ar_v['valueField']     = 'id';
    if (!isset($ar_v['allowBlank']))    $ar_v['allowBlank']     = false;
    if (!isset($ar_v['queryMode']))     $ar_v['queryMode']      = 'local';
    if (!isset($ar_v['minChars']))      $ar_v['minChars']       = 1;
    if (!isset($ar_v['labelAlign']))    $ar_v['labelAlign']     = 'left';
    if (!isset($ar_v['labelWidth']))    $ar_v['labelWidth']     = 100;
    if (!isset($ar_v['hidden']))        $ar_v['hidden']         = false;
    if (!isset($ar_v['store_fields']))  $ar_v['store_fields']    = array($ar_v['valueField'], $ar_v['displayField']);
    
    $tpl = " [
			 '<ul class=\"x-list-plain\">',
                '<tpl for=\".\">',
                '<li class=\"x-boundlist-item listItmes\" style=\"background-color:{color}\">{text}</li>',
                '</tpl>',
                '</ul>'
			 ],
           displayTpl: Ext.create('Ext.XTemplate',
                   '<tpl for=\".\">',
                       '{text}',
                    '</tpl>'
                       
            )";
    
    
    $c = new Extjs_compo('combo');
    $c->set(array(
        'name'          => $ar_v['name'],
        'value'         => $ar_v['value'],
        'fieldLabel'    => $ar_v['fieldLabel'],
        'displayField'  => $ar_v['displayField'],
        'valueField'    => $ar_v['valueField'],
        'emptyText'     => ' - seleziona -',
        'forceSelection'=> true,
       // 'multiSelect' => true,
        'allowBlank'    => $ar_v['allowBlank'],
        'labelWidth'    => $ar_v['labelWidth'],
        'labelAlign'    => $ar_v['labelAlign'],
        'queryMode'     => $ar_v['queryMode'],
        'minChars'      => $ar_v['minChars'],
        'typeAhead'     => true,
        'anyMatch'      => true,
        'hidden'        => $ar_v['hidden'],
        'store'       => array(
            'fields'      => $ar_v['store_fields'],
            'proxy'       => extjs_combo_proxy($ar_v['url'], $ar_v['extraParams'])), //store
        'tpl'        => extjs_code($tpl),
    ));
    
    $c->add_listener('beforequery', extjs_code("
        function(record){  
				    record.query = new RegExp(record.query, 'i');
					record.forceAll = true;
				}
    "));
    
    
    //se ha ricevuto un valore iniziale lo precarico (devo aver ricevuto cod e des)
    if (isset($ar_v['initialData'])){
        $c->add_listener('afterrender', extjs_code("
                    function(comp){
                        comp.store.loadData(" . acs_je($ar_v['initialData']) . ");
                        comp.setValue(" . acs_je($ar_v['initialValue']) . ");
                    }
                "));
    }
    
    
    //dopo la selezione/change devo resettare altri campi
    if (isset($ar_v['onChangeReset'])){
        $c->add_listener('select', extjs_code("
                    function(field, newVal){
                        var form = this.up('form'), 
                            changeField = form.getForm().findField(" . acs_je($ar_v['onChangeReset']) . ");
                        
                        changeField.setValue('');
                        //changeField.getStore().getProxy().extraParams.parentValue = newVal[0].get('id');
                        changeField.getStore().getProxy().extraParams.winFormValues = form.getValues()
                        changeField.getStore().load();
                    }
                "));
    }
    
    
    //dopo la selezione devo lanciare una funzione
    if (isset($ar_v['onChangeCallFormFunction'])){
        $c->add_listener('select', extjs_code("
                    function(field, newVal){
                        var form = this.up('form');
                        form." . $ar_v['onChangeCallFormFunction']. ".call(form);
                    }
                "));
    }
    
    //dopo la selezione devo lanciare una funzione
    if (isset($ar_v['selectRows']) && strlen($ar_v['selectRows']) > 0){
        $c->add_listener('select', extjs_code("
                    function(combobox, newVal){
                       var max = " . j($ar_v['selectRows']). ";
                       var num = combobox.getPicker().getSelectionModel().getCount();
                       if(num >= max){
                         combobox.collapse();
                         return false;
                       }
                        
                    }
                "));
    }
    
    
    global $auth;
    if ($auth->is_admin()){
        if (isset($ar_v['add_button_gest_tab'])){
            $bg = extjs_button_gear($ar_v['add_button_gest_tab']);
            
            //includo il combo in un fieldcontainer e aggiungo il button per la gestione della tabella
            $fc = new Extjs_compo('fieldcontainer', layout_ar('hbox'));
            $fc->add_item($c->code());  //combo
            $fc->add_item($bg);         //buttone
            return $fc->code();
        }
    }
    
    
    
    return $c->code();
}


function extjs_combo_fornitore($ar_v = array()){
    global $m_params;
    
    //Gestione default
    if (!isset($ar_v['url']))        $ar_v['url'] = extjs_url('get_data&cod=F', '../base/acs_get_anagrafica.php');    
    if (!isset($ar_v['fieldLabel'])) $ar_v['fieldLabel'] = 'Fornitore';
    if (!isset($ar_v['labelWidth'])) $ar_v['labelWidth']     = 100;
    
    $c = new Extjs_compo('combo');
    $c->set(array(
        'name' => $ar_v['name'],
        'fieldLabel' => $ar_v['fieldLabel'],
        'labelWidth'    => $ar_v['labelWidth'],
        'minChars' => 2,
        'valueField' => 'id',
        'displayField' => 'text',
        'typeAhead'  => false,
        'hideTrigger' => true,
        'pageSize' =>  1000,
        'anchor' => '-5',
        'store'       => array(   
            'fields'      => array('id', 'text'),
            'proxy'       => extjs_combo_proxy($ar_v['url'])), //store
            
        'listConfig' => array(
            'loadingText'   => 'Searching...'
          , 'emptyText'     => 'Nessun fornitore trovato'
          , 'getInnerTpl'   => extjs_code("
                function(){
                    return '<div class=\"search-item\">' +
                        '<h3><span>{text}</span></h3>' +
                        '[{id}] {text}' + 
                    '</div>';                    
                }
            ")  
        )
    ));
    return $c->code();
}

function extjs_combo_cliente($ar_v = array()){
    global $m_params;
    
    //Gestione default
    if (!isset($ar_v['url']))        $ar_v['url'] = "../desk_vend/acs_get_select_json.php?select=search_cli_anag";
    if (!isset($ar_v['fieldLabel'])) $ar_v['fieldLabel'] = 'Cliente';
    
    $c = new Extjs_compo('combo');
    $c->set(array(
        'name' => $ar_v['name'],
        'fieldLabel' => $ar_v['fieldLabel'],
        'minChars' => 2,
        'valueField' => 'cod',
        'displayField' => 'descr',
        'typeAhead'  => false,
        'hideTrigger' => true,
        'pageSize' =>  1000,
        'store'       => array(
            'fields'      => array('cod', 'descr', 'out_loc', 'ditta_orig'),
            'proxy'       => extjs_combo_proxy($ar_v['url'])), //store
        
        'listConfig' => array(
            'loadingText'   => 'Searching...'
            , 'emptyText'     => 'Nessun cliente trovato'
            , 'getInnerTpl'   => extjs_code("
                function(){
                    return '<div class=\"search-item\">' +
                        '<h3><span>{descr}</span></h3>' +
                         '[{cod}] {out_loc} {ditta_orig}' + 
                    '</div>';
                }
            ")
        )
    ));
    return $c->code();
}


function extjs_combo_proxy($url, $extraParams = array()){
    $c = new Extjs_compo();
    $c->set(array(
        'type'            => 'ajax',
        'url'             => $url,
        'extraParams'     => $extraParams,
        'reader'          => array('type' => 'json', 'root' => 'root', 'totalProperty' => 'totalCount'),
        'actionMethods'   => array('read' => 'POST'),
        'doRequest'       => extjs_code("personalizza_extraParams_to_jsonData")
    ));
    return $c->code();
}


function decod_regole_config($os, $tp, $var, $van, $ar_p = array()){
    
    $output = "";
    //se presente la domanda ********
    if (isset($var) && strlen($var) > 0){
        //decodifico variabile (domanda, PUVR)
        $dec_dom = get_TA_sys('PUVR', $var, null, null, null, null, 1);
        
        $output .= "{$os}, [{$var}] {$dec_dom['text']}";
        
        
        if (!isset($ar_p['risposta_a_capo']) || $ar_p['risposta_a_capo'] != false)
            $output .= "<br/>";
        else
            $output .= ", ";
                
        if ($dec_dom['tarest'] == 'N'){
            //Numerica
            $output .= "{$tp}, {$van}";
        } else {
            switch ($tp) {
                
                /*
                case "=": //decodifico la risposta (PUVN)
                    $dec_ris = get_TA_sys('PUVN', $van, null, $var);
                    $output .= "=, [" . trim($van) . "] {$dec_ris['text']}";
                    break;
                */
                    
                case "T": //decodifo la tipologia (PUTI)
                    $dec_tip = get_TA_sys('PUTI', $van);
                    $output .= "T, [" . trim($van) . "] {$dec_tip['text']}";
                    break;
                
                default:                    
                    if (strlen(trim($van)) > 0) {
                        $dec_ris = get_TA_sys('PUVN', $van, null, $var);
                        $dec_ris_text = $dec_ris['text'];
                        $output .= "{$tp}, [" . trim($van) . "] {$dec_ris_text}";
                    }
                    else {
                        $dec_ris_text = '';
                        $output .= "{$tp}";
                    }
                    break;
                    
            }
        }
    }
    
    return $output;
    
}

function decod_dom_ris($file, $dom_c, $ris_c, $tipo_c, $tipo_opz_risposta = array(), $opz_output = array()){
   
    global $id_ditta_default, $conn, $cfg_mod_Admin;
    $output = array();
    
    
    switch ($file) {
        case 'sys':      //XX0S2TA0
            
            //domanda
            $sql = "SELECT * FROM {$cfg_mod_Admin['file_tab_sys']}
            WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVR' AND TANR = '{$dom_c}' AND TALINV=''";
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt);
            $row = db2_fetch_assoc($stmt);
            if ($row)
                $dom_d = $row['TADESC'];
            else
                $dom_d = 'NON TROVATA';
                    
                    //Risposta ------------------
                if (isset($ris_c) && strlen($ris_c) > 0){
                    if (!isset($tipo_c)){ //STANDARD: decodifica la riposta da PUVN
                    $sql = "SELECT * FROM {$cfg_mod_Admin['file_tab_sys']}
                    WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVN' AND TANR = '{$ris_c}' AND TACOR2 = '{$dom_c}' AND TALINV=''";
                    $stmt = db2_prepare($conn, $sql);
                    $result = db2_execute($stmt);
                    $row = db2_fetch_assoc($stmt);
                    if ($row)
                        $ris_d = $row['TADESC'];
                        else
                            $ris_d = 'NON TROVATA';
                } else {

                    if (is_array($tipo_opz_risposta))
                        $ob_tipo_opz_risposta= json_decode(json_encode($tipo_opz_risposta, JSON_FORCE_OBJECT), false);
                    else
                        $ob_tipo_opz_risposta = $tipo_opz_risposta;
                    
                    if (is_array($ob_tipo_opz_risposta->opzioni)){
                    foreach ($ob_tipo_opz_risposta->opzioni as $t_config) {
                        if (trim($t_config->id) == trim($tipo_c)){

                            if ($t_config->taid == '*RIS*'){
                                $sql = "SELECT * FROM {$cfg_mod_Admin['file_tab_sys']}
                                        WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVN' AND TANR = '{$ris_c}' AND TACOR2 = '{$dom_c}' AND TALINV=''";

                                $stmt = db2_prepare($conn, $sql);
                                $result = db2_execute($stmt);
                                $row = db2_fetch_assoc($stmt);
                                if ($row)
                                    $ris_d = $row['TADESC'];
                                else
                                    $ris_d = 'NON TROVATA';
                            } else {
                                $row = get_TA_sys($t_config->taid, $ris_c, null, null, null, null, 0, '', 'N', 'Y', 'Y');
                                if ($row)
                                    $ris_d = "{$row['text']}";
                                else
                                    $ris_d = "NON TROVATA {$ris_c}";
                            }
                            
                            /*if (strlen(trim($tipo_c)) > 0){
                                $ris_d .= " [tip: {$tipo_c}]";
                                //$ris_c = $m_params->ris_c;
                                
                            }*/
                            
                        }
                    }
                }
                }
            } else {
                $ris_d = '';
            }
            break;
                    
        case 'vend':    //WPI0TA0
            break;
            
        case 'acq':     //WPI1TA0
            break;
            
        case 'util':    //WPI2TA0
            break;
    }
        
    $output['dom_c']  = $dom_c;
    $output['dom_d']  = $dom_d;
    $output['tipo_c']  = $tipo_c;
    $output['ris_c']  = $ris_c;
    $output['ris_d']  = $ris_d;
    
    
    $ar_output = array();
   
    
   // if (isset($ris_c) && strlen($ris_c) > 0){
        if (isset($dom_c) && strlen($dom_c) > 0 && $tipo_opz_risposta->output_domanda == true){
            $ar_output[] = "[" . trim($dom_c) . "]"; 
            $ar_output[] = $dom_d;
        }
        
        if (strlen(trim($tipo_c)) > 0)
            $ar_output[] = $tipo_c . '';
        
        if (!isset($output['risposta_a_capo']) || $output['risposta_a_capo'] != false)
            $ar_output[] .= "<br/>";
        else
            $ar_output[] .= ", ";
       
        if (isset($ris_c) && strlen($ris_c) > 0){
            $ar_output[] = "[" . trim($ris_c) . "]";
            $ar_output[] = $ris_d;
        }
    //}
  
    $output['output'] = implode(" ", $ar_output);

    if ($opz_output['solo_decod'] == true)
        return $output['output'];
    else
        return $output;
    
}

function elenco_ris_dom_ris($file, $dom_c, $tipo_c, $tipo_opz_risposta = array(), $opz_output = array()){
    
}