<?php


class Extjs_Grid extends Extjs_Compo {
    
    function __construct($layout = null, $title = null, $v = null) {
        parent::__construct('grid', $layout, $title, $v);
                
        //$this->set(array());
    }

    
    function set_features($ar_features){

        if (array_key_exists("grouping", $ar_features)) {
            if (!isset($ar_features['grouping']['startCollapsed'])) $ar_features['grouping']['startCollapsed'] = false;
            if (!isset($ar_features['grouping']['groupHeaderTpl'])) $ar_features['grouping']['groupHeaderTpl'] = '{[values.rows[0].data.header]}';
            $this->code['features'][] = array('ftype' => 'grouping', 
                           'startCollapsed' => $ar_features['grouping']['startCollapsed'],
                           'groupHeaderTpl' => $ar_features['grouping']['groupHeaderTpl']);             
        }
        if (array_key_exists("filters", $ar_features)) {
            $this->code['features'][] = array('ftype' => 'filters',
                'autoReload' => false, 'local' => true,  
                'filters' => array('type' => 'boolean', 'dataIndex' => 'visible'));
        }
        
    }
    
} //CLASS *******************************************************

//filter: {type: 'string'}, filterable: true}
/*features: [
              
              {ftype: 'grouping',  startCollapsed: true, groupHeaderTpl: '{[values.rows[0].data.header]}',},
              {ftype: 'filters',
                autoReload: false,
                local: true,
                filters: [{
                    dataIndex: 'articolo',
                    type: 'string'
                }, {
                    dataIndex: 'fornitore',
                    type: 'string'
                }]
            }]
*/





function extjs_grid_proxy($url, $extraParams = array()){
    $c = new Extjs_compo();
    $c->set(array(
        'type'            => 'ajax',
        'url'             => $url,
        'extraParams'     => $extraParams,
        'actionMethods'   => array('read' => 'POST'),
        'reader'          => array('root' => 'items'),
        'doRequest'       => new ApiProcRaw('personalizza_extraParams_to_jsonData')
    ));
    return $c->code();
}

function grid_itemcontextmenu($function_code){
    $ret = "function(view, rec, node, index, event) {";
    $ret .= $function_code;
    $ret .= "}";
    return $ret;
}

function grid_itemclick($function_code){
    $ret = "function(view,rec,item,index,eventObj) {";
    $ret .= $function_code;
    $ret .= "}";
    return $ret;
}

function grid_celldblclick($function_code){
    $ret = "function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {";
    
    $ret .= "var rec = iView.getRecord(iRowEl),
                 col_name = iView.getGridColumns()[iColIdx].dataIndex;    
            ";
    
    $ret .= $function_code;
    $ret .= "}";
    return $ret;
}

function grid_to_json_data($data){
    return $data;
}


function grid_column_h($text, $dataIndex, $wf = null, $tooltip = null, $v = array(), $options = array()){
    $c = new Extjs_compo();
    $c->set(array(
        'text'  => $text,
        'dataIndex' => $dataIndex
    ));
    
    if (!is_null($wf)){
     $t_wf = substr($wf, 0, 1);
     switch ($t_wf){
         case 'w': $c->set(array('width' => (float)substr($wf, 1))); break;
         case 'f': $c->set(array('flex'  => (float)substr($wf, 1))); break;
     }
    }
    
    if (!is_null($tooltip)) $c->set(array('tooltip' => $tooltip));
    if (isset($options['filter_as']))
        $c->set(array('filterable' => true, 'filter' => array('type' => $options['filter_as'])));
    
    if (!is_null($v)) $c->set($v);
    return $c->code();
}


function grid_column_h_f2($text, $dataIndex, $wf = null, $tooltip = null, $v = array(), $options = array()){
    if (is_null($wf)) $wf = 'w100';
    if (is_null($v))  $v = array();
    if (!isset($v['align']))     $v['align']    = 'right';
    if (!isset($v['renderer']))  $v['renderer'] = extjs_code('floatRenderer2');
    return grid_column_h($text, $dataIndex, $wf, $tooltip, $v, $options);
}

function grid_column_h_img($img, $dataIndex, $tooltip, $v = array()){
    $c = new Extjs_compo();
    $c->set(array(
        'text'  => "<img src=" . img_path("icone/48x48/{$img}.png") . " height=20>",
        'width' => 30,
        'align' => 'align',
        'dataIndex' => $dataIndex,
        'tooltip'   => $tooltip
    ));
    $c->set($v);
    return $c->code();
}

function grid_column_h_date($text, $dataIndex, $wf = null, $tooltip = null, $v = array(), $options = array()){
    if (is_null($wf)) $wf = 'w70';
    if (is_null($v))  $v = array();
    if (!isset($v['renderer']))  $v['renderer'] = extjs_code('date_from_AS');
    return grid_column_h($text, $dataIndex, $wf, $tooltip, $v, $options);
}
