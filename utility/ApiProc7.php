<?php

class ApiProc {
  public function __construct() {
  }
    
  public function out_response($data, $response_code = null){
    if (!is_null($response_code) || (isset($data['success']) && $data['success'] === false))
      header("HTTP/1.0 404 Not Found");
    echo ApiProcEncoder::encode($data);
  }
  
  public function out_json_response($data, $response_code = null){
      if (!is_null($response_code) || (isset($data['success']) && $data['success'] === false))
          header("HTTP/1.0 404 Not Found");
          echo ApiProcEncoder::encode($data);
  }
  
  public function get_json_response($data, $response_code = null){      
    return ApiProcEncoder::encode($data);
  }
}


/*
 *  DOWNLOAD FROM balping/json-raw-encoder
 *  per gestire le function(){...} all'interno del json
 *  es: $array = [
        'type' => 'cat',
        'count' => 42,
        'callback' => new ApiProcRaw('function(a){alert(a);}')
    ];
 */

class ApiProcEncoder {
    /**
     * Encode array containing Raw objects to JSON
     *
     * @see json_encode
     *
     * @param mixed $value
     * @param int $options
     * @param int $depth [optional]
     * @return string|false
     */
    static function encode($value){
        $rawObjects = array();
        
        // find raw object items in the input array
        array_walk_recursive($value, function($item) use (&$rawObjects){
            //if(is_object($item) && is_a($item, ApiProcRaw::class)){
            if(is_object($item) && is_a($item, 'ApiProcRaw')){
                $rawObjects[] = &$item;
            }
        });
            
        $encoded = acs_je($value);            
        return ApiProcReplacer::replace($encoded, $rawObjects);
    }
}



class ApiProcRaw implements \JsonSerializable {
    /**
     * Unique identifier. Gets replaced with raw value
     * after using built-in json_encode
     * @var string
     */
    protected $id;
    
    /**
     * Raw value. This is passed to json without any modification
     * (i.e. without escaping, etc.)
     * @var string
     */
    protected $value;
    
    public function __construct(string $value){
        $this->value = $value;
        $this->id = bin2hex(random_bytes(20));
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getValue(){
        return $this->value;
    }
    
    public function jsonSerialize(){
        return $this->getId();
    }
}




class ApiProcReplacer {
    /**
     * Replace unique ids with raw values in an encoded json
     *
     * @param  string $json       encoded json
     * @param  array  $rawObjects
     * @return string
     */
    static function replace($json, array $rawObjects){
        $encoded = &$json;
        
        // replace unique strings with raw values
        foreach ($rawObjects as $rawObject) {
            $encoded = str_replace(
                '"'. $rawObject->getId() . '"',
                $rawObject->getValue(),
                $encoded
                );
        }
        
        return $encoded;
    }
}

