<?php

/*
 /usr/local/zendphp7/bin/php-cli /www/zendphp7/htdocs/acs_portal/utility/varie/richiedi_sincro_cliente_per_cc_SN0_mancante.php
 */


error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors','On');

$disabilita_auth = 'Y';

require_once(dirname(__FILE__) . '/../../config.inc.php');
require_once(dirname(__FILE__) . '/../../class/class.SpedHistory.php');

echo "\nStart\n";

//Autentificazione
session_destroy();
$auth = Auth::get_session_auth();
$auth->set_authenticate('ACS', 'AMM'); //utente, profilo


// --------- Connession locale a As/400 ------------------
$i5_host	= '*LOCAL';
$i5_user	= 'WEBACS';
$i5_psw		= 'webacs';

$namingMode = DB2_I5_NAMING_ON; // ON or OFF ??????
$conn = db2_pconnect($i5_host, $i5_user, $i5_psw);


$sql = "
SELECT TADT, TACOR1, GCPROG
FROM QS36F.XX0S2TA0
INNER JOIN QS36F.WPI0GC0
  ON TADT = GCDT AND TACOR1 = GCCDCF
LEFT OUTER JOIN QS36F.WPI0CC0
  ON GCDT = CCDT AND GCPROG = CCPROG AND CCCCON = 'SN0'
WHERE TADT = '1 ' AND TAID = 'BCCO' AND TANR = 'SN0'
  AND CCCCON IS NULL
ORDER BY TACOR1
LIMIT 50
";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);

while ($row = db2_fetch_assoc($stmt)) {
    print_r($row);
    $cod_cli = trim($row['TACOR1']);
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'EXP_ANAG_CLI',
            "vals" => array(
                "RICLIE" => sprintf("%09d", $cod_cli)
            )
        )
        );
    
}

echo "\n..Fine\n";


