<?php

function crea_json_chart($campo_cod, $campo_desc){
 global $_REQUEST;

$chart= "{
	xtype: 'chart',
	animate: true,
	shadow: true,
	height: 240,
	flex: 1,
		
	store: {
		xtype: 'store',
		autoLoad:true,
		proxy: {
			url: '{$_SERVER['PHP_SELF']}?fn=get_json_data_chart1',
			method: 'POST',
			type: 'ajax',

			//Add these two properties
			actionMethods: {
				type: 'json',
				read: 'POST'
			},

			extraParams: {
		        campo_cod: '{$campo_cod}',
				campo_desc: '{$campo_desc}',
				open_request: ".acs_je(acs_m_params_json_decode())."
			},
			
			doRequest: personalizza_extraParams_to_jsonData,
		
			reader: {
				type: 'json',
				method: 'POST',
				root: 'root'
			},
				
		},

		fields: ['COD','DESC', 'NR']
			
	}, //store
		
		
	insetPadding: 25,
	theme: 'Base:gradients',


	series: [{
		type: 'pie',
		field: 'NR',
		showInLegend: true,
		donut: true,
		tips: {
			trackMouse: true,
			width: 260,
			height: 38,
			renderer: function(storeItem, item) {
				//calculate percentage.
				var total = 0;
				storeItem.store.each(function(rec) {
					total += parseFloat(rec.get('NR'));
				});
					this.setTitle(storeItem.get('COD') + ' - ' + storeItem.get('DESC') + ': ' + Math.round(storeItem.get('NR') / total * 100) + '% [' + floatRenderer0(storeItem.get('NR')) + ']');
			}
		},
		highlight: {
			segment: {
				margin: 20
			}
		},
		label: {
			field: 'COD',
			display: 'rotate',
			contrast: true,
			font: '11px Arial',
			/*renderer: function (label){
				// this will change the text displayed on the pie
				//var cmp = Ext.getCmp('chartCmp2'); // id of the chart
				var index = cmp.store.findExact('COD', label); // the field containing the current label
				var data = cmp.store.getAt(index).data;
				return data.COD; // the field containing the label to display on the chart
			}*/
		}
	}]
}";
	

return $chart;
}