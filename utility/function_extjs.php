<?php
require_once("extjs_php_functions/base.php");
require_once("extjs_php_functions/form.php");
require_once("extjs_php_functions/grid.php");
require_once("extjs_php_functions/tree.php");
require_once("extjs_php_functions/combo.php");
require_once("extjs_php_functions/datefield.php");
require_once("extjs_php_functions/timefield.php");
require_once("extjs_php_functions/checkbox.php");
require_once("extjs_php_functions/button.php");



function dx_mobile(){
  $code = "{ //tasto dx on mobile
    xtype:'actioncolumn', width:30,
    text: '<img src=" . img_path("icone/24x24/button_grey_play.png") . " width=20>',
    tdCls: 'tdAction', tooltip: 'Opzioni disponibili',
    menuDisabled: true, sortable: false,
    
    items: [{
                icon: " . img_path("icone/24x24/button_grey_play.png") . ",
	                handler: function(view, rowIndex, colIndex, item, e, record, row) {
	                	if (view.getSelectionModel().getSelection().length == 0)
	                		view.getSelectionModel().select([record]);
	                	view.fireEvent('itemcontextmenu', view, record, null, colIndex, e);		                	
	                }
        }]
    }";
  return $code;
}
  
	          


//scrittiura immediata codice json
function echo_je($data){
    $a = new ApiProc();
    $a->out_json_response($data);
}

//ritorno codice json
function code_je($data){
    $a = new ApiProc();
    return ApiProcEncoder::encode($data);    
}

//rendi non obbligatorio
function n_o($ar){
    $ar['allowBlank'] = true;
    return $ar;
}

function a15($ar){
    $ar['anchor'] = '-15';
    return $ar;
}



//*************************************************
// Utility
//*************************************************

function extjs_compo($xtype = null, $layout = null, $title = null, $v = null){
    $ret = new Extjs_compo($xtype, $layout, $title, $v);
    return $ret->code();
}

function extjs_code($code){
    return new ApiProcRaw($code);
}



function extjs_url($fn = null, $url = null){
  $url 		= (is_null($url)) ? $_SERVER['PHP_SELF'] : $url;
  $fn 		= (is_null($fn)) ?	'' : "fn={$fn}";
  return "{$url}?{$fn}";
}




function layout_ar($type){
    return array('type' => $type, 'pack' => 'start', 'align' => 'stretch');
}








//--------------------------------------------------------------------------------------
//caselle codice articolo (restituiscono un singolo params, mantenendo gli spazi
//--------------------------------------------------------------------------------------
//a blocchi (es. per codici articolo)
function extjs_cod_char($length, $label, $field, $values = array()){
    $ret = array();
    
    $ret = array(
        'xtype'         => 'fieldcontainer',
        'layout'        => layout_ar('hbox'),
        'items'         => array()
    );
    
    $ret['items'][] = array(
        'xtype'         => 'hiddenfield',
        'name'          => $field,
        'value'         => $values[$field]
    );
    
    $ret['items'][] = _extjs_single_char_label($label);
    for ($i = 1; $i<=$length; $i++){
        $ret['items'][] = _extjs_single_char_field($i, $label, $field, $values);
    }
    return $ret;
}


function extjs_cod_char_intestazione($length){
    $ret = array();
    
    $ret = array(
        'xtype'         => 'fieldcontainer',
        'layout'        => layout_ar('hbox'),
        'items'         => array()
    );
    
    $ret['items'][] = _extjs_single_char_label('');
    for ($i = 1; $i<=$length; $i++){
        $ret['items'][] = array(
            'xtype'         => 'displayfield',
            'width'         => 20, 'fieldStyle' => 'font-size:10px;',
            'value'         => $i
        );
    }
    return $ret;
}



function _extjs_single_char_label($label){
    $ret = array(
        'xtype'         => 'displayfield',
        'width'         => 120,
        'value'         => $label
    );
    return $ret;
}


function _extjs_single_char_field($i, $label, $field, $values){
    if (strlen(rtrim($values[$field])) >= $i)
        $v = substr($values[$field], $i-1, 1);
        else
            $v = '';
            
            $ret = array(
                'xtype'         => 'textfield',
                'width'         => 20,
                'maxLength'     => 1, 'enforceMaxLength'    => true,
                'value'         => trim($v),
                'listeners'     => extjs_code("{
                    change: function(field){
                        var value = this.getValue().toString();
    					if (value.length == 1) {
                            var nextfield = field.nextSibling();
                            nextfield.focus(true, 0);
                          }
                    
                        field.setValue(field.getValue().toUpperCase());
                    
                        //rigenero il valore del campo hidden
                        // (il campo hidden all'interno del mio fieldconainer)
                        var fc = field.up('fieldcontainer');
                        var fh = fc.down('hiddenfield');
                    
                        var new_value = '';
                    	Ext.each(fc.query('textfield'), function(tf) {
                    		if (!Ext.isEmpty(tf.getValue()))
                                new_value = new_value + tf.getValue().substring(0, 1)
                            else
                                new_value = new_value + ' ';
                    	});
	                    fh.setValue(new_value);
                    
                    } //change
                }")
            );
            return $ret;
}







//**************************************************************************************
function extjs_grid_store($url, $fn, $fields_str = '', $extraParams = "{}", $p = array(), $add_text = ''){
//**************************************************************************************
	$url 		= (is_null($url)) ? $_SERVER['PHP_SELF'] : $url;
	$fn 		= (is_null($fn)) ?	'' : "fn={$fn}";
	$fields 	= (!is_null($fields_str)) ? implode(",", array_map('u_to_j', explode(" ", $fields_str))) : $p['fields'];  
?>
{
	xtype: 'store',
	autoLoad:true,
	proxy: {
		url: '<?php echo "{$url}?{$fn}" ?>',
		method: 'POST',	type: 'ajax', actionMethods: {read: 'POST'},
		extraParams: <?php echo $extraParams; ?>,
        doRequest: personalizza_extraParams_to_jsonData,
		reader: {type: 'json',method: 'POST',root: 'root'}
	},
    fields: [<?php echo $fields; ?>]    	
    <?php
    	if (!is_null($p['groupField'])) echo ", groupField: " . j($p['groupField']);
    	if (!is_null($p['groupOnSort'])) echo ", groupOnSort: " . j($p['groupOnSort']);
    	if (!is_null($p['remoteGroup'])) echo ", remoteGroup: " . j($p['remoteGroup']);
    	
    	if (strlen($add_text) > 0)
    		echo ", " . $add_text;
    ?>
} //store
<?php }


//***************************************************************************
// Utility
//***************************************************************************
function u_to_j($v){
	return j(trim($v));
}
