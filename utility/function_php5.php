<?php

require_once("ApiProc5.php");

function acs_emogrify($html, $css){
    require_once 'emogrifier.php5.php';
    $emogrifier = new Emogrifier($html, $css);
    return $emogrifier->emogrify();
}



//sostituisce:
//from:	json_decode($HTTP_RAW_POST_DATA) 	//php5
//to:	acs_m_params_json_decode() 			//php7
function acs_m_params_json_decode(){
	global $acs_m_params;
	if (!isset($acs_m_params))
		$acs_m_params = json_decode(file_get_contents("php://input")); //php7
	return $acs_m_params;
}
function acs_raw_post_data(){
	global $acs_raw_post_data;
	if (!isset($acs_raw_post_data))
		$acs_raw_post_data = file_get_contents("php://input"); //php7
		return $acs_raw_post_data;
}



/* UTF8_ENCODE */

function acs_toDb($v){
    return utf8_decode($v);
}

function acs_u8e($v){
    return utf8_encode($v);
}

function acs_je($value, $pretty = false){
    return json_encode($value);
}


//sembra dare errore in php5 (almeno su midi)
//function acs_decamelize($string) {
//    return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $string));
//}
