/***********************************************************
*** Istruzion bottoni fascetta excel e invio email *********
************************************************************/

Ext.onReady(function() {

	//invio in formato excel
    if (Ext.get("p_send_excel") != null){		
	  	Ext.get("p_send_excel").on('click', function(){
	  		Ext.getBody().mask('Loading... ', 'loading').show();		  	
	  		 html_text = document.getElementById('my_content').outerHTML;
	  		 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore
	  		 style_text = document.getElementsByTagName('style')[0].outerHTML;	 
	  		
	        Ext.Ajax.request({
	            url : '../base/send_excel.php' ,
	            method: 'POST',
	            jsonData: {html_text: html_text, style_text: style_text},
	            success: function ( result, request) {
					Ext.getBody().unmask();	                
					
					output = result.responseText;
					output = 'bbbbbbbbbbbb';
					
	            	window.open('data:application/vnd.ms-excel,' + escape(output));
					///window.open('data:application/vnd.ms-excel;base64,' + $.base64.encode(html);
	            },
	            failure: function ( result, request) {
	            }
	        });  			  		
	  	});
    }

	//invio contenuto pagina come e-mail
  if (Ext.get("p_send_email") != null){
	
  	Ext.get("p_send_email").on('click', function(){
	//html_text = document.getElementsByTagName('body')[0].innerHTML;
	 html_text = document.getElementById('my_content').outerHTML;
	 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore
//per test	 	 
//	 html_text = html_text.replace(/&amp;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&gt;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&lt;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&�/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&@/g, ""); //replace altrimenti va in errore	 
	 html_text = html_text.replace(/\<br\>/g, "<br/>"); //replace altrimenti va in errore
	 html_text = html_text.replace(/\<hr\>/g, "<hr/>"); //replace altrimenti va in errore

	 style_text = document.getElementsByTagName('style')[0].outerHTML;	 
	 html_text += style_text;
	 //html_text = style_text;
	 console.log(html_text);
	 
	var n_doc = document.implementation.createDocument ('http://www.w3.org/1999/xhtml', 'html',  null);
    n_doc.documentElement.innerHTML = html_text;
    
	html_text = n_doc.getElementsByTagName('html')[0].innerHTML;  	
  		
 		var f = Ext.create('Ext.form.Panel', {
        frame: true,
        title: '',        
        bodyPadding: 5,
        url: 'acs_op_exe.php',
        fieldDefaults: {
//            labelAlign: 'top',
            msgTarget: 'side',
            labelWidth: 55,
            anchor: '100%'            
        },
		layout: {
            type: 'vbox',
            align: 'stretch'  // Child items are stretched to full width
        },        

        items: [{
        	xtype: 'hidden',
        	name: 'fn',
        	value: 'exe_send_email'
        	}, {
            xtype: 'combo',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: [ 'email', 'descr' ],
                data: data_email_json
            }),
            displayField: 'descr',
            valueField: 'email',
            fieldLabel: 'A',
            queryMode: 'local',
            selectOnTab: false,
            name: 'to'
        }, {
            xtype: 'textfield',
            name: 'f_oggetto',
            fieldLabel: 'Oggetto',
            anchor: '96%',
		    allowBlank: false,
		    value: value_email_subject	    
        }
        , {
            xtype: 'htmleditor',
            //xtype: 'textareafield',
            name: 'f_text',
            fieldLabel: 'Testo del messaggio',
            layout: 'fit',
            anchor: '96%',
            height: '100%',
		    allowBlank: false,
		    value: html_text,
		    flex: 1, //riempie tutto lo spazio
			hideLabel: true,
            style: 'margin:0' // Remove default margin		              
        }],

        buttons: [{
            text: 'Invia',
            scale: 'large',
            iconCls: 'icon-email_send-32',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var m_win = this.up('window');	            	
	                form.submit(
								{
		                            //waitMsg:'Loading...',
		                            success: function(form,action) {		                            	
										m_win.close();		        
										// Ext.MessageBox.alert('Avviso', 'Email inviata correttamente');
										acs_show_msg_info('Invio email completato');										                    	
		                            },
		                            failure: function(form,action){
		                                //Ext.MessageBox.alert('Erro');
		                            }
		                        }	                	
	                );
		            //this.up('window').close();            	                	                
	             }            
        		}]
    });


		// create and show window
		var win = new Ext.Window({
		  width: 800
		, height: 380
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Invia e-mail'
		, layout: 'fit'
		, border: true
		, closable: true
		, items: f
		});
		win.show().maximize();  
		    	    		    		
    	});		
		}
	});



