 function tabpanel_load_record_in_form(tabpanel, record, reset_original_value){
	 
	console.log('on tabpanel_load_record_in_form'); 
	 
	if (typeof(reset_original_value)=== 'undefined')
		reset_original_value = true;
	 
	Ext.each(tabpanel.query('[xtype="form"]'), function(p) {
		p.getForm().setValues(record);
				
		if (reset_original_value){
			//reset valore originale in base all'ultimo caricato
			var fs = p.query("field")		
			Ext.each(fs, function(f) {                       		
				f.initValue();	//imposta originalValue
				
				f.resetOriginalValue( );
				
				//if(f.isDirty){
                //    f.setValue(f.originalValue);
                //}
			});
		}
    });
 }
 
 
 function check_tabpanel_change(tabp, panTo, panFrom) {
		var activeTabIndex = tabp.items.findIndex('id', panFrom.id);                       
        
		//Verifico se nel panel in uscita (From) avevo fatto delle modifiche non non salvate						
		if (panFrom.xtype != 'form') return true;	
					
		var fm = panFrom.getForm();
		var is_modified = false;
		var v_modified = {};
                           
       	Ext.each(panFrom.query("field"), function(f) {                       		
			if (f.wasDirty && (!Ext.isEmpty(f.value) || !Ext.isEmpty(f.originalValue))){
				console.log('modified:');
				console.log(f);
				is_modified = true;
			}
	   	});                   
       
       	if (is_modified){
			Ext.Msg.confirm('Richiesta conferma', 'Attenzione!!!! Campi modificati non ancora salvati.<br/>Proseguire ugualmente?' , function(btn, text){
   		    	if (btn == 'yes'){
					return true;				    
	    		} else
		    		tabp.setActiveTab(activeTabIndex);
	   		  });                       
       	} //if is_modified     
      
	} //beforechange
 


if(typeof String.prototype.rtrim !== 'function') {
   String.prototype.rtrim = function() {
      return this.replace(/\s+$/,"");
   }
}


if(typeof String.prototype.ltrim !== 'function') {
   String.prototype.ltrim = function() {
	   return this.replace(/^\s+/,"");
   }
}
//aggiunta voce di menu in base a raggruppamento voci
function add_in_submenu(voci_menu, raggr_voci_menu, submenu_id, code){
	var submenu_trovato = false;
	Ext.each(voci_menu, function(menu_item) {
		if (menu_item.submenu_id == submenu_id){
			submenu_trovato = true;
			menu_item.menu.items.push(code);
		}
	});

	if (!submenu_trovato){
		voci_menu.push({submenu_id: submenu_id, text: raggr_voci_menu[submenu_id].text, iconCls:raggr_voci_menu[submenu_id].iconCls, menu: {items: [code]}});
	}
}


function acs_form_setValues(form, data){

	form.getForm().setValues(data);
	//gestione combo domanda/risposta (dom_ris) -------------------------------
	var dom_ris_fields = form.query('[acs_type="dom_ris"]');
	Ext.each(dom_ris_fields, function(fc) {
		var	g_dom_c = fc.down('[acs_type="dom_c"]'),
			g_dom_d = fc.down('[acs_type="dom_d"]'),
			g_ris_c = fc.down('[acs_type="ris_c"]'),
			f_output = fc.down('[acs_type="out_d"]'),
			output = '';
		
		if (f_output.rawValue != '')
			f_output.setValue(output);		
		
		if (!Ext.isEmpty(g_dom_c.dom_c_namefield))
			dom_c = data[g_dom_c.dom_c_namefield];
		else
			dom_c = g_dom_c.getValue();
		 
		if (!Ext.isEmpty(g_ris_c)) ris_c = g_ris_c.getValue();
				
		if (!Ext.isEmpty(dom_c) || !Ext.isEmpty(ris_c)){
			
			var to_params = {file_TA: fc.file_TA, dom_c: dom_c, ris_c: ris_c};
			if (!Ext.isEmpty(fc.down('[acs_type="tipo_c"]'))){				
				to_params.tipo_c = fc.down('[acs_type="tipo_c"]').getValue();
				to_params.tipo_opz_risposta = fc.down('[acs_type="tipo_c"]').tipo_opz_risposta;
			}
			
			//ToDo: finire di capire se serve
			/////to_params.combo_params = fc.combo_params;
			
			std_ajax_request('../base/decodifica_dom_ris.php?fn=decod', to_params, function(returnData, p){		
				if (!Ext.isEmpty(dom_c)) output += '[' + dom_c + '] '; 
				output += returnData.dom_d;
				if (!Ext.isEmpty(dom_c) && !Ext.isEmpty(g_dom_d))
					g_dom_d.setValue(returnData.dom_d);
				
				if (!Ext.isEmpty(ris_c)){
					if (Ext.isEmpty(fc.combo_params.risposta_a_capo) || fc.combo_params.risposta_a_capo != false)
						output += '<br/>';
					output += '[' + ris_c + '] ';
				}
				
				output += returnData.output;				
				fc.down('[acs_type="ris_d"]').setValue(returnData.ris_d);							
				fc.down('[acs_type="out_d"]').setValue(returnData.output);
	  		})							
		} else {
			
		}	
	});	
	
	//gestione combo regole configuratore (regola_config) -------------------------------
	var dom_ris_fields = form.query('[acs_type="regola_config"]');
	Ext.each(dom_ris_fields, function(fc) {		
		var	f_var = fc.down('[acs_type="f_var"]').getValue(),
			f_tp = fc.down('[acs_type="f_tp"]').getValue(),
			f_van = fc.down('[acs_type="f_van"]').getValue(),
			f_os = fc.down('[acs_type="f_os"]').getValue(),
			f_output = fc.down('[acs_type="out_d"]'),
			output = '';
		
		if (f_output.rawValue != '')
			f_output.setValue(output);
		
		var to_params = {f_var: f_var, f_tp: f_tp, f_van: f_van, f_os: f_os};
		to_params.combo_params = fc.combo_params;
		
		if (!Ext.isEmpty(f_var) || !Ext.isEmpty(f_tp) || !Ext.isEmpty(f_van) || !Ext.isEmpty(f_os)){
			std_ajax_request('../base/decodifica_regola_config.php?fn=decod', to_params, function(returnData, p){ 
				output += returnData.output;
				fc.down('[acs_type="out_d"]').setValue(output);
	  		});
		} else {
			
		}	
	});	
	
	
}




function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader',
        iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    console.log('aaaa');
    iframe.src = url;
    console.log(iframe);
    console.log('bbbb');
    
};



function downloadFile(filePath){
    var link=document.createElement('a');
    link.href = filePath;
    link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
    link.click();
}


function acs_show_msg_error(txt, sub, title){
	
	msg = txt;

	if (typeof(sub)=== 'undefined')
		sub = 'Opzione non ammessa';
	
	if (typeof(title)=== 'undefined')
		title = 'Segnalazione anomalie operative';	
	
	Ext.Msg.show({
		   title: title,
		   cls: 'acs_error',
		   msg: msg + '<br><span class=sub>(' + sub + ')</span>',
		   buttons: Ext.MessageBox.OK,								   
		   icon: Ext.MessageBox.ERROR
		});								
	
}


function acs_show_msg_info(txt, sub, title){
	
	msg = txt;

	if (typeof(sub)=== 'undefined')
		sub = 'Operazione completata';
	
	if (typeof(title)=== 'undefined')
		title = 'Notifica applicazione';	
	
	Ext.Msg.show({
		   title: title,
		   cls: 'acs_error',
		   width: 300, maxHeight : 500,
		   msg: msg + '<br><span class=sub>(' + sub + ')</span>',
		   buttons: Ext.MessageBox.OK,								   
		   icon: Ext.MessageBox.INFO
		});								
	
}

function acs_show_msg_confirm(txt, sub, title){
	
	msg = txt;

	if (typeof(sub)=== 'undefined')
		sub = 'Operazione completata';
	
	if (typeof(title)=== 'undefined')
		title = 'Notifica applicazione';	
	
	Ext.Msg.confirm({
		   title: title,
		   cls: 'acs_error',
		   width: 300,
		   msg: msg + '<br><span class=sub>(' + sub + ')</span>',
		   buttons: Ext.MessageBox.OK,								   
		   icon: Ext.MessageBox.INFO
		});								
	
}





function window_to_center(win){
	dskX = Ext.getBody().getViewSize().width-20;
    dskY = Ext.getBody().getViewSize().height-20;
    
    winX = win.getWidth();
    winY = win.getHeight();

    pos_X = (dskX - winX)/2; 
    pos_Y = (dskY - winY)/2;
   
    //no so perche' ma (almeno in INFO) non centra in verticale
    win.setPosition(pos_X, 50);
}



function acs_show_win_std(titolo, url, jsonData, width, height, listeners, iconCls, maximize, loadBodyMask, open_vars, help_codice){
	
	
	if(typeof(listeners)==='undefined' || listeners==null) listeners = {};		

	if (!typeof(help_codice) === 'undefined'){
	 var tools = [
				{
				    type:'help',
				    tooltip: 'Help',
				    handler: function(event, toolEl, panel){
				        show_win_help('MAIN_SEARCH');
				    }
				}								
			]
	} else var tools = [];
	
	//carico la form dal json ricevuto da php
	Ext.Ajax.request({
	        url        : url,
	        method     : 'POST',
	        waitMsg    : 'Data loading',
	        jsonData: jsonData,	        
	        success : function(result, request){
	        	try { 
	        		var jsonData = Ext.decode(result.responseText);
	        	} catch(err) {
	        		Ext.Msg.alert('Errore', 'Errore nel caricamento della pagina');
                    //Ext.Msg.alert('Errore', err.message);
                }
	            
	            //definisco height/width/icon/title
	            var m_width   = 600,
	                m_height  = 400,
	                m_iconCls = 'iconSpedizione',
	                m_title   = '',
	                m_loadBodyMask = 'N',
	                m_maximize = 'N';

	            if (!Ext.isEmpty(jsonData.m_win)){
	            	if (!Ext.isEmpty(jsonData.m_win.width))   m_width  = jsonData.m_win.width;	            
	            	if (!Ext.isEmpty(jsonData.m_win.height))  m_height = jsonData.m_win.height;
	            	if (!Ext.isEmpty(jsonData.m_win.iconCls)) m_iconCls = jsonData.m_win.iconCls;
	            	if (!Ext.isEmpty(jsonData.m_win.title))   m_title   = jsonData.m_win.title;
	            	if (!Ext.isEmpty(jsonData.m_win.loadBodyMask))   m_loadBodyMask   = jsonData.m_win.loadBodyMask;
	            	if (!Ext.isEmpty(jsonData.m_win.maximize))   m_maximize   = jsonData.m_win.maximize;
	            }
	            
	            if(!Ext.isEmpty(width))   m_width   = width;
	            if(!Ext.isEmpty(height))  m_height  = height;
	            if(!Ext.isEmpty(iconCls)) m_iconCls = iconCls;
	            if(!Ext.isEmpty(titolo))  m_title   = titolo;
	            if(!Ext.isEmpty(loadBodyMask))  m_loadBodyMask   = loadBodyMask;
	            if(!Ext.isEmpty(maximize))  m_maximize   = maximize;
	            
	            if (!Ext.isEmpty(jsonData.m_win)){
	            	if (!Ext.isEmpty(jsonData.m_win.title_pre)) m_title = jsonData.m_win.title_pre + ' - ' + m_title;
	            	if (!Ext.isEmpty(jsonData.m_win.title_suf)) m_title = m_title + ' - ' + jsonData.m_win.title_suf;
	            }

	            
	            
	        	if (m_loadBodyMask == 'Y')
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	            
	        	var print_w = new Ext.Window({
		      		  width:   m_width
		      		, height:  m_height
		          	, iconCls: m_iconCls
		      		, plain: true
		      		, title: m_title
		      		, layout: 'fit'
		      		, border: true
		      		, closable: true
		      		, listeners: listeners
		          	, maximizable: true
		          	, open_vars: open_vars
		          	, tools: tools
		      	});	
		      	
		      	if (m_maximize == 'Y')
		      		print_w.show().maximize();
		      	else
		      		print_w.show();
	            
	            print_w.add(jsonData.items);
	            print_w.doLayout();
	        },
	        failure    : function(result, request){
	            Ext.Msg.alert('Message', 'No data to be loaded');
	        }
	    });	
}





function acs_show_panel_std(url, tab_id, jsonData, listeners, loadBodyMask){
	
	if(typeof(listeners)==='undefined' || listeners==null) listeners = {};		
	
	if (loadBodyMask == 'Y')
		Ext.getBody().mask('Loading... ', 'loading').show();	
	

	mp = Ext.getCmp('m-panel');
	mp_tab = Ext.getCmp(tab_id);

	if (mp_tab){
		mp_tab.show();		    	
	} else {	
		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : url,
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        jsonData: jsonData,	        
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            mp.add(jsonData.items);
		            mp.doLayout();
		            mp.show();
		            
		        	//mostro l'ultimo tab aggiunto
		            mp.setActiveTab(mp.items.length - 1);
		            
		            //mp_tab = Ext.getCmp(tab_id).show();
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		    });
	}	
}










function perc0(value) {
    if (value) {
        var val = floatRenderer0(value * 100);

        return addSeparatorsNF(val, '.', ',', '.') + '%';
    }
    else return "";
}


function perc1(value) {
    if (value) {
        var val = floatRenderer1(value * 100);

        return addSeparatorsNF(val, '.', ',', '.') + '%';
    }
    else return "";
}

function perc2(value) {
    if (value) {
        var val = floatRenderer2(value * 100);

        return addSeparatorsNF(val, '.', ',', '.') + '%';
    }
    else return "";
}


function floatRenderer0(value) {
    if (value) {
        var val = parseFloat(value).toFixed(0);

        return addSeparatorsNF(val, '.', ',', '.');
    }
    else return "";
}	

function floatRenderer1(value) {
    if (value) {
        var val = parseFloat(value).toFixed(1);

        return addSeparatorsNF(val, '.', ',', '.');
    }
    else return "";
}	

function floatRenderer2(value) {
    if (value) {
    	var val = parseFloat(value).toFixed(2);
         return addSeparatorsNF(val, '.', ',', '.');
    }
    else return "";
}

function floatRenderer3(value) {
    if (value) {
        var val = parseFloat(value).toFixed(3);

        return addSeparatorsNF(val, '.', ',', '.');
    }
    else return "";
}

function floatRenderer4(value) {
	value = parseFloat(value);
    if (!isNaN(value)) {
        var val = parseFloat(value).toFixed(4);

        return addSeparatorsNF(val, '.', ',', '.');
    }
    else return "";
}	

function floatRenderer5(value) {
	value = parseFloat(value);
    if (!isNaN(value)) {
        var val = parseFloat(value).toFixed(5);

        return addSeparatorsNF(val, '.', ',', '.');
    }
    else return "";
}	

function floatRenderer0N(value) {
    if (value && parseFloat(value) != 0) return floatRenderer0(value);
    return "";
}	
function floatRenderer1N(value) {
    if (value && parseFloat(value) != 0) return floatRenderer1(value);
    return "";
}
function floatRenderer2N(value) {
    if (value && parseFloat(value) != 0) return floatRenderer2(value);
    return "";
}
function floatRenderer3N(value) {
    if (value && parseFloat(value) != 0) return floatRenderer3(value);
    return "";
}
function floatRenderer4N(value) {
    if (value && parseFloat(value) != 0) return floatRenderer4(value);
    return "";
}



function toFixedTruncate (num, places) {
	return Math.trunc(num * Math.pow(10, places)) / Math.pow(10, places);
}



function date_from_AS(value, p,record){		
	return acs_date_from_AS(value);
}

function js_date_from_AS(value, p,record){
	v = value + '';	
	d = new Date(v.substr(0,4), v.substr(4,2) - 1, v.substr(6,2));
	return d;
}

function acs_date_from_AS(value, format){
	if(typeof(format)==='undefined')  format  = 'd/m/y';	
	v = value + ''	    
	if (v.length == 8){
		
		if(v == '00000000')
			return '';
		if(v == '99999999')
			return '31/12/2099';
		
		d = new Date(v.substr(0,4), v.substr(4,2) - 1, v.substr(6,2));
		return Ext.Date.format(d, format);	    				
	}
}




function datetime_from_AS_concat(value, date_format, data_riferimento){
	if(typeof(date_format)==='undefined')  date_format  = 'd/m/y';
	if(typeof(data_riferimento)==='undefined') data_riferimento = -1;	
	v = value + '';
	if (v.length == 0)	return datetime_from_AS(0, 0, date_format);
	
	value = parseFloat(value);	
	value = ((value) / 1000000).toFixed(6);
	data_ora_ar = value.split('.');
	data = data_ora_ar[0];
	ora  = data_ora_ar[1];

//	data = parseInt(value);	
//	ora  = parseInt((value - data)*1000000);
	
	if (data == data_riferimento)
		return time_from_AS(ora, date_format); //stampo solo l'ora
	
	return datetime_from_AS(data, ora, date_format); //TODO OOOO	
}




function datetime_from_AS(value, ora, format){
	if(typeof(format)==='undefined')  format  = 'd/m/y';	
	v = value + '';
	ora = ora + '';    
	if (v.length == 8){
		var createdDateTo = acs_date_from_AS(value, format);
		
		createdDateTo = createdDateTo + " ";
		
		if (ora.length == 3)
			createdDateTo = createdDateTo + " " + ora.substr(0,1) + ":" + ora.substr(1,2);
		if (ora.length == 4)
			createdDateTo = createdDateTo + " " + ora.substr(0,2) + ":" + ora.substr(2,2);
		if (ora.length == 5)
			createdDateTo = createdDateTo + " " + ora.substr(0,1) + ":" + ora.substr(1,2);
		if (ora.length == 6)
			createdDateTo = createdDateTo + " " + ora.substr(0,2) + ":" + ora.substr(2,2);				
							
    	return createdDateTo;			
	}
}

function time_from_AS(ora, format){
	ora = ora + '';    
	createdDateTo = '';
		//perchè usare variabile vuota e lo spazio?
		if (ora.length == 3)
			createdDateTo = createdDateTo + " " + ora.substr(0,1) + ":" + ora.substr(1,2);
		if (ora.length == 4)
			createdDateTo = /*createdDateTo + " " +*/ ora.substr(0,2) + ":" + ora.substr(2,2);
		if (ora.length == 5)
			createdDateTo = createdDateTo + " " + ora.substr(0,1) + ":" + ora.substr(1,2);
		if (ora.length == 6)
			createdDateTo = createdDateTo + " " + ora.substr(0,2) + ":" + ora.substr(2,2);				
							
    	return createdDateTo;			

}




function addSeparatorsNF(nStr, inD, outD, sep)
{
	nStr += '';
	var dpos = nStr.indexOf(inD);
	var nStrEnd = '';
	if (dpos != -1) {
		nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
		nStr = nStr.substring(0, dpos);
	}
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(nStr)) {
		nStr = nStr.replace(rgx, '$1' + sep + '$2');
	}
	return nStr + nStrEnd;
}





/****************************************************************************
 *  PER GRAFICI
 ****************************************************************************/

function raggruppa_dati(array_dato, ar_etichetta, ar_serie, somma_dato, ritorna, m_array_filtri){
	 array_dati = {};
	 array_fields = [];
	 val_etichette = new Array();    
	 Ext.each(array_dato, function(record){
		
		 myarray = new Array();	
		 Ext.Object.each(record, function(key, value){
				 myarray[key] = value;										
		});	
		
		validazione = controllo_filtri(m_array_filtri['f_aspe'], m_array_filtri['f_divisione'], m_array_filtri['f_stato_ordine'], m_array_filtri['f_tipo_ordine'], m_array_filtri['tipo'], m_array_filtri['f_tipologia_ordine'], m_array_filtri['f_agente'], record);
	
		if (validazione)
		{		
			 val_serie = get_implode_array(myarray, ar_serie);						 				 
			 val_etichette = get_implode_array(myarray, ar_etichetta);												 
			 vari = Ext.Array.contains(array_fields, val_serie);
			 if(vari == false){
					array_fields.push(val_serie);
			 }
																										
			 if (typeof(array_dati[val_etichette]) === 'undefined'){
					array_dati[val_etichette] = new Array();    		 		
			 }	
			 
			 if (typeof(array_dati[val_etichette][val_serie]) === 'undefined'){
						array_dati[val_etichette][val_serie] = 0		     		 				
			 }	
			 array_dati[val_etichette][val_serie] += parseFloat(myarray[somma_dato]);
		}

									
	 });			         		     		     			     			  		     	       		    	  	     	     			     	    
		ecc = ar_etichetta;
		ar = ecc.join('-');
				
		mioarray = [];
		Ext.Object.each(array_dati, function(key, value){
				ser = key;
				v = {};
				v[ar] = ser;
				arr = value;			     	
				Ext.each(array_fields, function(r){	 
							v[r] = 0;				     	
				  });						      	
				  Ext.Object.each(arr, function(key, value){					  								  							  	
								v[key] = value;
				});						     	
				  mioarray.push(v);     	   
		});
			
		if (ritorna == "serie"){
			return (array_fields);
		}
		
		array_fields.push(ar);
		
		var group_data = Ext.create('Ext.data.JsonStore', {
		fields: array_fields,
		data: mioarray,
		});

		if (ritorna == "dati"){
			return (group_data);
		}
};	

function get_implode_array(array, ar_serie){			
		ecc = [array[ar_serie[0]], array[ar_serie[1]]];
		if (array[ar_serie[1]] == null)
			ar = array[ar_serie[0]];
		else
			ar = ecc.join('-');
		return(ar);
};

function controllo_filtri(aspe, divisione, stato_ordine, tipo_ordine, ordini_bloccati, tipologia_ordine, agente, record){
		
		if (aspe != null && (record['TDASPE'].trim() != aspe)) return false;	
	
		//if (tipologia_ordine != null && (record['TDCLOR'].trim() != tipologia_ordine)) return false;
		if (!Ext.isEmpty(tipologia_ordine) && !Ext.Array.contains(tipologia_ordine, record['TDCLOR'].trim())) return false;

		if (agente != null && (record['TDCAG1'].trim() != agente)) return false;
	
		if (!((divisione) != null) || (record['TDCDIV'].trim() == divisione))
		{	
			//if (!((stato_ordine) != null) || (record['TDSTAT'].trim() == stato_ordine))
			if (Ext.isEmpty(stato_ordine) || Ext.Array.contains(stato_ordine, record['TDSTAT'].trim()))
			{														
				controllo = Ext.Array.contains(tipo_ordine, record['TDOTPD'].trim());										
				if (!((tipo_ordine.length) > 0) || (controllo))
				{	
					if(ordini_bloccati == 'tutti'){
						return true;
					}
					if(ordini_bloccati == '1' && (record['TDFN03'].trim() == ordini_bloccati || record['TDFN02'].trim() == ordini_bloccati)){ //solo
						return true;
					}					
					if(ordini_bloccati == '0' && (record['TDFN03'].trim() == ordini_bloccati && record['TDFN02'].trim() == ordini_bloccati)){ //esclusi
						return true;
					}					
					
					
				}
			}
		}
		return false;
};



var personalizza_extraParams_to_jsonData = function personalizza_extraParams_to_jsonData(operation, callback, scope) {
    var writer  = this.getWriter(),
    request = this.buildRequest(operation, callback, scope);

	if (operation.allowWrite()) {
	    request = writer.write(request);
	}

	Ext.apply(request, {
	    headers       : this.headers,
	    timeout       : this.timeout,
	    scope         : this,
	    callback      : this.createRequestCallback(request, operation, callback, scope),
	    method        : this.getMethod(request),
	    disableCaching: false // explicitly set it to false, ServerProxy handles caching
	});
	
	/* extraParams lo passo come jsonData */
	request.jsonData = this.extraParams;
	
	Ext.Ajax.request(request);
	return request;
}


/***********************************************************
*** Istruzion bottoni fascetta excel e invio email *********
************************************************************/

Ext.onReady(function() {

	//invio in formato excel
    if (Ext.get("p_send_excel") != null){		
	  	Ext.get("p_send_excel").on('click', function(){
	  		Ext.getBody().mask('Loading... ', 'loading').show();		  	
	  		 html_text = document.getElementById('my_content').outerHTML;
	  		 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore	 
	  		 html_text = html_text.replace(/<br>/g, "<br/>"); //replace altrimenti va in errore
	  		 style_text = document.getElementsByTagName('style')[0].outerHTML;
	  		 
	  		 
	        Ext.Ajax.request({
	            url : '../base/send_excel.php' ,
	            timeout: 2400000,	            
	            method: 'POST',
	            jsonData: {html_text: html_text, style_text: style_text},
	            success: function ( result, request) {
					Ext.getBody().unmask();	                
	            	//window.open('data:application/vnd.ms-excel,' + escape(result.responseText));
					
					output = result.responseText;
					//output = 'bbbbbbbbbbbb';					
					window.open('data:application/vnd.ms-excel,' + escape(output));
	            },
	            failure: function ( result, request) {
	            }
	        }); 		  		
	  	});
    }
    
  //copy to clipboard
    if (Ext.get("p_copyToClipboard") != null){		
	  	Ext.get("p_copyToClipboard").on('click', function(){
	  			  	
	  		 html_text = document.getElementById('my_content').outerHTML;
	  		 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore	 
	  		 html_text = html_text.replace(/<br>/g, "<br/>"); //replace altrimenti va in errore
	  		 style_text = document.getElementsByTagName('style')[0].outerHTML;
	  		 
	  		const el = document.createElement('textarea');
	  		el.value = html_text;
	  		document.body.appendChild(el);
	  		el.select();
	  		document.execCommand('copy');
	  		document.body.removeChild(el);
	  		return false;
	  				  		
	  	});
    }

	//invio contenuto pagina come e-mail
  if (Ext.get("p_send_email") != null){
	
  	Ext.get("p_send_email").on('click', function(){
	//html_text = document.getElementsByTagName('body')[0].innerHTML;
	 html_text = document.getElementById('my_content').outerHTML;
	 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore
//per test	 	 
//	 html_text = html_text.replace(/&amp;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&gt;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&lt;/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&�/g, ""); //replace altrimenti va in errore	 
//	 html_text = html_text.replace(/&@/g, ""); //replace altrimenti va in errore	 
	 html_text = html_text.replace(/\<br\>/g, "<br/>"); //replace altrimenti va in errore
	 html_text = html_text.replace(/\<hr\>/g, "<hr/>"); //replace altrimenti va in errore

	 style_text = document.getElementsByTagName('style')[0].outerHTML;	 
	 html_text += style_text;
	 //html_text = style_text;
	 //console.log(html_text);
	
	 var obj = document.getElementById('email_obj');
	 if (obj != null) {
		 value_email_subject = document.getElementById('email_obj').innerHTML; 
	 }
	 
	var n_doc = document.implementation.createDocument ('http://www.w3.org/1999/xhtml', 'html',  null);
    n_doc.documentElement.innerHTML = html_text;
  
	html_text = n_doc.getElementsByTagName('html')[0].innerHTML;  
	 
  		
 		var f = Ext.create('Ext.form.Panel', {
        frame: true,
        title: '',        
        bodyPadding: 5,
        url: 'acs_op_exe.php',
        fieldDefaults: {
//            labelAlign: 'top',
            msgTarget: 'side',
            labelWidth: 55,
            anchor: '100%'            
        },
		layout: {
            type: 'vbox',
            align: 'stretch'  // Child items are stretched to full width
        },        

        items: [{
        	xtype: 'hidden',
        	name: 'fn',
        	value: 'exe_send_email'
        	}, {
            xtype: 'combo',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: [ 'email', 'descr' ],
                data: data_email_json
            }),
            displayField: 'descr',
            valueField: 'email',
            fieldLabel: 'A',
            queryMode: 'local',
            selectOnTab: false,
            name: 'to',
           // value : data_email_json[data_email_json.length-1]
		    listeners : {
        	 afterrender: function(comp){
        		 //console.log(data_email_json);
        	     for (var chiave in data_email_json) {
        	    	 if (typeof data_email_json[chiave].default !== 'undefined') {
                	    if (data_email_json[chiave].default == 'Y') {  //data_email_json[chiave]
                		 comp.setValue(data_email_json[chiave][0]);
                		}
        	    	 }
				   }
                 
                // comp.setValue(dft);
           
          }}

        }, {
            xtype: 'textfield',
            name: 'f_oggetto',
            fieldLabel: 'Oggetto',
            anchor: '96%',
		    //allowBlank: false,
		    value: value_email_subject	    
        }
        , {
            xtype: 'htmleditor',
            //xtype: 'textareafield',
            name: 'f_text',
            fieldLabel: 'Testo del messaggio',
            layout: 'fit',
            anchor: '96%',
            height: '100%',
		    allowBlank: false,
		    value: html_text,
		    flex: 1, //riempie tutto lo spazio
			hideLabel: true,
            style: 'margin:0' // Remove default margin		              
        }],

        buttons: [{
            text: 'Invia',
            scale: 'large',
            iconCls: 'icon-email_send-32',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var m_win = this.up('window');	            	
	                form.submit(
								{
		                            //waitMsg:'Loading...',
		                            success: function(form,action) {		                            	
										m_win.close();		        
										// Ext.MessageBox.alert('Avviso', 'Email inviata correttamente');
										acs_show_msg_info('Invio email completato');										                    	
		                            },
		                            failure: function(form,action){
		                                //Ext.MessageBox.alert('Erro');
		                            }
		                        }	                	
	                );
		            //this.up('window').close();            	                	                
	             }            
        		}]
    });


		// create and show window
		var win = new Ext.Window({
		  width: 800
		, height: 380
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Invia e-mail'
		, layout: 'fit'
		, border: true
		, closable: true
		, items: f
		});
		win.show().maximize();  
		    	    		    		
    	});		
		}
	});



//aggiunge 0 non significativi
function pad(number, length) {
    var str = '' + number;    
    
    while (str.length < length) {
        str = '0' + str;
    }
   
    return str;

}



function base64Encode(str) {
    var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var out = "", i = 0, len = str.length, c1, c2, c3;
    while (i < len) {
        c1 = str.charCodeAt(i++) & 0xff;
        if (i == len) {
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt((c1 & 0x3) << 4);
            out += "==";
            break;
        }
        c2 = str.charCodeAt(i++);
        if (i == len) {
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
            out += CHARS.charAt((c2 & 0xF) << 2);
            out += "=";
            break;
        }
        c3 = str.charCodeAt(i++);
        out += CHARS.charAt(c1 >> 2);
        out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
        out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
        out += CHARS.charAt(c3 & 0x3F);
    }
    return out;
}

function ControllaCF(cf){
    	cf = cf.toUpperCase();
    	if( cf == '' )  return '';
    	if( ! /^[0-9A-Z]{16}$/.test(cf) )
    		return "1";   //il codice fiscale non contiene 16 caratteri
    	var map = [1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 1, 0, 5, 7, 9, 13, 15, 17,
    		19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23];
    	var s = 0;
    	for(var i = 0; i < 15; i++){
    		var c = cf.charCodeAt(i);
    		if( c < 65 )
    			c = c - 48;
    		else
    			c = c - 55;
    		if( i % 2 == 0 )
    			s += map[c];
    		else
    			s += c < 10? c : c - 10;
    	}
    	var atteso = String.fromCharCode(65 + s % 26);
    	if( atteso != cf.charAt(15) )
    		return "2"; //il codice fiscale non è valido
    	return "";  //codice fiscale valido
    }



function std_ajax_request(url, params, success_callback, success_callback_parameters){
	  var me = this;
	  Ext.Ajax.request({
	    url: url,  jsonData: params, method:'POST',
	    waitMsg: 'Salvataggio in corso....',	                    
		
		success: function(result, request) {
			
			try {
				var returnData = Ext.JSON.decode(result.responseText);
		    } catch(e) {
				Ext.MessageBox.show({
		               title: 'EXCEPTION',
		               msg: 'Errore in json decode',
		               buttons: Ext.Msg.OK
		       	})
		       	return false;
		    }			
			
			
			
			//return with error
			if (returnData.success == false){
				Ext.MessageBox.show({
	                   title: 'EXCEPTION',
	                   msg: returnData.message,
	                   icon: Ext.MessageBox.ERROR,
	                   buttons: Ext.Msg.OK
	           	})
	          return false;										
			}
							
			if(!Ext.isEmpty(success_callback)){
				if (typeof success_callback === 'string' || success_callback instanceof String){
					if(typeof(me[success_callback]) == 'undefined'){
						console.log('Error ------------------');
						console.log('funzione non definita (' + success_callback + ')');
						console.log('scope: ');
						console.log(me);
						Ext.MessageBox.show({
				               title: 'EXCEPTION',
				               msg: 'funzione non definita (' + success_callback + ')',
				               buttons: Ext.Msg.OK
				       	})
					}   	
					else
					  me[success_callback].call(me, returnData, success_callback_parameters);
				}	
				else //eseguo direttamente la function
				  success_callback.call(me, returnData, success_callback_parameters);
			}
							
		}, scope: this,
		
		failure: function(response, opts) {
			Ext.MessageBox.show({
	               title: 'EXCEPTION',
	               msg: 'Errore sconosciuto',
	               //icon: Ext.MessageBox.ERROR,
	               buttons: Ext.Msg.OK
	       	})					
		}, scope: this,												 
	  });	 	
	} //std_ajax_request
