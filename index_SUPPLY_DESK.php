<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>ACS Portal</title>
	<link rel="stylesheet" type="text/css" href="extjs/resources/css/ext-all.css" />	
	<style type="text/css">
		@import "css/home-main_SUPPLY_DESK.css";
	</style>
	
	<link rel="stylesheet" type="text/css" href="css/toolbars.css" />
	
	<!-- EXTJS  js-->
	<script type="text/javascript" src="extjs/ext-all.js"></script>
	<script type="text/javascript" src="extjs/locale/ext-lang-it.js"></script>
	
	
<script type="text/javascript">

	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', 'ux');


    Ext.require(['*', 'Ext.ux.GMapPanel', 'Ext.ux.grid.FiltersFeature', 'Ext.selection.CheckboxModel']);
    
Ext.onReady(function() {
	
	
 <?php if ($auth->is_sec_admin() || $auth->is_system_configured()==false ) {  ?>	    


 <?php
 global $youtube_video_main_ID;
 if (isset($youtube_video_main_ID)){
 ?>
 Ext.get("logo_cliente").on('click', function(){

 	new Ext.Window({
 	    title : "Info",
 	    width : 300,
 	    height: 300,
 	    layout : 'fit',
 	    items : [{
 	        xtype : "component",
 	        id    : 'iframe-win',
 	        autoEl : {
 	            tag : "iframe",
 	            src : "http://www.youtube.com/embed/<?php echo $youtube_video_main_ID; ?>?autoplay=1" 
 	        }
 	    }]
 	}).show();
 	// Call below code through your any srcUpdate function.
 	// Ext.getDom('iframe-win').src = "http://www.yahoo.com";
  });

 <?php } ?>
 

 
    Ext.get("main-tools").on('click', function(){
			print_w = new Ext.Window({
					  width: 600
					, height: 300
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Pannello di amministrazione'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show(); 
			
				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'module/admin/acs_form_json_amm_pannello.php',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
			   	
    });
 
 <?php } ?>   
    
});    
</script>	
	
	
	
</head>

<body class="home">

   <!-- Begin Wrapper -->
   <div id="wrapper">
   
         <!-- Begin Header -->
         <div id="header">		 
		    <div id="logo_cliente"><img src=<?php echo logo_path(); ?> height=50></div>		
			<span id="logo_portale"><img src="images/logo_portale.png"></span>	    	 			   
		 </div>
		 <!-- End Header -->
		 
		 <!-- Begin Naviagtion -->		 
         <!-- <div id="navigation">		 
		       This is the Navigation		 			   
		 </div> -->
		 <!-- End Naviagtion -->
		 
		 <!-- Begin Content -->
		 <div id="content">		       

			
			 <div id="user">		 
			 <?php if (!$auth->is_authenticate()) : ?>		 
			  <form action="login.php">
				<input type=hidden name=fn value=login>
				<div class="user-div">
				  <table class=tab-user align=right>
				   <tr>
				    <td>
						<img src="images/icone/48x48/vcards.png" width="32" />
						<input type=text name=user>				    
				    </td>
				    <td>&nbsp;</td>
				   </tr>
				   <tr>
				    <td>
						<img src="images/icone/48x48/key.png" width="32"/>
						<input type=password name=password>				    
				    </td>
				    <td valign=bottom>
						<INPUT class="user-submit" style="margin-left: 0px" TYPE="image" SRC="images/icone/48x48/button_blue_play.png" ALT="Effettua Login" WIDTH="32">				    
				    </td>
				   </tr>
				  </table>
				</div>	
			  </form>
			  <?php else : ?>
			  	<!--  elenco moduli ammessi per utente -->
			  	<ul class="module-list">
			  	<?php
					$sql = "SELECT TA_MODUL.* FROM {$cfg_mod_Admin['file_moduli_utente']}
					        INNER JOIN  {$cfg_mod_Admin['file_tabelle']} TA_MODUL 
					        	ON TA_MODUL.TATAID = 'MODUL' AND TA_MODUL.TAKEY1 = UPCMOD  
							WHERE UPPRAC = " . sql_t($auth->get_profile());
		
					$stmt = db2_prepare($conn, $sql);		
					$result = db2_execute($stmt);

					while ($row = db2_fetch_assoc($stmt)) {
						echo "<li><a href=\"module/" . trim($row['TARIF1']) . "\">" . trim($row['TADESC']) . "</a>";
					}
				?>
			  	</ul>
			  
			  <?php endif; ?>
			 </div>			
			

		 </div>
		 <!-- Begin Content -->


		<div id="footer">
			<table width="100%" class="footerApra">
			 <tbody>
			 	<tr valign="middle">
					<td valign="middle"><span style="margin-left: 8px;">ACS_ERP Portal, v 1.2.1</span></td>
							
			 <?php if ($auth->is_authenticate()) : ?>
			    <td style="vertical-align: middle;">
						<img src="images/icone/48x48/vcards.png" width=32/>		  		
						<span style="position: relative; top: -10px;"><?php echo $auth->out_name(); ?></span>
						&nbsp;&nbsp;&nbsp;<a href="login.php?fn=logout"><img src="images/icone/48x48/button_grey_eject.png" width=28/></a>	
				</td>				 
			 <?php endif; ?>		
							
					<td valign="middle" align="right">			
					 <span>Powered by Apra Computer System</span>				 				 
					</td>
			 	</tr>
			</tbody></table>
		</div>
		
		<div id="footer-az">
			<table width="100%">
			 <tbody>
			 	<tr>
			 	 <td colspan=2 align=center>
			 	 	<span><?php echo "Servizio web integrazione utenze gestionali POS � <b>{$dati_azienda['den']}</b>"; ?></span>
			 	 </td>	
			 	</tr>
			</tbody></table>		
		</div>
		 
   </div>
   <!-- End Wrapper -->
   
</body>

</html>