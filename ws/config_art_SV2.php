<?php

//evito in config.inc.php il redirect al login (perche' non ho la session, ma qui non serve)
$disabilita_auth = 'Y';

require_once "../config.inc.php";
require_once "module/base/acs_configuratore_articolo_include.php";

//$m_params = new stdClass();
$m_params = acs_m_params_json_decode();
$m_params->c_art = $m_params->cod_art;

$ret = array();
$token = "2443fd7e39200521827623b3925937af";

//$m_params->token_auth = $token;
/*$m_params->c_art = 'ART-04'; //$m_params->cod_art; //MPD02001
$m_params->op = 'START';
*/

if($m_params->token_auth != $token){
    $ret['success'] = false;
    $ret['cod_errore'] = 'T01';
    $ret['msg_errore'] = 'Errore token non valido';
    echo acs_je($ret);
    exit;        
}


//gestione delle chiamate web service:
// * op: START (inizio configurazione)
if($m_params->op == 'START'){
    $idsc = uniqid();
    
    
    if(strlen($m_params->c_art) == 0){
        $ret['success'] = false;
        $ret['cod_errore'] = 'A01';
        $ret['msg_errore '] = 'Errore codice articolo vuoto';
       
    }
    
    $row_art = get_AR_row($m_params->c_art);
    if(!$row_art || trim($row_art['ARMODE']) == ''){
        $ret['success'] = false;
        $ret['cod_errore'] = 'A02';
        $ret['msg_errore '] = 'Codice articolo non valido o non configurato correttamente';
        echo acs_je($ret);
        exit;
    }
        
   
    insert_TC_row($idsc, $m_params);
    $ret_RI = write_RI_elab_config($idsc, $m_params);
    $row_tc = get_TC_row($idsc);
    
    
    $ret['ses_id'] = $idsc;
    $ret['success'] = true;
    $ret['fine_elaborazione'] = $row_tc['TCFINE'];
    $ret['pagina'] = $row_tc['TCPAGI'];
    $stmt_rc =  get_RC_stmt(0, $idsc, $row_tc['TCPAGI']);
    $ar_domande = array();
    while($row_rc = db2_fetch_assoc($stmt_rc)){
        
        $sequenza = $row_rc['RCSEQ3'].$row_rc['RCSEQ4'];
        $variabile = trim($row_rc['RCVARI']);
        $configuratore = trim($row_rc['RCMODE']);
        
        $row_ta = get_TA_sys('PUVR', $variabile, null, null, null, null, 1);
        $numerica = $row_ta['tarest'];
        if($row_rc['RCTIPO'] != 'S'){
            if($numerica == 'N'){
                $tipo = 'N';
                $ar_valid = get_valid_number($configuratore, $sequenza, $variabile);
                $min = $ar_valid['MDVAM1'];
                $max = $ar_valid['MDVAM5'];
                $default = $row_rc['RCVANN'];
            }else{
                $tipo = 'T';
                $min = "";
                $max = "";
                $default = $row_rc['RCVANA'];
            } 
            
            $ar_varianti = get_valori_ammessi($variabile, $configuratore, $sequenza);
            
            if(count($ar_varianti) == 0)
                $ar_varianti = find_TA_sys('PUVN', null, null, $variabile, null, null, 0, '', 'Y');
            
            $risposte = array();  
            foreach($ar_varianti as $v)
                $risposte[] = array('cod_risposta' => $v['id'], "descr_risposta" => $v['text']);
             
            $domanda = array( "id_riga" => $row_rc['RRN'],
                "cod_domanda"	=> $variabile,
                "descr_domanda"	=> $row_ta['text'],
                "tipo" => $tipo,
                "val_min" => $min,
                "val_MAX" => $max,
                "default" => $default,
                "risposte" => $risposte
            );
        
             $ar_domande[] = $domanda;
        }
    }
    
    $ret['domande'] = $ar_domande;
    
}


 
// * op: NEXT  (salva scelte da utente e elabora prossima config)
/*$m_params->op = 'NEXT';
$m_params->ses_id = '5e57ccc983ad2'; 
$m_params->risposte = array(
    array('id_riga' => '747', 'valore_risposta' => 'LT')
);
*/

if($m_params->op == 'NEXT'){       

    $row_tc = get_TC_row($m_params->ses_id);
        
    if(!$row_tc){
        $ret['success'] = false;
        $ret['cod_errore'] = 'ID01';
        $ret['msg_errore'] = 'Errore ID sessione non valido';
        echo acs_je($ret);
        exit;
    }
    
    if($row_tc['TCFINE'] == 'Y'){
        $ret['success'] = false;
        $ret['cod_errore'] = 'F01';
        $ret['msg_errore'] = 'Sessione di elaborazione gi&agrave; conclusa';
        echo acs_je($ret);
        exit;
        
    }
    
    
    $m_array = array();
    foreach($m_params->risposte as $v)
        $m_array[$v->id_riga] = $v->valore_risposta;
    
    $error_rrn = '';
    foreach($m_array as $k =>$v){
        if (is_numeric($k)){
            $stmt_rc =  get_RC_stmt($k);
            $row_rc = db2_fetch_assoc($stmt_rc);
            if(!$row_rc) $error_rrn = 'Y';
        }else $error_rrn = 'Y';
    }
    
    if($error_rrn == 'Y'){
        $ret['success'] = false;
        $ret['cod_errore'] = 'R01';
        $ret['msg_errore'] = 'Errore ID Riga non valido';
        echo acs_je($ret);
        exit;
    }
      
    
    update_RC_row($m_array);
    $ret_RI = write_RI_elab_config($m_params->ses_id , $m_params);
    if($ret_RI['RIESIT'] == 'W'){
        $errore = $ret_RI['RINOTE'];
        return;
    }
    
    
    $ret['success']  = true;
    $row_tc = get_TC_row($m_params->ses_id);
    $ret['fine_elaborazione'] = $row_tc['TCFINE'];
    if($ret['fine_elaborazione'] != 'Y'){
        $ret['pagina'] = $row_tc['TCPAGI'];
        
        $stmt_rc =  get_RC_stmt(0, $m_params->ses_id, $row_tc['TCPAGI']);
        $ar_domande = array();
        while($row_rc = db2_fetch_assoc($stmt_rc)){
            
            $sequenza = $row_rc['RCSEQ3'].$row_rc['RCSEQ4'];
            $variabile = trim($row_rc['RCVARI']);
            $configuratore = trim($row_rc['RCMODE']);
            
            $row_ta = get_TA_sys('PUVR', $variabile, null, null, null, null, 1);
            $numerica = $row_ta['tarest'];
            if($row_rc['RCTIPO'] != 'S'){
                if($numerica == 'N'){
                    $tipo = 'N';
                    $ar_valid = get_valid_number($configuratore, $sequenza, $variabile);
                    $min = $ar_valid['MDVAM1'];
                    $max = $ar_valid['MDVAM5'];
                    $default = $row_rc['RCVANN'];
                }else{
                    $tipo = 'T';
                    $min = "";
                    $max = "";
                    $default = $row_rc['RCVANA'];
                }
                
                $ar_varianti = get_valori_ammessi($variabile, $configuratore, $sequenza);
                
                if(count($ar_varianti) == 0)
                    $ar_varianti = find_TA_sys('PUVN', null, null, $variabile, null, null, 0, '', 'Y');
                 
                $risposte = array();
                foreach($ar_varianti as $v)
                    $risposte[] = array('cod_risposta' => $v['id'], "descr_risposta" => $v['text']);
                    
                    $domanda = array( "id_riga" 	=> $row_rc['RRN'],
                        "cod_domanda"	=> $variabile,
                        "descr_domanda"	=> $row_ta['text'],
                        "tipo" => $tipo,
                        "val_min" => $min,
                        "val_MAX" => $max,
                        "default" => $default,
                        "risposte" => $risposte
                    );
                    
                    
                    $ar_domande[] = $domanda;
            }  
        }
        
        $ret['domande'] = $ar_domande;
    }
    
}
echo acs_je($ret);
exit;



