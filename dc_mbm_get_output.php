<?php

error_reporting(E_ALL);
# Engine configuration
#  - first we enable error display
ini_set('display_errors',1);
#  - next we ensure that all errors are displayed
ini_set('error_reporting',E_ALL);

//forzo anche senza sessione
require_once('auth.php');
session_start();
$auth = Auth::get_session_auth();
if ($auth->is_authenticate('Y') == 0) {
	$auth->set_authenticate('FIRMA');
}


require_once "config.inc.php";

//$s = new Spedizioni();

ini_set('max_execution_time', 6000);

 $currentIncludePath = get_include_path();

 set_include_path($currentIncludePath.  PATH_SEPARATOR . ROOT_ABS_PATH . "utility/phpseclib1.0.5");

 require('Crypt/Base.php');
 require('Crypt/RC4.php');
 require('Crypt/Hash.php');
 require('Math/BigInteger.php');
 require('Net/SSH2.php');
 require('Net/SFTP.php');
 require('Crypt/Random.php');
 require('Crypt/Rijndael.php');
 

 define('NET_SFTP_LOGGING', NET_SFTP_LOG_COMPLEX);

 $sftp = new Net_SFTP($cfg_delivery_control_sftp['host']);
 if (!$sftp->login($cfg_delivery_control_sftp['user'], $cfg_delivery_control_sftp['psw'])) {
	exit('Login Failed');
 }

 
 foreach($sftp->rawlist('./xcliente/') as $filename => $attrs) {
 	echo "\n{$filename}";
 	
 	if (substr($filename, 0, 9) == 'consegne_' && substr($filename, -4) == '.dat') {
 		
 		$tmp_file = "/tmp/" . $filename;
 		if ($sftp->get("./xcliente/$filename", $tmp_file)){
 			echo "\nOk download";
 			if (($handle = fopen($tmp_file, "r")) !== FALSE) {
 				echo "\nOk apertura file in lettura";
 		
 				while(!feof($handle)) {
 		
 					$line = fgets($handle);
 					$row  = explode("|", trim($line));
 		
 					if (count($row) != 13) break;
 		
 					$ar_ins = array();
 					$ar_ins['DOESIT'] 	= 'A';
 					$ar_ins['DOID'] 	= $filename;
 					$ar_ins['DODTRI'] 	= oggi_AS_date();
 					$ar_ins['DOHMRI'] 	= oggi_AS_time();
 					$ar_ins['DOUSRI'] 	= $auth->get_user();
 		
 					$ar_ins['DODITT'] 	= $row[0];
 					$ar_ins['DOCDFI'] 	= $row[1];
 					$ar_ins['DODPRO'] 	= $row[2];
 					$ar_ins['DODELI'] 	= $row[3];
 					$ar_ins['DOCODE'] 	= $row[4];
 					$ar_ins['DORESU'] 	= $row[5];
 					$ar_ins['DOCAUS'] 	= $row[6];
 					$ar_ins['DODTCO'] 	= $row[7];
 					$ar_ins['DOLATI'] 	= $row[8];
 					$ar_ins['DOLONG'] 	= $row[9];
 					$ar_ins['DOPALM'] 	= $row[10];
 					$ar_ins['DOCFSP'] 	= $row[11];
 					$ar_ins['DONOSC'] 	= $row[12];
 		
 		
 					$sql = "INSERT INTO {$cfg_delivery_control_sftp['file_esito']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
 					$stmt = db2_prepare($conn, $sql);
 					echo db2_stmt_errormsg();
 					$result = db2_execute($stmt, $ar_ins);
 					echo db2_stmt_errormsg($stmt);
 					if ($result == false){
 						echo "\n" . $sql . "\n";
 						print_r($ar_ins);
 					}
 				} //per ogni riga

 				fclose($handle);
 				
 				//sposto il file su ftp
 				echo "\nSposto file";
 				
 				$sftp->rename("./xcliente/$filename", "./xcliente/loaded/$filename");
  				
 			} //apertura file
 		
 		}
 		
 	} //if consegene_... .dat
 	
 	//print_r($attrs);
 	echo "\n----------------------------";
 }
 
 exit;
 
 $tmp_file = "/tmp/" . 'consegne_20170418_1536.dat';
 if ($sftp->get('./xcliente/consegne_20170418_1536.dat', $tmp_file)){
 	echo "\nOk download";
 	if (($handle = fopen($tmp_file, "r")) !== FALSE) {
 		echo "\nOk apertura file in lettura";

 		while(!feof($handle)) { 			
 			
 			$line = fgets($handle);
 			$row  = explode("|", trim($line));
 			
 			if (count($row) != 12) break;
 			
 			$ar_ins = array();
 			$ar_ins['DOID'] 	= 'consegne_20170418_1536.dat'; //$attrs['filename'];
 			$ar_ins['DODTRI'] 	= oggi_AS_date();
 			$ar_ins['DOHMRI'] 	= oggi_AS_time();
 			$ar_ins['DOUSRI'] 	= $auth->get_user();
 			
 			$ar_ins['DODITT'] 	= $row[0];
 			$ar_ins['DOCDFI'] 	= $row[1];
 			$ar_ins['DODPRO'] 	= $row[2];
 			$ar_ins['DODELI'] 	= $row[3];
 			$ar_ins['DOCODE'] 	= $row[4];
 			$ar_ins['DORESU'] 	= $row[5];
 			$ar_ins['DOCAUS'] 	= $row[6];
 			$ar_ins['DODTCO'] 	= $row[7];
 			$ar_ins['DOLATI'] 	= $row[8];
 			$ar_ins['DOLONG'] 	= $row[9];
 			$ar_ins['DOPALM'] 	= $row[10];
 			
 			
 			$sql = "INSERT INTO {$cfg_delivery_control_sftp['file_esito']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
 			$stmt = db2_prepare($conn, $sql);
 			echo db2_stmt_errormsg();
 			$result = db2_execute($stmt, $ar_ins); 			 			 			
 			echo db2_stmt_errormsg($stmt);
 		} //per ogni riga
 			
 	} //apertura file
 	
 }
