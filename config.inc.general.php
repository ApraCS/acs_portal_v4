<?php

$system_general_tipologia_utente = array(
		array("id" => "AMM", 	"text"	=>	"Amministratore"),
		array("id" => "USER", 	"text"	=>	"Operatore"),
		array("id" => "GUEST", 	"text"	=>	"Guest"),
		array("id" => "TRASP", 	"text"	=>	"Trasportatore"),
		array("id" => "TERZ", 	"text"	=>	"Terzista"),
		array("id" => "FORN", 	"text"	=>	"Fornitore"),
		array("id" => "AGENTE",	"text"	=>	"Agente"),
        array("id" => "CLI",	"text"	=>	"Cliente"),
        array("id" => "SECAM",	"text"	=>	"Amministratore della sicurezza")
);
	
?>